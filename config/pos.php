<?php

return [
    'echo_server' => '',

    'echo_enabled' => false,

    'is_branch' => false,

    'price_list' => 1,

    'cash_cashbook_batch' => 3,

    'cash_receipt_cashbook_batch' => 10,

    'mpesa_cashbook_batch' => 4,

    'super_password' => 'Qwerty123!',

    'main_server_ip' => '127.0.0.1',

    'site_id' => 'HOL',

    'cash_customer_id' => 1,

    'has_alternate_price' => false,

    'has_second_price' => false,

    'has_uom' => false,

    'rr_stock_price_list' => 1,

    'rr_out_price_list' => 2,

    'alt_stock_price_list' => 3,

    'alt_out_price_list' => 4,

];
