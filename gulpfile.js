const elixir = require('laravel-elixir');

require('laravel-elixir-vueify');
require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix
        .copy([
            'node_modules/font-awesome/fonts/**',
            'node_modules/bootstrap-sass/assets/fonts/**'
        ], 'public/build/fonts')
        .copy(
            'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
            'resources/assets/sass/plugins/dataTables.scss'
        )
        .copy(
            'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            'resources/assets/sass/plugins/datepicker.scss'
        )
        .sass('app.scss')
        .webpack('app.js','./resources/assets/js/compiled/app.js')
        .scripts(['plugins/pace.min.js','plugins/axios.min.js', 'compiled/app.js'], 'public/js/app.js')
        .version(['css/app.css', 'js/app.js']);
});
