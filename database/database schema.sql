-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2017 at 10:11 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `POS_BACKUP`
--
CREATE DATABASE IF NOT EXISTS `POS_BACKUP` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `POS_BACKUP`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `AccountLink` int(11) NOT NULL,
  `Master_Sub_Account` varchar(91) DEFAULT NULL,
  `AccountLevel` int(11) DEFAULT NULL,
  `Account` varchar(91) DEFAULT NULL,
  `iAccountType` int(11) NOT NULL DEFAULT '0',
  `SubAccOfLink` int(11) DEFAULT NULL,
  `Dept` varchar(10) DEFAULT NULL,
  `Brch` varchar(10) DEFAULT NULL,
  `Jr` tinyint(1) NOT NULL DEFAULT '0',
  `Description` varchar(255) DEFAULT NULL,
  `CaseAcc` varchar(10) DEFAULT NULL,
  `ActiveAccount` tinyint(1) NOT NULL DEFAULT '1',
  `dAccountsTimeStamp` datetime(6) DEFAULT NULL,
  `cNextChequeNum` varchar(20) DEFAULT NULL,
  `iGLSegment0ID` int(11) DEFAULT NULL,
  `iGLSegment1ID` int(11) DEFAULT NULL,
  `iGLSegment2ID` int(11) DEFAULT NULL,
  `iGLSegment3ID` int(11) DEFAULT NULL,
  `iGLSegment4ID` int(11) DEFAULT NULL,
  `iGLSegment5ID` int(11) DEFAULT NULL,
  `iGLSegment6ID` int(11) DEFAULT NULL,
  `iGLSegment7ID` int(11) DEFAULT NULL,
  `iGLSegment8ID` int(11) DEFAULT NULL,
  `iGLSegment9ID` int(11) DEFAULT NULL,
  `iReportCategoryID` int(11) DEFAULT NULL,
  `fBankStatementBalance` double DEFAULT NULL,
  `cExtDescription` varchar(255) DEFAULT NULL,
  `iTaxTypeINVID` int(11) DEFAULT NULL,
  `iTaxTypeCRNID` int(11) DEFAULT NULL,
  `iTaxTypeGRVID` int(11) DEFAULT NULL,
  `iTaxTypeRTSID` int(11) DEFAULT NULL,
  `iAllowICSales` tinyint(1) NOT NULL DEFAULT '0',
  `iAllowICPurchases` tinyint(1) NOT NULL DEFAULT '0',
  `iMBReportingCategoryID` int(11) DEFAULT NULL,
  `iMBCashFlowCategoryID` int(11) DEFAULT NULL,
  `bMBIsAsset` tinyint(1) NOT NULL DEFAULT '0',
  `bMBIsGrant` tinyint(1) NOT NULL DEFAULT '0',
  `iMBAssetClassificationID` int(11) DEFAULT NULL,
  `iMBAssetCategoryID` int(11) DEFAULT NULL,
  `iMBAssetTypeID` int(11) DEFAULT NULL,
  `iMBGrantLevel1TypeID` int(11) DEFAULT NULL,
  `iMBGrantLevel2TypeID` int(11) DEFAULT NULL,
  `iMBGrantLevel3TypeID` int(11) DEFAULT NULL,
  `bIsBranchLoanAccount` tinyint(1) NOT NULL DEFAULT '0',
  `bForeignBankAcc` tinyint(1) NOT NULL DEFAULT '0',
  `iForeignBankCurrencyID` int(11) DEFAULT NULL,
  `iForeignBankPEXAccID` int(11) DEFAULT NULL,
  `iForeignBankLEXAccID` int(11) DEFAULT NULL,
  `bRevalueWithSellingRate` tinyint(1) NOT NULL DEFAULT '1',
  `bPaymentsBasedTax` tinyint(1) NOT NULL DEFAULT '0',
  `cBankName` varchar(40) DEFAULT NULL,
  `cBankAccountName` varchar(50) DEFAULT NULL,
  `cBankCode` varchar(15) DEFAULT NULL,
  `cBankAccountNumber` varchar(40) DEFAULT NULL,
  `cBranchName` varchar(30) DEFAULT NULL,
  `cSEPABranchCode` varchar(30) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `Accounts_iBranchID` int(11) DEFAULT NULL,
  `Accounts_dCreatedDate` datetime(6) DEFAULT NULL,
  `Accounts_dModifiedDate` datetime(6) DEFAULT NULL,
  `Accounts_iCreatedBranchID` int(11) DEFAULT NULL,
  `Accounts_iModifiedBranchID` int(11) DEFAULT NULL,
  `Accounts_iCreatedAgentID` int(11) DEFAULT NULL,
  `Accounts_iModifiedAgentID` int(11) DEFAULT NULL,
  `Accounts_iChangeSetID` int(11) DEFAULT NULL,
  `Accounts_Checksum` binary(20) DEFAULT NULL,
  `ulGLSector1` varchar(100) DEFAULT NULL,
  `ulGLSector2` varchar(100) DEFAULT NULL,
  `ulGLSector3` varchar(100) DEFAULT NULL,
  `ulGLSector4` varchar(100) DEFAULT NULL,
  `ulGLSector5` varchar(100) DEFAULT NULL,
  `ulGLSector6` varchar(100) DEFAULT NULL,
  `ulGLSector7` varchar(100) DEFAULT NULL,
  `ulGLSector8` varchar(100) DEFAULT NULL,
  `ulGLSector9` varchar(100) DEFAULT NULL,
  `ulGLSector10` varchar(100) DEFAULT NULL,
  `ulGLCostCentre` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `accprev`
--

DROP TABLE IF EXISTS `accprev`;
CREATE TABLE `accprev` (
  `LedgerLink` int(11) NOT NULL,
  `BFDebits00` double DEFAULT NULL,
  `BFDebits01` double DEFAULT NULL,
  `BFDebits02` double DEFAULT NULL,
  `BFDebits03` double DEFAULT NULL,
  `BFDebits04` double DEFAULT NULL,
  `BFDebits05` double DEFAULT NULL,
  `BFCredits00` double DEFAULT NULL,
  `BFCredits01` double DEFAULT NULL,
  `BFCredits02` double DEFAULT NULL,
  `BFCredits03` double DEFAULT NULL,
  `BFCredits04` double DEFAULT NULL,
  `BFCredits05` double DEFAULT NULL,
  `PrevBal01` double DEFAULT NULL,
  `PrevBal02` double DEFAULT NULL,
  `PrevBal03` double DEFAULT NULL,
  `PrevBal04` double DEFAULT NULL,
  `PrevBal05` double DEFAULT NULL,
  `PrevBal06` double DEFAULT NULL,
  `PrevBal07` double DEFAULT NULL,
  `PrevBal08` double DEFAULT NULL,
  `PrevBal09` double DEFAULT NULL,
  `PrevBal10` double DEFAULT NULL,
  `PrevBal11` double DEFAULT NULL,
  `PrevBal12` double DEFAULT NULL,
  `BFForeignDebits00` double DEFAULT NULL,
  `BFForeignDebits01` double DEFAULT NULL,
  `BFForeignDebits02` double DEFAULT NULL,
  `BFForeignDebits03` double DEFAULT NULL,
  `BFForeignDebits04` double DEFAULT NULL,
  `BFForeignDebits05` double DEFAULT NULL,
  `BFForeignCredits00` double DEFAULT NULL,
  `BFForeignCredits01` double DEFAULT NULL,
  `BFForeignCredits02` double DEFAULT NULL,
  `BFForeignCredits03` double DEFAULT NULL,
  `BFForeignCredits04` double DEFAULT NULL,
  `BFForeignCredits05` double DEFAULT NULL,
  `PrevForeignBal01` double DEFAULT NULL,
  `PrevForeignBal02` double DEFAULT NULL,
  `PrevForeignBal03` double DEFAULT NULL,
  `PrevForeignBal04` double DEFAULT NULL,
  `PrevForeignBal05` double DEFAULT NULL,
  `PrevForeignBal06` double DEFAULT NULL,
  `PrevForeignBal07` double DEFAULT NULL,
  `PrevForeignBal08` double DEFAULT NULL,
  `PrevForeignBal09` double DEFAULT NULL,
  `PrevForeignBal10` double DEFAULT NULL,
  `PrevForeignBal11` double DEFAULT NULL,
  `PrevForeignBal12` double DEFAULT NULL,
  `iTxBranchPrevID` int(11) NOT NULL DEFAULT '-1',
  `AccPrev_iBranchID` int(11) DEFAULT NULL,
  `AccPrev_dCreatedDate` datetime(6) DEFAULT NULL,
  `AccPrev_dModifiedDate` datetime(6) DEFAULT NULL,
  `AccPrev_iCreatedBranchID` int(11) DEFAULT NULL,
  `AccPrev_iModifiedBranchID` int(11) DEFAULT NULL,
  `AccPrev_iCreatedAgentID` int(11) DEFAULT NULL,
  `AccPrev_iModifiedAgentID` int(11) DEFAULT NULL,
  `AccPrev_iChangeSetID` int(11) DEFAULT NULL,
  `AccPrev_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `idAreas` int(11) NOT NULL,
  `Code` varchar(10) NOT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `Areas_iBranchID` int(11) DEFAULT NULL,
  `Areas_dCreatedDate` datetime(6) DEFAULT NULL,
  `Areas_dModifiedDate` datetime(6) DEFAULT NULL,
  `Areas_iCreatedBranchID` int(11) DEFAULT NULL,
  `Areas_iModifiedBranchID` int(11) DEFAULT NULL,
  `Areas_iCreatedAgentID` int(11) DEFAULT NULL,
  `Areas_iModifiedAgentID` int(11) DEFAULT NULL,
  `Areas_iChangeSetID` int(11) DEFAULT NULL,
  `Areas_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bankmain`
--

DROP TABLE IF EXISTS `bankmain`;
CREATE TABLE `bankmain` (
  `Counter` int(11) NOT NULL,
  `BankName` varchar(40) DEFAULT NULL,
  `Branch` varchar(12) DEFAULT NULL,
  `ActiveBank` varchar(1) DEFAULT NULL,
  `BankMain_iBranchID` int(11) DEFAULT NULL,
  `BankMain_dCreatedDate` datetime(6) DEFAULT NULL,
  `BankMain_dModifiedDate` datetime(6) DEFAULT NULL,
  `BankMain_iCreatedBranchID` int(11) DEFAULT NULL,
  `BankMain_iModifiedBranchID` int(11) DEFAULT NULL,
  `BankMain_iCreatedAgentID` int(11) DEFAULT NULL,
  `BankMain_iModifiedAgentID` int(11) DEFAULT NULL,
  `BankMain_iChangeSetID` int(11) DEFAULT NULL,
  `BankMain_Checksum` binary(20) DEFAULT NULL,
  `iSagePayBank` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bomcomp`
--

DROP TABLE IF EXISTS `bomcomp`;
CREATE TABLE `bomcomp` (
  `BomComponentKey` int(11) NOT NULL,
  `BomMasterKey` int(11) DEFAULT NULL,
  `ComponentStockLink` int(11) DEFAULT NULL,
  `ComponentIndex` int(11) DEFAULT NULL,
  `ProductionQty` double DEFAULT NULL,
  `UnitOfMeasure` varchar(4) DEFAULT NULL,
  `UnitCost` double DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `DefaultWhseID` int(11) DEFAULT NULL,
  `fBreakAllocCostPerc` double DEFAULT NULL,
  `fOffsetLeadTime` double DEFAULT NULL,
  `BomComp_iBranchID` int(11) DEFAULT NULL,
  `BomComp_dCreatedDate` datetime(6) DEFAULT NULL,
  `BomComp_dModifiedDate` datetime(6) DEFAULT NULL,
  `BomComp_iCreatedBranchID` int(11) DEFAULT NULL,
  `BomComp_iModifiedBranchID` int(11) DEFAULT NULL,
  `BomComp_iCreatedAgentID` int(11) DEFAULT NULL,
  `BomComp_iModifiedAgentID` int(11) DEFAULT NULL,
  `BomComp_iChangeSetID` int(11) DEFAULT NULL,
  `BomComp_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bomdef`
--

DROP TABLE IF EXISTS `bomdef`;
CREATE TABLE `bomdef` (
  `TxCode` varchar(20) DEFAULT NULL,
  `BrkUpTxCode` varchar(20) DEFAULT NULL,
  `UpdCostTxCode` varchar(20) DEFAULT NULL,
  `PromptToManuf` int(11) DEFAULT NULL,
  `TempNumber` int(11) DEFAULT NULL,
  `NextBillManufRef` varchar(50) DEFAULT NULL,
  `NextBillBrkRef` varchar(50) DEFAULT NULL,
  `NextBillUpdateRef` varchar(50) DEFAULT NULL,
  `iBreakupOnGrvPrompt` int(11) DEFAULT NULL,
  `bShowComponentsOnGRV` tinyint(1) DEFAULT NULL,
  `iAccountsIDDefSurplus` int(11) DEFAULT NULL,
  `iMFTxCodeID` int(11) DEFAULT NULL,
  `iDRTxCodeID` int(11) DEFAULT NULL,
  `iBPTxCodeID` int(11) DEFAULT NULL,
  `iWATxCodeID` int(11) DEFAULT NULL,
  `iUMTxCodeID` int(11) DEFAULT NULL,
  `iVATxCodeID` int(11) DEFAULT NULL,
  `bShowAvailAllLevels` tinyint(1) NOT NULL DEFAULT '0',
  `bShowCompOnInv` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoNumManuf` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoNumBreakUp` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoNumUpdateCosts` tinyint(1) NOT NULL DEFAULT '0',
  `bPromptPickSlip` tinyint(1) NOT NULL DEFAULT '0',
  `bAlwaysPrintPickSlip` tinyint(1) NOT NULL DEFAULT '0',
  `bManufAutoNumLine` tinyint(1) NOT NULL DEFAULT '0',
  `cNextManufRefLine` varchar(50) DEFAULT NULL,
  `bOverwriteAutoManufRef` tinyint(1) NOT NULL DEFAULT '0',
  `iMFPFilterStLength` int(11) NOT NULL DEFAULT '0',
  `bPickSlipPrintAllLines` tinyint(1) NOT NULL DEFAULT '0',
  `bPickSlipPrintUnprintedLines` tinyint(1) NOT NULL DEFAULT '0',
  `bPickSlipPrintLastSessionLines` tinyint(1) NOT NULL DEFAULT '1',
  `bPickSlipPrintSubManuf` tinyint(1) NOT NULL DEFAULT '0',
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `bUniqueManufRef` tinyint(1) DEFAULT NULL,
  `idBomDef` int(11) NOT NULL,
  `iDefaultComponentWhseID` int(11) DEFAULT NULL,
  `bOverDrawWarning` tinyint(1) NOT NULL DEFAULT '0',
  `BomDef_iBranchID` int(11) DEFAULT NULL,
  `BomDef_dCreatedDate` datetime(6) DEFAULT NULL,
  `BomDef_dModifiedDate` datetime(6) DEFAULT NULL,
  `BomDef_iCreatedBranchID` int(11) DEFAULT NULL,
  `BomDef_iModifiedBranchID` int(11) DEFAULT NULL,
  `BomDef_iCreatedAgentID` int(11) DEFAULT NULL,
  `BomDef_iModifiedAgentID` int(11) DEFAULT NULL,
  `BomDef_iChangeSetID` int(11) DEFAULT NULL,
  `BomDef_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bommast`
--

DROP TABLE IF EXISTS `bommast`;
CREATE TABLE `bommast` (
  `BomID` int(11) NOT NULL,
  `BomStockLink` int(11) DEFAULT NULL,
  `BomStockCode` varchar(255) DEFAULT NULL,
  `BomDescription` varchar(50) DEFAULT NULL,
  `BomProductionQty` double DEFAULT NULL,
  `BomUnitCost` double DEFAULT NULL,
  `ThisLevelCost` double DEFAULT NULL,
  `DateLastCosted` datetime DEFAULT NULL,
  `bBreakOnGrv` tinyint(1) NOT NULL DEFAULT '0',
  `bBreakAllocCostsbyPerc` tinyint(1) NOT NULL DEFAULT '0',
  `bShowComponentsOnGRV` tinyint(1) NOT NULL DEFAULT '0',
  `bShowAvailAllLevels` tinyint(1) NOT NULL DEFAULT '0',
  `bShowCompOnInv` tinyint(1) NOT NULL DEFAULT '0',
  `bManufWithDefaultWH` tinyint(1) NOT NULL DEFAULT '0',
  `bBreakUpWithDefaultWH` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowOverUnderManufacture` tinyint(1) NOT NULL DEFAULT '0',
  `BomMast_fLeadDays` double DEFAULT NULL,
  `BomMast_iBranchID` int(11) DEFAULT NULL,
  `BomMast_dCreatedDate` datetime(6) DEFAULT NULL,
  `BomMast_dModifiedDate` datetime(6) DEFAULT NULL,
  `BomMast_iCreatedBranchID` int(11) DEFAULT NULL,
  `BomMast_iModifiedBranchID` int(11) DEFAULT NULL,
  `BomMast_iCreatedAgentID` int(11) DEFAULT NULL,
  `BomMast_iModifiedAgentID` int(11) DEFAULT NULL,
  `BomMast_iChangeSetID` int(11) DEFAULT NULL,
  `BomMast_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ccdefs`
--

DROP TABLE IF EXISTS `ccdefs`;
CREATE TABLE `ccdefs` (
  `idCCDefs` int(11) NOT NULL,
  `PassScore` double NOT NULL,
  `Completeness` longtext,
  `iPromptOpt` int(11) DEFAULT NULL,
  `CCDefs_iBranchID` int(11) DEFAULT NULL,
  `CCDefs_dCreatedDate` datetime(6) DEFAULT NULL,
  `CCDefs_dModifiedDate` datetime(6) DEFAULT NULL,
  `CCDefs_iCreatedBranchID` int(11) DEFAULT NULL,
  `CCDefs_iModifiedBranchID` int(11) DEFAULT NULL,
  `CCDefs_iCreatedAgentID` int(11) DEFAULT NULL,
  `CCDefs_iModifiedAgentID` int(11) DEFAULT NULL,
  `CCDefs_iChangeSetID` int(11) DEFAULT NULL,
  `CCDefs_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ccdetail`
--

DROP TABLE IF EXISTS `ccdetail`;
CREATE TABLE `ccdetail` (
  `DebtorLink` int(11) NOT NULL,
  `TradeName` varchar(35) DEFAULT NULL,
  `TypeofBus` varchar(20) DEFAULT NULL,
  `MonthlyPur` double DEFAULT NULL,
  `Granted` tinyint(1) DEFAULT '0',
  `BankCode` smallint(6) DEFAULT '8',
  `BankContact` varchar(30) DEFAULT NULL,
  `BankDate` datetime DEFAULT NULL,
  `BankAmount` double DEFAULT NULL,
  `BankRD` tinyint(1) DEFAULT '0',
  `DateBusStart` datetime DEFAULT NULL,
  `LastCredGrant` datetime DEFAULT NULL,
  `LastCredDate` datetime DEFAULT NULL,
  `AuditorName` varchar(30) DEFAULT NULL,
  `CCDetail_iBranchID` int(11) DEFAULT NULL,
  `CCDetail_dCreatedDate` datetime(6) DEFAULT NULL,
  `CCDetail_dModifiedDate` datetime(6) DEFAULT NULL,
  `CCDetail_iCreatedBranchID` int(11) DEFAULT NULL,
  `CCDetail_iModifiedBranchID` int(11) DEFAULT NULL,
  `CCDetail_iCreatedAgentID` int(11) DEFAULT NULL,
  `CCDetail_iModifiedAgentID` int(11) DEFAULT NULL,
  `CCDetail_iChangeSetID` int(11) DEFAULT NULL,
  `CCDetail_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cliclass`
--

DROP TABLE IF EXISTS `cliclass`;
CREATE TABLE `cliclass` (
  `IdCliClass` int(11) NOT NULL,
  `Code` varchar(20) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `dGroupTimeStamp` datetime(6) DEFAULT NULL,
  `iAccountsIDControlAcc` int(11) DEFAULT NULL,
  `iAccountsIDProfitAcc` int(11) DEFAULT NULL,
  `iAccountsIDLossAcc` int(11) DEFAULT NULL,
  `iTaxControlAccID` int(11) DEFAULT NULL,
  `iRevProfitAcc` int(11) DEFAULT NULL,
  `iRevLossAcc` int(11) DEFAULT NULL,
  `iProvForRevAcc` int(11) DEFAULT NULL,
  `iInvoiceDocProfileID` int(11) DEFAULT NULL,
  `iSOInvoiceDocProfileID` int(11) DEFAULT NULL,
  `iCreditNoteDocProfileID` int(11) DEFAULT NULL,
  `iJCInvoiceDocProfileID` int(11) DEFAULT NULL,
  `CliClass_iBranchID` int(11) DEFAULT NULL,
  `CliClass_dCreatedDate` datetime(6) DEFAULT NULL,
  `CliClass_dModifiedDate` datetime(6) DEFAULT NULL,
  `CliClass_iCreatedBranchID` int(11) DEFAULT NULL,
  `CliClass_iModifiedBranchID` int(11) DEFAULT NULL,
  `CliClass_iCreatedAgentID` int(11) DEFAULT NULL,
  `CliClass_iModifiedAgentID` int(11) DEFAULT NULL,
  `CliClass_iChangeSetID` int(11) DEFAULT NULL,
  `CliClass_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clidef`
--

DROP TABLE IF EXISTS `clidef`;
CREATE TABLE `clidef` (
  `idCliDef` int(11) NOT NULL,
  `AutoYN` varchar(1) DEFAULT NULL,
  `AutoLength` int(11) DEFAULT NULL,
  `AutoAlphaLength` int(11) DEFAULT NULL,
  `UpperAccNo` varchar(1) DEFAULT NULL,
  `ForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `DefaultTxDesc` tinyint(1) NOT NULL DEFAULT '0',
  `LatestVDPrice` tinyint(1) NOT NULL DEFAULT '0',
  `FilterStartLength` int(11) DEFAULT '0',
  `iPDAgentID` int(11) DEFAULT NULL,
  `iPDPromptOpt` int(11) DEFAULT NULL,
  `dPDLastPrompt` datetime(6) DEFAULT NULL,
  `iTaxRateIdNoCharge` int(11) DEFAULT NULL,
  `iPEXTrCodeID` int(11) DEFAULT NULL,
  `iLEXTrCodeID` int(11) DEFAULT NULL,
  `fTaxPromptAmount` double DEFAULT NULL,
  `iDelAddressCodeID1` int(11) DEFAULT NULL,
  `iDelAddressCodeID2` int(11) DEFAULT NULL,
  `bUseAllocStoredProc` tinyint(1) NOT NULL DEFAULT '1',
  `bInvTxCheckAccAfterChange` tinyint(1) NOT NULL DEFAULT '0',
  `bUseRounding` tinyint(1) NOT NULL DEFAULT '0',
  `bRoundPOSOnly` tinyint(1) NOT NULL DEFAULT '0',
  `fMinRoundDenom` double NOT NULL,
  `iRoundToOpt` int(11) DEFAULT NULL,
  `iRoundingGLAccountID` int(11) DEFAULT NULL,
  `bForceRep` tinyint(1) NOT NULL DEFAULT '0',
  `iEFTSLayoutID` int(11) NOT NULL DEFAULT '0',
  `cEFTSPathOutFile` varchar(100) DEFAULT NULL,
  `iDefaultTermID` int(11) DEFAULT NULL,
  `bUseInsurance` tinyint(1) DEFAULT '0',
  `bForceAuthorisedBy` tinyint(1) DEFAULT '0',
  `bForceClaimNumber` tinyint(1) DEFAULT '0',
  `bForcePolicyNumber` tinyint(1) DEFAULT '0',
  `bForceIncidentDate` tinyint(1) DEFAULT '0',
  `bForceExcessAccName` tinyint(1) DEFAULT '0',
  `bForceExcessAccCont1` tinyint(1) DEFAULT '0',
  `bForceExcessAccCont2` tinyint(1) DEFAULT '0',
  `iExcessCustomerId` int(11) DEFAULT NULL,
  `iExcessInvoiceTaxTypeId` int(11) DEFAULT NULL,
  `iRevProfitAcc` int(11) DEFAULT NULL,
  `iRevLossAcc` int(11) DEFAULT NULL,
  `iProvForRevAcc` int(11) DEFAULT NULL,
  `bApplyDiscountToACs` tinyint(1) NOT NULL DEFAULT '0',
  `bStatementRun` tinyint(1) NOT NULL DEFAULT '0',
  `bStatementsAutoNumbers` tinyint(1) NOT NULL DEFAULT '0',
  `iStatementPadLength` int(11) DEFAULT NULL,
  `bStatementUniqueNumber` tinyint(1) NOT NULL DEFAULT '0',
  `bForceStatementReference` tinyint(1) NOT NULL DEFAULT '0',
  `cStatementPrefix` varchar(20) DEFAULT NULL,
  `bMBIgnoreServiceonAllocs` tinyint(1) NOT NULL DEFAULT '0',
  `CliDef_iBranchID` int(11) DEFAULT NULL,
  `CliDef_dCreatedDate` datetime(6) DEFAULT NULL,
  `CliDef_dModifiedDate` datetime(6) DEFAULT NULL,
  `CliDef_iCreatedBranchID` int(11) DEFAULT NULL,
  `CliDef_iModifiedBranchID` int(11) DEFAULT NULL,
  `CliDef_iCreatedAgentID` int(11) DEFAULT NULL,
  `CliDef_iModifiedAgentID` int(11) DEFAULT NULL,
  `CliDef_iChangeSetID` int(11) DEFAULT NULL,
  `CliDef_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `DCLink` int(11) NOT NULL,
  `Account` varchar(20) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Title` varchar(5) DEFAULT NULL,
  `Init` varchar(6) DEFAULT NULL,
  `Contact_Person` varchar(30) DEFAULT NULL,
  `Physical1` varchar(40) DEFAULT NULL,
  `Physical2` varchar(40) DEFAULT NULL,
  `Physical3` varchar(40) DEFAULT NULL,
  `Physical4` varchar(40) DEFAULT NULL,
  `Physical5` varchar(40) DEFAULT NULL,
  `PhysicalPC` varchar(15) DEFAULT NULL,
  `Addressee` varchar(30) DEFAULT NULL,
  `Post1` varchar(40) DEFAULT NULL,
  `Post2` varchar(40) DEFAULT NULL,
  `Post3` varchar(40) DEFAULT NULL,
  `Post4` varchar(40) DEFAULT NULL,
  `Post5` varchar(40) DEFAULT NULL,
  `PostPC` varchar(15) DEFAULT NULL,
  `Delivered_To` varchar(30) DEFAULT NULL,
  `Telephone` varchar(25) DEFAULT NULL,
  `Telephone2` varchar(25) DEFAULT NULL,
  `Fax1` varchar(25) DEFAULT NULL,
  `Fax2` varchar(25) DEFAULT NULL,
  `AccountTerms` int(11) DEFAULT NULL,
  `CT` tinyint(1) NOT NULL DEFAULT '1',
  `Tax_Number` varchar(50) DEFAULT NULL,
  `Registration` varchar(20) DEFAULT NULL,
  `Credit_Limit` double DEFAULT NULL,
  `RepID` int(11) DEFAULT NULL,
  `Interest_Rate` double DEFAULT NULL,
  `Discount` double DEFAULT NULL,
  `On_Hold` tinyint(1) NOT NULL DEFAULT '0',
  `BFOpenType` int(11) DEFAULT NULL,
  `EMail` varchar(200) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `AutoDisc` double DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `MainAccLink` int(11) DEFAULT NULL,
  `CashDebtor` tinyint(1) NOT NULL DEFAULT '0',
  `DCBalance` double DEFAULT NULL,
  `CheckTerms` tinyint(1) NOT NULL DEFAULT '0',
  `UseEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `iBusTypeID` int(11) DEFAULT NULL,
  `iBusClassID` int(11) DEFAULT NULL,
  `iCountryID` int(11) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `dTimeStamp` datetime(6) DEFAULT NULL,
  `cAccDescription` varchar(80) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `iClassID` int(11) DEFAULT NULL,
  `iAreasID` int(11) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bStatPrint` tinyint(1) NOT NULL DEFAULT '0',
  `bStatEmail` tinyint(1) NOT NULL DEFAULT '0',
  `cStatEmailPass` varchar(160) DEFAULT NULL,
  `bForCurAcc` tinyint(1) NOT NULL DEFAULT '0',
  `fForeignBalance` double DEFAULT NULL,
  `bTaxPrompt` tinyint(1) NOT NULL DEFAULT '1',
  `iARPriceListNameID` int(11) DEFAULT NULL,
  `iSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `bSourceDocPrint` tinyint(1) NOT NULL DEFAULT '1',
  `bSourceDocEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iEUCountryID` int(11) NOT NULL DEFAULT '0',
  `iDefTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `bCODAccount` tinyint(1) NOT NULL DEFAULT '0',
  `iAgeingTermID` int(11) DEFAULT NULL,
  `bElecDocAcceptance` tinyint(1) NOT NULL DEFAULT '0',
  `iBankDetailType` tinyint(3) UNSIGNED DEFAULT NULL,
  `cBankAccHolder` varchar(30) DEFAULT NULL,
  `cIDNumber` varchar(20) DEFAULT NULL,
  `cPassportNumber` varchar(20) DEFAULT NULL,
  `bInsuranceCustomer` tinyint(1) DEFAULT '0',
  `cBankCode` varchar(15) DEFAULT NULL,
  `cSwiftCode` varchar(11) DEFAULT NULL,
  `Client_iBranchID` int(11) DEFAULT NULL,
  `Client_dCreatedDate` datetime(6) DEFAULT NULL,
  `Client_dModifiedDate` datetime(6) DEFAULT NULL,
  `Client_iCreatedBranchID` int(11) DEFAULT NULL,
  `Client_iModifiedBranchID` int(11) DEFAULT NULL,
  `Client_iCreatedAgentID` int(11) DEFAULT NULL,
  `Client_iModifiedAgentID` int(11) DEFAULT NULL,
  `Client_iChangeSetID` int(11) DEFAULT NULL,
  `Client_Checksum` binary(20) DEFAULT NULL,
  `iSPQueueID` int(11) DEFAULT NULL,
  `bCustomerZoneEnabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `AutoIdx` int(11) NOT NULL,
  `DebtorLink` int(11) DEFAULT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Person` varchar(30) DEFAULT NULL,
  `Relationship` varchar(25) DEFAULT NULL,
  `TextField` longtext,
  `Dated` datetime DEFAULT NULL,
  `Time` varbinary(8) DEFAULT NULL,
  `TypeOfCont` varchar(25) DEFAULT NULL,
  `Contact_iBranchID` int(11) DEFAULT NULL,
  `Contact_dCreatedDate` datetime(6) DEFAULT NULL,
  `Contact_dModifiedDate` datetime(6) DEFAULT NULL,
  `Contact_iCreatedBranchID` int(11) DEFAULT NULL,
  `Contact_iModifiedBranchID` int(11) DEFAULT NULL,
  `Contact_iCreatedAgentID` int(11) DEFAULT NULL,
  `Contact_iModifiedAgentID` int(11) DEFAULT NULL,
  `Contact_iChangeSetID` int(11) DEFAULT NULL,
  `Contact_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

DROP TABLE IF EXISTS `cost`;
CREATE TABLE `cost` (
  `Autoidx` int(11) NOT NULL,
  `DebtorAccNo` varchar(20) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `Description` varchar(25) DEFAULT NULL,
  `Dated` datetime DEFAULT NULL,
  `TypeofCost` varchar(25) DEFAULT NULL,
  `Amount` double DEFAULT NULL,
  `Cost_iBranchID` int(11) DEFAULT NULL,
  `Cost_dCreatedDate` datetime(6) DEFAULT NULL,
  `Cost_dModifiedDate` datetime(6) DEFAULT NULL,
  `Cost_iCreatedBranchID` int(11) DEFAULT NULL,
  `Cost_iModifiedBranchID` int(11) DEFAULT NULL,
  `Cost_iCreatedAgentID` int(11) DEFAULT NULL,
  `Cost_iModifiedAgentID` int(11) DEFAULT NULL,
  `Cost_iChangeSetID` int(11) DEFAULT NULL,
  `Cost_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `costcntr`
--

DROP TABLE IF EXISTS `costcntr`;
CREATE TABLE `costcntr` (
  `Counter` int(11) NOT NULL,
  `CostCode` varchar(20) DEFAULT NULL,
  `CostName` varchar(40) DEFAULT NULL,
  `ActiveCenter` varchar(1) DEFAULT NULL,
  `iDepartmentID` int(11) DEFAULT NULL,
  `iGLAccountID` int(11) DEFAULT NULL,
  `CostCntr_iBranchID` int(11) DEFAULT NULL,
  `CostCntr_dCreatedDate` datetime(6) DEFAULT NULL,
  `CostCntr_dModifiedDate` datetime(6) DEFAULT NULL,
  `CostCntr_iCreatedBranchID` int(11) DEFAULT NULL,
  `CostCntr_iModifiedBranchID` int(11) DEFAULT NULL,
  `CostCntr_iCreatedAgentID` int(11) DEFAULT NULL,
  `CostCntr_iModifiedAgentID` int(11) DEFAULT NULL,
  `CostCntr_iChangeSetID` int(11) DEFAULT NULL,
  `CostCntr_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crdischd`
--

DROP TABLE IF EXISTS `crdischd`;
CREATE TABLE `crdischd` (
  `idCrDiscHd` int(11) NOT NULL,
  `Place` varchar(1) NOT NULL,
  `Position` int(11) NOT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `CrDiscHd_iBranchID` int(11) DEFAULT NULL,
  `CrDiscHd_dCreatedDate` datetime(6) DEFAULT NULL,
  `CrDiscHd_dModifiedDate` datetime(6) DEFAULT NULL,
  `CrDiscHd_iCreatedBranchID` int(11) DEFAULT NULL,
  `CrDiscHd_iModifiedBranchID` int(11) DEFAULT NULL,
  `CrDiscHd_iCreatedAgentID` int(11) DEFAULT NULL,
  `CrDiscHd_iModifiedAgentID` int(11) DEFAULT NULL,
  `CrDiscHd_iChangeSetID` int(11) DEFAULT NULL,
  `CrDiscHd_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crdiscmx`
--

DROP TABLE IF EXISTS `crdiscmx`;
CREATE TABLE `crdiscmx` (
  `idCrDiscMx` int(11) NOT NULL,
  `XPos` int(11) NOT NULL,
  `YPos` int(11) NOT NULL,
  `Percentage` double DEFAULT NULL,
  `CrDiscMx_iBranchID` int(11) DEFAULT NULL,
  `CrDiscMx_dCreatedDate` datetime(6) DEFAULT NULL,
  `CrDiscMx_dModifiedDate` datetime(6) DEFAULT NULL,
  `CrDiscMx_iCreatedBranchID` int(11) DEFAULT NULL,
  `CrDiscMx_iModifiedBranchID` int(11) DEFAULT NULL,
  `CrDiscMx_iCreatedAgentID` int(11) DEFAULT NULL,
  `CrDiscMx_iModifiedAgentID` int(11) DEFAULT NULL,
  `CrDiscMx_iChangeSetID` int(11) DEFAULT NULL,
  `CrDiscMx_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credmnt`
--

DROP TABLE IF EXISTS `credmnt`;
CREATE TABLE `credmnt` (
  `Autoidx` int(11) NOT NULL,
  `Category` varchar(1) DEFAULT NULL,
  `Description` varchar(25) DEFAULT NULL,
  `CredMnt_iBranchID` int(11) DEFAULT NULL,
  `CredMnt_dCreatedDate` datetime(6) DEFAULT NULL,
  `CredMnt_dModifiedDate` datetime(6) DEFAULT NULL,
  `CredMnt_iCreatedBranchID` int(11) DEFAULT NULL,
  `CredMnt_iModifiedBranchID` int(11) DEFAULT NULL,
  `CredMnt_iCreatedAgentID` int(11) DEFAULT NULL,
  `CredMnt_iModifiedAgentID` int(11) DEFAULT NULL,
  `CredMnt_iChangeSetID` int(11) DEFAULT NULL,
  `CredMnt_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `CurrencyLink` int(11) NOT NULL,
  `CurrencyCode` varchar(4) DEFAULT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `cCurrencySymbol` varchar(4) DEFAULT NULL,
  `iOptions` int(11) DEFAULT NULL,
  `iPromptAgentID` int(11) DEFAULT NULL,
  `Currency_iBranchID` int(11) DEFAULT NULL,
  `Currency_dCreatedDate` datetime(6) DEFAULT NULL,
  `Currency_dModifiedDate` datetime(6) DEFAULT NULL,
  `Currency_iCreatedBranchID` int(11) DEFAULT NULL,
  `Currency_iModifiedBranchID` int(11) DEFAULT NULL,
  `Currency_iCreatedAgentID` int(11) DEFAULT NULL,
  `Currency_iModifiedAgentID` int(11) DEFAULT NULL,
  `Currency_iChangeSetID` int(11) DEFAULT NULL,
  `Currency_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currencyhist`
--

DROP TABLE IF EXISTS `currencyhist`;
CREATE TABLE `currencyhist` (
  `idCurrencyHist` int(11) NOT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `dRateDate` datetime(6) DEFAULT NULL,
  `fBuyRate` double DEFAULT NULL,
  `fSellRate` double DEFAULT NULL,
  `CurrencyHist_iBranchID` int(11) DEFAULT NULL,
  `CurrencyHist_dCreatedDate` datetime(6) DEFAULT NULL,
  `CurrencyHist_dModifiedDate` datetime(6) DEFAULT NULL,
  `CurrencyHist_iCreatedBranchID` int(11) DEFAULT NULL,
  `CurrencyHist_iModifiedBranchID` int(11) DEFAULT NULL,
  `CurrencyHist_iCreatedAgentID` int(11) DEFAULT NULL,
  `CurrencyHist_iModifiedAgentID` int(11) DEFAULT NULL,
  `CurrencyHist_iChangeSetID` int(11) DEFAULT NULL,
  `CurrencyHist_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cwratio`
--

DROP TABLE IF EXISTS `cwratio`;
CREATE TABLE `cwratio` (
  `idCwRatio` int(11) NOT NULL,
  `CWNextMstAccount` int(11) DEFAULT NULL,
  `iCWRatioAccountType` int(11) NOT NULL DEFAULT '0',
  `CWRatio_iBranchID` int(11) DEFAULT NULL,
  `CWRatio_dCreatedDate` datetime(6) DEFAULT NULL,
  `CWRatio_dModifiedDate` datetime(6) DEFAULT NULL,
  `CWRatio_iCreatedBranchID` int(11) DEFAULT NULL,
  `CWRatio_iModifiedBranchID` int(11) DEFAULT NULL,
  `CWRatio_iCreatedAgentID` int(11) DEFAULT NULL,
  `CWRatio_iModifiedAgentID` int(11) DEFAULT NULL,
  `CWRatio_iChangeSetID` int(11) DEFAULT NULL,
  `CWRatio_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deltbl`
--

DROP TABLE IF EXISTS `deltbl`;
CREATE TABLE `deltbl` (
  `Counter` int(11) NOT NULL,
  `Method` varchar(35) NOT NULL,
  `Comment` varchar(80) DEFAULT NULL,
  `bForDelivery` tinyint(1) NOT NULL DEFAULT '1',
  `dEffectiveDate` datetime(6) DEFAULT NULL,
  `DelTbl_iBranchID` int(11) DEFAULT NULL,
  `DelTbl_dCreatedDate` datetime(6) DEFAULT NULL,
  `DelTbl_dModifiedDate` datetime(6) DEFAULT NULL,
  `DelTbl_iCreatedBranchID` int(11) DEFAULT NULL,
  `DelTbl_iModifiedBranchID` int(11) DEFAULT NULL,
  `DelTbl_iCreatedAgentID` int(11) DEFAULT NULL,
  `DelTbl_iModifiedAgentID` int(11) DEFAULT NULL,
  `DelTbl_iChangeSetID` int(11) DEFAULT NULL,
  `DelTbl_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dept`
--

DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `idDept` int(11) NOT NULL,
  `Name` varchar(10) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `Info` varchar(30) DEFAULT NULL,
  `dBrDeptTimeStamp` datetime(6) DEFAULT NULL,
  `Dept_iBranchID` int(11) DEFAULT NULL,
  `Dept_dCreatedDate` datetime(6) DEFAULT NULL,
  `Dept_dModifiedDate` datetime(6) DEFAULT NULL,
  `Dept_iCreatedBranchID` int(11) DEFAULT NULL,
  `Dept_iModifiedBranchID` int(11) DEFAULT NULL,
  `Dept_iCreatedAgentID` int(11) DEFAULT NULL,
  `Dept_iModifiedAgentID` int(11) DEFAULT NULL,
  `Dept_iChangeSetID` int(11) DEFAULT NULL,
  `Dept_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drdischd`
--

DROP TABLE IF EXISTS `drdischd`;
CREATE TABLE `drdischd` (
  `idDrDiscHd` int(11) NOT NULL,
  `Place` varchar(1) NOT NULL,
  `Position` int(11) NOT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `DrDiscHd_iBranchID` int(11) DEFAULT NULL,
  `DrDiscHd_dCreatedDate` datetime(6) DEFAULT NULL,
  `DrDiscHd_dModifiedDate` datetime(6) DEFAULT NULL,
  `DrDiscHd_iCreatedBranchID` int(11) DEFAULT NULL,
  `DrDiscHd_iModifiedBranchID` int(11) DEFAULT NULL,
  `DrDiscHd_iCreatedAgentID` int(11) DEFAULT NULL,
  `DrDiscHd_iModifiedAgentID` int(11) DEFAULT NULL,
  `DrDiscHd_iChangeSetID` int(11) DEFAULT NULL,
  `DrDiscHd_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drdiscmx`
--

DROP TABLE IF EXISTS `drdiscmx`;
CREATE TABLE `drdiscmx` (
  `idDrDiscMx` int(11) NOT NULL,
  `XPos` int(11) NOT NULL,
  `YPos` int(11) NOT NULL,
  `Percentage` double DEFAULT NULL,
  `DrDiscMx_iBranchID` int(11) DEFAULT NULL,
  `DrDiscMx_dCreatedDate` datetime(6) DEFAULT NULL,
  `DrDiscMx_dModifiedDate` datetime(6) DEFAULT NULL,
  `DrDiscMx_iCreatedBranchID` int(11) DEFAULT NULL,
  `DrDiscMx_iModifiedBranchID` int(11) DEFAULT NULL,
  `DrDiscMx_iCreatedAgentID` int(11) DEFAULT NULL,
  `DrDiscMx_iModifiedAgentID` int(11) DEFAULT NULL,
  `DrDiscMx_iChangeSetID` int(11) DEFAULT NULL,
  `DrDiscMx_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
CREATE TABLE `entities` (
  `idEntities` int(11) NOT NULL,
  `Name` varchar(120) DEFAULT NULL,
  `PhAddress1` varchar(50) DEFAULT NULL,
  `PhAddress2` varchar(50) DEFAULT NULL,
  `PhAddress3` varchar(50) DEFAULT NULL,
  `POAddress1` varchar(50) DEFAULT NULL,
  `POAddress2` varchar(50) DEFAULT NULL,
  `POAddress3` varchar(50) DEFAULT NULL,
  `Telephone1` varchar(15) DEFAULT NULL,
  `Telephone2` varchar(15) DEFAULT NULL,
  `Fax` varchar(15) DEFAULT NULL,
  `Version_Dont_Change` double DEFAULT NULL,
  `StrSpare1` varchar(60) DEFAULT NULL,
  `ShrStrSpare1` varchar(4) DEFAULT NULL,
  `iNextAuditNumber` int(11) DEFAULT NULL,
  `BankName` varchar(40) DEFAULT NULL,
  `BankAccount` varchar(40) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `EFTSCode` varchar(4) DEFAULT NULL,
  `EFTSName` varchar(40) DEFAULT NULL,
  `EFTSReference` varchar(15) DEFAULT NULL,
  `TranLogActive` varchar(1) DEFAULT NULL,
  `RegisterName` varchar(50) DEFAULT NULL,
  `TradingName` varchar(50) DEFAULT NULL,
  `Town` varchar(40) DEFAULT NULL,
  `CompanyInfo` longtext,
  `IncorpForm` varchar(25) DEFAULT NULL,
  `TypeofBus` varchar(30) DEFAULT NULL,
  `ShowInactiveGL` tinyint(1) NOT NULL DEFAULT '1',
  `ShowInactiveProjects` tinyint(1) NOT NULL DEFAULT '1',
  `PhPostalCode` varchar(15) DEFAULT NULL,
  `POPostalCode` varchar(15) DEFAULT NULL,
  `PhState` int(11) DEFAULT NULL,
  `POState` int(11) DEFAULT NULL,
  `PhCountry` int(11) DEFAULT NULL,
  `POCountry` int(11) DEFAULT NULL,
  `ContactPerson` varchar(50) DEFAULT NULL,
  `cWebSiteAddress` varchar(100) DEFAULT NULL,
  `cGLSegmentSeparator` varchar(10) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `cAccountName` varchar(50) DEFAULT NULL,
  `cBranchName` varchar(30) DEFAULT NULL,
  `fAccountLimit` double DEFAULT NULL,
  `iEUCountryID` int(11) NOT NULL DEFAULT '0',
  `bAPDocBudgetCheck` tinyint(1) NOT NULL DEFAULT '0',
  `bAPDocBudgetAnnual` tinyint(1) NOT NULL DEFAULT '0',
  `cHomeCurrency` varchar(4) DEFAULT NULL,
  `bSingleEntityReporting` tinyint(1) NOT NULL DEFAULT '1',
  `iFilterStartLength` int(11) NOT NULL DEFAULT '0',
  `bUseIntegreatedEFT` tinyint(1) DEFAULT NULL,
  `cMerchantType` varchar(4) DEFAULT NULL,
  `cMerchantID` varchar(20) DEFAULT NULL,
  `cIPAddress` varchar(20) DEFAULT NULL,
  `iPort` int(11) DEFAULT NULL,
  `dCommencementDate` datetime(6) DEFAULT NULL,
  `GatewayID` int(11) DEFAULT NULL,
  `bUseBins` tinyint(1) NOT NULL DEFAULT '0',
  `cBinIPAddress` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iBinPort` int(11) NOT NULL DEFAULT '12000',
  `cBankCode` varchar(15) DEFAULT NULL,
  `bAllowDCProcessing` tinyint(1) NOT NULL DEFAULT '0',
  `iDCBranchLoanAccountID` int(11) DEFAULT NULL,
  `Entities_iBranchID` int(11) DEFAULT NULL,
  `Entities_dCreatedDate` datetime(6) DEFAULT NULL,
  `Entities_dModifiedDate` datetime(6) DEFAULT NULL,
  `Entities_iCreatedBranchID` int(11) DEFAULT NULL,
  `Entities_iModifiedBranchID` int(11) DEFAULT NULL,
  `Entities_iCreatedAgentID` int(11) DEFAULT NULL,
  `Entities_iModifiedAgentID` int(11) DEFAULT NULL,
  `Entities_iChangeSetID` int(11) DEFAULT NULL,
  `Entities_Checksum` binary(20) DEFAULT NULL,
  `bUsemSCOA` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `glbranch`
--

DROP TABLE IF EXISTS `glbranch`;
CREATE TABLE `glbranch` (
  `idGLBranch` int(11) NOT NULL,
  `Name` varchar(10) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `Info` varchar(30) DEFAULT NULL,
  `dBrDeptTimeStamp` datetime(6) DEFAULT NULL,
  `GLBranch_iBranchID` int(11) DEFAULT NULL,
  `GLBranch_dCreatedDate` datetime(6) DEFAULT NULL,
  `GLBranch_dModifiedDate` datetime(6) DEFAULT NULL,
  `GLBranch_iCreatedBranchID` int(11) DEFAULT NULL,
  `GLBranch_iModifiedBranchID` int(11) DEFAULT NULL,
  `GLBranch_iCreatedAgentID` int(11) DEFAULT NULL,
  `GLBranch_iModifiedAgentID` int(11) DEFAULT NULL,
  `GLBranch_iChangeSetID` int(11) DEFAULT NULL,
  `GLBranch_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grptbl`
--

DROP TABLE IF EXISTS `grptbl`;
CREATE TABLE `grptbl` (
  `idGrpTbl` int(11) NOT NULL,
  `StGroup` varchar(20) NOT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `SalesAccLink` int(11) DEFAULT NULL,
  `COSAccLink` int(11) DEFAULT NULL,
  `StockAccLink` int(11) DEFAULT NULL,
  `PurchasesAccLink` int(11) DEFAULT NULL,
  `SMtrxCol` int(11) DEFAULT NULL,
  `PMtrxCol` int(11) DEFAULT NULL,
  `dGrpTblTimeStamp` datetime(6) DEFAULT NULL,
  `bPromptSales` tinyint(1) NOT NULL DEFAULT '0',
  `bPromptCOS` tinyint(1) NOT NULL DEFAULT '0',
  `bPromptStock` tinyint(1) NOT NULL DEFAULT '0',
  `bPromptPurchases` tinyint(1) NOT NULL DEFAULT '0',
  `CostVarianceAccLink` int(11) DEFAULT NULL,
  `bPromptCostVariance` tinyint(1) NOT NULL DEFAULT '0',
  `StockAdjustAccLink` int(11) DEFAULT NULL,
  `bPromptStockAdjust` tinyint(1) NOT NULL DEFAULT '0',
  `iStockCostVarianceAccID` int(11) NOT NULL DEFAULT '0',
  `bPromptStockCostVariance` tinyint(1) NOT NULL DEFAULT '0',
  `iWIPAccID` int(11) NOT NULL DEFAULT '0',
  `bPromptWIP` tinyint(1) NOT NULL DEFAULT '0',
  `fGroupGPPercent` float(24,0) DEFAULT NULL,
  `GrpTbl_iBranchID` int(11) DEFAULT NULL,
  `GrpTbl_dCreatedDate` datetime(6) DEFAULT NULL,
  `GrpTbl_dModifiedDate` datetime(6) DEFAULT NULL,
  `GrpTbl_iCreatedBranchID` int(11) DEFAULT NULL,
  `GrpTbl_iModifiedBranchID` int(11) DEFAULT NULL,
  `GrpTbl_iCreatedAgentID` int(11) DEFAULT NULL,
  `GrpTbl_iModifiedAgentID` int(11) DEFAULT NULL,
  `GrpTbl_iChangeSetID` int(11) DEFAULT NULL,
  `GrpTbl_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invnum`
--

DROP TABLE IF EXISTS `invnum`;
CREATE TABLE `invnum` (
  `AutoIndex` int(11) NOT NULL,
  `DocType` int(11) DEFAULT NULL,
  `DocVersion` int(11) DEFAULT NULL,
  `DocState` int(11) DEFAULT NULL,
  `DocFlag` int(11) DEFAULT NULL,
  `OrigDocID` int(11) DEFAULT NULL,
  `InvNumber` varchar(50) DEFAULT NULL,
  `GrvNumber` varchar(50) DEFAULT NULL,
  `GrvID` int(11) DEFAULT NULL,
  `AccountID` int(11) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `InvDate` datetime DEFAULT NULL,
  `OrderDate` datetime DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `DeliveryDate` datetime DEFAULT NULL,
  `TaxInclusive` tinyint(1) DEFAULT NULL,
  `Email_Sent` int(11) DEFAULT NULL,
  `Address1` varchar(40) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(40) DEFAULT NULL,
  `Address5` varchar(40) DEFAULT NULL,
  `Address6` varchar(40) DEFAULT NULL,
  `PAddress1` varchar(40) DEFAULT NULL,
  `PAddress2` varchar(40) DEFAULT NULL,
  `PAddress3` varchar(40) DEFAULT NULL,
  `PAddress4` varchar(40) DEFAULT NULL,
  `PAddress5` varchar(40) DEFAULT NULL,
  `PAddress6` varchar(40) DEFAULT NULL,
  `DelMethodID` int(11) DEFAULT NULL,
  `DocRepID` int(11) DEFAULT NULL,
  `OrderNum` varchar(50) DEFAULT NULL,
  `DeliveryNote` varchar(50) DEFAULT NULL,
  `InvDisc` double DEFAULT NULL,
  `InvDiscReasonID` int(11) DEFAULT NULL,
  `Message1` varchar(255) DEFAULT NULL,
  `Message2` varchar(255) DEFAULT NULL,
  `Message3` varchar(255) DEFAULT NULL,
  `ProjectID` int(11) DEFAULT NULL,
  `TillID` int(11) DEFAULT NULL,
  `POSAmntTendered` double DEFAULT NULL,
  `POSChange` double DEFAULT NULL,
  `GrvSplitFixedCost` tinyint(1) NOT NULL DEFAULT '0',
  `GrvSplitFixedAmnt` double DEFAULT NULL,
  `OrderStatusID` int(11) DEFAULT NULL,
  `OrderPriorityID` int(11) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `ForeignCurrencyID` int(11) DEFAULT NULL,
  `InvDiscAmnt` double DEFAULT NULL,
  `InvDiscAmntEx` double DEFAULT NULL,
  `InvTotExclDEx` double DEFAULT NULL,
  `InvTotTaxDEx` double DEFAULT NULL,
  `InvTotInclDEx` double DEFAULT NULL,
  `InvTotExcl` double DEFAULT NULL,
  `InvTotTax` double DEFAULT NULL,
  `InvTotIncl` double DEFAULT NULL,
  `OrdDiscAmnt` double DEFAULT NULL,
  `OrdDiscAmntEx` double DEFAULT NULL,
  `OrdTotExclDEx` double DEFAULT NULL,
  `OrdTotTaxDEx` double DEFAULT NULL,
  `OrdTotInclDEx` double DEFAULT NULL,
  `OrdTotExcl` double DEFAULT NULL,
  `OrdTotTax` double DEFAULT NULL,
  `OrdTotIncl` double DEFAULT NULL,
  `bUseFixedPrices` tinyint(1) NOT NULL DEFAULT '0',
  `iDocPrinted` int(11) DEFAULT NULL,
  `iINVNUMAgentID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fGrvSplitFixedAmntForeign` double DEFAULT NULL,
  `fInvDiscAmntForeign` double DEFAULT NULL,
  `fInvDiscAmntExForeign` double DEFAULT NULL,
  `fInvTotExclDExForeign` double DEFAULT NULL,
  `fInvTotTaxDExForeign` double DEFAULT NULL,
  `fInvTotInclDExForeign` double DEFAULT NULL,
  `fInvTotExclForeign` double DEFAULT NULL,
  `fInvTotTaxForeign` double DEFAULT NULL,
  `fInvTotInclForeign` double DEFAULT NULL,
  `fOrdDiscAmntForeign` double DEFAULT NULL,
  `fOrdDiscAmntExForeign` double DEFAULT NULL,
  `fOrdTotExclDExForeign` double DEFAULT NULL,
  `fOrdTotTaxDExForeign` double DEFAULT NULL,
  `fOrdTotInclDExForeign` double DEFAULT NULL,
  `fOrdTotExclForeign` double DEFAULT NULL,
  `fOrdTotTaxForeign` double DEFAULT NULL,
  `fOrdTotInclForeign` double DEFAULT NULL,
  `cTaxNumber` varchar(50) DEFAULT NULL,
  `cAccountName` varchar(150) DEFAULT NULL,
  `iProspectID` int(11) NOT NULL DEFAULT '0',
  `iOpportunityID` int(11) NOT NULL DEFAULT '0',
  `InvTotRounding` double NOT NULL DEFAULT '0',
  `OrdTotRounding` double NOT NULL DEFAULT '0',
  `fInvTotForeignRounding` double NOT NULL DEFAULT '0',
  `fOrdTotForeignRounding` double NOT NULL DEFAULT '0',
  `bInvRounding` tinyint(1) NOT NULL DEFAULT '0',
  `iInvSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `cSettlementTermInvMsg` varchar(255) DEFAULT NULL,
  `iOrderCancelReasonID` int(11) NOT NULL DEFAULT '0',
  `iLinkedDocID` int(11) NOT NULL DEFAULT '0',
  `bLinkedTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `InvTotInclExRounding` double NOT NULL DEFAULT '0',
  `OrdTotInclExRounding` double NOT NULL DEFAULT '0',
  `fInvTotInclForeignExRounding` double NOT NULL DEFAULT '0',
  `fOrdTotInclForeignExRounding` double NOT NULL DEFAULT '0',
  `iEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iPOAuthStatus` int(11) NOT NULL DEFAULT '0',
  `iPOIncidentID` int(11) NOT NULL DEFAULT '0',
  `iSupervisorID` int(11) DEFAULT NULL,
  `iMergedDocID` int(11) DEFAULT '0',
  `iDocEmailed` int(11) NOT NULL DEFAULT '0',
  `fDepositAmountForeign` double DEFAULT NULL,
  `fRefundAmount` double DEFAULT NULL,
  `bTaxPerLine` tinyint(1) NOT NULL DEFAULT '1',
  `fDepositAmountTotal` double DEFAULT NULL,
  `fDepositAmountUnallocated` double DEFAULT NULL,
  `fDepositAmountNew` double DEFAULT NULL,
  `fDepositAmountTotalForeign` double DEFAULT NULL,
  `fDepositAmountUnallocatedForeign` double DEFAULT NULL,
  `fRefundAmountForeign` double DEFAULT NULL,
  `KeepAsideCollectionDate` datetime DEFAULT NULL,
  `KeepAsideExpiryDate` datetime DEFAULT NULL,
  `cContact` varchar(50) DEFAULT NULL,
  `cTelephone` varchar(25) DEFAULT NULL,
  `cFax` varchar(25) DEFAULT NULL,
  `cEmail` varchar(60) DEFAULT NULL,
  `cCellular` varchar(25) DEFAULT NULL,
  `imgOrderSignature` longblob,
  `iInsuranceState` int(11) NOT NULL DEFAULT '0',
  `cAuthorisedBy` varchar(50) DEFAULT NULL,
  `cClaimNumber` varchar(40) DEFAULT NULL,
  `cPolicyNumber` varchar(40) DEFAULT NULL,
  `dIncidentDate` datetime(6) DEFAULT NULL,
  `cExcessAccName` varchar(150) DEFAULT NULL,
  `cExcessAccCont1` varchar(50) DEFAULT NULL,
  `cExcessAccCont2` varchar(50) DEFAULT NULL,
  `fExcessAmt` double DEFAULT NULL,
  `fExcessPct` double DEFAULT NULL,
  `fExcessExclusive` double NOT NULL DEFAULT '0',
  `fExcessInclusive` double NOT NULL DEFAULT '0',
  `fExcessTax` double NOT NULL DEFAULT '0',
  `fAddChargeExclusive` double NOT NULL DEFAULT '0',
  `fAddChargeTax` double NOT NULL DEFAULT '0',
  `fAddChargeInclusive` double NOT NULL DEFAULT '0',
  `fAddChargeExclusiveForeign` double NOT NULL DEFAULT '0',
  `fAddChargeTaxForeign` double NOT NULL DEFAULT '0',
  `fAddChargeInclusiveForeign` double NOT NULL DEFAULT '0',
  `fOrdAddChargeExclusive` double NOT NULL DEFAULT '0',
  `fOrdAddChargeTax` double NOT NULL DEFAULT '0',
  `fOrdAddChargeInclusive` double NOT NULL DEFAULT '0',
  `fOrdAddChargeExclusiveForeign` double NOT NULL DEFAULT '0',
  `fOrdAddChargeTaxForeign` double NOT NULL DEFAULT '0',
  `fOrdAddChargeInclusiveForeign` double NOT NULL DEFAULT '0',
  `iInvoiceSplitDocID` bigint(20) NOT NULL DEFAULT '0',
  `cGIVNumber` varchar(50) DEFAULT NULL,
  `bIsDCOrder` tinyint(1) NOT NULL DEFAULT '0',
  `iDCBranchID` int(11) DEFAULT NULL,
  `iSalesBranchID` int(11) DEFAULT NULL,
  `InvNum_iBranchID` int(11) DEFAULT NULL,
  `InvNum_dCreatedDate` datetime(6) DEFAULT NULL,
  `InvNum_dModifiedDate` datetime(6) DEFAULT NULL,
  `InvNum_iCreatedBranchID` int(11) DEFAULT NULL,
  `InvNum_iModifiedBranchID` int(11) DEFAULT NULL,
  `InvNum_iCreatedAgentID` int(11) DEFAULT NULL,
  `InvNum_iModifiedAgentID` int(11) DEFAULT NULL,
  `InvNum_iChangeSetID` int(11) DEFAULT NULL,
  `InvNum_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobdef`
--

DROP TABLE IF EXISTS `jobdef`;
CREATE TABLE `jobdef` (
  `AutoNumber` tinyint(1) NOT NULL DEFAULT '0',
  `CurrentNumber` varchar(15) DEFAULT NULL,
  `WIPM1Link` int(11) DEFAULT NULL,
  `RecoveryM1Link` int(11) DEFAULT NULL,
  `TaxM1Link` int(11) DEFAULT NULL,
  `StockM1Link` int(11) DEFAULT NULL,
  `CreditorM1Link` int(11) DEFAULT NULL,
  `SalesM1Link` int(11) DEFAULT NULL,
  `COSM1Link` int(11) DEFAULT NULL,
  `DebtorM1Link` int(11) DEFAULT NULL,
  `WIPM2Link` int(11) DEFAULT NULL,
  `RecoveryM2Link` int(11) DEFAULT NULL,
  `TaxM2Link` int(11) DEFAULT NULL,
  `StockM2Link` int(11) DEFAULT NULL,
  `CreditorM2Link` int(11) DEFAULT NULL,
  `SalesM2Link` int(11) DEFAULT NULL,
  `COSM2Link` int(11) DEFAULT NULL,
  `DebtorM2Link` int(11) DEFAULT NULL,
  `POSTINGM` int(11) DEFAULT NULL,
  `AutoTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `NextTemplate` varchar(15) DEFAULT NULL,
  `iCCDebitAccount` int(11) DEFAULT NULL,
  `iCCCreditAccount` int(11) DEFAULT NULL,
  `iJobPadTo` int(11) DEFAULT NULL,
  `cJobPrefix` varchar(20) DEFAULT NULL,
  `iTemplatePadTo` int(11) DEFAULT NULL,
  `cTemplatePrefix` varchar(20) DEFAULT NULL,
  `iJobTxTpIDGRV` int(11) DEFAULT NULL,
  `iJobTxTpIDGLOC` int(11) DEFAULT NULL,
  `bAutoJCQuote` tinyint(1) DEFAULT NULL,
  `cNextJCQuote` varchar(15) DEFAULT NULL,
  `iJCQuotePadTo` int(11) DEFAULT NULL,
  `cJCQuotePrefix` varchar(20) DEFAULT NULL,
  `bAutoJCDelNote` tinyint(1) DEFAULT NULL,
  `cNextJCDelNote` varchar(15) DEFAULT NULL,
  `iJCDelNotePadTo` int(11) DEFAULT NULL,
  `cJCDelNotePrefix` varchar(20) DEFAULT NULL,
  `bPrintAllLines` tinyint(1) NOT NULL DEFAULT '1',
  `iJobTxTpIDGrvGL` int(11) DEFAULT NULL,
  `bPostGRVCost` tinyint(1) NOT NULL DEFAULT '0',
  `bAPAllowSettlementTerms` tinyint(1) NOT NULL DEFAULT '0',
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `bForceRep` tinyint(1) NOT NULL DEFAULT '0',
  `bForceExtOrderNum` tinyint(1) NOT NULL DEFAULT '0',
  `bUniqueJobNum` tinyint(1) DEFAULT NULL,
  `bUniqueQuoteNum` tinyint(1) DEFAULT NULL,
  `bUniqueTemplateNum` tinyint(1) DEFAULT NULL,
  `bUniqueDeliveryNoteNum` tinyint(1) DEFAULT NULL,
  `idJobDef` int(11) NOT NULL,
  `bStrictWIP` tinyint(1) NOT NULL DEFAULT '1',
  `iFilterStartLength` int(11) DEFAULT NULL,
  `JobDef_iBranchID` int(11) DEFAULT NULL,
  `JobDef_dCreatedDate` datetime(6) DEFAULT NULL,
  `JobDef_dModifiedDate` datetime(6) DEFAULT NULL,
  `JobDef_iCreatedBranchID` int(11) DEFAULT NULL,
  `JobDef_iModifiedBranchID` int(11) DEFAULT NULL,
  `JobDef_iCreatedAgentID` int(11) DEFAULT NULL,
  `JobDef_iModifiedAgentID` int(11) DEFAULT NULL,
  `JobDef_iChangeSetID` int(11) DEFAULT NULL,
  `JobDef_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobnum`
--

DROP TABLE IF EXISTS `jobnum`;
CREATE TABLE `jobnum` (
  `AutoIndex` int(11) NOT NULL,
  `InvNumber` varchar(50) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `InvDate` datetime DEFAULT NULL,
  `Email_Sent` int(11) DEFAULT NULL,
  `AccountID` int(11) DEFAULT NULL,
  `iJCMasterID` int(11) DEFAULT NULL,
  `TaxInclusive` tinyint(1) DEFAULT NULL,
  `Address1` varchar(40) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(40) DEFAULT NULL,
  `Address5` varchar(40) DEFAULT NULL,
  `Address6` varchar(40) DEFAULT NULL,
  `PAddress1` varchar(40) DEFAULT NULL,
  `PAddress2` varchar(40) DEFAULT NULL,
  `PAddress3` varchar(40) DEFAULT NULL,
  `PAddress4` varchar(40) DEFAULT NULL,
  `PAddress5` varchar(40) DEFAULT NULL,
  `PAddress6` varchar(40) DEFAULT NULL,
  `iDelMethodID` int(11) DEFAULT NULL,
  `iRepID` int(11) DEFAULT NULL,
  `OrderNumber` varchar(50) DEFAULT NULL,
  `DeliveryNote` varchar(50) DEFAULT NULL,
  `InvDisc` double DEFAULT NULL,
  `Message1` varchar(255) DEFAULT NULL,
  `Message2` varchar(255) DEFAULT NULL,
  `Message3` varchar(255) DEFAULT NULL,
  `Narration` longtext,
  `iProjectID` int(11) DEFAULT NULL,
  `InvDiscAmnt` double DEFAULT NULL,
  `InvTotExclDEx` double DEFAULT NULL,
  `InvTotTaxDEx` double DEFAULT NULL,
  `InvTotInclDEx` double DEFAULT NULL,
  `InvTotExcl` double DEFAULT NULL,
  `InvTotTax` double DEFAULT NULL,
  `InvTotIncl` double DEFAULT NULL,
  `DocType` int(11) DEFAULT NULL,
  `bSummaryInv` tinyint(1) NOT NULL DEFAULT '0',
  `cExtOrderNumber` varchar(50) DEFAULT NULL,
  `iDocPrinted` int(11) DEFAULT NULL,
  `InvDiscAmntForeign` double DEFAULT NULL,
  `InvTotExclDExForeign` double DEFAULT NULL,
  `InvTotTaxDExForeign` double DEFAULT NULL,
  `InvTotInclDExForeign` double DEFAULT NULL,
  `InvTotExclForeign` double DEFAULT NULL,
  `InvTotTaxForeign` double DEFAULT NULL,
  `InvTotInclForeign` double DEFAULT NULL,
  `cTaxNumber` varchar(15) DEFAULT NULL,
  `cAccountName` varchar(150) DEFAULT NULL,
  `iInvSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `bTaxPerLine` tinyint(1) NOT NULL DEFAULT '1',
  `JobNum_iBranchID` int(11) DEFAULT NULL,
  `JobNum_dCreatedDate` datetime(6) DEFAULT NULL,
  `JobNum_dModifiedDate` datetime(6) DEFAULT NULL,
  `JobNum_iCreatedBranchID` int(11) DEFAULT NULL,
  `JobNum_iModifiedBranchID` int(11) DEFAULT NULL,
  `JobNum_iCreatedAgentID` int(11) DEFAULT NULL,
  `JobNum_iModifiedAgentID` int(11) DEFAULT NULL,
  `JobNum_iChangeSetID` int(11) DEFAULT NULL,
  `JobNum_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobstock`
--

DROP TABLE IF EXISTS `jobstock`;
CREATE TABLE `jobstock` (
  `JobStock` int(11) NOT NULL,
  `iJobStockGroupID` int(11) DEFAULT NULL,
  `StockItemLink` int(11) DEFAULT NULL,
  `UOM` varchar(10) DEFAULT NULL,
  `QtyIssued` double DEFAULT NULL,
  `QtyIssuedInvoiced` double DEFAULT NULL,
  `QtyIssuedToInvoice` double DEFAULT NULL,
  `QtyIssuedAvailable` double DEFAULT NULL,
  `QtyIssuedAdjusted` double NOT NULL DEFAULT '0',
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `CostPrice` double DEFAULT NULL,
  `Discount` double DEFAULT NULL,
  `SellPriceExcl` double DEFAULT NULL,
  `SellPriceIncl` double DEFAULT NULL,
  `TaxRate` double DEFAULT NULL,
  `LineTotExcl` double DEFAULT NULL,
  `LineTotIncl` double DEFAULT NULL,
  `LineTotTax` double DEFAULT NULL,
  `LineTotExclToInvoice` double DEFAULT NULL,
  `LineTotInclToInvoice` double DEFAULT NULL,
  `LineTotTaxToInvoice` double DEFAULT NULL,
  `LineTotCost` double DEFAULT NULL,
  `WarehouseID` int(11) DEFAULT NULL,
  `iSerialNumberGroupID` int(11) DEFAULT NULL,
  `ExchangeRate` double DEFAULT NULL,
  `SellPriceExclForeign` double DEFAULT NULL,
  `SellPriceInclForeign` double DEFAULT NULL,
  `LineTotExclForeign` double DEFAULT NULL,
  `LineTotInclForeign` double DEFAULT NULL,
  `LineTotTaxForeign` double DEFAULT NULL,
  `LineTotExclForeignToInvoice` double DEFAULT NULL,
  `LineTotInclForeignToInvoice` double DEFAULT NULL,
  `LineTotTaxForeignToInvoice` double DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `JobStock_iBranchID` int(11) DEFAULT NULL,
  `JobStock_dCreatedDate` datetime(6) DEFAULT NULL,
  `JobStock_dModifiedDate` datetime(6) DEFAULT NULL,
  `JobStock_iCreatedBranchID` int(11) DEFAULT NULL,
  `JobStock_iModifiedBranchID` int(11) DEFAULT NULL,
  `JobStock_iCreatedAgentID` int(11) DEFAULT NULL,
  `JobStock_iModifiedAgentID` int(11) DEFAULT NULL,
  `JobStock_iChangeSetID` int(11) DEFAULT NULL,
  `JobStock_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobtxtp`
--

DROP TABLE IF EXISTS `jobtxtp`;
CREATE TABLE `jobtxtp` (
  `idJobTxTp` int(11) NOT NULL,
  `TxType` varchar(20) NOT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `Source` int(11) DEFAULT NULL,
  `OverheadPercent` double DEFAULT NULL,
  `M1WIPLink` int(11) DEFAULT NULL,
  `M1RecoveryLink` int(11) DEFAULT NULL,
  `M1StockLink` int(11) DEFAULT NULL,
  `M1CreditorLink` int(11) DEFAULT NULL,
  `M1TaxLink` int(11) DEFAULT NULL,
  `M1SalesLink` int(11) DEFAULT NULL,
  `M1COSLink` int(11) DEFAULT NULL,
  `M1DebtorLink` int(11) DEFAULT NULL,
  `M2WIPLink` int(11) DEFAULT NULL,
  `M2RecoveryLink` int(11) DEFAULT NULL,
  `M2StockLink` int(11) DEFAULT NULL,
  `M2CreditorLink` int(11) DEFAULT NULL,
  `M2TaxLink` int(11) DEFAULT NULL,
  `M2SalesLink` int(11) DEFAULT NULL,
  `M2COSLink` int(11) DEFAULT NULL,
  `M2DebtorLink` int(11) DEFAULT NULL,
  `iTaxTypeIDInv` int(11) DEFAULT NULL,
  `iTaxTypeIDGrv` int(11) DEFAULT NULL,
  `bSalesFilter` tinyint(1) NOT NULL DEFAULT '1',
  `iSellingTaxGroupID` int(11) DEFAULT NULL,
  `iCostTaxGroupID` int(11) DEFAULT NULL,
  `JobTxTp_iBranchID` int(11) DEFAULT NULL,
  `JobTxTp_dCreatedDate` datetime(6) DEFAULT NULL,
  `JobTxTp_dModifiedDate` datetime(6) DEFAULT NULL,
  `JobTxTp_iCreatedBranchID` int(11) DEFAULT NULL,
  `JobTxTp_iModifiedBranchID` int(11) DEFAULT NULL,
  `JobTxTp_iCreatedAgentID` int(11) DEFAULT NULL,
  `JobTxTp_iModifiedAgentID` int(11) DEFAULT NULL,
  `JobTxTp_iChangeSetID` int(11) DEFAULT NULL,
  `JobTxTp_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mastoffs`
--

DROP TABLE IF EXISTS `mastoffs`;
CREATE TABLE `mastoffs` (
  `idMastOffs` int(11) NOT NULL,
  `Description` varchar(30) NOT NULL,
  `Address` longtext,
  `MastOffs_iBranchID` int(11) DEFAULT NULL,
  `MastOffs_dCreatedDate` datetime(6) DEFAULT NULL,
  `MastOffs_dModifiedDate` datetime(6) DEFAULT NULL,
  `MastOffs_iCreatedBranchID` int(11) DEFAULT NULL,
  `MastOffs_iModifiedBranchID` int(11) DEFAULT NULL,
  `MastOffs_iCreatedAgentID` int(11) DEFAULT NULL,
  `MastOffs_iModifiedAgentID` int(11) DEFAULT NULL,
  `MastOffs_iChangeSetID` int(11) DEFAULT NULL,
  `MastOffs_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nt_suppliers`
--

DROP TABLE IF EXISTS `nt_suppliers`;
CREATE TABLE `nt_suppliers` (
  `NTSupID` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `Name_of_Supplier/Person` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `Registration` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `id_number` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Reason_for_Restriction` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Period_From` datetime(6) DEFAULT NULL,
  `Period_To` datetime(6) DEFAULT NULL,
  `Authorised_by` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Still_Blocked` tinyint(1) NOT NULL,
  `Reason_for_Removal` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Date_of_entry` datetime(6) DEFAULT NULL,
  `Date_of_removal` datetime(6) DEFAULT NULL,
  `NT_Suppliers_iBranchID` int(11) DEFAULT NULL,
  `NT_Suppliers_dCreatedDate` datetime(6) DEFAULT NULL,
  `NT_Suppliers_dModifiedDate` datetime(6) DEFAULT NULL,
  `NT_Suppliers_iCreatedBranchID` int(11) DEFAULT NULL,
  `NT_Suppliers_iModifiedBranchID` int(11) DEFAULT NULL,
  `NT_Suppliers_iCreatedAgentID` int(11) DEFAULT NULL,
  `NT_Suppliers_iModifiedAgentID` int(11) DEFAULT NULL,
  `NT_Suppliers_iChangeSetID` int(11) DEFAULT NULL,
  `NT_Suppliers_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nt_suppliers_audit`
--

DROP TABLE IF EXISTS `nt_suppliers_audit`;
CREATE TABLE `nt_suppliers_audit` (
  `iNTSupAuditID` int(11) NOT NULL,
  `NTSupID` int(11) NOT NULL,
  `PeriodFrom` datetime(6) DEFAULT NULL,
  `PeriodTo` datetime(6) DEFAULT NULL,
  `StillBlocked` tinyint(1) DEFAULT NULL,
  `NT_SupplierAudit_dModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NT_SupplierAudit_iModifiedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `open_sessions`
--

DROP TABLE IF EXISTS `open_sessions`;
CREATE TABLE `open_sessions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `opened_at` datetime(6) NOT NULL,
  `open` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ordersdf`
--

DROP TABLE IF EXISTS `ordersdf`;
CREATE TABLE `ordersdf` (
  `DefaultCounter` int(11) NOT NULL,
  `iModule` int(11) NOT NULL DEFAULT '0',
  `OrderPrefix` varchar(20) DEFAULT NULL,
  `DN_POPrefix` varchar(20) DEFAULT NULL,
  `AutoDNNo` tinyint(1) NOT NULL DEFAULT '0',
  `NextDNNo` int(11) DEFAULT NULL,
  `DNoPadLgth` int(11) DEFAULT NULL,
  `AutoCustNo` tinyint(1) NOT NULL DEFAULT '0',
  `NextCustNo` int(11) DEFAULT NULL,
  `CNoPadLgth` int(11) DEFAULT NULL,
  `ReserveStock` tinyint(1) NOT NULL DEFAULT '0',
  `TrCodeID` int(11) DEFAULT NULL,
  `PrintAllLines` tinyint(1) NOT NULL DEFAULT '0',
  `TemplatePrefix` varchar(20) DEFAULT NULL,
  `NextTemplateNo` int(11) DEFAULT NULL,
  `PadTemplateLngth` int(11) DEFAULT NULL,
  `AutoTemplate` tinyint(1) DEFAULT NULL,
  `AutoQuote` tinyint(1) DEFAULT NULL,
  `QuotePrefix` varchar(20) DEFAULT NULL,
  `NextQuoteNo` int(11) DEFAULT NULL,
  `PadQuoteLngth` int(11) DEFAULT NULL,
  `bInvGrvSplit` tinyint(1) DEFAULT NULL,
  `iGrvTrCodeID` int(11) DEFAULT NULL,
  `iSInvGLVarAccID` int(11) DEFAULT NULL,
  `bUseNewCurrencyRate` tinyint(1) NOT NULL DEFAULT '0',
  `iPOAuthType` int(11) NOT NULL DEFAULT '0',
  `iPOIncidentTypeID` int(11) DEFAULT NULL,
  `bPOExclusive` tinyint(1) NOT NULL DEFAULT '1',
  `fPOLimit` double DEFAULT NULL,
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `bForceRep` tinyint(1) NOT NULL DEFAULT '0',
  `bArchiveQuotes` tinyint(1) NOT NULL DEFAULT '0',
  `bForceExtOrderNum` tinyint(1) NOT NULL DEFAULT '0',
  `bForceSupplierInvNumber` tinyint(1) NOT NULL DEFAULT '0',
  `bUniqueOrderNum` tinyint(1) DEFAULT NULL,
  `bUniqueQuoteNum` tinyint(1) DEFAULT NULL,
  `bUniqueTemplateNum` tinyint(1) DEFAULT NULL,
  `bUniqueDeliveryNoteNum` tinyint(1) DEFAULT NULL,
  `bAllowPOSTender` tinyint(1) NOT NULL DEFAULT '0',
  `bUseDeposits` tinyint(1) NOT NULL DEFAULT '0',
  `bForceDeposits` tinyint(1) NOT NULL DEFAULT '0',
  `iDepTrCodeID` int(11) DEFAULT NULL,
  `fMinDepositPerc` double DEFAULT NULL,
  `bPrintUnprocessedSOPS` tinyint(1) NOT NULL DEFAULT '0',
  `bPrintProcessedSOPS` tinyint(1) NOT NULL DEFAULT '1',
  `bForceDelivery` tinyint(1) NOT NULL DEFAULT '0',
  `bReserveSOLinkedPO` tinyint(1) NOT NULL DEFAULT '0',
  `bSOInvIssueSplit` tinyint(1) NOT NULL DEFAULT '0',
  `iSOInvIssueSplitAccrualAccID` int(11) NOT NULL DEFAULT '0',
  `OrdersDf_iBranchID` int(11) DEFAULT NULL,
  `OrdersDf_dCreatedDate` datetime(6) DEFAULT NULL,
  `OrdersDf_dModifiedDate` datetime(6) DEFAULT NULL,
  `OrdersDf_iCreatedBranchID` int(11) DEFAULT NULL,
  `OrdersDf_iModifiedBranchID` int(11) DEFAULT NULL,
  `OrdersDf_iCreatedAgentID` int(11) DEFAULT NULL,
  `OrdersDf_iModifiedAgentID` int(11) DEFAULT NULL,
  `OrdersDf_iChangeSetID` int(11) DEFAULT NULL,
  `OrdersDf_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ordersst`
--

DROP TABLE IF EXISTS `ordersst`;
CREATE TABLE `ordersst` (
  `StatusCounter` int(11) NOT NULL,
  `StatusDescrip` varchar(35) DEFAULT NULL,
  `OrdersSt_iBranchID` int(11) DEFAULT NULL,
  `OrdersSt_dCreatedDate` datetime(6) DEFAULT NULL,
  `OrdersSt_dModifiedDate` datetime(6) DEFAULT NULL,
  `OrdersSt_iCreatedBranchID` int(11) DEFAULT NULL,
  `OrdersSt_iModifiedBranchID` int(11) DEFAULT NULL,
  `OrdersSt_iCreatedAgentID` int(11) DEFAULT NULL,
  `OrdersSt_iModifiedAgentID` int(11) DEFAULT NULL,
  `OrdersSt_iChangeSetID` int(11) DEFAULT NULL,
  `OrdersSt_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pcktbl`
--

DROP TABLE IF EXISTS `pcktbl`;
CREATE TABLE `pcktbl` (
  `idPckTbl` int(11) NOT NULL,
  `Code` varchar(5) NOT NULL,
  `PackSize` double DEFAULT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `PckTbl_iBranchID` int(11) DEFAULT NULL,
  `PckTbl_dCreatedDate` datetime(6) DEFAULT NULL,
  `PckTbl_dModifiedDate` datetime(6) DEFAULT NULL,
  `PckTbl_iCreatedBranchID` int(11) DEFAULT NULL,
  `PckTbl_iModifiedBranchID` int(11) DEFAULT NULL,
  `PckTbl_iCreatedAgentID` int(11) DEFAULT NULL,
  `PckTbl_iModifiedAgentID` int(11) DEFAULT NULL,
  `PckTbl_iChangeSetID` int(11) DEFAULT NULL,
  `PckTbl_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periodpermissions`
--

DROP TABLE IF EXISTS `periodpermissions`;
CREATE TABLE `periodpermissions` (
  `idPeriodPermissions` int(11) NOT NULL,
  `Period` int(11) DEFAULT NULL,
  `AgentType` int(11) DEFAULT NULL,
  `AgentID` int(11) DEFAULT NULL,
  `Allow` tinyint(1) NOT NULL DEFAULT '0',
  `PeriodPermissions_iBranchID` int(11) DEFAULT NULL,
  `PeriodPermissions_dCreatedDate` datetime(6) DEFAULT NULL,
  `PeriodPermissions_dModifiedDate` datetime(6) DEFAULT NULL,
  `PeriodPermissions_iCreatedBranchID` int(11) DEFAULT NULL,
  `PeriodPermissions_iModifiedBranchID` int(11) DEFAULT NULL,
  `PeriodPermissions_iCreatedAgentID` int(11) DEFAULT NULL,
  `PeriodPermissions_iModifiedAgentID` int(11) DEFAULT NULL,
  `PeriodPermissions_iChangeSetID` int(11) DEFAULT NULL,
  `PeriodPermissions_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `group` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cashes`
--

DROP TABLE IF EXISTS `petty_cashes`;
CREATE TABLE `petty_cashes` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `petty_cash_type_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `reference` longtext CHARACTER SET utf8mb4 NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash_types`
--

DROP TABLE IF EXISTS `petty_cash_types`;
CREATE TABLE `petty_cash_types` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `cashbook_batch_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pinneditems`
--

DROP TABLE IF EXISTS `pinneditems`;
CREATE TABLE `pinneditems` (
  `PinnedItemID` int(11) NOT NULL,
  `Name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Description` varchar(160) CHARACTER SET utf8mb4 DEFAULT NULL,
  `BackGround` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Created` datetime(6) NOT NULL,
  `Modified` datetime(6) NOT NULL,
  `AgentID` int(11) NOT NULL,
  `ApplicationName` varchar(50) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posdefs`
--

DROP TABLE IF EXISTS `posdefs`;
CREATE TABLE `posdefs` (
  `IDPOSDefs` int(11) NOT NULL,
  `AllowLineDisc` varchar(1) DEFAULT NULL,
  `Max_LDisc` double DEFAULT NULL,
  `Max_Disc` double DEFAULT NULL,
  `RecPref` varchar(20) DEFAULT NULL,
  `RecNum` varchar(15) DEFAULT NULL,
  `iRecPad` int(11) DEFAULT NULL,
  `bAutoRecNum` tinyint(1) DEFAULT NULL,
  `PoleLine1` varchar(20) DEFAULT NULL,
  `PoleLine2` varchar(20) DEFAULT NULL,
  `AllocatePM` tinyint(1) NOT NULL DEFAULT '0',
  `iTrCodesIDSTSale` int(11) DEFAULT NULL,
  `iTrCodesIDSTReturn` int(11) DEFAULT NULL,
  `iTrCodesIDPOSFloat` int(11) DEFAULT NULL,
  `iTrCodesIDPOSBanking` int(11) DEFAULT NULL,
  `iTrCodesIDPOSPettyCash` int(11) DEFAULT NULL,
  `iTrCodesIDPOSShortage` int(11) DEFAULT NULL,
  `iTrCodesIDPOSExcess` int(11) DEFAULT NULL,
  `iTrCodesIDARReceipt` int(11) DEFAULT NULL,
  `iTrCodesIDAPPayment` int(11) DEFAULT NULL,
  `iTenderIDDefFloat` int(11) DEFAULT NULL,
  `iTenderIDDefBanking` int(11) DEFAULT NULL,
  `iTenderIDDefPettyCash` int(11) DEFAULT NULL,
  `iTenderIDDefShortage` int(11) DEFAULT NULL,
  `iTenderIDDefExcess` int(11) DEFAULT NULL,
  `iTenderIDDefReceipt` int(11) DEFAULT NULL,
  `iTenderIDDefPayment` int(11) DEFAULT NULL,
  `iTenderIDDefCash` int(11) DEFAULT '1',
  `bAutoAllocCashPM` tinyint(1) NOT NULL DEFAULT '0',
  `iTrCodesIDARDepositRefund` int(11) DEFAULT NULL,
  `iTenderIDDefDepositRefund` int(11) DEFAULT NULL,
  `iTrCodesIDARRefund` int(11) DEFAULT NULL,
  `iTenderIDDefRefund` int(11) DEFAULT NULL,
  `bUseDocumentRoundingOnTender` tinyint(1) DEFAULT '0',
  `PosDefs_iBranchID` int(11) DEFAULT NULL,
  `PosDefs_dCreatedDate` datetime(6) DEFAULT NULL,
  `PosDefs_dModifiedDate` datetime(6) DEFAULT NULL,
  `PosDefs_iCreatedBranchID` int(11) DEFAULT NULL,
  `PosDefs_iModifiedBranchID` int(11) DEFAULT NULL,
  `PosDefs_iCreatedAgentID` int(11) DEFAULT NULL,
  `PosDefs_iModifiedAgentID` int(11) DEFAULT NULL,
  `PosDefs_iChangeSetID` int(11) DEFAULT NULL,
  `PosDefs_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `postap`
--

DROP TABLE IF EXISTS `postap`;
CREATE TABLE `postap` (
  `AutoIdx` bigint(20) NOT NULL,
  `TxDate` datetime DEFAULT NULL,
  `Id` varchar(5) NOT NULL,
  `AccountLink` int(11) DEFAULT NULL,
  `TrCodeID` int(11) DEFAULT NULL,
  `Debit` double DEFAULT NULL,
  `Credit` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fForeignDebit` double DEFAULT NULL,
  `fForeignCredit` double DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `TaxTypeID` int(11) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `Order_No` varchar(50) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `cAuditNumber` varchar(50) DEFAULT NULL,
  `Tax_Amount` double DEFAULT NULL,
  `fForeignTax` double DEFAULT NULL,
  `Project` int(11) DEFAULT NULL,
  `Outstanding` double DEFAULT NULL,
  `fForeignOutstanding` double DEFAULT NULL,
  `cAllocs` longtext,
  `InvNumKey` int(11) DEFAULT NULL,
  `CRCCheck` double DEFAULT NULL,
  `DTStamp` datetime(6) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `iTaxPeriodID` int(11) DEFAULT NULL,
  `cReference2` varchar(50) DEFAULT NULL,
  `iAge` int(11) DEFAULT NULL,
  `dDateAged` datetime(6) DEFAULT NULL,
  `iPostSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iTxBranchID` int(11) DEFAULT NULL,
  `bPBTPaid` tinyint(1) NOT NULL DEFAULT '0',
  `iGLTaxAccountID` int(11) DEFAULT NULL,
  `bTxOnHold` tinyint(1) NOT NULL DEFAULT '0',
  `PostAP_iBranchID` int(11) DEFAULT NULL,
  `PostAP_dCreatedDate` datetime(6) DEFAULT NULL,
  `PostAP_dModifiedDate` datetime(6) DEFAULT NULL,
  `PostAP_iCreatedBranchID` int(11) DEFAULT NULL,
  `PostAP_iModifiedBranchID` int(11) DEFAULT NULL,
  `PostAP_iCreatedAgentID` int(11) DEFAULT NULL,
  `PostAP_iModifiedAgentID` int(11) DEFAULT NULL,
  `PostAP_iChangeSetID` int(11) DEFAULT NULL,
  `PostAP_Checksum` binary(20) DEFAULT NULL,
  `SagePayExtra1` longtext,
  `SagePayExtra2` longtext,
  `SagePayExtra3` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `postar`
--

DROP TABLE IF EXISTS `postar`;
CREATE TABLE `postar` (
  `AutoIdx` bigint(20) NOT NULL,
  `TxDate` datetime DEFAULT NULL,
  `Id` varchar(5) NOT NULL,
  `AccountLink` int(11) DEFAULT NULL,
  `TrCodeID` int(11) DEFAULT NULL,
  `Debit` double DEFAULT NULL,
  `Credit` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fForeignDebit` double DEFAULT NULL,
  `fForeignCredit` double DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `TaxTypeID` int(11) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `Order_No` varchar(50) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `cAuditNumber` varchar(50) DEFAULT NULL,
  `Tax_Amount` double DEFAULT NULL,
  `fForeignTax` double DEFAULT NULL,
  `Project` int(11) DEFAULT NULL,
  `Outstanding` double DEFAULT NULL,
  `fForeignOutstanding` double DEFAULT NULL,
  `cAllocs` longtext,
  `InvNumKey` int(11) DEFAULT NULL,
  `RepID` int(11) DEFAULT NULL,
  `LinkAccCode` int(11) DEFAULT NULL,
  `TillID` int(11) DEFAULT NULL,
  `CRCCheck` double DEFAULT NULL,
  `DTStamp` datetime(6) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `iTaxPeriodID` int(11) DEFAULT NULL,
  `cReference2` varchar(50) DEFAULT NULL,
  `fJCRepCost` double DEFAULT NULL,
  `iAge` int(11) DEFAULT NULL,
  `dDateAged` datetime(6) DEFAULT NULL,
  `iPostSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iTxBranchID` int(11) DEFAULT NULL,
  `iMBPropertyID` int(11) NOT NULL DEFAULT '0',
  `iMBPortionID` int(11) NOT NULL DEFAULT '0',
  `iMBServiceID` int(11) NOT NULL DEFAULT '0',
  `iMBMeterID` int(11) NOT NULL DEFAULT '0',
  `iMBPropertyPortionServiceID` int(11) NOT NULL DEFAULT '0',
  `bPBTPaid` tinyint(1) NOT NULL DEFAULT '0',
  `iGLTaxAccountID` int(11) DEFAULT NULL,
  `iTransactionType` int(11) NOT NULL DEFAULT '0',
  `PostAR_iBranchID` int(11) DEFAULT NULL,
  `PostAR_dCreatedDate` datetime(6) DEFAULT NULL,
  `PostAR_dModifiedDate` datetime(6) DEFAULT NULL,
  `PostAR_iCreatedBranchID` int(11) DEFAULT NULL,
  `PostAR_iModifiedBranchID` int(11) DEFAULT NULL,
  `PostAR_iCreatedAgentID` int(11) DEFAULT NULL,
  `PostAR_iModifiedAgentID` int(11) DEFAULT NULL,
  `PostAR_iChangeSetID` int(11) DEFAULT NULL,
  `PostAR_Checksum` binary(20) DEFAULT NULL,
  `SagePayExtra1` longtext,
  `SagePayExtra2` longtext,
  `SagePayExtra3` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `postgl`
--

DROP TABLE IF EXISTS `postgl`;
CREATE TABLE `postgl` (
  `AutoIdx` bigint(20) NOT NULL,
  `TxDate` datetime DEFAULT NULL,
  `Id` varchar(5) NOT NULL,
  `AccountLink` int(11) DEFAULT NULL,
  `TrCodeID` int(11) DEFAULT NULL,
  `Debit` double DEFAULT NULL,
  `Credit` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fForeignDebit` double DEFAULT NULL,
  `fForeignCredit` double DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `TaxTypeID` int(11) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `Order_No` varchar(50) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `cAuditNumber` varchar(50) DEFAULT NULL,
  `Tax_Amount` double DEFAULT NULL,
  `fForeignTax` double DEFAULT NULL,
  `Project` int(11) DEFAULT NULL,
  `Period` int(11) DEFAULT NULL,
  `DrCrAccount` int(11) DEFAULT NULL,
  `JobCodeLink` int(11) DEFAULT NULL,
  `CRCCheck` double DEFAULT NULL,
  `DTStamp` datetime(6) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `iTaxPeriodID` int(11) DEFAULT NULL,
  `cPayeeName` varchar(100) DEFAULT NULL,
  `bPrintCheque` tinyint(1) NOT NULL DEFAULT '0',
  `cReference2` varchar(50) DEFAULT NULL,
  `RepID` int(11) DEFAULT NULL,
  `fJCRepCost` double DEFAULT NULL,
  `iMFPID` int(11) DEFAULT NULL,
  `bIsJCDocLine` tinyint(1) NOT NULL DEFAULT '0',
  `bIsSTGLDocLine` tinyint(1) NOT NULL DEFAULT '0',
  `iInvLineID` bigint(20) NOT NULL DEFAULT '0',
  `iTxBranchID` int(11) DEFAULT NULL,
  `cBankRef` varchar(20) DEFAULT NULL,
  `bPBTPaid` tinyint(1) NOT NULL DEFAULT '0',
  `iGLTaxAccountID` int(11) DEFAULT NULL,
  `bReconciled` tinyint(1) NOT NULL DEFAULT '0',
  `PostGL_iBranchID` int(11) DEFAULT NULL,
  `PostGL_dCreatedDate` datetime(6) DEFAULT NULL,
  `PostGL_dModifiedDate` datetime(6) DEFAULT NULL,
  `PostGL_iCreatedBranchID` int(11) DEFAULT NULL,
  `PostGL_iModifiedBranchID` int(11) DEFAULT NULL,
  `PostGL_iCreatedAgentID` int(11) DEFAULT NULL,
  `PostGL_iModifiedAgentID` int(11) DEFAULT NULL,
  `PostGL_iChangeSetID` int(11) DEFAULT NULL,
  `PostGL_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `postst`
--

DROP TABLE IF EXISTS `postst`;
CREATE TABLE `postst` (
  `AutoIdx` bigint(20) NOT NULL,
  `TxDate` datetime DEFAULT NULL,
  `Id` varchar(5) NOT NULL,
  `AccountLink` int(11) DEFAULT NULL,
  `TrCodeID` int(11) DEFAULT NULL,
  `Debit` double DEFAULT NULL,
  `Credit` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fForeignDebit` double DEFAULT NULL,
  `fForeignCredit` double DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `TaxTypeID` int(11) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `Order_No` varchar(50) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `cAuditNumber` varchar(50) DEFAULT NULL,
  `Tax_Amount` double DEFAULT NULL,
  `fForeignTax` double DEFAULT NULL,
  `Project` int(11) DEFAULT NULL,
  `Quantity` double DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  `WarehouseID` int(11) DEFAULT NULL,
  `JobCodeLink` int(11) DEFAULT NULL,
  `iJobLineID` bigint(20) NOT NULL DEFAULT '0',
  `TillID` int(11) DEFAULT NULL,
  `DrCrAccount` int(11) DEFAULT NULL,
  `CRCCheck` double DEFAULT NULL,
  `DTStamp` datetime(6) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `iTaxPeriodID` int(11) DEFAULT NULL,
  `InvNumKey` int(11) DEFAULT NULL,
  `cReference2` varchar(50) DEFAULT NULL,
  `RepID` int(11) DEFAULT NULL,
  `bChargeCom` tinyint(1) NOT NULL DEFAULT '1',
  `iMFPID` int(11) DEFAULT NULL,
  `iLotID` int(11) DEFAULT NULL,
  `iMFPLineID` bigint(20) NOT NULL DEFAULT '0',
  `fUnManufactured` double NOT NULL DEFAULT '0',
  `fAdditionalCost` double NOT NULL DEFAULT '0',
  `iPostEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iGLAccountID` int(11) NOT NULL DEFAULT '0',
  `iTxBranchID` int(11) DEFAULT NULL,
  `fJCWIPQuantity` double NOT NULL DEFAULT '0',
  `fMFPWIPQuantity` double NOT NULL DEFAULT '0',
  `QuantityR` double DEFAULT NULL,
  `fQuantityInvoiced` double NOT NULL DEFAULT '0',
  `PostST_iBranchID` int(11) DEFAULT NULL,
  `PostST_dCreatedDate` datetime(6) DEFAULT NULL,
  `PostST_dModifiedDate` datetime(6) DEFAULT NULL,
  `PostST_iCreatedBranchID` int(11) DEFAULT NULL,
  `PostST_iModifiedBranchID` int(11) DEFAULT NULL,
  `PostST_iCreatedAgentID` int(11) DEFAULT NULL,
  `PostST_iModifiedAgentID` int(11) DEFAULT NULL,
  `PostST_iChangeSetID` int(11) DEFAULT NULL,
  `PostST_Checksum` binary(20) DEFAULT NULL,
  `SagePayExtra1` longtext,
  `SagePayExtra2` longtext,
  `SagePayExtra3` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `printgrp`
--

DROP TABLE IF EXISTS `printgrp`;
CREATE TABLE `printgrp` (
  `AutoIdx` int(11) NOT NULL,
  `Descrip` varchar(100) DEFAULT NULL,
  `GrpList` longtext,
  `DocType` int(11) DEFAULT NULL,
  `PrintGrp_iBranchID` int(11) DEFAULT NULL,
  `PrintGrp_dCreatedDate` datetime(6) DEFAULT NULL,
  `PrintGrp_dModifiedDate` datetime(6) DEFAULT NULL,
  `PrintGrp_iCreatedBranchID` int(11) DEFAULT NULL,
  `PrintGrp_iModifiedBranchID` int(11) DEFAULT NULL,
  `PrintGrp_iCreatedAgentID` int(11) DEFAULT NULL,
  `PrintGrp_iModifiedAgentID` int(11) DEFAULT NULL,
  `PrintGrp_iChangeSetID` int(11) DEFAULT NULL,
  `PrintGrp_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `ProjectLink` int(11) NOT NULL,
  `ProjectCode` varchar(21) DEFAULT NULL,
  `ProjectName` varchar(50) DEFAULT NULL,
  `ActiveProject` tinyint(1) NOT NULL DEFAULT '0',
  `ProjectDescription` varchar(60) DEFAULT NULL,
  `MasterSubProject` varchar(41) DEFAULT NULL,
  `ProjectLevel` int(11) DEFAULT NULL,
  `SubProjectOfLink` int(11) DEFAULT NULL,
  `Project_iBranchID` int(11) DEFAULT NULL,
  `Project_dCreatedDate` datetime(6) DEFAULT NULL,
  `Project_dModifiedDate` datetime(6) DEFAULT NULL,
  `Project_iCreatedBranchID` int(11) DEFAULT NULL,
  `Project_iModifiedBranchID` int(11) DEFAULT NULL,
  `Project_iCreatedAgentID` int(11) DEFAULT NULL,
  `Project_iModifiedAgentID` int(11) DEFAULT NULL,
  `Project_iChangeSetID` int(11) DEFAULT NULL,
  `Project_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recentitems`
--

DROP TABLE IF EXISTS `recentitems`;
CREATE TABLE `recentitems` (
  `RecentItemID` int(11) NOT NULL,
  `Name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Created` datetime(6) NOT NULL,
  `AgentID` int(11) NOT NULL,
  `ApplicationName` varchar(50) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recurrc`
--

DROP TABLE IF EXISTS `recurrc`;
CREATE TABLE `recurrc` (
  `AutoIdx` int(11) NOT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '0',
  `IntType` int(11) DEFAULT NULL,
  `IntData1` int(11) DEFAULT NULL,
  `IntData2` int(11) DEFAULT NULL,
  `Occur` int(11) DEFAULT NULL,
  `ActDate` datetime DEFAULT NULL,
  `TermDate` datetime DEFAULT NULL,
  `ChgNow` tinyint(1) NOT NULL DEFAULT '0',
  `RecurRC_iBranchID` int(11) DEFAULT NULL,
  `RecurRC_dCreatedDate` datetime(6) DEFAULT NULL,
  `RecurRC_dModifiedDate` datetime(6) DEFAULT NULL,
  `RecurRC_iCreatedBranchID` int(11) DEFAULT NULL,
  `RecurRC_iModifiedBranchID` int(11) DEFAULT NULL,
  `RecurRC_iCreatedAgentID` int(11) DEFAULT NULL,
  `RecurRC_iModifiedAgentID` int(11) DEFAULT NULL,
  `RecurRC_iChangeSetID` int(11) DEFAULT NULL,
  `RecurRC_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recurrdef`
--

DROP TABLE IF EXISTS `recurrdef`;
CREATE TABLE `recurrdef` (
  `idRecurrDef` int(11) NOT NULL,
  `iRRAgentID` int(11) DEFAULT NULL,
  `iDCModule` int(11) DEFAULT NULL,
  `iPromptOpt` int(11) DEFAULT NULL,
  `dLastPrompt` datetime(6) DEFAULT NULL,
  `bAlwaysLoad` tinyint(1) NOT NULL DEFAULT '1',
  `bAutoReference` tinyint(1) NOT NULL DEFAULT '0',
  `vReferencePrefix` varchar(20) DEFAULT NULL,
  `iReferenceNext` int(11) DEFAULT NULL,
  `iReferencePadLength` int(11) DEFAULT NULL,
  `iPMTrCodesID` int(11) DEFAULT NULL,
  `RecurRDef_iBranchID` int(11) DEFAULT NULL,
  `RecurRDef_dCreatedDate` datetime(6) DEFAULT NULL,
  `RecurRDef_dModifiedDate` datetime(6) DEFAULT NULL,
  `RecurRDef_iCreatedBranchID` int(11) DEFAULT NULL,
  `RecurRDef_iModifiedBranchID` int(11) DEFAULT NULL,
  `RecurRDef_iCreatedAgentID` int(11) DEFAULT NULL,
  `RecurRDef_iModifiedAgentID` int(11) DEFAULT NULL,
  `RecurRDef_iChangeSetID` int(11) DEFAULT NULL,
  `RecurRDef_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recurrf`
--

DROP TABLE IF EXISTS `recurrf`;
CREATE TABLE `recurrf` (
  `AutoIdx` int(11) NOT NULL,
  `Module` int(11) DEFAULT NULL,
  `Descrip` varchar(30) DEFAULT NULL,
  `Amount` double DEFAULT NULL,
  `Incl` tinyint(1) NOT NULL DEFAULT '0',
  `TrCodeID` int(11) DEFAULT NULL,
  `TaxTypeID` int(11) DEFAULT NULL,
  `CurrencyID` int(11) DEFAULT NULL,
  `BaseHome` tinyint(1) DEFAULT '1',
  `iTmplSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `RecurRF_iBranchID` int(11) DEFAULT NULL,
  `RecurRF_dCreatedDate` datetime(6) DEFAULT NULL,
  `RecurRF_dModifiedDate` datetime(6) DEFAULT NULL,
  `RecurRF_iCreatedBranchID` int(11) DEFAULT NULL,
  `RecurRF_iModifiedBranchID` int(11) DEFAULT NULL,
  `RecurRF_iCreatedAgentID` int(11) DEFAULT NULL,
  `RecurRF_iModifiedAgentID` int(11) DEFAULT NULL,
  `RecurRF_iChangeSetID` int(11) DEFAULT NULL,
  `RecurRF_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recurrl`
--

DROP TABLE IF EXISTS `recurrl`;
CREATE TABLE `recurrl` (
  `AutoIdx` int(11) NOT NULL,
  `Suspend` tinyint(1) NOT NULL DEFAULT '0',
  `LastUpd` datetime DEFAULT NULL,
  `State` int(11) DEFAULT NULL,
  `Module` int(11) DEFAULT NULL,
  `Account` int(11) DEFAULT NULL,
  `ChgType` int(11) DEFAULT NULL,
  `Template` int(11) DEFAULT NULL,
  `Config` int(11) DEFAULT NULL,
  `ActDate` datetime DEFAULT NULL,
  `TermDate` datetime DEFAULT NULL,
  `OccurCnt` int(11) DEFAULT NULL,
  `iContractID` int(11) DEFAULT NULL,
  `bCreateAsOrder` tinyint(1) NOT NULL DEFAULT '0',
  `bDebitOrder` tinyint(1) NOT NULL DEFAULT '0',
  `bCreatePayment` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowSettlementTerms` tinyint(1) NOT NULL DEFAULT '1',
  `RecurRL_iBranchID` int(11) DEFAULT NULL,
  `RecurRL_dCreatedDate` datetime(6) DEFAULT NULL,
  `RecurRL_dModifiedDate` datetime(6) DEFAULT NULL,
  `RecurRL_iCreatedBranchID` int(11) DEFAULT NULL,
  `RecurRL_iModifiedBranchID` int(11) DEFAULT NULL,
  `RecurRL_iCreatedAgentID` int(11) DEFAULT NULL,
  `RecurRL_iModifiedAgentID` int(11) DEFAULT NULL,
  `RecurRL_iChangeSetID` int(11) DEFAULT NULL,
  `RecurRL_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recurrtx`
--

DROP TABLE IF EXISTS `recurrtx`;
CREATE TABLE `recurrtx` (
  `AutoIdx` int(11) NOT NULL,
  `ListIdx` int(11) DEFAULT NULL,
  `TxDate` datetime DEFAULT NULL,
  `TxAmount` double DEFAULT NULL,
  `cTxAuditNumber` varchar(50) DEFAULT NULL,
  `iInvNumID` int(11) DEFAULT NULL,
  `bDebitOrderPosted` tinyint(1) NOT NULL DEFAULT '0',
  `bPaymentCreated` tinyint(1) NOT NULL DEFAULT '0',
  `CurrencyID` int(11) DEFAULT NULL,
  `ExchangeRate` double DEFAULT NULL,
  `TxAmountForeign` double DEFAULT NULL,
  `RecurRTX_iBranchID` int(11) DEFAULT NULL,
  `RecurRTX_dCreatedDate` datetime(6) DEFAULT NULL,
  `RecurRTX_dModifiedDate` datetime(6) DEFAULT NULL,
  `RecurRTX_iCreatedBranchID` int(11) DEFAULT NULL,
  `RecurRTX_iModifiedBranchID` int(11) DEFAULT NULL,
  `RecurRTX_iCreatedAgentID` int(11) DEFAULT NULL,
  `RecurRTX_iModifiedAgentID` int(11) DEFAULT NULL,
  `RecurRTX_iChangeSetID` int(11) DEFAULT NULL,
  `RecurRTX_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `refer`
--

DROP TABLE IF EXISTS `refer`;
CREATE TABLE `refer` (
  `AutoIdx` int(11) NOT NULL,
  `DebtorAccNo` varchar(20) DEFAULT NULL,
  `NameOfRef` varchar(30) DEFAULT NULL,
  `IDNumber` varchar(16) DEFAULT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  `Address1` varchar(30) DEFAULT NULL,
  `Address2` varchar(30) DEFAULT NULL,
  `Address3` varchar(30) DEFAULT NULL,
  `Telephone` varchar(20) DEFAULT NULL,
  `PerShare` double DEFAULT NULL,
  `ContactPerson` varchar(30) DEFAULT NULL,
  `CreditLimit` double DEFAULT NULL,
  `Rating` varchar(15) DEFAULT NULL,
  `Category` varchar(1) DEFAULT NULL,
  `CreditGranted` varchar(1) DEFAULT NULL,
  `PunctPayment` int(11) DEFAULT NULL,
  `Refer_iBranchID` int(11) DEFAULT NULL,
  `Refer_dCreatedDate` datetime(6) DEFAULT NULL,
  `Refer_dModifiedDate` datetime(6) DEFAULT NULL,
  `Refer_iCreatedBranchID` int(11) DEFAULT NULL,
  `Refer_iModifiedBranchID` int(11) DEFAULT NULL,
  `Refer_iCreatedAgentID` int(11) DEFAULT NULL,
  `Refer_iModifiedAgentID` int(11) DEFAULT NULL,
  `Refer_iChangeSetID` int(11) DEFAULT NULL,
  `Refer_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reminder`
--

DROP TABLE IF EXISTS `reminder`;
CREATE TABLE `reminder` (
  `Autoidx` int(11) NOT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `Dated` datetime DEFAULT NULL,
  `TypeofRemind` varchar(25) DEFAULT NULL,
  `TextMemo` longtext,
  `Status` varchar(1) DEFAULT NULL,
  `CompanyName` varchar(35) DEFAULT NULL,
  `ContactPerson` varchar(30) DEFAULT NULL,
  `Telephone` varchar(15) DEFAULT NULL,
  `Fax` varchar(15) DEFAULT NULL,
  `RemindDate` datetime DEFAULT NULL,
  `Reminder_iBranchID` int(11) DEFAULT NULL,
  `Reminder_dCreatedDate` datetime(6) DEFAULT NULL,
  `Reminder_dModifiedDate` datetime(6) DEFAULT NULL,
  `Reminder_iCreatedBranchID` int(11) DEFAULT NULL,
  `Reminder_iModifiedBranchID` int(11) DEFAULT NULL,
  `Reminder_iCreatedAgentID` int(11) DEFAULT NULL,
  `Reminder_iModifiedAgentID` int(11) DEFAULT NULL,
  `Reminder_iChangeSetID` int(11) DEFAULT NULL,
  `Reminder_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq`
--

DROP TABLE IF EXISTS `rfq`;
CREATE TABLE `rfq` (
  `iRFQID` int(11) NOT NULL,
  `iRequisitionLinesID` int(11) NOT NULL,
  `iModuleID` int(11) NOT NULL DEFAULT '0',
  `iStockCodeID` int(11) DEFAULT NULL,
  `cDescription` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iWareHouseID` int(11) DEFAULT NULL,
  `fQuantity` double NOT NULL DEFAULT '0',
  `fQtyProcessed` double NOT NULL DEFAULT '0',
  `fQtyConfirm` double NOT NULL DEFAULT '0',
  `fUnitPrice` double NOT NULL DEFAULT '0',
  `iTaxType` int(11) DEFAULT NULL,
  `fTaxRate` double DEFAULT NULL,
  `fLineDiscount` double DEFAULT NULL,
  `fQuantityLineTotExcl` double DEFAULT NULL,
  `fQuantityLineTaxAmount` double DEFAULT NULL,
  `dExpectedDate` datetime(6) DEFAULT NULL,
  `iRequisitionID` int(11) DEFAULT NULL,
  `OrderNum` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iState` int(11) NOT NULL DEFAULT '0',
  `iSupplierID` int(11) NOT NULL,
  `dOrderDate` datetime(6) DEFAULT NULL,
  `fQuotedPrice` double DEFAULT '0',
  `fQuotedQuantity` double DEFAULT '0',
  `Archived` tinyint(1) DEFAULT '0',
  `dLastModifiedDate` datetime(6) DEFAULT NULL,
  `dReceivedDate` datetime(6) DEFAULT NULL,
  `cLineNotes` varchar(1024) CHARACTER SET utf8mb4 DEFAULT NULL,
  `dEvaluationdate` datetime(6) DEFAULT NULL,
  `cEvaluationComments` varchar(1024) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cFileName` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cFileContent` longblob,
  `dQuotationDeadLine` datetime(6) DEFAULT NULL,
  `bApprovePO` tinyint(1) DEFAULT NULL,
  `bAuthorizePO` tinyint(1) DEFAULT NULL,
  `iHODAgent1` int(11) DEFAULT '0',
  `iHODAgent2` int(11) DEFAULT NULL,
  `fQuotedPriceForeign` double DEFAULT NULL,
  `fQuotedPriceIncl` double DEFAULT NULL,
  `iCriteriaID` int(11) DEFAULT '0',
  `bRecommend` tinyint(1) DEFAULT '0',
  `RFQ_iBranchID` int(11) DEFAULT NULL,
  `RFQ_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfqdf`
--

DROP TABLE IF EXISTS `rfqdf`;
CREATE TABLE `rfqdf` (
  `DefaultCounter` int(11) NOT NULL,
  `iWIPLink` int(11) NOT NULL DEFAULT '0',
  `iMasterLink` int(11) NOT NULL DEFAULT '0',
  `DCLink` int(11) NOT NULL DEFAULT '1',
  `bSalesOrder` tinyint(1) NOT NULL DEFAULT '0',
  `bWareHouse` tinyint(1) NOT NULL DEFAULT '0',
  `cSageAppPath` longtext CHARACTER SET utf8mb4,
  `bIsPrefSupplier` tinyint(1) DEFAULT '1',
  `IsGL` tinyint(1) DEFAULT '1',
  `IsOther` tinyint(1) DEFAULT '1',
  `iIncidentType` int(11) DEFAULT NULL,
  `bVolumeContract` tinyint(1) DEFAULT NULL,
  `cDBVersion` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bResZeroStock` tinyint(1) DEFAULT NULL,
  `bShowReportLogo` tinyint(1) DEFAULT NULL,
  `iRFQAutorization` int(11) DEFAULT '1',
  `IsQuotedPriceEditable` tinyint(1) DEFAULT NULL,
  `IsCostCentreEditable` tinyint(1) DEFAULT NULL,
  `IsQuantityEditable` tinyint(1) DEFAULT NULL,
  `SetRejectionProcess` tinyint(1) DEFAULT NULL,
  `bIsLinkedSupplier` tinyint(1) NOT NULL DEFAULT '1',
  `bIsCriteriaAside` tinyint(1) NOT NULL DEFAULT '0',
  `iSupplierIncident` int(11) DEFAULT NULL,
  `bProspectiveSupplier` tinyint(1) DEFAULT NULL,
  `bRFQWorkflow` tinyint(1) DEFAULT NULL,
  `AutoNewRFQNo` tinyint(1) DEFAULT '1',
  `NewRFQPrefix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NextNewRFQNo` int(11) DEFAULT NULL,
  `NewRFQPadLength` int(11) DEFAULT NULL,
  `fReportingAmount` double DEFAULT NULL,
  `fRestrictedSupOrdAmt` double DEFAULT NULL,
  `bUseInclPrices` tinyint(1) DEFAULT NULL,
  `bReportToNatTreasury` tinyint(1) DEFAULT NULL,
  `cReportingEmailID` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bForceEmailToNationalTreasury` tinyint(1) DEFAULT NULL,
  `bBlockRestrictedSup` tinyint(1) DEFAULT NULL,
  `cMunicipalityName` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bBBBEE_Script` tinyint(1) NOT NULL DEFAULT '0',
  `bRotateOnRFQ` tinyint(1) NOT NULL DEFAULT '0',
  `iRandomSupplier` int(11) NOT NULL DEFAULT '5',
  `bForceCostCentre` tinyint(1) DEFAULT NULL,
  `iSTTrCode` int(11) NOT NULL DEFAULT '0',
  `bSupplierRotation` tinyint(1) NOT NULL DEFAULT '1',
  `bSupplierPreference` tinyint(1) NOT NULL DEFAULT '0',
  `cSupplierUDFs` longtext CHARACTER SET utf8mb4,
  `bAutomatedCriteria` tinyint(1) NOT NULL DEFAULT '0',
  `IsST` tinyint(1) DEFAULT NULL,
  `bForceSector` tinyint(1) DEFAULT NULL,
  `RFQDF_iBranchID` int(11) DEFAULT NULL,
  `RFQDF_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQDF_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQDF_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQDF_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQDF_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQDF_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQDF_iChangeSetID` int(11) DEFAULT NULL,
  `RFQDF_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_agentcostcentremap`
--

DROP TABLE IF EXISTS `rfq_agentcostcentremap`;
CREATE TABLE `rfq_agentcostcentremap` (
  `idAgentCostCentreMap` int(11) NOT NULL,
  `idAgent` int(11) DEFAULT NULL,
  `CostCentreId` longtext,
  `dLastModifiedDate` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_agentsectormapping`
--

DROP TABLE IF EXISTS `rfq_agentsectormapping`;
CREATE TABLE `rfq_agentsectormapping` (
  `idAgentSectorMap` int(11) NOT NULL,
  `idAgents` int(11) DEFAULT NULL,
  `SectorIds` longtext,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_apshareholderlinks`
--

DROP TABLE IF EXISTS `rfq_apshareholderlinks`;
CREATE TABLE `rfq_apshareholderlinks` (
  `idAPShareholderLinks` int(11) NOT NULL,
  `iAPShareholderID` int(11) NOT NULL,
  `iSupplierID` int(11) NOT NULL,
  `fPercentage` double NOT NULL,
  `cPositionHeld` varchar(50) NOT NULL,
  `bDirector` tinyint(1) DEFAULT '0',
  `iBranchID` int(11) DEFAULT NULL,
  `dCreatedDate` datetime(6) DEFAULT NULL,
  `dModifiedDate` datetime(6) DEFAULT NULL,
  `iCreatedBranchID` int(11) DEFAULT NULL,
  `iModifiedBranchID` int(11) DEFAULT NULL,
  `iCreatedAgentID` int(11) DEFAULT NULL,
  `iModifiedAgentID` int(11) DEFAULT NULL,
  `iChangeSetID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_audit`
--

DROP TABLE IF EXISTS `rfq_audit`;
CREATE TABLE `rfq_audit` (
  `TableName` varchar(128) NOT NULL,
  `TableID` varchar(1000) DEFAULT NULL,
  `FieldName` varchar(128) NOT NULL,
  `OldValue` varchar(1000) DEFAULT NULL,
  `NewValue` varchar(1000) DEFAULT NULL,
  `UpdateDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UserName` varchar(128) DEFAULT NULL,
  `WorkStation` varchar(128) DEFAULT NULL,
  `Application` varchar(128) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_audittables`
--

DROP TABLE IF EXISTS `rfq_audittables`;
CREATE TABLE `rfq_audittables` (
  `iAuditTableID` int(11) NOT NULL,
  `cTableName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bIsAuditing` tinyint(1) DEFAULT '0',
  `bIsAudit` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_costcentre`
--

DROP TABLE IF EXISTS `rfq_costcentre`;
CREATE TABLE `rfq_costcentre` (
  `idCostCentre` int(11) NOT NULL,
  `cCostCentre` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cDescription` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bAddedToUDF` tinyint(1) DEFAULT NULL,
  `RFQ_costcentre_iBranchID` int(11) DEFAULT NULL,
  `RFQ_costcentre_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_costcentre_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_costcentre_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_costcentre_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_costcentre_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_costcentre_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_costcentre_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_costcentre_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_deviationreason`
--

DROP TABLE IF EXISTS `rfq_deviationreason`;
CREATE TABLE `rfq_deviationreason` (
  `idDeviationReason` int(11) NOT NULL,
  `cCode` varchar(10) DEFAULT NULL,
  `cReasonDesc` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bActive` tinyint(1) DEFAULT NULL,
  `bDefault` tinyint(1) DEFAULT '0',
  `dCreatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_DeviationReason_iBranchID` int(11) DEFAULT NULL,
  `RFQ_DeviationReason_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_DeviationReason_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_DeviationReason_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_DeviationReason_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_DeviationReason_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_DeviationReason_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_DeviationReason_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_DeviationReason_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_deviations`
--

DROP TABLE IF EXISTS `rfq_deviations`;
CREATE TABLE `rfq_deviations` (
  `idDeviation` int(11) NOT NULL,
  `cScreenName` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `cType` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `iUserID` int(11) NOT NULL,
  `iRequisitionID` int(11) NOT NULL,
  `iRFQID` int(11) DEFAULT NULL,
  `EntityID` int(11) DEFAULT NULL,
  `dDeviationDate` datetime(6) DEFAULT NULL,
  `iCommentID` int(11) DEFAULT NULL,
  `cAdditionalComment` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_event`
--

DROP TABLE IF EXISTS `rfq_event`;
CREATE TABLE `rfq_event` (
  `iEventID` int(11) NOT NULL,
  `cEventCode` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cDescription` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_fileattachment`
--

DROP TABLE IF EXISTS `rfq_fileattachment`;
CREATE TABLE `rfq_fileattachment` (
  `iFileID` int(11) NOT NULL,
  `iRFQID` int(11) NOT NULL,
  `cFileName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cFileContent` longblob,
  `iSupplierID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_newquotationparams`
--

DROP TABLE IF EXISTS `rfq_newquotationparams`;
CREATE TABLE `rfq_newquotationparams` (
  `PK_NewQuotationParamID` int(11) NOT NULL,
  `FK_iRequisitionID` int(11) DEFAULT NULL,
  `FK_QuotationParamID` int(11) DEFAULT NULL,
  `cParamName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `IsMandatory` tinyint(1) DEFAULT NULL,
  `iScore` int(11) DEFAULT NULL,
  `cComment` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cNewComment` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iCriteriaID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_newtender`
--

DROP TABLE IF EXISTS `rfq_newtender`;
CREATE TABLE `rfq_newtender` (
  `PK_NewTenderID` int(11) NOT NULL,
  `cTenderNo` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cTenderTitle` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `cTenderRefNo` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cDescription` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `dAnnouncementDate` datetime(6) NOT NULL,
  `dOpeningDate` datetime(6) NOT NULL,
  `dLastSubmissionDate` datetime(6) NOT NULL,
  `dCompletiondate` datetime(6) NOT NULL,
  `fEarnestMoney` double DEFAULT NULL,
  `bIsEMDMandatory` tinyint(1) NOT NULL DEFAULT '0',
  `iProjectID` int(11) DEFAULT NULL,
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `fTenderTotal` double DEFAULT NULL,
  `cTenderTerms` longtext CHARACTER SET utf8mb4,
  `dLastModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iLastModifiedBy` int(11) DEFAULT NULL,
  `RFQ_NewTender_iBranchID` int(11) DEFAULT NULL,
  `RFQ_NewTender_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_NewTender_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_NewTender_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_NewTender_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_NewTender_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_NewTender_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_NewTender_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_NewTender_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_newtenderdetails`
--

DROP TABLE IF EXISTS `rfq_newtenderdetails`;
CREATE TABLE `rfq_newtenderdetails` (
  `PK_NewTenderDetailID` int(11) NOT NULL,
  `FK_NewTenderID` int(11) NOT NULL,
  `iModuleID` int(11) NOT NULL,
  `iAccountID` int(11) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `fQuantity` double NOT NULL DEFAULT '0',
  `fExpectedPrice` double NOT NULL DEFAULT '0',
  `dExpectedDate` datetime(6) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL,
  `iJobID` int(11) NOT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `iEscalateGroupID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `iLineStatus` int(11) NOT NULL DEFAULT '0',
  `iIncidentID` int(11) DEFAULT '0',
  `iPOInvoiceID` int(11) DEFAULT '0',
  `fActualPrice` double DEFAULT '0',
  `fExchangeRate` double DEFAULT NULL,
  `fExpectedPriceForeign` double DEFAULT NULL,
  `fActualPriceForeign` double DEFAULT NULL,
  `dApprovalDate` datetime(6) DEFAULT NULL,
  `bHasParameters` tinyint(1) DEFAULT '0',
  `dLastModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iRequisitionLinesID` int(11) DEFAULT NULL,
  `cSector` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_newtenderparams`
--

DROP TABLE IF EXISTS `rfq_newtenderparams`;
CREATE TABLE `rfq_newtenderparams` (
  `PK_NewTenderParamID` int(11) NOT NULL,
  `FK_NewTenderDetailID` int(11) DEFAULT NULL,
  `FK_NewTenderID` int(11) DEFAULT NULL,
  `FK_TenderParamID` int(11) DEFAULT NULL,
  `cParamName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `IsMandatory` tinyint(1) DEFAULT NULL,
  `iScore` int(11) DEFAULT NULL,
  `cComment` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_notes`
--

DROP TABLE IF EXISTS `rfq_notes`;
CREATE TABLE `rfq_notes` (
  `iNoteID` int(11) NOT NULL,
  `iRequisitionID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `dDate` datetime(6) NOT NULL,
  `cNote` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `bStickyNote` tinyint(1) DEFAULT NULL,
  `cFunctionality` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_parametercriteria`
--

DROP TABLE IF EXISTS `rfq_parametercriteria`;
CREATE TABLE `rfq_parametercriteria` (
  `idRfqParam` int(11) NOT NULL,
  `cCriteria` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cDescription` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bRandomSelection` tinyint(1) DEFAULT '0',
  `iRandomSupplier` int(11) DEFAULT NULL,
  `iNoOfQuotes` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_iBranchID` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_ParameterCriteria_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_ParameterCriteria_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_ParameterCriteria_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_parameterudf`
--

DROP TABLE IF EXISTS `rfq_parameterudf`;
CREATE TABLE `rfq_parameterudf` (
  `idRfqParamUdf` int(11) NOT NULL,
  `fkParameterCriteriaId` int(11) DEFAULT NULL,
  `iUserDictId` int(11) DEFAULT NULL,
  `cFieldName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fWeightage` double DEFAULT NULL,
  `bWeightage` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_peoplelinks`
--

DROP TABLE IF EXISTS `rfq_peoplelinks`;
CREATE TABLE `rfq_peoplelinks` (
  `iPeopleLinks` int(11) NOT NULL,
  `iPeopleID` int(11) DEFAULT NULL,
  `iDebtorID` int(11) DEFAULT NULL,
  `dCreatedDate` datetime(6) DEFAULT NULL,
  `dModifiedDate` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_recordquotationparams`
--

DROP TABLE IF EXISTS `rfq_recordquotationparams`;
CREATE TABLE `rfq_recordquotationparams` (
  `PK_RecordQuotationParamID` int(11) NOT NULL,
  `FK_iRequisitionID` int(11) DEFAULT NULL,
  `FK_iSupplierID` int(11) DEFAULT NULL,
  `FK_QuotationParamID` int(11) DEFAULT NULL,
  `cParamName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iScore` int(11) NOT NULL,
  `iUserScore` int(11) DEFAULT NULL,
  `iPercent` double DEFAULT NULL,
  `cComment` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `IsMandatory` tinyint(1) DEFAULT NULL,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_recordtender`
--

DROP TABLE IF EXISTS `rfq_recordtender`;
CREATE TABLE `rfq_recordtender` (
  `PK_RecordTender` int(11) NOT NULL,
  `FK_NewTenderID` int(11) NOT NULL,
  `cTenderNo` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cTenderTitle` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `cTenderRefNo` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cDescription` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `dAnnouncementDate` datetime(6) NOT NULL,
  `dOpeningDate` datetime(6) NOT NULL,
  `dLastSubmissionDate` datetime(6) NOT NULL,
  `dCompletiondate` datetime(6) NOT NULL,
  `dSubmissiondate` datetime(6) NOT NULL,
  `fEarnestMoney` double DEFAULT NULL,
  `bIsEMDMandatory` tinyint(1) NOT NULL DEFAULT '0',
  `iSupplierID` int(11) NOT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `fTenderTotal` double DEFAULT NULL,
  `cTenderTerms` longtext CHARACTER SET utf8mb4,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `iRecordSavedBy` int(11) DEFAULT NULL,
  `iScoreEnteredBy` int(11) DEFAULT NULL,
  `iRecordEvaluatedBy` int(11) DEFAULT NULL,
  `dEvaluationdate` datetime(6) DEFAULT NULL,
  `cEvaluationComments` longtext CHARACTER SET utf8mb4,
  `RFQ_RecordTender_iBranchID` int(11) DEFAULT NULL,
  `RFQ_RecordTender_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_RecordTender_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_RecordTender_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_RecordTender_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_RecordTender_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_RecordTender_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_RecordTender_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_RecordTender_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_recordtenderdetails`
--

DROP TABLE IF EXISTS `rfq_recordtenderdetails`;
CREATE TABLE `rfq_recordtenderdetails` (
  `PK_RecordTenderDetailID` int(11) NOT NULL,
  `FK_RecordTenderID` int(11) NOT NULL,
  `iModuleID` int(11) NOT NULL,
  `iAccountID` int(11) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `fQuantity` double NOT NULL DEFAULT '0',
  `fQuotedQuantity` double DEFAULT NULL,
  `fExpectedPrice` double NOT NULL DEFAULT '0',
  `fQuotedPrice` double DEFAULT NULL,
  `dExpectedDate` datetime(6) DEFAULT NULL,
  `dQuoteddate` datetime(6) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL,
  `iJobID` int(11) NOT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `iEscalateGroupID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `iLineStatus` int(11) NOT NULL DEFAULT '0',
  `iIncidentID` int(11) NOT NULL DEFAULT '0',
  `iPOInvoiceID` int(11) DEFAULT '0',
  `fActualPrice` double DEFAULT '0',
  `fExchangeRate` double DEFAULT NULL,
  `fExpectedPriceForeign` double DEFAULT NULL,
  `fActualPriceForeign` double DEFAULT NULL,
  `dApprovalDate` datetime(6) DEFAULT NULL,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `iRequisitionLinesID` int(11) DEFAULT NULL,
  `cEvaluationComments` longtext CHARACTER SET utf8mb4,
  `cSector` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_recordtenderparams`
--

DROP TABLE IF EXISTS `rfq_recordtenderparams`;
CREATE TABLE `rfq_recordtenderparams` (
  `PK_RecordTenderParamID` int(11) NOT NULL,
  `FK_RecordTenderID` int(11) DEFAULT NULL,
  `FK_RecordTenderDetailID` int(11) DEFAULT NULL,
  `FK_TenderParamID` int(11) DEFAULT NULL,
  `cParamName` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iScore` int(11) NOT NULL,
  `iUserScore` int(11) DEFAULT NULL,
  `iPercent` double DEFAULT NULL,
  `cComment` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `IsMandatory` tinyint(1) DEFAULT NULL,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_reportlayout`
--

DROP TABLE IF EXISTS `rfq_reportlayout`;
CREATE TABLE `rfq_reportlayout` (
  `idReportLayout` int(11) NOT NULL,
  `cRptDescription` varchar(80) NOT NULL,
  `iModuleId` int(11) DEFAULT NULL,
  `iVersion` int(11) DEFAULT NULL,
  `nLayout` longblob NOT NULL,
  `bReadOnly` tinyint(1) NOT NULL,
  `bIsDefaultLayout` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_sector`
--

DROP TABLE IF EXISTS `rfq_sector`;
CREATE TABLE `rfq_sector` (
  `idSector` int(11) NOT NULL,
  `cSector` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cDescription` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bAddedToUDF` tinyint(1) DEFAULT NULL,
  `RFq_sector_iBranchID` int(11) DEFAULT NULL,
  `RFq_sector_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFq_sector_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFq_sector_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFq_sector_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFq_sector_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFq_sector_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFq_sector_iChangeSetID` int(11) DEFAULT NULL,
  `RFq_sector_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_stocklinks`
--

DROP TABLE IF EXISTS `rfq_stocklinks`;
CREATE TABLE `rfq_stocklinks` (
  `idStockLinks` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iDCLink` int(11) NOT NULL,
  `bItemActive` tinyint(1) NOT NULL DEFAULT '1',
  `dCreatedDate` datetime(6) DEFAULT NULL,
  `dModifiedDate` datetime(6) DEFAULT NULL,
  `cProductReference` varchar(30) DEFAULT NULL,
  `cModule` varchar(2) DEFAULT NULL,
  `cSupInvCode` varchar(20) DEFAULT NULL,
  `iWhseID` int(11) DEFAULT NULL,
  `bDefaultSupplier` tinyint(1) NOT NULL DEFAULT '0',
  `bDCOnHold` tinyint(1) NOT NULL DEFAULT '0',
  `fLastGRVCost` double DEFAULT '0',
  `fManualCost` double NOT NULL DEFAULT '0',
  `fminOrderQuantity` double DEFAULT NULL,
  `iBranchID` int(11) DEFAULT NULL,
  `fLeadDays` double DEFAULT NULL,
  `dLastGRVCostDate` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_supplierfiltering`
--

DROP TABLE IF EXISTS `rfq_supplierfiltering`;
CREATE TABLE `rfq_supplierfiltering` (
  `idSupplierUDF` int(11) NOT NULL,
  `iUserDictID` int(11) NOT NULL,
  `cFieldName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bCriteria` tinyint(1) DEFAULT NULL,
  `bIsActive` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_supplierpreference`
--

DROP TABLE IF EXISTS `rfq_supplierpreference`;
CREATE TABLE `rfq_supplierpreference` (
  `idSupplierUDF` int(11) NOT NULL,
  `iUserDictID` int(11) NOT NULL,
  `cFieldName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fWeightage` double DEFAULT NULL,
  `bIsActive` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_tenderdf`
--

DROP TABLE IF EXISTS `rfq_tenderdf`;
CREATE TABLE `rfq_tenderdf` (
  `AutoNewTenderNo` tinyint(1) DEFAULT '1',
  `NewTenderPrefix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NextNewTenderNo` int(11) DEFAULT NULL,
  `NewTenderPadLength` int(11) DEFAULT NULL,
  `AutoRecordTenderNo` tinyint(1) DEFAULT '1',
  `RecordTenderPrefix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `NextRecordTenderNo` int(11) DEFAULT NULL,
  `RecordTenderPadLength` int(11) DEFAULT NULL,
  `PK_Default` int(11) NOT NULL,
  `RFQ_TenderDF_iBranchID` int(11) DEFAULT NULL,
  `RFQ_TenderDF_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_TenderDF_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_TenderDF_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_TenderDF_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_TenderDF_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_TenderDF_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_TenderDF_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_TenderDF_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_tenderparameters`
--

DROP TABLE IF EXISTS `rfq_tenderparameters`;
CREATE TABLE `rfq_tenderparameters` (
  `PK_TenderParamID` int(11) NOT NULL,
  `cParamName` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `cDescription` varchar(500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bIsMandatory` tinyint(1) NOT NULL DEFAULT '0',
  `bIsActive` tinyint(1) DEFAULT '1',
  `dLastModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `RFQ_TenderParameters_iBranchID` int(11) DEFAULT NULL,
  `RFQ_TenderParameters_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_TenderParameters_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_TenderParameters_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_TenderParameters_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_TenderParameters_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_TenderParameters_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_TenderParameters_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_TenderParameters_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_udf`
--

DROP TABLE IF EXISTS `rfq_udf`;
CREATE TABLE `rfq_udf` (
  `idUDF` int(11) NOT NULL,
  `iVendorID` int(11) NOT NULL,
  `cFieldName` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `cValue` longtext CHARACTER SET utf8mb4 NOT NULL,
  `dModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_vendor`
--

DROP TABLE IF EXISTS `rfq_vendor`;
CREATE TABLE `rfq_vendor` (
  `idVendor` int(11) NOT NULL,
  `cVendorAccount` varchar(20) NOT NULL,
  `cVendorName` varchar(50) DEFAULT NULL,
  `cDescription` varchar(80) DEFAULT NULL,
  `cVendorTitle` varchar(5) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cInit` varchar(6) DEFAULT NULL,
  `bApproved` tinyint(1) DEFAULT '0',
  `bRejected` tinyint(1) DEFAULT '0',
  `bExportToEvo` tinyint(1) DEFAULT '0',
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `iClassID` int(11) DEFAULT NULL,
  `iAreasID` int(11) DEFAULT NULL,
  `BFOpen` int(11) DEFAULT NULL,
  `CheckTerms` tinyint(1) NOT NULL DEFAULT '0',
  `CT` tinyint(1) NOT NULL DEFAULT '0',
  `Tax_Number` varchar(50) DEFAULT NULL,
  `Registration` varchar(20) DEFAULT NULL,
  `iAgeingTermID` int(11) DEFAULT NULL,
  `AccountTerms` int(11) DEFAULT NULL,
  `Credit_Limit` double DEFAULT NULL,
  `Interest_Rate` double DEFAULT NULL,
  `iSettlementTermsID` int(11) DEFAULT NULL,
  `Discount` double DEFAULT NULL,
  `AutoDisc` double DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `bForCurAcc` tinyint(1) NOT NULL DEFAULT '0',
  `iCurrencyID` int(11) DEFAULT NULL,
  `Contact_Person` varchar(30) DEFAULT NULL,
  `Delivered_To` varchar(30) DEFAULT NULL,
  `Fax1` varchar(25) DEFAULT NULL,
  `Fax2` varchar(25) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `Telephone` varchar(25) DEFAULT NULL,
  `Telephone2` varchar(25) DEFAULT NULL,
  `Addressee` varchar(30) DEFAULT NULL,
  `EMail` varchar(60) DEFAULT NULL,
  `Post1` varchar(40) DEFAULT NULL,
  `Post2` varchar(40) DEFAULT NULL,
  `Post3` varchar(40) DEFAULT NULL,
  `Post4` varchar(40) DEFAULT NULL,
  `Post5` varchar(40) DEFAULT NULL,
  `PostPC` varchar(15) DEFAULT NULL,
  `Physical1` varchar(40) DEFAULT NULL,
  `Physical2` varchar(40) DEFAULT NULL,
  `Physical3` varchar(40) DEFAULT NULL,
  `Physical4` varchar(40) DEFAULT NULL,
  `Physical5` varchar(40) DEFAULT NULL,
  `PhysicalPC` varchar(15) DEFAULT NULL,
  `RFQ_Vendor_iBranchID` int(11) DEFAULT NULL,
  `RFQ_Vendor_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_Vendor_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_Vendor_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_Vendor_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_Vendor_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_Vendor_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_Vendor_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_Vendor_Checksum` binary(20) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `bStatPrint` tinyint(1) DEFAULT NULL,
  `bStatEmail` tinyint(1) DEFAULT NULL,
  `bSourceDocPrint` tinyint(1) DEFAULT NULL,
  `bSourceDocEmail` tinyint(1) DEFAULT NULL,
  `cStatEmailPass` varchar(20) DEFAULT NULL,
  `bRemittanceChequeEFTS` tinyint(1) DEFAULT NULL,
  `iDefTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `cIDNumber` varchar(20) DEFAULT NULL,
  `cPassportNumber` varchar(20) DEFAULT NULL,
  `cBankAccHolder` varchar(30) DEFAULT NULL,
  `cBankCode` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_vendorparameter`
--

DROP TABLE IF EXISTS `rfq_vendorparameter`;
CREATE TABLE `rfq_vendorparameter` (
  `VendorParameterID` int(11) NOT NULL,
  `cParameterName` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `cDescription` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bIsMandatory` tinyint(1) DEFAULT '0',
  `bIsActive` tinyint(1) DEFAULT '0',
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `RFQ_VendorParameter_iBranchID` int(11) DEFAULT NULL,
  `RFQ_VendorParameter_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_VendorParameter_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_VendorParameter_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_VendorParameter_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_VendorParameter_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_VendorParameter_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_VendorParameter_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_VendorParameter_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_vendorscore`
--

DROP TABLE IF EXISTS `rfq_vendorscore`;
CREATE TABLE `rfq_vendorscore` (
  `idVendorScore` int(11) NOT NULL,
  `iVendorID` int(11) NOT NULL,
  `iParameterID` int(11) NOT NULL,
  `cScore` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `dLastModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `RFQ_VendorScore_iBranchID` int(11) DEFAULT NULL,
  `RFQ_VendorScore_dCreatedDate` datetime(6) DEFAULT NULL,
  `RFQ_VendorScore_dModifiedDate` datetime(6) DEFAULT NULL,
  `RFQ_VendorScore_iCreatedBranchID` int(11) DEFAULT NULL,
  `RFQ_VendorScore_iModifiedBranchID` int(11) DEFAULT NULL,
  `RFQ_VendorScore_iCreatedAgentID` int(11) DEFAULT NULL,
  `RFQ_VendorScore_iModifiedAgentID` int(11) DEFAULT NULL,
  `RFQ_VendorScore_iChangeSetID` int(11) DEFAULT NULL,
  `RFQ_VendorScore_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_workflowlink`
--

DROP TABLE IF EXISTS `rfq_workflowlink`;
CREATE TABLE `rfq_workflowlink` (
  `iWorkflowLinkID` int(11) NOT NULL,
  `iWorkflowMemberID` int(11) NOT NULL,
  `iEventID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesrep`
--

DROP TABLE IF EXISTS `salesrep`;
CREATE TABLE `salesrep` (
  `idSalesRep` int(11) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `Method` smallint(6) DEFAULT NULL,
  `Target1` double DEFAULT NULL,
  `Commission1` double DEFAULT NULL,
  `Target2` double DEFAULT NULL,
  `Commission2` double DEFAULT NULL,
  `Target3` double DEFAULT NULL,
  `Commission3` double DEFAULT NULL,
  `Target4` double DEFAULT NULL,
  `Commission4` double DEFAULT NULL,
  `Target5` double DEFAULT NULL,
  `Commission5` double DEFAULT NULL,
  `Address1` varchar(40) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Entity` varchar(40) DEFAULT NULL,
  `Rep_On_Hold` varchar(1) DEFAULT NULL,
  `Bank_Account` varchar(40) DEFAULT NULL,
  `Comment1` varchar(80) DEFAULT NULL,
  `Comment2` varchar(80) DEFAULT NULL,
  `SalesRep_iBranchID` int(11) DEFAULT NULL,
  `SalesRep_dCreatedDate` datetime(6) DEFAULT NULL,
  `SalesRep_dModifiedDate` datetime(6) DEFAULT NULL,
  `SalesRep_iCreatedBranchID` int(11) DEFAULT NULL,
  `SalesRep_iModifiedBranchID` int(11) DEFAULT NULL,
  `SalesRep_iCreatedAgentID` int(11) DEFAULT NULL,
  `SalesRep_iModifiedAgentID` int(11) DEFAULT NULL,
  `SalesRep_iChangeSetID` int(11) DEFAULT NULL,
  `SalesRep_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `serialmf`
--

DROP TABLE IF EXISTS `serialmf`;
CREATE TABLE `serialmf` (
  `SerialCounter` int(11) NOT NULL,
  `SerialNumber` varchar(30) DEFAULT NULL,
  `SNStockLink` int(11) DEFAULT NULL,
  `SNDateLMove` datetime DEFAULT NULL,
  `CurrentLoc` int(11) DEFAULT NULL,
  `CurrentAccLink` int(11) DEFAULT NULL,
  `iSNLotID` int(11) DEFAULT NULL,
  `iSNMFPID` int(11) DEFAULT NULL,
  `iSNMFPLineID` bigint(20) NOT NULL DEFAULT '0',
  `SerialMF_iBranchID` int(11) DEFAULT NULL,
  `SerialMF_dCreatedDate` datetime(6) DEFAULT NULL,
  `SerialMF_dModifiedDate` datetime(6) DEFAULT NULL,
  `SerialMF_iCreatedBranchID` int(11) DEFAULT NULL,
  `SerialMF_iModifiedBranchID` int(11) DEFAULT NULL,
  `SerialMF_iCreatedAgentID` int(11) DEFAULT NULL,
  `SerialMF_iModifiedAgentID` int(11) DEFAULT NULL,
  `SerialMF_iChangeSetID` int(11) DEFAULT NULL,
  `SerialMF_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `serialtx`
--

DROP TABLE IF EXISTS `serialtx`;
CREATE TABLE `serialtx` (
  `SNTxCounter` bigint(20) NOT NULL,
  `SNLink` int(11) DEFAULT NULL,
  `SNTxDate` datetime DEFAULT NULL,
  `SNTxAccLink` int(11) DEFAULT NULL,
  `SNTxReference` varchar(50) DEFAULT NULL,
  `SNTrCodeID` int(11) DEFAULT NULL,
  `SNAccModule` varchar(5) DEFAULT NULL,
  `SNTransType` int(11) DEFAULT NULL,
  `SNProjectLink` int(11) DEFAULT NULL,
  `SNWarehouseID` int(11) DEFAULT NULL,
  `SNAuditNumber` varchar(50) DEFAULT NULL,
  `cSNTXReference2` varchar(20) DEFAULT NULL,
  `iTxBranchID` int(11) DEFAULT NULL,
  `SerialTX_iBranchID` int(11) DEFAULT NULL,
  `SerialTX_dCreatedDate` datetime(6) DEFAULT NULL,
  `SerialTX_dModifiedDate` datetime(6) DEFAULT NULL,
  `SerialTX_iCreatedBranchID` int(11) DEFAULT NULL,
  `SerialTX_iModifiedBranchID` int(11) DEFAULT NULL,
  `SerialTX_iCreatedAgentID` int(11) DEFAULT NULL,
  `SerialTX_iModifiedAgentID` int(11) DEFAULT NULL,
  `SerialTX_iChangeSetID` int(11) DEFAULT NULL,
  `SerialTX_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `default` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simplesettings`
--

DROP TABLE IF EXISTS `simplesettings`;
CREATE TABLE `simplesettings` (
  `SimpleSettingID` int(11) NOT NULL,
  `Name` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Value` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `AgentID` int(11) NOT NULL,
  `ApplicationName` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Created` datetime(6) NOT NULL,
  `Modified` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sliplay`
--

DROP TABLE IF EXISTS `sliplay`;
CREATE TABLE `sliplay` (
  `IDSlipLay` int(11) NOT NULL,
  `LayIdentifier` varchar(6) NOT NULL,
  `LayDefHeader` longtext,
  `LayDefBody` longtext,
  `LayDefFooter` longtext,
  `LayHeader` longtext,
  `LayBody` longtext,
  `LayFooter` longtext,
  `SlipLay_iBranchID` int(11) DEFAULT NULL,
  `SlipLay_dCreatedDate` datetime(6) DEFAULT NULL,
  `SlipLay_dModifiedDate` datetime(6) DEFAULT NULL,
  `SlipLay_iCreatedBranchID` int(11) DEFAULT NULL,
  `SlipLay_iModifiedBranchID` int(11) DEFAULT NULL,
  `SlipLay_iCreatedAgentID` int(11) DEFAULT NULL,
  `SlipLay_iModifiedAgentID` int(11) DEFAULT NULL,
  `SlipLay_iChangeSetID` int(11) DEFAULT NULL,
  `SlipLay_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stdftbl`
--

DROP TABLE IF EXISTS `stdftbl`;
CREATE TABLE `stdftbl` (
  `idStDfTbl` int(11) NOT NULL,
  `AutoInvNum` tinyint(1) DEFAULT '0',
  `InvPref` varchar(20) DEFAULT NULL,
  `InvNum` varchar(15) DEFAULT NULL,
  `InvTrCodeID` int(11) DEFAULT NULL,
  `AutoRtsNum` tinyint(1) DEFAULT NULL,
  `RtsPref` varchar(20) DEFAULT NULL,
  `RtsNum` varchar(15) DEFAULT NULL,
  `RtsTrCodeID` int(11) DEFAULT NULL,
  `AutoCrnNum` tinyint(1) DEFAULT NULL,
  `CrnPref` varchar(20) DEFAULT NULL,
  `CrnNum` varchar(15) DEFAULT NULL,
  `CrnTrCodeID` int(11) DEFAULT NULL,
  `AutoGrvNum` tinyint(1) DEFAULT NULL,
  `GrvPref` varchar(20) DEFAULT NULL,
  `GrvNum` varchar(15) DEFAULT NULL,
  `bInvGrvSplit` tinyint(1) DEFAULT '0',
  `SInvTrCodeID` int(11) DEFAULT NULL,
  `GrvTrCodeID` int(11) DEFAULT NULL,
  `SInvGLVarAccID` int(11) DEFAULT NULL,
  `AdjTrCodeID` int(11) DEFAULT NULL,
  `Neg_Qty` varchar(1) DEFAULT NULL,
  `Decimals` smallint(6) DEFAULT NULL,
  `QuantDec` smallint(6) DEFAULT NULL,
  `GRVNumOpt` varchar(1) DEFAULT NULL,
  `ShowInactive` varchar(1) DEFAULT NULL,
  `InvAutoDisc` varchar(1) DEFAULT NULL,
  `CrnAutoDisc` varchar(1) DEFAULT NULL,
  `GrvAutoDisc` varchar(1) DEFAULT NULL,
  `RtsAutoDisc` varchar(1) DEFAULT NULL,
  `SerialPerLine` smallint(6) DEFAULT NULL,
  `GrvSplitTaxTypeID` int(11) DEFAULT NULL,
  `RTSNumOpt` varchar(1) DEFAULT NULL,
  `AutoCode` tinyint(1) NOT NULL DEFAULT '0',
  `AutoCodeLength` int(11) DEFAULT NULL,
  `AutoCodeAlphaLength` int(11) DEFAULT NULL,
  `FilterStartLength` int(11) DEFAULT '0',
  `DefaultAdjDesc` tinyint(1) DEFAULT '0',
  `InvCountAuto` tinyint(1) DEFAULT NULL,
  `InvCountPrefix` varchar(20) DEFAULT NULL,
  `InvCountNum` varchar(15) DEFAULT NULL,
  `InvPad` int(11) DEFAULT NULL,
  `RTSPad` int(11) DEFAULT NULL,
  `CRNPad` int(11) DEFAULT NULL,
  `GRVPad` int(11) DEFAULT NULL,
  `AutoCodeNum` int(11) DEFAULT NULL,
  `AutoCodePref` varchar(3) DEFAULT NULL,
  `InvCountPad` int(11) DEFAULT NULL,
  `AutoQuoteNum` tinyint(1) DEFAULT NULL,
  `QuotePref` varchar(20) DEFAULT NULL,
  `QuoteNum` int(11) DEFAULT NULL,
  `QuotePad` int(11) DEFAULT NULL,
  `AutoTemplateNum` tinyint(1) DEFAULT NULL,
  `TemplatePref` varchar(20) DEFAULT NULL,
  `TemplateNum` int(11) DEFAULT NULL,
  `TemplatePad` int(11) DEFAULT NULL,
  `iINVTaxTypeID` int(11) DEFAULT NULL,
  `iCRNTaxTypeID` int(11) DEFAULT NULL,
  `iGRVTaxTypeID` int(11) DEFAULT NULL,
  `iRTSTaxTypeID` int(11) NOT NULL,
  `iPaymentTrCodesID` int(11) DEFAULT NULL,
  `iSerialFilterStLength` int(11) DEFAULT NULL,
  `bSerialFilterStart` tinyint(1) DEFAULT NULL,
  `bSerialFilterProcessing` tinyint(1) DEFAULT NULL,
  `bGrvSplitUseExemptTax` tinyint(1) NOT NULL DEFAULT '1',
  `iInvSeg1TypeID` int(11) DEFAULT NULL,
  `iInvSeg2TypeID` int(11) DEFAULT NULL,
  `iInvSeg3TypeID` int(11) DEFAULT NULL,
  `iInvSeg4TypeID` int(11) DEFAULT NULL,
  `iInvSeg5TypeID` int(11) DEFAULT NULL,
  `iInvSeg6TypeID` int(11) DEFAULT NULL,
  `iInvSeg7TypeID` int(11) DEFAULT NULL,
  `bUseInvSeg` tinyint(1) NOT NULL DEFAULT '0',
  `bPostRepPerLine` tinyint(1) NOT NULL DEFAULT '0',
  `iCostDecimals` smallint(6) DEFAULT NULL,
  `bPostProjectPerLine` tinyint(1) NOT NULL DEFAULT '0',
  `iDefaultLotStatus` int(11) DEFAULT NULL,
  `bUseUpperItemCode` tinyint(1) NOT NULL DEFAULT '0',
  `iPriceListNameID1` int(11) DEFAULT NULL,
  `iPriceListNameID2` int(11) DEFAULT NULL,
  `iPriceListNameID3` int(11) DEFAULT NULL,
  `bNegQtyWarn` tinyint(1) NOT NULL DEFAULT '0',
  `bIsPerpetual` tinyint(1) NOT NULL DEFAULT '1',
  `bIsInclusive` tinyint(1) NOT NULL DEFAULT '1',
  `iCostingMethod` int(11) NOT NULL DEFAULT '0',
  `iDefStockCostVarianceAccID` int(11) NOT NULL DEFAULT '0',
  `bCostPerWarehouse` tinyint(1) NOT NULL DEFAULT '0',
  `bInvJrBatchAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cInvJrBatchPrefix` varchar(20) DEFAULT NULL,
  `iInvJrBatchPadLength` int(11) DEFAULT NULL,
  `bInvJrRefAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cInvJrRefPrefix` varchar(20) DEFAULT NULL,
  `iInvJrRefPadLength` int(11) DEFAULT NULL,
  `iInvJrTrCodeID` int(11) DEFAULT NULL,
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `bForceRep` tinyint(1) NOT NULL DEFAULT '0',
  `bArchiveQuotes` tinyint(1) NOT NULL DEFAULT '0',
  `bSerialProcessActLookup` tinyint(1) NOT NULL DEFAULT '0',
  `bInvLinePostDocDesc` tinyint(1) NOT NULL DEFAULT '1',
  `bFinLinePostDocDesc` tinyint(1) NOT NULL DEFAULT '1',
  `bForceExtOrderNum` tinyint(1) NOT NULL DEFAULT '0',
  `bForceSupplierInvNumber` tinyint(1) NOT NULL DEFAULT '0',
  `bUniqueInvNum` tinyint(1) DEFAULT NULL,
  `bUniqueRtsNum` tinyint(1) DEFAULT NULL,
  `bUniqueCrnNum` tinyint(1) DEFAULT NULL,
  `bUniqueGrvNum` tinyint(1) DEFAULT NULL,
  `bUniqueQuoteNum` tinyint(1) DEFAULT NULL,
  `bUniqueTemplateNum` tinyint(1) DEFAULT NULL,
  `bUniqueInvCountNum` tinyint(1) DEFAULT NULL,
  `bUniqueExtOrderNum` tinyint(1) DEFAULT NULL,
  `bUniqueSupplierInvNum` tinyint(1) DEFAULT NULL,
  `cCostPriceEncodingKey` varchar(11) NOT NULL DEFAULT 'ABCDFGHJKL.',
  `fDefaultGPPercent` float(24,0) DEFAULT NULL,
  `bAllowPOSTender` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowNegStockDef` tinyint(1) NOT NULL DEFAULT '0',
  `bCalculateTaxPerLine` tinyint(1) NOT NULL DEFAULT '1',
  `iInvCountAgentID` int(11) DEFAULT NULL,
  `iInvCountPromptOpt` int(11) DEFAULT NULL,
  `dInvCountLastPrompt` datetime(6) DEFAULT NULL,
  `bInvPrcUpdBatchAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cInvPrcUpdBatchPrefix` varchar(20) DEFAULT NULL,
  `iInvPrcUpdBatchPadLength` int(11) DEFAULT NULL,
  `bAutoLotAlloc` tinyint(1) NOT NULL DEFAULT '0',
  `iAutoLotAllocType` int(11) DEFAULT NULL,
  `bInvPrcUpdRefAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cInvPrcUpdRefPrefix` varchar(20) DEFAULT NULL,
  `iInvPrcUpdRefPadLength` int(11) DEFAULT NULL,
  `bPostDeliveryPerLine` tinyint(1) NOT NULL DEFAULT '0',
  `bAddToDeliveryDate` tinyint(1) NOT NULL DEFAULT '0',
  `iAddToDeliveryType` int(11) DEFAULT NULL,
  `iAddDeliveryDays` int(11) DEFAULT NULL,
  `bForceDeliveryINV` tinyint(1) NOT NULL DEFAULT '0',
  `bForceDeliveryCRN` tinyint(1) NOT NULL DEFAULT '0',
  `bForceDeliveryGRV` tinyint(1) NOT NULL DEFAULT '0',
  `bForceDeliveryRTS` tinyint(1) NOT NULL DEFAULT '0',
  `bPostCostVarianceGLLine` tinyint(1) NOT NULL DEFAULT '1',
  `bUseDimensions` tinyint(1) DEFAULT '0',
  `bSellUsingDimensions` tinyint(1) DEFAULT '0',
  `bBuyUsingDimensions` tinyint(1) DEFAULT '0',
  `cDefMeasurement` varchar(5) DEFAULT '0',
  `cMeasurementRounding` varchar(5) DEFAULT '0',
  `cRoundingOption` varchar(20) DEFAULT '0',
  `bUseSalesOrders` tinyint(1) NOT NULL DEFAULT '0',
  `bUseUOM` tinyint(1) NOT NULL DEFAULT '1',
  `bInvIssueSplit` tinyint(1) NOT NULL DEFAULT '0',
  `iInvIssueSplitAccrualAccID` int(11) DEFAULT NULL,
  `bAutoGIVNumber` tinyint(1) NOT NULL DEFAULT '1',
  `bUniqueGIVNum` tinyint(1) NOT NULL DEFAULT '0',
  `cGIVPrefix` varchar(20) DEFAULT NULL,
  `cGIVNumber` varchar(15) DEFAULT NULL,
  `iGIVPad` int(11) DEFAULT NULL,
  `bAutoCGRNumber` tinyint(1) NOT NULL DEFAULT '1',
  `bUniqueCGRNum` tinyint(1) NOT NULL DEFAULT '0',
  `cCGRPrefix` varchar(20) DEFAULT NULL,
  `cCGRNumber` varchar(15) DEFAULT NULL,
  `iCGRPad` int(11) DEFAULT NULL,
  `bRTSAverageCostVariance` tinyint(1) NOT NULL DEFAULT '0',
  `StDfTbl_iBranchID` int(11) DEFAULT NULL,
  `StDfTbl_dCreatedDate` datetime(6) DEFAULT NULL,
  `StDfTbl_dModifiedDate` datetime(6) DEFAULT NULL,
  `StDfTbl_iCreatedBranchID` int(11) DEFAULT NULL,
  `StDfTbl_iModifiedBranchID` int(11) DEFAULT NULL,
  `StDfTbl_iCreatedAgentID` int(11) DEFAULT NULL,
  `StDfTbl_iModifiedAgentID` int(11) DEFAULT NULL,
  `StDfTbl_iChangeSetID` int(11) DEFAULT NULL,
  `StDfTbl_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stkitem`
--

DROP TABLE IF EXISTS `stkitem`;
CREATE TABLE `stkitem` (
  `StockLink` int(11) NOT NULL,
  `Code` varchar(400) DEFAULT NULL,
  `Description_1` varchar(50) DEFAULT NULL,
  `Description_2` varchar(50) DEFAULT NULL,
  `Description_3` varchar(50) DEFAULT NULL,
  `ItemGroup` varchar(20) DEFAULT NULL,
  `Pack` varchar(5) DEFAULT NULL,
  `TTI` varchar(10) DEFAULT NULL,
  `TTC` varchar(10) DEFAULT NULL,
  `TTG` varchar(10) DEFAULT NULL,
  `TTR` varchar(10) DEFAULT NULL,
  `Bar_Code` varchar(400) DEFAULT NULL,
  `Re_Ord_Lvl` double DEFAULT NULL,
  `Re_Ord_Qty` double DEFAULT NULL,
  `Min_Lvl` double DEFAULT NULL,
  `Max_Lvl` double DEFAULT NULL,
  `AveUCst` double NOT NULL DEFAULT '0',
  `LatUCst` double NOT NULL DEFAULT '0',
  `LowUCst` double NOT NULL DEFAULT '0',
  `HigUCst` double NOT NULL DEFAULT '0',
  `StdUCst` double NOT NULL DEFAULT '0',
  `Qty_On_Hand` double NOT NULL DEFAULT '0',
  `LGrvCount` double DEFAULT NULL,
  `ServiceItem` tinyint(1) NOT NULL DEFAULT '0',
  `ItemActive` tinyint(1) NOT NULL DEFAULT '1',
  `ReservedQty` double NOT NULL DEFAULT '0',
  `QtyOnPO` double NOT NULL DEFAULT '0',
  `QtyOnSO` double NOT NULL DEFAULT '0',
  `WhseItem` tinyint(1) NOT NULL DEFAULT '0',
  `SerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `DuplicateSN` tinyint(1) NOT NULL DEFAULT '0',
  `StrictSN` tinyint(1) NOT NULL DEFAULT '0',
  `BomCode` varchar(1) DEFAULT NULL,
  `SMtrxCol` int(11) DEFAULT NULL,
  `PMtrxCol` int(11) DEFAULT NULL,
  `JobQty` double NOT NULL DEFAULT '0',
  `cModel` varchar(50) DEFAULT NULL,
  `cRevision` varchar(50) DEFAULT NULL,
  `cComponent` varchar(50) DEFAULT NULL,
  `dDateReleased` datetime DEFAULT NULL,
  `iBinLocationID` int(11) DEFAULT NULL,
  `dStkitemTimeStamp` datetime(6) DEFAULT NULL,
  `iInvSegValue1ID` int(11) DEFAULT NULL,
  `iInvSegValue2ID` int(11) DEFAULT NULL,
  `iInvSegValue3ID` int(11) DEFAULT NULL,
  `iInvSegValue4ID` int(11) DEFAULT NULL,
  `iInvSegValue5ID` int(11) DEFAULT NULL,
  `iInvSegValue6ID` int(11) DEFAULT NULL,
  `iInvSegValue7ID` int(11) DEFAULT NULL,
  `cExtDescription` varchar(255) DEFAULT NULL,
  `cSimpleCode` varchar(20) DEFAULT NULL,
  `bCommissionItem` tinyint(1) NOT NULL DEFAULT '1',
  `MFPQty` double NOT NULL DEFAULT '0',
  `bLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iLotStatus` int(11) DEFAULT NULL,
  `bLotMustExpire` tinyint(1) NOT NULL DEFAULT '1',
  `iItemCostingMethod` int(11) NOT NULL DEFAULT '0',
  `fItemLastGRVCost` double NOT NULL DEFAULT '0',
  `iEUCommodityID` int(11) NOT NULL DEFAULT '0',
  `iEUSupplementaryUnitID` int(11) NOT NULL DEFAULT '0',
  `fNetMass` double NOT NULL DEFAULT '0',
  `iUOMStockingUnitID` int(11) DEFAULT NULL,
  `iUOMDefPurchaseUnitID` int(11) DEFAULT NULL,
  `iUOMDefSellUnitID` int(11) DEFAULT NULL,
  `fStockGPPercent` float(24,0) DEFAULT NULL,
  `bAllowNegStock` tinyint(1) NOT NULL DEFAULT '0',
  `fQtyToDeliver` double DEFAULT NULL,
  `StkItem_fLeadDays` double DEFAULT NULL,
  `cEachDescription` varchar(30) DEFAULT '0',
  `cMeasurement` varchar(5) DEFAULT '0',
  `fBuyLength` double DEFAULT '0',
  `fBuyWidth` double DEFAULT '0',
  `fBuyHeight` double DEFAULT '0',
  `fBuyArea` double DEFAULT '0',
  `fBuyVolume` double DEFAULT '0',
  `cBuyWeight` double DEFAULT '0',
  `cBuyUnit` varchar(5) DEFAULT '0',
  `fSellLength` double DEFAULT '0',
  `fSellWidth` double DEFAULT '0',
  `fSellHeight` double DEFAULT '0',
  `fSellArea` double DEFAULT '0',
  `fSellVolume` double DEFAULT '0',
  `cSellWeight` double DEFAULT '0',
  `cSellUnit` varchar(5) DEFAULT '0',
  `bOverrideSell` tinyint(1) DEFAULT '0',
  `bUOMItem` tinyint(1) NOT NULL DEFAULT '0',
  `bDimensionItem` tinyint(1) NOT NULL DEFAULT '0',
  `iBuyingAgentID` int(11) DEFAULT NULL,
  `bVASItem` tinyint(1) NOT NULL DEFAULT '0',
  `bAirtimeItem` tinyint(1) NOT NULL DEFAULT '0',
  `fIBTQtyToIssue` double NOT NULL DEFAULT '0',
  `fIBTQtyToReceive` double NOT NULL DEFAULT '0',
  `StkItem_iBranchID` int(11) DEFAULT NULL,
  `StkItem_dCreatedDate` datetime(6) DEFAULT NULL,
  `StkItem_dModifiedDate` datetime(6) DEFAULT NULL,
  `StkItem_iCreatedBranchID` int(11) DEFAULT NULL,
  `StkItem_iModifiedBranchID` int(11) DEFAULT NULL,
  `StkItem_iCreatedAgentID` int(11) DEFAULT NULL,
  `StkItem_iModifiedAgentID` int(11) DEFAULT NULL,
  `StkItem_iChangeSetID` int(11) DEFAULT NULL,
  `StkItem_Checksum` binary(20) DEFAULT NULL,
  `bSyncToSOT` tinyint(1) NOT NULL DEFAULT '0',
  `ucIIUnitWt` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taxrate`
--

DROP TABLE IF EXISTS `taxrate`;
CREATE TABLE `taxrate` (
  `idTaxRate` int(11) NOT NULL,
  `Code` varchar(10) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `TaxRate` double DEFAULT NULL,
  `TaxRate_iBranchID` int(11) DEFAULT NULL,
  `TaxRate_dCreatedDate` datetime(6) DEFAULT NULL,
  `TaxRate_dModifiedDate` datetime(6) DEFAULT NULL,
  `TaxRate_iCreatedBranchID` int(11) DEFAULT NULL,
  `TaxRate_iModifiedBranchID` int(11) DEFAULT NULL,
  `TaxRate_iCreatedAgentID` int(11) DEFAULT NULL,
  `TaxRate_iModifiedAgentID` int(11) DEFAULT NULL,
  `TaxRate_iChangeSetID` int(11) DEFAULT NULL,
  `TaxRate_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender`
--

DROP TABLE IF EXISTS `tender`;
CREATE TABLE `tender` (
  `IdTender` int(11) NOT NULL,
  `TenderNo` varchar(10) NOT NULL,
  `Description` varchar(30) DEFAULT NULL,
  `Force_Narrative` varchar(1) DEFAULT NULL,
  `House_Limit` double DEFAULT NULL,
  `iTrCodesIDInvPM` int(11) DEFAULT NULL,
  `iTrCodesIDCRNRF` int(11) DEFAULT NULL,
  `bUsePinPad` tinyint(1) DEFAULT NULL,
  `iCardDisplayFirst` int(11) DEFAULT NULL,
  `iCardDisplayLast` int(11) DEFAULT NULL,
  `bForceCardNumber` tinyint(1) DEFAULT NULL,
  `bForceCardHolder` tinyint(1) DEFAULT NULL,
  `cExpiryFormat` varchar(10) DEFAULT NULL,
  `bForceExpiry` tinyint(1) DEFAULT NULL,
  `bApplyDocumentRounding` tinyint(1) DEFAULT '0',
  `Tender_iBranchID` int(11) DEFAULT NULL,
  `Tender_dCreatedDate` datetime(6) DEFAULT NULL,
  `Tender_dModifiedDate` datetime(6) DEFAULT NULL,
  `Tender_iCreatedBranchID` int(11) DEFAULT NULL,
  `Tender_iModifiedBranchID` int(11) DEFAULT NULL,
  `Tender_iCreatedAgentID` int(11) DEFAULT NULL,
  `Tender_iModifiedAgentID` int(11) DEFAULT NULL,
  `Tender_iChangeSetID` int(11) DEFAULT NULL,
  `Tender_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tills`
--

DROP TABLE IF EXISTS `tills`;
CREATE TABLE `tills` (
  `IdTills` int(11) NOT NULL,
  `TillNo` varchar(4) NOT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iDeviceIDDisplay` int(11) DEFAULT NULL,
  `iDeviceIDDrawer` int(11) DEFAULT NULL,
  `iDeviceIDPrinter` int(11) DEFAULT NULL,
  `iDeviceIDFiscalPrinter` int(11) DEFAULT NULL,
  `iDeviceIDPinPad` int(11) DEFAULT NULL,
  `cTerminalID` varchar(20) DEFAULT NULL,
  `Tills_iBranchID` int(11) DEFAULT NULL,
  `Tills_dCreatedDate` datetime(6) DEFAULT NULL,
  `Tills_dModifiedDate` datetime(6) DEFAULT NULL,
  `Tills_iCreatedBranchID` int(11) DEFAULT NULL,
  `Tills_iModifiedBranchID` int(11) DEFAULT NULL,
  `Tills_iCreatedAgentID` int(11) DEFAULT NULL,
  `Tills_iModifiedAgentID` int(11) DEFAULT NULL,
  `Tills_iChangeSetID` int(11) DEFAULT NULL,
  `Tills_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `transaction_number` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `original_transaction_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `cashbook_id` int(11) DEFAULT NULL,
  `petty_cash_id` int(11) DEFAULT NULL,
  `transaction_type` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `payment_type` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `amount` double NOT NULL,
  `transaction_data` longtext CHARACTER SET utf8mb4,
  `reversed` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `printed` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trcodes`
--

DROP TABLE IF EXISTS `trcodes`;
CREATE TABLE `trcodes` (
  `idTrCodes` int(11) NOT NULL,
  `iModule` int(11) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `LinkID` int(11) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `DebitTrans` tinyint(1) NOT NULL DEFAULT '1',
  `Tax` tinyint(1) NOT NULL DEFAULT '0',
  `Rep` tinyint(1) NOT NULL DEFAULT '0',
  `Account1Link` int(11) DEFAULT NULL,
  `Account2Link` int(11) DEFAULT NULL,
  `TaxAccountLink` int(11) DEFAULT NULL,
  `GLPrompt` tinyint(1) NOT NULL DEFAULT '0',
  `TaxTypeID` int(11) DEFAULT NULL,
  `SplitTr` tinyint(1) NOT NULL DEFAULT '0',
  `bSalesFilter` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowSubAccTrans` tinyint(1) NOT NULL DEFAULT '0',
  `bSettlementDisc` tinyint(1) NOT NULL DEFAULT '0',
  `iDtTaxGroupID` int(11) DEFAULT NULL,
  `iCtTaxGroupID` int(11) DEFAULT NULL,
  `iTaxGroupID` int(11) DEFAULT NULL,
  `iMBServiceID` int(11) NOT NULL DEFAULT '0',
  `TrCodes_iBranchID` int(11) DEFAULT NULL,
  `TrCodes_dCreatedDate` datetime(6) DEFAULT NULL,
  `TrCodes_dModifiedDate` datetime(6) DEFAULT NULL,
  `TrCodes_iCreatedBranchID` int(11) DEFAULT NULL,
  `TrCodes_iModifiedBranchID` int(11) DEFAULT NULL,
  `TrCodes_iCreatedAgentID` int(11) DEFAULT NULL,
  `TrCodes_iModifiedAgentID` int(11) DEFAULT NULL,
  `TrCodes_iChangeSetID` int(11) DEFAULT NULL,
  `TrCodes_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `venclass`
--

DROP TABLE IF EXISTS `venclass`;
CREATE TABLE `venclass` (
  `idVenClass` int(11) NOT NULL,
  `Code` varchar(20) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `dGroupTimeStamp` datetime(6) DEFAULT NULL,
  `iAccountsIDControlAcc` int(11) DEFAULT NULL,
  `iAccountsIDProfitAcc` int(11) DEFAULT NULL,
  `iAccountsIDLossAcc` int(11) DEFAULT NULL,
  `iTaxControlAccID` int(11) DEFAULT NULL,
  `iRevProfitAcc` int(11) DEFAULT NULL,
  `iRevLossAcc` int(11) DEFAULT NULL,
  `iProvForRevAcc` int(11) DEFAULT NULL,
  `iInvoiceDocProfileID` int(11) DEFAULT NULL,
  `iSOInvoiceDocProfileID` int(11) DEFAULT NULL,
  `iCreditNoteDocProfileID` int(11) DEFAULT NULL,
  `iJCInvoiceDocProfileID` int(11) DEFAULT NULL,
  `VenClass_iBranchID` int(11) DEFAULT NULL,
  `VenClass_dCreatedDate` datetime(6) DEFAULT NULL,
  `VenClass_dModifiedDate` datetime(6) DEFAULT NULL,
  `VenClass_iCreatedBranchID` int(11) DEFAULT NULL,
  `VenClass_iModifiedBranchID` int(11) DEFAULT NULL,
  `VenClass_iCreatedAgentID` int(11) DEFAULT NULL,
  `VenClass_iModifiedAgentID` int(11) DEFAULT NULL,
  `VenClass_iChangeSetID` int(11) DEFAULT NULL,
  `VenClass_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendef`
--

DROP TABLE IF EXISTS `vendef`;
CREATE TABLE `vendef` (
  `idVenDef` int(11) NOT NULL,
  `AutoYN` varchar(1) DEFAULT NULL,
  `AutoLength` int(11) DEFAULT NULL,
  `AutoAlphaLength` int(11) DEFAULT NULL,
  `UpperAccNo` varchar(1) DEFAULT NULL,
  `ForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `DefaultTxDesc` tinyint(1) NOT NULL DEFAULT '0',
  `LatestVDPrice` tinyint(1) NOT NULL DEFAULT '0',
  `FilterStartLength` int(11) DEFAULT NULL,
  `iTaxRateIdNoCharge` int(11) DEFAULT NULL,
  `iPEXTrCodeID` int(11) DEFAULT NULL,
  `iLEXTrCodeID` int(11) DEFAULT NULL,
  `fTaxPromptAmount` double DEFAULT NULL,
  `iDelAddressCodeID1` int(11) DEFAULT NULL,
  `iDelAddressCodeID2` int(11) DEFAULT NULL,
  `bUseAllocStoredProc` tinyint(1) NOT NULL DEFAULT '1',
  `bInvTxCheckAccAfterChange` tinyint(1) NOT NULL DEFAULT '0',
  `iEFTSLayoutID` int(11) NOT NULL DEFAULT '0',
  `cEFTSPathOutFile` varchar(100) DEFAULT NULL,
  `iDefaultTermID` int(11) DEFAULT NULL,
  `iRevProfitAcc` int(11) DEFAULT NULL,
  `iRevLossAcc` int(11) DEFAULT NULL,
  `iProvForRevAcc` int(11) DEFAULT NULL,
  `iRemitBatchManageOpt` int(11) NOT NULL DEFAULT '0',
  `VenDef_iBranchID` int(11) DEFAULT NULL,
  `VenDef_dCreatedDate` datetime(6) DEFAULT NULL,
  `VenDef_dModifiedDate` datetime(6) DEFAULT NULL,
  `VenDef_iCreatedBranchID` int(11) DEFAULT NULL,
  `VenDef_iModifiedBranchID` int(11) DEFAULT NULL,
  `VenDef_iCreatedAgentID` int(11) DEFAULT NULL,
  `VenDef_iModifiedAgentID` int(11) DEFAULT NULL,
  `VenDef_iChangeSetID` int(11) DEFAULT NULL,
  `VenDef_Checksum` binary(20) DEFAULT NULL,
  `iPDAgentID` int(11) NOT NULL DEFAULT '0',
  `iPDPromptOpt` int(11) NOT NULL DEFAULT '0',
  `dPDLastPrompt` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `DCLink` int(11) NOT NULL,
  `Account` varchar(20) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Title` varchar(5) DEFAULT NULL,
  `Init` varchar(6) DEFAULT NULL,
  `Contact_Person` varchar(30) DEFAULT NULL,
  `Physical1` varchar(40) DEFAULT NULL,
  `Physical2` varchar(40) DEFAULT NULL,
  `Physical3` varchar(40) DEFAULT NULL,
  `Physical4` varchar(40) DEFAULT NULL,
  `Physical5` varchar(40) DEFAULT NULL,
  `PhysicalPC` varchar(15) DEFAULT NULL,
  `Addressee` varchar(30) DEFAULT NULL,
  `Post1` varchar(40) DEFAULT NULL,
  `Post2` varchar(40) DEFAULT NULL,
  `Post3` varchar(40) DEFAULT NULL,
  `Post4` varchar(40) DEFAULT NULL,
  `Post5` varchar(40) DEFAULT NULL,
  `PostPC` varchar(15) DEFAULT NULL,
  `Delivered_To` varchar(30) DEFAULT NULL,
  `Telephone` varchar(25) DEFAULT NULL,
  `Telephone2` varchar(25) DEFAULT NULL,
  `Fax1` varchar(25) DEFAULT NULL,
  `Fax2` varchar(25) DEFAULT NULL,
  `AccountTerms` int(11) DEFAULT NULL,
  `CT` tinyint(1) NOT NULL DEFAULT '0',
  `Tax_Number` varchar(50) DEFAULT NULL,
  `Registration` varchar(20) DEFAULT NULL,
  `Credit_Limit` double DEFAULT NULL,
  `Interest_Rate` double DEFAULT NULL,
  `Discount` double DEFAULT NULL,
  `On_Hold` tinyint(1) NOT NULL DEFAULT '0',
  `BFOpenType` int(11) DEFAULT NULL,
  `EMail` varchar(200) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `AutoDisc` double DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `DCBalance` double DEFAULT NULL,
  `CheckTerms` tinyint(1) NOT NULL DEFAULT '0',
  `UseEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iBusTypeID` int(11) DEFAULT NULL,
  `iBusClassID` int(11) DEFAULT NULL,
  `iCountryID` int(11) DEFAULT NULL,
  `cAccDescription` varchar(80) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `dTimeStamp` datetime(6) DEFAULT NULL,
  `iClassID` int(11) DEFAULT NULL,
  `iAreasID` int(11) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bStatPrint` tinyint(1) NOT NULL DEFAULT '0',
  `bStatEmail` tinyint(1) NOT NULL DEFAULT '0',
  `cStatEmailPass` varchar(160) DEFAULT NULL,
  `bForCurAcc` tinyint(1) NOT NULL DEFAULT '0',
  `bRemittanceChequeEFTS` tinyint(1) NOT NULL DEFAULT '1',
  `fForeignBalance` double DEFAULT NULL,
  `iSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `bSourceDocPrint` tinyint(1) NOT NULL DEFAULT '1',
  `bSourceDocEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iEUCountryID` int(11) NOT NULL DEFAULT '0',
  `iDefTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `iAgeingTermID` int(11) DEFAULT NULL,
  `iBankDetailType` tinyint(3) UNSIGNED DEFAULT NULL,
  `cBankAccHolder` varchar(30) DEFAULT NULL,
  `cIDNumber` varchar(20) DEFAULT NULL,
  `cPassportNumber` varchar(20) DEFAULT NULL,
  `cBankCode` varchar(15) DEFAULT NULL,
  `cSwiftCode` varchar(11) DEFAULT NULL,
  `iBankDetailID` int(11) DEFAULT NULL,
  `Vendor_iBranchID` int(11) DEFAULT NULL,
  `Vendor_dCreatedDate` datetime(6) DEFAULT NULL,
  `Vendor_dModifiedDate` datetime(6) DEFAULT NULL,
  `Vendor_iCreatedBranchID` int(11) DEFAULT NULL,
  `Vendor_iModifiedBranchID` int(11) DEFAULT NULL,
  `Vendor_iCreatedAgentID` int(11) DEFAULT NULL,
  `Vendor_iModifiedAgentID` int(11) DEFAULT NULL,
  `Vendor_iChangeSetID` int(11) DEFAULT NULL,
  `Vendor_Checksum` binary(20) DEFAULT NULL,
  `iSPQueueID` int(11) DEFAULT NULL,
  `ulAPSector1` varchar(100) DEFAULT NULL,
  `ulAPSector2` varchar(100) DEFAULT NULL,
  `ulAPSector3` varchar(100) DEFAULT NULL,
  `ulAPSector4` varchar(100) DEFAULT NULL,
  `ulAPSector5` varchar(100) DEFAULT NULL,
  `ulAPSector6` varchar(100) DEFAULT NULL,
  `ulAPSector7` varchar(100) DEFAULT NULL,
  `ulAPSector8` varchar(100) DEFAULT NULL,
  `ulAPSector9` varchar(100) DEFAULT NULL,
  `ulAPSector10` varchar(100) DEFAULT NULL,
  `ulAPSupplierLocation` varchar(100) DEFAULT NULL,
  `udAPRegistrationDate` datetime(6) DEFAULT NULL,
  `ubAPBEECompliant` tinyint(1) DEFAULT NULL,
  `ulAPStatus` varchar(100) DEFAULT NULL,
  `ulAPDeliveryGoods` varchar(100) DEFAULT NULL,
  `ubAPFailToDeliver` tinyint(1) DEFAULT NULL,
  `ubAPWorkforGovt` tinyint(1) DEFAULT NULL,
  `ubAPBlacklistedbyGovt` tinyint(1) DEFAULT NULL,
  `ubAPConfirmCompany` tinyint(1) DEFAULT NULL,
  `ufAPHDI` double DEFAULT NULL,
  `ufAPFunctionality` double DEFAULT NULL,
  `ufAPExperience` double DEFAULT NULL,
  `ufAPWomen` double DEFAULT NULL,
  `ufAPWhite` double DEFAULT NULL,
  `ufAPMiscellaneous` double DEFAULT NULL,
  `ufAPPrice` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `whsemst`
--

DROP TABLE IF EXISTS `whsemst`;
CREATE TABLE `whsemst` (
  `WhseLink` int(11) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `KnownAs` varchar(50) DEFAULT NULL,
  `Address1` varchar(40) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `PostCode` varchar(15) DEFAULT NULL,
  `Tel` varchar(15) DEFAULT NULL,
  `Manager` varchar(30) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `EMail` varchar(60) DEFAULT NULL,
  `ModemTel` varchar(15) DEFAULT NULL,
  `DefaultWhse` tinyint(1) NOT NULL DEFAULT '0',
  `AddNewStock` tinyint(1) NOT NULL DEFAULT '0',
  `dWarehouseTimeStamp` datetime(6) DEFAULT NULL,
  `iWhseTypeID` int(11) NOT NULL DEFAULT '0',
  `bAllowToBuyInto` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowToSellFrom` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowNegStock` tinyint(1) NOT NULL DEFAULT '0',
  `cDefaultItemGroup` varchar(20) DEFAULT NULL,
  `WhseMst_iBranchID` int(11) DEFAULT NULL,
  `WhseMst_dCreatedDate` datetime(6) DEFAULT NULL,
  `WhseMst_dModifiedDate` datetime(6) DEFAULT NULL,
  `WhseMst_iCreatedBranchID` int(11) DEFAULT NULL,
  `WhseMst_iModifiedBranchID` int(11) DEFAULT NULL,
  `WhseMst_iCreatedAgentID` int(11) DEFAULT NULL,
  `WhseMst_iModifiedAgentID` int(11) DEFAULT NULL,
  `WhseMst_iChangeSetID` int(11) DEFAULT NULL,
  `WhseMst_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `whsestk`
--

DROP TABLE IF EXISTS `whsestk`;
CREATE TABLE `whsestk` (
  `IdWhseStk` bigint(20) NOT NULL,
  `WHWhseID` int(11) DEFAULT NULL,
  `WHStockLink` int(11) NOT NULL,
  `WHStockGroup` varchar(20) DEFAULT NULL,
  `WHQtyOnHand` double NOT NULL DEFAULT '0',
  `WHQtyOnSO` double NOT NULL DEFAULT '0',
  `WHQtyOnPO` double NOT NULL DEFAULT '0',
  `WHQtyReserved` double NOT NULL DEFAULT '0',
  `WHTTInv` varchar(10) DEFAULT NULL,
  `WHTTCrn` varchar(10) DEFAULT NULL,
  `WHTTGrv` varchar(10) DEFAULT NULL,
  `WHTTRts` varchar(10) DEFAULT NULL,
  `WHBarCode` varchar(400) DEFAULT NULL,
  `WHRe_Ord_Lvl` double DEFAULT NULL,
  `WHRe_Ord_Qty` double DEFAULT NULL,
  `WHMin_Lvl` double DEFAULT NULL,
  `WHMax_Lvl` double DEFAULT NULL,
  `WHUsePriceDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHUseInfoDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHUseOrderDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHUseDefaultDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHPackCode` varchar(5) DEFAULT NULL,
  `WHJobQty` double NOT NULL DEFAULT '0',
  `iBinLocationID` int(11) DEFAULT NULL,
  `fLGRVCount` double DEFAULT NULL,
  `WHMFPQty` double NOT NULL DEFAULT '0',
  `WHUseSupplierDefs` tinyint(1) NOT NULL DEFAULT '1',
  `fAverageCost` double NOT NULL DEFAULT '0',
  `fLatestCost` double NOT NULL DEFAULT '0',
  `fLowestCost` double NOT NULL DEFAULT '0',
  `fHighestCost` double NOT NULL DEFAULT '0',
  `fManualCost` double NOT NULL DEFAULT '0',
  `fWhseLastGRVCost` double NOT NULL DEFAULT '0',
  `bWHAllowNegStock` tinyint(1) NOT NULL DEFAULT '0',
  `fWHQtyToDeliver` double DEFAULT NULL,
  `WhseStk_fLeadDays` double DEFAULT NULL,
  `WHBuyingAgentID` int(11) DEFAULT NULL,
  `fIBTQtyToIssue` double NOT NULL DEFAULT '0',
  `fIBTQtyToReceive` double NOT NULL DEFAULT '0',
  `WhseStk_iBranchID` int(11) DEFAULT NULL,
  `WhseStk_dCreatedDate` datetime(6) DEFAULT NULL,
  `WhseStk_dModifiedDate` datetime(6) DEFAULT NULL,
  `WhseStk_iCreatedBranchID` int(11) DEFAULT NULL,
  `WhseStk_iModifiedBranchID` int(11) DEFAULT NULL,
  `WhseStk_iCreatedAgentID` int(11) DEFAULT NULL,
  `WhseStk_iModifiedAgentID` int(11) DEFAULT NULL,
  `WhseStk_iChangeSetID` int(11) DEFAULT NULL,
  `WhseStk_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wht_batch`
--

DROP TABLE IF EXISTS `wht_batch`;
CREATE TABLE `wht_batch` (
  `idBatch` int(11) NOT NULL,
  `batchNumber` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `batchState` varchar(2) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fWHTPercentage` double DEFAULT NULL,
  `fWHTAmount` double DEFAULT NULL,
  `idInvNum` bigint(20) DEFAULT NULL,
  `idInvoiceLines` bigint(20) DEFAULT NULL,
  `InvNumber` varchar(18) CHARACTER SET utf8mb4 DEFAULT NULL,
  `AccountId` bigint(20) DEFAULT NULL,
  `InvDate` datetime DEFAULT NULL,
  `OrderDate` datetime DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `DeliveryDate` datetime DEFAULT NULL,
  `OrderNum` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cAccountName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `cDescription` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fQtyProcessed` double DEFAULT NULL,
  `fUnitPriceExcl` double DEFAULT NULL,
  `fUnitPriceIncl` double DEFAULT NULL,
  `fTaxRate` double DEFAULT NULL,
  `iTaxTypeId` bigint(20) DEFAULT NULL,
  `ForeignCurrencyID` int(11) DEFAULT NULL,
  `fUnitPriceExclForeign` double DEFAULT NULL,
  `fUnitPriceInclForeign` double DEFAULT NULL,
  `WHT_Batch_iBranchID` int(11) DEFAULT NULL,
  `WHT_Batch_dCreatedDate` datetime(6) DEFAULT NULL,
  `WHT_Batch_dModifiedDate` datetime(6) DEFAULT NULL,
  `WHT_Batch_iCreatedBranchID` int(11) DEFAULT NULL,
  `WHT_Batch_iModifiedBranchID` int(11) DEFAULT NULL,
  `WHT_Batch_iCreatedAgentID` int(11) DEFAULT NULL,
  `WHT_Batch_iModifiedAgentID` int(11) DEFAULT NULL,
  `WHT_Batch_iChangeSetID` int(11) DEFAULT NULL,
  `WHT_Batch_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wht_batchstatus`
--

DROP TABLE IF EXISTS `wht_batchstatus`;
CREATE TABLE `wht_batchstatus` (
  `idBatchNumber` int(11) NOT NULL,
  `batchNumber` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `batchStatus` tinyint(1) DEFAULT NULL,
  `creationDate` datetime(6) DEFAULT NULL,
  `processingDate` datetime(6) DEFAULT NULL,
  `WHT_BatchStatus_iBranchID` int(11) DEFAULT NULL,
  `WHT_BatchStatus_dCreatedDate` datetime(6) DEFAULT NULL,
  `WHT_BatchStatus_dModifiedDate` datetime(6) DEFAULT NULL,
  `WHT_BatchStatus_iCreatedBranchID` int(11) DEFAULT NULL,
  `WHT_BatchStatus_iModifiedBranchID` int(11) DEFAULT NULL,
  `WHT_BatchStatus_iCreatedAgentID` int(11) DEFAULT NULL,
  `WHT_BatchStatus_iModifiedAgentID` int(11) DEFAULT NULL,
  `WHT_BatchStatus_iChangeSetID` int(11) DEFAULT NULL,
  `WHT_BatchStatus_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wht_defaultsettings`
--

DROP TABLE IF EXISTS `wht_defaultsettings`;
CREATE TABLE `wht_defaultsettings` (
  `idAccountLink` int(11) DEFAULT NULL,
  `idVendorGLAccount` int(11) DEFAULT NULL,
  `idTrCodes` int(11) DEFAULT NULL,
  `SubAccount` varchar(41) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Account` varchar(21) CHARACTER SET utf8mb4 DEFAULT NULL,
  `AccountType` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `TrCode` varchar(4) CHARACTER SET utf8mb4 DEFAULT NULL,
  `TrCodeDescription` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `CutOffDate` datetime(6) DEFAULT NULL,
  `isGlItemToShow` tinyint(1) DEFAULT '1',
  `isStItemToShow` tinyint(1) DEFAULT '1',
  `isServiceItemToShow` tinyint(1) DEFAULT '1',
  `WHT_DefaultSettings_iBranchID` int(11) DEFAULT NULL,
  `WHT_DefaultSettings_dCreatedDate` datetime(6) DEFAULT NULL,
  `WHT_DefaultSettings_dModifiedDate` datetime(6) DEFAULT NULL,
  `WHT_DefaultSettings_iCreatedBranchID` int(11) DEFAULT NULL,
  `WHT_DefaultSettings_iModifiedBranchID` int(11) DEFAULT NULL,
  `WHT_DefaultSettings_iCreatedAgentID` int(11) DEFAULT NULL,
  `WHT_DefaultSettings_iModifiedAgentID` int(11) DEFAULT NULL,
  `WHT_DefaultSettings_iChangeSetID` int(11) DEFAULT NULL,
  `WHT_DefaultSettings_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wht_state`
--

DROP TABLE IF EXISTS `wht_state`;
CREATE TABLE `wht_state` (
  `State` varchar(2) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Description` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `WHT_State_iBranchID` int(11) DEFAULT NULL,
  `WHT_State_dCreatedDate` datetime(6) DEFAULT NULL,
  `WHT_State_dModifiedDate` datetime(6) DEFAULT NULL,
  `WHT_State_iCreatedBranchID` int(11) DEFAULT NULL,
  `WHT_State_iModifiedBranchID` int(11) DEFAULT NULL,
  `WHT_State_iCreatedAgentID` int(11) DEFAULT NULL,
  `WHT_State_iModifiedAgentID` int(11) DEFAULT NULL,
  `WHT_State_iChangeSetID` int(11) DEFAULT NULL,
  `WHT_State_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wht_taxmaster`
--

DROP TABLE IF EXISTS `wht_taxmaster`;
CREATE TABLE `wht_taxmaster` (
  `idTaxMaster` int(11) NOT NULL,
  `idVendor` int(11) DEFAULT NULL,
  `VendorAccount` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `VendorName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bIsVendorWHT` tinyint(1) DEFAULT NULL,
  `fWHTPercentage` double DEFAULT NULL,
  `bIsSelect` tinyint(1) DEFAULT NULL,
  `WHT_TaxMaster_iBranchID` int(11) DEFAULT NULL,
  `WHT_TaxMaster_dCreatedDate` datetime(6) DEFAULT NULL,
  `WHT_TaxMaster_dModifiedDate` datetime(6) DEFAULT NULL,
  `WHT_TaxMaster_iCreatedBranchID` int(11) DEFAULT NULL,
  `WHT_TaxMaster_iModifiedBranchID` int(11) DEFAULT NULL,
  `WHT_TaxMaster_iCreatedAgentID` int(11) DEFAULT NULL,
  `WHT_TaxMaster_iModifiedAgentID` int(11) DEFAULT NULL,
  `WHT_TaxMaster_iChangeSetID` int(11) DEFAULT NULL,
  `WHT_TaxMaster_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wht_userdetails`
--

DROP TABLE IF EXISTS `wht_userdetails`;
CREATE TABLE `wht_userdetails` (
  `idUser` int(11) NOT NULL,
  `cUserName` varchar(24) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cPassword` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bIsAdmin` tinyint(1) DEFAULT NULL,
  `cRole` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `WHT_UserDetails_iBranchID` int(11) DEFAULT NULL,
  `WHT_UserDetails_dCreatedDate` datetime(6) DEFAULT NULL,
  `WHT_UserDetails_dModifiedDate` datetime(6) DEFAULT NULL,
  `WHT_UserDetails_iCreatedBranchID` int(11) DEFAULT NULL,
  `WHT_UserDetails_iModifiedBranchID` int(11) DEFAULT NULL,
  `WHT_UserDetails_iCreatedAgentID` int(11) DEFAULT NULL,
  `WHT_UserDetails_iModifiedAgentID` int(11) DEFAULT NULL,
  `WHT_UserDetails_iChangeSetID` int(11) DEFAULT NULL,
  `WHT_UserDetails_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailaccounts`
--

DROP TABLE IF EXISTS `_atblbulkemailaccounts`;
CREATE TABLE `_atblbulkemailaccounts` (
  `idBulkEmailAccount` int(11) NOT NULL,
  `cEmailSettingName` varchar(100) NOT NULL,
  `cFromEmail` varchar(200) DEFAULT NULL,
  `cToEmail` varchar(200) DEFAULT NULL,
  `cSMTPServer` varchar(50) DEFAULT NULL,
  `iPortNumber` int(11) DEFAULT NULL,
  `cEmailUserName` varchar(200) DEFAULT NULL,
  `cEmailPassword` varchar(150) DEFAULT NULL,
  `bEmailRequiresSSL` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailfilters`
--

DROP TABLE IF EXISTS `_atblbulkemailfilters`;
CREATE TABLE `_atblbulkemailfilters` (
  `idBulkEmailFilter` int(11) NOT NULL,
  `cEmailFilterName` varchar(50) NOT NULL,
  `iFromCustomer` varchar(20) DEFAULT NULL,
  `iToCustomer` varchar(20) DEFAULT NULL,
  `cCustomerGroups` longtext,
  `cAreas` longtext,
  `cSalesReps` longtext,
  `cCurrencies` longtext,
  `cCustomerField` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailhistory`
--

DROP TABLE IF EXISTS `_atblbulkemailhistory`;
CREATE TABLE `_atblbulkemailhistory` (
  `idBulkEmailHistory` int(11) NOT NULL,
  `iCustomerID` int(11) NOT NULL,
  `iBulkEmailTemplateID` int(11) NOT NULL,
  `cSentToEmailAddress` varchar(50) DEFAULT NULL,
  `dTimeStamp` datetime(6) NOT NULL,
  `cMessage` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailhistorydocuments`
--

DROP TABLE IF EXISTS `_atblbulkemailhistorydocuments`;
CREATE TABLE `_atblbulkemailhistorydocuments` (
  `idBulkEmailHistoryDocument` int(11) NOT NULL,
  `iBulkEmailHistoryID` int(11) NOT NULL,
  `binDocument` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailtemplatedata`
--

DROP TABLE IF EXISTS `_atblbulkemailtemplatedata`;
CREATE TABLE `_atblbulkemailtemplatedata` (
  `idBulkEmailTemplateData` int(11) NOT NULL,
  `iBulkEmailTemplateID` int(11) NOT NULL,
  `binData` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailtemplates`
--

DROP TABLE IF EXISTS `_atblbulkemailtemplates`;
CREATE TABLE `_atblbulkemailtemplates` (
  `idBulkEmailTemplate` int(11) NOT NULL,
  `cTemplateName` varchar(50) DEFAULT NULL,
  `cTemplateDescription` varchar(50) DEFAULT NULL,
  `cDefaultOutline` varchar(100) DEFAULT NULL,
  `bAllowSendSameTemplate` tinyint(1) NOT NULL,
  `bCreateIncident` tinyint(1) NOT NULL,
  `iIncidentDefaultAgentID` int(11) NOT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `iDueInDays` int(11) NOT NULL,
  `bCreateAsClosed` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblbulkemailudffilters`
--

DROP TABLE IF EXISTS `_atblbulkemailudffilters`;
CREATE TABLE `_atblbulkemailudffilters` (
  `idBulkEmailUDFFilter` int(11) NOT NULL,
  `iBulkEmailFilterID` int(11) NOT NULL,
  `cUDFTableName` varchar(100) NOT NULL,
  `cUDFFieldName` varchar(100) NOT NULL,
  `cValue` varchar(100) DEFAULT NULL,
  `cToValue` varchar(100) DEFAULT NULL,
  `iFieldType` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblcolumnlookups`
--

DROP TABLE IF EXISTS `_atblcolumnlookups`;
CREATE TABLE `_atblcolumnlookups` (
  `idColumnLookup` int(11) NOT NULL,
  `cTableName` varchar(256) NOT NULL,
  `cColumnName` varchar(256) NOT NULL,
  `cLookupTable` varchar(256) NOT NULL,
  `cLookupColumn` varchar(256) NOT NULL,
  `bIsValueLookup` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblcolumnlookupvalues`
--

DROP TABLE IF EXISTS `_atblcolumnlookupvalues`;
CREATE TABLE `_atblcolumnlookupvalues` (
  `idColumnLookupValue` int(11) NOT NULL,
  `iColumnLookupID` int(11) NOT NULL,
  `cLookupCode` varchar(100) NOT NULL,
  `cValue` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblcolumns`
--

DROP TABLE IF EXISTS `_atblcolumns`;
CREATE TABLE `_atblcolumns` (
  `idColumn` int(11) NOT NULL,
  `cTableName` varchar(256) NOT NULL,
  `cColumnName` varchar(256) NOT NULL,
  `iColumnSelectType` int(11) NOT NULL,
  `cDefaultValue` varchar(1000) NOT NULL,
  `bSystemColumn` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbldocdefaults`
--

DROP TABLE IF EXISTS `_atbldocdefaults`;
CREATE TABLE `_atbldocdefaults` (
  `idDocDefault` int(11) NOT NULL,
  `bCreateIncident` tinyint(1) NOT NULL,
  `iIncidentDefaultAgentID` int(11) DEFAULT NULL,
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `bIncidentAttachDocument` tinyint(1) DEFAULT NULL,
  `iImportFileType` int(11) NOT NULL,
  `cDelimiter` varchar(5) NOT NULL,
  `cDateFormat` varchar(20) NOT NULL,
  `cDateSeparator` varchar(5) NOT NULL,
  `bUseShortDate` tinyint(1) NOT NULL,
  `cDocumentImportPath` varchar(500) NOT NULL,
  `bSendEmail` tinyint(1) NOT NULL,
  `bEmailAttachDocument` tinyint(1) DEFAULT NULL,
  `iEmailAccountID` int(11) NOT NULL,
  `iHeaderCaptionType` int(11) NOT NULL,
  `cServerName` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbldocimportdocumenttemplates`
--

DROP TABLE IF EXISTS `_atbldocimportdocumenttemplates`;
CREATE TABLE `_atbldocimportdocumenttemplates` (
  `idDocImportDocumentTemplate` int(11) NOT NULL,
  `cDescription` varchar(100) NOT NULL,
  `cPrimaryTable` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbldocimportdocumenttemplatetables`
--

DROP TABLE IF EXISTS `_atbldocimportdocumenttemplatetables`;
CREATE TABLE `_atbldocimportdocumenttemplatetables` (
  `idDocumentTemplateTable` int(11) NOT NULL,
  `iDocumentTemplateID` int(11) NOT NULL,
  `cTableName` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbldocimportdocumenttypes`
--

DROP TABLE IF EXISTS `_atbldocimportdocumenttypes`;
CREATE TABLE `_atbldocimportdocumenttypes` (
  `idDocImportDocumentTypes` int(11) NOT NULL,
  `cDocumentType` varchar(100) NOT NULL,
  `cFilePrefix` varchar(50) NOT NULL,
  `iDocImportDocumentTemplateID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbldocimportfieldmappings`
--

DROP TABLE IF EXISTS `_atbldocimportfieldmappings`;
CREATE TABLE `_atbldocimportfieldmappings` (
  `idDocImportFieldMapping` int(11) NOT NULL,
  `iDocImportDocumentTypeID` int(11) NOT NULL,
  `cTableName` varchar(100) NOT NULL,
  `cFieldName` varchar(100) NOT NULL,
  `iPosition` int(11) NOT NULL,
  `bImportCodeInstead` tinyint(1) NOT NULL,
  `bRequiredField` tinyint(1) NOT NULL,
  `cCaption` varchar(100) DEFAULT NULL,
  `bCreate` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblemailaccounts`
--

DROP TABLE IF EXISTS `_atblemailaccounts`;
CREATE TABLE `_atblemailaccounts` (
  `idEmailAccount` int(11) NOT NULL,
  `cEmailSettingName` varchar(100) NOT NULL,
  `cFromEmail` varchar(200) DEFAULT NULL,
  `cToEmail` varchar(200) DEFAULT NULL,
  `cSMTPServer` varchar(50) DEFAULT NULL,
  `iPortNumber` int(11) DEFAULT NULL,
  `cEmailUserName` varchar(200) DEFAULT NULL,
  `cEmailPassword` varchar(50) DEFAULT NULL,
  `bEmailRequiresSSL` tinyint(1) NOT NULL,
  `cDefaultOutline` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblexportdefaults`
--

DROP TABLE IF EXISTS `_atblexportdefaults`;
CREATE TABLE `_atblexportdefaults` (
  `idExportDefault` int(11) NOT NULL,
  `iImportFileType` int(11) NOT NULL,
  `cDelimiter` varchar(5) NOT NULL,
  `cDateFormat` varchar(20) NOT NULL,
  `cDateSeparator` varchar(5) NOT NULL,
  `bUseShortDate` tinyint(1) NOT NULL,
  `cDocumentExportPath` varchar(500) NOT NULL,
  `bSendEmail` tinyint(1) NOT NULL,
  `bAppendDate` tinyint(1) NOT NULL,
  `iHeaderCaptionType` int(11) NOT NULL,
  `iEmailAccountID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblexportfieldmappings`
--

DROP TABLE IF EXISTS `_atblexportfieldmappings`;
CREATE TABLE `_atblexportfieldmappings` (
  `idExportFieldMapping` int(11) NOT NULL,
  `iExportTemplateID` int(11) NOT NULL,
  `cTableName` varchar(100) NOT NULL,
  `cFieldName` varchar(100) NOT NULL,
  `iPosition` int(11) NOT NULL,
  `cCaption` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atblexporttemplates`
--

DROP TABLE IF EXISTS `_atblexporttemplates`;
CREATE TABLE `_atblexporttemplates` (
  `idExportTemplate` int(11) NOT NULL,
  `cTemplateDescription` varchar(100) NOT NULL,
  `cTableName` varchar(258) NOT NULL,
  `cFilePrefix` varchar(20) NOT NULL,
  `bAppendDateToFile` tinyint(1) NOT NULL,
  `bPlaceInSubFolder` tinyint(1) NOT NULL,
  `cSubFolderName` varchar(128) NOT NULL,
  `bSendEmailAfterExport` tinyint(1) NOT NULL,
  `bAttachExportFileToEmail` tinyint(1) NOT NULL,
  `cSendEmailTo` varchar(256) NOT NULL,
  `bCreateIncidentAfterExport` tinyint(1) NOT NULL,
  `bAttachExportFileToIncident` tinyint(1) NOT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `iAssignedToAgentID` int(11) NOT NULL,
  `iExportType` int(11) DEFAULT NULL,
  `cColumnName` varchar(128) DEFAULT NULL,
  `iLastExportedID` int(11) DEFAULT NULL,
  `dLastExportedDate` datetime(6) DEFAULT NULL,
  `bIncludeRowCounter` tinyint(1) DEFAULT NULL,
  `bCreateModifiedTrigger` tinyint(1) DEFAULT NULL,
  `cRowCounterCaption` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbltablerelationships`
--

DROP TABLE IF EXISTS `_atbltablerelationships`;
CREATE TABLE `_atbltablerelationships` (
  `idTableRelationship` int(11) NOT NULL,
  `cPrimaryTable` varchar(256) NOT NULL,
  `cPrimaryColumn` varchar(256) NOT NULL,
  `cForeignTable` varchar(256) NOT NULL,
  `cForeignColumn` varchar(256) NOT NULL,
  `cCriteriaColumn` varchar(1024) NOT NULL,
  `iCompareType` varchar(1024) NOT NULL,
  `cCriteriaValues` varchar(1024) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_atbltables`
--

DROP TABLE IF EXISTS `_atbltables`;
CREATE TABLE `_atbltables` (
  `idTable` int(11) NOT NULL,
  `cName` varchar(256) NOT NULL,
  `cUserFreindlyTableName` varchar(256) NOT NULL,
  `cCodeColumn` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblagentoptions`
--

DROP TABLE IF EXISTS `_btblagentoptions`;
CREATE TABLE `_btblagentoptions` (
  `IDAgentOptions` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `iCalTimeFormat` int(11) DEFAULT NULL,
  `iCalDVDaysVisible` int(11) DEFAULT NULL,
  `iCalDVTimeInterval` int(11) DEFAULT NULL,
  `bCalDVShowWeekends` tinyint(1) NOT NULL DEFAULT '1',
  `bCalWVShowEventTimes` tinyint(1) NOT NULL DEFAULT '0',
  `bCalMVShowEvents` tinyint(1) NOT NULL DEFAULT '1',
  `bCalMVShowEventTimes` tinyint(1) NOT NULL DEFAULT '0',
  `bAlarmSet` tinyint(1) NOT NULL DEFAULT '0',
  `iAlarmAdvance` int(11) DEFAULT NULL,
  `iAlarmAdvanceType` int(11) DEFAULT NULL,
  `cDingPath` varchar(255) DEFAULT NULL,
  `bShowContactPerson` tinyint(1) NOT NULL DEFAULT '1',
  `bShowIncTypeGroup` tinyint(1) NOT NULL DEFAULT '1',
  `bShowWorkflow` tinyint(1) NOT NULL DEFAULT '1',
  `bShowEscGroup` tinyint(1) NOT NULL DEFAULT '1',
  `bShowInvItem` tinyint(1) NOT NULL DEFAULT '1',
  `bShowSupplier` tinyint(1) NOT NULL DEFAULT '1',
  `bShowWorker` tinyint(1) NOT NULL DEFAULT '1',
  `bShowFixedAssets` tinyint(1) NOT NULL DEFAULT '1',
  `bShowProject` tinyint(1) NOT NULL DEFAULT '1',
  `bShowJobCosting` tinyint(1) NOT NULL DEFAULT '1',
  `bShowProspect` tinyint(1) NOT NULL DEFAULT '1',
  `bShowOpportunity` tinyint(1) DEFAULT '1',
  `_btblAgentOptions_iBranchID` int(11) DEFAULT NULL,
  `_btblAgentOptions_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblAgentOptions_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblAgentOptions_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblAgentOptions_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblAgentOptions_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblAgentOptions_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblAgentOptions_iChangeSetID` int(11) DEFAULT NULL,
  `_btblAgentOptions_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblagentoutoffice`
--

DROP TABLE IF EXISTS `_btblagentoutoffice`;
CREATE TABLE `_btblagentoutoffice` (
  `IDAgentOutOffice` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `dDateFrom` datetime(6) DEFAULT NULL,
  `dDateTo` datetime(6) DEFAULT NULL,
  `iReason` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_iBranchID` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblAgentOutOffice_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblAgentOutOffice_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_iChangeSetID` int(11) DEFAULT NULL,
  `_btblAgentOutOffice_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblagentoutofficereasons`
--

DROP TABLE IF EXISTS `_btblagentoutofficereasons`;
CREATE TABLE `_btblagentoutofficereasons` (
  `iAgentOutOfficeID` int(11) NOT NULL,
  `cReason` varchar(50) NOT NULL,
  `_btblAgentOutOfficeReasons_iBranchID` int(11) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_iChangeSetID` int(11) DEFAULT NULL,
  `_btblAgentOutOfficeReasons_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblagentsystemfunctions`
--

DROP TABLE IF EXISTS `_btblagentsystemfunctions`;
CREATE TABLE `_btblagentsystemfunctions` (
  `iAgentID` int(11) NOT NULL,
  `cAgentType` char(1) NOT NULL,
  `iSystemFunctionID` int(11) NOT NULL,
  `iReadAccess` int(11) DEFAULT NULL,
  `iEditAccess` int(11) DEFAULT NULL,
  `iAddAccess` int(11) DEFAULT NULL,
  `iDeleteAccess` int(11) DEFAULT NULL,
  `iRuleAccess` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_iBranchID` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblAgentSystemFunctions_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblAgentSystemFunctions_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_iChangeSetID` int(11) DEFAULT NULL,
  `_btblAgentSystemFunctions_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblagentsystemtree`
--

DROP TABLE IF EXISTS `_btblagentsystemtree`;
CREATE TABLE `_btblagentsystemtree` (
  `idAgentSystemTree` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cAgentType` char(1) NOT NULL,
  `iSystemTreeID` int(11) NOT NULL,
  `iParentAgentSystemTreeID` int(11) NOT NULL,
  `iOrder` int(11) NOT NULL,
  `cDescription` varchar(64) DEFAULT NULL,
  `cToolbar` varchar(50) DEFAULT NULL,
  `cMenu` varchar(50) DEFAULT NULL,
  `cMenuName` varchar(50) DEFAULT NULL,
  `bMenuSub` tinyint(1) NOT NULL DEFAULT '0',
  `_btblAgentSystemTree_iBranchID` int(11) DEFAULT NULL,
  `_btblAgentSystemTree_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblAgentSystemTree_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblAgentSystemTree_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblAgentSystemTree_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblAgentSystemTree_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblAgentSystemTree_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblAgentSystemTree_iChangeSetID` int(11) DEFAULT NULL,
  `_btblAgentSystemTree_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblages`
--

DROP TABLE IF EXISTS `_btblages`;
CREATE TABLE `_btblages` (
  `idAges` int(11) NOT NULL,
  `nARAges` longblob,
  `nAPAges` longblob,
  `_btblAges_iBranchID` int(11) DEFAULT NULL,
  `_btblAges_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblAges_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblAges_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblAges_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblAges_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblAges_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblAges_iChangeSetID` int(11) DEFAULT NULL,
  `_btblAges_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblbatchcheckout`
--

DROP TABLE IF EXISTS `_btblbatchcheckout`;
CREATE TABLE `_btblbatchcheckout` (
  `idBatchCheckout` int(11) NOT NULL,
  `cBatchClass` varchar(32) NOT NULL,
  `iBatchID` int(11) NOT NULL,
  `cNetworkUser` varchar(32) NOT NULL,
  `cAgentName` varchar(50) DEFAULT NULL,
  `cCheckoutGUID` varchar(50) NOT NULL,
  `bIncludeOpenBatches` tinyint(1) NOT NULL DEFAULT '0',
  `_btblBatchCheckout_iBranchID` int(11) DEFAULT NULL,
  `_btblBatchCheckout_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblBatchCheckout_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblBatchCheckout_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblBatchCheckout_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblBatchCheckout_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblBatchCheckout_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblBatchCheckout_iChangeSetID` int(11) DEFAULT NULL,
  `_btblBatchCheckout_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblbinlocation`
--

DROP TABLE IF EXISTS `_btblbinlocation`;
CREATE TABLE `_btblbinlocation` (
  `idBinLocation` int(11) NOT NULL,
  `cBinLocationName` varchar(30) NOT NULL,
  `cBinLocationDescription` varchar(100) DEFAULT NULL,
  `_btblBINLocation_iBranchID` int(11) DEFAULT NULL,
  `_btblBINLocation_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblBINLocation_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblBINLocation_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblBINLocation_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblBINLocation_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblBINLocation_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblBINLocation_iChangeSetID` int(11) DEFAULT NULL,
  `_btblBINLocation_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbbankimportdefaults`
--

DROP TABLE IF EXISTS `_btblcbbankimportdefaults`;
CREATE TABLE `_btblcbbankimportdefaults` (
  `idCBBankImportDefaults` int(11) NOT NULL,
  `bFixedLength` tinyint(1) NOT NULL DEFAULT '0',
  `cDelimiter` varchar(3) DEFAULT NULL,
  `bImpliedDecimals` tinyint(1) NOT NULL DEFAULT '0',
  `iTxDateStartPos` int(11) DEFAULT NULL,
  `iTxDateLength` int(11) DEFAULT NULL,
  `iTxDatePosition` int(11) DEFAULT NULL,
  `iReferenceStartPos` int(11) DEFAULT NULL,
  `iReferenceLength` int(11) DEFAULT NULL,
  `iReferencePosition` int(11) DEFAULT NULL,
  `iAmountStartPos` int(11) DEFAULT NULL,
  `iAmountLength` int(11) DEFAULT NULL,
  `iAmountPosition` int(11) DEFAULT NULL,
  `bWindowsDateFormat` tinyint(1) NOT NULL DEFAULT '1',
  `cCustomDateFormat` varchar(8) DEFAULT NULL,
  `cDateSeparator` varchar(1) DEFAULT NULL,
  `iHeaderRecords` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_iBranchID` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCBBankImportDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCBBankImportDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCBBankImportDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbbatchdefs`
--

DROP TABLE IF EXISTS `_btblcbbatchdefs`;
CREATE TABLE `_btblcbbatchdefs` (
  `idBatchDefs` int(11) NOT NULL,
  `bAutoNumbers` tinyint(1) NOT NULL,
  `iPadLength` int(11) DEFAULT NULL,
  `cPrefix` varchar(20) DEFAULT NULL,
  `iInputTaxID` int(11) DEFAULT NULL,
  `iInputTaxAccID` int(11) DEFAULT NULL,
  `iOutputTaxID` int(11) DEFAULT NULL,
  `iOutputTaxAccID` int(11) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `iGLBankAccID` int(11) DEFAULT NULL,
  `iGLARAccID` int(11) DEFAULT NULL,
  `iGLAPAccID` int(11) DEFAULT NULL,
  `iARDiscTrCodeID` int(11) DEFAULT NULL,
  `iAPDiscTrCodeID` int(11) DEFAULT NULL,
  `bBatchRefAutoNumbers` tinyint(1) NOT NULL DEFAULT '1',
  `iBatchRefPadLength` int(11) DEFAULT NULL,
  `cBatchRefPrefix` varchar(20) DEFAULT NULL,
  `iNextBatchRefNo` int(11) DEFAULT NULL,
  `bForceBatchRefNo` tinyint(1) NOT NULL DEFAULT '1',
  `iCurrencyID` int(11) DEFAULT NULL,
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `bForceRep` tinyint(1) NOT NULL DEFAULT '0',
  `iEFTSLayoutID` int(11) DEFAULT NULL,
  `cEFTSPathOutFile` varchar(100) DEFAULT NULL,
  `_btblCbBatchDefs_iBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchDefs_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCbBatchDefs_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCbBatchDefs_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchDefs_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchDefs_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatchDefs_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatchDefs_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCbBatchDefs_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbbatches`
--

DROP TABLE IF EXISTS `_btblcbbatches`;
CREATE TABLE `_btblcbbatches` (
  `idBatches` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDesc` varchar(40) DEFAULT NULL,
  `bModuleGL` tinyint(1) NOT NULL,
  `bModuleAR` tinyint(1) NOT NULL,
  `bModuleAP` tinyint(1) NOT NULL,
  `iInputTaxID` int(11) DEFAULT NULL,
  `iInputTaxAccID` int(11) DEFAULT NULL,
  `iOutputTaxID` int(11) DEFAULT NULL,
  `iOutputTaxAccID` int(11) DEFAULT NULL,
  `bCalcTax` tinyint(1) NOT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `bClearBatch` tinyint(1) NOT NULL,
  `iDateLineOpt` int(11) DEFAULT NULL,
  `dDefDate` datetime DEFAULT NULL,
  `iRefLineOpt` int(11) DEFAULT NULL,
  `cDefRef` varchar(20) DEFAULT NULL,
  `iDescLineOpt` int(11) DEFAULT NULL,
  `cDefDesc` varchar(40) DEFAULT NULL,
  `iGLBankAccID` int(11) DEFAULT NULL,
  `iGLARAccID` int(11) DEFAULT NULL,
  `iGLAPAccID` int(11) DEFAULT NULL,
  `iDefModule` int(11) DEFAULT NULL,
  `bAllowDisc` tinyint(1) NOT NULL DEFAULT '1',
  `iARDiscTrCodeID` int(11) DEFAULT NULL,
  `iAPDiscTrCodeID` int(11) DEFAULT NULL,
  `cDiscDesc` varchar(40) DEFAULT NULL,
  `bCheckedOut` tinyint(1) DEFAULT '0',
  `bDupRefs` tinyint(1) DEFAULT '1',
  `bPrintCheque` tinyint(1) NOT NULL DEFAULT '0',
  `bIncludeBankStatement` tinyint(1) NOT NULL DEFAULT '0',
  `iMaxRecur` int(11) DEFAULT NULL,
  `iBatchPosted` int(11) DEFAULT NULL,
  `cBatchRef` varchar(50) DEFAULT NULL,
  `fValidationTotDeposits` double DEFAULT NULL,
  `fValidationTotPayments` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bPromptGlobalChanges` tinyint(1) DEFAULT '0',
  `bTransDateOnCheque` tinyint(1) NOT NULL DEFAULT '0',
  `dDateBatchCreated` datetime(6) DEFAULT NULL,
  `iAgentBatchCreated` int(11) NOT NULL DEFAULT '1',
  `iAgentCheckedOut` int(11) NOT NULL DEFAULT '0',
  `bApplySettDisc` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowZeroValues` tinyint(1) NOT NULL DEFAULT '0',
  `bARAllowLinkedAccounts` tinyint(1) NOT NULL DEFAULT '0',
  `bInterBranchBatch` tinyint(1) NOT NULL DEFAULT '0',
  `iBranchLoanAccountID` int(11) DEFAULT NULL,
  `bOnlyAllowForCurrency` tinyint(1) NOT NULL DEFAULT '0',
  `dProcessedDate` datetime(6) DEFAULT NULL,
  `bAutoAllocBBForward` tinyint(1) DEFAULT NULL,
  `bAutoAllocOpenItem` tinyint(1) NOT NULL DEFAULT '0',
  `bEFTSExport` tinyint(1) NOT NULL DEFAULT '0',
  `_btblCbBatches_iBranchID` int(11) DEFAULT NULL,
  `_btblCbBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCbBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCbBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCbBatches_Checksum` binary(20) DEFAULT NULL,
  `bAutoAllocByReference` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbbatchlines`
--

DROP TABLE IF EXISTS `_btblcbbatchlines`;
CREATE TABLE `_btblcbbatchlines` (
  `idBatchLines` int(11) NOT NULL,
  `iBatchesID` int(11) NOT NULL,
  `iSplitType` int(11) DEFAULT '0',
  `iSplitGroup` int(11) DEFAULT NULL,
  `dTxDate` datetime NOT NULL,
  `iModule` int(11) NOT NULL,
  `iAccountID` int(11) NOT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `fDebit` double DEFAULT NULL,
  `fCredit` double DEFAULT NULL,
  `bReconcile` tinyint(1) NOT NULL,
  `fTaxAmount` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iTaxAccountID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `bPostDated` tinyint(1) NOT NULL,
  `fDiscPerc` double DEFAULT NULL,
  `iDiscTrCodeID` int(11) DEFAULT NULL,
  `cDiscDesc` varchar(35) DEFAULT NULL,
  `iDiscTaxTypeID` int(11) DEFAULT NULL,
  `iDiscTaxAccID` int(11) DEFAULT NULL,
  `fDiscTaxAmount` double DEFAULT NULL,
  `cPayeeName` varchar(100) DEFAULT NULL,
  `bPrintCheque` tinyint(1) NOT NULL DEFAULT '0',
  `bChequePrinted` tinyint(1) NOT NULL DEFAULT '0',
  `iRepID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fDebitForeign` double DEFAULT NULL,
  `fCreditForeign` double DEFAULT NULL,
  `fTaxAmountForeign` double DEFAULT NULL,
  `fDiscTaxAmountForeign` double DEFAULT NULL,
  `iCBBatchLinesReconID` int(11) DEFAULT NULL,
  `fFCAccountAmount` double NOT NULL DEFAULT '0',
  `fFCAccountExchange` double NOT NULL DEFAULT '1',
  `fFCAccountDiscAmount` double NOT NULL DEFAULT '0',
  `fFCAccountDiscTax` double NOT NULL DEFAULT '0',
  `iSettDiscGroupID` int(11) NOT NULL DEFAULT '0',
  `iSettDiscPostARAPID` bigint(20) NOT NULL DEFAULT '0',
  `cBankRef` varchar(20) DEFAULT NULL,
  `bIsPosted` tinyint(1) NOT NULL DEFAULT '0',
  `iMBPropertyID` int(11) DEFAULT '0',
  `iMBPortionID` int(11) DEFAULT '0',
  `iMBServiceID` int(11) DEFAULT '0',
  `iMBPropertyPortionServiceID` int(11) DEFAULT '0',
  `bMBOverride` tinyint(1) NOT NULL DEFAULT '0',
  `iMBMeterID` int(11) DEFAULT '0',
  `_btblCbBatchLines_iBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCbBatchLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCbBatchLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_Checksum` binary(20) DEFAULT NULL,
  `fFCAccountTaxAmount` double NOT NULL DEFAULT '0',
  `SagePayExtra1` longtext,
  `SagePayExtra2` longtext,
  `SagePayExtra3` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbimporthistory`
--

DROP TABLE IF EXISTS `_btblcbimporthistory`;
CREATE TABLE `_btblcbimporthistory` (
  `ID` int(11) NOT NULL,
  `DateTime` datetime(6) DEFAULT NULL,
  `FileName` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `FileDateTime` datetime(6) DEFAULT NULL,
  `BatchID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbmatchrules`
--

DROP TABLE IF EXISTS `_btblcbmatchrules`;
CREATE TABLE `_btblcbmatchrules` (
  `ID` int(11) NOT NULL,
  `Keywords` varchar(100) DEFAULT NULL,
  `Module` int(11) DEFAULT NULL,
  `AccountID` int(11) DEFAULT NULL,
  `Account` varchar(100) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `TaxRate` varchar(10) DEFAULT NULL,
  `Priority` int(11) DEFAULT NULL,
  `Project` varchar(20) DEFAULT NULL,
  `Effect` int(11) DEFAULT NULL,
  `CashbookID` int(11) DEFAULT NULL,
  `LastUsed` datetime(6) DEFAULT NULL,
  `LedgerDescription` varchar(100) DEFAULT NULL,
  `Source` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcbstatement`
--

DROP TABLE IF EXISTS `_btblcbstatement`;
CREATE TABLE `_btblcbstatement` (
  `BatchID` int(11) NOT NULL,
  `Date` datetime(6) NOT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `Module` int(11) DEFAULT NULL,
  `AccountID` int(11) DEFAULT NULL,
  `Account` varchar(100) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `LedgerDescription` varchar(100) DEFAULT NULL,
  `TaxRate` varchar(10) DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `Debit` double DEFAULT NULL,
  `Credit` double DEFAULT NULL,
  `UniqueID` varchar(60) DEFAULT NULL,
  `Posted` tinyint(1) DEFAULT NULL,
  `Project` varchar(20) DEFAULT NULL,
  `Batch` int(11) DEFAULT NULL,
  `RowNumber` int(11) DEFAULT NULL,
  `AutoMapped` int(11) DEFAULT NULL,
  `SagePayExtra1` longtext,
  `SagePayExtra2` longtext,
  `SagePayExtra3` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcmevent`
--

DROP TABLE IF EXISTS `_btblcmevent`;
CREATE TABLE `_btblcmevent` (
  `idEvent` int(11) NOT NULL,
  `dStartTime` datetime(6) DEFAULT NULL,
  `dEndTime` datetime(6) DEFAULT NULL,
  `iAgentID` int(11) NOT NULL,
  `cDescription` varchar(1024) DEFAULT NULL,
  `bAllDayEvent` tinyint(1) NOT NULL DEFAULT '0',
  `iRepeatCode` int(11) DEFAULT NULL,
  `dRepeatRangeEnd` datetime(6) DEFAULT NULL,
  `iCustomInterval` int(11) DEFAULT NULL,
  `iIncidentID` int(11) DEFAULT NULL,
  `cEventOutline` varchar(255) DEFAULT NULL,
  `_btblCMEvent_iBranchID` int(11) DEFAULT NULL,
  `_btblCMEvent_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCMEvent_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCMEvent_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCMEvent_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCMEvent_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCMEvent_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCMEvent_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCMEvent_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcmeventattendees`
--

DROP TABLE IF EXISTS `_btblcmeventattendees`;
CREATE TABLE `_btblcmeventattendees` (
  `iEventID` int(11) NOT NULL,
  `iAttendeeID` int(11) NOT NULL,
  `cAttendeeType` char(1) NOT NULL,
  `cAttendeeName` varchar(255) DEFAULT NULL,
  `iResponse` int(11) DEFAULT NULL,
  `cComment` varchar(255) DEFAULT NULL,
  `dSnoozeTime` datetime(6) DEFAULT NULL,
  `bAlarmSet` tinyint(1) NOT NULL DEFAULT '0',
  `iAlarmAdvance` int(11) DEFAULT NULL,
  `iAlarmAdvanceType` int(11) DEFAULT NULL,
  `cAlarmWavPath` varchar(255) DEFAULT NULL,
  `cAttendeeEmail` varchar(255) DEFAULT NULL,
  `idCMEventAttendees` int(11) NOT NULL,
  `_btblCMEventAttendees_iBranchID` int(11) DEFAULT NULL,
  `_btblCMEventAttendees_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCMEventAttendees_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCMEventAttendees_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCMEventAttendees_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCMEventAttendees_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCMEventAttendees_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCMEventAttendees_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCMEventAttendees_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcmworkflow`
--

DROP TABLE IF EXISTS `_btblcmworkflow`;
CREATE TABLE `_btblcmworkflow` (
  `idWorkflow` int(11) NOT NULL,
  `cName` varchar(30) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `bPOWorkflow` tinyint(1) NOT NULL DEFAULT '0',
  `_btblCMWorkflow_iBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflow_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCMWorkflow_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCMWorkflow_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflow_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflow_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCMWorkflow_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCMWorkflow_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCMWorkflow_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcmworkflowmembers`
--

DROP TABLE IF EXISTS `_btblcmworkflowmembers`;
CREATE TABLE `_btblcmworkflowmembers` (
  `idWorkflowMembers` int(11) NOT NULL,
  `iWorkflowID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cAgentType` char(1) NOT NULL,
  `iWorkflowStatusID` int(11) NOT NULL,
  `iSequenceNo` int(11) NOT NULL,
  `bAllowReject` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowCloseAfterReject` tinyint(1) NOT NULL DEFAULT '1',
  `iEscGroupID` int(11) DEFAULT NULL,
  `bOverrideAutoAssign` tinyint(1) NOT NULL DEFAULT '0',
  `bPOApprove` tinyint(1) NOT NULL DEFAULT '0',
  `bPOCancelonReject` tinyint(1) NOT NULL DEFAULT '0',
  `iPOCancelReasonID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_iBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCMWorkflowMembers_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCMWorkflowMembers_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCMWorkflowMembers_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblcmworkflowstatus`
--

DROP TABLE IF EXISTS `_btblcmworkflowstatus`;
CREATE TABLE `_btblcmworkflowstatus` (
  `idWorkflowStatus` int(11) NOT NULL,
  `cStatusCode` varchar(20) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `_btblCMWorkflowStatus_iBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflowStatus_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblCMWorkflowStatus_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblCMWorkflowStatus_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflowStatus_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCMWorkflowStatus_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCMWorkflowStatus_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCMWorkflowStatus_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCMWorkflowStatus_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaasset`
--

DROP TABLE IF EXISTS `_btblfaasset`;
CREATE TABLE `_btblfaasset` (
  `idAssetNo` int(11) NOT NULL,
  `cAssetCode` varchar(30) NOT NULL,
  `cTransferInd` char(1) DEFAULT NULL,
  `iFromAssetNo` int(11) DEFAULT NULL,
  `dFromTransferDate` datetime DEFAULT NULL,
  `iFromTransferPeriodNo` int(11) DEFAULT NULL,
  `iToAssetNo` int(11) DEFAULT NULL,
  `dToTransferDate` datetime DEFAULT NULL,
  `iToTransferPeriodNo` int(11) DEFAULT NULL,
  `cAssetDesc` varchar(80) NOT NULL,
  `iAssetTypeNo` int(11) NOT NULL,
  `iCostCenterNo` int(11) DEFAULT NULL,
  `iLocationNo` int(11) DEFAULT NULL,
  `iSupplierNo` int(11) DEFAULT NULL,
  `iCapexBudgetNo` int(11) DEFAULT NULL,
  `iCapexOrderNo` int(11) DEFAULT NULL,
  `fNoOfUnits` double NOT NULL,
  `dPurchaseDate` datetime NOT NULL,
  `dDepreciationStartDate` datetime NOT NULL,
  `iDepreciationStartPeriodNo` int(11) DEFAULT NULL,
  `dWTStartDate` datetime NOT NULL,
  `iWTStartPeriodNo` int(11) DEFAULT NULL,
  `fPurchaseValue` double NOT NULL,
  `fRevalueValue` double NOT NULL,
  `fInsuredValue` double NOT NULL,
  `fResidualValue` double NOT NULL,
  `fScrapValue` double NOT NULL,
  `fDeprPriorYearsTakeOn` double DEFAULT NULL,
  `fDeprCurrYearTakeOn` double DEFAULT NULL,
  `fWTPriorYearsTakeOn` double DEFAULT NULL,
  `fWTCurrYearTakeOn` double DEFAULT NULL,
  `fSellingPrice` double DEFAULT NULL,
  `dSellingDate` datetime DEFAULT NULL,
  `dReplacementDate` datetime DEFAULT NULL,
  `fReplacementCost` double DEFAULT NULL,
  `iBookOverrideMonths` int(11) DEFAULT NULL,
  `iTaxOverrideMonths` int(11) DEFAULT NULL,
  `dLastImportDate` datetime DEFAULT NULL,
  `iToAssetNo2` int(11) DEFAULT NULL,
  `cCurrentInd` char(1) DEFAULT NULL,
  `cBarCode` varchar(80) DEFAULT NULL,
  `dOverrideDate` datetime DEFAULT NULL,
  `dOriginalWTStartDate` datetime NOT NULL,
  `dOriginalDeprStartDate` datetime NOT NULL,
  `iMasterAssetID` int(11) DEFAULT NULL,
  `cMasterAssetCode` varchar(30) DEFAULT NULL,
  `fBookInitialAllowance` double DEFAULT NULL,
  `fTaxInitialAllowance` double DEFAULT NULL,
  `bInitialAllowancePosted` tinyint(1) NOT NULL DEFAULT '0',
  `fDivAssetBookPriorYearDepr` double DEFAULT NULL,
  `fDivAssetBookCurrYearDepr` double DEFAULT NULL,
  `fDivAssetTaxPriorYearDepr` double DEFAULT NULL,
  `fDivAssetTaxCurrYearDepr` double DEFAULT NULL,
  `iDivAssetBookBlockPeriods` int(11) DEFAULT NULL,
  `iDivAssetTaxBlockPeriods` int(11) DEFAULT NULL,
  `cSellOrScrap` varchar(1) DEFAULT NULL,
  `cSellScrapReason` varchar(256) DEFAULT NULL,
  `dDivDate` datetime(6) DEFAULT NULL,
  `fWTResidualValue` double DEFAULT '0',
  `bWTWriteOffAsset` tinyint(1) DEFAULT '0',
  `fCGTBaseCost` double DEFAULT '0',
  `bCGTRolloverRelief` tinyint(1) DEFAULT '0',
  `idFinMethod` int(11) DEFAULT '0',
  `fFinRate` double DEFAULT '0',
  `fFinResidual` double DEFAULT '0',
  `cFinSecurities` varchar(50) DEFAULT NULL,
  `fFinPeriod` int(11) DEFAULT '0',
  `cFinAccountNumber` varchar(50) DEFAULT NULL,
  `cFinWhere` varchar(50) DEFAULT NULL,
  `fImpairmentCost` double DEFAULT '0',
  `cRevaluationMethod` varchar(50) DEFAULT NULL,
  `_btblFAAsset_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAsset_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAsset_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAsset_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAsset_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAsset_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAsset_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAsset_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAsset_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassetblock`
--

DROP TABLE IF EXISTS `_btblfaassetblock`;
CREATE TABLE `_btblfaassetblock` (
  `idAssetBlockNo` int(11) NOT NULL,
  `dFromDate` datetime DEFAULT NULL,
  `dToDate` datetime DEFAULT NULL,
  `iAssetNo` int(11) NOT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `dTaxFromDate` datetime DEFAULT NULL,
  `dTaxToDate` datetime DEFAULT NULL,
  `_btblFAAssetBlock_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetBlock_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetBlock_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetBlock_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetBlock_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetBlock_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetBlock_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetBlock_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetBlock_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassetimages`
--

DROP TABLE IF EXISTS `_btblfaassetimages`;
CREATE TABLE `_btblfaassetimages` (
  `idImageNo` int(11) NOT NULL,
  `iAssetNo` int(11) NOT NULL,
  `cImageDesc` varchar(80) NOT NULL,
  `nImage` longblob NOT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `cImageType` varchar(10) DEFAULT NULL,
  `_btblFAAssetImages_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetImages_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetImages_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetImages_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetImages_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetImages_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetImages_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetImages_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetImages_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassetserialno`
--

DROP TABLE IF EXISTS `_btblfaassetserialno`;
CREATE TABLE `_btblfaassetserialno` (
  `idSerialNo` int(11) NOT NULL,
  `cAssetCode` varchar(30) NOT NULL,
  `iUnitNo` int(11) NOT NULL,
  `cSerialNo` varchar(80) DEFAULT NULL,
  `cBarcode` varchar(50) DEFAULT NULL,
  `iLocationNo` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetSerialNo_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetSerialNo_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetSerialNo_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassettracking`
--

DROP TABLE IF EXISTS `_btblfaassettracking`;
CREATE TABLE `_btblfaassettracking` (
  `idAssetTracking` int(11) NOT NULL,
  `cAssetTrackingNo` varchar(50) DEFAULT NULL,
  `cDescription` varchar(60) DEFAULT NULL,
  `dPrepared` datetime(6) DEFAULT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `cStartAssetCode` varchar(255) DEFAULT NULL,
  `cEndAssetCode` varchar(255) DEFAULT NULL,
  `iCount` int(11) NOT NULL DEFAULT '0',
  `iUncounted` int(11) NOT NULL DEFAULT '0',
  `iAgentID` int(11) DEFAULT NULL,
  `cLocations` varchar(1024) DEFAULT NULL,
  `bDeleteAftComplete` tinyint(1) NOT NULL DEFAULT '1',
  `bCompleted` tinyint(1) NOT NULL DEFAULT '0',
  `_btblFAAssetTracking_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetTracking_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetTracking_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetTracking_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetTracking_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetTracking_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetTracking_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetTracking_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetTracking_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassettrackinglines`
--

DROP TABLE IF EXISTS `_btblfaassettrackinglines`;
CREATE TABLE `_btblfaassettrackinglines` (
  `idAssetTrackingLines` int(11) NOT NULL,
  `iAssetTrackingID` int(11) NOT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `cSerialNo` varchar(50) DEFAULT NULL,
  `cBarCode` varchar(50) DEFAULT NULL,
  `cLocationCode` varchar(35) DEFAULT NULL,
  `iSystemUnitNoID` int(11) DEFAULT NULL,
  `cSystemAssetCode` varchar(30) DEFAULT NULL,
  `cSystemSerialNo` varchar(50) DEFAULT NULL,
  `cSystemBarCode` varchar(50) DEFAULT NULL,
  `cSystemLocationCode` varchar(35) DEFAULT NULL,
  `fCountQty` double DEFAULT NULL,
  `bModified` tinyint(1) NOT NULL DEFAULT '0',
  `iTrackingStatus` int(11) DEFAULT '0',
  `iTrackingDifference` int(11) DEFAULT '0',
  `iTrackingAdjustment` int(11) DEFAULT '0',
  `_btblFAAssetTrackingLines_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetTrackingLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetTrackingLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetTrackingLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetTrackingLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetTrackingLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetTrackingLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetTrackingLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetTrackingLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassettype`
--

DROP TABLE IF EXISTS `_btblfaassettype`;
CREATE TABLE `_btblfaassettype` (
  `idAssetTypeNo` int(11) NOT NULL,
  `cAssetTypeCode` varchar(30) NOT NULL,
  `cAssetTypeDesc` varchar(80) NOT NULL,
  `iDepreciationNo` int(11) NOT NULL,
  `iTaxDepreciationNo` int(11) NOT NULL,
  `fRevaluationIndex` double NOT NULL,
  `fInsuranceIndex` double NOT NULL,
  `iGLAccountNo` int(11) DEFAULT NULL,
  `fInsuranceCostFactor` double NOT NULL,
  `fResidualFactor` double NOT NULL,
  `iCreditGLAccountID` int(11) DEFAULT NULL,
  `iAssetGLAccountID` int(11) DEFAULT NULL,
  `_btblFAAssetType_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetType_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaassetunitsofusage`
--

DROP TABLE IF EXISTS `_btblfaassetunitsofusage`;
CREATE TABLE `_btblfaassetunitsofusage` (
  `idAssetUnitNo` int(11) NOT NULL,
  `fNoOfUnits` double NOT NULL,
  `iAssetNo` int(11) NOT NULL,
  `iGLPeriodNo` int(11) NOT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `cDeprTypeInd` char(1) NOT NULL DEFAULT 'B',
  `_btblFAAssetUnitsOfUsage_iBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAAssetUnitsOfUsage_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfacapexbudget`
--

DROP TABLE IF EXISTS `_btblfacapexbudget`;
CREATE TABLE `_btblfacapexbudget` (
  `idCapexBudgetNo` int(11) NOT NULL,
  `cCapexDesc` varchar(80) NOT NULL,
  `cReplacementNewInd` char(1) NOT NULL,
  `iAssetTypeNo` int(11) NOT NULL,
  `iCostCenterNo` int(11) DEFAULT NULL,
  `fBudgetAmount` double NOT NULL,
  `fAmountSpent` double DEFAULT NULL,
  `fAmountCommited` double DEFAULT NULL,
  `dCapitalisationDate` datetime NOT NULL,
  `_btblFACapexBudget_iBranchID` int(11) DEFAULT NULL,
  `_btblFACapexBudget_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFACapexBudget_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFACapexBudget_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFACapexBudget_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFACapexBudget_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFACapexBudget_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFACapexBudget_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFACapexBudget_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfacapexorder`
--

DROP TABLE IF EXISTS `_btblfacapexorder`;
CREATE TABLE `_btblfacapexorder` (
  `idCapexOrderNo` int(11) NOT NULL,
  `cCapexOrderCode` varchar(30) NOT NULL,
  `iCapexBudgetNo` int(11) NOT NULL,
  `dCapexOrderDate` datetime NOT NULL,
  `fCapexOrderAmount` double NOT NULL,
  `cCapexOrderDesc` varchar(80) NOT NULL,
  `_btblFACapexOrder_iBranchID` int(11) DEFAULT NULL,
  `_btblFACapexOrder_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFACapexOrder_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFACapexOrder_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFACapexOrder_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFACapexOrder_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFACapexOrder_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFACapexOrder_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFACapexOrder_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfacapexphasing`
--

DROP TABLE IF EXISTS `_btblfacapexphasing`;
CREATE TABLE `_btblfacapexphasing` (
  `idCapexPhasingNo` int(11) NOT NULL,
  `dPhaseDate` datetime NOT NULL,
  `fPhaseAmount` double NOT NULL,
  `iCapexBudgetNo` int(11) NOT NULL,
  `_btblFACapexPhasing_iBranchID` int(11) DEFAULT NULL,
  `_btblFACapexPhasing_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFACapexPhasing_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFACapexPhasing_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFACapexPhasing_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFACapexPhasing_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFACapexPhasing_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFACapexPhasing_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFACapexPhasing_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfacompanysetup`
--

DROP TABLE IF EXISTS `_btblfacompanysetup`;
CREATE TABLE `_btblfacompanysetup` (
  `iFACompanyNo` int(11) NOT NULL,
  `cDebitMethod` char(1) DEFAULT NULL,
  `cCapexInd` char(1) NOT NULL,
  `cDecimalInd` char(1) NOT NULL,
  `dLastYearEndRun` datetime DEFAULT NULL,
  `dLastImportDate` datetime DEFAULT NULL,
  `cGLIntegrationInd` char(1) NOT NULL,
  `cCalcStartDateInd` char(1) NOT NULL,
  `iCalcDateDayNo` int(11) DEFAULT NULL,
  `iNoPeriodsInYear` int(11) DEFAULT NULL,
  `bCCRequired` tinyint(1) DEFAULT NULL,
  `bLocationRequired` tinyint(1) DEFAULT NULL,
  `cIntegrationMethod` char(1) DEFAULT NULL,
  `iGLPeriodID` int(11) DEFAULT NULL,
  `iGLIntegrationOption` int(11) DEFAULT NULL,
  `iPastelJrTypeNo` int(11) DEFAULT NULL,
  `cPastelDataPath` varchar(256) DEFAULT NULL,
  `cPastelAppPath` varchar(256) DEFAULT NULL,
  `iJrBatchID` int(11) DEFAULT NULL,
  `cAutoYN` varchar(1) DEFAULT NULL,
  `iAutoLength` int(11) DEFAULT NULL,
  `iAutoAlphaLength` int(11) DEFAULT NULL,
  `cUpperAccNo` varchar(1) DEFAULT NULL,
  `iPastelTxJrTypeNo` int(11) DEFAULT NULL,
  `iTxJrBatchID` int(11) DEFAULT NULL,
  `bCreateTxJrEntries` tinyint(1) NOT NULL DEFAULT '0',
  `iPastelSDKVersionID` int(11) DEFAULT NULL,
  `bTrackingAuto` tinyint(1) DEFAULT NULL,
  `cTrackingNum` varchar(15) DEFAULT NULL,
  `cTrackingPrefix` varchar(20) DEFAULT NULL,
  `iTrackingPad` int(11) DEFAULT NULL,
  `bDepreciateByAccPeriod` tinyint(1) NOT NULL,
  `_btblFACompanySetup_iBranchID` int(11) DEFAULT NULL,
  `_btblFACompanySetup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFACompanySetup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFACompanySetup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFACompanySetup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFACompanySetup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFACompanySetup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFACompanySetup_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFACompanySetup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfadepreciationmethod`
--

DROP TABLE IF EXISTS `_btblfadepreciationmethod`;
CREATE TABLE `_btblfadepreciationmethod` (
  `idDepreciationNo` int(11) NOT NULL,
  `cDepreciationDesc` varchar(80) NOT NULL,
  `cBasisInd` char(1) NOT NULL,
  `fPercentage` double NOT NULL,
  `iNoYears` int(11) DEFAULT NULL,
  `iUnitsUsage` int(11) DEFAULT NULL,
  `fInitialPercentage` double DEFAULT NULL,
  `fResidualValue` double DEFAULT NULL,
  `cSystemInd` char(1) NOT NULL,
  `_btblFADepreciationMethod_iBranchID` int(11) DEFAULT NULL,
  `_btblFADepreciationMethod_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFADepreciationMethod_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFADepreciationMethod_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFADepreciationMethod_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFADepreciationMethod_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFADepreciationMethod_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFADepreciationMethod_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFADepreciationMethod_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfadepreciationyear`
--

DROP TABLE IF EXISTS `_btblfadepreciationyear`;
CREATE TABLE `_btblfadepreciationyear` (
  `idDepreciationYearNo` int(11) NOT NULL,
  `iYearNo` int(11) NOT NULL,
  `fPercentage` double NOT NULL,
  `iDepreciationNo` int(11) NOT NULL,
  `_btblFADepreciationYear_iBranchID` int(11) DEFAULT NULL,
  `_btblFADepreciationYear_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFADepreciationYear_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFADepreciationYear_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFADepreciationYear_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFADepreciationYear_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFADepreciationYear_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFADepreciationYear_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFADepreciationYear_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfafinancemethod`
--

DROP TABLE IF EXISTS `_btblfafinancemethod`;
CREATE TABLE `_btblfafinancemethod` (
  `idFinMethod` int(11) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `_btblFAFinanceMethod_iBranchID` int(11) DEFAULT NULL,
  `_btblFAFinanceMethod_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAFinanceMethod_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAFinanceMethod_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAFinanceMethod_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAFinanceMethod_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAFinanceMethod_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAFinanceMethod_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAFinanceMethod_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaglbatch`
--

DROP TABLE IF EXISTS `_btblfaglbatch`;
CREATE TABLE `_btblfaglbatch` (
  `idBatch` int(11) NOT NULL,
  `cDescription` varchar(50) NOT NULL,
  `dDateRun` datetime(6) NOT NULL,
  `bPosted` tinyint(1) NOT NULL,
  `dBatchDate` datetime DEFAULT NULL,
  `iBatchType` int(11) NOT NULL DEFAULT '0',
  `_btblFAGLBatch_iBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatch_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLBatch_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLBatch_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatch_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatch_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLBatch_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLBatch_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAGLBatch_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaglbatchassetvalues`
--

DROP TABLE IF EXISTS `_btblfaglbatchassetvalues`;
CREATE TABLE `_btblfaglbatchassetvalues` (
  `idBatchAssetValues` int(11) NOT NULL,
  `iBatchID` int(11) DEFAULT NULL,
  `iAssetID` int(11) DEFAULT NULL,
  `dDate` datetime(6) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `bInitialAllowance` tinyint(1) NOT NULL DEFAULT '0',
  `_btblFAGLBatchAssetValues_iBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAGLBatchAssetValues_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaglbatchglentries`
--

DROP TABLE IF EXISTS `_btblfaglbatchglentries`;
CREATE TABLE `_btblfaglbatchglentries` (
  `idBatchGLEntries` int(11) NOT NULL,
  `iBatchID` int(11) DEFAULT NULL,
  `iGLAccountID` int(11) DEFAULT NULL,
  `dDate` datetime(6) DEFAULT NULL,
  `fDrAmount` double DEFAULT NULL,
  `fCrAmount` double DEFAULT NULL,
  `iAssetTypeID` int(11) DEFAULT NULL,
  `iCostCentreID` int(11) DEFAULT NULL,
  `iAssetCount` int(11) DEFAULT NULL,
  `cGLDescription` varchar(100) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_iBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAGLBatchGLEntries_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaglperiod`
--

DROP TABLE IF EXISTS `_btblfaglperiod`;
CREATE TABLE `_btblfaglperiod` (
  `idGLPeriodNo` int(11) NOT NULL,
  `dStartDate` datetime NOT NULL,
  `dEndDate` datetime NOT NULL,
  `cDescription` varchar(80) NOT NULL,
  `iYear` int(11) NOT NULL,
  `iPeriodNo` int(11) NOT NULL,
  `bBlockInd` tinyint(1) DEFAULT NULL,
  `bClosedInd` tinyint(1) DEFAULT '0',
  `bReopened` tinyint(1) DEFAULT '0',
  `_btblFAGLPeriod_iBranchID` int(11) DEFAULT NULL,
  `_btblFAGLPeriod_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLPeriod_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLPeriod_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLPeriod_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLPeriod_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLPeriod_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLPeriod_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAGLPeriod_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfagltotalassetvalues`
--

DROP TABLE IF EXISTS `_btblfagltotalassetvalues`;
CREATE TABLE `_btblfagltotalassetvalues` (
  `idTotalAssetValues` int(11) NOT NULL,
  `iAssetID` int(11) DEFAULT NULL,
  `dDate` datetime(6) DEFAULT NULL,
  `iPeriodID` int(11) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_iBranchID` int(11) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAGLTotalAssetValues_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfagltotalglentries`
--

DROP TABLE IF EXISTS `_btblfagltotalglentries`;
CREATE TABLE `_btblfagltotalglentries` (
  `idTotalGLEntries` int(11) NOT NULL,
  `iGLAccountID` int(11) DEFAULT NULL,
  `dDate` datetime(6) DEFAULT NULL,
  `iPeriodID` int(11) DEFAULT NULL,
  `fDrAmount` double DEFAULT NULL,
  `fCrAmount` double DEFAULT NULL,
  `_btblFAGLTotalGLEntries_iBranchID` int(11) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAGLTotalGLEntries_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfalocation`
--

DROP TABLE IF EXISTS `_btblfalocation`;
CREATE TABLE `_btblfalocation` (
  `idLocationNo` int(11) NOT NULL,
  `cLocationCode` varchar(30) NOT NULL,
  `cLocationDesc` varchar(80) NOT NULL,
  `_btblFALocation_iBranchID` int(11) DEFAULT NULL,
  `_btblFALocation_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFALocation_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFALocation_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFALocation_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFALocation_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFALocation_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFALocation_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFALocation_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfamovementtransaction`
--

DROP TABLE IF EXISTS `_btblfamovementtransaction`;
CREATE TABLE `_btblfamovementtransaction` (
  `idMovementTransactionNo` int(11) NOT NULL,
  `iAssetID` int(11) NOT NULL,
  `iTransactionTypeID` int(11) NOT NULL,
  `fNoOfUnits` double NOT NULL,
  `fAmount` double NOT NULL,
  `dMovementDate` datetime NOT NULL,
  `cReference` varchar(200) DEFAULT NULL,
  `iPeopleID` int(11) DEFAULT NULL,
  `cAssetCode` varchar(30) DEFAULT NULL,
  `iAssetTypeID` int(11) DEFAULT NULL,
  `iCostCentreID` int(11) DEFAULT NULL,
  `iLocationID` int(11) DEFAULT NULL,
  `DTStamp` datetime(6) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `cDeprTypeInd` varchar(1) DEFAULT NULL,
  `_btblFAMovementTransaction_iBranchID` int(11) DEFAULT NULL,
  `_btblFAMovementTransaction_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAMovementTransaction_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAMovementTransaction_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAMovementTransaction_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAMovementTransaction_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAMovementTransaction_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAMovementTransaction_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAMovementTransaction_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfaperiodclose`
--

DROP TABLE IF EXISTS `_btblfaperiodclose`;
CREATE TABLE `_btblfaperiodclose` (
  `idPeriodClose` int(11) NOT NULL,
  `cAssetCode` char(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `dPeriodCloseDate` datetime(6) DEFAULT NULL,
  `iGLPeriodNoID` int(11) NOT NULL,
  `fBookCurrentYearDepreciation` double DEFAULT NULL,
  `fBookPriorYearDepreciation` double DEFAULT NULL,
  `fWTCurrentYearDepreciation` double DEFAULT NULL,
  `fWTPriorYearDepreciation` double DEFAULT NULL,
  `fTotalBookBlockPeriod` double DEFAULT NULL,
  `fTotalWTBlockPeriod` double DEFAULT NULL,
  `bReopened` tinyint(1) DEFAULT '0',
  `_btblFAPeriodClose_iBranchID` int(11) DEFAULT NULL,
  `_btblFAPeriodClose_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFAPeriodClose_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFAPeriodClose_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFAPeriodClose_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFAPeriodClose_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFAPeriodClose_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFAPeriodClose_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFAPeriodClose_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblfatxdefaultglaccounts`
--

DROP TABLE IF EXISTS `_btblfatxdefaultglaccounts`;
CREATE TABLE `_btblfatxdefaultglaccounts` (
  `idTXDefaultGLAccount` int(11) NOT NULL,
  `cTransactionType` varchar(1) DEFAULT NULL,
  `iDebitGLAccountID` int(11) DEFAULT NULL,
  `iProfitGLAccountID` int(11) DEFAULT NULL,
  `iLossGLAccountID` int(11) DEFAULT NULL,
  `iRevaluationGLAccountID` int(11) DEFAULT NULL,
  `iCreditGLAccountID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_iBranchID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_iChangeSetID` int(11) DEFAULT NULL,
  `_btblFATxDefaultGLAccounts_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvcount`
--

DROP TABLE IF EXISTS `_btblinvcount`;
CREATE TABLE `_btblinvcount` (
  `idInvCount` int(11) NOT NULL,
  `cInvCountNo` varchar(50) DEFAULT NULL,
  `cDescription` varchar(60) DEFAULT NULL,
  `dPrepared` datetime(6) DEFAULT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `cStartCode` varchar(255) DEFAULT NULL,
  `cEndCode` varchar(255) DEFAULT NULL,
  `cGroups` varchar(1024) DEFAULT NULL,
  `cPacks` varchar(1024) DEFAULT NULL,
  `bIgnoreZero` tinyint(1) NOT NULL DEFAULT '0',
  `bIgnoreInactive` tinyint(1) NOT NULL DEFAULT '1',
  `iCount` int(11) NOT NULL DEFAULT '0',
  `iUncounted` int(11) NOT NULL DEFAULT '0',
  `iGroupBy` int(11) NOT NULL DEFAULT '0',
  `iSortBy` int(11) NOT NULL DEFAULT '0',
  `bIncludeSystemQty` tinyint(1) NOT NULL DEFAULT '1',
  `cWarehouses` varchar(1024) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `cBinLocations` varchar(1024) DEFAULT NULL,
  `iStartBinLocationID` int(11) DEFAULT NULL,
  `iEndBinLocationID` int(11) DEFAULT NULL,
  `cInventoryTypes` varchar(1024) DEFAULT NULL,
  `cLotStatus` varchar(1024) DEFAULT NULL,
  `bIncludeJCWIP` tinyint(1) NOT NULL DEFAULT '1',
  `bIncludeMFWIP` tinyint(1) NOT NULL DEFAULT '1',
  `bIncludeOrdResQty` tinyint(1) NOT NULL DEFAULT '0',
  `bDeleteAftComplete` tinyint(1) NOT NULL DEFAULT '1',
  `bIncludeDelivery` tinyint(1) NOT NULL DEFAULT '0',
  `bSpotCount` tinyint(1) NOT NULL DEFAULT '0',
  `bScheduledCount` tinyint(1) NOT NULL DEFAULT '0',
  `dScheduledDate` datetime(6) DEFAULT NULL,
  `iStatus` int(11) DEFAULT NULL,
  `bForceCapture` tinyint(1) NOT NULL DEFAULT '0',
  `cGroupsChkLstInd` char(1) DEFAULT NULL,
  `cPacksChkLstInd` char(1) DEFAULT NULL,
  `cWarehousesChkLstInd` char(1) DEFAULT NULL,
  `cBinLocationsChkLstInd` char(1) DEFAULT NULL,
  `cInventoryTypesChkLstInd` char(1) DEFAULT NULL,
  `cLotStatusChkLstInd` char(1) DEFAULT NULL,
  `bTakeSnapshot` tinyint(1) DEFAULT NULL,
  `cBinFromCode` varchar(30) DEFAULT NULL,
  `cBinToCode` varchar(30) DEFAULT NULL,
  `_btblInvCount_iBranchID` int(11) DEFAULT NULL,
  `_btblInvCount_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvCount_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvCount_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvCount_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvCount_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvCount_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvCount_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvCount_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvcountlines`
--

DROP TABLE IF EXISTS `_btblinvcountlines`;
CREATE TABLE `_btblinvcountlines` (
  `idInvCountLines` bigint(20) NOT NULL,
  `iInvCountID` int(11) NOT NULL,
  `cItemGroup` varchar(20) DEFAULT NULL,
  `cPack` varchar(5) DEFAULT NULL,
  `cBarcode` varchar(255) DEFAULT NULL,
  `fSystemQty` double DEFAULT NULL,
  `fCountQty` double DEFAULT NULL,
  `bModified` tinyint(1) NOT NULL DEFAULT '0',
  `bWhseItem` tinyint(1) NOT NULL DEFAULT '0',
  `bSerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `tSerialList` longtext,
  `bSNAllowDups` tinyint(1) NOT NULL DEFAULT '0',
  `iBinLocationId` int(11) DEFAULT NULL,
  `iLotTrackingID` int(11) DEFAULT NULL,
  `bLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iStockID` int(11) NOT NULL DEFAULT '0',
  `iWarehouseID` int(11) NOT NULL DEFAULT '0',
  `bUOMCounted` tinyint(1) NOT NULL DEFAULT '0',
  `bDimensionItem` tinyint(1) NOT NULL DEFAULT '0',
  `_btblInvCountLines_iBranchID` int(11) DEFAULT NULL,
  `_btblInvCountLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvCountLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvCountLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvCountLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvCountLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvCountLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvCountLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvCountLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvcountlinesuom`
--

DROP TABLE IF EXISTS `_btblinvcountlinesuom`;
CREATE TABLE `_btblinvcountlinesuom` (
  `IDInvCountLinesUOM` bigint(20) NOT NULL,
  `iInvCountID` int(11) NOT NULL,
  `iInvCountLinesID` bigint(20) NOT NULL,
  `iUnitsID` int(11) NOT NULL DEFAULT '0',
  `fUnitCountQty` double DEFAULT NULL,
  `iCountedPieces` int(11) NOT NULL DEFAULT '0',
  `fLength` double NOT NULL DEFAULT '0',
  `fHeight` double NOT NULL DEFAULT '0',
  `fWidth` double NOT NULL DEFAULT '0',
  `_btblInvCountLinesUOM_iBranchID` int(11) DEFAULT NULL,
  `_btblInvCountLinesUOM_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvCountLinesUOM_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvCountLinesUOM_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvCountLinesUOM_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvCountLinesUOM_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvCountLinesUOM_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvCountLinesUOM_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvCountLinesUOM_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvoicegrvsplit`
--

DROP TABLE IF EXISTS `_btblinvoicegrvsplit`;
CREATE TABLE `_btblinvoicegrvsplit` (
  `idInvoiceGrvSplit` int(11) NOT NULL,
  `iGrvSplitInvoiceID` bigint(20) NOT NULL,
  `iGrvSplitVendorID` int(11) NOT NULL,
  `cGRVSplitReference` varchar(20) DEFAULT NULL,
  `cGrvSplitDescription` varchar(30) DEFAULT NULL,
  `fGrvSplitAmount` double DEFAULT NULL,
  `iGrvSplitTaxTypeID` int(11) DEFAULT NULL,
  `fGrvSplitTaxAmnt` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `fForexRate` double DEFAULT NULL,
  `fForexAmount` double DEFAULT NULL,
  `_btblInvoiceGrvSplit_iBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceGrvSplit_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceGrvSplit_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceGrvSplit_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceGrvSplit_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceGrvSplit_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceGrvSplit_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceGrvSplit_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvoiceGrvSplit_Checksum` binary(20) DEFAULT NULL,
  `fForexTaxAmount` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvoicelines`
--

DROP TABLE IF EXISTS `_btblinvoicelines`;
CREATE TABLE `_btblinvoicelines` (
  `idInvoiceLines` bigint(20) NOT NULL,
  `iInvoiceID` int(11) NOT NULL,
  `iOrigLineID` bigint(20) DEFAULT NULL,
  `iGrvLineID` bigint(20) DEFAULT NULL,
  `iLineDocketMode` int(11) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `fQuantity` double DEFAULT NULL,
  `fQtyChange` double DEFAULT NULL,
  `fQtyToProcess` double DEFAULT NULL,
  `fQtyLastProcess` double DEFAULT NULL,
  `fQtyProcessed` double DEFAULT NULL,
  `fQtyReserved` double DEFAULT NULL,
  `fQtyReservedChange` double DEFAULT NULL,
  `cLineNotes` longtext,
  `fUnitPriceExcl` double DEFAULT NULL,
  `fUnitPriceIncl` double DEFAULT NULL,
  `iUnitPriceOverrideReasonID` int(11) DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `fLineDiscount` double DEFAULT NULL,
  `iLineDiscountReasonID` int(11) DEFAULT NULL,
  `iReturnReasonID` int(11) DEFAULT NULL,
  `fTaxRate` double DEFAULT NULL,
  `bIsSerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `bIsWhseItem` tinyint(1) NOT NULL DEFAULT '0',
  `fAddCost` double DEFAULT NULL,
  `cTradeinItem` varchar(20) DEFAULT NULL,
  `iStockCodeID` int(11) DEFAULT NULL,
  `iJobID` int(11) DEFAULT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iPriceListNameID` int(11) DEFAULT NULL,
  `fQuantityLineTotIncl` double DEFAULT NULL,
  `fQuantityLineTotExcl` double DEFAULT NULL,
  `fQuantityLineTotInclNoDisc` double DEFAULT NULL,
  `fQuantityLineTotExclNoDisc` double DEFAULT NULL,
  `fQuantityLineTaxAmount` double DEFAULT NULL,
  `fQuantityLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyChangeLineTotIncl` double DEFAULT NULL,
  `fQtyChangeLineTotExcl` double DEFAULT NULL,
  `fQtyChangeLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyChangeLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyChangeLineTaxAmount` double DEFAULT NULL,
  `fQtyChangeLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyToProcessLineTotIncl` double DEFAULT NULL,
  `fQtyToProcessLineTotExcl` double DEFAULT NULL,
  `fQtyToProcessLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyToProcessLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmount` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyLastProcessLineTotIncl` double DEFAULT NULL,
  `fQtyLastProcessLineTotExcl` double DEFAULT NULL,
  `fQtyLastProcessLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyLastProcessLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmount` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyProcessedLineTotIncl` double DEFAULT NULL,
  `fQtyProcessedLineTotExcl` double DEFAULT NULL,
  `fQtyProcessedLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyProcessedLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmount` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmountNoDisc` double DEFAULT NULL,
  `fUnitPriceExclForeign` double DEFAULT NULL,
  `fUnitPriceInclForeign` double DEFAULT NULL,
  `fUnitCostForeign` double DEFAULT NULL,
  `fAddCostForeign` double DEFAULT NULL,
  `fQuantityLineTotInclForeign` double DEFAULT NULL,
  `fQuantityLineTotExclForeign` double DEFAULT NULL,
  `fQuantityLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQuantityLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQuantityLineTaxAmountForeign` double DEFAULT NULL,
  `fQuantityLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyChangeLineTotInclForeign` double DEFAULT NULL,
  `fQtyChangeLineTotExclForeign` double DEFAULT NULL,
  `fQtyChangeLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyChangeLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyChangeLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyChangeLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotInclForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotExclForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotInclForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotExclForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotInclForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotExclForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `iLineRepID` int(11) DEFAULT NULL,
  `iLineProjectID` int(11) DEFAULT NULL,
  `iLedgerAccountID` int(11) DEFAULT NULL,
  `iModule` int(11) NOT NULL DEFAULT '0',
  `bChargeCom` tinyint(1) NOT NULL DEFAULT '1',
  `bIsLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iLotID` int(11) DEFAULT NULL,
  `cLotNumber` varchar(50) DEFAULT NULL,
  `dLotExpiryDate` datetime(6) DEFAULT NULL,
  `iMFPID` int(11) DEFAULT NULL,
  `iLineID` int(11) NOT NULL DEFAULT '0',
  `iLinkedLineID` bigint(20) NOT NULL DEFAULT '0',
  `fQtyLinkedUsed` double DEFAULT NULL,
  `fUnitPriceInclOrig` double DEFAULT NULL,
  `fUnitPriceExclOrig` double DEFAULT NULL,
  `fUnitPriceInclForeignOrig` double DEFAULT NULL,
  `fUnitPriceExclForeignOrig` double DEFAULT NULL,
  `iDeliveryMethodID` int(11) DEFAULT NULL,
  `fQtyDeliver` double DEFAULT NULL,
  `dDeliveryDate` datetime(6) DEFAULT NULL,
  `iDeliveryStatus` int(11) DEFAULT NULL,
  `fQtyForDelivery` double DEFAULT NULL,
  `bPromotionApplied` tinyint(1) NOT NULL DEFAULT '0',
  `fPromotionPriceExcl` double DEFAULT NULL,
  `fPromotionPriceIncl` double DEFAULT NULL,
  `cPromotionCode` varchar(20) DEFAULT NULL,
  `iSOLinkedPOLineID` bigint(20) NOT NULL DEFAULT '0',
  `fLength` double DEFAULT '0',
  `fWidth` double DEFAULT '0',
  `fHeight` double DEFAULT '0',
  `iPieces` int(11) DEFAULT '0',
  `iPiecesToProcess` int(11) DEFAULT '0',
  `iPiecesLastProcess` int(11) DEFAULT '0',
  `iPiecesProcessed` int(11) DEFAULT '0',
  `iPiecesReserved` int(11) DEFAULT '0',
  `iPiecesDeliver` int(11) DEFAULT '0',
  `iPiecesForDelivery` int(11) DEFAULT '0',
  `fQuantityUR` double DEFAULT NULL,
  `fQtyChangeUR` double DEFAULT NULL,
  `fQtyToProcessUR` double DEFAULT NULL,
  `fQtyLastProcessUR` double DEFAULT NULL,
  `fQtyProcessedUR` double DEFAULT NULL,
  `fQtyReservedUR` double DEFAULT NULL,
  `fQtyReservedChangeUR` double DEFAULT NULL,
  `fQtyDeliverUR` double DEFAULT NULL,
  `fQtyForDeliveryUR` double DEFAULT NULL,
  `fQtyLinkedUsedUR` double DEFAULT NULL,
  `iPiecesLinkedUsed` int(11) DEFAULT NULL,
  `iSalesWhseID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvoicelinesn`
--

DROP TABLE IF EXISTS `_btblinvoicelinesn`;
CREATE TABLE `_btblinvoicelinesn` (
  `idInvoiceLineSN` int(11) NOT NULL,
  `iSerialInvoiceID` int(11) NOT NULL,
  `iSerialInvoiceLineID` bigint(20) NOT NULL,
  `cSerialNumber` varchar(50) DEFAULT NULL,
  `_btblInvoiceLineSN_iBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLineSN_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceLineSN_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceLineSN_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLineSN_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLineSN_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceLineSN_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceLineSN_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvoiceLineSN_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblinvoicemessages`
--

DROP TABLE IF EXISTS `_btblinvoicemessages`;
CREATE TABLE `_btblinvoicemessages` (
  `idInvoiceMessages` int(11) NOT NULL,
  `iType` int(11) NOT NULL,
  `cDescription` varchar(50) NOT NULL,
  `cMessage1` varchar(255) DEFAULT NULL,
  `cMessage2` varchar(255) DEFAULT NULL,
  `cMessage3` varchar(255) DEFAULT NULL,
  `bIsExcessInvoice` tinyint(1) NOT NULL DEFAULT '0',
  `_btblInvoiceMessages_iBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceMessages_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceMessages_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblInvoiceMessages_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceMessages_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceMessages_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceMessages_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceMessages_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvoiceMessages_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljcinvoicelines`
--

DROP TABLE IF EXISTS `_btbljcinvoicelines`;
CREATE TABLE `_btbljcinvoicelines` (
  `idJCInvoiceLines` bigint(20) NOT NULL,
  `iJobNumID` int(11) NOT NULL,
  `iJobTxTpID` int(11) DEFAULT NULL,
  `bAdded` tinyint(1) NOT NULL DEFAULT '0',
  `iStockID` int(11) DEFAULT NULL,
  `iSupplierID` int(11) DEFAULT NULL,
  `iLedgerID` int(11) DEFAULT NULL,
  `iEmployeeId` int(11) DEFAULT NULL,
  `cDescription` varchar(150) DEFAULT NULL,
  `fQuantity` double DEFAULT NULL,
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `fUnitPriceExcl` double DEFAULT NULL,
  `fUnitPriceIncl` double DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `fLineDiscount` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `fTaxRate` double DEFAULT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iPriceListNameID` int(11) DEFAULT NULL,
  `fLineTotIncl` double DEFAULT NULL,
  `fLineTotExcl` double DEFAULT NULL,
  `fLineTotInclNoDisc` double DEFAULT NULL,
  `fLineTotExclNoDisc` double DEFAULT NULL,
  `fLineTotTaxAmount` double DEFAULT NULL,
  `fLineTotTaxAmountNoDisc` double DEFAULT NULL,
  `iJobStockGroupID` int(11) DEFAULT NULL,
  `iSerialNumberGroupID` int(11) DEFAULT NULL,
  `iSource` int(11) DEFAULT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fUnitPriceExclForeign` double DEFAULT NULL,
  `fUnitPriceInclForeign` double DEFAULT NULL,
  `fLineTotInclForeign` double DEFAULT NULL,
  `fLineTotExclForeign` double DEFAULT NULL,
  `fLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fLineTotTaxAmountForeign` double DEFAULT NULL,
  `fLineTotTaxAmountNoDiscForeign` double DEFAULT NULL,
  `iLineRepID` int(11) DEFAULT NULL,
  `iLineProjectID` int(11) DEFAULT NULL,
  `bChargeCom` tinyint(1) NOT NULL DEFAULT '1',
  `bLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iLotID` int(11) DEFAULT NULL,
  `cLotNumber` varchar(50) DEFAULT NULL,
  `dLotExpiryDate` datetime(6) DEFAULT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `iAPSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iInvEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iJCTxLinesID` bigint(20) DEFAULT NULL,
  `_btblJCInvoiceLines_iBranchID` int(11) DEFAULT NULL,
  `_btblJCInvoiceLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJCInvoiceLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJCInvoiceLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJCInvoiceLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJCInvoiceLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJCInvoiceLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJCInvoiceLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJCInvoiceLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljcmaster`
--

DROP TABLE IF EXISTS `_btbljcmaster`;
CREATE TABLE `_btbljcmaster` (
  `IdJCMaster` int(11) NOT NULL,
  `cJobCode` varchar(50) DEFAULT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `iStatus` int(11) DEFAULT NULL,
  `iPostingMethod` int(11) DEFAULT NULL,
  `iAccountsIdWIP` int(11) DEFAULT NULL,
  `iAccountsIdSales` int(11) NOT NULL,
  `iAccountsIdCOS` int(11) DEFAULT NULL,
  `iAccountsIdRecovery` int(11) DEFAULT NULL,
  `iClientId` int(11) DEFAULT NULL,
  `cOrderNo` varchar(50) DEFAULT NULL,
  `cFinalInvoiceNo` varchar(50) DEFAULT NULL,
  `cDeliveryNoteNo` varchar(50) DEFAULT NULL,
  `cFinalCheck` varchar(20) DEFAULT NULL,
  `cAuthorised` varchar(20) DEFAULT NULL,
  `dStartDate` datetime(6) DEFAULT NULL,
  `dCompletionDate` datetime(6) DEFAULT NULL,
  `dDeliveryDate` datetime(6) DEFAULT NULL,
  `dClosingDate` datetime(6) DEFAULT NULL,
  `fQuoteAmount` double DEFAULT NULL,
  `bIsTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `bFinal` tinyint(1) NOT NULL DEFAULT '0',
  `iDeliveryMethodID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `bInclusiveEntry` tinyint(1) NOT NULL DEFAULT '0',
  `bTaxPerLineEntry` tinyint(1) NOT NULL DEFAULT '1',
  `fDiscountPercent` double DEFAULT NULL,
  `iSalesRepId` int(11) DEFAULT NULL,
  `dInvDate` datetime(6) DEFAULT NULL,
  `cAddress1` varchar(40) DEFAULT NULL,
  `cAddress2` varchar(40) DEFAULT NULL,
  `cAddress3` varchar(40) DEFAULT NULL,
  `cAddress4` varchar(40) DEFAULT NULL,
  `cAddress5` varchar(40) DEFAULT NULL,
  `cAddress6` varchar(40) DEFAULT NULL,
  `cPAddress1` varchar(40) DEFAULT NULL,
  `cPAddress2` varchar(40) DEFAULT NULL,
  `cPAddress3` varchar(40) DEFAULT NULL,
  `cPAddress4` varchar(40) DEFAULT NULL,
  `cPAddress5` varchar(40) DEFAULT NULL,
  `cPAddress6` varchar(40) DEFAULT NULL,
  `cMessage1` varchar(255) DEFAULT NULL,
  `cMessage2` varchar(255) DEFAULT NULL,
  `cMessage3` varchar(255) DEFAULT NULL,
  `tNarration` longtext,
  `cExtOrderNo` varchar(50) DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `iJobSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iTxAddEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iTxRemEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iSortDocID` int(11) DEFAULT NULL,
  `bInventoryMade` tinyint(1) NOT NULL DEFAULT '0',
  `cJMAuditNumber` varchar(50) DEFAULT NULL,
  `_btblJCMaster_iBranchID` int(11) DEFAULT NULL,
  `_btblJCMaster_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJCMaster_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJCMaster_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJCMaster_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJCMaster_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJCMaster_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJCMaster_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJCMaster_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljctxlines`
--

DROP TABLE IF EXISTS `_btbljctxlines`;
CREATE TABLE `_btbljctxlines` (
  `idJCTxLines` bigint(20) NOT NULL,
  `iJCMasterID` int(11) DEFAULT NULL,
  `iJobTxTpID` int(11) DEFAULT NULL,
  `iSource` int(11) DEFAULT NULL,
  `iStockID` int(11) DEFAULT NULL,
  `iSupplierID` int(11) DEFAULT NULL,
  `iLedgerID` int(11) DEFAULT NULL,
  `cDescription` varchar(150) DEFAULT NULL,
  `iStatus` int(11) DEFAULT NULL,
  `iDuration` int(11) DEFAULT NULL,
  `dStartDate` datetime(6) DEFAULT NULL,
  `dEndDate` datetime(6) DEFAULT NULL,
  `fMainDiscount` double DEFAULT NULL,
  `fUnitPriceExcl` double DEFAULT NULL,
  `fUnitPriceIncl` double DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `fLineDiscount` double DEFAULT NULL,
  `iTaxTypeIDInv` int(11) DEFAULT NULL,
  `fTaxRateInv` double DEFAULT NULL,
  `fTransQty` double DEFAULT NULL,
  `fTransQtyToInvoice` double DEFAULT NULL,
  `fTransQtyInvoiced` double DEFAULT NULL,
  `fTransQtyAvailable` double DEFAULT NULL,
  `fTransQtyAdjusted` double NOT NULL DEFAULT '0',
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iPriceListNameID` int(11) DEFAULT NULL,
  `iTaxTypeIDGrv` int(11) DEFAULT NULL,
  `fTaxRateGrv` double DEFAULT NULL,
  `fTaxAmountGrv` double DEFAULT NULL,
  `iEmployeeID` int(11) DEFAULT NULL,
  `fBudgetUnitPriceExcl` double DEFAULT NULL,
  `fBudgetUnitPriceIncl` double DEFAULT NULL,
  `fBudgetUnitCost` double DEFAULT NULL,
  `fBudgetLineTotalExcl` double DEFAULT NULL,
  `fBudgetLineTotalIncl` double DEFAULT NULL,
  `fBudgetLineTotalTaxAmountInv` double DEFAULT NULL,
  `fBudgetLineTotalTaxAmountGrv` double DEFAULT NULL,
  `fBudgetLineTotalCost` double DEFAULT NULL,
  `fLineTotalExcl` double DEFAULT NULL,
  `fLineTotalIncl` double DEFAULT NULL,
  `fLineTotalTaxAmountInv` double DEFAULT NULL,
  `fLineTotalExclToInvoice` double DEFAULT NULL,
  `fLineTotalInclToInvoice` double DEFAULT NULL,
  `fLineTotalExclForeign` double DEFAULT NULL,
  `fLineTotalInclForeign` double DEFAULT NULL,
  `fLineTotalTaxAmountInvForeign` double DEFAULT NULL,
  `fLineTotalExclForeignToInvoice` double DEFAULT NULL,
  `fLineTotalInclForeignToInvoice` double DEFAULT NULL,
  `fLineTotalTaxAmountInvForeignToInvoice` double DEFAULT NULL,
  `fLineTotalTaxAmountInvToInvoice` double DEFAULT NULL,
  `fLineTotalExclInvoiced` double DEFAULT NULL,
  `fLineTotalInclInvoiced` double DEFAULT NULL,
  `fLineTotalTaxAmountInvInvoiced` double DEFAULT NULL,
  `fLineTotalExclForeignInvoiced` double DEFAULT NULL,
  `fLineTotalInclForeignInvoiced` double DEFAULT NULL,
  `fLineTotalTaxAmountInvForeignInvoiced` double DEFAULT NULL,
  `fLineTotalCost` double DEFAULT NULL,
  `fLineTotalCostInvoiced` double DEFAULT NULL,
  `bPosted` tinyint(1) NOT NULL DEFAULT '0',
  `bInvoiced` tinyint(1) NOT NULL DEFAULT '0',
  `iJobNumID` int(11) DEFAULT NULL,
  `cinvNumber` varchar(50) DEFAULT NULL,
  `cUserName` varchar(50) DEFAULT NULL,
  `iJobStockGroupID` int(11) DEFAULT NULL,
  `iSerialNumberGroupID` int(11) DEFAULT NULL,
  `iSerialNumberInvoicedGroupID` int(11) DEFAULT NULL,
  `iSerialNumberToInvoiceGroupID` int(11) DEFAULT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `iInvNumID` int(11) DEFAULT NULL,
  `bPicked` tinyint(1) NOT NULL DEFAULT '0',
  `fExchangeRate` double DEFAULT NULL,
  `fUnitPriceExclForeign` double DEFAULT NULL,
  `fUnitPriceInclForeign` double DEFAULT NULL,
  `fExchangeRateGrv` double DEFAULT NULL,
  `fTaxAmountGrvForeign` double DEFAULT NULL,
  `fBudgetUnitPriceExclForeign` double DEFAULT NULL,
  `fBudgetUnitPriceInclForeign` double DEFAULT NULL,
  `fBudgetLineTotalExclForeign` double DEFAULT NULL,
  `fBudgetLineTotalInclForeign` double DEFAULT NULL,
  `fBudgetLineTotalTaxAmountInvForeign` double DEFAULT NULL,
  `fBudgetLineTotalTaxAmountGrvForeign` double DEFAULT NULL,
  `iGrvCurrencyID` int(11) DEFAULT NULL,
  `iLineRepID` int(11) DEFAULT NULL,
  `iLineProjectID` int(11) DEFAULT NULL,
  `bChargeCom` tinyint(1) NOT NULL DEFAULT '1',
  `iMFPID` int(11) DEFAULT NULL,
  `iLotID` int(11) DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `iInvSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iAPSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iLineID` int(11) DEFAULT NULL,
  `_btblJCTxLines_iBranchID` int(11) DEFAULT NULL,
  `_btblJCTxLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJCTxLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJCTxLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJCTxLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJCTxLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJCTxLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJCTxLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJCTxLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljobfiscaltaxes`
--

DROP TABLE IF EXISTS `_btbljobfiscaltaxes`;
CREATE TABLE `_btbljobfiscaltaxes` (
  `idInvoiceTaxes` bigint(20) NOT NULL,
  `iInvoiceID` int(11) NOT NULL,
  `iTaxTypeA` int(11) DEFAULT NULL,
  `FTaxTypeAAm` double DEFAULT NULL,
  `fTaxTypeATax` double DEFAULT NULL,
  `iTaxTypeB` int(11) DEFAULT NULL,
  `fTaxTypeBAm` double DEFAULT NULL,
  `fTaxTypeBTax` double DEFAULT NULL,
  `iTaxTypeC` int(11) DEFAULT NULL,
  `fTaxTypeCAm` double DEFAULT NULL,
  `fTaxTypeCTax` double DEFAULT NULL,
  `iTaxTypeD` int(11) DEFAULT NULL,
  `fTaxTypeDAm` double DEFAULT NULL,
  `fTaxTypeDTax` double DEFAULT NULL,
  `iTaxTypeE` int(11) DEFAULT NULL,
  `fTaxTypeEAm` double DEFAULT NULL,
  `fTaxTypeETax` double DEFAULT NULL,
  `iTaxTypeF` int(11) DEFAULT NULL,
  `fTaxTypeFAm` double DEFAULT NULL,
  `fTaxTypeFTax` double DEFAULT NULL,
  `cSignatureNormal` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `DeviceDateNormalDoc` datetime DEFAULT NULL,
  `cSignatureCopy` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL,
  `DeviceDateCopyDoc` datetime DEFAULT NULL,
  `bDocType` tinyint(1) NOT NULL DEFAULT '0',
  `cMRCNormalDoc` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cMRCCopyDoc` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `_btblJobFiscalTaxes_iBranchID` int(11) DEFAULT NULL,
  `_btblJobFiscalTaxes_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJobFiscalTaxes_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJobFiscalTaxes_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJobFiscalTaxes_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJobFiscalTaxes_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJobFiscalTaxes_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJobFiscalTaxes_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJobFiscalTaxes_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljrbatchdefs`
--

DROP TABLE IF EXISTS `_btbljrbatchdefs`;
CREATE TABLE `_btbljrbatchdefs` (
  `idBatchDefs` int(11) NOT NULL,
  `bAutoNumbers` tinyint(1) NOT NULL,
  `iPadLength` int(11) DEFAULT NULL,
  `cPrefix` varchar(20) DEFAULT NULL,
  `iInputTaxID` int(11) DEFAULT NULL,
  `iInputTaxAccID` int(11) DEFAULT NULL,
  `iOutputTaxID` int(11) DEFAULT NULL,
  `iOutputTaxAccID` int(11) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `bBatchRefAutoNumbers` tinyint(1) NOT NULL DEFAULT '1',
  `iBatchRefPadLength` int(11) DEFAULT NULL,
  `cBatchRefPrefix` varchar(20) DEFAULT NULL,
  `iNextBatchRefNo` int(11) DEFAULT NULL,
  `bForceBatchRefNo` tinyint(1) NOT NULL DEFAULT '1',
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `iRevBatchID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_iBranchID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJrBatchDefs_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJrBatchDefs_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJrBatchDefs_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljrbatches`
--

DROP TABLE IF EXISTS `_btbljrbatches`;
CREATE TABLE `_btbljrbatches` (
  `idBatches` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDesc` varchar(40) DEFAULT NULL,
  `iInputTaxID` int(11) DEFAULT NULL,
  `iInputTaxAccID` int(11) DEFAULT NULL,
  `iOutputTaxID` int(11) DEFAULT NULL,
  `iOutputTaxAccID` int(11) DEFAULT NULL,
  `bCalcTax` tinyint(1) NOT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `bClearBatch` tinyint(1) NOT NULL,
  `iDateLineOpt` int(11) DEFAULT NULL,
  `dDefDate` datetime DEFAULT NULL,
  `iRefLineOpt` int(11) DEFAULT NULL,
  `cDefRef` varchar(20) DEFAULT NULL,
  `iDescLineOpt` int(11) DEFAULT NULL,
  `cDefDesc` varchar(40) DEFAULT NULL,
  `bCheckedOut` tinyint(1) DEFAULT '0',
  `iMaxRecur` int(11) DEFAULT NULL,
  `iBatchPosted` int(11) DEFAULT NULL,
  `cBatchRef` varchar(50) DEFAULT NULL,
  `bPromptGlobalChanges` tinyint(1) DEFAULT '0',
  `dDateBatchCreated` datetime(6) DEFAULT NULL,
  `iAgentBatchCreated` int(11) NOT NULL DEFAULT '1',
  `iAgentCheckedOut` int(11) NOT NULL DEFAULT '0',
  `bAccrualBatch` tinyint(1) DEFAULT '0',
  `iAccrualDateOpt` int(11) DEFAULT NULL,
  `dDefAccrualDay` int(11) DEFAULT NULL,
  `iAccrualRefOpt` int(11) DEFAULT NULL,
  `cDefAccrualRefPrefixOrSuffix` varchar(20) DEFAULT NULL,
  `dProcessedDate` datetime(6) DEFAULT NULL,
  `bInterBranchBatch` tinyint(1) NOT NULL DEFAULT '0',
  `iBranchLoanAccountID` int(11) DEFAULT NULL,
  `bRevaluationBatch` tinyint(1) NOT NULL DEFAULT '0',
  `_btblJrBatches_iBranchID` int(11) DEFAULT NULL,
  `_btblJrBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJrBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJrBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJrBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJrBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJrBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJrBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJrBatches_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbljrbatchlines`
--

DROP TABLE IF EXISTS `_btbljrbatchlines`;
CREATE TABLE `_btbljrbatchlines` (
  `idBatchLines` int(11) NOT NULL,
  `iBatchesID` int(11) NOT NULL,
  `dTxDate` datetime DEFAULT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `fDebit` double DEFAULT NULL,
  `fCredit` double DEFAULT NULL,
  `fTaxAmount` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iTaxAccountID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `bAccrual` tinyint(1) DEFAULT '0',
  `_btblJrBatchLines_iBranchID` int(11) DEFAULT NULL,
  `_btblJrBatchLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblJrBatchLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblJrBatchLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblJrBatchLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblJrBatchLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblJrBatchLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblJrBatchLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblJrBatchLines_Checksum` binary(20) DEFAULT NULL,
  `iFCAccCurrencyID` int(11) NOT NULL DEFAULT '0',
  `fExchangeRate` double NOT NULL DEFAULT '0',
  `fDebitForeign` double NOT NULL DEFAULT '0',
  `fCreditForeign` double NOT NULL DEFAULT '0',
  `fTaxAmountForeign` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbllogdetails`
--

DROP TABLE IF EXISTS `_btbllogdetails`;
CREATE TABLE `_btbllogdetails` (
  `idLogDetails` int(11) NOT NULL,
  `iLogMasterID` int(11) NOT NULL,
  `dGroupTime` datetime NOT NULL,
  `dTimeLogged` datetime(6) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `iSeverity` int(11) NOT NULL,
  `cDetails` varchar(1024) DEFAULT NULL,
  `cAddInfo` varchar(1024) DEFAULT NULL,
  `_btblLogDetails_iBranchID` int(11) DEFAULT NULL,
  `_btblLogDetails_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblLogDetails_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblLogDetails_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblLogDetails_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblLogDetails_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblLogDetails_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblLogDetails_iChangeSetID` int(11) DEFAULT NULL,
  `_btblLogDetails_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbllogmaster`
--

DROP TABLE IF EXISTS `_btbllogmaster`;
CREATE TABLE `_btbllogmaster` (
  `idLogMaster` int(11) NOT NULL,
  `iLogOrdinal` int(11) NOT NULL,
  `cLogName` varchar(50) NOT NULL,
  `_btblLogMaster_iBranchID` int(11) DEFAULT NULL,
  `_btblLogMaster_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblLogMaster_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblLogMaster_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblLogMaster_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblLogMaster_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblLogMaster_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblLogMaster_iChangeSetID` int(11) DEFAULT NULL,
  `_btblLogMaster_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblnotes`
--

DROP TABLE IF EXISTS `_btblnotes`;
CREATE TABLE `_btblnotes` (
  `idNotes` int(11) NOT NULL,
  `cNOTETBLTableName` varchar(30) DEFAULT NULL,
  `cNOTETBLTableID` varchar(50) DEFAULT NULL,
  `dNOTETBLCreated` datetime(6) DEFAULT NULL,
  `dNOTETBLModified` datetime(6) DEFAULT NULL,
  `iNOTETBLAgentID` int(11) DEFAULT NULL,
  `nNOTETBLText` longtext,
  `cNOTETBLTableKeyField` varchar(64) DEFAULT NULL,
  `_btblNotes_iBranchID` int(11) DEFAULT NULL,
  `_btblNotes_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblNotes_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblNotes_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblNotes_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblNotes_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblNotes_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblNotes_iChangeSetID` int(11) DEFAULT NULL,
  `_btblNotes_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblpostendertx`
--

DROP TABLE IF EXISTS `_btblpostendertx`;
CREATE TABLE `_btblpostendertx` (
  `IDPOSTenderTx` int(11) NOT NULL,
  `iTenderID` int(11) NOT NULL,
  `cNarrative` varchar(30) DEFAULT NULL,
  `fTxAmount` double DEFAULT NULL,
  `iPOSXZTableID` int(11) DEFAULT NULL,
  `cCardNumber` varchar(20) DEFAULT NULL,
  `cCardHolder` varchar(100) DEFAULT NULL,
  `dExpiryDate` datetime(6) DEFAULT NULL,
  `cEMVApplicationID` varchar(40) DEFAULT NULL,
  `cEMVVerification` varchar(10) DEFAULT NULL,
  `cEMVTrCertificate` varchar(20) DEFAULT NULL,
  `cEMVApplLabel` varchar(20) DEFAULT NULL,
  `cCardType` varchar(200) DEFAULT NULL,
  `cAuthCode` varchar(6) DEFAULT NULL,
  `dEFTDateTime` datetime(6) DEFAULT NULL,
  `cEMVTSI` varchar(4) DEFAULT NULL,
  `cEFTBudgetPeriod` varchar(2) NOT NULL DEFAULT '0',
  `cAuthorisationID` varchar(6) DEFAULT NULL,
  `cInstitutionID` varchar(11) DEFAULT NULL,
  `cTransactionType` varchar(2) DEFAULT NULL,
  `cAccountType` varchar(2) DEFAULT NULL,
  `cE0210RespCode` varchar(2) DEFAULT NULL,
  `cE0202RespCode` varchar(2) DEFAULT NULL,
  `bChipCard` tinyint(1) NOT NULL DEFAULT '0',
  `cEFTReferenceNumber` varchar(20) DEFAULT NULL,
  `bManualEFT` tinyint(1) NOT NULL DEFAULT '0',
  `_btblPOSTenderTx_iBranchID` int(11) DEFAULT NULL,
  `_btblPOSTenderTx_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblPOSTenderTx_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblPOSTenderTx_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblPOSTenderTx_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblPOSTenderTx_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblPOSTenderTx_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblPOSTenderTx_iChangeSetID` int(11) DEFAULT NULL,
  `_btblPOSTenderTx_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblposxztable`
--

DROP TABLE IF EXISTS `_btblposxztable`;
CREATE TABLE `_btblposxztable` (
  `IDPOSXZTable` int(11) NOT NULL,
  `dTranDate` datetime(6) DEFAULT NULL,
  `iTillTxType` int(11) DEFAULT NULL,
  `iTillsID` int(11) DEFAULT NULL,
  `iAgentsID` int(11) DEFAULT NULL,
  `iTrCodesID` int(11) DEFAULT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `cXZAuditNumber` varchar(50) DEFAULT NULL,
  `fAmtTendered` double DEFAULT NULL,
  `fTranAmount` double DEFAULT NULL,
  `fChange` double DEFAULT NULL,
  `_btblPOSXZTable_iBranchID` int(11) DEFAULT NULL,
  `_btblPOSXZTable_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblPOSXZTable_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblPOSXZTable_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblPOSXZTable_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblPOSXZTable_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblPOSXZTable_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblPOSXZTable_iChangeSetID` int(11) DEFAULT NULL,
  `_btblPOSXZTable_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblrbfolder`
--

DROP TABLE IF EXISTS `_btblrbfolder`;
CREATE TABLE `_btblrbfolder` (
  `IdFolder` int(11) NOT NULL,
  `cFolderName` varchar(60) NOT NULL,
  `iParentId` int(11) NOT NULL,
  `_btblRBFolder_iBranchID` int(11) DEFAULT NULL,
  `_btblRBFolder_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblRBFolder_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblRBFolder_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblRBFolder_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblRBFolder_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblRBFolder_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblRBFolder_iChangeSetID` int(11) DEFAULT NULL,
  `_btblRBFolder_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblrbitem`
--

DROP TABLE IF EXISTS `_btblrbitem`;
CREATE TABLE `_btblrbitem` (
  `IdItem` int(11) NOT NULL,
  `iFolderId` int(11) NOT NULL,
  `cItemName` varchar(60) NOT NULL,
  `iItemSize` int(11) DEFAULT NULL,
  `iItemType` int(11) NOT NULL,
  `dModified` datetime(6) NOT NULL,
  `dDeleted` datetime(6) DEFAULT NULL,
  `imgTemplate` longblob,
  `_btblRBItem_iBranchID` int(11) DEFAULT NULL,
  `_btblRBItem_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblRBItem_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblRBItem_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblRBItem_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblRBItem_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblRBItem_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblRBItem_iChangeSetID` int(11) DEFAULT NULL,
  `_btblRBItem_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblrbudeffield`
--

DROP TABLE IF EXISTS `_btblrbudeffield`;
CREATE TABLE `_btblrbudeffield` (
  `cTableName` varchar(60) NOT NULL,
  `cFieldName` varchar(60) NOT NULL,
  `cFieldAlias` varchar(60) DEFAULT NULL,
  `cDataType` varchar(60) DEFAULT NULL,
  `cSelectable` char(1) DEFAULT NULL,
  `cSearchable` char(1) DEFAULT NULL,
  `cSortable` char(1) DEFAULT NULL,
  `cAutoSearch` char(1) DEFAULT NULL,
  `cMandatory` char(1) DEFAULT NULL,
  `_btblRBUDefField_iBranchID` int(11) DEFAULT NULL,
  `_btblRBUDefField_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblRBUDefField_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblRBUDefField_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblRBUDefField_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblRBUDefField_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblRBUDefField_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblRBUDefField_iChangeSetID` int(11) DEFAULT NULL,
  `_btblRBUDefField_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblserialnumberlink`
--

DROP TABLE IF EXISTS `_btblserialnumberlink`;
CREATE TABLE `_btblserialnumberlink` (
  `IDSerialNumberLink` int(11) NOT NULL,
  `iSerialNumberGroupID` int(11) NOT NULL,
  `iSerialMfID` int(11) NOT NULL,
  `_btblSerialNumberLink_iBranchID` int(11) DEFAULT NULL,
  `_btblSerialNumberLink_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblSerialNumberLink_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblSerialNumberLink_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblSerialNumberLink_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblSerialNumberLink_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblSerialNumberLink_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblSerialNumberLink_iChangeSetID` int(11) DEFAULT NULL,
  `_btblSerialNumberLink_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblstate`
--

DROP TABLE IF EXISTS `_btblstate`;
CREATE TABLE `_btblstate` (
  `idState` int(11) NOT NULL,
  `cStateCode` varchar(10) NOT NULL,
  `cStateDescription` varchar(50) DEFAULT NULL,
  `_btblState_iBranchID` int(11) DEFAULT NULL,
  `_btblState_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblState_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblState_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblState_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblState_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblState_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblState_iChangeSetID` int(11) DEFAULT NULL,
  `_btblState_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblsystemfunction`
--

DROP TABLE IF EXISTS `_btblsystemfunction`;
CREATE TABLE `_btblsystemfunction` (
  `idSystemFunction` int(11) NOT NULL,
  `cFunctionDesc` varchar(100) DEFAULT NULL,
  `iParentSystemFunctionID` int(11) DEFAULT NULL,
  `iModulesHi` int(11) DEFAULT NULL,
  `iModulesLo` int(11) DEFAULT NULL,
  `bIsBusinessRule` tinyint(1) NOT NULL DEFAULT '0',
  `bCanDelete` tinyint(1) NOT NULL DEFAULT '0',
  `bIsSystemRule` tinyint(1) NOT NULL DEFAULT '1',
  `iAppID` int(11) DEFAULT NULL,
  `gIdentifier` varchar(64) DEFAULT NULL,
  `_btblSystemFunction_iBranchID` int(11) DEFAULT NULL,
  `_btblSystemFunction_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblSystemFunction_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblSystemFunction_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblSystemFunction_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblSystemFunction_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblSystemFunction_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblSystemFunction_iChangeSetID` int(11) DEFAULT NULL,
  `_btblSystemFunction_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btblsystemtree`
--

DROP TABLE IF EXISTS `_btblsystemtree`;
CREATE TABLE `_btblsystemtree` (
  `idSystemTree` int(11) NOT NULL,
  `iModuleLo` int(11) DEFAULT NULL,
  `iModuleHi` int(11) DEFAULT NULL,
  `iForceModulesLo` int(11) DEFAULT NULL,
  `iForceModulesHi` int(11) DEFAULT NULL,
  `mDisableForModulesLo` int(11) DEFAULT NULL,
  `mDisableForModulesHi` int(11) DEFAULT NULL,
  `iOrder` int(11) NOT NULL,
  `cPackage` varchar(32) NOT NULL,
  `cDescription` varchar(64) DEFAULT NULL,
  `iType` int(11) NOT NULL,
  `cObject` varchar(50) DEFAULT NULL,
  `cCommand` varchar(256) DEFAULT NULL,
  `bAlwaysLoad` tinyint(1) NOT NULL DEFAULT '0',
  `cToolbar` varchar(32) DEFAULT NULL,
  `cMenu` varchar(32) DEFAULT NULL,
  `cMenuName` varchar(50) DEFAULT NULL,
  `bMenuSub` tinyint(1) NOT NULL DEFAULT '0',
  `bHideNoChild` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowDesign` tinyint(1) NOT NULL DEFAULT '1',
  `bAdminOnly` tinyint(1) NOT NULL DEFAULT '0',
  `bSysNode` tinyint(1) NOT NULL DEFAULT '0',
  `bExcludeBranches` tinyint(1) NOT NULL DEFAULT '0',
  `iAppID` int(11) DEFAULT NULL,
  `gIdentifier` varchar(64) DEFAULT NULL,
  `_btblSystemTree_iBranchID` int(11) DEFAULT NULL,
  `_btblSystemTree_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblSystemTree_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblSystemTree_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblSystemTree_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblSystemTree_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblSystemTree_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblSystemTree_iChangeSetID` int(11) DEFAULT NULL,
  `_btblSystemTree_Checksum` binary(20) DEFAULT NULL,
  `bEvolutionHandled` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbltmcalcsheet`
--

DROP TABLE IF EXISTS `_btbltmcalcsheet`;
CREATE TABLE `_btbltmcalcsheet` (
  `idCalcSheet` int(11) NOT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `cOperation` char(1) NOT NULL,
  `cDescription` varchar(100) NOT NULL,
  `bEditable` tinyint(1) NOT NULL DEFAULT '0',
  `iTaxBoxID` int(11) DEFAULT NULL,
  `iTaxBoxDestID` int(11) NOT NULL DEFAULT '0',
  `iOrder` int(11) NOT NULL DEFAULT '1',
  `fValue` double DEFAULT NULL,
  `bDirect` tinyint(1) NOT NULL DEFAULT '0',
  `fTaxRate` double DEFAULT NULL,
  `dStartDate` datetime(6) DEFAULT NULL,
  `dEndDate` datetime(6) DEFAULT NULL,
  `_btblTMCalcSheet_iBranchID` int(11) DEFAULT NULL,
  `_btblTMCalcSheet_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblTMCalcSheet_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblTMCalcSheet_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblTMCalcSheet_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblTMCalcSheet_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblTMCalcSheet_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblTMCalcSheet_iChangeSetID` int(11) DEFAULT NULL,
  `_btblTMCalcSheet_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbltmtaxbox`
--

DROP TABLE IF EXISTS `_btbltmtaxbox`;
CREATE TABLE `_btbltmtaxbox` (
  `idTaxBox` int(11) NOT NULL,
  `cCode` varchar(4) NOT NULL,
  `cDescription` varchar(100) NOT NULL,
  `bTax` tinyint(1) NOT NULL DEFAULT '1',
  `bPayroll` tinyint(1) NOT NULL DEFAULT '1',
  `bWithHolding` tinyint(1) NOT NULL DEFAULT '1',
  `iGLSign` int(11) DEFAULT '1',
  `dStartDate` datetime(6) DEFAULT NULL,
  `dEndDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxBox_iBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxBox_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxBox_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxBox_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxBox_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxBox_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblTMTaxBox_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblTMTaxBox_iChangeSetID` int(11) DEFAULT NULL,
  `_btblTMTaxBox_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbltmtaxperiod`
--

DROP TABLE IF EXISTS `_btbltmtaxperiod`;
CREATE TABLE `_btbltmtaxperiod` (
  `idTaxPeriod` int(11) NOT NULL,
  `dStartDate` datetime NOT NULL,
  `dEndDate` datetime NOT NULL,
  `cDescription` varchar(80) NOT NULL,
  `iYear` int(11) NOT NULL,
  `iPeriodNo` int(11) NOT NULL,
  `bWithHolding` tinyint(1) NOT NULL DEFAULT '1',
  `bPayroll` tinyint(1) NOT NULL DEFAULT '1',
  `bTax` tinyint(1) NOT NULL DEFAULT '1',
  `bClosed` tinyint(1) NOT NULL DEFAULT '0',
  `_btblTMTaxPeriod_iBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxPeriod_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxPeriod_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxPeriod_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxPeriod_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxPeriod_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblTMTaxPeriod_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblTMTaxPeriod_iChangeSetID` int(11) DEFAULT NULL,
  `_btblTMTaxPeriod_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_btbltmtaxtotals`
--

DROP TABLE IF EXISTS `_btbltmtaxtotals`;
CREATE TABLE `_btbltmtaxtotals` (
  `idTaxTotals` int(11) NOT NULL,
  `fTotal` double NOT NULL,
  `iTaxPeriodID` int(11) NOT NULL,
  `iTaxBoxID` int(11) NOT NULL,
  `_btblTMTaxTotals_iBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxTotals_dCreatedDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxTotals_dModifiedDate` datetime(6) DEFAULT NULL,
  `_btblTMTaxTotals_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxTotals_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblTMTaxTotals_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblTMTaxTotals_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblTMTaxTotals_iChangeSetID` int(11) DEFAULT NULL,
  `_btblTMTaxTotals_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbladdinregister`
--

DROP TABLE IF EXISTS `_etbladdinregister`;
CREATE TABLE `_etbladdinregister` (
  `idAddIn` int(11) NOT NULL,
  `cAddInGuid` varchar(64) NOT NULL,
  `cName` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `cPublisher` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `cPublisherContact` varchar(15) CHARACTER SET utf8mb4 NOT NULL,
  `cPublisherTel` varchar(15) CHARACTER SET utf8mb4 NOT NULL,
  `cPublisherHomePage` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `cPublisherEmail` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `bAddInEnabled` tinyint(1) NOT NULL,
  `fAddInVersion` double NOT NULL,
  `_etblAddinRegister_iBranchID` int(11) DEFAULT NULL,
  `_etblAddinRegister_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAddinRegister_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAddinRegister_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAddinRegister_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAddinRegister_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAddinRegister_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAddinRegister_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAddinRegister_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbladditionalcharges`
--

DROP TABLE IF EXISTS `_etbladditionalcharges`;
CREATE TABLE `_etbladditionalcharges` (
  `idAdditionalCharge` int(11) NOT NULL,
  `cCode` varchar(20) DEFAULT NULL,
  `cDescription` varchar(120) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL,
  `bIsPercent` tinyint(1) NOT NULL DEFAULT '0',
  `bInclusive` tinyint(1) NOT NULL,
  `fMinAmt` double DEFAULT NULL,
  `fMinPct` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iAccountLink` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_iBranchID` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAdditionalCharges_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAdditionalCharges_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAdditionalCharges_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblagentdocprofiles`
--

DROP TABLE IF EXISTS `_etblagentdocprofiles`;
CREATE TABLE `_etblagentdocprofiles` (
  `idAgentDocProfiles` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `iDocProfileID` int(11) NOT NULL,
  `bActiveDocProfile` tinyint(1) NOT NULL DEFAULT '0',
  `_etblAgentDocProfiles_iBranchID` int(11) DEFAULT NULL,
  `_etblAgentDocProfiles_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAgentDocProfiles_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAgentDocProfiles_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAgentDocProfiles_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAgentDocProfiles_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAgentDocProfiles_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAgentDocProfiles_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAgentDocProfiles_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblagentpwdhistory`
--

DROP TABLE IF EXISTS `_etblagentpwdhistory`;
CREATE TABLE `_etblagentpwdhistory` (
  `iAgentID` int(11) NOT NULL,
  `cPassword` varchar(24) NOT NULL,
  `dPwdLastUsed` datetime DEFAULT NULL,
  `_etblAgentPwdHistory_iBranchID` int(11) DEFAULT NULL,
  `_etblAgentPwdHistory_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAgentPwdHistory_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAgentPwdHistory_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAgentPwdHistory_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAgentPwdHistory_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAgentPwdHistory_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAgentPwdHistory_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAgentPwdHistory_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblallocsdclinkrangetemp`
--

DROP TABLE IF EXISTS `_etblallocsdclinkrangetemp`;
CREATE TABLE `_etblallocsdclinkrangetemp` (
  `DCLink` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblallocstemp`
--

DROP TABLE IF EXISTS `_etblallocstemp`;
CREATE TABLE `_etblallocstemp` (
  `idAllocsTemp` int(11) NOT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `iFromRecID` bigint(20) DEFAULT NULL,
  `iToRecID` bigint(20) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `fAmountForeign` double DEFAULT NULL,
  `dFromRecDate` datetime(6) DEFAULT NULL,
  `dToRecDate` datetime(6) DEFAULT NULL,
  `iPLRecID` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblalloctemp`
--

DROP TABLE IF EXISTS `_etblalloctemp`;
CREATE TABLE `_etblalloctemp` (
  `TmpAutoIdx` bigint(20) DEFAULT NULL,
  `TmpTxTerm` int(11) DEFAULT NULL,
  `TmpAllocID` bigint(20) DEFAULT NULL,
  `TmpAllocAmt` double DEFAULT NULL,
  `TmpAllocDate` datetime(6) DEFAULT NULL,
  `TmpTotCredits` double DEFAULT NULL,
  `TmpTotDebits` double DEFAULT NULL,
  `TmpAllUACredits` double DEFAULT NULL,
  `TmpAllUADebits` double DEFAULT NULL,
  `TmpDCLink` int(11) DEFAULT NULL,
  `TmpPas_Cursor` int(11) DEFAULT NULL,
  `TmpTranModule` varchar(2) DEFAULT NULL,
  `TmpLinkedID` bigint(20) DEFAULT NULL,
  `TmpLineID` int(11) NOT NULL,
  `TmpRepID` int(11) DEFAULT NULL,
  `TmpStockAllocAmtExc` double DEFAULT NULL,
  `TmpStockCost` double DEFAULT NULL,
  `iTxBranchID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblapshareholderlinks`
--

DROP TABLE IF EXISTS `_etblapshareholderlinks`;
CREATE TABLE `_etblapshareholderlinks` (
  `idAPShareholderLinks` int(11) NOT NULL,
  `iAPShareholderID` int(11) NOT NULL,
  `iSupplierID` int(11) NOT NULL,
  `fPercentage` double NOT NULL,
  `cPositionHeld` varchar(50) NOT NULL,
  `bDirector` tinyint(1) DEFAULT '0',
  `_etblAPShareholderLinks_iBranchID` int(11) DEFAULT NULL,
  `_etblAPShareholderLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAPShareholderLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAPShareholderLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAPShareholderLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAPShareholderLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAPShareholderLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAPShareholderLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAPShareholderLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblapshareholders`
--

DROP TABLE IF EXISTS `_etblapshareholders`;
CREATE TABLE `_etblapshareholders` (
  `idAPShareholders` int(11) NOT NULL,
  `cName` varchar(50) NOT NULL,
  `cIDNumber` varchar(20) NOT NULL,
  `cCitizenship` varchar(50) DEFAULT NULL,
  `dCitizenshipAquired` datetime(6) DEFAULT NULL,
  `cRace` varchar(30) DEFAULT NULL,
  `cGender` varchar(1) DEFAULT NULL,
  `bDisabled` tinyint(1) DEFAULT '0',
  `bYouth` tinyint(1) DEFAULT '0',
  `cGovernmentEmployeeNumber` varchar(50) DEFAULT NULL,
  `cTaxNumber` varchar(50) DEFAULT NULL,
  `_etblAPShareholders_iBranchID` int(11) DEFAULT NULL,
  `_etblAPShareholders_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAPShareholders_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAPShareholders_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAPShareholders_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAPShareholders_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAPShareholders_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAPShareholders_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAPShareholders_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatchcontrasplit`
--

DROP TABLE IF EXISTS `_etblarapbatchcontrasplit`;
CREATE TABLE `_etblarapbatchcontrasplit` (
  `idARAPBatchContraSplit` int(11) NOT NULL,
  `iBatchID` int(11) NOT NULL,
  `iLinePermID` int(11) NOT NULL,
  `iGLAccountID` int(11) NOT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `fAmountForeign` double DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `fAmountIncl` double DEFAULT '0',
  `fAmountInclForeign` double DEFAULT '0',
  `iTaxAccountID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchContraSplit_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchContraSplit_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplit_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatchcontrasplithistory`
--

DROP TABLE IF EXISTS `_etblarapbatchcontrasplithistory`;
CREATE TABLE `_etblarapbatchcontrasplithistory` (
  `idARAPBatchContraSplitHistory` int(11) NOT NULL,
  `iBatchHistoryID` int(11) NOT NULL,
  `iBatchID` int(11) NOT NULL,
  `iLinePermID` int(11) NOT NULL,
  `iGLAccountID` int(11) NOT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `fAmountForeign` double DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `fAmountIncl` double NOT NULL DEFAULT '0',
  `fAmountInclForeign` double DEFAULT '0',
  `iTaxAccountID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatchContraSplitHistory_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatchdefaults`
--

DROP TABLE IF EXISTS `_etblarapbatchdefaults`;
CREATE TABLE `_etblarapbatchdefaults` (
  `idARAPBatchDefaults` int(11) NOT NULL,
  `iDCModule` int(11) NOT NULL,
  `bAutoNumbers` tinyint(1) NOT NULL DEFAULT '0',
  `iAutoNumPadLength` int(11) DEFAULT NULL,
  `cAutoNumPrefix` varchar(20) DEFAULT NULL,
  `bAutoRefNumbers` tinyint(1) NOT NULL DEFAULT '0',
  `iAutoRefNumPadLength` int(11) DEFAULT NULL,
  `cAutoRefNumPrefix` varchar(20) DEFAULT NULL,
  `_etblARAPBatchDefaults_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatchDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatches`
--

DROP TABLE IF EXISTS `_etblarapbatches`;
CREATE TABLE `_etblarapbatches` (
  `idARAPBatches` int(11) NOT NULL,
  `iDCModule` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDesc` varchar(50) NOT NULL,
  `cBatchRef` varchar(50) DEFAULT NULL,
  `bClearAfterPost` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowDupRef` tinyint(1) NOT NULL DEFAULT '0',
  `iCurrencyID` int(11) DEFAULT NULL,
  `bCurrencySingle` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineDateOpt` int(11) DEFAULT NULL,
  `dNewLineDateDef` datetime(6) DEFAULT NULL,
  `iNewLineRefOpt` int(11) DEFAULT NULL,
  `cNewLineRefDef` varchar(20) DEFAULT NULL,
  `bNewLineRefInc` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineDescOpt` int(11) DEFAULT NULL,
  `cNewLineDescDef` varchar(40) DEFAULT NULL,
  `bNewLineDescInc` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineTrCodeOpt` int(11) DEFAULT NULL,
  `iNewLineTrCodeDefID` int(11) DEFAULT NULL,
  `bNewLineTrCodeForce` tinyint(1) NOT NULL DEFAULT '0',
  `cOriginalBatchNo` varchar(50) DEFAULT NULL,
  `cOriginalbatchDesc` varchar(50) DEFAULT NULL,
  `iAgentCreatorID` int(11) DEFAULT NULL,
  `fValidateDebits` double DEFAULT NULL,
  `fValidateCredits` double DEFAULT NULL,
  `bShowGLContra` tinyint(1) NOT NULL DEFAULT '0',
  `bEditGLContra` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowGLContraSplit` tinyint(1) NOT NULL DEFAULT '0',
  `bEnterTaxOnGlContraSplit` tinyint(1) NOT NULL DEFAULT '0',
  `bIncludeLinkedAccounts` tinyint(1) NOT NULL DEFAULT '0',
  `bEnterExclOnGlContraSplit` tinyint(1) NOT NULL DEFAULT '1',
  `bValidateOverTerms` tinyint(1) NOT NULL DEFAULT '1',
  `bValidateOverLimit` tinyint(1) NOT NULL DEFAULT '1',
  `bInterBranchBatch` tinyint(1) NOT NULL DEFAULT '0',
  `iBranchLoanAccountID` int(11) DEFAULT NULL,
  `bModuleAR` tinyint(1) NOT NULL DEFAULT '1',
  `bModuleAP` tinyint(1) NOT NULL DEFAULT '1',
  `bModuleGL` tinyint(1) NOT NULL DEFAULT '1',
  `iInputTaxID` int(11) DEFAULT NULL,
  `iInputTaxAccID` int(11) DEFAULT NULL,
  `iOutputTaxID` int(11) DEFAULT NULL,
  `iOutputTaxAccID` int(11) DEFAULT NULL,
  `bCalcTax` tinyint(1) NOT NULL DEFAULT '0',
  `iDefaultModule` int(11) DEFAULT NULL,
  `_etblARAPBatches_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatches_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatchhistory`
--

DROP TABLE IF EXISTS `_etblarapbatchhistory`;
CREATE TABLE `_etblarapbatchhistory` (
  `idARAPBatchHistory` int(11) NOT NULL,
  `iBatchID` int(11) NOT NULL,
  `dActionDate` datetime(6) NOT NULL,
  `iActionType` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDesc` varchar(40) NOT NULL,
  `cBatchReference` varchar(50) DEFAULT NULL,
  `iLineCount` int(11) NOT NULL,
  `_etblARAPBatchHistory_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistory_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchHistory_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchHistory_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistory_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistory_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistory_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistory_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistory_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatchhistorylines`
--

DROP TABLE IF EXISTS `_etblarapbatchhistorylines`;
CREATE TABLE `_etblarapbatchhistorylines` (
  `idARAPBatchHistoryLines` int(11) NOT NULL,
  `iBatchHistoryID` int(11) DEFAULT NULL,
  `iBatchID` int(11) NOT NULL,
  `idLinePermanent` int(11) DEFAULT NULL,
  `dTxDate` datetime(6) DEFAULT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `iAccountCurrencyID` int(11) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `iGLContraID` int(11) DEFAULT NULL,
  `bPostDated` tinyint(1) NOT NULL DEFAULT '0',
  `cReference` varchar(50) DEFAULT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `cOrderNumber` varchar(20) DEFAULT NULL,
  `fAmountExcl` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `fAmountIncl` double DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fAmountExclForeign` double DEFAULT NULL,
  `fAmountInclForeign` double DEFAULT NULL,
  `fAccountExchangeRate` double DEFAULT NULL,
  `fAccountForeignAmountExcl` double DEFAULT NULL,
  `fAccountForeignAmountIncl` double DEFAULT NULL,
  `iDiscGLContraID` int(11) DEFAULT NULL,
  `fDiscPercent` double DEFAULT NULL,
  `fDiscAmountExcl` double DEFAULT NULL,
  `iDiscTaxTypeID` int(11) DEFAULT NULL,
  `fDiscAmountIncl` double DEFAULT NULL,
  `fDiscAmountExclForeign` double DEFAULT NULL,
  `fDiscAmountInclForeign` double DEFAULT NULL,
  `fAccountForeignDiscAmountExcl` double DEFAULT NULL,
  `fAccountForeignDiscAmountIncl` double DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iSalesRepID` int(11) DEFAULT NULL,
  `iBatchSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iModule` int(11) DEFAULT NULL,
  `iTaxAccountID` int(11) DEFAULT NULL,
  `bIsDebit` tinyint(1) NOT NULL DEFAULT '0',
  `_etblARAPBatchHistoryLines_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatchHistoryLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarapbatchlines`
--

DROP TABLE IF EXISTS `_etblarapbatchlines`;
CREATE TABLE `_etblarapbatchlines` (
  `idARAPBatchLines` int(11) NOT NULL,
  `iBatchID` int(11) NOT NULL,
  `idLinePermanent` int(11) DEFAULT NULL,
  `dTxDate` datetime(6) DEFAULT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `iAccountCurrencyID` int(11) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `iGLContraID` int(11) DEFAULT NULL,
  `bPostDated` tinyint(1) NOT NULL DEFAULT '0',
  `cReference` varchar(50) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `cOrderNumber` varchar(20) DEFAULT NULL,
  `fAmountExcl` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `fAmountIncl` double DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fAmountExclForeign` double DEFAULT NULL,
  `fAmountInclForeign` double DEFAULT NULL,
  `fAccountExchangeRate` double DEFAULT NULL,
  `fAccountForeignAmountExcl` double DEFAULT NULL,
  `fAccountForeignAmountIncl` double DEFAULT NULL,
  `iDiscGLContraID` int(11) DEFAULT NULL,
  `fDiscPercent` double DEFAULT NULL,
  `fDiscAmountExcl` double DEFAULT NULL,
  `iDiscTaxTypeID` int(11) DEFAULT NULL,
  `fDiscAmountIncl` double DEFAULT NULL,
  `fDiscAmountExclForeign` double DEFAULT NULL,
  `fDiscAmountInclForeign` double DEFAULT NULL,
  `fAccountForeignDiscAmountExcl` double DEFAULT NULL,
  `fAccountForeignDiscAmountIncl` double DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iSalesRepID` int(11) DEFAULT NULL,
  `iBatchSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `iModule` int(11) DEFAULT NULL,
  `iTaxAccountID` int(11) DEFAULT NULL,
  `bIsDebit` tinyint(1) NOT NULL DEFAULT '0',
  `iMBPropertyID` int(11) DEFAULT NULL,
  `iMBPortionID` int(11) DEFAULT NULL,
  `iMBServiceID` int(11) DEFAULT NULL,
  `iMBPropertyPortionServiceID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_iBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARAPBatchLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARAPBatchLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarstatementrun`
--

DROP TABLE IF EXISTS `_etblarstatementrun`;
CREATE TABLE `_etblarstatementrun` (
  `idStatementRun` int(11) NOT NULL,
  `cReference` varchar(255) DEFAULT NULL,
  `dRunGenerated` datetime(6) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `_etblARStatementRun_iBranchID` int(11) DEFAULT NULL,
  `_etblARStatementRun_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARStatementRun_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARStatementRun_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARStatementRun_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARStatementRun_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARStatementRun_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARStatementRun_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARStatementRun_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblarstatements`
--

DROP TABLE IF EXISTS `_etblarstatements`;
CREATE TABLE `_etblarstatements` (
  `idStatements` int(11) NOT NULL,
  `iStatementRunID` int(11) NOT NULL,
  `iClientID` int(11) NOT NULL,
  `nPDFDocument` longblob,
  `_etblARStatements_iBranchID` int(11) DEFAULT NULL,
  `_etblARStatements_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblARStatements_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblARStatements_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblARStatements_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblARStatements_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblARStatements_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblARStatements_iChangeSetID` int(11) DEFAULT NULL,
  `_etblARStatements_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblauditinglog`
--

DROP TABLE IF EXISTS `_etblauditinglog`;
CREATE TABLE `_etblauditinglog` (
  `idAuditingLog` int(11) NOT NULL,
  `dModificationDate` datetime(6) NOT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `cTableName` varchar(128) DEFAULT NULL,
  `iActionType` int(11) DEFAULT NULL,
  `cComment` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblautostrings`
--

DROP TABLE IF EXISTS `_etblautostrings`;
CREATE TABLE `_etblautostrings` (
  `idAutoStrings` int(11) NOT NULL,
  `cAutoString` varchar(5) NOT NULL,
  `bPublic` tinyint(1) NOT NULL DEFAULT '1',
  `cAutoText` varchar(120) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `_etblAutoStrings_iBranchID` int(11) DEFAULT NULL,
  `_etblAutoStrings_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblAutoStrings_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblAutoStrings_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblAutoStrings_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblAutoStrings_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblAutoStrings_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblAutoStrings_iChangeSetID` int(11) DEFAULT NULL,
  `_etblAutoStrings_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblbankdetails`
--

DROP TABLE IF EXISTS `_etblbankdetails`;
CREATE TABLE `_etblbankdetails` (
  `idBankDetail` int(11) NOT NULL,
  `cBankName` varchar(50) NOT NULL,
  `cBankAccName` varchar(50) DEFAULT NULL,
  `cBankCode` varchar(20) DEFAULT NULL,
  `cBankAccNumber` varchar(50) DEFAULT NULL,
  `cBranchName` varchar(50) DEFAULT NULL,
  `cBranchCode` varchar(20) DEFAULT NULL,
  `cBankRefNumber` varchar(50) DEFAULT NULL,
  `fAccLimit` double DEFAULT NULL,
  `cEFTSCode` varchar(20) DEFAULT NULL,
  `cEFTSName` varchar(50) DEFAULT NULL,
  `cEFTSReference` varchar(20) DEFAULT NULL,
  `iEFTSLayoutID` int(11) DEFAULT NULL,
  `cEFTSFile` varchar(50) DEFAULT NULL,
  `_etblBankDetails_iBranchID` int(11) DEFAULT NULL,
  `_etblBankDetails_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblBankDetails_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblBankDetails_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblBankDetails_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblBankDetails_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblBankDetails_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblBankDetails_iChangeSetID` int(11) DEFAULT NULL,
  `_etblBankDetails_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblbatchpermissions`
--

DROP TABLE IF EXISTS `_etblbatchpermissions`;
CREATE TABLE `_etblbatchpermissions` (
  `idBatchPermissions` int(11) NOT NULL,
  `iBatchID` int(11) NOT NULL DEFAULT '0',
  `cBatchType` varchar(1) NOT NULL DEFAULT 'C',
  `cAgentType` varchar(1) NOT NULL DEFAULT 'A',
  `iAgentID` int(11) NOT NULL DEFAULT '0',
  `bBatchVisible` tinyint(1) NOT NULL DEFAULT '1',
  `dDatePermissionCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iAgentPermissionCreated` int(11) NOT NULL DEFAULT '0',
  `_etblBatchPermissions_iBranchID` int(11) DEFAULT NULL,
  `_etblBatchPermissions_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblBatchPermissions_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblBatchPermissions_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblBatchPermissions_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblBatchPermissions_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblBatchPermissions_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblBatchPermissions_iChangeSetID` int(11) DEFAULT NULL,
  `_etblBatchPermissions_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblbranch`
--

DROP TABLE IF EXISTS `_etblbranch`;
CREATE TABLE `_etblbranch` (
  `idBranch` int(11) NOT NULL,
  `cBranchCode` varchar(20) DEFAULT NULL,
  `cBranchDescription` varchar(50) DEFAULT NULL,
  `bBranchActive` tinyint(1) NOT NULL DEFAULT '1',
  `bSyncEnabled` tinyint(1) NOT NULL DEFAULT '1',
  `dLastExport` datetime(6) DEFAULT NULL,
  `dLastImport` datetime(6) DEFAULT NULL,
  `iLastImportSeq` int(11) DEFAULT NULL,
  `iLastExportSeq` int(11) DEFAULT NULL,
  `iLastExportChangeSetID` int(11) DEFAULT NULL,
  `bIsDCBranch` tinyint(1) NOT NULL DEFAULT '0',
  `iDCWarehouseID` int(11) NOT NULL DEFAULT '0',
  `_etblBranch_iBranchID` int(11) DEFAULT NULL,
  `_etblBranch_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblBranch_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblBranch_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblBranch_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblBranch_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblBranch_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblBranch_iChangeSetID` int(11) DEFAULT NULL,
  `_etblBranch_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblbudgetsprev`
--

DROP TABLE IF EXISTS `_etblbudgetsprev`;
CREATE TABLE `_etblbudgetsprev` (
  `idBudgetsPrev` int(11) NOT NULL,
  `iBudgetPrevAccountID` int(11) NOT NULL,
  `iBudgetPrevProjectID` int(11) NOT NULL,
  `iBudgetPrevPeriodID` int(11) NOT NULL,
  `iBudgetPrevTxBranchID` int(11) NOT NULL,
  `iBudgetPrevAccountType` int(11) NOT NULL,
  `fBudgetPrev` double DEFAULT NULL,
  `fUnprocessedPOValuePrev` double DEFAULT NULL,
  `dBudgetPrevDTStamp` datetime(6) DEFAULT NULL,
  `_etblBudgetsPrev_iBranchID` int(11) DEFAULT NULL,
  `_etblBudgetsPrev_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblBudgetsPrev_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblBudgetsPrev_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblBudgetsPrev_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblBudgetsPrev_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblBudgetsPrev_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblBudgetsPrev_iChangeSetID` int(11) DEFAULT NULL,
  `_etblBudgetsPrev_Checksum` binary(20) DEFAULT NULL,
  `fBudgetPrevForeign` double NOT NULL DEFAULT '0',
  `fUnprocessedPOValuePrevForeign` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblcmagentcontact`
--

DROP TABLE IF EXISTS `_etblcmagentcontact`;
CREATE TABLE `_etblcmagentcontact` (
  `idContact` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cContactType` char(1) DEFAULT NULL,
  `iSourceID` int(11) DEFAULT NULL,
  `cLastName` varchar(255) NOT NULL,
  `cFirstName` varchar(255) DEFAULT NULL,
  `cEmailAddress` varchar(255) DEFAULT NULL,
  `cPhone` varchar(50) DEFAULT NULL,
  `_etblCMAgentContact_iBranchID` int(11) DEFAULT NULL,
  `_etblCMAgentContact_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblCMAgentContact_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblCMAgentContact_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblCMAgentContact_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblCMAgentContact_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblCMAgentContact_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblCMAgentContact_iChangeSetID` int(11) DEFAULT NULL,
  `_etblCMAgentContact_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblcmrejectreason`
--

DROP TABLE IF EXISTS `_etblcmrejectreason`;
CREATE TABLE `_etblcmrejectreason` (
  `idRejectReason` int(11) NOT NULL,
  `cRejectCode` varchar(20) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `_etblCMRejectReason_iBranchID` int(11) DEFAULT NULL,
  `_etblCMRejectReason_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblCMRejectReason_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblCMRejectReason_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblCMRejectReason_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblCMRejectReason_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblCMRejectReason_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblCMRejectReason_iChangeSetID` int(11) DEFAULT NULL,
  `_etblCMRejectReason_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldashboardlayouts`
--

DROP TABLE IF EXISTS `_etbldashboardlayouts`;
CREATE TABLE `_etbldashboardlayouts` (
  `idDashboardLayouts` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `iAgentGroupID` int(11) NOT NULL,
  `nLayout` longtext NOT NULL,
  `_etblDashboardLayouts_iBranchID` int(11) DEFAULT NULL,
  `_etblDashboardLayouts_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblDashboardLayouts_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblDashboardLayouts_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblDashboardLayouts_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblDashboardLayouts_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblDashboardLayouts_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblDashboardLayouts_iChangeSetID` int(11) DEFAULT NULL,
  `_etblDashboardLayouts_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldeladdress`
--

DROP TABLE IF EXISTS `_etbldeladdress`;
CREATE TABLE `_etbldeladdress` (
  `idDelAddress` int(11) NOT NULL,
  `iDelAddressCodeID` int(11) NOT NULL DEFAULT '0',
  `iAccountID` int(11) NOT NULL,
  `iDCModule` int(11) NOT NULL,
  `cDescription` varchar(60) DEFAULT NULL,
  `cDelAddress1` varchar(40) DEFAULT NULL,
  `cDelAddress2` varchar(40) DEFAULT NULL,
  `cDelAddress3` varchar(40) DEFAULT NULL,
  `cDelAddress4` varchar(40) DEFAULT NULL,
  `cDelAddress5` varchar(40) DEFAULT NULL,
  `cDelAddressPC` varchar(15) DEFAULT NULL,
  `cDelContact1` varchar(30) DEFAULT NULL,
  `cDelContact2` varchar(30) DEFAULT NULL,
  `cDelTelephone1` varchar(25) DEFAULT NULL,
  `cDelTelephone2` varchar(25) DEFAULT NULL,
  `cDelCellular` varchar(25) DEFAULT NULL,
  `cDelFax` varchar(25) DEFAULT NULL,
  `cEmail` varchar(200) DEFAULT NULL,
  `bDefault` tinyint(1) NOT NULL DEFAULT '0',
  `iSalesRepID` int(11) DEFAULT NULL,
  `iChargeTaxOpt` int(11) DEFAULT NULL,
  `dDelAddressTimeStamp` datetime(6) DEFAULT NULL,
  `iDefDeliveryMethodID` int(11) NOT NULL DEFAULT '0',
  `_etblDelAddress_iBranchID` int(11) DEFAULT NULL,
  `_etblDelAddress_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblDelAddress_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblDelAddress_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblDelAddress_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblDelAddress_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblDelAddress_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblDelAddress_iChangeSetID` int(11) DEFAULT NULL,
  `_etblDelAddress_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldeladdresscode`
--

DROP TABLE IF EXISTS `_etbldeladdresscode`;
CREATE TABLE `_etbldeladdresscode` (
  `IDDelAddressCode` int(11) NOT NULL,
  `cDelAddressCode` varchar(20) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `_etblDelAddressCode_iBranchID` int(11) DEFAULT NULL,
  `_etblDelAddressCode_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblDelAddressCode_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblDelAddressCode_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblDelAddressCode_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblDelAddressCode_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblDelAddressCode_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblDelAddressCode_iChangeSetID` int(11) DEFAULT NULL,
  `_etblDelAddressCode_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldeleted`
--

DROP TABLE IF EXISTS `_etbldeleted`;
CREATE TABLE `_etbldeleted` (
  `idDeleted` int(11) NOT NULL,
  `cTableName` varchar(128) DEFAULT NULL,
  `cPKFields` varchar(512) DEFAULT NULL,
  `cPKValues` varchar(128) DEFAULT NULL,
  `iRowBranchID` int(11) DEFAULT NULL,
  `iDeletedAtBranchID` int(11) DEFAULT NULL,
  `_auditDate` datetime(6) DEFAULT NULL,
  `_auditHostName` varchar(64) DEFAULT NULL,
  `_auditSystemUser` varchar(64) DEFAULT NULL,
  `_auditUserName` varchar(64) DEFAULT NULL,
  `_auditAppName` varchar(128) DEFAULT NULL,
  `iChangeSetID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldoccat`
--

DROP TABLE IF EXISTS `_etbldoccat`;
CREATE TABLE `_etbldoccat` (
  `idDocCat` int(11) NOT NULL,
  `cDocCatCode` varchar(30) NOT NULL,
  `cDocCatDescription` varchar(100) DEFAULT NULL,
  `cDocCatLocation` varchar(256) DEFAULT NULL,
  `iDocCatGroupID` int(11) DEFAULT '0',
  `_etblDocCat_iBranchID` int(11) DEFAULT NULL,
  `_etblDocCat_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblDocCat_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblDocCat_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblDocCat_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblDocCat_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblDocCat_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblDocCat_iChangeSetID` int(11) DEFAULT NULL,
  `_etblDocCat_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldoccatgroup`
--

DROP TABLE IF EXISTS `_etbldoccatgroup`;
CREATE TABLE `_etbldoccatgroup` (
  `idDocCatGroup` int(11) NOT NULL,
  `cDocCatGroupCode` varchar(30) NOT NULL,
  `cDocCatGroupDescription` varchar(100) DEFAULT NULL,
  `cDocCatGroupLocation` varchar(256) DEFAULT NULL,
  `_etblDocCatGroup_iBranchID` int(11) DEFAULT NULL,
  `_etblDocCatGroup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblDocCatGroup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblDocCatGroup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblDocCatGroup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblDocCatGroup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblDocCatGroup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblDocCatGroup_iChangeSetID` int(11) DEFAULT NULL,
  `_etblDocCatGroup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbldocprofiles`
--

DROP TABLE IF EXISTS `_etbldocprofiles`;
CREATE TABLE `_etbldocprofiles` (
  `idDocProfiles` int(11) NOT NULL,
  `cCode` varchar(20) NOT NULL,
  `cDescription` varchar(120) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '0',
  `iDocType` int(11) NOT NULL,
  `iDocSubType` int(11) NOT NULL,
  `bUseDefaults` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `iNextAutoNum` int(11) DEFAULT NULL,
  `iAutoNumPadLength` int(11) DEFAULT NULL,
  `cAutoNumPrefix` varchar(20) DEFAULT NULL,
  `bUniqueNum` tinyint(1) DEFAULT '0',
  `iDocTrCodeID` int(11) DEFAULT NULL,
  `iDocHomeLayoutID` int(11) DEFAULT NULL,
  `bDocHomeLayoutPrompt` tinyint(1) NOT NULL DEFAULT '0',
  `iDocForeignLayoutID` int(11) DEFAULT NULL,
  `bDocForeignLayoutPrompt` tinyint(1) NOT NULL DEFAULT '0',
  `iExclusiveDoc` int(11) DEFAULT NULL,
  `iTaxPerDoc` int(11) DEFAULT NULL,
  `bHasBeenUsed` tinyint(1) NOT NULL DEFAULT '0',
  `bIsUserLayout` tinyint(1) NOT NULL DEFAULT '0',
  `bIsUserForeignLayout` tinyint(1) NOT NULL DEFAULT '0',
  `iDocHomeEmailLayoutID` int(11) DEFAULT NULL,
  `bDocHomeEmailLayoutPrompt` tinyint(1) NOT NULL DEFAULT '0',
  `bIsUserEmailLayout` tinyint(1) NOT NULL DEFAULT '0',
  `iDocForeignEmailLayoutID` int(11) DEFAULT NULL,
  `bDocForeignEmailLayoutPrompt` tinyint(1) NOT NULL DEFAULT '0',
  `bIsUserForeignEmailLayout` tinyint(1) NOT NULL DEFAULT '0',
  `_etblDocProfiles_iBranchID` int(11) DEFAULT NULL,
  `_etblDocProfiles_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblDocProfiles_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblDocProfiles_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblDocProfiles_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblDocProfiles_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblDocProfiles_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblDocProfiles_iChangeSetID` int(11) DEFAULT NULL,
  `_etblDocProfiles_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleftgatewaytype`
--

DROP TABLE IF EXISTS `_etbleftgatewaytype`;
CREATE TABLE `_etbleftgatewaytype` (
  `GatewayID` int(11) NOT NULL,
  `cCode` smallint(6) DEFAULT NULL,
  `cGatewayName` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `_etblEFTGatewayType_iBranchID` int(11) DEFAULT NULL,
  `_etblEFTGatewayType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEFTGatewayType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEFTGatewayType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEFTGatewayType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEFTGatewayType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEFTGatewayType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEFTGatewayType_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEFTGatewayType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleftreference`
--

DROP TABLE IF EXISTS `_etbleftreference`;
CREATE TABLE `_etbleftreference` (
  `idNumber` int(11) NOT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `_etblEFTReference_iBranchID` int(11) DEFAULT NULL,
  `_etblEFTReference_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEFTReference_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEFTReference_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEFTReference_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEFTReference_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEFTReference_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEFTReference_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEFTReference_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleftsfilelayout`
--

DROP TABLE IF EXISTS `_etbleftsfilelayout`;
CREATE TABLE `_etbleftsfilelayout` (
  `idEFTSLayout` int(11) NOT NULL,
  `cDescription` varchar(200) NOT NULL,
  `bSystemLayout` tinyint(1) NOT NULL DEFAULT '1',
  `cNameOutFile` varchar(50) DEFAULT NULL,
  `cTypeOutFile` varchar(3) DEFAULT NULL,
  `iDelimiterOutFile` int(11) DEFAULT NULL,
  `iEOLCharOutFile` int(11) DEFAULT NULL,
  `iCountryCode` int(11) NOT NULL DEFAULT '0',
  `iNoOfHeaders` int(11) DEFAULT NULL,
  `iNoOFTransactions` int(11) DEFAULT NULL,
  `iNoOfFooters` int(11) DEFAULT NULL,
  `iMaxNoOfRecords` int(11) DEFAULT NULL,
  `iHashTotalCalc` int(11) NOT NULL DEFAULT '0',
  `cSeedNumber` varchar(32) DEFAULT NULL,
  `bAllowTestRun` tinyint(1) NOT NULL DEFAULT '0',
  `bAskBatchNo` tinyint(1) NOT NULL DEFAULT '0',
  `iDayDifference` int(11) NOT NULL DEFAULT '0',
  `iACBServiceType` int(11) NOT NULL DEFAULT '0',
  `bIncrementFileName` tinyint(1) NOT NULL DEFAULT '0',
  `cUserName` varchar(50) DEFAULT NULL,
  `cPassword` varchar(160) DEFAULT NULL,
  `cPin` varchar(160) DEFAULT NULL,
  `iEFTSOrderLetterID` int(11) NOT NULL DEFAULT '0',
  `iSourceLayoutID` int(11) DEFAULT '0',
  `cXSLT` longtext,
  `_etblEFTSFileLayout_iBranchID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayout_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEFTSFileLayout_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEFTSFileLayout_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayout_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayout_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayout_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayout_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayout_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleftsfilelayoutdetails`
--

DROP TABLE IF EXISTS `_etbleftsfilelayoutdetails`;
CREATE TABLE `_etbleftsfilelayoutdetails` (
  `idEFTSLayoutDetails` int(11) NOT NULL,
  `iEFTSLayoutID` int(11) NOT NULL,
  `cRecordType` varchar(50) NOT NULL,
  `cDescription` varchar(30) NOT NULL,
  `iFieldLength` int(11) NOT NULL,
  `iDecimalPlaces` int(11) DEFAULT NULL,
  `cFieldFormat` varchar(30) DEFAULT NULL,
  `iFiller` int(11) DEFAULT NULL,
  `bFieldInUse` tinyint(1) NOT NULL DEFAULT '1',
  `cValueType` varchar(1) DEFAULT NULL,
  `cDefaultValue` varchar(120) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_iBranchID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEFTSFileLayoutDetails_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleucommodity`
--

DROP TABLE IF EXISTS `_etbleucommodity`;
CREATE TABLE `_etbleucommodity` (
  `IDEUCommodity` int(11) NOT NULL,
  `cEUCommodityCode` varchar(20) DEFAULT NULL,
  `cEUCommodityDescription` varchar(1000) DEFAULT NULL,
  `_etblEUCommodity_iBranchID` int(11) DEFAULT NULL,
  `_etblEUCommodity_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEUCommodity_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEUCommodity_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEUCommodity_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEUCommodity_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEUCommodity_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEUCommodity_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEUCommodity_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleucountry`
--

DROP TABLE IF EXISTS `_etbleucountry`;
CREATE TABLE `_etbleucountry` (
  `IDEUCountry` int(11) NOT NULL,
  `cEUCountryCode` varchar(2) NOT NULL,
  `cEUCountryName` varchar(30) DEFAULT NULL,
  `_etblEUCountry_iBranchID` int(11) DEFAULT NULL,
  `_etblEUCountry_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEUCountry_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEUCountry_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEUCountry_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEUCountry_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEUCountry_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEUCountry_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEUCountry_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleunotc`
--

DROP TABLE IF EXISTS `_etbleunotc`;
CREATE TABLE `_etbleunotc` (
  `IDEUNoTC` int(11) NOT NULL,
  `cEUNoTCCode` varchar(2) DEFAULT NULL,
  `cEUNoTCDescription` varchar(200) DEFAULT NULL,
  `_etblEUNoTC_iBranchID` int(11) DEFAULT NULL,
  `_etblEUNoTC_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEUNoTC_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEUNoTC_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEUNoTC_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEUNoTC_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEUNoTC_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEUNoTC_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEUNoTC_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbleusupplementaryunit`
--

DROP TABLE IF EXISTS `_etbleusupplementaryunit`;
CREATE TABLE `_etbleusupplementaryunit` (
  `IDEUSupplementaryUnit` int(11) NOT NULL,
  `cEUSupplementaryUnitCode` varchar(20) DEFAULT NULL,
  `cEUSupplementaryUnitDescription` varchar(100) DEFAULT NULL,
  `_etblEUSupplementaryUnit_iBranchID` int(11) DEFAULT NULL,
  `_etblEUSupplementaryUnit_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblEUSupplementaryUnit_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblEUSupplementaryUnit_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblEUSupplementaryUnit_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblEUSupplementaryUnit_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblEUSupplementaryUnit_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblEUSupplementaryUnit_iChangeSetID` int(11) DEFAULT NULL,
  `_etblEUSupplementaryUnit_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblfiscalprintermodels`
--

DROP TABLE IF EXISTS `_etblfiscalprintermodels`;
CREATE TABLE `_etblfiscalprintermodels` (
  `iFiscalPrinterModelsId` int(11) NOT NULL,
  `cPrinterModelName` varchar(50) DEFAULT NULL,
  `cPrinterModelManufacture` varchar(15) DEFAULT NULL,
  `cFiscalDriverName` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bAllowAlphaNumDoc` tinyint(1) NOT NULL DEFAULT '1',
  `bIsPrinter` tinyint(1) NOT NULL DEFAULT '1',
  `_etblFiscalPrinterModels_iBranchID` int(11) DEFAULT NULL,
  `_etblFiscalPrinterModels_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblFiscalPrinterModels_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblFiscalPrinterModels_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblFiscalPrinterModels_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblFiscalPrinterModels_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblFiscalPrinterModels_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblFiscalPrinterModels_iChangeSetID` int(11) DEFAULT NULL,
  `_etblFiscalPrinterModels_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblfiscalprinters`
--

DROP TABLE IF EXISTS `_etblfiscalprinters`;
CREATE TABLE `_etblfiscalprinters` (
  `iFiscalPrinterId` int(11) NOT NULL,
  `iPOSDeviceId` int(11) DEFAULT NULL,
  `cIPAddress` varchar(15) DEFAULT NULL,
  `cPortNumber` varchar(15) DEFAULT NULL,
  `cSubnetMask` varchar(15) DEFAULT NULL,
  `cDefaultGateway` varchar(15) DEFAULT NULL,
  `cMACAddress` varchar(17) DEFAULT NULL,
  `iTaxTypeA` int(11) DEFAULT NULL,
  `iTaxTypeB` int(11) DEFAULT NULL,
  `iTaxTypeC` int(11) DEFAULT NULL,
  `iTaxTypeD` int(11) DEFAULT NULL,
  `iTaxTypeE` int(11) DEFAULT NULL,
  `iTaxTypeF` int(11) DEFAULT NULL,
  `bDeviceConfigured` tinyint(1) DEFAULT NULL,
  `cRegistrationKey` varchar(50) DEFAULT NULL,
  `cTINNumber` varchar(50) DEFAULT NULL,
  `cMRCNumber` varchar(50) DEFAULT NULL,
  `_etblFiscalPrinters_iBranchID` int(11) DEFAULT NULL,
  `_etblFiscalPrinters_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblFiscalPrinters_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblFiscalPrinters_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblFiscalPrinters_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblFiscalPrinters_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblFiscalPrinters_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblFiscalPrinters_iChangeSetID` int(11) DEFAULT NULL,
  `_etblFiscalPrinters_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblglaccounttypes`
--

DROP TABLE IF EXISTS `_etblglaccounttypes`;
CREATE TABLE `_etblglaccounttypes` (
  `idGLAccountType` int(11) NOT NULL,
  `cAccountTypeDescription` varchar(100) NOT NULL,
  `bIsBalanceSheet` tinyint(1) NOT NULL DEFAULT '1',
  `bIsDebit` tinyint(1) NOT NULL DEFAULT '1',
  `iReportingGroup` int(11) NOT NULL DEFAULT '1',
  `iReportingGroupSort` int(11) NOT NULL DEFAULT '1',
  `bDualPrinting` tinyint(1) NOT NULL DEFAULT '0',
  `_etblGLAccountTypes_iBranchID` int(11) DEFAULT NULL,
  `_etblGLAccountTypes_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblGLAccountTypes_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblGLAccountTypes_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblGLAccountTypes_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblGLAccountTypes_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblGLAccountTypes_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblGLAccountTypes_iChangeSetID` int(11) DEFAULT NULL,
  `_etblGLAccountTypes_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblglmscoaaccounts`
--

DROP TABLE IF EXISTS `_etblglmscoaaccounts`;
CREATE TABLE `_etblglmscoaaccounts` (
  `idGLmSCOAAccount` int(11) NOT NULL,
  `SCOAId` varchar(50) NOT NULL,
  `ParentSCOAId` varchar(50) DEFAULT NULL,
  `DefinitionDescription` longtext NOT NULL,
  `SCOAFile` varchar(20) NOT NULL,
  `SCOAAccount` varchar(255) NOT NULL,
  `SCOALevel` int(11) NOT NULL,
  `ExcelRowNumber` int(11) NOT NULL,
  `ShortDescription` varchar(255) NOT NULL,
  `VATStatus` varchar(50) DEFAULT NULL,
  `BreakDownAllowed` varchar(20) DEFAULT NULL,
  `Principle` varchar(255) DEFAULT NULL,
  `ApplicableTo` varchar(50) DEFAULT NULL,
  `bPostingLevel` tinyint(1) NOT NULL DEFAULT '1',
  `PostingLevelDescription` varchar(20) DEFAULT NULL,
  `AccountNumber` varchar(50) NOT NULL,
  `AccountNumberPrefix` varchar(10) NOT NULL,
  `AccountNumber1` int(11) NOT NULL,
  `AccountNumber2` int(11) NOT NULL,
  `AccountNumber3` int(11) NOT NULL,
  `AccountNumber4` int(11) NOT NULL,
  `AccountNumber5` int(11) NOT NULL,
  `AccountNumber6` int(11) NOT NULL,
  `AccountNumber7` int(11) NOT NULL,
  `AccountNumber8` int(11) NOT NULL,
  `AccountNumber9` int(11) NOT NULL,
  `AccountNumber10` int(11) NOT NULL,
  `AccountNumber11` int(11) NOT NULL,
  `AccountNumber12` int(11) NOT NULL,
  `GFSCode` varchar(20) DEFAULT NULL,
  `MSCOACheck` binary(20) DEFAULT NULL,
  `NextSubAccountNumber` int(11) NOT NULL DEFAULT '1',
  `_etblGLmSCOAAccounts_iBranchID` int(11) DEFAULT NULL,
  `_etblGLmSCOAAccounts_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblGLmSCOAAccounts_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblGLmSCOAAccounts_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblGLmSCOAAccounts_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblGLmSCOAAccounts_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblGLmSCOAAccounts_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblGLmSCOAAccounts_iChangeSetID` int(11) DEFAULT NULL,
  `_etblGLmSCOAAccounts_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblglreportcategory`
--

DROP TABLE IF EXISTS `_etblglreportcategory`;
CREATE TABLE `_etblglreportcategory` (
  `idReportCategory` int(11) NOT NULL,
  `cCode` varchar(40) NOT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `_etblGLReportCategory_iBranchID` int(11) DEFAULT NULL,
  `_etblGLReportCategory_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblGLReportCategory_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblGLReportCategory_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblGLReportCategory_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblGLReportCategory_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblGLReportCategory_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblGLReportCategory_iChangeSetID` int(11) DEFAULT NULL,
  `_etblGLReportCategory_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblglrevisebudget`
--

DROP TABLE IF EXISTS `_etblglrevisebudget`;
CREATE TABLE `_etblglrevisebudget` (
  `idGLReviseBudget` int(11) NOT NULL,
  `iGLAccountID` int(11) NOT NULL,
  `iProjectID` int(11) NOT NULL DEFAULT '0',
  `iPeriod` int(11) NOT NULL,
  `fNewBudget` double NOT NULL DEFAULT '0',
  `fOldBudget` double NOT NULL DEFAULT '0',
  `cReason` varchar(50) NOT NULL,
  `dDate` datetime(6) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `iTxBranchRevisedBudgetID` int(11) NOT NULL DEFAULT '0',
  `_etblGLReviseBudget_iBranchID` int(11) DEFAULT NULL,
  `_etblGLReviseBudget_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblGLReviseBudget_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblGLReviseBudget_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblGLReviseBudget_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblGLReviseBudget_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblGLReviseBudget_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblGLReviseBudget_iChangeSetID` int(11) DEFAULT NULL,
  `_etblGLReviseBudget_Checksum` binary(20) DEFAULT NULL,
  `fNewBudgetForeign` double NOT NULL DEFAULT '0',
  `fOldBudgetForeign` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblglsegment`
--

DROP TABLE IF EXISTS `_etblglsegment`;
CREATE TABLE `_etblglsegment` (
  `idSegment` int(11) NOT NULL,
  `iSegmentNo` int(11) NOT NULL,
  `cCode` varchar(40) NOT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `iSegmentBranchID` int(11) DEFAULT NULL,
  `_etblGLSegment_iBranchID` int(11) DEFAULT NULL,
  `_etblGLSegment_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblGLSegment_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblGLSegment_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblGLSegment_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblGLSegment_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblGLSegment_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblGLSegment_iChangeSetID` int(11) DEFAULT NULL,
  `_etblGLSegment_Checksum` binary(20) DEFAULT NULL,
  `imSCOAAccountID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblglsegmentsetup`
--

DROP TABLE IF EXISTS `_etblglsegmentsetup`;
CREATE TABLE `_etblglsegmentsetup` (
  `idSegmentNo` int(11) NOT NULL,
  `bForceValue` tinyint(1) NOT NULL DEFAULT '0',
  `bSegmentUsed` tinyint(1) NOT NULL DEFAULT '0',
  `cSegmentLabel` varchar(35) NOT NULL,
  `cSegmentMask` varchar(40) DEFAULT NULL,
  `bInUse` tinyint(1) NOT NULL DEFAULT '1',
  `bIsBranchSegment` tinyint(1) NOT NULL DEFAULT '0',
  `_etblGLSegmentSetup_iBranchID` int(11) DEFAULT NULL,
  `_etblGLSegmentSetup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblGLSegmentSetup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblGLSegmentSetup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblGLSegmentSetup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblGLSegmentSetup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblGLSegmentSetup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblGLSegmentSetup_iChangeSetID` int(11) DEFAULT NULL,
  `_etblGLSegmentSetup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvcosttracking`
--

DROP TABLE IF EXISTS `_etblinvcosttracking`;
CREATE TABLE `_etblinvcosttracking` (
  `idCostTracking` bigint(20) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iWarehouseID` int(11) NOT NULL,
  `iLotID` int(11) NOT NULL,
  `iAutoIdx` bigint(20) NOT NULL,
  `dTxDate` datetime(6) NOT NULL,
  `dDateStamp` datetime(6) NOT NULL,
  `fAverageCost` double NOT NULL,
  `fLatestCost` double NOT NULL,
  `fLowestCost` double NOT NULL,
  `fHighestCost` double NOT NULL,
  `fManualCost` double NOT NULL,
  `fQtyOnHand` double NOT NULL,
  `fJobQty` double NOT NULL,
  `fMFPQty` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvimages`
--

DROP TABLE IF EXISTS `_etblinvimages`;
CREATE TABLE `_etblinvimages` (
  `idInvImage` int(11) NOT NULL,
  `iStockLink` int(11) NOT NULL,
  `nInvImage` longblob,
  `cInvImageType` varchar(10) DEFAULT NULL,
  `cInvImageDesc` varchar(50) DEFAULT NULL,
  `bDisplayProportion` tinyint(1) DEFAULT NULL,
  `bDisplayStretch` tinyint(1) DEFAULT NULL,
  `iWidth` int(11) DEFAULT NULL,
  `iHeight` int(11) DEFAULT NULL,
  `cActualType` varchar(20) DEFAULT NULL,
  `fLongtitude` double DEFAULT NULL,
  `fLatitude` double DEFAULT NULL,
  `fSize` double DEFAULT NULL,
  `cTitle` varchar(50) DEFAULT NULL,
  `_etblInvImages_iBranchID` int(11) DEFAULT NULL,
  `_etblInvImages_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvImages_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvImages_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvImages_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvImages_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvImages_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvImages_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvImages_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvjrbatches`
--

DROP TABLE IF EXISTS `_etblinvjrbatches`;
CREATE TABLE `_etblinvjrbatches` (
  `IDInvJrBatches` int(11) NOT NULL,
  `cInvJrNumber` varchar(50) DEFAULT NULL,
  `cInvJrDescription` varchar(40) DEFAULT NULL,
  `cInvJrReference` varchar(50) DEFAULT NULL,
  `iCreateAgentID` int(11) NOT NULL DEFAULT '0',
  `bClearAfterPost` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowDupRef` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowEditGLContra` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineDateOpt` int(11) DEFAULT NULL,
  `dNewLineDateDef` datetime(6) DEFAULT NULL,
  `iNewLineRefOpt` int(11) DEFAULT NULL,
  `cNewLineRefDef` varchar(20) DEFAULT NULL,
  `bNewLineRefInc` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineDescOpt` int(11) DEFAULT NULL,
  `cNewLineDescDef` varchar(40) DEFAULT NULL,
  `bNewLineDescInc` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineProjectOpt` int(11) DEFAULT NULL,
  `iNewLineProjectDefID` int(11) NOT NULL DEFAULT '0',
  `iNewLineWarehouseOpt` int(11) DEFAULT NULL,
  `iNewLineWarehouseDefID` int(11) NOT NULL DEFAULT '0',
  `bJustCleared` tinyint(1) NOT NULL DEFAULT '0',
  `iTransactionCode` int(11) DEFAULT NULL,
  `_etblInvJrBatches_iBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvJrBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvJrBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvJrBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvJrBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvJrBatches_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvjrbatchlines`
--

DROP TABLE IF EXISTS `_etblinvjrbatchlines`;
CREATE TABLE `_etblinvjrbatchlines` (
  `idInvJrBatchLines` int(11) NOT NULL,
  `iInvJrBatchID` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iWarehouseID` int(11) NOT NULL DEFAULT '0',
  `dTrDate` datetime(6) NOT NULL,
  `iTrCodeID` int(11) NOT NULL,
  `iGLContraID` int(11) NOT NULL DEFAULT '0',
  `cReference` varchar(20) DEFAULT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `fQtyIn` double DEFAULT NULL,
  `fQtyOut` double DEFAULT NULL,
  `fNewCost` double DEFAULT NULL,
  `iProjectID` int(11) NOT NULL DEFAULT '0',
  `bIsSerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `bIsLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iSNGroupID` int(11) NOT NULL DEFAULT '0',
  `iJobID` int(11) NOT NULL DEFAULT '0',
  `iLotID` int(11) NOT NULL DEFAULT '0',
  `cLotNumber` varchar(50) DEFAULT NULL,
  `dLotExpiryDate` datetime(6) DEFAULT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_iBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvJrBatchLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvJrBatchLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvjrbatchlinesn`
--

DROP TABLE IF EXISTS `_etblinvjrbatchlinesn`;
CREATE TABLE `_etblinvjrbatchlinesn` (
  `IDInvJrBatchLineSN` int(11) NOT NULL,
  `iInvJrBatchID` int(11) DEFAULT NULL,
  `iSNGroupID` int(11) DEFAULT NULL,
  `iSerialMFID` int(11) DEFAULT NULL,
  `cSerialNumber` varchar(50) DEFAULT NULL,
  `_etblInvJrBatchLineSN_iBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLineSN_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvJrBatchLineSN_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvJrBatchLineSN_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLineSN_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLineSN_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLineSN_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLineSN_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvJrBatchLineSN_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvoicedeposits`
--

DROP TABLE IF EXISTS `_etblinvoicedeposits`;
CREATE TABLE `_etblinvoicedeposits` (
  `idInvoiceDeposits` int(11) NOT NULL,
  `iInvoiceID` bigint(20) DEFAULT NULL,
  `fDepositAmount` double DEFAULT NULL,
  `fUnallocatedAmount` double DEFAULT NULL,
  `iPostARID` bigint(20) DEFAULT NULL,
  `iInvoiceIDOrig` bigint(20) DEFAULT NULL,
  `_etblInvoiceDeposits_iBranchID` int(11) DEFAULT NULL,
  `_etblInvoiceDeposits_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvoiceDeposits_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvoiceDeposits_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvoiceDeposits_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvoiceDeposits_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvoiceDeposits_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvoiceDeposits_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvoiceDeposits_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvpriceupdatebatches`
--

DROP TABLE IF EXISTS `_etblinvpriceupdatebatches`;
CREATE TABLE `_etblinvpriceupdatebatches` (
  `idInvPriceUpdateBatches` int(11) NOT NULL,
  `cInvPriceUpdateNumber` varchar(50) DEFAULT NULL,
  `cInvPriceUpdateDescription` varchar(40) DEFAULT NULL,
  `cInvPriceUpdateReference` varchar(50) DEFAULT NULL,
  `cStartCode` varchar(255) DEFAULT NULL,
  `cEndCode` varchar(255) DEFAULT NULL,
  `cGroups` varchar(1024) DEFAULT NULL,
  `cWarehouses` varchar(1024) DEFAULT NULL,
  `bUpdateAllWarehouses` tinyint(1) NOT NULL DEFAULT '0',
  `bSalesWarehouses` tinyint(1) NOT NULL DEFAULT '0',
  `bIgnoreInactive` tinyint(1) NOT NULL DEFAULT '1',
  `cPriceLists` varchar(1024) DEFAULT NULL,
  `iUpdateType` int(11) DEFAULT NULL,
  `iUpdateAction` int(11) DEFAULT NULL,
  `iRounding` int(11) DEFAULT NULL,
  `iToNearest` int(11) DEFAULT NULL,
  `bPosted` tinyint(1) NOT NULL DEFAULT '0',
  `_etblInvPriceUpdateBatches_iBranchID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatches_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvpriceupdatebatchlines`
--

DROP TABLE IF EXISTS `_etblinvpriceupdatebatchlines`;
CREATE TABLE `_etblinvpriceupdatebatchlines` (
  `idInvPriceUpdateBatchLines` int(11) NOT NULL,
  `iInvPriceUpdateBatchID` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iWarehouseID` int(11) NOT NULL DEFAULT '0',
  `iPriceListID` int(11) NOT NULL,
  `fExclPrice` double DEFAULT NULL,
  `fInclPrice` double DEFAULT NULL,
  `iCostMethod` int(11) DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `fGrossMargin` double DEFAULT NULL,
  `fGMPercentage` double DEFAULT NULL,
  `iUpdateType` int(11) DEFAULT NULL,
  `iUpdateAction` int(11) DEFAULT NULL,
  `fPercentageChange` double DEFAULT NULL,
  `iRounding` int(11) DEFAULT NULL,
  `iToNearest` int(11) DEFAULT NULL,
  `fUpdatePrice` double DEFAULT NULL,
  `fNewExclPrice` double DEFAULT NULL,
  `fNewInclPrice` double DEFAULT NULL,
  `fNewGrossMargin` double DEFAULT NULL,
  `fNewGMPercentage` double DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_iBranchID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvPriceUpdateBatchLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvseggroup`
--

DROP TABLE IF EXISTS `_etblinvseggroup`;
CREATE TABLE `_etblinvseggroup` (
  `idInvSegGroup` int(11) NOT NULL,
  `iInvSegTypeID` int(11) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `_etblInvSegGroup_iBranchID` int(11) DEFAULT NULL,
  `_etblInvSegGroup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvSegGroup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvSegGroup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvSegGroup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvSegGroup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvSegGroup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvSegGroup_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvSegGroup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvsegtype`
--

DROP TABLE IF EXISTS `_etblinvsegtype`;
CREATE TABLE `_etblinvsegtype` (
  `idInvSegType` int(11) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `_etblInvSegType_iBranchID` int(11) DEFAULT NULL,
  `_etblInvSegType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvSegType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvSegType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvSegType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvSegType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvSegType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvSegType_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvSegType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblinvsegvalue`
--

DROP TABLE IF EXISTS `_etblinvsegvalue`;
CREATE TABLE `_etblinvsegvalue` (
  `idInvSegValue` int(11) NOT NULL,
  `iInvSegGroupID` int(11) NOT NULL,
  `cValue` varchar(50) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `_etblInvSegValue_iBranchID` int(11) DEFAULT NULL,
  `_etblInvSegValue_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblInvSegValue_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblInvSegValue_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblInvSegValue_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblInvSegValue_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblInvSegValue_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblInvSegValue_iChangeSetID` int(11) DEFAULT NULL,
  `_etblInvSegValue_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbllotstatus`
--

DROP TABLE IF EXISTS `_etbllotstatus`;
CREATE TABLE `_etbllotstatus` (
  `idLotStatus` int(11) NOT NULL,
  `cLotStatusDescription` varchar(50) DEFAULT NULL,
  `bAllowPurchases` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowSales` tinyint(1) NOT NULL DEFAULT '1',
  `_etblLotStatus_iBranchID` int(11) DEFAULT NULL,
  `_etblLotStatus_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblLotStatus_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblLotStatus_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblLotStatus_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblLotStatus_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblLotStatus_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblLotStatus_iChangeSetID` int(11) DEFAULT NULL,
  `_etblLotStatus_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbllottracking`
--

DROP TABLE IF EXISTS `_etbllottracking`;
CREATE TABLE `_etbllottracking` (
  `idLotTracking` int(11) NOT NULL,
  `cLotDescription` varchar(50) DEFAULT NULL,
  `iStockID` int(11) DEFAULT NULL,
  `iLotStatusID` int(11) DEFAULT NULL,
  `dExpiryDate` datetime(6) DEFAULT NULL,
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `dLastGRVDate` datetime(6) DEFAULT NULL,
  `_etblLotTracking_iBranchID` int(11) DEFAULT NULL,
  `_etblLotTracking_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblLotTracking_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblLotTracking_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblLotTracking_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblLotTracking_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblLotTracking_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblLotTracking_iChangeSetID` int(11) DEFAULT NULL,
  `_etblLotTracking_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbllottrackingqty`
--

DROP TABLE IF EXISTS `_etbllottrackingqty`;
CREATE TABLE `_etbllottrackingqty` (
  `idLotTrackingQty` int(11) NOT NULL,
  `iLotTrackingID` int(11) DEFAULT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `fQtyOnHand` double DEFAULT NULL,
  `fQtyPurchased` double DEFAULT NULL,
  `fQtySold` double DEFAULT NULL,
  `fQtyAdjustOut` double DEFAULT NULL,
  `fQtyAdjustIn` double DEFAULT NULL,
  `fQtyToSupplier` double DEFAULT NULL,
  `fQtyFromClient` double DEFAULT NULL,
  `fQtyFromWarehouse` double DEFAULT NULL,
  `fQtyToWarehouse` double DEFAULT NULL,
  `fQtyReserved` double DEFAULT NULL,
  `fQtyJCWIP` double DEFAULT NULL,
  `fQtyMFWIP` double DEFAULT NULL,
  `_etblLotTrackingQty_iBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingQty_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblLotTrackingQty_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblLotTrackingQty_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingQty_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingQty_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblLotTrackingQty_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblLotTrackingQty_iChangeSetID` int(11) DEFAULT NULL,
  `_etblLotTrackingQty_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbllottrackingtx`
--

DROP TABLE IF EXISTS `_etbllottrackingtx`;
CREATE TABLE `_etbllottrackingtx` (
  `idLotTrackingTx` bigint(20) NOT NULL,
  `iLotTrackingID` int(11) DEFAULT NULL,
  `dLTTxDate` datetime(6) DEFAULT NULL,
  `iLTTxAccountID` int(11) DEFAULT NULL,
  `cLTTxReference` varchar(50) DEFAULT NULL,
  `cLTTxReference2` varchar(50) DEFAULT NULL,
  `iLTTxTrCodeID` int(11) DEFAULT NULL,
  `iLTTxTransTypeID` int(11) DEFAULT NULL,
  `iLTTxWarehouseID` int(11) DEFAULT NULL,
  `cLTTxAuditNumber` varchar(50) DEFAULT NULL,
  `dLTTxExpiryDate` datetime(6) DEFAULT NULL,
  `iLTTxStatusID` int(11) DEFAULT NULL,
  `fLTTxQty` double DEFAULT NULL,
  `iTxBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_iBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblLotTrackingTx_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblLotTrackingTx_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_iChangeSetID` int(11) DEFAULT NULL,
  `_etblLotTrackingTx_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblmanufprocess`
--

DROP TABLE IF EXISTS `_etblmanufprocess`;
CREATE TABLE `_etblmanufprocess` (
  `idManufProcess` int(11) NOT NULL,
  `iStatus` char(1) NOT NULL,
  `cProcessRefNumber` varchar(50) NOT NULL,
  `cOtherRefNumber` varchar(50) DEFAULT NULL,
  `iBOMMasterID` int(11) NOT NULL,
  `cManufDescription` varchar(50) DEFAULT NULL,
  `dCreated` datetime(6) NOT NULL,
  `dLastUpdated` datetime(6) NOT NULL,
  `fManufQuantity` double DEFAULT NULL,
  `fQtyManufactured` double DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iManufWarehouseID` int(11) DEFAULT NULL,
  `bOverrideCompWhse` tinyint(1) NOT NULL DEFAULT '1',
  `iInvoiceLineID` bigint(20) NOT NULL,
  `bIsLinkedToOrder` tinyint(1) NOT NULL DEFAULT '0',
  `iInvNumID` int(11) DEFAULT NULL,
  `iJCMasterID` int(11) DEFAULT NULL,
  `dProjectedCompletionDate` datetime(6) DEFAULT NULL,
  `dActualCompletionDate` datetime(6) DEFAULT NULL,
  `_etblManufProcess_fLeadDays` double DEFAULT NULL,
  `_etblManufProcess_iBranchID` int(11) DEFAULT NULL,
  `_etblManufProcess_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblManufProcess_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblManufProcess_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblManufProcess_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblManufProcess_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblManufProcess_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblManufProcess_iChangeSetID` int(11) DEFAULT NULL,
  `_etblManufProcess_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblmanufprocessitem`
--

DROP TABLE IF EXISTS `_etblmanufprocessitem`;
CREATE TABLE `_etblmanufprocessitem` (
  `idManufProcessItem` bigint(20) NOT NULL,
  `iMFPItemID` double DEFAULT NULL,
  `iParentMFPItemID` int(11) NOT NULL,
  `iManufProcessID` int(11) NOT NULL,
  `iInvItemID` int(11) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `fProductionQty` double DEFAULT NULL,
  `cUnitOfMeasure` varchar(50) DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `iDefaultWhseID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_iBranchID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblManufProcessItem_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblManufProcessItem_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_iChangeSetID` int(11) DEFAULT NULL,
  `_etblManufProcessItem_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblmanufprocessline`
--

DROP TABLE IF EXISTS `_etblmanufprocessline`;
CREATE TABLE `_etblmanufprocessline` (
  `idManufProcessLine` bigint(20) NOT NULL,
  `iManufProcessID` int(11) NOT NULL,
  `iAction` int(11) NOT NULL,
  `iLineNo` int(11) NOT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `iMFPItemID` double DEFAULT NULL,
  `iInvItemID` int(11) NOT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iNewInvItemID` int(11) DEFAULT NULL,
  `iNewWarehouseID` int(11) DEFAULT NULL,
  `fQuantity` double DEFAULT NULL,
  `fCost` double DEFAULT NULL,
  `bProcessed` tinyint(1) NOT NULL DEFAULT '0',
  `dTransactionDate` datetime(6) DEFAULT NULL,
  `dLastUpdateDate` datetime(6) DEFAULT NULL,
  `fQtyAvailable` double DEFAULT NULL,
  `cDescription` varchar(255) DEFAULT NULL,
  `iLotID` int(11) DEFAULT NULL,
  `iPickingSlipPrinted` int(11) NOT NULL DEFAULT '0',
  `iUnmanufactureLineNo` int(11) NOT NULL DEFAULT '0',
  `iDocVersion` int(11) NOT NULL DEFAULT '0',
  `fLineCost` double NOT NULL DEFAULT '0',
  `_etblManufProcessLine_iBranchID` int(11) DEFAULT NULL,
  `_etblManufProcessLine_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblManufProcessLine_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblManufProcessLine_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblManufProcessLine_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblManufProcessLine_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblManufProcessLine_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblManufProcessLine_iChangeSetID` int(11) DEFAULT NULL,
  `_etblManufProcessLine_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblmcagentcriteria`
--

DROP TABLE IF EXISTS `_etblmcagentcriteria`;
CREATE TABLE `_etblmcagentcriteria` (
  `idAgentCriteria` int(11) NOT NULL,
  `cAgentCriteriaDesc` varchar(100) DEFAULT NULL,
  `cAgent` varchar(50) DEFAULT NULL,
  `cModule` varchar(40) NOT NULL,
  `cMonitorValue` varchar(100) NOT NULL,
  `cOperator` varchar(30) DEFAULT NULL,
  `cResultValue` varchar(250) DEFAULT NULL,
  `cOwnResultValue` varchar(50) DEFAULT NULL,
  `cSQL` longtext,
  `cFrequencyDescription` varchar(50) DEFAULT NULL,
  `iFrequencyTimeInMin` int(11) DEFAULT NULL,
  `dFrequencyStartTime` datetime DEFAULT NULL,
  `dFrequencyEndTime` datetime DEFAULT NULL,
  `cNotificationFrequency` varchar(1) DEFAULT NULL,
  `bNotifyTrayIcon` tinyint(1) NOT NULL DEFAULT '0',
  `bNotifyEmail` tinyint(1) NOT NULL DEFAULT '0',
  `bNotifySMS` tinyint(1) NOT NULL DEFAULT '0',
  `bLogNotification` tinyint(1) NOT NULL DEFAULT '0',
  `bAddToMyNotifications` tinyint(1) NOT NULL DEFAULT '0',
  `cEmailToAddress` longtext,
  `cEmailFromAddress` varchar(60) DEFAULT NULL,
  `cEmailSubject` varchar(120) DEFAULT NULL,
  `cEmailMessage` longtext,
  `cSMSToAddress` longtext,
  `cSMSMessage` longtext,
  `cTrayIconMessage` longtext,
  `cMyNotificationsMessage` longtext,
  `dLastNotificationDateTime` datetime DEFAULT NULL,
  `cProcessing` varchar(1) DEFAULT NULL,
  `cEmailCC` longtext,
  `cEmailBcc` longtext,
  `iWarehouseID` int(11) NOT NULL DEFAULT '0',
  `_etblMCAgentCriteria_iBranchID` int(11) DEFAULT NULL,
  `_etblMCAgentCriteria_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblMCAgentCriteria_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblMCAgentCriteria_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblMCAgentCriteria_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblMCAgentCriteria_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblMCAgentCriteria_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblMCAgentCriteria_iChangeSetID` int(11) DEFAULT NULL,
  `_etblMCAgentCriteria_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblmcdefaultcriteria`
--

DROP TABLE IF EXISTS `_etblmcdefaultcriteria`;
CREATE TABLE `_etblmcdefaultcriteria` (
  `idDefaultCriteria` int(11) NOT NULL,
  `cModule` varchar(100) NOT NULL,
  `cMonitorValue` varchar(200) NOT NULL,
  `cMonitorField` varchar(200) DEFAULT NULL,
  `cMonitorFieldType` varchar(15) DEFAULT NULL,
  `cOperator` varchar(30) DEFAULT NULL,
  `cResultValue` varchar(1024) DEFAULT NULL,
  `cFieldsForResult` varchar(1024) DEFAULT NULL,
  `cMessageValueDesc` varchar(1024) DEFAULT NULL,
  `cMessageValueField` varchar(1024) DEFAULT NULL,
  `cCriteriaSQLText` varchar(1024) DEFAULT NULL,
  `_etblMCDefaultCriteria_iBranchID` int(11) DEFAULT NULL,
  `_etblMCDefaultCriteria_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblMCDefaultCriteria_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblMCDefaultCriteria_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblMCDefaultCriteria_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblMCDefaultCriteria_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblMCDefaultCriteria_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblMCDefaultCriteria_iChangeSetID` int(11) DEFAULT NULL,
  `_etblMCDefaultCriteria_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblordercancelreason`
--

DROP TABLE IF EXISTS `_etblordercancelreason`;
CREATE TABLE `_etblordercancelreason` (
  `idOrderCancelReason` int(11) NOT NULL,
  `cCancellationReasonCode` varchar(10) DEFAULT NULL,
  `cCancellationReasonDesc` varchar(30) NOT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_etblOrderCancelReason_iBranchID` int(11) DEFAULT NULL,
  `_etblOrderCancelReason_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblOrderCancelReason_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblOrderCancelReason_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblOrderCancelReason_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblOrderCancelReason_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblOrderCancelReason_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblOrderCancelReason_iChangeSetID` int(11) DEFAULT NULL,
  `_etblOrderCancelReason_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpaymentsbasedtax`
--

DROP TABLE IF EXISTS `_etblpaymentsbasedtax`;
CREATE TABLE `_etblpaymentsbasedtax` (
  `idPaymentsBasedTax` int(11) NOT NULL,
  `iFromPeriod` int(11) DEFAULT NULL,
  `iToPeriod` int(11) DEFAULT NULL,
  `dProcessDate` datetime(6) DEFAULT NULL,
  `cNumber` varchar(50) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `fExcludeLessThan` double DEFAULT NULL,
  `bProcessed` tinyint(1) NOT NULL DEFAULT '0',
  `cProcessedAuditNumber` varchar(50) DEFAULT NULL,
  `_etblPaymentsBasedTax_iBranchID` int(11) DEFAULT NULL,
  `_etblPaymentsBasedTax_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPaymentsBasedTax_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPaymentsBasedTax_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPaymentsBasedTax_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPaymentsBasedTax_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPaymentsBasedTax_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPaymentsBasedTax_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPaymentsBasedTax_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblperiod`
--

DROP TABLE IF EXISTS `_etblperiod`;
CREATE TABLE `_etblperiod` (
  `idPeriod` int(11) NOT NULL,
  `dPeriodDate` datetime(6) NOT NULL,
  `bBlocked` tinyint(1) NOT NULL DEFAULT '0',
  `bPBTProcessed` tinyint(1) NOT NULL DEFAULT '0',
  `bPeriodProcessed` tinyint(1) NOT NULL DEFAULT '0',
  `iYearID` int(11) NOT NULL DEFAULT '0',
  `_etblPeriod_iBranchID` int(11) DEFAULT NULL,
  `_etblPeriod_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPeriod_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPeriod_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPeriod_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPeriod_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPeriod_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPeriod_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPeriod_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblperiodyear`
--

DROP TABLE IF EXISTS `_etblperiodyear`;
CREATE TABLE `_etblperiodyear` (
  `idYear` int(11) NOT NULL,
  `cYearDescription` varchar(50) NOT NULL,
  `dYearStartDate` datetime(6) NOT NULL,
  `bArchived` tinyint(1) NOT NULL DEFAULT '0',
  `bPurged` tinyint(1) NOT NULL DEFAULT '0',
  `_etblPeriodYear_iBranchID` int(11) DEFAULT NULL,
  `_etblPeriodYear_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPeriodYear_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPeriodYear_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPeriodYear_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPeriodYear_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPeriodYear_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPeriodYear_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPeriodYear_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpopdefaults`
--

DROP TABLE IF EXISTS `_etblpopdefaults`;
CREATE TABLE `_etblpopdefaults` (
  `idPOPDefaults` int(11) NOT NULL,
  `bAutoRequisition` tinyint(1) NOT NULL DEFAULT '1',
  `cRequisitionPrefix` varchar(20) DEFAULT NULL,
  `iNextRequisitionNo` int(11) DEFAULT NULL,
  `iPadRequisitionLength` int(11) DEFAULT NULL,
  `bReqBudgetCheck` tinyint(1) NOT NULL DEFAULT '0',
  `bReqBudgetAnnual` tinyint(1) NOT NULL DEFAULT '0',
  `bReqToPOIgnoreExpDate` tinyint(1) NOT NULL DEFAULT '0',
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `_etblPOPDefaults_iBranchID` int(11) DEFAULT NULL,
  `_etblPOPDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPOPDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPOPDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPOPDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPOPDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPOPDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPOPDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPOPDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpoprequisitionlines`
--

DROP TABLE IF EXISTS `_etblpoprequisitionlines`;
CREATE TABLE `_etblpoprequisitionlines` (
  `idPOPRequisitionLines` int(11) NOT NULL,
  `iRequisitionID` int(11) NOT NULL,
  `iModuleID` int(11) NOT NULL,
  `iAccountID` int(11) NOT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `iSupplierID` int(11) NOT NULL,
  `fQuantity` double NOT NULL DEFAULT '0',
  `fExpectedPrice` double NOT NULL DEFAULT '0',
  `dExpectedDate` datetime(6) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL,
  `iJobID` int(11) NOT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `iEscalateGroupID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `iLineStatus` int(11) NOT NULL DEFAULT '0',
  `iIncidentID` int(11) NOT NULL DEFAULT '0',
  `iPOInvoiceID` int(11) DEFAULT '0',
  `fActualPrice` double DEFAULT '0',
  `fExchangeRate` double DEFAULT NULL,
  `fExpectedPriceForeign` double DEFAULT NULL,
  `fActualPriceForeign` double DEFAULT NULL,
  `dApprovalDate` datetime(6) DEFAULT NULL,
  `iActionAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_iBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitionLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitionLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLines_Checksum` binary(20) DEFAULT NULL,
  `cSector` varchar(100) DEFAULT NULL,
  `cCostCentre` varchar(100) DEFAULT NULL,
  `iGenRFQAgentID` int(11) DEFAULT NULL,
  `iAreaID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpoprequisitionlineshist`
--

DROP TABLE IF EXISTS `_etblpoprequisitionlineshist`;
CREATE TABLE `_etblpoprequisitionlineshist` (
  `idPOPRequisitionLinesHist` int(11) NOT NULL,
  `iRequisitionHistID` int(11) NOT NULL,
  `iRequisitionID` int(11) NOT NULL,
  `iModuleID` int(11) NOT NULL,
  `iAccountID` int(11) NOT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `iSupplierID` int(11) NOT NULL,
  `fQuantity` double NOT NULL,
  `fExpectedPrice` double NOT NULL,
  `dExpectedDate` datetime(6) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL,
  `iJobID` int(11) NOT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `iEscalateGroupID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `iLineStatus` int(11) NOT NULL,
  `iIncidentID` int(11) NOT NULL,
  `iPOInvoiceID` int(11) NOT NULL,
  `fActualPrice` double DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fExpectedPriceForeign` double DEFAULT NULL,
  `fActualPriceForeign` double DEFAULT NULL,
  `dApprovalDate` datetime(6) DEFAULT NULL,
  `iActionAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_iBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionLinesHist_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpoprequisitions`
--

DROP TABLE IF EXISTS `_etblpoprequisitions`;
CREATE TABLE `_etblpoprequisitions` (
  `idPOPRequisitions` int(11) NOT NULL,
  `cRequisitionNo` varchar(50) DEFAULT NULL,
  `dRequisitionDate` datetime(6) NOT NULL,
  `iProjectDefaultID` int(11) NOT NULL,
  `cRequestedBy` varchar(50) DEFAULT NULL,
  `iIncidentTypeDefaultID` int(11) NOT NULL,
  `iStatus` int(11) NOT NULL DEFAULT '0',
  `iAgentID` int(11) DEFAULT '0',
  `_etblPOPRequisitions_iBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitions_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitions_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitions_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitions_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitions_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitions_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitions_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPOPRequisitions_Checksum` binary(20) DEFAULT NULL,
  `cSector` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpoprequisitionshist`
--

DROP TABLE IF EXISTS `_etblpoprequisitionshist`;
CREATE TABLE `_etblpoprequisitionshist` (
  `idPOPRequisitionsHist` int(11) NOT NULL,
  `iRequisitionID` int(11) NOT NULL,
  `cRequisitionNo` varchar(50) DEFAULT NULL,
  `dRequisitionDate` datetime(6) NOT NULL,
  `iProjectDefaultID` int(11) NOT NULL,
  `cRequestedBy` varchar(50) DEFAULT NULL,
  `iIncidentTypeDefaultID` int(11) NOT NULL,
  `iStatus` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `iVersion` int(11) NOT NULL,
  `_etblPOPRequisitionsHist_iBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionsHist_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitionsHist_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPOPRequisitionsHist_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionsHist_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionsHist_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionsHist_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionsHist_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPOPRequisitionsHist_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblposdevices`
--

DROP TABLE IF EXISTS `_etblposdevices`;
CREATE TABLE `_etblposdevices` (
  `idPOSDevices` int(11) NOT NULL,
  `cDeviceCode` varchar(12) DEFAULT NULL,
  `cDeviceDescription` varchar(50) DEFAULT NULL,
  `iDeviceType` int(11) DEFAULT NULL,
  `iPortType` int(11) DEFAULT NULL,
  `iPortNum` int(11) DEFAULT NULL,
  `iBaudrate` int(11) DEFAULT NULL,
  `cControlCodes` varchar(120) DEFAULT NULL,
  `iPoleDisplayWidth` int(11) DEFAULT NULL,
  `iFiscalPrinterId` int(11) DEFAULT NULL,
  `cPrinterName` varchar(100) DEFAULT NULL,
  `iFiscalPrinterModelsId` int(11) DEFAULT NULL,
  `cPrinterCOMName` varchar(50) DEFAULT NULL,
  `_etblPOSDevices_iBranchID` int(11) DEFAULT NULL,
  `_etblPOSDevices_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPOSDevices_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPOSDevices_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPOSDevices_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPOSDevices_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPOSDevices_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPOSDevices_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPOSDevices_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpostdatedcheques`
--

DROP TABLE IF EXISTS `_etblpostdatedcheques`;
CREATE TABLE `_etblpostdatedcheques` (
  `idPostDatedCheques` int(11) NOT NULL,
  `dpdcDate` datetime DEFAULT NULL,
  `ipdcAccountID` int(11) NOT NULL DEFAULT '0',
  `ipdcTrCodeID` int(11) NOT NULL DEFAULT '0',
  `ipdcTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `ipdcProjectID` int(11) NOT NULL DEFAULT '0',
  `ipdcRepID` int(11) NOT NULL DEFAULT '0',
  `ipdcContraLedgerID` int(11) NOT NULL DEFAULT '0',
  `cpdcReference` varchar(50) DEFAULT NULL,
  `cpdcReference2` varchar(50) DEFAULT NULL,
  `cpdcDescription` varchar(100) DEFAULT NULL,
  `cpdcOrderNo` varchar(50) DEFAULT NULL,
  `fpdcInclusive` double NOT NULL DEFAULT '0',
  `fpdcExclusive` double NOT NULL DEFAULT '0',
  `fpdcTax` double NOT NULL DEFAULT '0',
  `fpdcFCInclusive` double NOT NULL DEFAULT '0',
  `fpdcFCExclusive` double NOT NULL DEFAULT '0',
  `fpdcFCTax` double NOT NULL DEFAULT '0',
  `ipdcDiscTrCodeID` int(11) NOT NULL DEFAULT '0',
  `ipdcDiscTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `ipdcDiscTaxLedgerID` int(11) NOT NULL DEFAULT '0',
  `ipdcDiscContraLedgerID` int(11) NOT NULL DEFAULT '0',
  `cpdcDiscDescription` varchar(35) NOT NULL,
  `fpdcDiscInclusive` double NOT NULL DEFAULT '0',
  `fpdcDiscExclusive` double NOT NULL DEFAULT '0',
  `fpdcDiscTax` double NOT NULL DEFAULT '0',
  `fpdcDiscFCInclusive` double NOT NULL DEFAULT '0',
  `fpdcDiscFCExclusive` double NOT NULL DEFAULT '0',
  `fpdcDiscFCTax` double NOT NULL DEFAULT '0',
  `fpdcExchangeRate` double NOT NULL DEFAULT '0',
  `ipdcGLControlID` int(11) NOT NULL DEFAULT '0',
  `bpdcCancelled` tinyint(1) NOT NULL DEFAULT '0',
  `cpdcCancellationReason` varchar(40) DEFAULT NULL,
  `iVMVoucherID` int(11) NOT NULL DEFAULT '0',
  `ipdcDCModule` int(11) NOT NULL DEFAULT '0',
  `_etblPostDatedCheques_iBranchID` int(11) DEFAULT NULL,
  `_etblPostDatedCheques_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPostDatedCheques_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPostDatedCheques_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPostDatedCheques_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPostDatedCheques_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPostDatedCheques_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPostDatedCheques_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPostDatedCheques_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpostglhist`
--

DROP TABLE IF EXISTS `_etblpostglhist`;
CREATE TABLE `_etblpostglhist` (
  `AutoIdx` bigint(20) NOT NULL,
  `TxDate` datetime DEFAULT NULL,
  `Id` varchar(5) NOT NULL,
  `AccountLink` int(11) DEFAULT NULL,
  `TrCodeID` int(11) DEFAULT NULL,
  `Debit` double DEFAULT NULL,
  `Credit` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fForeignDebit` double DEFAULT NULL,
  `fForeignCredit` double DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `TaxTypeID` int(11) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `Order_No` varchar(50) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `cAuditNumber` varchar(50) DEFAULT NULL,
  `Tax_Amount` double DEFAULT NULL,
  `fForeignTax` double DEFAULT NULL,
  `Project` int(11) DEFAULT NULL,
  `Period` int(11) DEFAULT NULL,
  `DrCrAccount` int(11) DEFAULT NULL,
  `JobCodeLink` int(11) DEFAULT NULL,
  `CRCCheck` double DEFAULT NULL,
  `DTStamp` datetime(6) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `iTaxPeriodID` int(11) DEFAULT NULL,
  `cPayeeName` varchar(100) DEFAULT NULL,
  `bPrintCheque` tinyint(1) NOT NULL,
  `cReference2` varchar(50) DEFAULT NULL,
  `RepID` int(11) DEFAULT NULL,
  `fJCRepCost` double DEFAULT NULL,
  `iMFPID` int(11) DEFAULT NULL,
  `bIsJCDocLine` tinyint(1) NOT NULL,
  `bIsSTGLDocLine` tinyint(1) NOT NULL,
  `iInvLineID` bigint(20) NOT NULL,
  `iTxBranchID` int(11) DEFAULT NULL,
  `cBankRef` varchar(20) DEFAULT NULL,
  `bPBTPaid` tinyint(1) NOT NULL,
  `iGLTaxAccountID` int(11) DEFAULT NULL,
  `bReconciled` tinyint(1) NOT NULL DEFAULT '0',
  `_etblPostGLHist_iBranchID` int(11) DEFAULT NULL,
  `_etblPostGLHist_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPostGLHist_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPostGLHist_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPostGLHist_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPostGLHist_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPostGLHist_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPostGLHist_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPostGLHist_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpostoutstandingexclap`
--

DROP TABLE IF EXISTS `_etblpostoutstandingexclap`;
CREATE TABLE `_etblpostoutstandingexclap` (
  `idPostOutstandingExcl` int(11) NOT NULL,
  `iPostLnk` int(11) NOT NULL,
  `fLnkAmount` double NOT NULL,
  `fFCLnkAmount` double DEFAULT NULL,
  `_etblPostOutstandingExclAP_iBranchID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAP_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPostOutstandingExclAP_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPostOutstandingExclAP_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAP_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAP_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAP_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAP_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAP_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpostoutstandingexclar`
--

DROP TABLE IF EXISTS `_etblpostoutstandingexclar`;
CREATE TABLE `_etblpostoutstandingexclar` (
  `idPostOutstandingExcl` int(11) NOT NULL,
  `iPostLnk` int(11) NOT NULL,
  `fLnkAmount` double NOT NULL,
  `fFCLnkAmount` double DEFAULT NULL,
  `_etblPostOutstandingExclAR_iBranchID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAR_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPostOutstandingExclAR_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPostOutstandingExclAR_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAR_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAR_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAR_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAR_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPostOutstandingExclAR_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpricelistname`
--

DROP TABLE IF EXISTS `_etblpricelistname`;
CREATE TABLE `_etblpricelistname` (
  `IDPriceListName` int(11) NOT NULL,
  `cName` varchar(30) NOT NULL,
  `cDescription` varchar(60) DEFAULT NULL,
  `bDefault` tinyint(1) NOT NULL DEFAULT '0',
  `iCurrencyID` int(11) DEFAULT NULL,
  `dPLNameTimeStamp` datetime(6) DEFAULT NULL,
  `_etblPriceListName_iBranchID` int(11) DEFAULT NULL,
  `_etblPriceListName_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPriceListName_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPriceListName_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListName_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListName_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListName_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListName_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPriceListName_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpricelistprices`
--

DROP TABLE IF EXISTS `_etblpricelistprices`;
CREATE TABLE `_etblpricelistprices` (
  `IDPriceListPrices` bigint(20) NOT NULL,
  `iPriceListNameID` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iWarehouseID` int(11) DEFAULT '0',
  `bUseMarkup` tinyint(1) NOT NULL DEFAULT '0',
  `iMarkupOnCost` int(11) NOT NULL DEFAULT '0',
  `fMarkupRate` double DEFAULT NULL,
  `fExclPrice` double DEFAULT NULL,
  `fInclPrice` double DEFAULT NULL,
  `dPLPricesTimeStamp` datetime(6) DEFAULT NULL,
  `_etblPriceListPrices_iBranchID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPriceListPrices_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPriceListPrices_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpromotion`
--

DROP TABLE IF EXISTS `_etblpromotion`;
CREATE TABLE `_etblpromotion` (
  `iPromotionID` int(11) NOT NULL,
  `cPromotionCode` varchar(20) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `iTriggerQTY` int(11) DEFAULT NULL,
  `iQualifyingQTY` int(11) DEFAULT NULL,
  `fDiscount` double DEFAULT NULL,
  `fFixedPrice` double DEFAULT NULL,
  `dStartDate` datetime(6) DEFAULT NULL,
  `dEndDate` datetime(6) DEFAULT NULL,
  `bProportion` tinyint(1) DEFAULT NULL,
  `bLimit` tinyint(1) NOT NULL DEFAULT '0',
  `iLimitQTY` int(11) DEFAULT NULL,
  `iPromotionType` int(11) NOT NULL DEFAULT '0',
  `fTriggerValue` double DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `iTriggerUOM` int(11) DEFAULT NULL,
  `iQualifyingUOM` int(11) DEFAULT NULL,
  `iCustomerType` int(11) DEFAULT NULL,
  `bInclusive` tinyint(1) NOT NULL DEFAULT '1',
  `_etblPromotion_iBranchID` int(11) DEFAULT NULL,
  `_etblPromotion_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPromotion_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPromotion_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPromotion_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPromotion_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPromotion_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPromotion_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPromotion_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpromotionitem`
--

DROP TABLE IF EXISTS `_etblpromotionitem`;
CREATE TABLE `_etblpromotionitem` (
  `iPromotionItemID` int(11) NOT NULL,
  `iPromotionItemListID` int(11) DEFAULT NULL,
  `StockLink` int(11) DEFAULT NULL,
  `cItemGroup` varchar(20) DEFAULT NULL,
  `iTriggerQTY` int(11) DEFAULT NULL,
  `iQualifyingQTY` int(11) DEFAULT NULL,
  `iUOMID` int(11) DEFAULT NULL,
  `iPriority` int(11) DEFAULT NULL,
  `_etblPromotionItem_iBranchID` int(11) DEFAULT NULL,
  `_etblPromotionItem_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPromotionItem_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPromotionItem_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPromotionItem_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPromotionItem_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPromotionItem_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPromotionItem_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPromotionItem_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblpromotionitemlist`
--

DROP TABLE IF EXISTS `_etblpromotionitemlist`;
CREATE TABLE `_etblpromotionitemlist` (
  `iPromotionItemListID` int(11) NOT NULL,
  `iPromotionID` int(11) DEFAULT NULL,
  `cListCode` varchar(20) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `bTriggerItem` tinyint(1) DEFAULT NULL,
  `bQualifyingItem` tinyint(1) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_etblPromotionItemList_iBranchID` int(11) DEFAULT NULL,
  `_etblPromotionItemList_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblPromotionItemList_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblPromotionItemList_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPromotionItemList_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPromotionItemList_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPromotionItemList_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPromotionItemList_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPromotionItemList_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblremittancebatches`
--

DROP TABLE IF EXISTS `_etblremittancebatches`;
CREATE TABLE `_etblremittancebatches` (
  `idRemittanceBatches` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDesc` varchar(50) NOT NULL,
  `cBatchRef` varchar(50) DEFAULT NULL,
  `bClearAfterPost` tinyint(1) NOT NULL,
  `bCheckedOut` tinyint(1) DEFAULT NULL,
  `iAgentCheckedOut` int(11) DEFAULT NULL,
  `dProcessedDate` datetime(6) DEFAULT NULL,
  `bChequeEFTSRun` tinyint(1) DEFAULT '1',
  `idefaultTranType` int(11) DEFAULT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `bIncDescription` tinyint(1) DEFAULT '0',
  `cReference` varchar(20) DEFAULT NULL,
  `bIncReference` tinyint(1) DEFAULT '0',
  `bdefaultPrintCheque` tinyint(1) DEFAULT '1',
  `bAllInvoicesPaid` tinyint(1) DEFAULT '1',
  `bPrintChequeOrEFTSOnly` tinyint(1) DEFAULT '0',
  `bPrintRemittance` tinyint(1) DEFAULT '1',
  `bPrintAlsoChequeOrEFTS` tinyint(1) DEFAULT '1',
  `bPrintSamePageRemCheq` tinyint(1) DEFAULT '0',
  `cEFTSFileName` varchar(100) DEFAULT NULL,
  `iDiscTranType` int(11) DEFAULT NULL,
  `bPromptValidationOnClose` tinyint(1) DEFAULT '1',
  `bWarnNotIncludedInRun` tinyint(1) DEFAULT '1',
  `bPreviewBeforePrint` tinyint(1) DEFAULT '1',
  `iDiscTaxType` int(11) DEFAULT NULL,
  `bdefaultEFTSProc` tinyint(1) DEFAULT '0',
  `dEFTSActionDate` datetime(6) DEFAULT NULL,
  `bApplyTerms` tinyint(1) DEFAULT '1',
  `cEFTSBatchDescription` varchar(30) DEFAULT NULL,
  `iEFTSBatchNumber` int(11) DEFAULT NULL,
  `ddefaultPaymentDate` datetime(6) DEFAULT NULL,
  `cEFTSACBServiceType` varchar(100) DEFAULT NULL,
  `iEFTSType` int(11) DEFAULT NULL,
  `bIncludeSupOnHold` tinyint(1) DEFAULT '1',
  `fMaxAmt` double DEFAULT NULL,
  `fMinAmt` double DEFAULT NULL,
  `dPayDueDate` datetime(6) DEFAULT NULL,
  `cSupArea` varchar(255) DEFAULT NULL,
  `cSupFrom` varchar(100) DEFAULT NULL,
  `cSupGrp` varchar(255) DEFAULT NULL,
  `cSupTo` varchar(100) DEFAULT NULL,
  `cEFTSTransactionDesc` varchar(30) DEFAULT NULL,
  `cTrCodes` varchar(255) DEFAULT NULL,
  `cEFTSLayoutDesc` varchar(200) DEFAULT NULL,
  `bAllocateToOldest` tinyint(1) DEFAULT '0',
  `bAllowSettDisc` tinyint(1) DEFAULT '0',
  `cEFTSBatchTypeString` varchar(5) DEFAULT NULL,
  `bEFTSAutoDateForward` tinyint(1) DEFAULT '0',
  `bEFTSTxProofOfPayment` tinyint(1) DEFAULT '0',
  `bEFTSDuplicateFile` tinyint(1) DEFAULT '0',
  `bEFTSExportOrderLetter` tinyint(1) DEFAULT '0',
  `cEFTSOrderLetter` varchar(10) DEFAULT NULL,
  `bInclOutstandingDebits` tinyint(1) DEFAULT '0',
  `bAutoAllocOutstanding` tinyint(1) DEFAULT '0',
  `bTxOnHold` tinyint(1) DEFAULT '0',
  `bTxOnHoldRemove` tinyint(1) DEFAULT '0',
  `iStatus` int(11) NOT NULL DEFAULT '1',
  `iBankDetailID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_iBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblRemittanceBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblRemittanceBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_etblRemittanceBatches_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblremittancelines`
--

DROP TABLE IF EXISTS `_etblremittancelines`;
CREATE TABLE `_etblremittancelines` (
  `IDRemittanceLines` int(11) NOT NULL,
  `iSupplierID` int(11) DEFAULT NULL,
  `cDocumentNumber` varchar(50) DEFAULT NULL,
  `fAmountOutstanding` double DEFAULT NULL,
  `fDiscAmount` double DEFAULT NULL,
  `fAmountToPay` double DEFAULT NULL,
  `bPayTransaction` tinyint(1) NOT NULL DEFAULT '0',
  `fDiscAmountExcl` double DEFAULT NULL,
  `fDiscTaxAmount` double DEFAULT NULL,
  `dDocumentDate` datetime(6) DEFAULT NULL,
  `fDocumentAmount` double DEFAULT NULL,
  `bAllocated` tinyint(1) NOT NULL DEFAULT '0',
  `iInvRecID` bigint(20) DEFAULT NULL,
  `cInvDescription` varchar(255) DEFAULT '',
  `cInvReference1` varchar(255) DEFAULT '',
  `cInvReference2` varchar(255) DEFAULT '',
  `iInvSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `fSettDiscAmount` double NOT NULL DEFAULT '0',
  `cSettTermCode` varchar(20) DEFAULT NULL,
  `fSettTermDiscPerc` double NOT NULL DEFAULT '0',
  `iSettTermDays` int(11) NOT NULL DEFAULT '0',
  `iSettTermPayMethod` int(11) NOT NULL DEFAULT '0',
  `bApplyDisc` tinyint(1) NOT NULL DEFAULT '0',
  `fDiscPerc` double NOT NULL DEFAULT '0',
  `cInvOrderNumber` varchar(20) DEFAULT NULL,
  `bTxOnHold` tinyint(1) NOT NULL DEFAULT '0',
  `iBatchID` int(11) NOT NULL DEFAULT '0',
  `fAmountPaid` double NOT NULL DEFAULT '0',
  `_etblRemittanceLines_iBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblRemittanceLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblRemittanceLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblRemittanceLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblRemittanceLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblRemittanceLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblremittancesuppliers`
--

DROP TABLE IF EXISTS `_etblremittancesuppliers`;
CREATE TABLE `_etblremittancesuppliers` (
  `IDRemittanceSuppliers` int(11) NOT NULL,
  `iSupplierID` int(11) DEFAULT NULL,
  `cPayeeName` varchar(255) DEFAULT NULL,
  `bPrintCheque` tinyint(1) NOT NULL DEFAULT '0',
  `cDescription` varchar(40) DEFAULT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `fDiscReceived` double DEFAULT NULL,
  `fTotalPaid` double DEFAULT NULL,
  `fDocumentTotal` double DEFAULT NULL,
  `bIncludedInRun` tinyint(1) NOT NULL DEFAULT '0',
  `bChequePrinted` tinyint(1) NOT NULL DEFAULT '0',
  `bRemittancePrinted` tinyint(1) NOT NULL DEFAULT '0',
  `bEFTSProcessed` tinyint(1) NOT NULL DEFAULT '0',
  `fUnallocatedDebits` double DEFAULT NULL,
  `fAllocatedDebits` double DEFAULT NULL,
  `fDiscReceivedExcl` double DEFAULT NULL,
  `fTotalPaidExcl` double DEFAULT NULL,
  `fDiscTaxAmount` double DEFAULT NULL,
  `bPosted` tinyint(1) NOT NULL DEFAULT '0',
  `bProduceEFTS` tinyint(1) NOT NULL DEFAULT '0',
  `iDiscTaxType` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `dPaymentDate` datetime(6) DEFAULT NULL,
  `cPayRecIDs` varchar(255) DEFAULT NULL,
  `bDefaultPayAllInvoices` tinyint(1) NOT NULL DEFAULT '0',
  `cDCCode` varchar(20) DEFAULT NULL,
  `cDCName` varchar(50) DEFAULT NULL,
  `fUnallocatedCredits` double DEFAULT '0',
  `iInvoiceCount` int(11) DEFAULT '0',
  `iConfiguredCount` double DEFAULT '0',
  `iBatchID` int(11) NOT NULL DEFAULT '0',
  `fAmountToPay` double NOT NULL DEFAULT '0',
  `_etblRemittanceSuppliers_iBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceSuppliers_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblRemittanceSuppliers_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblRemittanceSuppliers_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceSuppliers_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblRemittanceSuppliers_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblRemittanceSuppliers_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblRemittanceSuppliers_iChangeSetID` int(11) DEFAULT NULL,
  `_etblRemittanceSuppliers_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblrepllog`
--

DROP TABLE IF EXISTS `_etblrepllog`;
CREATE TABLE `_etblrepllog` (
  `idReplLog` int(11) NOT NULL,
  `iChangeSetID` int(11) NOT NULL,
  `iBranchID` int(11) NOT NULL,
  `cAction` char(1) NOT NULL,
  `dInitDateUtc` datetime(6) DEFAULT NULL,
  `cFileName` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblreportjoblog`
--

DROP TABLE IF EXISTS `_etblreportjoblog`;
CREATE TABLE `_etblreportjoblog` (
  `idReportJobLog` int(11) NOT NULL,
  `iJobType` int(11) DEFAULT NULL,
  `iReportJobID` int(11) DEFAULT NULL,
  `dLogTime` datetime(6) DEFAULT NULL,
  `iLogType` int(11) DEFAULT NULL,
  `cLogDescription` varchar(1024) DEFAULT NULL,
  `nLogData` longblob,
  `_etblReportJobLog_iBranchID` int(11) DEFAULT NULL,
  `_etblReportJobLog_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblReportJobLog_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblReportJobLog_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblReportJobLog_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblReportJobLog_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblReportJobLog_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblReportJobLog_iChangeSetID` int(11) DEFAULT NULL,
  `_etblReportJobLog_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblreportjobs`
--

DROP TABLE IF EXISTS `_etblreportjobs`;
CREATE TABLE `_etblreportjobs` (
  `idReportJobs` int(11) NOT NULL,
  `iReportJobParentID` int(11) DEFAULT NULL,
  `cReportJobName` varchar(64) DEFAULT NULL,
  `iReportJobType` int(11) DEFAULT NULL,
  `nReportJobProps` longtext,
  `iReportJobAgentID` int(11) DEFAULT NULL,
  `_etblReportJobs_iBranchID` int(11) DEFAULT NULL,
  `_etblReportJobs_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblReportJobs_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblReportJobs_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblReportJobs_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblReportJobs_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblReportJobs_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblReportJobs_iChangeSetID` int(11) DEFAULT NULL,
  `_etblReportJobs_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblrevaluationhistory`
--

DROP TABLE IF EXISTS `_etblrevaluationhistory`;
CREATE TABLE `_etblrevaluationhistory` (
  `idRevaluationHistory` int(11) NOT NULL,
  `iModule` int(11) DEFAULT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `dTransactionDate` datetime(6) DEFAULT NULL,
  `iAgentID` int(11) NOT NULL,
  `fRevaluationRate` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bPosted` tinyint(1) DEFAULT NULL,
  `fRevaluationAmount` double DEFAULT NULL,
  `fOldHomeBalance` double DEFAULT NULL,
  `fNewHomeBalance` double DEFAULT NULL,
  `cBatchNumber` varchar(10) DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `iGLAccountProfitLoss` int(11) DEFAULT NULL,
  `iGLAccountProvision` int(11) DEFAULT NULL,
  `cAccountName` varchar(100) DEFAULT NULL,
  `iPeriodID` int(11) NOT NULL DEFAULT '0',
  `fForeignBalance` double NOT NULL DEFAULT '0',
  `dUpToTxDate` datetime(6) DEFAULT NULL,
  `dRevalRateDate` datetime(6) DEFAULT NULL,
  `_etblRevaluationHistory_iBranchID` int(11) DEFAULT NULL,
  `_etblRevaluationHistory_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblRevaluationHistory_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblRevaluationHistory_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblRevaluationHistory_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblRevaluationHistory_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblRevaluationHistory_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblRevaluationHistory_iChangeSetID` int(11) DEFAULT NULL,
  `_etblRevaluationHistory_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsagepaybanks`
--

DROP TABLE IF EXISTS `_etblsagepaybanks`;
CREATE TABLE `_etblsagepaybanks` (
  `SagePayBankID` int(11) NOT NULL,
  `CountryCode` smallint(6) DEFAULT NULL,
  `BankNameDisplay` varchar(100) DEFAULT NULL,
  `BankNameFile` varchar(100) DEFAULT NULL,
  `BranchName` varchar(100) DEFAULT NULL,
  `BranchCode` varchar(100) DEFAULT NULL,
  `_etblSagePayBanks_iBranchID` int(11) DEFAULT NULL,
  `_etblSagePayBanks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayBanks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayBanks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayBanks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayBanks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayBanks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayBanks_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSagePayBanks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsagepayerrorcodes`
--

DROP TABLE IF EXISTS `_etblsagepayerrorcodes`;
CREATE TABLE `_etblsagepayerrorcodes` (
  `idSagePayErrorCode` int(11) NOT NULL,
  `iServiceID` int(11) DEFAULT NULL,
  `iResponse` int(11) DEFAULT NULL,
  `bWebServiceFailure` tinyint(1) DEFAULT NULL,
  `cResponse` varchar(50) DEFAULT NULL,
  `_etblSagePayErrorCodes_iBranchID` int(11) DEFAULT NULL,
  `_etblSagePayErrorCodes_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayErrorCodes_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayErrorCodes_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayErrorCodes_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayErrorCodes_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayErrorCodes_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayErrorCodes_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSagePayErrorCodes_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsagepaynow`
--

DROP TABLE IF EXISTS `_etblsagepaynow`;
CREATE TABLE `_etblsagepaynow` (
  `idSagePayNow` int(11) NOT NULL,
  `InvNumID` bigint(20) NOT NULL,
  `UniqueReference` varchar(10) DEFAULT NULL,
  `Amount` decimal(18,2) DEFAULT NULL,
  `BankStatementRef` varchar(50) DEFAULT NULL,
  `CardHoldersEmailAddress` varchar(100) DEFAULT NULL,
  `Extra1DebtorsRef` longtext,
  `Extra2` longtext,
  `Extra3` longtext,
  `AcceptDeclineURLParams` varchar(200) DEFAULT NULL,
  `PCode` varchar(15) DEFAULT NULL,
  `PayNowResponse` text,
  `DocType` int(11) NOT NULL,
  `_etblSagePayNow_iBranchID` int(11) DEFAULT NULL,
  `_etblSagePayNow_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayNow_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayNow_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayNow_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayNow_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayNow_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayNow_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSagePayNow_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsagepayqueue`
--

DROP TABLE IF EXISTS `_etblsagepayqueue`;
CREATE TABLE `_etblsagepayqueue` (
  `idSPQueue` int(11) NOT NULL,
  `iService` int(11) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `cInstruction` varchar(50) DEFAULT NULL,
  `bTestOnly` tinyint(1) DEFAULT NULL,
  `iSubmissionStatus` int(11) DEFAULT NULL,
  `iResponseStatus` int(11) DEFAULT NULL,
  `dCreated` datetime(6) DEFAULT NULL,
  `dLastPolled` datetime(6) DEFAULT NULL,
  `cToken` varchar(50) DEFAULT NULL,
  `cBatchName` varchar(50) DEFAULT NULL,
  `cBatchData` longtext,
  `iRecordID` int(11) DEFAULT NULL,
  `cModule` varchar(50) DEFAULT NULL,
  `cSubmissionData` longtext,
  `cResponseData` longtext,
  `iAgentID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_iBranchID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayQueue_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayQueue_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSagePayQueue_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsagepayservicekeys`
--

DROP TABLE IF EXISTS `_etblsagepayservicekeys`;
CREATE TABLE `_etblsagepayservicekeys` (
  `idSagePayServiceKey` int(11) NOT NULL,
  `iServiceType` int(11) NOT NULL,
  `cServiceKeyName` varchar(50) NOT NULL,
  `cServiceDescription` varchar(150) DEFAULT NULL,
  `cServiceKeyValue` varchar(37) DEFAULT NULL,
  `iConnectionTimeout` int(11) DEFAULT NULL,
  `iReceiveTimeout` int(11) DEFAULT NULL,
  `iSendTimeout` int(11) DEFAULT NULL,
  `bIsActive` tinyint(1) NOT NULL,
  `_etblSagePayServiceKeys_iBranchID` int(11) DEFAULT NULL,
  `_etblSagePayServiceKeys_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayServiceKeys_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSagePayServiceKeys_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayServiceKeys_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSagePayServiceKeys_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayServiceKeys_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSagePayServiceKeys_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSagePayServiceKeys_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsettlementterms`
--

DROP TABLE IF EXISTS `_etblsettlementterms`;
CREATE TABLE `_etblsettlementterms` (
  `idSettlementTerms` int(11) NOT NULL,
  `cSettlementCode` varchar(20) NOT NULL,
  `cSettlementDescription` varchar(100) DEFAULT NULL,
  `iPaymentMethod` int(11) DEFAULT NULL,
  `iSettlementDays` int(11) DEFAULT NULL,
  `fSettlementDisc` double NOT NULL DEFAULT '0',
  `cInvMessage` varchar(255) DEFAULT NULL,
  `_etblSettlementTerms_iBranchID` int(11) DEFAULT NULL,
  `_etblSettlementTerms_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSettlementTerms_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSettlementTerms_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSettlementTerms_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSettlementTerms_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSettlementTerms_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSettlementTerms_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSettlementTerms_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsystem`
--

DROP TABLE IF EXISTS `_etblsystem`;
CREATE TABLE `_etblsystem` (
  `idSystem` int(11) NOT NULL,
  `cIdentity` varchar(35) NOT NULL,
  `cValue` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsystemdefaults`
--

DROP TABLE IF EXISTS `_etblsystemdefaults`;
CREATE TABLE `_etblsystemdefaults` (
  `idSystemDefaults` int(11) NOT NULL,
  `DefaultAccess` varchar(5) DEFAULT NULL,
  `Security` varchar(684) DEFAULT NULL,
  `CheckValue` varchar(684) DEFAULT NULL,
  `SmartFilter` tinyint(1) NOT NULL DEFAULT '1',
  `SMTPAccount` varchar(256) DEFAULT NULL,
  `SMTPAuthenticate` tinyint(1) NOT NULL DEFAULT '0',
  `SMTPAuthPass` varchar(684) DEFAULT NULL,
  `SMTPAuthUser` varchar(256) DEFAULT NULL,
  `SMTPFrom` varchar(256) DEFAULT NULL,
  `SMTPName` varchar(256) DEFAULT NULL,
  `SMTPOrg` varchar(256) DEFAULT NULL,
  `SMTPPassword` varchar(256) DEFAULT NULL,
  `SMTPPort` int(11) DEFAULT NULL,
  `SMTPReplyTo` varchar(256) DEFAULT NULL,
  `SMTPServer` varchar(256) DEFAULT NULL,
  `SMTPUseGlobalFromAddr` tinyint(1) NOT NULL DEFAULT '0',
  `SyncDataFolder` varchar(256) DEFAULT NULL,
  `SyncDBServerDataFolder` varchar(256) DEFAULT NULL,
  `UpdateCheck` int(11) DEFAULT NULL,
  `UpdateCheckInterval` int(11) DEFAULT NULL,
  `BICRepository` varchar(256) DEFAULT NULL,
  `BICRptFinancialYear` int(11) DEFAULT '5',
  `HtmlRoot` varchar(256) DEFAULT NULL,
  `UseSegmentedGL` tinyint(1) NOT NULL DEFAULT '0',
  `bUseTLS` int(11) DEFAULT '0',
  `bDisableRTF` tinyint(1) NOT NULL DEFAULT '0',
  `bUseCOM` tinyint(1) DEFAULT '0',
  `bUseMapi` tinyint(1) NOT NULL DEFAULT '1',
  `cSMTPBccAddresss` varchar(50) DEFAULT '0',
  `bUpdateGLBalances` tinyint(1) NOT NULL DEFAULT '1',
  `iGLRelinkCheckInterval` int(11) NOT NULL DEFAULT '1',
  `iPwrNumberChar` int(11) NOT NULL DEFAULT '0',
  `iPwrLetterChar` int(11) NOT NULL DEFAULT '0',
  `iPwrSymbolChar` int(11) NOT NULL DEFAULT '0',
  `iPwrUppercaseChar` int(11) NOT NULL DEFAULT '0',
  `iPwrLowercaseChar` int(11) NOT NULL DEFAULT '0',
  `iLockOutAttempts` int(11) NOT NULL DEFAULT '0',
  `iLockOutDuration` int(11) NOT NULL DEFAULT '0',
  `bPwrComplexity` tinyint(1) NOT NULL DEFAULT '0',
  `bEnableLockOut` tinyint(1) NOT NULL DEFAULT '0',
  `cFreedomService` varchar(250) DEFAULT NULL,
  `cFailOverService` varchar(250) DEFAULT NULL,
  `bUseLocalService` tinyint(1) NOT NULL DEFAULT '1',
  `iFSSessionTimeOut` int(11) NOT NULL DEFAULT '20',
  `bExceptionTrapping` tinyint(1) DEFAULT '0',
  `_etblSystemDefaults_iBranchID` int(11) DEFAULT NULL,
  `_etblSystemDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSystemDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSystemDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSystemDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSystemDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSystemDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSystemDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSystemDefaults_Checksum` binary(20) DEFAULT NULL,
  `bUseOutgoingServerSettings` tinyint(1) NOT NULL DEFAULT '0',
  `iSMTPProvider` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblsystemupdate`
--

DROP TABLE IF EXISTS `_etblsystemupdate`;
CREATE TABLE `_etblsystemupdate` (
  `idSystemUpdate` int(11) NOT NULL,
  `iUpdate` int(11) NOT NULL,
  `bForce` tinyint(1) NOT NULL,
  `_etblSystemUpdate_iBranchID` int(11) DEFAULT NULL,
  `_etblSystemUpdate_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblSystemUpdate_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblSystemUpdate_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblSystemUpdate_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblSystemUpdate_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblSystemUpdate_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblSystemUpdate_iChangeSetID` int(11) DEFAULT NULL,
  `_etblSystemUpdate_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbltaxboxlayout`
--

DROP TABLE IF EXISTS `_etbltaxboxlayout`;
CREATE TABLE `_etbltaxboxlayout` (
  `idTaxBoxLayout` int(11) NOT NULL,
  `iTaxGroupID` int(11) DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iTaxBoxSetupID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_iBranchID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblTaxBoxLayout_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblTaxBoxLayout_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_iChangeSetID` int(11) DEFAULT NULL,
  `_etblTaxBoxLayout_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbltaxboxsetup`
--

DROP TABLE IF EXISTS `_etbltaxboxsetup`;
CREATE TABLE `_etbltaxboxsetup` (
  `idTaxBoxSetup` int(11) NOT NULL,
  `iBoxNumber` int(11) NOT NULL,
  `cBoxLabel` varchar(5) DEFAULT NULL,
  `cBoxHeading` varchar(256) DEFAULT NULL,
  `cExamples` varchar(256) DEFAULT NULL,
  `iValueTypeID` int(11) DEFAULT NULL,
  `iValueID` int(11) DEFAULT NULL,
  `iRoundingID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_iBranchID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblTaxBoxSetup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblTaxBoxSetup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_iChangeSetID` int(11) DEFAULT NULL,
  `_etblTaxBoxSetup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbltaxdefaults`
--

DROP TABLE IF EXISTS `_etbltaxdefaults`;
CREATE TABLE `_etbltaxdefaults` (
  `idTaxDefaults` int(11) NOT NULL,
  `cTaxNumber` varchar(15) DEFAULT NULL,
  `cRegistration` varchar(20) DEFAULT NULL,
  `cCustomsCode` varchar(8) DEFAULT NULL,
  `iTaxDueLedgerAccount` int(11) DEFAULT NULL,
  `iPBTStartPeriodID` int(11) DEFAULT NULL,
  `bPrincipalVendor` tinyint(1) DEFAULT '0',
  `bValidateTaxGroups` tinyint(1) DEFAULT '1',
  `cTaxContactName` varchar(90) DEFAULT NULL,
  `cTaxContactSurname` varchar(53) DEFAULT NULL,
  `cTaxContactTelephone1` varchar(15) DEFAULT NULL,
  `cTaxContactTelephone2` varchar(15) DEFAULT NULL,
  `cTaxContactCellular` varchar(15) DEFAULT NULL,
  `cTaxContactFax` varchar(15) DEFAULT NULL,
  `cTaxContactEmail` varchar(60) DEFAULT NULL,
  `bForceClientTaxIdentification` tinyint(1) DEFAULT '0',
  `bForceSupplierTaxIdentification` tinyint(1) DEFAULT '0',
  `bValidateClientTaxIdentification` tinyint(1) NOT NULL DEFAULT '0',
  `bValidateSupplierTaxIdentification` tinyint(1) NOT NULL DEFAULT '0',
  `_etblTaxDefaults_iBranchID` int(11) DEFAULT NULL,
  `_etblTaxDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblTaxDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblTaxDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblTaxDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblTaxDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblTaxDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblTaxDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_etblTaxDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbltaxgroup`
--

DROP TABLE IF EXISTS `_etbltaxgroup`;
CREATE TABLE `_etbltaxgroup` (
  `idTaxGroup` int(11) NOT NULL,
  `cCode` varchar(20) DEFAULT NULL,
  `cDescription` varchar(120) DEFAULT NULL,
  `_etblTaxGroup_iBranchID` int(11) DEFAULT NULL,
  `_etblTaxGroup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblTaxGroup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblTaxGroup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblTaxGroup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblTaxGroup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblTaxGroup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblTaxGroup_iChangeSetID` int(11) DEFAULT NULL,
  `_etblTaxGroup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etbltaxgrouptranstype`
--

DROP TABLE IF EXISTS `_etbltaxgrouptranstype`;
CREATE TABLE `_etbltaxgrouptranstype` (
  `idTaxGroupTransType` int(11) NOT NULL,
  `iTaxGroupID` int(11) NOT NULL,
  `iTransTypeID` int(11) NOT NULL,
  `cModule` varchar(30) DEFAULT NULL,
  `_etblTaxGroupTransType_iBranchID` int(11) DEFAULT NULL,
  `_etblTaxGroupTransType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblTaxGroupTransType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblTaxGroupTransType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblTaxGroupTransType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblTaxGroupTransType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblTaxGroupTransType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblTaxGroupTransType_iChangeSetID` int(11) DEFAULT NULL,
  `_etblTaxGroupTransType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblterms`
--

DROP TABLE IF EXISTS `_etblterms`;
CREATE TABLE `_etblterms` (
  `iTermID` int(11) NOT NULL,
  `iModule` int(11) NOT NULL,
  `cCode` varchar(20) NOT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `iTermDescOption` int(11) DEFAULT NULL,
  `cTermDesc1` varchar(50) DEFAULT NULL,
  `cTermDesc2` varchar(50) DEFAULT NULL,
  `cTermDesc3` varchar(50) DEFAULT NULL,
  `cTermDesc4` varchar(50) DEFAULT NULL,
  `cTermDesc5` varchar(50) DEFAULT NULL,
  `cTermDesc6` varchar(50) DEFAULT NULL,
  `cTermDesc7` varchar(50) DEFAULT NULL,
  `iAgeTypeOption` int(11) DEFAULT NULL,
  `iIntervalOption` int(11) DEFAULT NULL,
  `iInterval1Days` int(11) DEFAULT NULL,
  `iInterval2Days` int(11) DEFAULT NULL,
  `iInterval3Days` int(11) DEFAULT NULL,
  `iInterval4Days` int(11) DEFAULT NULL,
  `iInterval5Days` int(11) DEFAULT NULL,
  `iInterval6Days` int(11) DEFAULT NULL,
  `iInterval7Days` int(11) DEFAULT NULL,
  `dStateCloseDate1` datetime(6) DEFAULT NULL,
  `dStateCloseDate2` datetime(6) DEFAULT NULL,
  `dStateCloseDate3` datetime(6) DEFAULT NULL,
  `dStateCloseDate4` datetime(6) DEFAULT NULL,
  `dStateCloseDate5` datetime(6) DEFAULT NULL,
  `dStateCloseDate6` datetime(6) DEFAULT NULL,
  `dStateCloseDate7` datetime(6) DEFAULT NULL,
  `bAutoSetToPeriod` tinyint(1) NOT NULL DEFAULT '0',
  `cAge1Message1` varchar(100) DEFAULT NULL,
  `cAge1Message2` varchar(100) DEFAULT NULL,
  `cAge2Message1` varchar(100) DEFAULT NULL,
  `cAge2Message2` varchar(100) DEFAULT NULL,
  `cAge3Message1` varchar(100) DEFAULT NULL,
  `cAge3Message2` varchar(100) DEFAULT NULL,
  `cAge4Message1` varchar(100) DEFAULT NULL,
  `cAge4Message2` varchar(100) DEFAULT NULL,
  `cAge5Message1` varchar(100) DEFAULT NULL,
  `cAge5Message2` varchar(100) DEFAULT NULL,
  `cAge6Message1` varchar(100) DEFAULT NULL,
  `cAge6Message2` varchar(100) DEFAULT NULL,
  `cAge7Message1` varchar(100) DEFAULT NULL,
  `cAge7Message2` varchar(100) DEFAULT NULL,
  `_etblTerms_iBranchID` int(11) DEFAULT NULL,
  `_etblTerms_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblTerms_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblTerms_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblTerms_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblTerms_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblTerms_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblTerms_iChangeSetID` int(11) DEFAULT NULL,
  `_etblTerms_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblunitcategory`
--

DROP TABLE IF EXISTS `_etblunitcategory`;
CREATE TABLE `_etblunitcategory` (
  `idUnitCategory` int(11) NOT NULL,
  `cUnitCatDescription` varchar(20) NOT NULL,
  `_etblUnitCategory_iBranchID` int(11) DEFAULT NULL,
  `_etblUnitCategory_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblUnitCategory_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblUnitCategory_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblUnitCategory_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblUnitCategory_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblUnitCategory_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblUnitCategory_iChangeSetID` int(11) DEFAULT NULL,
  `_etblUnitCategory_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblunitconversion`
--

DROP TABLE IF EXISTS `_etblunitconversion`;
CREATE TABLE `_etblunitconversion` (
  `idUnitConversion` int(11) NOT NULL,
  `iUnitAID` int(11) NOT NULL,
  `fUnitAQty` double NOT NULL,
  `iUnitBID` int(11) NOT NULL,
  `fUnitBQty` double NOT NULL,
  `fMarkup` double DEFAULT NULL,
  `_etblUnitConversion_iBranchID` int(11) DEFAULT NULL,
  `_etblUnitConversion_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblUnitConversion_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblUnitConversion_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iChangeSetID` int(11) DEFAULT NULL,
  `_etblUnitConversion_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblunits`
--

DROP TABLE IF EXISTS `_etblunits`;
CREATE TABLE `_etblunits` (
  `idUnits` int(11) NOT NULL,
  `cUnitCode` varchar(50) DEFAULT NULL,
  `cUnitDescription` varchar(50) DEFAULT NULL,
  `iUnitCategoryID` int(11) DEFAULT NULL,
  `bUnitRoundUp` tinyint(1) NOT NULL DEFAULT '0',
  `_etblUnits_iBranchID` int(11) DEFAULT NULL,
  `_etblUnits_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblUnits_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblUnits_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblUnits_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblUnits_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblUnits_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblUnits_iChangeSetID` int(11) DEFAULT NULL,
  `_etblUnits_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvasairtimeitem`
--

DROP TABLE IF EXISTS `_etblvasairtimeitem`;
CREATE TABLE `_etblvasairtimeitem` (
  `idVASAirtimeItem` int(11) NOT NULL,
  `idVASAirtimeMaster` int(11) NOT NULL,
  `iStockLink` int(11) NOT NULL,
  `dDiscountPercentage` bigint(20) DEFAULT NULL,
  `_etblVASAirtimeItem_iBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeItem_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeItem_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeItem_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeItem_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeItem_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeItem_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeItem_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVASAirtimeItem_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvasairtimemaster`
--

DROP TABLE IF EXISTS `_etblvasairtimemaster`;
CREATE TABLE `_etblvasairtimemaster` (
  `idVASAirtimeMaster` int(11) NOT NULL,
  `cContractCode` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `cContractDescription` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `iSetupType` int(11) NOT NULL,
  `bActive` tinyint(1) DEFAULT NULL,
  `bDeleted` tinyint(1) DEFAULT NULL,
  `_etblVASAirtimeMaster_iBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeMaster_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeMaster_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeMaster_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeMaster_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeMaster_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeMaster_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeMaster_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVASAirtimeMaster_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvasairtimenetwork`
--

DROP TABLE IF EXISTS `_etblvasairtimenetwork`;
CREATE TABLE `_etblvasairtimenetwork` (
  `idVASAirtimeNetwork` int(11) NOT NULL,
  `idVASAirtimeMaster` int(11) NOT NULL,
  `cNetworkCode` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `cNetworkName` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cNetworkDescription` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `iStockLink` int(11) NOT NULL,
  `dDiscountPercentage` bigint(20) DEFAULT NULL,
  `_etblVASAirtimeNetwork_iBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeNetwork_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeNetwork_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeNetwork_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeNetwork_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeNetwork_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeNetwork_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeNetwork_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVASAirtimeNetwork_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvasairtimeproduct`
--

DROP TABLE IF EXISTS `_etblvasairtimeproduct`;
CREATE TABLE `_etblvasairtimeproduct` (
  `idVASAirtimeProduct` int(11) NOT NULL,
  `idVASAirtimeNetwork` int(11) NOT NULL,
  `cProductCode` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `cProductDescription` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `dProductPrice` bigint(20) NOT NULL,
  `iStockLink` int(11) NOT NULL,
  `dDiscountPercentage` bigint(20) DEFAULT NULL,
  `_etblVASAirtimeProduct_iBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeProduct_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeProduct_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVASAirtimeProduct_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeProduct_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVASAirtimeProduct_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeProduct_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVASAirtimeProduct_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVASAirtimeProduct_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvdap`
--

DROP TABLE IF EXISTS `_etblvdap`;
CREATE TABLE `_etblvdap` (
  `IDVD` int(11) NOT NULL,
  `iARAPID` int(11) DEFAULT NULL,
  `iGroupID` int(11) DEFAULT '0',
  `iCurrencyID` int(11) NOT NULL DEFAULT '0',
  `cContractName` varchar(40) DEFAULT NULL,
  `bOnHold` tinyint(1) NOT NULL DEFAULT '0',
  `tDescription` longtext,
  `bARAPAll` tinyint(1) DEFAULT '0',
  `bIsTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `_etblVDAP_iBranchID` int(11) DEFAULT NULL,
  `_etblVDAP_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVDAP_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVDAP_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVDAP_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVDAP_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVDAP_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVDAP_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVDAP_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvdar`
--

DROP TABLE IF EXISTS `_etblvdar`;
CREATE TABLE `_etblvdar` (
  `IDVD` int(11) NOT NULL,
  `iARAPID` int(11) DEFAULT NULL,
  `iGroupID` int(11) DEFAULT '0',
  `iCurrencyID` int(11) NOT NULL DEFAULT '0',
  `cContractName` varchar(40) DEFAULT NULL,
  `bOnHold` tinyint(1) NOT NULL DEFAULT '0',
  `tDescription` longtext,
  `bARAPAll` tinyint(1) DEFAULT '0',
  `bIsTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `_etblVDAR_iBranchID` int(11) DEFAULT NULL,
  `_etblVDAR_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVDAR_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVDAR_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVDAR_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVDAR_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVDAR_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVDAR_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVDAR_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvdlnap`
--

DROP TABLE IF EXISTS `_etblvdlnap`;
CREATE TABLE `_etblvdlnap` (
  `IDVDLn` int(11) NOT NULL,
  `iVDID` int(11) DEFAULT NULL,
  `iStockID` int(11) DEFAULT NULL,
  `iStGroupID` int(11) DEFAULT '0',
  `iCurrencyID` int(11) NOT NULL DEFAULT '0',
  `dEffDate` datetime DEFAULT NULL,
  `dExpDate` datetime DEFAULT NULL,
  `bUseStockPrc` tinyint(1) NOT NULL DEFAULT '0',
  `cEnterInclExcl` varchar(1) DEFAULT 'E',
  `bIncremental` tinyint(1) NOT NULL DEFAULT '0',
  `bStockAll` tinyint(1) DEFAULT '0',
  `_etblVDLnAP_iBranchID` int(11) DEFAULT NULL,
  `_etblVDLnAP_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnAP_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnAP_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnAP_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnAP_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnAP_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnAP_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVDLnAP_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvdlnar`
--

DROP TABLE IF EXISTS `_etblvdlnar`;
CREATE TABLE `_etblvdlnar` (
  `IDVDLn` int(11) NOT NULL,
  `iVDID` int(11) DEFAULT NULL,
  `iStockID` int(11) DEFAULT NULL,
  `iStGroupID` int(11) DEFAULT '0',
  `iCurrencyID` int(11) NOT NULL DEFAULT '0',
  `dEffDate` datetime DEFAULT NULL,
  `dExpDate` datetime DEFAULT NULL,
  `bUseStockPrc` tinyint(1) NOT NULL DEFAULT '0',
  `cEnterInclExcl` varchar(1) DEFAULT 'E',
  `bIncremental` tinyint(1) NOT NULL DEFAULT '0',
  `bStockAll` tinyint(1) DEFAULT '0',
  `_etblVDLnAR_iBranchID` int(11) DEFAULT NULL,
  `_etblVDLnAR_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnAR_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnAR_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnAR_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnAR_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnAR_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnAR_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVDLnAR_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvdlnlvlap`
--

DROP TABLE IF EXISTS `_etblvdlnlvlap`;
CREATE TABLE `_etblvdlnlvlap` (
  `IDVDLnLvl` int(11) NOT NULL,
  `iVDLnID` int(11) DEFAULT NULL,
  `iLevel` int(11) DEFAULT NULL,
  `fQuantity` double DEFAULT NULL,
  `fPriceDisc` double DEFAULT NULL,
  `_etblVDLnLvlAP_iBranchID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAP_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnLvlAP_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnLvlAP_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAP_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAP_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAP_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAP_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAP_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblvdlnlvlar`
--

DROP TABLE IF EXISTS `_etblvdlnlvlar`;
CREATE TABLE `_etblvdlnlvlar` (
  `IDVDLnLvl` int(11) NOT NULL,
  `iVDLnID` int(11) DEFAULT NULL,
  `iLevel` int(11) DEFAULT NULL,
  `fQuantity` double DEFAULT NULL,
  `fPriceDisc` double DEFAULT NULL,
  `_etblVDLnLvlAR_iBranchID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAR_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnLvlAR_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblVDLnLvlAR_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAR_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAR_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAR_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAR_iChangeSetID` int(11) DEFAULT NULL,
  `_etblVDLnLvlAR_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblwhdefaults`
--

DROP TABLE IF EXISTS `_etblwhdefaults`;
CREATE TABLE `_etblwhdefaults` (
  `IDWhDefaults` int(11) NOT NULL,
  `bWhTfBatchAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cWhTfBatchPrefix` varchar(20) DEFAULT NULL,
  `iWhTfBatchPadLength` int(11) DEFAULT NULL,
  `iWhTfTrCodeID` int(11) DEFAULT NULL,
  `bWhTfRefAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cWhTfRefPrefix` varchar(20) DEFAULT NULL,
  `iWhTfRefPadLength` int(11) DEFAULT NULL,
  `bIBTNumAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cIBTNumPrefix` varchar(20) DEFAULT NULL,
  `iIBTNumPadLength` int(11) DEFAULT NULL,
  `iIBTTrCodeID` int(11) DEFAULT NULL,
  `iIBTAddCostTrCodeID` int(11) DEFAULT NULL,
  `bIBTDelNoteAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cIBTDelNotePrefix` varchar(20) DEFAULT NULL,
  `iIBTDelNotePadLength` int(11) NOT NULL,
  `iDefInTransitWHID` int(11) DEFAULT NULL,
  `iDefVarianceWHID` int(11) DEFAULT NULL,
  `iDefDamagedWHID` int(11) DEFAULT NULL,
  `bWhseIBTActivated` tinyint(1) NOT NULL DEFAULT '0',
  `bForceProject` tinyint(1) NOT NULL DEFAULT '0',
  `iBranchLoanAccountID` int(11) DEFAULT NULL,
  `bIBTDefaultCostToIssue` tinyint(1) NOT NULL DEFAULT '0',
  `bIBTAllowOverDelivery` tinyint(1) NOT NULL DEFAULT '0',
  `cIBTRequestNumPrefix` varchar(20) DEFAULT NULL,
  `iIBTRequestNumPadLength` int(11) DEFAULT NULL,
  `bIBTRequestNumAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `_etblWhDefaults_iBranchID` int(11) DEFAULT NULL,
  `_etblWhDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWhDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWhDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWhDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWhDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWhDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWhDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWhDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblwhseibt`
--

DROP TABLE IF EXISTS `_etblwhseibt`;
CREATE TABLE `_etblwhseibt` (
  `IDWhseIBT` int(11) NOT NULL,
  `cIBTNumber` varchar(50) DEFAULT NULL,
  `cIBTDescription` varchar(40) DEFAULT NULL,
  `iWhseIDFrom` int(11) DEFAULT NULL,
  `iWhseIDTo` int(11) DEFAULT NULL,
  `iWhseIDIntransit` int(11) DEFAULT NULL,
  `iWhseIDVariance` int(11) DEFAULT NULL,
  `iWhseIDDamaged` int(11) DEFAULT NULL,
  `iIBTStatus` int(11) DEFAULT NULL,
  `cDelNoteNumber` varchar(50) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL DEFAULT '0',
  `dDateIssued` datetime(6) DEFAULT NULL,
  `dDateReceived` datetime(6) DEFAULT NULL,
  `cAuditNumberIssued` varchar(50) DEFAULT NULL,
  `cAuditNumberReceived` varchar(50) DEFAULT NULL,
  `bUseAddCostPerLine` tinyint(1) NOT NULL DEFAULT '0',
  `fFixedAddCost` double NOT NULL DEFAULT '0',
  `iAgentIDIssue` int(11) NOT NULL DEFAULT '0',
  `iAgentIDReceive` int(11) NOT NULL DEFAULT '0',
  `iBranchIDFrom` int(11) DEFAULT NULL,
  `dDateRequired` datetime(6) DEFAULT NULL,
  `dDateRequested` datetime(6) DEFAULT NULL,
  `dDateApproved` datetime(6) DEFAULT NULL,
  `iLinkedReqID` int(11) DEFAULT NULL,
  `_etblWhseIBT_iBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBT_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBT_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBT_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBT_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBT_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBT_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBT_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWhseIBT_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblwhseibtaddcosts`
--

DROP TABLE IF EXISTS `_etblwhseibtaddcosts`;
CREATE TABLE `_etblwhseibtaddcosts` (
  `IDWhseIBTAddCosts` int(11) NOT NULL,
  `iWhseIBTID` int(11) NOT NULL,
  `iSupplierID` int(11) NOT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `fLineTotalExcl` double NOT NULL DEFAULT '0',
  `iTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `fLineTaxAmount` double NOT NULL DEFAULT '0',
  `iCurrencyID` int(11) NOT NULL DEFAULT '0',
  `fExchangeRate` double NOT NULL DEFAULT '0',
  `fLineTotalExclForeign` double NOT NULL DEFAULT '0',
  `_etblWhseIBTAddCosts_iBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTAddCosts_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBTAddCosts_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBTAddCosts_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTAddCosts_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTAddCosts_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBTAddCosts_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBTAddCosts_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWhseIBTAddCosts_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblwhseibtlines`
--

DROP TABLE IF EXISTS `_etblwhseibtlines`;
CREATE TABLE `_etblwhseibtlines` (
  `IDWhseIBTLines` int(11) NOT NULL,
  `iWhseIBTID` int(11) DEFAULT NULL,
  `iStockID` int(11) DEFAULT NULL,
  `cReference` varchar(20) DEFAULT NULL,
  `cDescription` varchar(40) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL DEFAULT '0',
  `bIsSerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `bIsLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iLotID` int(11) DEFAULT NULL,
  `cLotNumber` varchar(50) DEFAULT NULL,
  `dLotExpiryDate` datetime(6) DEFAULT NULL,
  `fQtyIssued` double DEFAULT NULL,
  `fQtyReceived` double DEFAULT NULL,
  `fQtyDamaged` double DEFAULT NULL,
  `fQtyVariance` double DEFAULT NULL,
  `fQtyOverDelivered` double NOT NULL DEFAULT '0',
  `fNewReceiveCost` double DEFAULT NULL,
  `iSNIssuedGroupID` int(11) DEFAULT NULL,
  `iSNReceivedGroupID` int(11) DEFAULT NULL,
  `iSNDamagedGroupID` int(11) DEFAULT NULL,
  `iSNVarianceGroupID` int(11) DEFAULT NULL,
  `cLineNotes` varchar(1024) DEFAULT NULL,
  `fAdditionalCost` double NOT NULL DEFAULT '0',
  `fIssuedCost` double DEFAULT NULL,
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `fQtyRequired` double NOT NULL DEFAULT '0',
  `fQtyApproved` double NOT NULL DEFAULT '0',
  `iReqLineStatus` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_iBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBTLines_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBTLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWhseIBTLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblwhseibtlinesn`
--

DROP TABLE IF EXISTS `_etblwhseibtlinesn`;
CREATE TABLE `_etblwhseibtlinesn` (
  `IDWhseIBTLineSN` int(11) NOT NULL,
  `iWhseIBTID` int(11) DEFAULT NULL,
  `iSNGroupID` int(11) DEFAULT NULL,
  `iSerialMFID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_iBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBTLineSN_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWhseIBTLineSN_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWhseIBTLineSN_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblwhsetransferbatches`
--

DROP TABLE IF EXISTS `_etblwhsetransferbatches`;
CREATE TABLE `_etblwhsetransferbatches` (
  `idWhseTransferBatch` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDescription` varchar(40) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `iCreateAgentID` int(11) DEFAULT NULL,
  `bClearBatchAfterPost` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowDuplicateRef` tinyint(1) NOT NULL DEFAULT '1',
  `bPrintJournal` tinyint(1) NOT NULL DEFAULT '1',
  `iNewLineRefOpt` int(11) DEFAULT NULL,
  `cNewLineRefDef` varchar(20) DEFAULT NULL,
  `bNewLineRefIncr` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineDescOpt` int(11) DEFAULT NULL,
  `cNewLineDescDef` varchar(40) DEFAULT NULL,
  `bNewLineDescIncr` tinyint(1) NOT NULL DEFAULT '0',
  `iNewLineWHFromOpt` int(11) DEFAULT NULL,
  `iNewLineWHFromDefID` int(11) DEFAULT NULL,
  `iNewLineWHToOpt` int(11) DEFAULT NULL,
  `iNewLineWHToDefID` int(11) DEFAULT NULL,
  `iNewLineProjectOpt` int(11) DEFAULT NULL,
  `iNewLineProjectDefID` int(11) DEFAULT NULL,
  `cBatchRefNo` varchar(50) DEFAULT NULL,
  `_etblWhseTransferBatches_iBranchID` int(11) DEFAULT NULL,
  `_etblWhseTransferBatches_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWhseTransferBatches_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWhseTransferBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWhseTransferBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWhseTransferBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWhseTransferBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWhseTransferBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWhseTransferBatches_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_etblworkers`
--

DROP TABLE IF EXISTS `_etblworkers`;
CREATE TABLE `_etblworkers` (
  `idWorkers` int(11) NOT NULL,
  `cWorkerCode` varchar(20) NOT NULL,
  `cWorkerName` varchar(50) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `fWorkerCost` double DEFAULT NULL,
  `fBillableRate` double DEFAULT NULL,
  `_etblWorkers_iBranchID` int(11) DEFAULT NULL,
  `_etblWorkers_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblWorkers_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblWorkers_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblWorkers_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblWorkers_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblWorkers_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblWorkers_iChangeSetID` int(11) DEFAULT NULL,
  `_etblWorkers_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_mtblmbrcategories`
--

DROP TABLE IF EXISTS `_mtblmbrcategories`;
CREATE TABLE `_mtblmbrcategories` (
  `idMBRCategories` int(11) NOT NULL,
  `cMBRCategory` varchar(20) DEFAULT NULL,
  `cMBRDescription` varchar(100) DEFAULT NULL,
  `iMBRType` int(11) DEFAULT NULL,
  `iMBRCategoryLinkID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_iBranchID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_dCreatedDate` datetime(6) DEFAULT NULL,
  `_mtblMBRCategories_dModifiedDate` datetime(6) DEFAULT NULL,
  `_mtblMBRCategories_iCreatedBranchID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_iModifiedBranchID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_iCreatedAgentID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_iModifiedAgentID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_iChangeSetID` int(11) DEFAULT NULL,
  `_mtblMBRCategories_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retagentsession`
--

DROP TABLE IF EXISTS `_retagentsession`;
CREATE TABLE `_retagentsession` (
  `idAgentSession` int(11) NOT NULL,
  `iTradingSessionID` int(11) DEFAULT NULL,
  `bIsTillSession` tinyint(1) NOT NULL DEFAULT '0',
  `iTillSessionID` int(11) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `iTillID` int(11) DEFAULT NULL,
  `fAgentFloat` double DEFAULT NULL,
  `dCurrentDate` datetime(6) DEFAULT NULL,
  `bCashedUp` tinyint(1) DEFAULT '0',
  `cCashUpReference` varchar(50) DEFAULT NULL,
  `dCashUpDate` datetime(6) DEFAULT NULL,
  `iCashUpTillID` int(11) DEFAULT NULL,
  `bCashUpFinalised` tinyint(1) NOT NULL DEFAULT '0',
  `iFinaliseAgentID` int(11) DEFAULT NULL,
  `dFinalisedDate` datetime(6) DEFAULT NULL,
  `fAgentFloatCounted` double DEFAULT NULL,
  `fAgentFloatFinalised` double DEFAULT NULL,
  `iFloatInitialiseAgentID` int(11) DEFAULT NULL,
  `iCashUpAgentID` int(11) DEFAULT NULL,
  `_retAgentSession_iBranchID` int(11) DEFAULT NULL,
  `_retAgentSession_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retAgentSession_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retAgentSession_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retAgentSession_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retAgentSession_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retAgentSession_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retAgentSession_iChangeSetID` int(11) DEFAULT NULL,
  `_retAgentSession_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retcashpickup`
--

DROP TABLE IF EXISTS `_retcashpickup`;
CREATE TABLE `_retcashpickup` (
  `idCashPickup` int(11) NOT NULL,
  `iAgentSessionID` int(11) DEFAULT NULL,
  `iTillID` int(11) DEFAULT NULL,
  `dPickupDate` datetime(6) DEFAULT NULL,
  `cCashPickupReference` varchar(50) DEFAULT NULL,
  `fPickupAmount` double DEFAULT NULL,
  `_retCashPickup_iBranchID` int(11) DEFAULT NULL,
  `_retCashPickup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retCashPickup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retCashPickup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retCashPickup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retCashPickup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retCashPickup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retCashPickup_iChangeSetID` int(11) DEFAULT NULL,
  `_retCashPickup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retdefaults`
--

DROP TABLE IF EXISTS `_retdefaults`;
CREATE TABLE `_retdefaults` (
  `idRetailDefaults` int(11) NOT NULL,
  `fDefaultFloatAmount` double DEFAULT NULL,
  `fDefaultCashPickupAmount` double DEFAULT NULL,
  `iDefaultCustomerID` int(11) DEFAULT NULL,
  `iModelCustomerID` int(11) DEFAULT NULL,
  `cAutoNumBranchPrefix` varchar(3) DEFAULT NULL,
  `iDocketTrCodeID` int(11) DEFAULT NULL,
  `iDocketAdjTrCodeID` int(11) DEFAULT NULL,
  `iCashUpBankTrCodeID` int(11) DEFAULT NULL,
  `iCashUpExcessTrCodeID` int(11) DEFAULT NULL,
  `iCashUpShortageTrCodeID` int(11) DEFAULT NULL,
  `iPettyCashAdvancedTrCodeID` int(11) DEFAULT NULL,
  `iPettyCashProcessedTrCodeID` int(11) DEFAULT NULL,
  `cPoleDisplayDef1` varchar(20) DEFAULT NULL,
  `cPoleDisplayDef2` varchar(20) DEFAULT NULL,
  `iReceiptDiscountTrCodeID` int(11) DEFAULT NULL,
  `bAllocationPrompt` tinyint(1) NOT NULL DEFAULT '0',
  `bVariableBarcodesEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `iVariableBarcodePriceListID` int(11) DEFAULT NULL,
  `iDeliveryMethodDefaultID` int(11) DEFAULT NULL,
  `bDocketForceReps` tinyint(1) NOT NULL DEFAULT '0',
  `bDocketPromptForCustomerAccount` tinyint(1) NOT NULL DEFAULT '0',
  `bPettyCashAdvanceAuthorisation` tinyint(1) NOT NULL DEFAULT '0',
  `fPettyCashAdvanceLimit` double DEFAULT NULL,
  `bRestrictPettyCashAdvance` tinyint(1) NOT NULL DEFAULT '0',
  `bCashPickupWarning` tinyint(1) NOT NULL DEFAULT '0',
  `cCashPickupWarningMessage` varchar(50) DEFAULT NULL,
  `fCashPickupMaxCashInTillLimit` double DEFAULT NULL,
  `bDepositUse` tinyint(1) NOT NULL DEFAULT '0',
  `bDepositForce` tinyint(1) NOT NULL DEFAULT '0',
  `iDepositTrCodeID` int(11) DEFAULT NULL,
  `fDepositMinPerc` double DEFAULT NULL,
  `fDepositMinAmnt` double DEFAULT NULL,
  `bDepositAllow` tinyint(1) NOT NULL DEFAULT '0',
  `fDepositGreaterThan` double DEFAULT NULL,
  `iInactivityTimeout` int(11) DEFAULT NULL,
  `iReversalInvJrBatchID` int(11) DEFAULT NULL,
  `iReversalAdjTrCodeID` int(11) DEFAULT NULL,
  `iLayByModelCustomerID` int(11) DEFAULT NULL,
  `iReversalAgentID` int(11) DEFAULT NULL,
  `iLayByPaymentTermCount` int(11) DEFAULT NULL,
  `iLayByPaymentTermOnDay` int(11) DEFAULT NULL,
  `iLayByPaymentTermOfEvery` int(11) DEFAULT NULL,
  `fLayByDepositMinPerc` double DEFAULT NULL,
  `fLayByDepositMinAmnt` double DEFAULT NULL,
  `fLayByCancellationPerc` double DEFAULT NULL,
  `fLayByCancellationAmnt` double DEFAULT NULL,
  `iLayByCancellationTrCodeID` int(11) DEFAULT NULL,
  `bReserveStockOrders` tinyint(1) NOT NULL DEFAULT '0',
  `bReserveStockLayBys` tinyint(1) NOT NULL DEFAULT '0',
  `bPromoAutoYN` tinyint(1) NOT NULL DEFAULT '0',
  `iPromoAutoLength` int(11) NOT NULL DEFAULT '6',
  `iPromoAutoAlphaLength` int(11) NOT NULL DEFAULT '3',
  `bUpperPromoNo` tinyint(1) NOT NULL DEFAULT '0',
  `iKeepAsideDaysToExpiry` int(11) NOT NULL DEFAULT '7',
  `iDisplayDocketPromptFields` int(11) NOT NULL DEFAULT '0',
  `iForceDocketPromptFields` int(11) NOT NULL DEFAULT '0',
  `bLayByFirstPaymentInCurrentPeriod` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoAdjust` tinyint(1) NOT NULL DEFAULT '0',
  `cDocketModeDocketPromptFields` varchar(400) DEFAULT NULL,
  `cTillConfigPassword` varchar(160) DEFAULT NULL,
  `bDisplayDocketPromotionCode` tinyint(1) NOT NULL DEFAULT '1',
  `bDisplayDocketPromotionDescription` tinyint(1) NOT NULL DEFAULT '0',
  `fReturnLimit` double DEFAULT NULL,
  `bReturnAuthorisation` tinyint(1) NOT NULL DEFAULT '0',
  `bRestrictReturn` tinyint(1) NOT NULL DEFAULT '0',
  `bPromoItemListAutoYN` tinyint(1) NOT NULL DEFAULT '0',
  `bUpperPromoItemListNo` tinyint(1) NOT NULL DEFAULT '0',
  `iPromoItemListAutoLength` int(11) NOT NULL DEFAULT '6',
  `iPromoItemListAutoAlphaLength` int(11) NOT NULL DEFAULT '3',
  `bLayByEdit` tinyint(1) NOT NULL DEFAULT '0',
  `bLineForceReps` tinyint(1) NOT NULL DEFAULT '0',
  `bFloatPerAgentOrTill` tinyint(1) NOT NULL DEFAULT '0',
  `iTradingSessionDuration` int(11) DEFAULT NULL,
  `bDisableTradeOnExpiry` tinyint(1) NOT NULL DEFAULT '0',
  `bForceSerialNumbers` tinyint(1) NOT NULL DEFAULT '0',
  `bUseDocketRoundingOnTender` tinyint(1) DEFAULT '0',
  `bRoundingOnCashTenderOnly` tinyint(1) DEFAULT '0',
  `iRoundingGLAccount` int(11) DEFAULT NULL,
  `bAllowCashDrawerHandover` tinyint(1) DEFAULT NULL,
  `iInvSplitProcessOption` int(11) NOT NULL DEFAULT '0',
  `bUseVASAirtime` tinyint(1) DEFAULT '0',
  `cVASAirtimeMerchantID` varchar(40) DEFAULT NULL,
  `cVASAirtimeHostURI` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `_retDefaults_iBranchID` int(11) DEFAULT NULL,
  `_retDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_retDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retdenomination`
--

DROP TABLE IF EXISTS `_retdenomination`;
CREATE TABLE `_retdenomination` (
  `idDenomination` int(11) NOT NULL,
  `cDenominationCode` varchar(10) DEFAULT NULL,
  `mMultiple` decimal(19,4) DEFAULT NULL,
  `bIsCoin` tinyint(1) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_retDenomination_iBranchID` int(11) DEFAULT NULL,
  `_retDenomination_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retDenomination_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retDenomination_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retDenomination_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retDenomination_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retDenomination_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retDenomination_iChangeSetID` int(11) DEFAULT NULL,
  `_retDenomination_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retdiscountreason`
--

DROP TABLE IF EXISTS `_retdiscountreason`;
CREATE TABLE `_retdiscountreason` (
  `idDiscountReason` int(11) NOT NULL,
  `cDiscountReasonCode` varchar(10) DEFAULT NULL,
  `cDiscountReasonDesc` varchar(30) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_retDiscountReason_iBranchID` int(11) DEFAULT NULL,
  `_retDiscountReason_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retDiscountReason_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retDiscountReason_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retDiscountReason_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retDiscountReason_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retDiscountReason_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retDiscountReason_iChangeSetID` int(11) DEFAULT NULL,
  `_retDiscountReason_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retdocketlock`
--

DROP TABLE IF EXISTS `_retdocketlock`;
CREATE TABLE `_retdocketlock` (
  `idDocketLock` int(11) NOT NULL,
  `iDocketID` bigint(20) NOT NULL,
  `iTillID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retlaybys`
--

DROP TABLE IF EXISTS `_retlaybys`;
CREATE TABLE `_retlaybys` (
  `idLayBys` int(11) NOT NULL,
  `iInvoiceID` bigint(20) DEFAULT NULL,
  `iTermCount` int(11) DEFAULT NULL,
  `iTermOnDay` int(11) DEFAULT NULL,
  `iTermOfEvery` int(11) DEFAULT NULL,
  `fLayByTotal` double DEFAULT NULL,
  `dInceptionDate` datetime(6) DEFAULT NULL,
  `iTermsElapsed` int(11) DEFAULT NULL,
  `fPaidToDate` double DEFAULT NULL,
  `dLastPaymentDate` datetime(6) DEFAULT NULL,
  `dNextPaymentDate` datetime(6) DEFAULT NULL,
  `dFinalPaymentDate` datetime(6) DEFAULT NULL,
  `fLayByDeposit` double DEFAULT NULL,
  `fInstallmentAmount` double DEFAULT NULL,
  `dLastUpdated` datetime(6) DEFAULT NULL,
  `fPaymentDue` double DEFAULT NULL,
  `iInvoiceIDFinalised` bigint(20) DEFAULT NULL,
  `fCancellationFee` double DEFAULT NULL,
  `fCancellationFeeTax` double DEFAULT NULL,
  `_retLayBys_iBranchID` int(11) DEFAULT NULL,
  `_retLayBys_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retLayBys_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retLayBys_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retLayBys_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retLayBys_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retLayBys_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retLayBys_iChangeSetID` int(11) DEFAULT NULL,
  `_retLayBys_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retpettycash`
--

DROP TABLE IF EXISTS `_retpettycash`;
CREATE TABLE `_retpettycash` (
  `idPettyCash` int(11) NOT NULL,
  `iPettyCashTypeID` int(11) DEFAULT NULL,
  `cComment` varchar(128) DEFAULT NULL,
  `iAdvancedAgentSessionID` int(11) DEFAULT NULL,
  `dAdvancedDate` datetime(6) DEFAULT NULL,
  `fAdvancedAmount` double DEFAULT NULL,
  `bProcessed` tinyint(1) NOT NULL DEFAULT '0',
  `iProcessedAgentSessionID` int(11) DEFAULT NULL,
  `dProcessedDate` datetime(6) DEFAULT NULL,
  `fChangeAmount` double DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `iAdvancedTillID` int(11) DEFAULT NULL,
  `iProcessedTillID` int(11) DEFAULT NULL,
  `_retPettyCash_iBranchID` int(11) DEFAULT NULL,
  `_retPettyCash_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPettyCash_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPettyCash_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPettyCash_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPettyCash_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPettyCash_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPettyCash_iChangeSetID` int(11) DEFAULT NULL,
  `_retPettyCash_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retpettycashline`
--

DROP TABLE IF EXISTS `_retpettycashline`;
CREATE TABLE `_retpettycashline` (
  `idPettyCashLine` int(11) NOT NULL,
  `iPettyCashID` int(11) DEFAULT NULL,
  `cReference` varchar(128) DEFAULT NULL,
  `fExclAmount` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `fTaxRate` double DEFAULT NULL,
  `fInclAmount` double DEFAULT NULL,
  `_retPettyCashLine_iBranchID` int(11) DEFAULT NULL,
  `_retPettyCashLine_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPettyCashLine_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPettyCashLine_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPettyCashLine_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPettyCashLine_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPettyCashLine_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPettyCashLine_iChangeSetID` int(11) DEFAULT NULL,
  `_retPettyCashLine_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retpettycashtype`
--

DROP TABLE IF EXISTS `_retpettycashtype`;
CREATE TABLE `_retpettycashtype` (
  `idPettyCashType` int(11) NOT NULL,
  `cPettyCashTypeCode` varchar(10) DEFAULT NULL,
  `cPettyCashTypeDesc` varchar(30) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `iPettyCashLedgerID` int(11) DEFAULT NULL,
  `iPettyCashTaxTypeID` int(11) DEFAULT NULL,
  `iPettyCashTaxLedgerID` int(11) DEFAULT NULL,
  `_retPettyCashType_iBranchID` int(11) DEFAULT NULL,
  `_retPettyCashType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPettyCashType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPettyCashType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPettyCashType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPettyCashType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPettyCashType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPettyCashType_iChangeSetID` int(11) DEFAULT NULL,
  `_retPettyCashType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retposlogfile`
--

DROP TABLE IF EXISTS `_retposlogfile`;
CREATE TABLE `_retposlogfile` (
  `idPOSLogFile` int(11) NOT NULL,
  `iSystemFunctionID` int(11) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `iTillID` int(11) NOT NULL,
  `iSupervisorAgentID` int(11) DEFAULT NULL,
  `dCurrentDate` datetime(6) DEFAULT NULL,
  `cDescription` varchar(1000) DEFAULT NULL,
  `iTradingSessionID` int(11) DEFAULT NULL,
  `_retPOSLogFile_iBranchID` int(11) DEFAULT NULL,
  `_retPOSLogFile_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPOSLogFile_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPOSLogFile_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPOSLogFile_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPOSLogFile_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPOSLogFile_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPOSLogFile_iChangeSetID` int(11) DEFAULT NULL,
  `_retPOSLogFile_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retposloglinks`
--

DROP TABLE IF EXISTS `_retposloglinks`;
CREATE TABLE `_retposloglinks` (
  `idPOSLogLinks` bigint(20) NOT NULL,
  `iInvNumID` int(11) DEFAULT NULL,
  `iInvLineID` bigint(20) DEFAULT NULL,
  `iLogID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_iBranchID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPOSLogLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPOSLogLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_retPOSLogLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retposmenu`
--

DROP TABLE IF EXISTS `_retposmenu`;
CREATE TABLE `_retposmenu` (
  `idPOSMenu` bigint(20) NOT NULL,
  `iPOSKeyIndex` int(11) DEFAULT NULL,
  `cPOSKeyCaption` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iPOSKeyColor` int(11) DEFAULT NULL,
  `iPOSKeyFontColor` int(11) DEFAULT NULL,
  `iCustomIndex` int(11) DEFAULT NULL,
  `idPOSMenuSetup` bigint(20) DEFAULT NULL,
  `cSkinName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `bUseDesignColour` tinyint(1) DEFAULT NULL,
  `iStockLink` int(11) DEFAULT '0',
  `iMID` int(11) DEFAULT '0',
  `_retPOSMenu_iBranchID` int(11) DEFAULT NULL,
  `_retPOSMenu_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPOSMenu_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPOSMenu_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPOSMenu_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPOSMenu_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPOSMenu_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPOSMenu_iChangeSetID` int(11) DEFAULT NULL,
  `_retPOSMenu_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retposmenusetup`
--

DROP TABLE IF EXISTS `_retposmenusetup`;
CREATE TABLE `_retposmenusetup` (
  `idPOSMenuSetup` bigint(20) NOT NULL,
  `cPOSMenuCode` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cPOSMenuDescription` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iBackgroundColour` int(11) DEFAULT NULL,
  `bTransparentDisabledButtons` tinyint(1) DEFAULT NULL,
  `_retPOSMenuSetup_iBranchID` int(11) DEFAULT NULL,
  `_retPOSMenuSetup_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPOSMenuSetup_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPOSMenuSetup_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPOSMenuSetup_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPOSMenuSetup_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPOSMenuSetup_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPOSMenuSetup_iChangeSetID` int(11) DEFAULT NULL,
  `_retPOSMenuSetup_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retpostender`
--

DROP TABLE IF EXISTS `_retpostender`;
CREATE TABLE `_retpostender` (
  `idPOSTender` bigint(20) NOT NULL,
  `iPOSTransactionID` bigint(20) DEFAULT NULL,
  `iTenderTypeID` int(11) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `cNarrative` varchar(1024) DEFAULT NULL,
  `cCardNumber` varchar(20) DEFAULT NULL,
  `cCardHolder` varchar(100) DEFAULT NULL,
  `dExpiryDate` datetime(6) DEFAULT NULL,
  `cEMVApplicationID` varchar(40) DEFAULT NULL,
  `cEMVVerification` varchar(10) DEFAULT NULL,
  `cEMVTrCertificate` varchar(20) DEFAULT NULL,
  `cEMVApplLabel` varchar(20) DEFAULT NULL,
  `cCardType` varchar(200) DEFAULT NULL,
  `cAuthCode` varchar(6) DEFAULT NULL,
  `dEFTDateTime` datetime(6) DEFAULT NULL,
  `cEMVTSI` varchar(4) DEFAULT NULL,
  `idInvoiceDeposits` int(11) DEFAULT NULL,
  `cEFTBudgetPeriod` varchar(2) NOT NULL DEFAULT '0',
  `cAuthorisationID` varchar(6) DEFAULT NULL,
  `cInstitutionID` varchar(11) DEFAULT NULL,
  `cTransactionType` varchar(2) DEFAULT NULL,
  `cAccountType` varchar(2) DEFAULT NULL,
  `cE0210RespCode` varchar(2) DEFAULT NULL,
  `cE0202RespCode` varchar(2) DEFAULT NULL,
  `bChipCard` tinyint(1) NOT NULL DEFAULT '0',
  `cEFTReferenceNumber` varchar(20) DEFAULT NULL,
  `bManualEFT` tinyint(1) NOT NULL DEFAULT '0',
  `_retPOSTender_iBranchID` int(11) DEFAULT NULL,
  `_retPOSTender_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPOSTender_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPOSTender_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPOSTender_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPOSTender_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPOSTender_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPOSTender_iChangeSetID` int(11) DEFAULT NULL,
  `_retPOSTender_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retpostransaction`
--

DROP TABLE IF EXISTS `_retpostransaction`;
CREATE TABLE `_retpostransaction` (
  `idPOSTransaction` bigint(20) NOT NULL,
  `iTillID` int(11) DEFAULT NULL,
  `dTransactionDate` datetime(6) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `iTrCodesID` int(11) DEFAULT NULL,
  `iAccountID` int(11) DEFAULT NULL,
  `iTillTxType` int(11) DEFAULT NULL,
  `cAuditNumber` varchar(50) DEFAULT NULL,
  `fAmount` double DEFAULT NULL,
  `fAmountTendered` double DEFAULT NULL,
  `fAmountChange` double DEFAULT NULL,
  `iAgentSessionID` int(11) DEFAULT NULL,
  `iInvNumID` bigint(20) DEFAULT NULL,
  `_retPOSTransaction_iBranchID` int(11) DEFAULT NULL,
  `_retPOSTransaction_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPOSTransaction_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPOSTransaction_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPOSTransaction_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPOSTransaction_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPOSTransaction_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPOSTransaction_iChangeSetID` int(11) DEFAULT NULL,
  `_retPOSTransaction_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retpriceoverridereason`
--

DROP TABLE IF EXISTS `_retpriceoverridereason`;
CREATE TABLE `_retpriceoverridereason` (
  `idPriceOverrideReason` int(11) NOT NULL,
  `cPriceOverrideReasonCode` varchar(10) DEFAULT NULL,
  `cPriceOverrideReasonDesc` varchar(30) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_retPriceOverrideReason_iBranchID` int(11) DEFAULT NULL,
  `_retPriceOverrideReason_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retPriceOverrideReason_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retPriceOverrideReason_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retPriceOverrideReason_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retPriceOverrideReason_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retPriceOverrideReason_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retPriceOverrideReason_iChangeSetID` int(11) DEFAULT NULL,
  `_retPriceOverrideReason_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retreturnreason`
--

DROP TABLE IF EXISTS `_retreturnreason`;
CREATE TABLE `_retreturnreason` (
  `idReturnReason` int(11) NOT NULL,
  `cReturnReasonCode` varchar(10) DEFAULT NULL,
  `cReturnReasonDesc` varchar(30) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_retReturnReason_iBranchID` int(11) DEFAULT NULL,
  `_retReturnReason_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retReturnReason_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retReturnReason_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retReturnReason_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retReturnReason_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retReturnReason_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retReturnReason_iChangeSetID` int(11) DEFAULT NULL,
  `_retReturnReason_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rettendertype`
--

DROP TABLE IF EXISTS `_rettendertype`;
CREATE TABLE `_rettendertype` (
  `idTenderType` int(11) NOT NULL,
  `cTenderTypeCode` varchar(10) DEFAULT NULL,
  `cTenderTypeDesc` varchar(30) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `iDisplayOrder` int(11) DEFAULT NULL,
  `bAllowOverTender` tinyint(1) NOT NULL DEFAULT '0',
  `bOpenDrawer` tinyint(1) NOT NULL DEFAULT '0',
  `fHouseLimit` double DEFAULT NULL,
  `bRequireNarration` tinyint(1) NOT NULL DEFAULT '0',
  `iReceiptTrCodeID` int(11) DEFAULT NULL,
  `iRefundTrCodeID` int(11) DEFAULT NULL,
  `iDepositTrCodeID` double DEFAULT NULL,
  `iTypeOfTender` int(11) DEFAULT NULL,
  `iCardDisplayFirst` int(11) DEFAULT NULL,
  `iCardDisplayLast` int(11) DEFAULT NULL,
  `bForceCardNumber` tinyint(1) NOT NULL DEFAULT '0',
  `bForceCardHolder` tinyint(1) NOT NULL DEFAULT '0',
  `cExpiryFormat` varchar(20) DEFAULT NULL,
  `bForceExpiry` tinyint(1) NOT NULL DEFAULT '0',
  `bUsePinPad` tinyint(1) DEFAULT NULL,
  `bApplyDocketRounding` tinyint(1) DEFAULT '0',
  `_retTenderType_iBranchID` int(11) DEFAULT NULL,
  `_retTenderType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retTenderType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retTenderType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retTenderType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retTenderType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retTenderType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retTenderType_iChangeSetID` int(11) DEFAULT NULL,
  `_retTenderType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rettill`
--

DROP TABLE IF EXISTS `_rettill`;
CREATE TABLE `_rettill` (
  `idTill` int(11) NOT NULL,
  `cTillCode` varchar(6) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `iCurrentAgentID` int(11) DEFAULT NULL,
  `bAutoNumPrependBranch` tinyint(1) NOT NULL DEFAULT '1',
  `iAutoNumInvNext` int(11) DEFAULT NULL,
  `iAutoNumInvPad` int(11) DEFAULT NULL,
  `cAutoNumInvPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumOrdNext` int(11) DEFAULT NULL,
  `iAutoNumOrdPad` int(11) DEFAULT NULL,
  `cAutoNumOrdPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumCrnNext` int(11) DEFAULT NULL,
  `iAutoNumCrnPad` int(11) DEFAULT NULL,
  `cAutoNumCrnPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumQuoNext` int(11) DEFAULT NULL,
  `iAutoNumQuoPad` int(11) DEFAULT NULL,
  `cAutoNumQuoPrefix` varchar(20) DEFAULT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iAutoNumCashUpNext` int(11) DEFAULT NULL,
  `iAutoNumCashUpPad` int(11) DEFAULT NULL,
  `cAutoNumCashUpPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumPettyCashNext` int(11) DEFAULT NULL,
  `iAutoNumPettyCashPad` int(11) DEFAULT NULL,
  `cAutoNumPettyCashPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumCashPickupNext` int(11) DEFAULT NULL,
  `iAutoNumCashPickupPad` int(11) DEFAULT NULL,
  `cAutoNumCashPickupPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumReceiptNext` int(11) DEFAULT NULL,
  `iAutoNumReceiptPad` int(11) DEFAULT NULL,
  `cAutoNumReceiptPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumRefundNext` int(11) DEFAULT NULL,
  `iAutoNumRefundPad` int(11) DEFAULT NULL,
  `cAutoNumRefundPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumLayByReceiptNext` int(11) DEFAULT NULL,
  `iAutoNumLayByReceiptPad` int(11) DEFAULT NULL,
  `cAutoNumLayByReceiptPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumLayByRefundNext` int(11) DEFAULT NULL,
  `iAutoNumLayByRefundPad` int(11) DEFAULT NULL,
  `cAutoNumLayByRefundPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumDelivNoteNext` int(11) DEFAULT NULL,
  `iAutoNumDelivNotePad` int(11) DEFAULT NULL,
  `cAutoNumDelivNotePrefix` varchar(20) DEFAULT NULL,
  `iAutoNumLayByNext` int(11) DEFAULT NULL,
  `iAutoNumLayByPad` int(11) DEFAULT NULL,
  `cAutoNumLayByPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumKeepAsideNext` int(11) DEFAULT NULL,
  `iAutoNumKeepAsidePad` int(11) DEFAULT NULL,
  `iAutoNumKeepAsidePrefix` varchar(20) DEFAULT NULL,
  `iDocketInputMode` int(11) NOT NULL DEFAULT '0',
  `iAutoNumCashDrawerHandoverNext` int(11) DEFAULT NULL,
  `iAutoNumCashDrawerHandoverPad` int(11) DEFAULT NULL,
  `cAutoNumCashDrawerHandoverPrefix` varchar(20) DEFAULT NULL,
  `bUseOnScreenKeyboard` tinyint(1) DEFAULT '0',
  `idPOSMenuSetup` bigint(20) DEFAULT NULL,
  `iAutoNumGIVNext` int(11) DEFAULT NULL,
  `iAutoNumGIVPad` int(11) DEFAULT NULL,
  `cAutoNumGIVPrefix` varchar(20) DEFAULT NULL,
  `iAutoNumCGRNext` int(11) DEFAULT NULL,
  `iAutoNumCGRPad` int(11) DEFAULT NULL,
  `cAutoNumCGRPrefix` varchar(20) DEFAULT NULL,
  `_retTill_iBranchID` int(11) DEFAULT NULL,
  `_retTill_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retTill_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retTill_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retTill_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retTill_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retTill_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retTill_iChangeSetID` int(11) DEFAULT NULL,
  `_retTill_Checksum` binary(20) DEFAULT NULL,
  `iTillLoginScreen` int(11) NOT NULL DEFAULT '0',
  `bAutoLogout` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rettillsecurity`
--

DROP TABLE IF EXISTS `_rettillsecurity`;
CREATE TABLE `_rettillsecurity` (
  `idTillSecurity` int(11) NOT NULL,
  `iSystemFunction` int(11) NOT NULL,
  `iPermission` int(11) DEFAULT NULL,
  `_retTillSecurity_iBranchID` int(11) DEFAULT NULL,
  `_retTillSecurity_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retTillSecurity_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retTillSecurity_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retTillSecurity_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retTillSecurity_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retTillSecurity_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retTillSecurity_iChangeSetID` int(11) DEFAULT NULL,
  `_retTillSecurity_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rettillstationery`
--

DROP TABLE IF EXISTS `_rettillstationery`;
CREATE TABLE `_rettillstationery` (
  `idTillStationery` int(11) NOT NULL,
  `iSloTypeID` int(11) DEFAULT NULL,
  `iSloSource` int(11) DEFAULT NULL,
  `iSloLayoutID` int(11) DEFAULT NULL,
  `iPrinterPaperSize` int(11) DEFAULT NULL,
  `iPrinterCopies` int(11) DEFAULT NULL,
  `iPrinterCollate` int(11) DEFAULT NULL,
  `iPrinterDuplex` int(11) DEFAULT NULL,
  `iEmailFormatIndex` int(11) DEFAULT NULL,
  `bZip` tinyint(1) DEFAULT NULL,
  `cEmailDefaultSubject` varchar(255) DEFAULT NULL,
  `cEmailDefaultBody` varchar(1024) DEFAULT NULL,
  `bEmailDifferentLayout` tinyint(1) DEFAULT NULL,
  `iEmailLayoutIndex` int(11) DEFAULT NULL,
  `_retTillStationery_iBranchID` int(11) DEFAULT NULL,
  `_retTillStationery_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retTillStationery_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retTillStationery_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retTillStationery_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retTillStationery_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retTillStationery_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retTillStationery_iChangeSetID` int(11) DEFAULT NULL,
  `_retTillStationery_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rettradingsession`
--

DROP TABLE IF EXISTS `_rettradingsession`;
CREATE TABLE `_rettradingsession` (
  `idTradingSession` int(11) NOT NULL,
  `iSessionStatus` int(11) DEFAULT NULL,
  `dTradingDate` datetime(6) DEFAULT NULL,
  `cSessionDescription` varchar(50) DEFAULT NULL,
  `dStartTime` datetime(6) DEFAULT NULL,
  `iStartAgentID` int(11) DEFAULT NULL,
  `dEndTime` datetime(6) DEFAULT NULL,
  `iEndAgentID` int(11) DEFAULT NULL,
  `iFinaliseAgentID` int(11) DEFAULT NULL,
  `iExpectedDuration` int(11) DEFAULT NULL,
  `bDisableTradingOnExpiry` tinyint(1) NOT NULL DEFAULT '0',
  `_retTradingSession_iBranchID` int(11) DEFAULT NULL,
  `_retTradingSession_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retTradingSession_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retTradingSession_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retTradingSession_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retTradingSession_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retTradingSession_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retTradingSession_iChangeSetID` int(11) DEFAULT NULL,
  `_retTradingSession_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_retvariablebarcode`
--

DROP TABLE IF EXISTS `_retvariablebarcode`;
CREATE TABLE `_retvariablebarcode` (
  `idVariableBarcode` int(11) NOT NULL,
  `cCode` varchar(10) DEFAULT NULL,
  `cDesc` varchar(30) DEFAULT NULL,
  `cPrefix` varchar(2) DEFAULT NULL,
  `iFullLength` int(11) DEFAULT NULL,
  `iItemStart` int(11) DEFAULT NULL,
  `iItemLength` int(11) DEFAULT NULL,
  `iValueStart` int(11) DEFAULT NULL,
  `iValueLength` int(11) DEFAULT NULL,
  `iValueDecimals` int(11) DEFAULT NULL,
  `bValueType` tinyint(1) NOT NULL,
  `_retVariableBarcode_iBranchID` int(11) DEFAULT NULL,
  `_retVariableBarcode_dCreatedDate` datetime(6) DEFAULT NULL,
  `_retVariableBarcode_dModifiedDate` datetime(6) DEFAULT NULL,
  `_retVariableBarcode_iCreatedBranchID` int(11) DEFAULT NULL,
  `_retVariableBarcode_iModifiedBranchID` int(11) DEFAULT NULL,
  `_retVariableBarcode_iCreatedAgentID` int(11) DEFAULT NULL,
  `_retVariableBarcode_iModifiedAgentID` int(11) DEFAULT NULL,
  `_retVariableBarcode_iChangeSetID` int(11) DEFAULT NULL,
  `_retVariableBarcode_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblagentgroupmembers`
--

DROP TABLE IF EXISTS `_rtblagentgroupmembers`;
CREATE TABLE `_rtblagentgroupmembers` (
  `iGroupID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `bSysGroupMember` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblAgentGroupMembers_iBranchID` int(11) DEFAULT NULL,
  `_rtblAgentGroupMembers_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblAgentGroupMembers_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblAgentGroupMembers_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblAgentGroupMembers_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblAgentGroupMembers_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblAgentGroupMembers_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblAgentGroupMembers_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblAgentGroupMembers_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblagentgroups`
--

DROP TABLE IF EXISTS `_rtblagentgroups`;
CREATE TABLE `_rtblagentgroups` (
  `idAgentGroups` int(11) NOT NULL,
  `bSysGroup` tinyint(1) NOT NULL DEFAULT '0',
  `cGroupName` varchar(30) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `cComments` varchar(1024) DEFAULT NULL,
  `bCanAssign` tinyint(1) NOT NULL DEFAULT '1',
  `iAssignRule` int(11) NOT NULL DEFAULT '0',
  `iAssignAgent` int(11) DEFAULT NULL,
  `bUseDefaultTree` tinyint(1) NOT NULL DEFAULT '1',
  `bCBGrpAllVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bCBGrpNoneVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bJRGrpAllVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bJRGrpNoneVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bCBGrpAgentVisible` tinyint(1) NOT NULL DEFAULT '1',
  `bJRGrpAgentVisible` tinyint(1) NOT NULL DEFAULT '1',
  `iPOAuthType` int(11) NOT NULL DEFAULT '0',
  `iPOIncidentTypeID` int(11) DEFAULT NULL,
  `bPOExclusive` tinyint(1) NOT NULL DEFAULT '1',
  `fPOLimit` double DEFAULT NULL,
  `bPOUseDefaults` tinyint(1) NOT NULL DEFAULT '1',
  `idPOSMenuSetup` int(11) DEFAULT NULL,
  `_rtblAgentGroups_iBranchID` int(11) DEFAULT NULL,
  `_rtblAgentGroups_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblAgentGroups_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblAgentGroups_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblAgentGroups_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblAgentGroups_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblAgentGroups_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblAgentGroups_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblAgentGroups_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblagentlockedout`
--

DROP TABLE IF EXISTS `_rtblagentlockedout`;
CREATE TABLE `_rtblagentlockedout` (
  `IDAgentLockedOut` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `dLockedOutDate` datetime(6) NOT NULL,
  `dUnlockedOutDate` datetime(6) DEFAULT NULL,
  `iUnlockOutAgentID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_iBranchID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblAgentLockedOut_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblAgentLockedOut_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblAgentLockedOut_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblagents`
--

DROP TABLE IF EXISTS `_rtblagents`;
CREATE TABLE `_rtblagents` (
  `idAgents` int(11) NOT NULL,
  `bSysAccount` tinyint(1) NOT NULL DEFAULT '0',
  `cAgentName` varchar(50) DEFAULT NULL,
  `cPassword` varchar(160) DEFAULT NULL,
  `cFirstName` varchar(20) DEFAULT NULL,
  `cInitials` varchar(5) DEFAULT NULL,
  `cLastName` varchar(30) DEFAULT NULL,
  `cTitle` varchar(6) DEFAULT NULL,
  `cDisplayName` varchar(60) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `cTelWork` varchar(20) DEFAULT NULL,
  `cTelFax` varchar(20) DEFAULT NULL,
  `cTelMobile` varchar(20) DEFAULT NULL,
  `cTelHome` varchar(20) DEFAULT NULL,
  `cEmail` varchar(60) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `cComments` varchar(1024) DEFAULT NULL,
  `cAddressStreet` varchar(512) DEFAULT NULL,
  `cAddressPOBox` varchar(30) DEFAULT NULL,
  `cAddressCity` varchar(30) DEFAULT NULL,
  `cAddressState` varchar(30) DEFAULT NULL,
  `cAddressZip` varchar(15) DEFAULT NULL,
  `cAddressCountry` varchar(30) DEFAULT NULL,
  `bCanAssign` tinyint(1) NOT NULL DEFAULT '1',
  `bPwdCanChange` tinyint(1) NOT NULL DEFAULT '1',
  `bPwdMustChange` tinyint(1) NOT NULL DEFAULT '1',
  `bPwdChangeEvery` tinyint(1) NOT NULL DEFAULT '0',
  `iPwdChangeDays` int(11) DEFAULT NULL,
  `dPwdLastChange` datetime DEFAULT NULL,
  `cPwdRemind` varchar(160) DEFAULT NULL,
  `bAgentOutOffice` tinyint(1) NOT NULL DEFAULT '0',
  `bExitWarning` tinyint(1) NOT NULL DEFAULT '1',
  `bCanSetOutOfOffice` tinyint(1) NOT NULL DEFAULT '1',
  `bKnowledgeBaseWarning` tinyint(1) NOT NULL DEFAULT '1',
  `bNewIncidentNotification` tinyint(1) NOT NULL DEFAULT '0',
  `bUseDefaultTree` tinyint(1) NOT NULL DEFAULT '1',
  `bAutoSpellCheck` tinyint(1) NOT NULL DEFAULT '0',
  `iDefIncidentTypeGroupID` int(11) DEFAULT NULL,
  `iDefTillId` int(11) DEFAULT NULL,
  `iDefCashAccount` int(11) DEFAULT NULL,
  `iDefWhseId` int(11) DEFAULT NULL,
  `iNotifyEscalateMinutes` int(11) DEFAULT NULL,
  `iNotifyDueMinutes` int(11) DEFAULT NULL,
  `bForceThisWarehouse` tinyint(1) NOT NULL DEFAULT '0',
  `bAgentActive` tinyint(1) NOT NULL DEFAULT '1',
  `bCBAgNoneVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bCBAgAllVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bJRAgNoneVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bJRAgAllVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bCBUseGrpDefaults` tinyint(1) NOT NULL DEFAULT '0',
  `bJRUseGrpDefaults` tinyint(1) NOT NULL DEFAULT '0',
  `bCBAgOwnVisible` tinyint(1) NOT NULL DEFAULT '1',
  `bJRAgOwnVisible` tinyint(1) NOT NULL DEFAULT '1',
  `iPOAuthType` int(11) NOT NULL DEFAULT '0',
  `iPOIncidentTypeID` int(11) DEFAULT NULL,
  `bPOExclusive` tinyint(1) NOT NULL DEFAULT '1',
  `fPOLimit` double DEFAULT NULL,
  `bPOUseGrpDefaults` tinyint(1) NOT NULL DEFAULT '1',
  `cAccessPurchaseWhIDLst` varchar(1024) DEFAULT NULL,
  `cAccessSalesWhIDLst` varchar(1024) DEFAULT NULL,
  `cAccessOtherTxWhIDList` varchar(1024) DEFAULT NULL,
  `cAccessPurchaseWhChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessSalesWhChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessOtherTxWhChkLstInd` char(1) NOT NULL DEFAULT '2',
  `iDefProjectID` int(11) DEFAULT NULL,
  `cAccessProjectIDLst` varchar(1024) DEFAULT NULL,
  `cAccessProjectChkLstInd` char(1) DEFAULT NULL,
  `iDefRepID` int(11) DEFAULT NULL,
  `cAccessRepIDLst` varchar(1024) DEFAULT NULL,
  `cAccessRepChkLstInd` char(1) DEFAULT NULL,
  `Max_LDisc` double NOT NULL DEFAULT '100',
  `Max_Disc` double NOT NULL DEFAULT '100',
  `cOperatorCode` varchar(50) DEFAULT NULL,
  `cOperatorPassword` varchar(50) DEFAULT NULL,
  `cOperatorNewPassword` varchar(50) DEFAULT NULL,
  `cAccessBranchIDLst` varchar(1024) DEFAULT NULL,
  `cAccessBranchChkLstInd` char(1) DEFAULT NULL,
  `iDocketInputMode` int(11) NOT NULL DEFAULT '0',
  `cOperatorCodePOS` varchar(50) DEFAULT NULL,
  `cOperatorPasswordPOS` varchar(50) DEFAULT NULL,
  `cOperatorNewPasswordPOS` varchar(50) DEFAULT NULL,
  `bCanChangeSessionDate` tinyint(1) NOT NULL DEFAULT '0',
  `cEFTOperatorCode` varchar(6) DEFAULT NULL,
  `bSupervisorAgent` tinyint(1) DEFAULT '0',
  `bQuoteAir` tinyint(1) NOT NULL DEFAULT '0',
  `bLockedOut` tinyint(1) NOT NULL DEFAULT '0',
  `cAccessARGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessARGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `bIncludeARNoGroups` tinyint(1) NOT NULL DEFAULT '1',
  `bApplyARGroupsToEnqRep` tinyint(1) DEFAULT NULL,
  `cAccessAPGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessAPGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `bIncludeAPNoGroups` tinyint(1) NOT NULL DEFAULT '1',
  `bApplyAPGroupsToEnqRep` tinyint(1) DEFAULT NULL,
  `vbBiometric` longtext,
  `fDefMax_LDisc` double DEFAULT NULL,
  `fDefMax_Disc` double DEFAULT NULL,
  `bApplyAccessRepsToReports` tinyint(1) DEFAULT NULL,
  `bApplyAccessProjectsToReports` tinyint(1) DEFAULT NULL,
  `idPOSMenuSetup` int(11) DEFAULT NULL,
  `bAgentIsBuyer` tinyint(1) NOT NULL DEFAULT '0',
  `bUseBiometric` tinyint(1) NOT NULL DEFAULT '0',
  `FiscalPrinterId` int(11) NOT NULL DEFAULT '0',
  `FiscalDeviceId` int(11) NOT NULL DEFAULT '0',
  `_rtblAgents_iBranchID` int(11) DEFAULT NULL,
  `_rtblAgents_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblAgents_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblAgents_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblAgents_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblAgents_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblAgents_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblAgents_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblAgents_Checksum` binary(20) DEFAULT NULL,
  `cAccessDocCatGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessDocCatGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `iDefDocCatID` int(11) DEFAULT NULL,
  `cAccessDocCatIDLst` varchar(1024) DEFAULT NULL,
  `cAccessDocCatChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessIncidentTypeGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessIncidentTypeGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessIncidentTypeIDLst` varchar(1024) DEFAULT NULL,
  `cAccessIncidentTypeChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cSagePayUserName` varchar(50) DEFAULT NULL,
  `cSagePayPassword` varchar(160) DEFAULT NULL,
  `cSagePayPIN` varchar(30) DEFAULT NULL,
  `iAgentLoginScreen` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblbusclass`
--

DROP TABLE IF EXISTS `_rtblbusclass`;
CREATE TABLE `_rtblbusclass` (
  `idBusClass` int(11) NOT NULL,
  `cBusClass` varchar(50) DEFAULT NULL,
  `_rtblBusClass_iBranchID` int(11) DEFAULT NULL,
  `_rtblBusClass_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblBusClass_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblBusClass_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblBusClass_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblBusClass_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblBusClass_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblBusClass_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblBusClass_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblbusdept`
--

DROP TABLE IF EXISTS `_rtblbusdept`;
CREATE TABLE `_rtblbusdept` (
  `idBusDept` int(11) NOT NULL,
  `cBusDept` varchar(50) DEFAULT NULL,
  `_rtblBusDept_iBranchID` int(11) DEFAULT NULL,
  `_rtblBusDept_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblBusDept_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblBusDept_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblBusDept_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblBusDept_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblBusDept_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblBusDept_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblBusDept_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblbusdesig`
--

DROP TABLE IF EXISTS `_rtblbusdesig`;
CREATE TABLE `_rtblbusdesig` (
  `idBusDesig` int(11) NOT NULL,
  `cBusDesig` varchar(50) DEFAULT NULL,
  `_rtblBusDesig_iBranchID` int(11) DEFAULT NULL,
  `_rtblBusDesig_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblBusDesig_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblBusDesig_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblBusDesig_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblBusDesig_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblBusDesig_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblBusDesig_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblBusDesig_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblbustype`
--

DROP TABLE IF EXISTS `_rtblbustype`;
CREATE TABLE `_rtblbustype` (
  `idBusType` int(11) NOT NULL,
  `cBusType` varchar(50) DEFAULT NULL,
  `_rtblBusType_iBranchID` int(11) DEFAULT NULL,
  `_rtblBusType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblBusType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblBusType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblBusType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblBusType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblBusType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblBusType_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblBusType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblclass`
--

DROP TABLE IF EXISTS `_rtblclass`;
CREATE TABLE `_rtblclass` (
  `idClass` int(11) NOT NULL,
  `cDescription` varchar(32) DEFAULT NULL,
  `bAvailable` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblClass_iBranchID` int(11) DEFAULT NULL,
  `_rtblClass_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblClass_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblClass_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblClass_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblClass_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblClass_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblClass_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblClass_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcmdefaults`
--

DROP TABLE IF EXISTS `_rtblcmdefaults`;
CREATE TABLE `_rtblcmdefaults` (
  `idCMDefaults` int(11) NOT NULL,
  `iPeopleFilterStLength` int(11) DEFAULT NULL,
  `bAutoContracts` tinyint(1) NOT NULL DEFAULT '0',
  `vContractPrefix` varchar(20) DEFAULT NULL,
  `iContractPadLength` int(11) DEFAULT NULL,
  `iContractFilterStLength` int(11) DEFAULT NULL,
  `vKBPrefix` varchar(20) DEFAULT NULL,
  `iKBPadLength` int(11) DEFAULT NULL,
  `vIncidentPrefix` varchar(20) DEFAULT NULL,
  `iIncidentPadLength` int(11) DEFAULT NULL,
  `bIncDefAgentName` tinyint(1) NOT NULL DEFAULT '0',
  `bIncDispCustNotes` tinyint(1) NOT NULL DEFAULT '0',
  `vDocStorePath` varchar(256) DEFAULT NULL,
  `bRestrictAgents` tinyint(1) DEFAULT '1',
  `bRestrictInActiveAgents` tinyint(1) DEFAULT '0',
  `bColorCodeOverDueDate` tinyint(1) DEFAULT '0',
  `iOverDueColor` int(11) DEFAULT NULL,
  `bSpellCheck` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoIncidents` tinyint(1) NOT NULL DEFAULT '0',
  `bAutoKB` tinyint(1) NOT NULL DEFAULT '0',
  `iDueDateIncrement` int(11) DEFAULT NULL,
  `iDueTimeIncrement` int(11) DEFAULT NULL,
  `bUseExpContracts` tinyint(1) NOT NULL DEFAULT '0',
  `bUseBlockedContracts` tinyint(1) NOT NULL DEFAULT '0',
  `bPostIncidentOnHoldCust` tinyint(1) NOT NULL DEFAULT '1',
  `bSFAOpportunityAutoNum` tinyint(1) NOT NULL DEFAULT '1',
  `cSFAOpportunityNextNum` varchar(15) DEFAULT NULL,
  `iSFAOpportunityPadTo` int(11) DEFAULT NULL,
  `cSFAOpportunityPrefix` varchar(20) DEFAULT NULL,
  `iDeftOpportunityIncidentTypeID` int(11) DEFAULT NULL,
  `bSFAApplyAgentFilter` tinyint(1) NOT NULL DEFAULT '0',
  `bSFAUseSalesOrders` tinyint(1) NOT NULL DEFAULT '0',
  `iSFAReopenOppStatusID` int(11) NOT NULL DEFAULT '0',
  `iSFAFilterStLength` int(11) NOT NULL DEFAULT '0',
  `bAutoEmailAssignedAgent` tinyint(1) NOT NULL DEFAULT '0',
  `iDocumentFilterStLength` int(11) NOT NULL DEFAULT '0',
  `bSourceDocLinked` tinyint(1) NOT NULL DEFAULT '0',
  `iSourceDocLinkDisplay` int(11) NOT NULL DEFAULT '0',
  `_rtblCMDefaults_iBranchID` int(11) DEFAULT NULL,
  `_rtblCMDefaults_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblCMDefaults_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblCMDefaults_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblCMDefaults_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblCMDefaults_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblCMDefaults_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblCMDefaults_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblCMDefaults_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcompetitor`
--

DROP TABLE IF EXISTS `_rtblcompetitor`;
CREATE TABLE `_rtblcompetitor` (
  `idCompetitor` int(11) NOT NULL,
  `cCompanyName` varchar(50) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `_rtblCompetitor_iBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitor_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblCompetitor_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblCompetitor_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitor_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitor_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblCompetitor_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblCompetitor_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblCompetitor_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcompetitorproduct`
--

DROP TABLE IF EXISTS `_rtblcompetitorproduct`;
CREATE TABLE `_rtblcompetitorproduct` (
  `IDCompetitorProduct` int(11) NOT NULL,
  `cProductName` varchar(50) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `cDetailedDescription` varchar(1024) DEFAULT NULL,
  `_rtblCompetitorProduct_iBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitorProduct_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblCompetitorProduct_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblCompetitorProduct_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitorProduct_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitorProduct_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblCompetitorProduct_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblCompetitorProduct_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblCompetitorProduct_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcompetitorproductlink`
--

DROP TABLE IF EXISTS `_rtblcompetitorproductlink`;
CREATE TABLE `_rtblcompetitorproductlink` (
  `idCompetitorProductLink` int(11) NOT NULL,
  `iCompetitorID` int(11) NOT NULL DEFAULT '0',
  `iCompetitorProductID` int(11) NOT NULL DEFAULT '0',
  `fProductPrice` double DEFAULT NULL,
  `dDtPriceUpdated` datetime(6) DEFAULT NULL,
  `_rtblCompetitorProductLink_iBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitorProductLink_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblCompetitorProductLink_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblCompetitorProductLink_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitorProductLink_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblCompetitorProductLink_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblCompetitorProductLink_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblCompetitorProductLink_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblCompetitorProductLink_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcontractdoclinks`
--

DROP TABLE IF EXISTS `_rtblcontractdoclinks`;
CREATE TABLE `_rtblcontractdoclinks` (
  `idDocLinks` int(11) NOT NULL,
  `iDocStoreID` int(11) DEFAULT NULL,
  `iLinkSource` int(11) DEFAULT '0',
  `iLinkID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblContractDocLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblContractDocLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblContractDocLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcontracts`
--

DROP TABLE IF EXISTS `_rtblcontracts`;
CREATE TABLE `_rtblcontracts` (
  `idContracts` int(11) NOT NULL,
  `cContractNumber` varchar(50) DEFAULT NULL,
  `iDebtorID` int(11) NOT NULL DEFAULT '0',
  `cContractName` varchar(50) NOT NULL,
  `cContractReference` varchar(50) DEFAULT NULL,
  `dCreated` datetime(6) DEFAULT NULL,
  `dStartDate` datetime NOT NULL,
  `dEndDate` datetime NOT NULL,
  `iBillType` int(11) NOT NULL,
  `iTimeUnit` int(11) NOT NULL DEFAULT '0',
  `fAmount` double NOT NULL DEFAULT '0',
  `bBlock` tinyint(1) NOT NULL DEFAULT '0',
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `bAllowOverride` tinyint(1) NOT NULL DEFAULT '1',
  `cInvoice` varchar(30) DEFAULT NULL,
  `iUnitsUsed` int(11) NOT NULL DEFAULT '0',
  `iRecurrTransID` int(11) DEFAULT NULL,
  `bExtendEndDate` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblContracts_iBranchID` int(11) DEFAULT NULL,
  `_rtblContracts_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblContracts_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblContracts_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblContracts_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblContracts_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblContracts_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblContracts_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblContracts_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcontracttemplates`
--

DROP TABLE IF EXISTS `_rtblcontracttemplates`;
CREATE TABLE `_rtblcontracttemplates` (
  `idContractTemplates` int(11) NOT NULL,
  `cTemplateName` varchar(50) NOT NULL,
  `iDefYears` int(11) NOT NULL DEFAULT '0',
  `iDefMonths` int(11) NOT NULL DEFAULT '0',
  `iDefDays` int(11) NOT NULL DEFAULT '0',
  `iBillType` int(11) NOT NULL DEFAULT '0',
  `iTimeUnit` int(11) NOT NULL DEFAULT '0',
  `fAmount` double NOT NULL DEFAULT '0',
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `bAllowOverride` tinyint(1) NOT NULL DEFAULT '1',
  `iCInvTemplateID` int(11) DEFAULT NULL,
  `iRRConfigurationID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_iBranchID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblContractTemplates_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblContractTemplates_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblContractTemplates_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcontracttx`
--

DROP TABLE IF EXISTS `_rtblcontracttx`;
CREATE TABLE `_rtblcontracttx` (
  `idContractTx` int(11) NOT NULL,
  `dDate` datetime NOT NULL,
  `iContractID` int(11) NOT NULL,
  `iIncidentID` int(11) NOT NULL,
  `fAmount` double NOT NULL DEFAULT '0',
  `iDurationMins` int(11) NOT NULL DEFAULT '0',
  `dTimeStamp` datetime(6) DEFAULT NULL,
  `cTxInvoice` varchar(30) DEFAULT NULL,
  `_rtblContractTx_iBranchID` int(11) DEFAULT NULL,
  `_rtblContractTx_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblContractTx_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblContractTx_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblContractTx_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblContractTx_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblContractTx_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblContractTx_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblContractTx_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblcountry`
--

DROP TABLE IF EXISTS `_rtblcountry`;
CREATE TABLE `_rtblcountry` (
  `idCountry` int(11) NOT NULL,
  `cCountryName` varchar(30) NOT NULL,
  `_rtblCountry_iBranchID` int(11) DEFAULT NULL,
  `_rtblCountry_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblCountry_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblCountry_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblCountry_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblCountry_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblCountry_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblCountry_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblCountry_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtbldoccat`
--

DROP TABLE IF EXISTS `_rtbldoccat`;
CREATE TABLE `_rtbldoccat` (
  `idDocCat` int(11) NOT NULL,
  `cDescription` varchar(64) NOT NULL,
  `_rtblDocCat_iBranchID` int(11) DEFAULT NULL,
  `_rtblDocCat_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblDocCat_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblDocCat_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblDocCat_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblDocCat_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblDocCat_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblDocCat_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblDocCat_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtbldoclinks`
--

DROP TABLE IF EXISTS `_rtbldoclinks`;
CREATE TABLE `_rtbldoclinks` (
  `idDocLinks` int(11) NOT NULL,
  `iDocStoreID` int(11) NOT NULL,
  `iLinkSource` int(11) NOT NULL DEFAULT '0',
  `iLinkID` int(11) NOT NULL,
  `_rtblDocLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblDocLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblDocLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblDocLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblDocLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblDocLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblDocLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblDocLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblDocLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtbldocstore`
--

DROP TABLE IF EXISTS `_rtbldocstore`;
CREATE TABLE `_rtbldocstore` (
  `idDocStore` int(11) NOT NULL,
  `cDocStoreName` varchar(20) NOT NULL,
  `cDocName` varchar(255) NOT NULL,
  `iDocCatID` int(11) DEFAULT NULL,
  `cDocDescription` varchar(255) NOT NULL,
  `dModified` datetime NOT NULL,
  `iAgentID` int(11) NOT NULL,
  `nIcon` longblob,
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `_rtblDocStore_iBranchID` int(11) DEFAULT NULL,
  `_rtblDocStore_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblDocStore_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblDocStore_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblDocStore_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblDocStore_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblDocStore_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblDocStore_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblDocStore_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblescalategrp`
--

DROP TABLE IF EXISTS `_rtblescalategrp`;
CREATE TABLE `_rtblescalategrp` (
  `idEscalateGrp` int(11) NOT NULL,
  `cDescription` varchar(30) NOT NULL,
  `_rtblEscalateGrp_iBranchID` int(11) DEFAULT NULL,
  `_rtblEscalateGrp_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblEscalateGrp_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblEscalateGrp_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblEscalateGrp_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblEscalateGrp_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblEscalateGrp_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblEscalateGrp_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblEscalateGrp_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidentaction`
--

DROP TABLE IF EXISTS `_rtblincidentaction`;
CREATE TABLE `_rtblincidentaction` (
  `idIncidentAction` int(11) NOT NULL,
  `cDescription` varchar(32) DEFAULT NULL,
  `cPDescription` varchar(32) DEFAULT NULL,
  `_rtblIncidentAction_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentAction_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentAction_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentAction_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentAction_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentAction_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentAction_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentAction_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentAction_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidentcat`
--

DROP TABLE IF EXISTS `_rtblincidentcat`;
CREATE TABLE `_rtblincidentcat` (
  `idIncidentCat` int(11) NOT NULL,
  `cDescription` varchar(32) DEFAULT NULL,
  `_rtblIncidentCat_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentCat_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentCat_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentCat_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentCat_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentCat_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentCat_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentCat_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentCat_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidentlog`
--

DROP TABLE IF EXISTS `_rtblincidentlog`;
CREATE TABLE `_rtblincidentlog` (
  `idIncidentLog` int(11) NOT NULL,
  `iIncidentID` int(11) NOT NULL,
  `dActionDate` datetime(6) NOT NULL,
  `iIncidentActionID` int(11) NOT NULL,
  `cResolution` longtext,
  `iAgentID` int(11) DEFAULT NULL,
  `bProxy` tinyint(1) NOT NULL DEFAULT '0',
  `iNewAgentID` int(11) DEFAULT NULL,
  `cSourceContent` longtext,
  `cSourceID` varchar(128) DEFAULT '',
  `iRejectReasonID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentLog_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentLog_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentLog_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidentpriority`
--

DROP TABLE IF EXISTS `_rtblincidentpriority`;
CREATE TABLE `_rtblincidentpriority` (
  `idIncidentPriority` int(11) NOT NULL,
  `cDescription` varchar(32) DEFAULT NULL,
  `iColor` int(11) DEFAULT NULL,
  `bDefault` tinyint(1) DEFAULT '0',
  `_rtblIncidentPriority_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentPriority_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentPriority_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentPriority_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentPriority_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentPriority_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentPriority_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentPriority_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentPriority_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidents`
--

DROP TABLE IF EXISTS `_rtblincidents`;
CREATE TABLE `_rtblincidents` (
  `idIncidents` int(11) NOT NULL,
  `dCreated` datetime(6) NOT NULL,
  `dLastModified` datetime(6) NOT NULL,
  `iClassID` int(11) NOT NULL,
  `iIncidentStatusID` int(11) NOT NULL,
  `bRequireAck` tinyint(1) NOT NULL DEFAULT '0',
  `iDebtorID` int(11) NOT NULL DEFAULT '0',
  `iPersonID` int(11) NOT NULL DEFAULT '0',
  `iIncidentCatID` int(11) DEFAULT '0',
  `cOurRef` varchar(50) DEFAULT NULL,
  `cYourRef` varchar(50) DEFAULT NULL,
  `cOutline` varchar(1024) DEFAULT NULL,
  `iPriorityID` int(11) NOT NULL,
  `iEscalateGrpID` int(11) NOT NULL DEFAULT '0',
  `iAgentGroupID` int(11) NOT NULL DEFAULT '0',
  `iCurrentAgentID` int(11) NOT NULL,
  `iContractTxID` int(11) NOT NULL DEFAULT '0',
  `iStockID` int(11) NOT NULL DEFAULT '0',
  `iPrivNode` int(11) DEFAULT NULL,
  `iIncidentTypeID` int(11) NOT NULL DEFAULT '0',
  `dDueBy` datetime(6) DEFAULT NULL,
  `iDuration` int(11) DEFAULT NULL,
  `iContractID` int(11) DEFAULT NULL,
  `cChangeLog` longtext,
  `iIncidentTypeGroupID` int(11) DEFAULT NULL,
  `iWorkflowID` int(11) DEFAULT NULL,
  `iWorkflowStatusID` int(11) DEFAULT NULL,
  `bHasBeenRejected` tinyint(1) NOT NULL DEFAULT '0',
  `iSupplierID` int(11) DEFAULT NULL,
  `iFixedAssetID` int(11) DEFAULT NULL,
  `iEmployeeID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iJobCostingID` int(11) DEFAULT NULL,
  `iProspectID` int(11) NOT NULL DEFAULT '0',
  `iOpportunityID` int(11) NOT NULL DEFAULT '0',
  `iPOInvoiceID` int(11) NOT NULL DEFAULT '0',
  `bPOViewed` tinyint(1) NOT NULL DEFAULT '0',
  `iRequisitionID` int(11) NOT NULL DEFAULT '0',
  `iLinkID` int(11) DEFAULT NULL,
  `iRfqID` int(11) DEFAULT NULL,
  `iSIMReqID` int(11) DEFAULT NULL,
  `_rtblIncidents_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidents_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidents_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidents_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidents_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidents_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidents_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidents_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidents_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidentstatus`
--

DROP TABLE IF EXISTS `_rtblincidentstatus`;
CREATE TABLE `_rtblincidentstatus` (
  `idIncidentStatus` int(11) NOT NULL,
  `cDescription` varchar(32) DEFAULT NULL,
  `_rtblIncidentStatus_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentStatus_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentStatus_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentStatus_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentStatus_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentStatus_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentStatus_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentStatus_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentStatus_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidents_archive`
--

DROP TABLE IF EXISTS `_rtblincidents_archive`;
CREATE TABLE `_rtblincidents_archive` (
  `idIncidents` int(11) NOT NULL,
  `dCreated` datetime(6) NOT NULL,
  `dLastModified` datetime(6) NOT NULL,
  `iClassID` int(11) NOT NULL,
  `iIncidentStatusID` int(11) NOT NULL,
  `bRequireAck` tinyint(1) NOT NULL,
  `iDebtorID` int(11) NOT NULL,
  `iPersonID` int(11) NOT NULL,
  `iIncidentCatID` int(11) DEFAULT NULL,
  `cOurRef` varchar(50) DEFAULT NULL,
  `cYourRef` varchar(50) DEFAULT NULL,
  `cOutline` varchar(1024) DEFAULT NULL,
  `iPriorityID` int(11) NOT NULL,
  `iEscalateGrpID` int(11) NOT NULL,
  `iAgentGroupID` int(11) NOT NULL,
  `iCurrentAgentID` int(11) NOT NULL,
  `iContractTxID` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iPrivNode` int(11) DEFAULT NULL,
  `iIncidentTypeID` int(11) NOT NULL,
  `dDueBy` datetime(6) DEFAULT NULL,
  `iDuration` int(11) DEFAULT NULL,
  `iContractID` int(11) DEFAULT NULL,
  `cChangeLog` longtext,
  `iIncidentTypeGroupID` int(11) DEFAULT NULL,
  `iWorkflowID` int(11) DEFAULT NULL,
  `iWorkflowStatusID` int(11) DEFAULT NULL,
  `bHasBeenRejected` tinyint(1) NOT NULL,
  `iSupplierID` int(11) DEFAULT NULL,
  `iFixedAssetID` int(11) DEFAULT NULL,
  `iEmployeeID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iJobCostingID` int(11) DEFAULT NULL,
  `iProspectID` int(11) NOT NULL DEFAULT '0',
  `iOpportunityID` int(11) NOT NULL DEFAULT '0',
  `iRequisitionID` int(11) NOT NULL DEFAULT '0',
  `bPOViewed` tinyint(1) DEFAULT NULL,
  `iPOInvoiceID` int(11) NOT NULL DEFAULT '0',
  `iLinkID` int(11) DEFAULT NULL,
  `iRfqID` int(11) DEFAULT NULL,
  `iSIMReqID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidents_Archive_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidents_Archive_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidents_Archive_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidenttemplates`
--

DROP TABLE IF EXISTS `_rtblincidenttemplates`;
CREATE TABLE `_rtblincidenttemplates` (
  `idIncidentTemplates` int(11) NOT NULL,
  `cTemplateName` varchar(32) NOT NULL,
  `iIncidentID` int(11) NOT NULL,
  `iAgentID` int(11) NOT NULL DEFAULT '0',
  `_rtblIncidentTemplates_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentTemplates_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentTemplates_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentTemplates_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentTemplates_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentTemplates_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentTemplates_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentTemplates_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentTemplates_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblincidenttype`
--

DROP TABLE IF EXISTS `_rtblincidenttype`;
CREATE TABLE `_rtblincidenttype` (
  `idIncidentType` int(11) NOT NULL,
  `cDescription` varchar(50) NOT NULL,
  `iEscGroupID` int(11) DEFAULT NULL,
  `bAllowOverride` tinyint(1) NOT NULL DEFAULT '1',
  `bRequireContract` tinyint(1) NOT NULL DEFAULT '0',
  `iIncidentTypeGroupID` int(11) DEFAULT NULL,
  `iWorkflowID` int(11) DEFAULT NULL,
  `bAllowOverrideIncidentType` tinyint(1) NOT NULL DEFAULT '1',
  `bPOIncidentType` tinyint(1) NOT NULL DEFAULT '0',
  `cDefaultOutline` varchar(1024) DEFAULT NULL,
  `bActive` tinyint(1) NOT NULL DEFAULT '1',
  `_rtblIncidentType_iBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentType_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentType_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblIncidentType_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentType_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblIncidentType_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentType_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblIncidentType_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblIncidentType_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblkbadoclinks`
--

DROP TABLE IF EXISTS `_rtblkbadoclinks`;
CREATE TABLE `_rtblkbadoclinks` (
  `idDocLinks` int(11) NOT NULL,
  `iDocStoreID` int(11) DEFAULT NULL,
  `iLinkSource` int(11) DEFAULT '0',
  `iLinkID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblKBADocLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblKBADocLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblKBADocLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblkbcategorylinks`
--

DROP TABLE IF EXISTS `_rtblkbcategorylinks`;
CREATE TABLE `_rtblkbcategorylinks` (
  `idCategoryLinks` int(11) NOT NULL,
  `iKnowledgeBaseID` int(11) NOT NULL DEFAULT '0',
  `iKnowledgeBaseCatValueID` int(11) NOT NULL,
  `bSelected` tinyint(1) NOT NULL,
  `_rtblKBCategoryLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblKBCategoryLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblKBCategoryLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblKBCategoryLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblKBCategoryLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblKBCategoryLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblKBCategoryLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblKBCategoryLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblKBCategoryLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblknowledgebase`
--

DROP TABLE IF EXISTS `_rtblknowledgebase`;
CREATE TABLE `_rtblknowledgebase` (
  `idKnowledgeBase` int(11) NOT NULL,
  `cArticleNumber` varchar(50) DEFAULT '0',
  `cSummary` varchar(1024) DEFAULT NULL,
  `iStockID` int(11) DEFAULT '0',
  `iStatus` int(11) NOT NULL DEFAULT '0',
  `bPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dCreatedDate` datetime NOT NULL,
  `iCreatedAgentID` int(11) NOT NULL DEFAULT '0',
  `dDateEdited` datetime DEFAULT NULL,
  `iEditedAgentID` int(11) NOT NULL DEFAULT '0',
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `_rtblKnowledgeBase_iBranchID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBase_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblKnowledgeBase_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblKnowledgeBase_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBase_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBase_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBase_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBase_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBase_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblknowledgebasecat`
--

DROP TABLE IF EXISTS `_rtblknowledgebasecat`;
CREATE TABLE `_rtblknowledgebasecat` (
  `idKnowledgeBaseCat` int(11) NOT NULL,
  `cName` varchar(30) NOT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `bInUse` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblKnowledgeBaseCat_iBranchID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblKnowledgeBaseCat_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblnotify`
--

DROP TABLE IF EXISTS `_rtblnotify`;
CREATE TABLE `_rtblnotify` (
  `idNotify` int(11) NOT NULL,
  `dNotifyDate` datetime(6) DEFAULT NULL,
  `iForAgentID` int(11) NOT NULL,
  `iIncidentID` int(11) DEFAULT NULL,
  `iIncidentLogID` int(11) DEFAULT NULL,
  `bRead` tinyint(1) NOT NULL DEFAULT '0',
  `iWhseIBTID` int(11) DEFAULT NULL,
  `_rtblNotify_iBranchID` int(11) DEFAULT NULL,
  `_rtblNotify_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblNotify_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblNotify_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblNotify_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblNotify_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblNotify_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblNotify_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblNotify_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblopportunity`
--

DROP TABLE IF EXISTS `_rtblopportunity`;
CREATE TABLE `_rtblopportunity` (
  `IDOpportunity` int(11) NOT NULL,
  `cOpportunityNumber` varchar(50) DEFAULT NULL,
  `iClientID` int(11) NOT NULL DEFAULT '0',
  `iProspectID` int(11) NOT NULL DEFAULT '0',
  `iPeopleID` int(11) NOT NULL DEFAULT '0',
  `iAgentID` int(11) NOT NULL DEFAULT '0',
  `iOpportunityStageID` int(11) NOT NULL DEFAULT '0',
  `iOpportunityStatusID` int(11) NOT NULL DEFAULT '0',
  `iOpportunitySourceID` int(11) NOT NULL DEFAULT '0',
  `iOppSourceClientID` int(11) NOT NULL DEFAULT '0',
  `iOppSourceSupplierID` int(11) NOT NULL DEFAULT '0',
  `iOppSourceAgentID` int(11) NOT NULL DEFAULT '0',
  `dDateStart` datetime(6) DEFAULT NULL,
  `dDateClose` datetime(6) DEFAULT NULL,
  `dDateActualClose` datetime(6) DEFAULT NULL,
  `fClosedAmount` double DEFAULT NULL,
  `fProbabilityPerc` double DEFAULT NULL,
  `bPublic` tinyint(1) NOT NULL DEFAULT '0',
  `iActiveInvNumID` int(11) NOT NULL DEFAULT '0',
  `fForecastAmount` double DEFAULT NULL,
  `fBudgetedAmount` double DEFAULT NULL,
  `cOpportunityDescription` varchar(100) DEFAULT NULL,
  `iProjectID` int(11) NOT NULL DEFAULT '0',
  `_rtblOpportunity_iBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunity_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunity_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunity_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunity_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunity_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunity_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunity_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblOpportunity_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblopportunitycompetitor`
--

DROP TABLE IF EXISTS `_rtblopportunitycompetitor`;
CREATE TABLE `_rtblopportunitycompetitor` (
  `idOpportunityCompetitor` int(11) NOT NULL,
  `iOpportunityID` int(11) NOT NULL,
  `iCompetitorID` int(11) NOT NULL,
  `iCompetitorProductID` int(11) NOT NULL DEFAULT '0',
  `fPrice` double DEFAULT NULL,
  `bWonDeal` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblOpportunityCompetitor_iBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityCompetitor_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityCompetitor_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityCompetitor_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityCompetitor_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityCompetitor_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityCompetitor_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityCompetitor_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblOpportunityCompetitor_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblopportunitydoclinks`
--

DROP TABLE IF EXISTS `_rtblopportunitydoclinks`;
CREATE TABLE `_rtblopportunitydoclinks` (
  `IDDocLinks` int(11) NOT NULL,
  `iDocStoreID` int(11) DEFAULT NULL,
  `iLinkSource` int(11) DEFAULT NULL,
  `iLinkID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityDocLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityDocLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblOpportunityDocLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblopportunitysource`
--

DROP TABLE IF EXISTS `_rtblopportunitysource`;
CREATE TABLE `_rtblopportunitysource` (
  `IDOpportunitySource` int(11) NOT NULL,
  `cSourceDesc` varchar(50) DEFAULT NULL,
  `_rtblOpportunitySource_iBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunitySource_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunitySource_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunitySource_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunitySource_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunitySource_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunitySource_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunitySource_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblOpportunitySource_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblopportunitystage`
--

DROP TABLE IF EXISTS `_rtblopportunitystage`;
CREATE TABLE `_rtblopportunitystage` (
  `IDOpportunityStage` int(11) NOT NULL,
  `cStageName` varchar(50) DEFAULT NULL,
  `cStageDescription` varchar(100) DEFAULT NULL,
  `iOpportunityStatusID` int(11) DEFAULT NULL,
  `iStageSequence` int(11) DEFAULT NULL,
  `fDefProbabilityPerc` double DEFAULT NULL,
  `_rtblOpportunityStage_iBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityStage_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityStage_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityStage_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityStage_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityStage_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityStage_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityStage_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblOpportunityStage_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblopportunitystatus`
--

DROP TABLE IF EXISTS `_rtblopportunitystatus`;
CREATE TABLE `_rtblopportunitystatus` (
  `IDOpportunityStatus` int(11) NOT NULL,
  `cStatusName` varchar(50) DEFAULT NULL,
  `bFinal` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblOpportunityStatus_iBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityStatus_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityStatus_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblOpportunityStatus_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityStatus_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblOpportunityStatus_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityStatus_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblOpportunityStatus_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblOpportunityStatus_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblpeople`
--

DROP TABLE IF EXISTS `_rtblpeople`;
CREATE TABLE `_rtblpeople` (
  `idPeople` int(11) NOT NULL,
  `cFirstName` varchar(20) DEFAULT NULL,
  `cInitials` varchar(5) DEFAULT NULL,
  `cLastName` varchar(30) DEFAULT NULL,
  `cDisplayName` varchar(60) DEFAULT NULL,
  `cTitle` varchar(6) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `cTelWork` varchar(20) DEFAULT NULL,
  `cTelFax` varchar(20) DEFAULT NULL,
  `cTelMobile` varchar(20) DEFAULT NULL,
  `cTelHome` varchar(20) DEFAULT NULL,
  `cEmail` varchar(60) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `cComments` varchar(1024) DEFAULT NULL,
  `cAddress` varchar(240) DEFAULT NULL,
  `cPostalAddress` varchar(240) DEFAULT NULL,
  `iBusDeptID` int(11) DEFAULT NULL,
  `iBusDesigID` int(11) DEFAULT NULL,
  `dBirthDate` datetime DEFAULT NULL,
  `dPeopleTimeStamp` datetime(6) DEFAULT NULL,
  `_rtblPeople_iBranchID` int(11) DEFAULT NULL,
  `_rtblPeople_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblPeople_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblPeople_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblPeople_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblPeople_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblPeople_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblPeople_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblPeople_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblpeoplelinks`
--

DROP TABLE IF EXISTS `_rtblpeoplelinks`;
CREATE TABLE `_rtblpeoplelinks` (
  `idPeopleLinks` int(11) NOT NULL,
  `iPeopleID` int(11) DEFAULT NULL,
  `iDebtorID` int(11) DEFAULT NULL,
  `cModule` varchar(2) DEFAULT NULL,
  `_rtblPeopleLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblPeopleLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblPeopleLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblPeopleLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblPeopleLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblPeopleLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblPeopleLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblPeopleLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblPeopleLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblprospect`
--

DROP TABLE IF EXISTS `_rtblprospect`;
CREATE TABLE `_rtblprospect` (
  `IDProspect` int(11) NOT NULL,
  `cCompanyName` varchar(50) DEFAULT NULL,
  `cTelephone` varchar(25) DEFAULT NULL,
  `cFax` varchar(25) DEFAULT NULL,
  `cPhysicalAddress1` varchar(40) DEFAULT NULL,
  `cPhysicalAddress2` varchar(40) DEFAULT NULL,
  `cPhysicalAddress3` varchar(40) DEFAULT NULL,
  `cPhysicalAddress4` varchar(40) DEFAULT NULL,
  `cPhysicalAddress5` varchar(40) DEFAULT NULL,
  `cPhysicalAddressPC` varchar(15) DEFAULT NULL,
  `cPostalAddress1` varchar(40) DEFAULT NULL,
  `cPostalAddress2` varchar(40) DEFAULT NULL,
  `cPostalAddress3` varchar(40) DEFAULT NULL,
  `cPostalAddress4` varchar(40) DEFAULT NULL,
  `cPostalAddress5` varchar(40) DEFAULT NULL,
  `cPostalAddressPC` varchar(15) DEFAULT NULL,
  `cWebsite` varchar(60) DEFAULT NULL,
  `cEmail` varchar(60) DEFAULT NULL,
  `bChargeTax` tinyint(1) NOT NULL DEFAULT '1',
  `iAgentID` int(11) NOT NULL DEFAULT '0',
  `bPublic` tinyint(1) NOT NULL DEFAULT '0',
  `iRepID` int(11) NOT NULL DEFAULT '0',
  `_rtblProspect_iBranchID` int(11) DEFAULT NULL,
  `_rtblProspect_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblProspect_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblProspect_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblProspect_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblProspect_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblProspect_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblProspect_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblProspect_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblrefbase`
--

DROP TABLE IF EXISTS `_rtblrefbase`;
CREATE TABLE `_rtblrefbase` (
  `idRefBase` int(11) NOT NULL,
  `cRefType` varchar(30) DEFAULT NULL,
  `iNextNo` int(11) DEFAULT '100000',
  `_rtblRefBase_iBranchID` int(11) DEFAULT NULL,
  `_rtblRefBase_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblRefBase_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblRefBase_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblRefBase_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblRefBase_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblRefBase_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblRefBase_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblRefBase_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblrefbook`
--

DROP TABLE IF EXISTS `_rtblrefbook`;
CREATE TABLE `_rtblrefbook` (
  `idRefBook` int(11) NOT NULL,
  `iRefBaseID` int(11) DEFAULT NULL,
  `iBookedNo` int(11) DEFAULT NULL,
  `bAvailable` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblRefBook_iBranchID` int(11) DEFAULT NULL,
  `_rtblRefBook_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblRefBook_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblRefBook_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblRefBook_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblRefBook_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblRefBook_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblRefBook_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblRefBook_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblstocklinks`
--

DROP TABLE IF EXISTS `_rtblstocklinks`;
CREATE TABLE `_rtblstocklinks` (
  `idStockLinks` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iDCLink` int(11) NOT NULL,
  `bItemActive` tinyint(1) NOT NULL DEFAULT '1',
  `cProductReference` varchar(30) DEFAULT NULL,
  `cModule` varchar(2) NOT NULL,
  `cSupInvCode` varchar(20) DEFAULT NULL,
  `iWhseID` int(11) DEFAULT NULL,
  `bDefaultSupplier` tinyint(1) NOT NULL DEFAULT '0',
  `bDCOnHold` tinyint(1) NOT NULL DEFAULT '0',
  `dTimeStamp` datetime(6) DEFAULT NULL,
  `fLastGRVCost` double NOT NULL DEFAULT '0',
  `dLastGRVCostDate` datetime(6) DEFAULT NULL,
  `fManualCost` double NOT NULL DEFAULT '0',
  `_rtblStockLinks_fLeadDays` double DEFAULT NULL,
  `fMinOrderQuantity` double DEFAULT NULL,
  `_rtblStockLinks_iBranchID` int(11) DEFAULT NULL,
  `_rtblStockLinks_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblStockLinks_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblStockLinks_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblStockLinks_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblStockLinks_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblStockLinks_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblStockLinks_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblStockLinks_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtbluserdict`
--

DROP TABLE IF EXISTS `_rtbluserdict`;
CREATE TABLE `_rtbluserdict` (
  `idUserDict` int(11) NOT NULL,
  `cFieldName` varchar(50) NOT NULL,
  `cFieldDescription` varchar(50) NOT NULL,
  `iFieldType` int(11) NOT NULL,
  `iFieldSize` int(11) DEFAULT NULL,
  `iFieldIndex` int(11) NOT NULL,
  `cTableName` varchar(50) NOT NULL,
  `cLookupOptions` longtext,
  `bForceValue` tinyint(1) NOT NULL DEFAULT '0',
  `cDefaultValue` varchar(250) DEFAULT NULL,
  `iPageIndex` int(11) DEFAULT NULL,
  `cPageName` varchar(50) DEFAULT NULL,
  `iFieldDecimals` int(11) DEFAULT NULL,
  `iModuleOptions` int(11) DEFAULT NULL,
  `_rtblUserDict_iBranchID` int(11) DEFAULT NULL,
  `_rtblUserDict_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblUserDict_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblUserDict_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblUserDict_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblUserDict_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblUserDict_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblUserDict_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblUserDict_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblworkcal`
--

DROP TABLE IF EXISTS `_rtblworkcal`;
CREATE TABLE `_rtblworkcal` (
  `idWorkCal` int(11) NOT NULL,
  `iStartTime` int(11) NOT NULL,
  `iEndTime` int(11) NOT NULL,
  `cDescription` varchar(20) DEFAULT NULL,
  `bSunday` tinyint(1) NOT NULL DEFAULT '0',
  `bMonday` tinyint(1) NOT NULL DEFAULT '0',
  `bTuesday` tinyint(1) NOT NULL DEFAULT '0',
  `bWednesday` tinyint(1) NOT NULL DEFAULT '0',
  `bThursday` tinyint(1) NOT NULL DEFAULT '0',
  `bFriday` tinyint(1) NOT NULL DEFAULT '0',
  `bSaturday` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblWorkCal_iBranchID` int(11) DEFAULT NULL,
  `_rtblWorkCal_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblWorkCal_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblWorkCal_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblWorkCal_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblWorkCal_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblWorkCal_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblWorkCal_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblWorkCal_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_rtblworkcalexdates`
--

DROP TABLE IF EXISTS `_rtblworkcalexdates`;
CREATE TABLE `_rtblworkcalexdates` (
  `idWorkCalExDates` int(11) NOT NULL,
  `dExDate` datetime NOT NULL,
  `bRepeat` tinyint(1) NOT NULL DEFAULT '0',
  `_rtblWorkCalExDates_iBranchID` int(11) DEFAULT NULL,
  `_rtblWorkCalExDates_dCreatedDate` datetime(6) DEFAULT NULL,
  `_rtblWorkCalExDates_dModifiedDate` datetime(6) DEFAULT NULL,
  `_rtblWorkCalExDates_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblWorkCalExDates_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblWorkCalExDates_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblWorkCalExDates_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblWorkCalExDates_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblWorkCalExDates_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtbldefaults`
--

DROP TABLE IF EXISTS `_simtbldefaults`;
CREATE TABLE `_simtbldefaults` (
  `bGeneralLedger` tinyint(1) DEFAULT NULL,
  `bJobCards` tinyint(1) DEFAULT NULL,
  `bProject` tinyint(1) DEFAULT NULL,
  `cStockIssue_Trans` varchar(50) DEFAULT NULL,
  `cStockCredit_Trans` varchar(50) DEFAULT NULL,
  `cIncidentType` varchar(50) DEFAULT NULL,
  `bAllowStock_requistion` tinyint(1) DEFAULT NULL,
  `bStockAuto_Numbering` tinyint(1) DEFAULT NULL,
  `cStockPrefix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iStock_PadtoNumber` int(11) DEFAULT NULL,
  `iStockNextNumber` int(11) DEFAULT NULL,
  `bStockUniqueNumber` tinyint(1) DEFAULT NULL,
  `bIssueAuto_Numbering` tinyint(1) DEFAULT NULL,
  `cIssuePrefix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iIssue_PadtoNumber` int(11) DEFAULT NULL,
  `iIssueNextNumber` int(11) DEFAULT NULL,
  `bIssueUniqueNumber` tinyint(1) DEFAULT NULL,
  `bTemplateAutoNumbering` tinyint(1) DEFAULT NULL,
  `cTemplatePrefix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iTemplate_PadtoNumber` int(11) DEFAULT NULL,
  `iTemplateNextNumber` int(11) DEFAULT NULL,
  `bTemplateUniqueNumber` tinyint(1) DEFAULT NULL,
  `iTrcode` int(11) DEFAULT NULL,
  `bUseWorkFlow` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtblreportlayout`
--

DROP TABLE IF EXISTS `_simtblreportlayout`;
CREATE TABLE `_simtblreportlayout` (
  `idReportLayout` int(11) NOT NULL,
  `cRptDescription` varchar(80) NOT NULL,
  `iModuleId` int(11) DEFAULT NULL,
  `iVersion` int(11) DEFAULT NULL,
  `nLayout` longblob NOT NULL,
  `bReadOnly` tinyint(1) NOT NULL,
  `bIsDefaultLayout` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtblreqheader`
--

DROP TABLE IF EXISTS `_simtblreqheader`;
CREATE TABLE `_simtblreqheader` (
  `idReqHeader` int(11) NOT NULL,
  `cRequisitionNo` varchar(50) DEFAULT NULL,
  `dRequisitionDate` datetime(6) DEFAULT NULL,
  `iProjectDefaultID` int(11) DEFAULT NULL,
  `cRequestedBy` varchar(50) DEFAULT NULL,
  `iIncidentTypeDefaultID` int(11) DEFAULT NULL,
  `iStatus` int(11) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtblreqlines`
--

DROP TABLE IF EXISTS `_simtblreqlines`;
CREATE TABLE `_simtblreqlines` (
  `idReqLines` int(11) NOT NULL,
  `fk_idReqHeader` int(11) DEFAULT NULL,
  `iStockId` int(11) DEFAULT NULL,
  `cDescription` longtext CHARACTER SET utf8mb4,
  `iReqStatus` int(11) DEFAULT NULL,
  `dSartDate` datetime(6) DEFAULT NULL,
  `dEndDate` datetime(6) DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `iUnitOfMeasure` int(11) DEFAULT NULL,
  `iWarehouseId` int(11) DEFAULT NULL,
  `fLineTotalCost` double DEFAULT NULL,
  `bIsprocessed` tinyint(1) DEFAULT NULL,
  `fConfirmQty` double DEFAULT NULL,
  `bIswarehouse` tinyint(1) DEFAULT NULL,
  `cType` char(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cTypeDetails` int(11) DEFAULT NULL,
  `iTrcodeId` int(11) DEFAULT NULL,
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `iLineNo` int(11) DEFAULT NULL,
  `iTaxType` int(11) DEFAULT NULL,
  `fk_iIncidentId` int(11) DEFAULT NULL,
  `iStockingUnitID` int(11) DEFAULT '0',
  `iUnitCategoryID` int(11) DEFAULT '0',
  `iStockingUnitCategoryID` int(11) DEFAULT '0',
  `bIsLot` tinyint(1) DEFAULT '0',
  `bIsSerialItem` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtblstkissuelines`
--

DROP TABLE IF EXISTS `_simtblstkissuelines`;
CREATE TABLE `_simtblstkissuelines` (
  `iAutoIdx` int(11) NOT NULL,
  `iStkIssueId` int(11) DEFAULT NULL,
  `iStkIssueTaxTpId` int(11) DEFAULT NULL,
  `iStockId` int(11) DEFAULT NULL,
  `cDescription` longtext CHARACTER SET utf8mb4,
  `iStatus` int(11) DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `iUnitOfMeasure` int(11) DEFAULT NULL,
  `iWarehouseId` int(11) DEFAULT NULL,
  `fLineTotalCost` double DEFAULT NULL,
  `bIsprocessed` tinyint(1) DEFAULT NULL,
  `fConfirmQty` double DEFAULT NULL,
  `bIswarehouse` tinyint(1) DEFAULT NULL,
  `cType` char(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cTypeDetails` char(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cProject` char(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iTrcodeId` int(11) DEFAULT NULL,
  `bIsLot` tinyint(1) DEFAULT NULL,
  `iLotNumber` int(11) DEFAULT NULL,
  `iProjectId` int(11) DEFAULT NULL,
  `iStockingUnitID` int(11) DEFAULT '0',
  `iUnitCategoryID` int(11) DEFAULT '0',
  `iStockingUnitCategoryID` int(11) DEFAULT '0',
  `bIsSerialItem` tinyint(1) DEFAULT '0',
  `dLotExpiryDate` datetime(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtblstockissuelinesn`
--

DROP TABLE IF EXISTS `_simtblstockissuelinesn`;
CREATE TABLE `_simtblstockissuelinesn` (
  `idStockIssueLineSN` int(11) NOT NULL,
  `iSerialStockIssueID` int(11) NOT NULL,
  `iSerialStockIssueLineID` bigint(20) NOT NULL,
  `cSerialNumber` varchar(50) DEFAULT NULL,
  `iSerialMFID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_simtblstockissuemaster`
--

DROP TABLE IF EXISTS `_simtblstockissuemaster`;
CREATE TABLE `_simtblstockissuemaster` (
  `iStkIssueId` int(11) NOT NULL,
  `cStkIssueNumber` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cDescripton` longtext CHARACTER SET utf8mb4,
  `cType` char(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iStatus` int(11) DEFAULT NULL,
  `dIssueDate` datetime(6) DEFAULT NULL,
  `iProjectId` int(11) DEFAULT NULL,
  `bIsTemplate` tinyint(1) DEFAULT NULL,
  `cTemplateId` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cTemplateDescription` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `iRequisitionId` int(11) NOT NULL DEFAULT '0',
  `cRequestedBy` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_wtblipaddetails`
--

DROP TABLE IF EXISTS `_wtblipaddetails`;
CREATE TABLE `_wtblipaddetails` (
  `idIPadNumber` bigint(20) NOT NULL,
  `cCode` varchar(18) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cDescription` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cUDID` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cAutoNumber` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `dtTimeStamp` datetime(6) DEFAULT NULL,
  `dtCreatedDt` datetime(6) DEFAULT NULL,
  `dtModifiedDt` datetime(6) DEFAULT NULL,
  `iCreatedAgentId` int(11) DEFAULT NULL,
  `iModifiedAgentId` int(11) DEFAULT NULL,
  `iDeviceType` int(11) NOT NULL DEFAULT '0',
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_wtblipaduser`
--

DROP TABLE IF EXISTS `_wtblipaduser`;
CREATE TABLE `_wtblipaduser` (
  `idIPadUser` bigint(20) NOT NULL,
  `iAgentId` bigint(20) NOT NULL,
  `bIsUserRoleMgmt` tinyint(1) NOT NULL,
  `bIsActive` tinyint(1) NOT NULL,
  `dtCreatedDt` datetime(6) DEFAULT NULL,
  `dtModifiedDt` datetime(6) DEFAULT NULL,
  `iCreatedAgentId` int(11) DEFAULT NULL,
  `iModifiedAgentId` int(11) DEFAULT NULL,
  `iModuleType` int(11) NOT NULL,
  `iDefSalesRepID` int(11) DEFAULT NULL,
  `iDefWarehouseID` int(11) DEFAULT NULL,
  `iAssignedIPadID` bigint(20) DEFAULT NULL,
  `bAllCustomers` tinyint(1) DEFAULT NULL,
  `iSalesWarehouseID` int(11) DEFAULT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `bUseInvoice` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_wtblpemmobilitymodules`
--

DROP TABLE IF EXISTS `_wtblpemmobilitymodules`;
CREATE TABLE `_wtblpemmobilitymodules` (
  `idMobilityModule` int(11) NOT NULL,
  `cCode` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cName` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `cDescription` longtext CHARACTER SET utf8mb4 NOT NULL,
  `iModuleTypeId` int(11) NOT NULL,
  `gIdentifier` varchar(64) DEFAULT NULL,
  `bIsWinjitModule` tinyint(1) NOT NULL,
  `bIsOneAgentPerDevice` tinyint(1) NOT NULL,
  `bIsMultipleDevicePerAgent` tinyint(1) NOT NULL,
  `bIsActive` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_wtblsystem`
--

DROP TABLE IF EXISTS `_wtblsystem`;
CREATE TABLE `_wtblsystem` (
  `idSystem` int(11) NOT NULL,
  `cIdentity` varchar(35) DEFAULT NULL,
  `cValue` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`AccountLink`),
  ADD KEY `idx_Accounts_Accounts_Checksum` (`Accounts_Checksum`),
  ADD KEY `idx_Accounts_Accounts_dModifiedDate` (`Accounts_dModifiedDate`),
  ADD KEY `idx_Accounts_Accounts_iBranchID` (`Accounts_iBranchID`),
  ADD KEY `idx_Accounts_Accounts_iChangeSetID` (`Accounts_iChangeSetID`),
  ADD KEY `idx_Accounts_SubAccOfLink` (`SubAccOfLink`),
  ADD KEY `idxCaseAcc` (`CaseAcc`),
  ADD KEY `idxDescription` (`Description`),
  ADD KEY `idxMasterSubAccount` (`Master_Sub_Account`);

--
-- Indexes for table `accprev`
--
ALTER TABLE `accprev`
  ADD PRIMARY KEY (`LedgerLink`,`iTxBranchPrevID`),
  ADD KEY `idx_AccPrev_AccPrev_Checksum` (`AccPrev_Checksum`),
  ADD KEY `idx_AccPrev_AccPrev_dModifiedDate` (`AccPrev_dModifiedDate`),
  ADD KEY `idx_AccPrev_AccPrev_iBranchID` (`AccPrev_iBranchID`),
  ADD KEY `idx_AccPrev_AccPrev_iChangeSetID` (`AccPrev_iChangeSetID`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`idAreas`),
  ADD KEY `idx_Areas_Areas_Checksum` (`Areas_Checksum`),
  ADD KEY `idx_Areas_Areas_dModifiedDate` (`Areas_dModifiedDate`),
  ADD KEY `idx_Areas_Areas_iBranchID` (`Areas_iBranchID`),
  ADD KEY `idx_Areas_Areas_iChangeSetID` (`Areas_iChangeSetID`);

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignments_user_id_index` (`user_id`),
  ADD KEY `assignments_warehouse_id_index` (`warehouse_id`);

--
-- Indexes for table `bankmain`
--
ALTER TABLE `bankmain`
  ADD PRIMARY KEY (`Counter`),
  ADD KEY `idx_BankMain_BankMain_Checksum` (`BankMain_Checksum`),
  ADD KEY `idx_BankMain_BankMain_dModifiedDate` (`BankMain_dModifiedDate`),
  ADD KEY `idx_BankMain_BankMain_iBranchID` (`BankMain_iBranchID`),
  ADD KEY `idx_BankMain_BankMain_iChangeSetID` (`BankMain_iChangeSetID`),
  ADD KEY `idxBankName` (`BankName`);

--
-- Indexes for table `bomcomp`
--
ALTER TABLE `bomcomp`
  ADD PRIMARY KEY (`BomComponentKey`),
  ADD KEY `idx_BomComp_BomComp_Checksum` (`BomComp_Checksum`),
  ADD KEY `idx_BomComp_BomComp_dModifiedDate` (`BomComp_dModifiedDate`),
  ADD KEY `idx_BomComp_BomComp_iBranchID` (`BomComp_iBranchID`),
  ADD KEY `idx_BomComp_BomComp_iChangeSetID` (`BomComp_iChangeSetID`),
  ADD KEY `idxBomComponent` (`BomMasterKey`,`ComponentStockLink`),
  ADD KEY `idxBomMasterKey` (`BomMasterKey`,`ComponentIndex`),
  ADD KEY `idxComponentStockLink` (`ComponentStockLink`);

--
-- Indexes for table `bomdef`
--
ALTER TABLE `bomdef`
  ADD PRIMARY KEY (`idBomDef`),
  ADD KEY `idx_BomDef_BomDef_Checksum` (`BomDef_Checksum`),
  ADD KEY `idx_BomDef_BomDef_dModifiedDate` (`BomDef_dModifiedDate`),
  ADD KEY `idx_BomDef_BomDef_iBranchID` (`BomDef_iBranchID`),
  ADD KEY `idx_BomDef_BomDef_iChangeSetID` (`BomDef_iChangeSetID`);

--
-- Indexes for table `bommast`
--
ALTER TABLE `bommast`
  ADD PRIMARY KEY (`BomID`),
  ADD KEY `idx_BomMast_BomMast_Checksum` (`BomMast_Checksum`),
  ADD KEY `idx_BomMast_BomMast_dModifiedDate` (`BomMast_dModifiedDate`),
  ADD KEY `idx_BomMast_BomMast_iBranchID` (`BomMast_iBranchID`),
  ADD KEY `idx_BomMast_BomMast_iChangeSetID` (`BomMast_iChangeSetID`),
  ADD KEY `idxBomStockCode` (`BomStockCode`),
  ADD KEY `idxBomStockLink` (`BomStockLink`),
  ADD KEY `idxDescription` (`BomDescription`);

--
-- Indexes for table `ccdefs`
--
ALTER TABLE `ccdefs`
  ADD PRIMARY KEY (`idCCDefs`),
  ADD KEY `idx_CCDefs_CCDefs_Checksum` (`CCDefs_Checksum`),
  ADD KEY `idx_CCDefs_CCDefs_dModifiedDate` (`CCDefs_dModifiedDate`),
  ADD KEY `idx_CCDefs_CCDefs_iBranchID` (`CCDefs_iBranchID`),
  ADD KEY `idx_CCDefs_CCDefs_iChangeSetID` (`CCDefs_iChangeSetID`);

--
-- Indexes for table `ccdetail`
--
ALTER TABLE `ccdetail`
  ADD PRIMARY KEY (`DebtorLink`),
  ADD KEY `idx_CCDetail_CCDetail_Checksum` (`CCDetail_Checksum`),
  ADD KEY `idx_CCDetail_CCDetail_dModifiedDate` (`CCDetail_dModifiedDate`),
  ADD KEY `idx_CCDetail_CCDetail_iBranchID` (`CCDetail_iBranchID`),
  ADD KEY `idx_CCDetail_CCDetail_iChangeSetID` (`CCDetail_iChangeSetID`);

--
-- Indexes for table `cliclass`
--
ALTER TABLE `cliclass`
  ADD PRIMARY KEY (`IdCliClass`),
  ADD KEY `idx_CliClass_CliClass_Checksum` (`CliClass_Checksum`),
  ADD KEY `idx_CliClass_CliClass_dModifiedDate` (`CliClass_dModifiedDate`),
  ADD KEY `idx_CliClass_CliClass_iBranchID` (`CliClass_iBranchID`),
  ADD KEY `idx_CliClass_CliClass_iChangeSetID` (`CliClass_iChangeSetID`);

--
-- Indexes for table `clidef`
--
ALTER TABLE `clidef`
  ADD PRIMARY KEY (`idCliDef`),
  ADD KEY `idx_CliDef_CliDef_Checksum` (`CliDef_Checksum`),
  ADD KEY `idx_CliDef_CliDef_dModifiedDate` (`CliDef_dModifiedDate`),
  ADD KEY `idx_CliDef_CliDef_iBranchID` (`CliDef_iBranchID`),
  ADD KEY `idx_CliDef_CliDef_iChangeSetID` (`CliDef_iChangeSetID`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`DCLink`),
  ADD KEY `idx_Client_cIDNumber` (`cIDNumber`),
  ADD KEY `idx_Client_Client_Checksum` (`Client_Checksum`),
  ADD KEY `idx_Client_Client_dModifiedDate` (`Client_dModifiedDate`),
  ADD KEY `idx_Client_Client_iBranchID` (`Client_iBranchID`),
  ADD KEY `idx_Client_Client_iChangeSetID` (`Client_iChangeSetID`),
  ADD KEY `idx_Client_cPassportNumber` (`cPassportNumber`),
  ADD KEY `idx_CLIENT_iAreasID` (`iAreasID`),
  ADD KEY `idx_CLIENT_iClassID` (`iClassID`),
  ADD KEY `idxAccount` (`Account`),
  ADD KEY `idxName` (`Name`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_Contact_Contact_Checksum` (`Contact_Checksum`),
  ADD KEY `idx_Contact_Contact_dModifiedDate` (`Contact_dModifiedDate`),
  ADD KEY `idx_Contact_Contact_iBranchID` (`Contact_iBranchID`),
  ADD KEY `idx_Contact_Contact_iChangeSetID` (`Contact_iChangeSetID`),
  ADD KEY `idxDebtorLink` (`DebtorLink`);

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`Autoidx`),
  ADD KEY `idx_Cost_Cost_Checksum` (`Cost_Checksum`),
  ADD KEY `idx_Cost_Cost_dModifiedDate` (`Cost_dModifiedDate`),
  ADD KEY `idx_Cost_Cost_iBranchID` (`Cost_iBranchID`),
  ADD KEY `idx_Cost_Cost_iChangeSetID` (`Cost_iChangeSetID`),
  ADD KEY `idxAccNo` (`DebtorAccNo`);

--
-- Indexes for table `costcntr`
--
ALTER TABLE `costcntr`
  ADD PRIMARY KEY (`Counter`),
  ADD KEY `idx_CostCntr_CostCntr_Checksum` (`CostCntr_Checksum`),
  ADD KEY `idx_CostCntr_CostCntr_dModifiedDate` (`CostCntr_dModifiedDate`),
  ADD KEY `idx_CostCntr_CostCntr_iBranchID` (`CostCntr_iBranchID`),
  ADD KEY `idx_CostCntr_CostCntr_iChangeSetID` (`CostCntr_iChangeSetID`),
  ADD KEY `idxCostCode` (`CostCode`);

--
-- Indexes for table `crdischd`
--
ALTER TABLE `crdischd`
  ADD PRIMARY KEY (`idCrDiscHd`),
  ADD KEY `idx_CrDiscHd_CrDiscHd_Checksum` (`CrDiscHd_Checksum`),
  ADD KEY `idx_CrDiscHd_CrDiscHd_dModifiedDate` (`CrDiscHd_dModifiedDate`),
  ADD KEY `idx_CrDiscHd_CrDiscHd_iBranchID` (`CrDiscHd_iBranchID`),
  ADD KEY `idx_CrDiscHd_CrDiscHd_iChangeSetID` (`CrDiscHd_iChangeSetID`);

--
-- Indexes for table `crdiscmx`
--
ALTER TABLE `crdiscmx`
  ADD PRIMARY KEY (`idCrDiscMx`),
  ADD KEY `idx_CrDiscMx_CrDiscMx_Checksum` (`CrDiscMx_Checksum`),
  ADD KEY `idx_CrDiscMx_CrDiscMx_dModifiedDate` (`CrDiscMx_dModifiedDate`),
  ADD KEY `idx_CrDiscMx_CrDiscMx_iBranchID` (`CrDiscMx_iBranchID`),
  ADD KEY `idx_CrDiscMx_CrDiscMx_iChangeSetID` (`CrDiscMx_iChangeSetID`);

--
-- Indexes for table `credmnt`
--
ALTER TABLE `credmnt`
  ADD PRIMARY KEY (`Autoidx`),
  ADD KEY `idx_CredMnt_CredMnt_Checksum` (`CredMnt_Checksum`),
  ADD KEY `idx_CredMnt_CredMnt_dModifiedDate` (`CredMnt_dModifiedDate`),
  ADD KEY `idx_CredMnt_CredMnt_iBranchID` (`CredMnt_iBranchID`),
  ADD KEY `idx_CredMnt_CredMnt_iChangeSetID` (`CredMnt_iChangeSetID`),
  ADD KEY `idxDesc` (`Category`,`Description`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`CurrencyLink`),
  ADD KEY `idx_Currency_Currency_Checksum` (`Currency_Checksum`),
  ADD KEY `idx_Currency_Currency_dModifiedDate` (`Currency_dModifiedDate`),
  ADD KEY `idx_Currency_Currency_iBranchID` (`Currency_iBranchID`),
  ADD KEY `idx_Currency_Currency_iChangeSetID` (`Currency_iChangeSetID`),
  ADD KEY `idxCurrencyCode` (`CurrencyCode`);

--
-- Indexes for table `currencyhist`
--
ALTER TABLE `currencyhist`
  ADD PRIMARY KEY (`idCurrencyHist`),
  ADD KEY `idx_CurrencyHist_CurrencyHist_Checksum` (`CurrencyHist_Checksum`),
  ADD KEY `idx_CurrencyHist_CurrencyHist_dModifiedDate` (`CurrencyHist_dModifiedDate`),
  ADD KEY `idx_CurrencyHist_CurrencyHist_iBranchID` (`CurrencyHist_iBranchID`),
  ADD KEY `idx_CurrencyHist_CurrencyHist_iChangeSetID` (`CurrencyHist_iChangeSetID`);

--
-- Indexes for table `cwratio`
--
ALTER TABLE `cwratio`
  ADD PRIMARY KEY (`idCwRatio`),
  ADD KEY `idx_CWRatio_CWRatio_Checksum` (`CWRatio_Checksum`),
  ADD KEY `idx_CWRatio_CWRatio_dModifiedDate` (`CWRatio_dModifiedDate`),
  ADD KEY `idx_CWRatio_CWRatio_iBranchID` (`CWRatio_iBranchID`),
  ADD KEY `idx_CWRatio_CWRatio_iChangeSetID` (`CWRatio_iChangeSetID`);

--
-- Indexes for table `deltbl`
--
ALTER TABLE `deltbl`
  ADD PRIMARY KEY (`Counter`),
  ADD KEY `idx_DelTbl_DelTbl_Checksum` (`DelTbl_Checksum`),
  ADD KEY `idx_DelTbl_DelTbl_dModifiedDate` (`DelTbl_dModifiedDate`),
  ADD KEY `idx_DelTbl_DelTbl_iBranchID` (`DelTbl_iBranchID`),
  ADD KEY `idx_DelTbl_DelTbl_iChangeSetID` (`DelTbl_iChangeSetID`),
  ADD KEY `idxCounter` (`Counter`);

--
-- Indexes for table `dept`
--
ALTER TABLE `dept`
  ADD PRIMARY KEY (`idDept`),
  ADD KEY `idx_Dept_Dept_Checksum` (`Dept_Checksum`),
  ADD KEY `idx_Dept_Dept_dModifiedDate` (`Dept_dModifiedDate`),
  ADD KEY `idx_Dept_Dept_iBranchID` (`Dept_iBranchID`),
  ADD KEY `idx_Dept_Dept_iChangeSetID` (`Dept_iChangeSetID`);

--
-- Indexes for table `drdischd`
--
ALTER TABLE `drdischd`
  ADD PRIMARY KEY (`idDrDiscHd`),
  ADD KEY `idx_DrDiscHd_DrDiscHd_Checksum` (`DrDiscHd_Checksum`),
  ADD KEY `idx_DrDiscHd_DrDiscHd_dModifiedDate` (`DrDiscHd_dModifiedDate`),
  ADD KEY `idx_DrDiscHd_DrDiscHd_iBranchID` (`DrDiscHd_iBranchID`),
  ADD KEY `idx_DrDiscHd_DrDiscHd_iChangeSetID` (`DrDiscHd_iChangeSetID`);

--
-- Indexes for table `drdiscmx`
--
ALTER TABLE `drdiscmx`
  ADD PRIMARY KEY (`idDrDiscMx`),
  ADD KEY `idx_DrDiscMx_DrDiscMx_Checksum` (`DrDiscMx_Checksum`),
  ADD KEY `idx_DrDiscMx_DrDiscMx_dModifiedDate` (`DrDiscMx_dModifiedDate`),
  ADD KEY `idx_DrDiscMx_DrDiscMx_iBranchID` (`DrDiscMx_iBranchID`),
  ADD KEY `idx_DrDiscMx_DrDiscMx_iChangeSetID` (`DrDiscMx_iChangeSetID`);

--
-- Indexes for table `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`idEntities`),
  ADD KEY `idx_Entities_Entities_Checksum` (`Entities_Checksum`),
  ADD KEY `idx_Entities_Entities_dModifiedDate` (`Entities_dModifiedDate`),
  ADD KEY `idx_Entities_Entities_iBranchID` (`Entities_iBranchID`),
  ADD KEY `idx_Entities_Entities_iChangeSetID` (`Entities_iChangeSetID`);

--
-- Indexes for table `glbranch`
--
ALTER TABLE `glbranch`
  ADD PRIMARY KEY (`idGLBranch`),
  ADD KEY `idx_GLBranch_GLBranch_Checksum` (`GLBranch_Checksum`),
  ADD KEY `idx_GLBranch_GLBranch_dModifiedDate` (`GLBranch_dModifiedDate`),
  ADD KEY `idx_GLBranch_GLBranch_iBranchID` (`GLBranch_iBranchID`),
  ADD KEY `idx_GLBranch_GLBranch_iChangeSetID` (`GLBranch_iChangeSetID`);

--
-- Indexes for table `grptbl`
--
ALTER TABLE `grptbl`
  ADD PRIMARY KEY (`idGrpTbl`),
  ADD KEY `idx_GrpTbl_GrpTbl_Checksum` (`GrpTbl_Checksum`),
  ADD KEY `idx_GrpTbl_GrpTbl_dModifiedDate` (`GrpTbl_dModifiedDate`),
  ADD KEY `idx_GrpTbl_GrpTbl_iBranchID` (`GrpTbl_iBranchID`),
  ADD KEY `idx_GrpTbl_GrpTbl_iChangeSetID` (`GrpTbl_iChangeSetID`);

--
-- Indexes for table `invnum`
--
ALTER TABLE `invnum`
  ADD PRIMARY KEY (`AutoIndex`),
  ADD KEY `idx_InvNum_InvNum_Checksum` (`InvNum_Checksum`),
  ADD KEY `idx_InvNum_InvNum_dModifiedDate` (`InvNum_dModifiedDate`),
  ADD KEY `idx_InvNum_InvNum_iBranchID` (`InvNum_iBranchID`),
  ADD KEY `idx_InvNum_InvNum_iChangeSetID` (`InvNum_iChangeSetID`),
  ADD KEY `idx_INVNUM_iOpportunityID` (`iOpportunityID`),
  ADD KEY `idxAccountID_DocType` (`AccountID`,`DocType`),
  ADD KEY `idxDocType` (`DocType`,`DocState`,`InvNumber`);

--
-- Indexes for table `jobdef`
--
ALTER TABLE `jobdef`
  ADD PRIMARY KEY (`idJobDef`),
  ADD KEY `idx_JobDef_JobDef_Checksum` (`JobDef_Checksum`),
  ADD KEY `idx_JobDef_JobDef_dModifiedDate` (`JobDef_dModifiedDate`),
  ADD KEY `idx_JobDef_JobDef_iBranchID` (`JobDef_iBranchID`),
  ADD KEY `idx_JobDef_JobDef_iChangeSetID` (`JobDef_iChangeSetID`);

--
-- Indexes for table `jobnum`
--
ALTER TABLE `jobnum`
  ADD PRIMARY KEY (`AutoIndex`),
  ADD KEY `idx_JobNum_JobNum_Checksum` (`JobNum_Checksum`),
  ADD KEY `idx_JobNum_JobNum_dModifiedDate` (`JobNum_dModifiedDate`),
  ADD KEY `idx_JobNum_JobNum_iBranchID` (`JobNum_iBranchID`),
  ADD KEY `idx_JobNum_JobNum_iChangeSetID` (`JobNum_iChangeSetID`);

--
-- Indexes for table `jobstock`
--
ALTER TABLE `jobstock`
  ADD UNIQUE KEY `PK_JOBSTOCK` (`JobStock`),
  ADD KEY `idx_JobStock_JobStock_Checksum` (`JobStock_Checksum`),
  ADD KEY `idx_JobStock_JobStock_dModifiedDate` (`JobStock_dModifiedDate`),
  ADD KEY `idx_JobStock_JobStock_iBranchID` (`JobStock_iBranchID`),
  ADD KEY `idx_JobStock_JobStock_iChangeSetID` (`JobStock_iChangeSetID`);

--
-- Indexes for table `jobtxtp`
--
ALTER TABLE `jobtxtp`
  ADD PRIMARY KEY (`idJobTxTp`),
  ADD KEY `idx_JobTxTp_JobTxTp_Checksum` (`JobTxTp_Checksum`),
  ADD KEY `idx_JobTxTp_JobTxTp_dModifiedDate` (`JobTxTp_dModifiedDate`),
  ADD KEY `idx_JobTxTp_JobTxTp_iBranchID` (`JobTxTp_iBranchID`),
  ADD KEY `idx_JobTxTp_JobTxTp_iChangeSetID` (`JobTxTp_iChangeSetID`),
  ADD KEY `idxDescription` (`Description`);

--
-- Indexes for table `mastoffs`
--
ALTER TABLE `mastoffs`
  ADD PRIMARY KEY (`idMastOffs`),
  ADD KEY `idx_MastOffs_MastOffs_Checksum` (`MastOffs_Checksum`),
  ADD KEY `idx_MastOffs_MastOffs_dModifiedDate` (`MastOffs_dModifiedDate`),
  ADD KEY `idx_MastOffs_MastOffs_iBranchID` (`MastOffs_iBranchID`),
  ADD KEY `idx_MastOffs_MastOffs_iChangeSetID` (`MastOffs_iChangeSetID`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nt_suppliers`
--
ALTER TABLE `nt_suppliers`
  ADD PRIMARY KEY (`NTSupID`);

--
-- Indexes for table `nt_suppliers_audit`
--
ALTER TABLE `nt_suppliers_audit`
  ADD PRIMARY KEY (`iNTSupAuditID`);

--
-- Indexes for table `open_sessions`
--
ALTER TABLE `open_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `open_sessions_user_id_index` (`user_id`),
  ADD KEY `open_sessions_warehouse_id_index` (`warehouse_id`);

--
-- Indexes for table `ordersdf`
--
ALTER TABLE `ordersdf`
  ADD PRIMARY KEY (`DefaultCounter`),
  ADD KEY `idx_OrdersDf_OrdersDf_Checksum` (`OrdersDf_Checksum`),
  ADD KEY `idx_OrdersDf_OrdersDf_dModifiedDate` (`OrdersDf_dModifiedDate`),
  ADD KEY `idx_OrdersDf_OrdersDf_iBranchID` (`OrdersDf_iBranchID`),
  ADD KEY `idx_OrdersDf_OrdersDf_iChangeSetID` (`OrdersDf_iChangeSetID`);

--
-- Indexes for table `ordersst`
--
ALTER TABLE `ordersst`
  ADD PRIMARY KEY (`StatusCounter`),
  ADD KEY `idx_OrdersSt_OrdersSt_Checksum` (`OrdersSt_Checksum`),
  ADD KEY `idx_OrdersSt_OrdersSt_dModifiedDate` (`OrdersSt_dModifiedDate`),
  ADD KEY `idx_OrdersSt_OrdersSt_iBranchID` (`OrdersSt_iBranchID`),
  ADD KEY `idx_OrdersSt_OrdersSt_iChangeSetID` (`OrdersSt_iChangeSetID`),
  ADD KEY `idxStatusDescrip` (`StatusDescrip`);

--
-- Indexes for table `pcktbl`
--
ALTER TABLE `pcktbl`
  ADD PRIMARY KEY (`idPckTbl`),
  ADD KEY `idx_PckTbl_PckTbl_Checksum` (`PckTbl_Checksum`),
  ADD KEY `idx_PckTbl_PckTbl_dModifiedDate` (`PckTbl_dModifiedDate`),
  ADD KEY `idx_PckTbl_PckTbl_iBranchID` (`PckTbl_iBranchID`),
  ADD KEY `idx_PckTbl_PckTbl_iChangeSetID` (`PckTbl_iChangeSetID`);

--
-- Indexes for table `periodpermissions`
--
ALTER TABLE `periodpermissions`
  ADD PRIMARY KEY (`idPeriodPermissions`),
  ADD KEY `idx_PeriodPermissions_PeriodPermissions_Checksum` (`PeriodPermissions_Checksum`),
  ADD KEY `idx_PeriodPermissions_PeriodPermissions_dModifiedDate` (`PeriodPermissions_dModifiedDate`),
  ADD KEY `idx_PeriodPermissions_PeriodPermissions_iBranchID` (`PeriodPermissions_iBranchID`),
  ADD KEY `idx_PeriodPermissions_PeriodPermissions_iChangeSetID` (`PeriodPermissions_iChangeSetID`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petty_cashes`
--
ALTER TABLE `petty_cashes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `petty_cashes_user_id_index` (`user_id`),
  ADD KEY `petty_cashes_petty_cash_type_id_index` (`petty_cash_type_id`);

--
-- Indexes for table `petty_cash_types`
--
ALTER TABLE `petty_cash_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pinneditems`
--
ALTER TABLE `pinneditems`
  ADD PRIMARY KEY (`PinnedItemID`);

--
-- Indexes for table `posdefs`
--
ALTER TABLE `posdefs`
  ADD PRIMARY KEY (`IDPOSDefs`),
  ADD KEY `idx_PosDefs_PosDefs_Checksum` (`PosDefs_Checksum`),
  ADD KEY `idx_PosDefs_PosDefs_dModifiedDate` (`PosDefs_dModifiedDate`),
  ADD KEY `idx_PosDefs_PosDefs_iBranchID` (`PosDefs_iBranchID`),
  ADD KEY `idx_PosDefs_PosDefs_iChangeSetID` (`PosDefs_iChangeSetID`);

--
-- Indexes for table `postap`
--
ALTER TABLE `postap`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_PostAP_PostAP_Checksum` (`PostAP_Checksum`),
  ADD KEY `idx_PostAP_PostAP_dModifiedDate` (`PostAP_dModifiedDate`),
  ADD KEY `idx_PostAP_PostAP_iBranchID` (`PostAP_iBranchID`),
  ADD KEY `idx_PostAP_PostAP_iChangeSetID` (`PostAP_iChangeSetID`),
  ADD KEY `idxAccountLink` (`AccountLink`),
  ADD KEY `idxAuditNumber` (`cAuditNumber`),
  ADD KEY `idxDate` (`TxDate`),
  ADD KEY `idxPostAP_iTxBranchID` (`iTxBranchID`);

--
-- Indexes for table `postar`
--
ALTER TABLE `postar`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_PostAR_LinkAccCode` (`LinkAccCode`),
  ADD KEY `idx_PostAR_PostAR_Checksum` (`PostAR_Checksum`),
  ADD KEY `idx_PostAR_PostAR_dModifiedDate` (`PostAR_dModifiedDate`),
  ADD KEY `idx_PostAR_PostAR_iBranchID` (`PostAR_iBranchID`),
  ADD KEY `idx_PostAR_PostAR_iChangeSetID` (`PostAR_iChangeSetID`),
  ADD KEY `idxAccountLink` (`AccountLink`),
  ADD KEY `idxAuditNumber` (`cAuditNumber`),
  ADD KEY `idxDate` (`TxDate`),
  ADD KEY `idxPostAR_iTxBranchID` (`iTxBranchID`);

--
-- Indexes for table `postgl`
--
ALTER TABLE `postgl`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_PostGL_PostGL_Checksum` (`PostGL_Checksum`),
  ADD KEY `idx_PostGL_PostGL_dModifiedDate` (`PostGL_dModifiedDate`),
  ADD KEY `idx_PostGL_PostGL_iBranchID` (`PostGL_iBranchID`),
  ADD KEY `idx_PostGL_PostGL_iChangeSetID` (`PostGL_iChangeSetID`),
  ADD KEY `idxAuditNumber` (`cAuditNumber`),
  ADD KEY `idxDate` (`TxDate`),
  ADD KEY `idxPeriod` (`Period`),
  ADD KEY `idxPostGL_iTxBranchID` (`iTxBranchID`);

--
-- Indexes for table `postst`
--
ALTER TABLE `postst`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_PostST_PostST_Checksum` (`PostST_Checksum`),
  ADD KEY `idx_PostST_PostST_dModifiedDate` (`PostST_dModifiedDate`),
  ADD KEY `idx_PostST_PostST_iBranchID` (`PostST_iBranchID`),
  ADD KEY `idx_PostST_PostST_iChangeSetID` (`PostST_iChangeSetID`),
  ADD KEY `idxAccountLink` (`AccountLink`),
  ADD KEY `idxAuditNumber` (`cAuditNumber`),
  ADD KEY `idxDate` (`TxDate`),
  ADD KEY `idxPostST_iTxBranchID` (`iTxBranchID`);

--
-- Indexes for table `printgrp`
--
ALTER TABLE `printgrp`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_PrintGrp_PrintGrp_Checksum` (`PrintGrp_Checksum`),
  ADD KEY `idx_PrintGrp_PrintGrp_dModifiedDate` (`PrintGrp_dModifiedDate`),
  ADD KEY `idx_PrintGrp_PrintGrp_iBranchID` (`PrintGrp_iBranchID`),
  ADD KEY `idx_PrintGrp_PrintGrp_iChangeSetID` (`PrintGrp_iChangeSetID`),
  ADD KEY `idxDesc` (`Descrip`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`ProjectLink`),
  ADD KEY `idx_Project_Project_Checksum` (`Project_Checksum`),
  ADD KEY `idx_Project_Project_dModifiedDate` (`Project_dModifiedDate`),
  ADD KEY `idx_Project_Project_iBranchID` (`Project_iBranchID`),
  ADD KEY `idx_Project_Project_iChangeSetID` (`Project_iChangeSetID`),
  ADD KEY `idxMasterSubProject` (`MasterSubProject`),
  ADD KEY `idxProjectCode` (`ProjectCode`);

--
-- Indexes for table `recentitems`
--
ALTER TABLE `recentitems`
  ADD PRIMARY KEY (`RecentItemID`);

--
-- Indexes for table `recurrc`
--
ALTER TABLE `recurrc`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_RecurRC_RecurRC_Checksum` (`RecurRC_Checksum`),
  ADD KEY `idx_RecurRC_RecurRC_dModifiedDate` (`RecurRC_dModifiedDate`),
  ADD KEY `idx_RecurRC_RecurRC_iBranchID` (`RecurRC_iBranchID`),
  ADD KEY `idx_RecurRC_RecurRC_iChangeSetID` (`RecurRC_iChangeSetID`),
  ADD KEY `idxDescription` (`Description`);

--
-- Indexes for table `recurrdef`
--
ALTER TABLE `recurrdef`
  ADD KEY `idx_RecurRDef_RecurRDef_Checksum` (`RecurRDef_Checksum`),
  ADD KEY `idx_RecurRDef_RecurRDef_dModifiedDate` (`RecurRDef_dModifiedDate`),
  ADD KEY `idx_RecurRDef_RecurRDef_iBranchID` (`RecurRDef_iBranchID`),
  ADD KEY `idx_RecurRDef_RecurRDef_iChangeSetID` (`RecurRDef_iChangeSetID`);

--
-- Indexes for table `recurrf`
--
ALTER TABLE `recurrf`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_RecurRF_RecurRF_Checksum` (`RecurRF_Checksum`),
  ADD KEY `idx_RecurRF_RecurRF_dModifiedDate` (`RecurRF_dModifiedDate`),
  ADD KEY `idx_RecurRF_RecurRF_iBranchID` (`RecurRF_iBranchID`),
  ADD KEY `idx_RecurRF_RecurRF_iChangeSetID` (`RecurRF_iChangeSetID`),
  ADD KEY `idxDescription` (`Descrip`);

--
-- Indexes for table `recurrl`
--
ALTER TABLE `recurrl`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_RecurRL_RecurRL_Checksum` (`RecurRL_Checksum`),
  ADD KEY `idx_RecurRL_RecurRL_dModifiedDate` (`RecurRL_dModifiedDate`),
  ADD KEY `idx_RecurRL_RecurRL_iBranchID` (`RecurRL_iBranchID`),
  ADD KEY `idx_RecurRL_RecurRL_iChangeSetID` (`RecurRL_iChangeSetID`);

--
-- Indexes for table `recurrtx`
--
ALTER TABLE `recurrtx`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_RecurRTX_RecurRTX_Checksum` (`RecurRTX_Checksum`),
  ADD KEY `idx_RecurRTX_RecurRTX_dModifiedDate` (`RecurRTX_dModifiedDate`),
  ADD KEY `idx_RecurRTX_RecurRTX_iBranchID` (`RecurRTX_iBranchID`),
  ADD KEY `idx_RecurRTX_RecurRTX_iChangeSetID` (`RecurRTX_iChangeSetID`);

--
-- Indexes for table `refer`
--
ALTER TABLE `refer`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx_Refer_Refer_Checksum` (`Refer_Checksum`),
  ADD KEY `idx_Refer_Refer_dModifiedDate` (`Refer_dModifiedDate`),
  ADD KEY `idx_Refer_Refer_iBranchID` (`Refer_iBranchID`),
  ADD KEY `idx_Refer_Refer_iChangeSetID` (`Refer_iChangeSetID`),
  ADD KEY `idxAcc` (`NameOfRef`),
  ADD KEY `idxAll` (`Category`,`DebtorAccNo`,`NameOfRef`),
  ADD KEY `idxDeb` (`DebtorAccNo`);

--
-- Indexes for table `reminder`
--
ALTER TABLE `reminder`
  ADD PRIMARY KEY (`Autoidx`),
  ADD KEY `idx_Reminder_Reminder_Checksum` (`Reminder_Checksum`),
  ADD KEY `idx_Reminder_Reminder_dModifiedDate` (`Reminder_dModifiedDate`),
  ADD KEY `idx_Reminder_Reminder_iBranchID` (`Reminder_iBranchID`),
  ADD KEY `idx_Reminder_Reminder_iChangeSetID` (`Reminder_iChangeSetID`),
  ADD KEY `idxComp` (`CompanyName`),
  ADD KEY `idxRemind` (`Status`,`UserName`,`RemindDate`),
  ADD KEY `idxType` (`TypeofRemind`);

--
-- Indexes for table `rfq`
--
ALTER TABLE `rfq`
  ADD PRIMARY KEY (`iRFQID`),
  ADD KEY `idx_RFQ_RFQ_iBranchID` (`RFQ_iBranchID`),
  ADD KEY `idx_RFQ_RFQ_dModifiedDate` (`RFQ_dModifiedDate`),
  ADD KEY `idx_RFQ_RFQ_iChangeSetID` (`RFQ_iChangeSetID`),
  ADD KEY `idx_RFQ_RFQ_Checksum` (`RFQ_Checksum`);

--
-- Indexes for table `rfqdf`
--
ALTER TABLE `rfqdf`
  ADD PRIMARY KEY (`DefaultCounter`),
  ADD KEY `idx_RFQDF_RFQDF_iBranchID` (`RFQDF_iBranchID`),
  ADD KEY `idx_RFQDF_RFQDF_dModifiedDate` (`RFQDF_dModifiedDate`),
  ADD KEY `idx_RFQDF_RFQDF_iChangeSetID` (`RFQDF_iChangeSetID`),
  ADD KEY `idx_RFQDF_RFQDF_Checksum` (`RFQDF_Checksum`);

--
-- Indexes for table `rfq_agentcostcentremap`
--
ALTER TABLE `rfq_agentcostcentremap`
  ADD PRIMARY KEY (`idAgentCostCentreMap`),
  ADD KEY `FK_RFQ_AgentCostCentreMap__rtblAgents` (`idAgent`);

--
-- Indexes for table `rfq_agentsectormapping`
--
ALTER TABLE `rfq_agentsectormapping`
  ADD PRIMARY KEY (`idAgentSectorMap`),
  ADD KEY `FK__RFQ_Agent__idAgents` (`idAgents`);

--
-- Indexes for table `rfq_apshareholderlinks`
--
ALTER TABLE `rfq_apshareholderlinks`
  ADD PRIMARY KEY (`idAPShareholderLinks`);

--
-- Indexes for table `rfq_audittables`
--
ALTER TABLE `rfq_audittables`
  ADD PRIMARY KEY (`iAuditTableID`);

--
-- Indexes for table `rfq_costcentre`
--
ALTER TABLE `rfq_costcentre`
  ADD PRIMARY KEY (`idCostCentre`),
  ADD KEY `idx_RFQ_costcentre_RFQ_costcentre_iBranchID` (`RFQ_costcentre_iBranchID`),
  ADD KEY `idx_RFQ_costcentre_RFQ_costcentre_dModifiedDate` (`RFQ_costcentre_dModifiedDate`),
  ADD KEY `idx_RFQ_costcentre_RFQ_costcentre_iChangeSetID` (`RFQ_costcentre_iChangeSetID`),
  ADD KEY `idx_RFQ_costcentre_RFQ_costcentre_Checksum` (`RFQ_costcentre_Checksum`);

--
-- Indexes for table `rfq_deviationreason`
--
ALTER TABLE `rfq_deviationreason`
  ADD PRIMARY KEY (`idDeviationReason`),
  ADD KEY `idx_RFQ_DeviationReason_RFQ_DeviationReason_iBranchID` (`RFQ_DeviationReason_iBranchID`),
  ADD KEY `idx_RFQ_DeviationReason_RFQ_DeviationReason_dModifiedDate` (`RFQ_DeviationReason_dModifiedDate`),
  ADD KEY `idx_RFQ_DeviationReason_RFQ_DeviationReason_iChangeSetID` (`RFQ_DeviationReason_iChangeSetID`),
  ADD KEY `idx_RFQ_DeviationReason_RFQ_DeviationReason_Checksum` (`RFQ_DeviationReason_Checksum`);

--
-- Indexes for table `rfq_deviations`
--
ALTER TABLE `rfq_deviations`
  ADD PRIMARY KEY (`idDeviation`);

--
-- Indexes for table `rfq_event`
--
ALTER TABLE `rfq_event`
  ADD PRIMARY KEY (`iEventID`);

--
-- Indexes for table `rfq_fileattachment`
--
ALTER TABLE `rfq_fileattachment`
  ADD PRIMARY KEY (`iFileID`);

--
-- Indexes for table `rfq_newquotationparams`
--
ALTER TABLE `rfq_newquotationparams`
  ADD PRIMARY KEY (`PK_NewQuotationParamID`);

--
-- Indexes for table `rfq_newtender`
--
ALTER TABLE `rfq_newtender`
  ADD PRIMARY KEY (`PK_NewTenderID`),
  ADD KEY `idx_RFQ_NewTender_RFQ_NewTender_iBranchID` (`RFQ_NewTender_iBranchID`),
  ADD KEY `idx_RFQ_NewTender_RFQ_NewTender_dModifiedDate` (`RFQ_NewTender_dModifiedDate`),
  ADD KEY `idx_RFQ_NewTender_RFQ_NewTender_iChangeSetID` (`RFQ_NewTender_iChangeSetID`),
  ADD KEY `idx_RFQ_NewTender_RFQ_NewTender_Checksum` (`RFQ_NewTender_Checksum`);

--
-- Indexes for table `rfq_newtenderdetails`
--
ALTER TABLE `rfq_newtenderdetails`
  ADD PRIMARY KEY (`PK_NewTenderDetailID`),
  ADD KEY `FK_RFQ_NewTenderDetails_RFQ_NewTender` (`FK_NewTenderID`);

--
-- Indexes for table `rfq_newtenderparams`
--
ALTER TABLE `rfq_newtenderparams`
  ADD PRIMARY KEY (`PK_NewTenderParamID`),
  ADD KEY `FK_RFQ_NewTenderParams_RFQ_NewTenderDetails` (`FK_NewTenderDetailID`),
  ADD KEY `FK_RFQ_NewTenderParams_RFQ_TenderParameters` (`FK_TenderParamID`);

--
-- Indexes for table `rfq_notes`
--
ALTER TABLE `rfq_notes`
  ADD PRIMARY KEY (`iNoteID`);

--
-- Indexes for table `rfq_parametercriteria`
--
ALTER TABLE `rfq_parametercriteria`
  ADD PRIMARY KEY (`idRfqParam`),
  ADD KEY `idx_RFQ_ParameterCriteria_RFQ_ParameterCriteria_iBranchID` (`RFQ_ParameterCriteria_iBranchID`),
  ADD KEY `idx_RFQ_ParameterCriteria_RFQ_ParameterCriteria_dModifiedDate` (`RFQ_ParameterCriteria_dModifiedDate`),
  ADD KEY `idx_RFQ_ParameterCriteria_RFQ_ParameterCriteria_iChangeSetID` (`RFQ_ParameterCriteria_iChangeSetID`),
  ADD KEY `idx_RFQ_ParameterCriteria_RFQ_ParameterCriteria_Checksum` (`RFQ_ParameterCriteria_Checksum`);

--
-- Indexes for table `rfq_parameterudf`
--
ALTER TABLE `rfq_parameterudf`
  ADD PRIMARY KEY (`idRfqParamUdf`);

--
-- Indexes for table `rfq_peoplelinks`
--
ALTER TABLE `rfq_peoplelinks`
  ADD PRIMARY KEY (`iPeopleLinks`);

--
-- Indexes for table `rfq_recordquotationparams`
--
ALTER TABLE `rfq_recordquotationparams`
  ADD PRIMARY KEY (`PK_RecordQuotationParamID`);

--
-- Indexes for table `rfq_recordtender`
--
ALTER TABLE `rfq_recordtender`
  ADD PRIMARY KEY (`PK_RecordTender`),
  ADD KEY `idx_RFQ_RecordTender_RFQ_RecordTender_iBranchID` (`RFQ_RecordTender_iBranchID`),
  ADD KEY `idx_RFQ_RecordTender_RFQ_RecordTender_dModifiedDate` (`RFQ_RecordTender_dModifiedDate`),
  ADD KEY `idx_RFQ_RecordTender_RFQ_RecordTender_iChangeSetID` (`RFQ_RecordTender_iChangeSetID`),
  ADD KEY `idx_RFQ_RecordTender_RFQ_RecordTender_Checksum` (`RFQ_RecordTender_Checksum`);

--
-- Indexes for table `rfq_recordtenderdetails`
--
ALTER TABLE `rfq_recordtenderdetails`
  ADD PRIMARY KEY (`PK_RecordTenderDetailID`),
  ADD KEY `FK_RFQ_RecordTenderDetails_RFQ_RecordTenderDetails` (`FK_RecordTenderID`);

--
-- Indexes for table `rfq_recordtenderparams`
--
ALTER TABLE `rfq_recordtenderparams`
  ADD PRIMARY KEY (`PK_RecordTenderParamID`),
  ADD KEY `FK_RFQ_RecordTenderParams_RFQ_RecordTenderDetails` (`FK_RecordTenderDetailID`),
  ADD KEY `FK_RFQ_RecordTenderParams_RFQ_TenderParameters` (`FK_TenderParamID`);

--
-- Indexes for table `rfq_sector`
--
ALTER TABLE `rfq_sector`
  ADD PRIMARY KEY (`idSector`),
  ADD KEY `idx_RFq_sector_RFq_sector_iBranchID` (`RFq_sector_iBranchID`),
  ADD KEY `idx_RFq_sector_RFq_sector_dModifiedDate` (`RFq_sector_dModifiedDate`),
  ADD KEY `idx_RFq_sector_RFq_sector_iChangeSetID` (`RFq_sector_iChangeSetID`),
  ADD KEY `idx_RFq_sector_RFq_sector_Checksum` (`RFq_sector_Checksum`);

--
-- Indexes for table `rfq_stocklinks`
--
ALTER TABLE `rfq_stocklinks`
  ADD PRIMARY KEY (`idStockLinks`);

--
-- Indexes for table `rfq_supplierfiltering`
--
ALTER TABLE `rfq_supplierfiltering`
  ADD PRIMARY KEY (`idSupplierUDF`);

--
-- Indexes for table `rfq_supplierpreference`
--
ALTER TABLE `rfq_supplierpreference`
  ADD PRIMARY KEY (`idSupplierUDF`);

--
-- Indexes for table `rfq_tenderdf`
--
ALTER TABLE `rfq_tenderdf`
  ADD PRIMARY KEY (`PK_Default`),
  ADD KEY `idx_RFQ_TenderDF_RFQ_TenderDF_iBranchID` (`RFQ_TenderDF_iBranchID`),
  ADD KEY `idx_RFQ_TenderDF_RFQ_TenderDF_dModifiedDate` (`RFQ_TenderDF_dModifiedDate`),
  ADD KEY `idx_RFQ_TenderDF_RFQ_TenderDF_iChangeSetID` (`RFQ_TenderDF_iChangeSetID`),
  ADD KEY `idx_RFQ_TenderDF_RFQ_TenderDF_Checksum` (`RFQ_TenderDF_Checksum`);

--
-- Indexes for table `rfq_tenderparameters`
--
ALTER TABLE `rfq_tenderparameters`
  ADD PRIMARY KEY (`PK_TenderParamID`),
  ADD KEY `idx_RFQ_TenderParameters_RFQ_TenderParameters_iBranchID` (`RFQ_TenderParameters_iBranchID`),
  ADD KEY `idx_RFQ_TenderParameters_RFQ_TenderParameters_dModifiedDate` (`RFQ_TenderParameters_dModifiedDate`),
  ADD KEY `idx_RFQ_TenderParameters_RFQ_TenderParameters_iChangeSetID` (`RFQ_TenderParameters_iChangeSetID`),
  ADD KEY `idx_RFQ_TenderParameters_RFQ_TenderParameters_Checksum` (`RFQ_TenderParameters_Checksum`);

--
-- Indexes for table `rfq_udf`
--
ALTER TABLE `rfq_udf`
  ADD PRIMARY KEY (`idUDF`);

--
-- Indexes for table `rfq_vendor`
--
ALTER TABLE `rfq_vendor`
  ADD PRIMARY KEY (`idVendor`),
  ADD KEY `idx_RFQ_Vendor_RFQ_Vendor_iBranchID` (`RFQ_Vendor_iBranchID`),
  ADD KEY `idx_RFQ_Vendor_RFQ_Vendor_dModifiedDate` (`RFQ_Vendor_dModifiedDate`),
  ADD KEY `idx_RFQ_Vendor_RFQ_Vendor_iChangeSetID` (`RFQ_Vendor_iChangeSetID`),
  ADD KEY `idx_RFQ_Vendor_RFQ_Vendor_Checksum` (`RFQ_Vendor_Checksum`);

--
-- Indexes for table `rfq_vendorparameter`
--
ALTER TABLE `rfq_vendorparameter`
  ADD PRIMARY KEY (`VendorParameterID`),
  ADD KEY `idx_RFQ_VendorParameter_RFQ_VendorParameter_iBranchID` (`RFQ_VendorParameter_iBranchID`),
  ADD KEY `idx_RFQ_VendorParameter_RFQ_VendorParameter_dModifiedDate` (`RFQ_VendorParameter_dModifiedDate`),
  ADD KEY `idx_RFQ_VendorParameter_RFQ_VendorParameter_iChangeSetID` (`RFQ_VendorParameter_iChangeSetID`),
  ADD KEY `idx_RFQ_VendorParameter_RFQ_VendorParameter_Checksum` (`RFQ_VendorParameter_Checksum`);

--
-- Indexes for table `rfq_vendorscore`
--
ALTER TABLE `rfq_vendorscore`
  ADD PRIMARY KEY (`idVendorScore`),
  ADD KEY `idx_RFQ_VendorScore_RFQ_VendorScore_iBranchID` (`RFQ_VendorScore_iBranchID`),
  ADD KEY `idx_RFQ_VendorScore_RFQ_VendorScore_dModifiedDate` (`RFQ_VendorScore_dModifiedDate`),
  ADD KEY `idx_RFQ_VendorScore_RFQ_VendorScore_iChangeSetID` (`RFQ_VendorScore_iChangeSetID`),
  ADD KEY `idx_RFQ_VendorScore_RFQ_VendorScore_Checksum` (`RFQ_VendorScore_Checksum`),
  ADD KEY `FK_RFQ_Score_RFQ_Vendor` (`iVendorID`),
  ADD KEY `FK_RFQ_Score_RFQ_VendorParameter` (`iParameterID`);

--
-- Indexes for table `rfq_workflowlink`
--
ALTER TABLE `rfq_workflowlink`
  ADD PRIMARY KEY (`iWorkflowLinkID`),
  ADD KEY `FK_RFQ_WorkflowLink__btblCMWorkflowMembers` (`iWorkflowMemberID`),
  ADD KEY `FK_RFQ_WorkflowLink_RFQ_Event` (`iEventID`);

--
-- Indexes for table `salesrep`
--
ALTER TABLE `salesrep`
  ADD PRIMARY KEY (`idSalesRep`),
  ADD KEY `idx_SalesRep_SalesRep_Checksum` (`SalesRep_Checksum`),
  ADD KEY `idx_SalesRep_SalesRep_dModifiedDate` (`SalesRep_dModifiedDate`),
  ADD KEY `idx_SalesRep_SalesRep_iBranchID` (`SalesRep_iBranchID`),
  ADD KEY `idx_SalesRep_SalesRep_iChangeSetID` (`SalesRep_iChangeSetID`),
  ADD KEY `idxCode` (`Code`);

--
-- Indexes for table `serialmf`
--
ALTER TABLE `serialmf`
  ADD PRIMARY KEY (`SerialCounter`),
  ADD KEY `idx_SerialMF_SerialMF_Checksum` (`SerialMF_Checksum`),
  ADD KEY `idx_SerialMF_SerialMF_dModifiedDate` (`SerialMF_dModifiedDate`),
  ADD KEY `idx_SerialMF_SerialMF_iBranchID` (`SerialMF_iBranchID`),
  ADD KEY `idx_SerialMF_SerialMF_iChangeSetID` (`SerialMF_iChangeSetID`),
  ADD KEY `idxCode_Loc_Serial` (`SNStockLink`,`CurrentLoc`,`SerialNumber`),
  ADD KEY `idxSerial` (`SerialNumber`),
  ADD KEY `idxSerial_Code` (`SerialNumber`,`SNStockLink`);

--
-- Indexes for table `serialtx`
--
ALTER TABLE `serialtx`
  ADD PRIMARY KEY (`SNTxCounter`),
  ADD KEY `idx_SerialTX_SerialTX_Checksum` (`SerialTX_Checksum`),
  ADD KEY `idx_SerialTX_SerialTX_dModifiedDate` (`SerialTX_dModifiedDate`),
  ADD KEY `idx_SerialTX_SerialTX_iBranchID` (`SerialTX_iBranchID`),
  ADD KEY `idx_SerialTX_SerialTX_iChangeSetID` (`SerialTX_iChangeSetID`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simplesettings`
--
ALTER TABLE `simplesettings`
  ADD PRIMARY KEY (`SimpleSettingID`);

--
-- Indexes for table `sliplay`
--
ALTER TABLE `sliplay`
  ADD PRIMARY KEY (`IDSlipLay`),
  ADD KEY `idx_SlipLay_SlipLay_Checksum` (`SlipLay_Checksum`),
  ADD KEY `idx_SlipLay_SlipLay_dModifiedDate` (`SlipLay_dModifiedDate`),
  ADD KEY `idx_SlipLay_SlipLay_iBranchID` (`SlipLay_iBranchID`),
  ADD KEY `idx_SlipLay_SlipLay_iChangeSetID` (`SlipLay_iChangeSetID`);

--
-- Indexes for table `stdftbl`
--
ALTER TABLE `stdftbl`
  ADD PRIMARY KEY (`idStDfTbl`),
  ADD KEY `idx_StDfTbl_StDfTbl_Checksum` (`StDfTbl_Checksum`),
  ADD KEY `idx_StDfTbl_StDfTbl_dModifiedDate` (`StDfTbl_dModifiedDate`),
  ADD KEY `idx_StDfTbl_StDfTbl_iBranchID` (`StDfTbl_iBranchID`),
  ADD KEY `idx_StDfTbl_StDfTbl_iChangeSetID` (`StDfTbl_iChangeSetID`);

--
-- Indexes for table `stkitem`
--
ALTER TABLE `stkitem`
  ADD PRIMARY KEY (`StockLink`),
  ADD KEY `idx_StkItem_Code` (`Code`(255)),
  ADD KEY `idx_StkItem_StkItem_Checksum` (`StkItem_Checksum`),
  ADD KEY `idx_StkItem_StkItem_dModifiedDate` (`StkItem_dModifiedDate`),
  ADD KEY `idx_StkItem_StkItem_iBranchID` (`StkItem_iBranchID`),
  ADD KEY `idx_StkItem_StkItem_iChangeSetID` (`StkItem_iChangeSetID`),
  ADD KEY `idxBarCode` (`Bar_Code`(255)),
  ADD KEY `idxDescription1` (`Description_1`),
  ADD KEY `idxGroup` (`ItemGroup`);

--
-- Indexes for table `taxrate`
--
ALTER TABLE `taxrate`
  ADD PRIMARY KEY (`idTaxRate`),
  ADD KEY `idx_TaxRate_TaxRate_Checksum` (`TaxRate_Checksum`),
  ADD KEY `idx_TaxRate_TaxRate_dModifiedDate` (`TaxRate_dModifiedDate`),
  ADD KEY `idx_TaxRate_TaxRate_iBranchID` (`TaxRate_iBranchID`),
  ADD KEY `idx_TaxRate_TaxRate_iChangeSetID` (`TaxRate_iChangeSetID`);

--
-- Indexes for table `tender`
--
ALTER TABLE `tender`
  ADD PRIMARY KEY (`IdTender`),
  ADD KEY `idx_Tender_Tender_Checksum` (`Tender_Checksum`),
  ADD KEY `idx_Tender_Tender_dModifiedDate` (`Tender_dModifiedDate`),
  ADD KEY `idx_Tender_Tender_iBranchID` (`Tender_iBranchID`),
  ADD KEY `idx_Tender_Tender_iChangeSetID` (`Tender_iChangeSetID`),
  ADD KEY `idxDesc` (`Description`);

--
-- Indexes for table `tills`
--
ALTER TABLE `tills`
  ADD PRIMARY KEY (`IdTills`),
  ADD KEY `idx_Tills_Tills_Checksum` (`Tills_Checksum`),
  ADD KEY `idx_Tills_Tills_dModifiedDate` (`Tills_dModifiedDate`),
  ADD KEY `idx_Tills_Tills_iBranchID` (`Tills_iBranchID`),
  ADD KEY `idx_Tills_Tills_iChangeSetID` (`Tills_iChangeSetID`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_index` (`user_id`),
  ADD KEY `transactions_warehouse_id_index` (`warehouse_id`);

--
-- Indexes for table `trcodes`
--
ALTER TABLE `trcodes`
  ADD PRIMARY KEY (`idTrCodes`),
  ADD KEY `idx_TrCodes_TrCodes_Checksum` (`TrCodes_Checksum`),
  ADD KEY `idx_TrCodes_TrCodes_dModifiedDate` (`TrCodes_dModifiedDate`),
  ADD KEY `idx_TrCodes_TrCodes_iBranchID` (`TrCodes_iBranchID`),
  ADD KEY `idx_TrCodes_TrCodes_iChangeSetID` (`TrCodes_iChangeSetID`);

--
-- Indexes for table `venclass`
--
ALTER TABLE `venclass`
  ADD PRIMARY KEY (`idVenClass`),
  ADD KEY `idx_VenClass_VenClass_Checksum` (`VenClass_Checksum`),
  ADD KEY `idx_VenClass_VenClass_dModifiedDate` (`VenClass_dModifiedDate`),
  ADD KEY `idx_VenClass_VenClass_iBranchID` (`VenClass_iBranchID`),
  ADD KEY `idx_VenClass_VenClass_iChangeSetID` (`VenClass_iChangeSetID`);

--
-- Indexes for table `vendef`
--
ALTER TABLE `vendef`
  ADD PRIMARY KEY (`idVenDef`),
  ADD KEY `idx_VenDef_VenDef_Checksum` (`VenDef_Checksum`),
  ADD KEY `idx_VenDef_VenDef_dModifiedDate` (`VenDef_dModifiedDate`),
  ADD KEY `idx_VenDef_VenDef_iBranchID` (`VenDef_iBranchID`),
  ADD KEY `idx_VenDef_VenDef_iChangeSetID` (`VenDef_iChangeSetID`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`DCLink`),
  ADD KEY `idx_Vendor_cIDNumber` (`cIDNumber`),
  ADD KEY `idx_Vendor_cPassportNumber` (`cPassportNumber`),
  ADD KEY `idx_VENDOR_iAreasID` (`iAreasID`),
  ADD KEY `idx_VENDOR_iClassID` (`iClassID`),
  ADD KEY `idx_Vendor_Vendor_Checksum` (`Vendor_Checksum`),
  ADD KEY `idx_Vendor_Vendor_dModifiedDate` (`Vendor_dModifiedDate`),
  ADD KEY `idx_Vendor_Vendor_iBranchID` (`Vendor_iBranchID`),
  ADD KEY `idx_Vendor_Vendor_iChangeSetID` (`Vendor_iChangeSetID`),
  ADD KEY `idxAccount` (`Account`),
  ADD KEY `idxName` (`Name`);

--
-- Indexes for table `whsemst`
--
ALTER TABLE `whsemst`
  ADD PRIMARY KEY (`WhseLink`),
  ADD KEY `idx_WhseMst_WhseMst_Checksum` (`WhseMst_Checksum`),
  ADD KEY `idx_WhseMst_WhseMst_dModifiedDate` (`WhseMst_dModifiedDate`),
  ADD KEY `idx_WhseMst_WhseMst_iBranchID` (`WhseMst_iBranchID`),
  ADD KEY `idx_WhseMst_WhseMst_iChangeSetID` (`WhseMst_iChangeSetID`);

--
-- Indexes for table `whsestk`
--
ALTER TABLE `whsestk`
  ADD PRIMARY KEY (`IdWhseStk`),
  ADD KEY `idx_WhseStk_WhseStk_Checksum` (`WhseStk_Checksum`),
  ADD KEY `idx_WhseStk_WhseStk_dModifiedDate` (`WhseStk_dModifiedDate`),
  ADD KEY `idx_WhseStk_WhseStk_iBranchID` (`WhseStk_iBranchID`),
  ADD KEY `idx_WhseStk_WhseStk_iChangeSetID` (`WhseStk_iChangeSetID`),
  ADD KEY `idxWhseStk_StockLink` (`WHStockLink`),
  ADD KEY `idxWhseStk_WhseID` (`WHWhseID`),
  ADD KEY `idxWhseStock` (`WHWhseID`,`WHStockLink`);

--
-- Indexes for table `wht_batch`
--
ALTER TABLE `wht_batch`
  ADD PRIMARY KEY (`idBatch`),
  ADD KEY `idx_WHT_Batch_WHT_Batch_iBranchID` (`WHT_Batch_iBranchID`),
  ADD KEY `idx_WHT_Batch_WHT_Batch_dModifiedDate` (`WHT_Batch_dModifiedDate`),
  ADD KEY `idx_WHT_Batch_WHT_Batch_iChangeSetID` (`WHT_Batch_iChangeSetID`),
  ADD KEY `idx_WHT_Batch_WHT_Batch_Checksum` (`WHT_Batch_Checksum`);

--
-- Indexes for table `wht_batchstatus`
--
ALTER TABLE `wht_batchstatus`
  ADD PRIMARY KEY (`idBatchNumber`),
  ADD KEY `idx_WHT_BatchStatus_WHT_BatchStatus_iBranchID` (`WHT_BatchStatus_iBranchID`),
  ADD KEY `idx_WHT_BatchStatus_WHT_BatchStatus_dModifiedDate` (`WHT_BatchStatus_dModifiedDate`),
  ADD KEY `idx_WHT_BatchStatus_WHT_BatchStatus_iChangeSetID` (`WHT_BatchStatus_iChangeSetID`),
  ADD KEY `idx_WHT_BatchStatus_WHT_BatchStatus_Checksum` (`WHT_BatchStatus_Checksum`);

--
-- Indexes for table `wht_defaultsettings`
--
ALTER TABLE `wht_defaultsettings`
  ADD KEY `idx_WHT_DefaultSettings_WHT_DefaultSettings_iBranchID` (`WHT_DefaultSettings_iBranchID`),
  ADD KEY `idx_WHT_DefaultSettings_WHT_DefaultSettings_dModifiedDate` (`WHT_DefaultSettings_dModifiedDate`),
  ADD KEY `idx_WHT_DefaultSettings_WHT_DefaultSettings_iChangeSetID` (`WHT_DefaultSettings_iChangeSetID`),
  ADD KEY `idx_WHT_DefaultSettings_WHT_DefaultSettings_Checksum` (`WHT_DefaultSettings_Checksum`);

--
-- Indexes for table `wht_state`
--
ALTER TABLE `wht_state`
  ADD KEY `idx_WHT_State_WHT_State_iBranchID` (`WHT_State_iBranchID`),
  ADD KEY `idx_WHT_State_WHT_State_dModifiedDate` (`WHT_State_dModifiedDate`),
  ADD KEY `idx_WHT_State_WHT_State_iChangeSetID` (`WHT_State_iChangeSetID`),
  ADD KEY `idx_WHT_State_WHT_State_Checksum` (`WHT_State_Checksum`);

--
-- Indexes for table `wht_taxmaster`
--
ALTER TABLE `wht_taxmaster`
  ADD PRIMARY KEY (`idTaxMaster`),
  ADD KEY `idx_WHT_TaxMaster_WHT_TaxMaster_iBranchID` (`WHT_TaxMaster_iBranchID`),
  ADD KEY `idx_WHT_TaxMaster_WHT_TaxMaster_dModifiedDate` (`WHT_TaxMaster_dModifiedDate`),
  ADD KEY `idx_WHT_TaxMaster_WHT_TaxMaster_iChangeSetID` (`WHT_TaxMaster_iChangeSetID`),
  ADD KEY `idx_WHT_TaxMaster_WHT_TaxMaster_Checksum` (`WHT_TaxMaster_Checksum`);

--
-- Indexes for table `wht_userdetails`
--
ALTER TABLE `wht_userdetails`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `idx_WHT_UserDetails_WHT_UserDetails_iBranchID` (`WHT_UserDetails_iBranchID`),
  ADD KEY `idx_WHT_UserDetails_WHT_UserDetails_dModifiedDate` (`WHT_UserDetails_dModifiedDate`),
  ADD KEY `idx_WHT_UserDetails_WHT_UserDetails_iChangeSetID` (`WHT_UserDetails_iChangeSetID`),
  ADD KEY `idx_WHT_UserDetails_WHT_UserDetails_Checksum` (`WHT_UserDetails_Checksum`);

--
-- Indexes for table `_atblbulkemailaccounts`
--
ALTER TABLE `_atblbulkemailaccounts`
  ADD PRIMARY KEY (`idBulkEmailAccount`);

--
-- Indexes for table `_atblbulkemailfilters`
--
ALTER TABLE `_atblbulkemailfilters`
  ADD PRIMARY KEY (`idBulkEmailFilter`);

--
-- Indexes for table `_atblbulkemailhistory`
--
ALTER TABLE `_atblbulkemailhistory`
  ADD PRIMARY KEY (`idBulkEmailHistory`);

--
-- Indexes for table `_atblbulkemailhistorydocuments`
--
ALTER TABLE `_atblbulkemailhistorydocuments`
  ADD PRIMARY KEY (`idBulkEmailHistoryDocument`);

--
-- Indexes for table `_atblbulkemailtemplatedata`
--
ALTER TABLE `_atblbulkemailtemplatedata`
  ADD PRIMARY KEY (`idBulkEmailTemplateData`);

--
-- Indexes for table `_atblbulkemailtemplates`
--
ALTER TABLE `_atblbulkemailtemplates`
  ADD PRIMARY KEY (`idBulkEmailTemplate`);

--
-- Indexes for table `_atblbulkemailudffilters`
--
ALTER TABLE `_atblbulkemailudffilters`
  ADD PRIMARY KEY (`idBulkEmailUDFFilter`);

--
-- Indexes for table `_atblcolumnlookups`
--
ALTER TABLE `_atblcolumnlookups`
  ADD PRIMARY KEY (`idColumnLookup`);

--
-- Indexes for table `_atblcolumnlookupvalues`
--
ALTER TABLE `_atblcolumnlookupvalues`
  ADD PRIMARY KEY (`idColumnLookupValue`);

--
-- Indexes for table `_atblcolumns`
--
ALTER TABLE `_atblcolumns`
  ADD PRIMARY KEY (`idColumn`);

--
-- Indexes for table `_atbldocdefaults`
--
ALTER TABLE `_atbldocdefaults`
  ADD PRIMARY KEY (`idDocDefault`);

--
-- Indexes for table `_atbldocimportdocumenttemplates`
--
ALTER TABLE `_atbldocimportdocumenttemplates`
  ADD PRIMARY KEY (`idDocImportDocumentTemplate`);

--
-- Indexes for table `_atbldocimportdocumenttemplatetables`
--
ALTER TABLE `_atbldocimportdocumenttemplatetables`
  ADD PRIMARY KEY (`idDocumentTemplateTable`);

--
-- Indexes for table `_atbldocimportdocumenttypes`
--
ALTER TABLE `_atbldocimportdocumenttypes`
  ADD PRIMARY KEY (`idDocImportDocumentTypes`);

--
-- Indexes for table `_atbldocimportfieldmappings`
--
ALTER TABLE `_atbldocimportfieldmappings`
  ADD PRIMARY KEY (`idDocImportFieldMapping`);

--
-- Indexes for table `_atblemailaccounts`
--
ALTER TABLE `_atblemailaccounts`
  ADD PRIMARY KEY (`idEmailAccount`);

--
-- Indexes for table `_atblexportdefaults`
--
ALTER TABLE `_atblexportdefaults`
  ADD PRIMARY KEY (`idExportDefault`);

--
-- Indexes for table `_atblexportfieldmappings`
--
ALTER TABLE `_atblexportfieldmappings`
  ADD PRIMARY KEY (`idExportFieldMapping`);

--
-- Indexes for table `_atblexporttemplates`
--
ALTER TABLE `_atblexporttemplates`
  ADD PRIMARY KEY (`idExportTemplate`);

--
-- Indexes for table `_atbltablerelationships`
--
ALTER TABLE `_atbltablerelationships`
  ADD PRIMARY KEY (`idTableRelationship`);

--
-- Indexes for table `_atbltables`
--
ALTER TABLE `_atbltables`
  ADD PRIMARY KEY (`idTable`);

--
-- Indexes for table `_btblagentoptions`
--
ALTER TABLE `_btblagentoptions`
  ADD PRIMARY KEY (`IDAgentOptions`),
  ADD KEY `idx__btblAgentOptions__btblAgentOptions_Checksum` (`_btblAgentOptions_Checksum`),
  ADD KEY `idx__btblAgentOptions__btblAgentOptions_dModifiedDate` (`_btblAgentOptions_dModifiedDate`),
  ADD KEY `idx__btblAgentOptions__btblAgentOptions_iBranchID` (`_btblAgentOptions_iBranchID`),
  ADD KEY `idx__btblAgentOptions__btblAgentOptions_iChangeSetID` (`_btblAgentOptions_iChangeSetID`);

--
-- Indexes for table `_btblagentoutoffice`
--
ALTER TABLE `_btblagentoutoffice`
  ADD PRIMARY KEY (`IDAgentOutOffice`),
  ADD KEY `idx__btblAgentOutOffice__btblAgentOutOffice_Checksum` (`_btblAgentOutOffice_Checksum`),
  ADD KEY `idx__btblAgentOutOffice__btblAgentOutOffice_dModifiedDate` (`_btblAgentOutOffice_dModifiedDate`),
  ADD KEY `idx__btblAgentOutOffice__btblAgentOutOffice_iBranchID` (`_btblAgentOutOffice_iBranchID`),
  ADD KEY `idx__btblAgentOutOffice__btblAgentOutOffice_iChangeSetID` (`_btblAgentOutOffice_iChangeSetID`);

--
-- Indexes for table `_btblagentoutofficereasons`
--
ALTER TABLE `_btblagentoutofficereasons`
  ADD PRIMARY KEY (`iAgentOutOfficeID`),
  ADD KEY `idx__btblAgentOutOfficeReasons__btblAgentOutOfficeReasons_Chec39` (`_btblAgentOutOfficeReasons_Checksum`),
  ADD KEY `idx__btblAgentOutOfficeReasons__btblAgentOutOfficeReasons_dMod40` (`_btblAgentOutOfficeReasons_dModifiedDate`),
  ADD KEY `idx__btblAgentOutOfficeReasons__btblAgentOutOfficeReasons_iBra41` (`_btblAgentOutOfficeReasons_iBranchID`),
  ADD KEY `idx__btblAgentOutOfficeReasons__btblAgentOutOfficeReasons_iCha42` (`_btblAgentOutOfficeReasons_iChangeSetID`);

--
-- Indexes for table `_btblagentsystemfunctions`
--
ALTER TABLE `_btblagentsystemfunctions`
  ADD PRIMARY KEY (`iAgentID`,`cAgentType`,`iSystemFunctionID`),
  ADD KEY `idx__btblAgentSystemFunctions__btblAgentSystemFunctions_Checksum` (`_btblAgentSystemFunctions_Checksum`),
  ADD KEY `idx__btblAgentSystemFunctions__btblAgentSystemFunctions_dModif43` (`_btblAgentSystemFunctions_dModifiedDate`),
  ADD KEY `idx__btblAgentSystemFunctions__btblAgentSystemFunctions_iBranc44` (`_btblAgentSystemFunctions_iBranchID`),
  ADD KEY `idx__btblAgentSystemFunctions__btblAgentSystemFunctions_iChang45` (`_btblAgentSystemFunctions_iChangeSetID`);

--
-- Indexes for table `_btblagentsystemtree`
--
ALTER TABLE `_btblagentsystemtree`
  ADD PRIMARY KEY (`idAgentSystemTree`),
  ADD KEY `IDX__btblAgentSystemTree` (`iAgentID`,`cAgentType`,`iSystemTreeID`,`iParentAgentSystemTreeID`),
  ADD KEY `idx__btblAgentSystemTree__btblAgentSystemTree_Checksum` (`_btblAgentSystemTree_Checksum`),
  ADD KEY `idx__btblAgentSystemTree__btblAgentSystemTree_dModifiedDate` (`_btblAgentSystemTree_dModifiedDate`),
  ADD KEY `idx__btblAgentSystemTree__btblAgentSystemTree_iBranchID` (`_btblAgentSystemTree_iBranchID`),
  ADD KEY `idx__btblAgentSystemTree__btblAgentSystemTree_iChangeSetID` (`_btblAgentSystemTree_iChangeSetID`);

--
-- Indexes for table `_btblages`
--
ALTER TABLE `_btblages`
  ADD PRIMARY KEY (`idAges`),
  ADD KEY `idx__btblAges__btblAges_Checksum` (`_btblAges_Checksum`),
  ADD KEY `idx__btblAges__btblAges_dModifiedDate` (`_btblAges_dModifiedDate`),
  ADD KEY `idx__btblAges__btblAges_iBranchID` (`_btblAges_iBranchID`),
  ADD KEY `idx__btblAges__btblAges_iChangeSetID` (`_btblAges_iChangeSetID`);

--
-- Indexes for table `_btblbatchcheckout`
--
ALTER TABLE `_btblbatchcheckout`
  ADD PRIMARY KEY (`idBatchCheckout`),
  ADD KEY `idx__btblBatchCheckout__btblBatchCheckout_Checksum` (`_btblBatchCheckout_Checksum`),
  ADD KEY `idx__btblBatchCheckout__btblBatchCheckout_dModifiedDate` (`_btblBatchCheckout_dModifiedDate`),
  ADD KEY `idx__btblBatchCheckout__btblBatchCheckout_iBranchID` (`_btblBatchCheckout_iBranchID`),
  ADD KEY `idx__btblBatchCheckout__btblBatchCheckout_iChangeSetID` (`_btblBatchCheckout_iChangeSetID`);

--
-- Indexes for table `_btblbinlocation`
--
ALTER TABLE `_btblbinlocation`
  ADD PRIMARY KEY (`idBinLocation`),
  ADD KEY `idx__btblBINLocation__btblBINLocation_Checksum` (`_btblBINLocation_Checksum`),
  ADD KEY `idx__btblBINLocation__btblBINLocation_dModifiedDate` (`_btblBINLocation_dModifiedDate`),
  ADD KEY `idx__btblBINLocation__btblBINLocation_iBranchID` (`_btblBINLocation_iBranchID`),
  ADD KEY `idx__btblBINLocation__btblBINLocation_iChangeSetID` (`_btblBINLocation_iChangeSetID`);

--
-- Indexes for table `_btblcbbankimportdefaults`
--
ALTER TABLE `_btblcbbankimportdefaults`
  ADD PRIMARY KEY (`idCBBankImportDefaults`),
  ADD KEY `idx__btblCBBankImportDefaults__btblCBBankImportDefaults_Checksum` (`_btblCBBankImportDefaults_Checksum`),
  ADD KEY `idx__btblCBBankImportDefaults__btblCBBankImportDefaults_dModif55` (`_btblCBBankImportDefaults_dModifiedDate`),
  ADD KEY `idx__btblCBBankImportDefaults__btblCBBankImportDefaults_iBranc56` (`_btblCBBankImportDefaults_iBranchID`),
  ADD KEY `idx__btblCBBankImportDefaults__btblCBBankImportDefaults_iChang57` (`_btblCBBankImportDefaults_iChangeSetID`);

--
-- Indexes for table `_btblcbbatchdefs`
--
ALTER TABLE `_btblcbbatchdefs`
  ADD PRIMARY KEY (`idBatchDefs`),
  ADD KEY `idx__btblCbBatchDefs__btblCbBatchDefs_Checksum` (`_btblCbBatchDefs_Checksum`),
  ADD KEY `idx__btblCbBatchDefs__btblCbBatchDefs_dModifiedDate` (`_btblCbBatchDefs_dModifiedDate`),
  ADD KEY `idx__btblCbBatchDefs__btblCbBatchDefs_iBranchID` (`_btblCbBatchDefs_iBranchID`),
  ADD KEY `idx__btblCbBatchDefs__btblCbBatchDefs_iChangeSetID` (`_btblCbBatchDefs_iChangeSetID`);

--
-- Indexes for table `_btblcbbatches`
--
ALTER TABLE `_btblcbbatches`
  ADD PRIMARY KEY (`idBatches`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_Checksum` (`_btblCbBatches_Checksum`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_dModifiedDate` (`_btblCbBatches_dModifiedDate`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_iBranchID` (`_btblCbBatches_iBranchID`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_iChangeSetID` (`_btblCbBatches_iChangeSetID`);

--
-- Indexes for table `_btblcbbatchlines`
--
ALTER TABLE `_btblcbbatchlines`
  ADD PRIMARY KEY (`idBatchLines`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_Checksum` (`_btblCbBatchLines_Checksum`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_dModifiedDate` (`_btblCbBatchLines_dModifiedDate`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_iBranchID` (`_btblCbBatchLines_iBranchID`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_iChangeSetID` (`_btblCbBatchLines_iChangeSetID`);

--
-- Indexes for table `_btblcbimporthistory`
--
ALTER TABLE `_btblcbimporthistory`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `_btblcbmatchrules`
--
ALTER TABLE `_btblcbmatchrules`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `_btblcbstatement`
--
ALTER TABLE `_btblcbstatement`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `_btblcmevent`
--
ALTER TABLE `_btblcmevent`
  ADD PRIMARY KEY (`idEvent`),
  ADD KEY `idx__btblCMEvent__btblCMEvent_Checksum` (`_btblCMEvent_Checksum`),
  ADD KEY `idx__btblCMEvent__btblCMEvent_dModifiedDate` (`_btblCMEvent_dModifiedDate`),
  ADD KEY `idx__btblCMEvent__btblCMEvent_iBranchID` (`_btblCMEvent_iBranchID`),
  ADD KEY `idx__btblCMEvent__btblCMEvent_iChangeSetID` (`_btblCMEvent_iChangeSetID`),
  ADD KEY `IDX__btblCMEvent_iIncidentID` (`iIncidentID`);

--
-- Indexes for table `_btblcmeventattendees`
--
ALTER TABLE `_btblcmeventattendees`
  ADD PRIMARY KEY (`idCMEventAttendees`),
  ADD UNIQUE KEY `IDX__btblCMEventAttendees` (`iEventID`,`iAttendeeID`,`cAttendeeType`),
  ADD KEY `idx__btblCMEventAttendees__btblCMEventAttendees_Checksum` (`_btblCMEventAttendees_Checksum`),
  ADD KEY `idx__btblCMEventAttendees__btblCMEventAttendees_dModifiedDate` (`_btblCMEventAttendees_dModifiedDate`),
  ADD KEY `idx__btblCMEventAttendees__btblCMEventAttendees_iBranchID` (`_btblCMEventAttendees_iBranchID`),
  ADD KEY `idx__btblCMEventAttendees__btblCMEventAttendees_iChangeSetID` (`_btblCMEventAttendees_iChangeSetID`);

--
-- Indexes for table `_btblcmworkflow`
--
ALTER TABLE `_btblcmworkflow`
  ADD PRIMARY KEY (`idWorkflow`),
  ADD KEY `idx__btblCMWorkflow__btblCMWorkflow_Checksum` (`_btblCMWorkflow_Checksum`),
  ADD KEY `idx__btblCMWorkflow__btblCMWorkflow_dModifiedDate` (`_btblCMWorkflow_dModifiedDate`),
  ADD KEY `idx__btblCMWorkflow__btblCMWorkflow_iBranchID` (`_btblCMWorkflow_iBranchID`),
  ADD KEY `idx__btblCMWorkflow__btblCMWorkflow_iChangeSetID` (`_btblCMWorkflow_iChangeSetID`);

--
-- Indexes for table `_btblcmworkflowmembers`
--
ALTER TABLE `_btblcmworkflowmembers`
  ADD PRIMARY KEY (`idWorkflowMembers`),
  ADD KEY `idx__btblCMWorkflowMembers__btblCMWorkflowMembers_Checksum` (`_btblCMWorkflowMembers_Checksum`),
  ADD KEY `idx__btblCMWorkflowMembers__btblCMWorkflowMembers_dModifiedDate` (`_btblCMWorkflowMembers_dModifiedDate`),
  ADD KEY `idx__btblCMWorkflowMembers__btblCMWorkflowMembers_iBranchID` (`_btblCMWorkflowMembers_iBranchID`),
  ADD KEY `idx__btblCMWorkflowMembers__btblCMWorkflowMembers_iChangeSetID` (`_btblCMWorkflowMembers_iChangeSetID`);

--
-- Indexes for table `_btblcmworkflowstatus`
--
ALTER TABLE `_btblcmworkflowstatus`
  ADD PRIMARY KEY (`idWorkflowStatus`),
  ADD KEY `idx__btblCMWorkflowStatus__btblCMWorkflowStatus_Checksum` (`_btblCMWorkflowStatus_Checksum`),
  ADD KEY `idx__btblCMWorkflowStatus__btblCMWorkflowStatus_dModifiedDate` (`_btblCMWorkflowStatus_dModifiedDate`),
  ADD KEY `idx__btblCMWorkflowStatus__btblCMWorkflowStatus_iBranchID` (`_btblCMWorkflowStatus_iBranchID`),
  ADD KEY `idx__btblCMWorkflowStatus__btblCMWorkflowStatus_iChangeSetID` (`_btblCMWorkflowStatus_iChangeSetID`);

--
-- Indexes for table `_btblfaasset`
--
ALTER TABLE `_btblfaasset`
  ADD PRIMARY KEY (`idAssetNo`),
  ADD KEY `idx__btblFAAsset__btblFAAsset_Checksum` (`_btblFAAsset_Checksum`),
  ADD KEY `idx__btblFAAsset__btblFAAsset_dModifiedDate` (`_btblFAAsset_dModifiedDate`),
  ADD KEY `idx__btblFAAsset__btblFAAsset_iBranchID` (`_btblFAAsset_iBranchID`),
  ADD KEY `idx__btblFAAsset__btblFAAsset_iChangeSetID` (`_btblFAAsset_iChangeSetID`),
  ADD KEY `idx_assetcode` (`cAssetCode`);

--
-- Indexes for table `_btblfaassetblock`
--
ALTER TABLE `_btblfaassetblock`
  ADD PRIMARY KEY (`idAssetBlockNo`),
  ADD KEY `idx__btblFAAssetBlock__btblFAAssetBlock_Checksum` (`_btblFAAssetBlock_Checksum`),
  ADD KEY `idx__btblFAAssetBlock__btblFAAssetBlock_dModifiedDate` (`_btblFAAssetBlock_dModifiedDate`),
  ADD KEY `idx__btblFAAssetBlock__btblFAAssetBlock_iBranchID` (`_btblFAAssetBlock_iBranchID`),
  ADD KEY `idx__btblFAAssetBlock__btblFAAssetBlock_iChangeSetID` (`_btblFAAssetBlock_iChangeSetID`);

--
-- Indexes for table `_btblfaassetimages`
--
ALTER TABLE `_btblfaassetimages`
  ADD PRIMARY KEY (`idImageNo`),
  ADD KEY `idx__btblFAAssetImages__btblFAAssetImages_Checksum` (`_btblFAAssetImages_Checksum`),
  ADD KEY `idx__btblFAAssetImages__btblFAAssetImages_dModifiedDate` (`_btblFAAssetImages_dModifiedDate`),
  ADD KEY `idx__btblFAAssetImages__btblFAAssetImages_iBranchID` (`_btblFAAssetImages_iBranchID`),
  ADD KEY `idx__btblFAAssetImages__btblFAAssetImages_iChangeSetID` (`_btblFAAssetImages_iChangeSetID`);

--
-- Indexes for table `_btblfaassetserialno`
--
ALTER TABLE `_btblfaassetserialno`
  ADD PRIMARY KEY (`idSerialNo`),
  ADD KEY `idx__btblFAAssetSerialNo__btblFAAssetSerialNo_Checksum` (`_btblFAAssetSerialNo_Checksum`),
  ADD KEY `idx__btblFAAssetSerialNo__btblFAAssetSerialNo_dModifiedDate` (`_btblFAAssetSerialNo_dModifiedDate`),
  ADD KEY `idx__btblFAAssetSerialNo__btblFAAssetSerialNo_iBranchID` (`_btblFAAssetSerialNo_iBranchID`),
  ADD KEY `idx__btblFAAssetSerialNo__btblFAAssetSerialNo_iChangeSetID` (`_btblFAAssetSerialNo_iChangeSetID`),
  ADD KEY `idx_serialno` (`cSerialNo`,`idSerialNo`,`cAssetCode`);

--
-- Indexes for table `_btblfaassettracking`
--
ALTER TABLE `_btblfaassettracking`
  ADD PRIMARY KEY (`idAssetTracking`),
  ADD KEY `idx__btblFAAssetTracking__btblFAAssetTracking_Checksum` (`_btblFAAssetTracking_Checksum`),
  ADD KEY `idx__btblFAAssetTracking__btblFAAssetTracking_dModifiedDate` (`_btblFAAssetTracking_dModifiedDate`),
  ADD KEY `idx__btblFAAssetTracking__btblFAAssetTracking_iBranchID` (`_btblFAAssetTracking_iBranchID`),
  ADD KEY `idx__btblFAAssetTracking__btblFAAssetTracking_iChangeSetID` (`_btblFAAssetTracking_iChangeSetID`);

--
-- Indexes for table `_btblfaassettrackinglines`
--
ALTER TABLE `_btblfaassettrackinglines`
  ADD PRIMARY KEY (`idAssetTrackingLines`),
  ADD KEY `idx__btblFAAssetTrackingLines__btblFAAssetTrackingLines_Checksum` (`_btblFAAssetTrackingLines_Checksum`),
  ADD KEY `idx__btblFAAssetTrackingLines__btblFAAssetTrackingLines_dModif23` (`_btblFAAssetTrackingLines_dModifiedDate`),
  ADD KEY `idx__btblFAAssetTrackingLines__btblFAAssetTrackingLines_iBranc24` (`_btblFAAssetTrackingLines_iBranchID`),
  ADD KEY `idx__btblFAAssetTrackingLines__btblFAAssetTrackingLines_iChang25` (`_btblFAAssetTrackingLines_iChangeSetID`);

--
-- Indexes for table `_btblfaassettype`
--
ALTER TABLE `_btblfaassettype`
  ADD PRIMARY KEY (`idAssetTypeNo`),
  ADD KEY `idx__btblFAAssetType__btblFAAssetType_Checksum` (`_btblFAAssetType_Checksum`),
  ADD KEY `idx__btblFAAssetType__btblFAAssetType_dModifiedDate` (`_btblFAAssetType_dModifiedDate`),
  ADD KEY `idx__btblFAAssetType__btblFAAssetType_iBranchID` (`_btblFAAssetType_iBranchID`),
  ADD KEY `idx__btblFAAssetType__btblFAAssetType_iChangeSetID` (`_btblFAAssetType_iChangeSetID`);

--
-- Indexes for table `_btblfaassetunitsofusage`
--
ALTER TABLE `_btblfaassetunitsofusage`
  ADD PRIMARY KEY (`idAssetUnitNo`),
  ADD KEY `idx__btblFAAssetUnitsOfUsage__btblFAAssetUnitsOfUsage_Checksum` (`_btblFAAssetUnitsOfUsage_Checksum`),
  ADD KEY `idx__btblFAAssetUnitsOfUsage__btblFAAssetUnitsOfUsage_dModifie37` (`_btblFAAssetUnitsOfUsage_dModifiedDate`),
  ADD KEY `idx__btblFAAssetUnitsOfUsage__btblFAAssetUnitsOfUsage_iBranchID` (`_btblFAAssetUnitsOfUsage_iBranchID`),
  ADD KEY `idx__btblFAAssetUnitsOfUsage__btblFAAssetUnitsOfUsage_iChangeS38` (`_btblFAAssetUnitsOfUsage_iChangeSetID`);

--
-- Indexes for table `_btblfacapexbudget`
--
ALTER TABLE `_btblfacapexbudget`
  ADD PRIMARY KEY (`idCapexBudgetNo`),
  ADD KEY `idx__btblFACapexBudget__btblFACapexBudget_Checksum` (`_btblFACapexBudget_Checksum`),
  ADD KEY `idx__btblFACapexBudget__btblFACapexBudget_dModifiedDate` (`_btblFACapexBudget_dModifiedDate`),
  ADD KEY `idx__btblFACapexBudget__btblFACapexBudget_iBranchID` (`_btblFACapexBudget_iBranchID`),
  ADD KEY `idx__btblFACapexBudget__btblFACapexBudget_iChangeSetID` (`_btblFACapexBudget_iChangeSetID`);

--
-- Indexes for table `_btblfacapexorder`
--
ALTER TABLE `_btblfacapexorder`
  ADD PRIMARY KEY (`idCapexOrderNo`),
  ADD KEY `idx__btblFACapexOrder__btblFACapexOrder_Checksum` (`_btblFACapexOrder_Checksum`),
  ADD KEY `idx__btblFACapexOrder__btblFACapexOrder_dModifiedDate` (`_btblFACapexOrder_dModifiedDate`),
  ADD KEY `idx__btblFACapexOrder__btblFACapexOrder_iBranchID` (`_btblFACapexOrder_iBranchID`),
  ADD KEY `idx__btblFACapexOrder__btblFACapexOrder_iChangeSetID` (`_btblFACapexOrder_iChangeSetID`);

--
-- Indexes for table `_btblfacapexphasing`
--
ALTER TABLE `_btblfacapexphasing`
  ADD PRIMARY KEY (`idCapexPhasingNo`),
  ADD KEY `idx__btblFACapexPhasing__btblFACapexPhasing_Checksum` (`_btblFACapexPhasing_Checksum`),
  ADD KEY `idx__btblFACapexPhasing__btblFACapexPhasing_dModifiedDate` (`_btblFACapexPhasing_dModifiedDate`),
  ADD KEY `idx__btblFACapexPhasing__btblFACapexPhasing_iBranchID` (`_btblFACapexPhasing_iBranchID`),
  ADD KEY `idx__btblFACapexPhasing__btblFACapexPhasing_iChangeSetID` (`_btblFACapexPhasing_iChangeSetID`);

--
-- Indexes for table `_btblfacompanysetup`
--
ALTER TABLE `_btblfacompanysetup`
  ADD PRIMARY KEY (`iFACompanyNo`),
  ADD KEY `idx__btblFACompanySetup__btblFACompanySetup_Checksum` (`_btblFACompanySetup_Checksum`),
  ADD KEY `idx__btblFACompanySetup__btblFACompanySetup_dModifiedDate` (`_btblFACompanySetup_dModifiedDate`),
  ADD KEY `idx__btblFACompanySetup__btblFACompanySetup_iBranchID` (`_btblFACompanySetup_iBranchID`),
  ADD KEY `idx__btblFACompanySetup__btblFACompanySetup_iChangeSetID` (`_btblFACompanySetup_iChangeSetID`);

--
-- Indexes for table `_btblfadepreciationmethod`
--
ALTER TABLE `_btblfadepreciationmethod`
  ADD PRIMARY KEY (`idDepreciationNo`),
  ADD KEY `idx__btblFADepreciationMethod__btblFADepreciationMethod_Checksum` (`_btblFADepreciationMethod_Checksum`),
  ADD KEY `idx__btblFADepreciationMethod__btblFADepreciationMethod_dModif46` (`_btblFADepreciationMethod_dModifiedDate`),
  ADD KEY `idx__btblFADepreciationMethod__btblFADepreciationMethod_iBranc47` (`_btblFADepreciationMethod_iBranchID`),
  ADD KEY `idx__btblFADepreciationMethod__btblFADepreciationMethod_iChang48` (`_btblFADepreciationMethod_iChangeSetID`);

--
-- Indexes for table `_btblfadepreciationyear`
--
ALTER TABLE `_btblfadepreciationyear`
  ADD PRIMARY KEY (`idDepreciationYearNo`),
  ADD KEY `idx__btblFADepreciationYear__btblFADepreciationYear_Checksum` (`_btblFADepreciationYear_Checksum`),
  ADD KEY `idx__btblFADepreciationYear__btblFADepreciationYear_dModifiedD49` (`_btblFADepreciationYear_dModifiedDate`),
  ADD KEY `idx__btblFADepreciationYear__btblFADepreciationYear_iBranchID` (`_btblFADepreciationYear_iBranchID`),
  ADD KEY `idx__btblFADepreciationYear__btblFADepreciationYear_iChangeSetID` (`_btblFADepreciationYear_iChangeSetID`);

--
-- Indexes for table `_btblfafinancemethod`
--
ALTER TABLE `_btblfafinancemethod`
  ADD PRIMARY KEY (`idFinMethod`),
  ADD KEY `idx__btblFAFinanceMethod__btblFAFinanceMethod_Checksum` (`_btblFAFinanceMethod_Checksum`),
  ADD KEY `idx__btblFAFinanceMethod__btblFAFinanceMethod_dModifiedDate` (`_btblFAFinanceMethod_dModifiedDate`),
  ADD KEY `idx__btblFAFinanceMethod__btblFAFinanceMethod_iBranchID` (`_btblFAFinanceMethod_iBranchID`),
  ADD KEY `idx__btblFAFinanceMethod__btblFAFinanceMethod_iChangeSetID` (`_btblFAFinanceMethod_iChangeSetID`);

--
-- Indexes for table `_btblfaglbatch`
--
ALTER TABLE `_btblfaglbatch`
  ADD PRIMARY KEY (`idBatch`),
  ADD KEY `idx__btblFAGLBatch__btblFAGLBatch_Checksum` (`_btblFAGLBatch_Checksum`),
  ADD KEY `idx__btblFAGLBatch__btblFAGLBatch_dModifiedDate` (`_btblFAGLBatch_dModifiedDate`),
  ADD KEY `idx__btblFAGLBatch__btblFAGLBatch_iBranchID` (`_btblFAGLBatch_iBranchID`),
  ADD KEY `idx__btblFAGLBatch__btblFAGLBatch_iChangeSetID` (`_btblFAGLBatch_iChangeSetID`);

--
-- Indexes for table `_btblfaglbatchassetvalues`
--
ALTER TABLE `_btblfaglbatchassetvalues`
  ADD PRIMARY KEY (`idBatchAssetValues`),
  ADD KEY `idx__btblFAGLBatchAssetValues__btblFAGLBatchAssetValues_Checksum` (`_btblFAGLBatchAssetValues_Checksum`),
  ADD KEY `idx__btblFAGLBatchAssetValues__btblFAGLBatchAssetValues_dModif52` (`_btblFAGLBatchAssetValues_dModifiedDate`),
  ADD KEY `idx__btblFAGLBatchAssetValues__btblFAGLBatchAssetValues_iBranc53` (`_btblFAGLBatchAssetValues_iBranchID`),
  ADD KEY `idx__btblFAGLBatchAssetValues__btblFAGLBatchAssetValues_iChang54` (`_btblFAGLBatchAssetValues_iChangeSetID`);

--
-- Indexes for table `_btblfaglbatchglentries`
--
ALTER TABLE `_btblfaglbatchglentries`
  ADD PRIMARY KEY (`idBatchGLEntries`),
  ADD KEY `idx__btblFAGLBatchGLEntries__btblFAGLBatchGLEntries_Checksum` (`_btblFAGLBatchGLEntries_Checksum`),
  ADD KEY `idx__btblFAGLBatchGLEntries__btblFAGLBatchGLEntries_dModifiedD58` (`_btblFAGLBatchGLEntries_dModifiedDate`),
  ADD KEY `idx__btblFAGLBatchGLEntries__btblFAGLBatchGLEntries_iBranchID` (`_btblFAGLBatchGLEntries_iBranchID`),
  ADD KEY `idx__btblFAGLBatchGLEntries__btblFAGLBatchGLEntries_iChangeSetID` (`_btblFAGLBatchGLEntries_iChangeSetID`);

--
-- Indexes for table `_btblfaglperiod`
--
ALTER TABLE `_btblfaglperiod`
  ADD PRIMARY KEY (`idGLPeriodNo`),
  ADD KEY `idx__btblFAGLPeriod__btblFAGLPeriod_Checksum` (`_btblFAGLPeriod_Checksum`),
  ADD KEY `idx__btblFAGLPeriod__btblFAGLPeriod_dModifiedDate` (`_btblFAGLPeriod_dModifiedDate`),
  ADD KEY `idx__btblFAGLPeriod__btblFAGLPeriod_iBranchID` (`_btblFAGLPeriod_iBranchID`),
  ADD KEY `idx__btblFAGLPeriod__btblFAGLPeriod_iChangeSetID` (`_btblFAGLPeriod_iChangeSetID`);

--
-- Indexes for table `_btblfagltotalassetvalues`
--
ALTER TABLE `_btblfagltotalassetvalues`
  ADD PRIMARY KEY (`idTotalAssetValues`),
  ADD KEY `idx__btblFAGLTotalAssetValues__btblFAGLTotalAssetValues_Checksum` (`_btblFAGLTotalAssetValues_Checksum`),
  ADD KEY `idx__btblFAGLTotalAssetValues__btblFAGLTotalAssetValues_dModif75` (`_btblFAGLTotalAssetValues_dModifiedDate`),
  ADD KEY `idx__btblFAGLTotalAssetValues__btblFAGLTotalAssetValues_iBranc76` (`_btblFAGLTotalAssetValues_iBranchID`),
  ADD KEY `idx__btblFAGLTotalAssetValues__btblFAGLTotalAssetValues_iChang77` (`_btblFAGLTotalAssetValues_iChangeSetID`);

--
-- Indexes for table `_btblfagltotalglentries`
--
ALTER TABLE `_btblfagltotalglentries`
  ADD PRIMARY KEY (`idTotalGLEntries`),
  ADD KEY `idx__btblFAGLTotalGLEntries__btblFAGLTotalGLEntries_Checksum` (`_btblFAGLTotalGLEntries_Checksum`),
  ADD KEY `idx__btblFAGLTotalGLEntries__btblFAGLTotalGLEntries_dModifiedD78` (`_btblFAGLTotalGLEntries_dModifiedDate`),
  ADD KEY `idx__btblFAGLTotalGLEntries__btblFAGLTotalGLEntries_iBranchID` (`_btblFAGLTotalGLEntries_iBranchID`),
  ADD KEY `idx__btblFAGLTotalGLEntries__btblFAGLTotalGLEntries_iChangeSetID` (`_btblFAGLTotalGLEntries_iChangeSetID`);

--
-- Indexes for table `_btblfalocation`
--
ALTER TABLE `_btblfalocation`
  ADD PRIMARY KEY (`idLocationNo`),
  ADD KEY `idx__btblFALocation__btblFALocation_Checksum` (`_btblFALocation_Checksum`),
  ADD KEY `idx__btblFALocation__btblFALocation_dModifiedDate` (`_btblFALocation_dModifiedDate`),
  ADD KEY `idx__btblFALocation__btblFALocation_iBranchID` (`_btblFALocation_iBranchID`),
  ADD KEY `idx__btblFALocation__btblFALocation_iChangeSetID` (`_btblFALocation_iChangeSetID`);

--
-- Indexes for table `_btblfamovementtransaction`
--
ALTER TABLE `_btblfamovementtransaction`
  ADD PRIMARY KEY (`idMovementTransactionNo`),
  ADD KEY `idx__btblFAMovementTransaction__btblFAMovementTransaction_Chec84` (`_btblFAMovementTransaction_Checksum`),
  ADD KEY `idx__btblFAMovementTransaction__btblFAMovementTransaction_dMod85` (`_btblFAMovementTransaction_dModifiedDate`),
  ADD KEY `idx__btblFAMovementTransaction__btblFAMovementTransaction_iBra86` (`_btblFAMovementTransaction_iBranchID`),
  ADD KEY `idx__btblFAMovementTransaction__btblFAMovementTransaction_iCha87` (`_btblFAMovementTransaction_iChangeSetID`);

--
-- Indexes for table `_btblfaperiodclose`
--
ALTER TABLE `_btblfaperiodclose`
  ADD KEY `idx__btblFAPeriodClose__btblFAPeriodClose_Checksum` (`_btblFAPeriodClose_Checksum`),
  ADD KEY `idx__btblFAPeriodClose__btblFAPeriodClose_dModifiedDate` (`_btblFAPeriodClose_dModifiedDate`),
  ADD KEY `idx__btblFAPeriodClose__btblFAPeriodClose_iBranchID` (`_btblFAPeriodClose_iBranchID`),
  ADD KEY `idx__btblFAPeriodClose__btblFAPeriodClose_iChangeSetID` (`_btblFAPeriodClose_iChangeSetID`);

--
-- Indexes for table `_btblfatxdefaultglaccounts`
--
ALTER TABLE `_btblfatxdefaultglaccounts`
  ADD PRIMARY KEY (`idTXDefaultGLAccount`),
  ADD KEY `idx__btblFATxDefaultGLAccounts__btblFATxDefaultGLAccounts_Chec92` (`_btblFATxDefaultGLAccounts_Checksum`),
  ADD KEY `idx__btblFATxDefaultGLAccounts__btblFATxDefaultGLAccounts_dMod93` (`_btblFATxDefaultGLAccounts_dModifiedDate`),
  ADD KEY `idx__btblFATxDefaultGLAccounts__btblFATxDefaultGLAccounts_iBra94` (`_btblFATxDefaultGLAccounts_iBranchID`),
  ADD KEY `idx__btblFATxDefaultGLAccounts__btblFATxDefaultGLAccounts_iCha95` (`_btblFATxDefaultGLAccounts_iChangeSetID`);

--
-- Indexes for table `_btblinvcount`
--
ALTER TABLE `_btblinvcount`
  ADD PRIMARY KEY (`idInvCount`),
  ADD KEY `idx__btblInvCount__btblInvCount_Checksum` (`_btblInvCount_Checksum`),
  ADD KEY `idx__btblInvCount__btblInvCount_dModifiedDate` (`_btblInvCount_dModifiedDate`),
  ADD KEY `idx__btblInvCount__btblInvCount_iBranchID` (`_btblInvCount_iBranchID`),
  ADD KEY `idx__btblInvCount__btblInvCount_iChangeSetID` (`_btblInvCount_iChangeSetID`);

--
-- Indexes for table `_btblinvcountlines`
--
ALTER TABLE `_btblinvcountlines`
  ADD PRIMARY KEY (`idInvCountLines`),
  ADD KEY `idx__btblInvCountLines` (`iInvCountID`),
  ADD KEY `idx__btblInvCountLines__btblInvCountLines_Checksum` (`_btblInvCountLines_Checksum`),
  ADD KEY `idx__btblInvCountLines__btblInvCountLines_dModifiedDate` (`_btblInvCountLines_dModifiedDate`),
  ADD KEY `idx__btblInvCountLines__btblInvCountLines_iBranchID` (`_btblInvCountLines_iBranchID`),
  ADD KEY `idx__btblInvCountLines__btblInvCountLines_iChangeSetID` (`_btblInvCountLines_iChangeSetID`);

--
-- Indexes for table `_btblinvcountlinesuom`
--
ALTER TABLE `_btblinvcountlinesuom`
  ADD PRIMARY KEY (`IDInvCountLinesUOM`),
  ADD KEY `idx__btblInvCountLinesUOM__btblInvCountLinesUOM_Checksum` (`_btblInvCountLinesUOM_Checksum`),
  ADD KEY `idx__btblInvCountLinesUOM__btblInvCountLinesUOM_dModifiedDate` (`_btblInvCountLinesUOM_dModifiedDate`),
  ADD KEY `idx__btblInvCountLinesUOM__btblInvCountLinesUOM_iBranchID` (`_btblInvCountLinesUOM_iBranchID`),
  ADD KEY `idx__btblInvCountLinesUOM__btblInvCountLinesUOM_iChangeSetID` (`_btblInvCountLinesUOM_iChangeSetID`);

--
-- Indexes for table `_btblinvoicegrvsplit`
--
ALTER TABLE `_btblinvoicegrvsplit`
  ADD PRIMARY KEY (`idInvoiceGrvSplit`),
  ADD KEY `idx__btblInvoiceGrvSplit__btblInvoiceGrvSplit_Checksum` (`_btblInvoiceGrvSplit_Checksum`),
  ADD KEY `idx__btblInvoiceGrvSplit__btblInvoiceGrvSplit_dModifiedDate` (`_btblInvoiceGrvSplit_dModifiedDate`),
  ADD KEY `idx__btblInvoiceGrvSplit__btblInvoiceGrvSplit_iBranchID` (`_btblInvoiceGrvSplit_iBranchID`),
  ADD KEY `idx__btblInvoiceGrvSplit__btblInvoiceGrvSplit_iChangeSetID` (`_btblInvoiceGrvSplit_iChangeSetID`),
  ADD KEY `idxInvoiceID` (`iGrvSplitInvoiceID`);

--
-- Indexes for table `_btblinvoicelines`
--
ALTER TABLE `_btblinvoicelines`
  ADD PRIMARY KEY (`idInvoiceLines`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_Checksum` (`_btblInvoiceLines_Checksum`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_dModifiedDate` (`_btblInvoiceLines_dModifiedDate`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_iBranchID` (`_btblInvoiceLines_iBranchID`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_iChangeSetID` (`_btblInvoiceLines_iChangeSetID`),
  ADD KEY `idx__btblInvoiceLines_iSOLinkedPOLineID` (`iSOLinkedPOLineID`),
  ADD KEY `idxInvoiceID` (`iInvoiceID`);

--
-- Indexes for table `_btblinvoicelinesn`
--
ALTER TABLE `_btblinvoicelinesn`
  ADD PRIMARY KEY (`idInvoiceLineSN`),
  ADD KEY `idx__btblInvoiceLineSN__btblInvoiceLineSN_Checksum` (`_btblInvoiceLineSN_Checksum`),
  ADD KEY `idx__btblInvoiceLineSN__btblInvoiceLineSN_dModifiedDate` (`_btblInvoiceLineSN_dModifiedDate`),
  ADD KEY `idx__btblInvoiceLineSN__btblInvoiceLineSN_iBranchID` (`_btblInvoiceLineSN_iBranchID`),
  ADD KEY `idx__btblInvoiceLineSN__btblInvoiceLineSN_iChangeSetID` (`_btblInvoiceLineSN_iChangeSetID`),
  ADD KEY `idxInvoiceID` (`iSerialInvoiceID`,`iSerialInvoiceLineID`);

--
-- Indexes for table `_btblinvoicemessages`
--
ALTER TABLE `_btblinvoicemessages`
  ADD PRIMARY KEY (`idInvoiceMessages`),
  ADD KEY `idx__btblInvoiceMessages__btblInvoiceMessages_Checksum` (`_btblInvoiceMessages_Checksum`),
  ADD KEY `idx__btblInvoiceMessages__btblInvoiceMessages_dModifiedDate` (`_btblInvoiceMessages_dModifiedDate`),
  ADD KEY `idx__btblInvoiceMessages__btblInvoiceMessages_iBranchID` (`_btblInvoiceMessages_iBranchID`),
  ADD KEY `idx__btblInvoiceMessages__btblInvoiceMessages_iChangeSetID` (`_btblInvoiceMessages_iChangeSetID`);

--
-- Indexes for table `_btbljcinvoicelines`
--
ALTER TABLE `_btbljcinvoicelines`
  ADD PRIMARY KEY (`idJCInvoiceLines`),
  ADD KEY `idx__btblJCInvoiceLines__btblJCInvoiceLines_Checksum` (`_btblJCInvoiceLines_Checksum`),
  ADD KEY `idx__btblJCInvoiceLines__btblJCInvoiceLines_dModifiedDate` (`_btblJCInvoiceLines_dModifiedDate`),
  ADD KEY `idx__btblJCInvoiceLines__btblJCInvoiceLines_iBranchID` (`_btblJCInvoiceLines_iBranchID`),
  ADD KEY `idx__btblJCInvoiceLines__btblJCInvoiceLines_iChangeSetID` (`_btblJCInvoiceLines_iChangeSetID`);

--
-- Indexes for table `_btbljcmaster`
--
ALTER TABLE `_btbljcmaster`
  ADD PRIMARY KEY (`IdJCMaster`),
  ADD KEY `idx__btblJCMaster__btblJCMaster_Checksum` (`_btblJCMaster_Checksum`),
  ADD KEY `idx__btblJCMaster__btblJCMaster_dModifiedDate` (`_btblJCMaster_dModifiedDate`),
  ADD KEY `idx__btblJCMaster__btblJCMaster_iBranchID` (`_btblJCMaster_iBranchID`),
  ADD KEY `idx__btblJCMaster__btblJCMaster_iChangeSetID` (`_btblJCMaster_iChangeSetID`),
  ADD KEY `idxJobCode` (`cJobCode`);

--
-- Indexes for table `_btbljctxlines`
--
ALTER TABLE `_btbljctxlines`
  ADD PRIMARY KEY (`idJCTxLines`),
  ADD KEY `idx__btblJCTxLines__btblJCTxLines_Checksum` (`_btblJCTxLines_Checksum`),
  ADD KEY `idx__btblJCTxLines__btblJCTxLines_dModifiedDate` (`_btblJCTxLines_dModifiedDate`),
  ADD KEY `idx__btblJCTxLines__btblJCTxLines_iBranchID` (`_btblJCTxLines_iBranchID`),
  ADD KEY `idx__btblJCTxLines__btblJCTxLines_iChangeSetID` (`_btblJCTxLines_iChangeSetID`),
  ADD KEY `idx_btblJCTxLines_iJCMasterID` (`iJCMasterID`);

--
-- Indexes for table `_btbljobfiscaltaxes`
--
ALTER TABLE `_btbljobfiscaltaxes`
  ADD PRIMARY KEY (`idInvoiceTaxes`),
  ADD KEY `idx__btblJobFiscalTaxes__btblJobFiscalTaxes_Checksum` (`_btblJobFiscalTaxes_Checksum`),
  ADD KEY `idx__btblJobFiscalTaxes__btblJobFiscalTaxes_dModifiedDate` (`_btblJobFiscalTaxes_dModifiedDate`),
  ADD KEY `idx__btblJobFiscalTaxes__btblJobFiscalTaxes_iBranchID` (`_btblJobFiscalTaxes_iBranchID`),
  ADD KEY `idx__btblJobFiscalTaxes__btblJobFiscalTaxes_iChangeSetID` (`_btblJobFiscalTaxes_iChangeSetID`);

--
-- Indexes for table `_btbljrbatchdefs`
--
ALTER TABLE `_btbljrbatchdefs`
  ADD PRIMARY KEY (`idBatchDefs`),
  ADD KEY `idx__btblJrBatchDefs__btblJrBatchDefs_Checksum` (`_btblJrBatchDefs_Checksum`),
  ADD KEY `idx__btblJrBatchDefs__btblJrBatchDefs_dModifiedDate` (`_btblJrBatchDefs_dModifiedDate`),
  ADD KEY `idx__btblJrBatchDefs__btblJrBatchDefs_iBranchID` (`_btblJrBatchDefs_iBranchID`),
  ADD KEY `idx__btblJrBatchDefs__btblJrBatchDefs_iChangeSetID` (`_btblJrBatchDefs_iChangeSetID`);

--
-- Indexes for table `_btbljrbatches`
--
ALTER TABLE `_btbljrbatches`
  ADD PRIMARY KEY (`idBatches`),
  ADD KEY `idx__btblJrBatches__btblJrBatches_Checksum` (`_btblJrBatches_Checksum`),
  ADD KEY `idx__btblJrBatches__btblJrBatches_dModifiedDate` (`_btblJrBatches_dModifiedDate`),
  ADD KEY `idx__btblJrBatches__btblJrBatches_iBranchID` (`_btblJrBatches_iBranchID`),
  ADD KEY `idx__btblJrBatches__btblJrBatches_iChangeSetID` (`_btblJrBatches_iChangeSetID`);

--
-- Indexes for table `_btbljrbatchlines`
--
ALTER TABLE `_btbljrbatchlines`
  ADD PRIMARY KEY (`idBatchLines`),
  ADD KEY `idx__btblJrBatchLines__btblJrBatchLines_Checksum` (`_btblJrBatchLines_Checksum`),
  ADD KEY `idx__btblJrBatchLines__btblJrBatchLines_dModifiedDate` (`_btblJrBatchLines_dModifiedDate`),
  ADD KEY `idx__btblJrBatchLines__btblJrBatchLines_iBranchID` (`_btblJrBatchLines_iBranchID`),
  ADD KEY `idx__btblJrBatchLines__btblJrBatchLines_iChangeSetID` (`_btblJrBatchLines_iChangeSetID`);

--
-- Indexes for table `_btbllogdetails`
--
ALTER TABLE `_btbllogdetails`
  ADD PRIMARY KEY (`idLogDetails`),
  ADD KEY `idx__btblLogDetails__btblLogDetails_Checksum` (`_btblLogDetails_Checksum`),
  ADD KEY `idx__btblLogDetails__btblLogDetails_dModifiedDate` (`_btblLogDetails_dModifiedDate`),
  ADD KEY `idx__btblLogDetails__btblLogDetails_iBranchID` (`_btblLogDetails_iBranchID`),
  ADD KEY `idx__btblLogDetails__btblLogDetails_iChangeSetID` (`_btblLogDetails_iChangeSetID`);

--
-- Indexes for table `_btbllogmaster`
--
ALTER TABLE `_btbllogmaster`
  ADD PRIMARY KEY (`iLogOrdinal`),
  ADD KEY `idx__btblLogMaster__btblLogMaster_Checksum` (`_btblLogMaster_Checksum`),
  ADD KEY `idx__btblLogMaster__btblLogMaster_dModifiedDate` (`_btblLogMaster_dModifiedDate`),
  ADD KEY `idx__btblLogMaster__btblLogMaster_iBranchID` (`_btblLogMaster_iBranchID`),
  ADD KEY `idx__btblLogMaster__btblLogMaster_iChangeSetID` (`_btblLogMaster_iChangeSetID`);

--
-- Indexes for table `_btblnotes`
--
ALTER TABLE `_btblnotes`
  ADD PRIMARY KEY (`idNotes`),
  ADD KEY `idx__btblNotes__btblNotes_Checksum` (`_btblNotes_Checksum`),
  ADD KEY `idx__btblNotes__btblNotes_dModifiedDate` (`_btblNotes_dModifiedDate`),
  ADD KEY `idx__btblNotes__btblNotes_iBranchID` (`_btblNotes_iBranchID`),
  ADD KEY `idx__btblNotes__btblNotes_iChangeSetID` (`_btblNotes_iChangeSetID`),
  ADD KEY `idxTableID` (`cNOTETBLTableID`);

--
-- Indexes for table `_btblpostendertx`
--
ALTER TABLE `_btblpostendertx`
  ADD PRIMARY KEY (`IDPOSTenderTx`),
  ADD KEY `idx__btblPOSTenderTx__btblPOSTenderTx_Checksum` (`_btblPOSTenderTx_Checksum`),
  ADD KEY `idx__btblPOSTenderTx__btblPOSTenderTx_dModifiedDate` (`_btblPOSTenderTx_dModifiedDate`),
  ADD KEY `idx__btblPOSTenderTx__btblPOSTenderTx_iBranchID` (`_btblPOSTenderTx_iBranchID`),
  ADD KEY `idx__btblPOSTenderTx__btblPOSTenderTx_iChangeSetID` (`_btblPOSTenderTx_iChangeSetID`);

--
-- Indexes for table `_btblposxztable`
--
ALTER TABLE `_btblposxztable`
  ADD PRIMARY KEY (`IDPOSXZTable`),
  ADD KEY `idx__btblPOSXZTable__btblPOSXZTable_Checksum` (`_btblPOSXZTable_Checksum`),
  ADD KEY `idx__btblPOSXZTable__btblPOSXZTable_dModifiedDate` (`_btblPOSXZTable_dModifiedDate`),
  ADD KEY `idx__btblPOSXZTable__btblPOSXZTable_iBranchID` (`_btblPOSXZTable_iBranchID`),
  ADD KEY `idx__btblPOSXZTable__btblPOSXZTable_iChangeSetID` (`_btblPOSXZTable_iChangeSetID`);

--
-- Indexes for table `_btblrbfolder`
--
ALTER TABLE `_btblrbfolder`
  ADD PRIMARY KEY (`cFolderName`,`iParentId`),
  ADD KEY `folder_idx` (`IdFolder`),
  ADD KEY `idx__btblRBFolder__btblRBFolder_Checksum` (`_btblRBFolder_Checksum`),
  ADD KEY `idx__btblRBFolder__btblRBFolder_dModifiedDate` (`_btblRBFolder_dModifiedDate`),
  ADD KEY `idx__btblRBFolder__btblRBFolder_iBranchID` (`_btblRBFolder_iBranchID`),
  ADD KEY `idx__btblRBFolder__btblRBFolder_iChangeSetID` (`_btblRBFolder_iChangeSetID`),
  ADD KEY `parent_idx` (`iParentId`);

--
-- Indexes for table `_btblrbitem`
--
ALTER TABLE `_btblrbitem`
  ADD PRIMARY KEY (`iFolderId`,`iItemType`,`cItemName`,`dModified`),
  ADD KEY `folder_item_name_idx` (`iFolderId`,`iItemType`,`cItemName`),
  ADD KEY `idx__btblRBItem__btblRBItem_Checksum` (`_btblRBItem_Checksum`),
  ADD KEY `idx__btblRBItem__btblRBItem_dModifiedDate` (`_btblRBItem_dModifiedDate`),
  ADD KEY `idx__btblRBItem__btblRBItem_iBranchID` (`_btblRBItem_iBranchID`),
  ADD KEY `idx__btblRBItem__btblRBItem_iChangeSetID` (`_btblRBItem_iChangeSetID`),
  ADD KEY `item_id_idx` (`IdItem`);

--
-- Indexes for table `_btblrbudeffield`
--
ALTER TABLE `_btblrbudeffield`
  ADD PRIMARY KEY (`cTableName`,`cFieldName`),
  ADD UNIQUE KEY `rb_table_field_name_idx` (`cTableName`,`cFieldName`),
  ADD UNIQUE KEY `rb_table_field_alias_idx` (`cTableName`,`cFieldAlias`),
  ADD KEY `idx__btblRBUDefField__btblRBUDefField_Checksum` (`_btblRBUDefField_Checksum`),
  ADD KEY `idx__btblRBUDefField__btblRBUDefField_dModifiedDate` (`_btblRBUDefField_dModifiedDate`),
  ADD KEY `idx__btblRBUDefField__btblRBUDefField_iBranchID` (`_btblRBUDefField_iBranchID`),
  ADD KEY `idx__btblRBUDefField__btblRBUDefField_iChangeSetID` (`_btblRBUDefField_iChangeSetID`);

--
-- Indexes for table `_btblserialnumberlink`
--
ALTER TABLE `_btblserialnumberlink`
  ADD PRIMARY KEY (`IDSerialNumberLink`),
  ADD KEY `idx__btblSerialNumberLink__btblSerialNumberLink_Checksum` (`_btblSerialNumberLink_Checksum`),
  ADD KEY `idx__btblSerialNumberLink__btblSerialNumberLink_dModifiedDate` (`_btblSerialNumberLink_dModifiedDate`),
  ADD KEY `idx__btblSerialNumberLink__btblSerialNumberLink_iBranchID` (`_btblSerialNumberLink_iBranchID`),
  ADD KEY `idx__btblSerialNumberLink__btblSerialNumberLink_iChangeSetID` (`_btblSerialNumberLink_iChangeSetID`),
  ADD KEY `idxSerialNumberGroupID` (`iSerialMfID`);

--
-- Indexes for table `_btblstate`
--
ALTER TABLE `_btblstate`
  ADD PRIMARY KEY (`idState`),
  ADD KEY `idx__btblState__btblState_Checksum` (`_btblState_Checksum`),
  ADD KEY `idx__btblState__btblState_dModifiedDate` (`_btblState_dModifiedDate`),
  ADD KEY `idx__btblState__btblState_iBranchID` (`_btblState_iBranchID`),
  ADD KEY `idx__btblState__btblState_iChangeSetID` (`_btblState_iChangeSetID`);

--
-- Indexes for table `_btblsystemfunction`
--
ALTER TABLE `_btblsystemfunction`
  ADD PRIMARY KEY (`idSystemFunction`),
  ADD UNIQUE KEY `gIdentifier` (`gIdentifier`),
  ADD KEY `idx__btblSystemFunction__btblSystemFunction_Checksum` (`_btblSystemFunction_Checksum`),
  ADD KEY `idx__btblSystemFunction__btblSystemFunction_dModifiedDate` (`_btblSystemFunction_dModifiedDate`),
  ADD KEY `idx__btblSystemFunction__btblSystemFunction_iBranchID` (`_btblSystemFunction_iBranchID`),
  ADD KEY `idx__btblSystemFunction__btblSystemFunction_iChangeSetID` (`_btblSystemFunction_iChangeSetID`);

--
-- Indexes for table `_btblsystemtree`
--
ALTER TABLE `_btblsystemtree`
  ADD PRIMARY KEY (`idSystemTree`),
  ADD UNIQUE KEY `gIdentifier` (`gIdentifier`),
  ADD KEY `idx__btblSystemTree__btblSystemTree_Checksum` (`_btblSystemTree_Checksum`),
  ADD KEY `idx__btblSystemTree__btblSystemTree_dModifiedDate` (`_btblSystemTree_dModifiedDate`),
  ADD KEY `idx__btblSystemTree__btblSystemTree_iBranchID` (`_btblSystemTree_iBranchID`),
  ADD KEY `idx__btblSystemTree__btblSystemTree_iChangeSetID` (`_btblSystemTree_iChangeSetID`);

--
-- Indexes for table `_btbltmcalcsheet`
--
ALTER TABLE `_btbltmcalcsheet`
  ADD PRIMARY KEY (`idCalcSheet`),
  ADD KEY `idx__btblTMCalcSheet__btblTMCalcSheet_Checksum` (`_btblTMCalcSheet_Checksum`),
  ADD KEY `idx__btblTMCalcSheet__btblTMCalcSheet_dModifiedDate` (`_btblTMCalcSheet_dModifiedDate`),
  ADD KEY `idx__btblTMCalcSheet__btblTMCalcSheet_iBranchID` (`_btblTMCalcSheet_iBranchID`),
  ADD KEY `idx__btblTMCalcSheet__btblTMCalcSheet_iChangeSetID` (`_btblTMCalcSheet_iChangeSetID`);

--
-- Indexes for table `_btbltmtaxbox`
--
ALTER TABLE `_btbltmtaxbox`
  ADD PRIMARY KEY (`idTaxBox`),
  ADD KEY `idx__btblTMTaxBox__btblTMTaxBox_Checksum` (`_btblTMTaxBox_Checksum`),
  ADD KEY `idx__btblTMTaxBox__btblTMTaxBox_dModifiedDate` (`_btblTMTaxBox_dModifiedDate`),
  ADD KEY `idx__btblTMTaxBox__btblTMTaxBox_iBranchID` (`_btblTMTaxBox_iBranchID`),
  ADD KEY `idx__btblTMTaxBox__btblTMTaxBox_iChangeSetID` (`_btblTMTaxBox_iChangeSetID`);

--
-- Indexes for table `_btbltmtaxperiod`
--
ALTER TABLE `_btbltmtaxperiod`
  ADD PRIMARY KEY (`idTaxPeriod`),
  ADD KEY `idx__btblTMTaxPeriod__btblTMTaxPeriod_Checksum` (`_btblTMTaxPeriod_Checksum`),
  ADD KEY `idx__btblTMTaxPeriod__btblTMTaxPeriod_dModifiedDate` (`_btblTMTaxPeriod_dModifiedDate`),
  ADD KEY `idx__btblTMTaxPeriod__btblTMTaxPeriod_iBranchID` (`_btblTMTaxPeriod_iBranchID`),
  ADD KEY `idx__btblTMTaxPeriod__btblTMTaxPeriod_iChangeSetID` (`_btblTMTaxPeriod_iChangeSetID`);

--
-- Indexes for table `_btbltmtaxtotals`
--
ALTER TABLE `_btbltmtaxtotals`
  ADD PRIMARY KEY (`idTaxTotals`),
  ADD KEY `idx__btblTMTaxTotals__btblTMTaxTotals_Checksum` (`_btblTMTaxTotals_Checksum`),
  ADD KEY `idx__btblTMTaxTotals__btblTMTaxTotals_dModifiedDate` (`_btblTMTaxTotals_dModifiedDate`),
  ADD KEY `idx__btblTMTaxTotals__btblTMTaxTotals_iBranchID` (`_btblTMTaxTotals_iBranchID`),
  ADD KEY `idx__btblTMTaxTotals__btblTMTaxTotals_iChangeSetID` (`_btblTMTaxTotals_iChangeSetID`);

--
-- Indexes for table `_etbladdinregister`
--
ALTER TABLE `_etbladdinregister`
  ADD PRIMARY KEY (`idAddIn`),
  ADD UNIQUE KEY `cAddInGuid` (`cAddInGuid`),
  ADD KEY `idx__etblAddinRegister__etblAddinRegister_Checksum` (`_etblAddinRegister_Checksum`),
  ADD KEY `idx__etblAddinRegister__etblAddinRegister_dModifiedDate` (`_etblAddinRegister_dModifiedDate`),
  ADD KEY `idx__etblAddinRegister__etblAddinRegister_iBranchID` (`_etblAddinRegister_iBranchID`),
  ADD KEY `idx__etblAddinRegister__etblAddinRegister_iChangeSetID` (`_etblAddinRegister_iChangeSetID`);

--
-- Indexes for table `_etbladditionalcharges`
--
ALTER TABLE `_etbladditionalcharges`
  ADD PRIMARY KEY (`idAdditionalCharge`),
  ADD KEY `idx__etblAdditionalCharges__etblAdditionalCharges_Checksum` (`_etblAdditionalCharges_Checksum`),
  ADD KEY `idx__etblAdditionalCharges__etblAdditionalCharges_dModifiedDate` (`_etblAdditionalCharges_dModifiedDate`),
  ADD KEY `idx__etblAdditionalCharges__etblAdditionalCharges_iBranchID` (`_etblAdditionalCharges_iBranchID`),
  ADD KEY `idx__etblAdditionalCharges__etblAdditionalCharges_iChangeSetID` (`_etblAdditionalCharges_iChangeSetID`);

--
-- Indexes for table `_etblagentdocprofiles`
--
ALTER TABLE `_etblagentdocprofiles`
  ADD PRIMARY KEY (`idAgentDocProfiles`),
  ADD KEY `idx__etblAgentDocProfiles__etblAgentDocProfiles_Checksum` (`_etblAgentDocProfiles_Checksum`),
  ADD KEY `idx__etblAgentDocProfiles__etblAgentDocProfiles_dModifiedDate` (`_etblAgentDocProfiles_dModifiedDate`),
  ADD KEY `idx__etblAgentDocProfiles__etblAgentDocProfiles_iBranchID` (`_etblAgentDocProfiles_iBranchID`),
  ADD KEY `idx__etblAgentDocProfiles__etblAgentDocProfiles_iChangeSetID` (`_etblAgentDocProfiles_iChangeSetID`),
  ADD KEY `idx__etblAgentDocProfiles_iAgentIDiDocProfileIDbActiveDocProfile` (`iAgentID`,`iDocProfileID`,`bActiveDocProfile`);

--
-- Indexes for table `_etblagentpwdhistory`
--
ALTER TABLE `_etblagentpwdhistory`
  ADD PRIMARY KEY (`iAgentID`,`cPassword`),
  ADD KEY `idx__etblAgentPwdHistory__etblAgentPwdHistory_Checksum` (`_etblAgentPwdHistory_Checksum`),
  ADD KEY `idx__etblAgentPwdHistory__etblAgentPwdHistory_dModifiedDate` (`_etblAgentPwdHistory_dModifiedDate`),
  ADD KEY `idx__etblAgentPwdHistory__etblAgentPwdHistory_iBranchID` (`_etblAgentPwdHistory_iBranchID`),
  ADD KEY `idx__etblAgentPwdHistory__etblAgentPwdHistory_iChangeSetID` (`_etblAgentPwdHistory_iChangeSetID`);

--
-- Indexes for table `_etblallocsdclinkrangetemp`
--
ALTER TABLE `_etblallocsdclinkrangetemp`
  ADD PRIMARY KEY (`DCLink`);

--
-- Indexes for table `_etblallocstemp`
--
ALTER TABLE `_etblallocstemp`
  ADD PRIMARY KEY (`idAllocsTemp`),
  ADD KEY `idx__etblAllocsTemp_iFromRecID` (`iFromRecID`),
  ADD KEY `idx__etblAllocsTemp_iToRecID` (`iToRecID`),
  ADD KEY `idxallocstemp_accountid` (`iAccountID`);

--
-- Indexes for table `_etblapshareholderlinks`
--
ALTER TABLE `_etblapshareholderlinks`
  ADD PRIMARY KEY (`idAPShareholderLinks`),
  ADD KEY `idx__etblAPShareholderLinks__etblAPShareholderLinks_Checksum` (`_etblAPShareholderLinks_Checksum`),
  ADD KEY `idx__etblAPShareholderLinks__etblAPShareholderLinks_dModifiedD16` (`_etblAPShareholderLinks_dModifiedDate`),
  ADD KEY `idx__etblAPShareholderLinks__etblAPShareholderLinks_iBranchID` (`_etblAPShareholderLinks_iBranchID`),
  ADD KEY `idx__etblAPShareholderLinks__etblAPShareholderLinks_iChangeSetID` (`_etblAPShareholderLinks_iChangeSetID`);

--
-- Indexes for table `_etblapshareholders`
--
ALTER TABLE `_etblapshareholders`
  ADD PRIMARY KEY (`idAPShareholders`),
  ADD KEY `idx__etblAPShareholders__etblAPShareholders_Checksum` (`_etblAPShareholders_Checksum`),
  ADD KEY `idx__etblAPShareholders__etblAPShareholders_dModifiedDate` (`_etblAPShareholders_dModifiedDate`),
  ADD KEY `idx__etblAPShareholders__etblAPShareholders_iBranchID` (`_etblAPShareholders_iBranchID`),
  ADD KEY `idx__etblAPShareholders__etblAPShareholders_iChangeSetID` (`_etblAPShareholders_iChangeSetID`),
  ADD KEY `idx__etblAPShareholders_cName` (`cName`);

--
-- Indexes for table `_etblarapbatchcontrasplit`
--
ALTER TABLE `_etblarapbatchcontrasplit`
  ADD PRIMARY KEY (`idARAPBatchContraSplit`),
  ADD KEY `idx__etblARAPBatchContraSplit__etblARAPBatchContraSplit_Checksum` (`_etblARAPBatchContraSplit_Checksum`),
  ADD KEY `idx__etblARAPBatchContraSplit__etblARAPBatchContraSplit_dModif28` (`_etblARAPBatchContraSplit_dModifiedDate`),
  ADD KEY `idx__etblARAPBatchContraSplit__etblARAPBatchContraSplit_iBranc29` (`_etblARAPBatchContraSplit_iBranchID`),
  ADD KEY `idx__etblARAPBatchContraSplit__etblARAPBatchContraSplit_iChang30` (`_etblARAPBatchContraSplit_iChangeSetID`);

--
-- Indexes for table `_etblarapbatchcontrasplithistory`
--
ALTER TABLE `_etblarapbatchcontrasplithistory`
  ADD PRIMARY KEY (`idARAPBatchContraSplitHistory`),
  ADD KEY `idx__etblARAPBatchContraSplitHistory__etblARAPBatchContraSplit33` (`_etblARAPBatchContraSplitHistory_Checksum`),
  ADD KEY `idx__etblARAPBatchContraSplitHistory__etblARAPBatchContraSplit34` (`_etblARAPBatchContraSplitHistory_dModifiedDate`),
  ADD KEY `idx__etblARAPBatchContraSplitHistory__etblARAPBatchContraSplit35` (`_etblARAPBatchContraSplitHistory_iBranchID`),
  ADD KEY `idx__etblARAPBatchContraSplitHistory__etblARAPBatchContraSplit36` (`_etblARAPBatchContraSplitHistory_iChangeSetID`);

--
-- Indexes for table `_etblarapbatchdefaults`
--
ALTER TABLE `_etblarapbatchdefaults`
  ADD PRIMARY KEY (`idARAPBatchDefaults`),
  ADD KEY `idx__etblARAPBatchDefaults__etblARAPBatchDefaults_Checksum` (`_etblARAPBatchDefaults_Checksum`),
  ADD KEY `idx__etblARAPBatchDefaults__etblARAPBatchDefaults_dModifiedDate` (`_etblARAPBatchDefaults_dModifiedDate`),
  ADD KEY `idx__etblARAPBatchDefaults__etblARAPBatchDefaults_iBranchID` (`_etblARAPBatchDefaults_iBranchID`),
  ADD KEY `idx__etblARAPBatchDefaults__etblARAPBatchDefaults_iChangeSetID` (`_etblARAPBatchDefaults_iChangeSetID`);

--
-- Indexes for table `_etblarapbatches`
--
ALTER TABLE `_etblarapbatches`
  ADD PRIMARY KEY (`idARAPBatches`),
  ADD KEY `idx__etblARAPBatches__etblARAPBatches_Checksum` (`_etblARAPBatches_Checksum`),
  ADD KEY `idx__etblARAPBatches__etblARAPBatches_dModifiedDate` (`_etblARAPBatches_dModifiedDate`),
  ADD KEY `idx__etblARAPBatches__etblARAPBatches_iBranchID` (`_etblARAPBatches_iBranchID`),
  ADD KEY `idx__etblARAPBatches__etblARAPBatches_iChangeSetID` (`_etblARAPBatches_iChangeSetID`);

--
-- Indexes for table `_etblarapbatchhistory`
--
ALTER TABLE `_etblarapbatchhistory`
  ADD PRIMARY KEY (`idARAPBatchHistory`),
  ADD KEY `idx__etblARAPBatchHistory__etblARAPBatchHistory_Checksum` (`_etblARAPBatchHistory_Checksum`),
  ADD KEY `idx__etblARAPBatchHistory__etblARAPBatchHistory_dModifiedDate` (`_etblARAPBatchHistory_dModifiedDate`),
  ADD KEY `idx__etblARAPBatchHistory__etblARAPBatchHistory_iBranchID` (`_etblARAPBatchHistory_iBranchID`),
  ADD KEY `idx__etblARAPBatchHistory__etblARAPBatchHistory_iChangeSetID` (`_etblARAPBatchHistory_iChangeSetID`);

--
-- Indexes for table `_etblarapbatchhistorylines`
--
ALTER TABLE `_etblarapbatchhistorylines`
  ADD PRIMARY KEY (`idARAPBatchHistoryLines`),
  ADD KEY `idx__etblARAPBatchHistoryLines__etblARAPBatchHistoryLines_Chec79` (`_etblARAPBatchHistoryLines_Checksum`),
  ADD KEY `idx__etblARAPBatchHistoryLines__etblARAPBatchHistoryLines_dMod80` (`_etblARAPBatchHistoryLines_dModifiedDate`),
  ADD KEY `idx__etblARAPBatchHistoryLines__etblARAPBatchHistoryLines_iBra81` (`_etblARAPBatchHistoryLines_iBranchID`),
  ADD KEY `idx__etblARAPBatchHistoryLines__etblARAPBatchHistoryLines_iCha82` (`_etblARAPBatchHistoryLines_iChangeSetID`);

--
-- Indexes for table `_etblarapbatchlines`
--
ALTER TABLE `_etblarapbatchlines`
  ADD PRIMARY KEY (`idARAPBatchLines`),
  ADD KEY `idx__etblARAPBatchLines__etblARAPBatchLines_Checksum` (`_etblARAPBatchLines_Checksum`),
  ADD KEY `idx__etblARAPBatchLines__etblARAPBatchLines_dModifiedDate` (`_etblARAPBatchLines_dModifiedDate`),
  ADD KEY `idx__etblARAPBatchLines__etblARAPBatchLines_iBranchID` (`_etblARAPBatchLines_iBranchID`),
  ADD KEY `idx__etblARAPBatchLines__etblARAPBatchLines_iChangeSetID` (`_etblARAPBatchLines_iChangeSetID`);

--
-- Indexes for table `_etblarstatementrun`
--
ALTER TABLE `_etblarstatementrun`
  ADD PRIMARY KEY (`idStatementRun`),
  ADD KEY `idx__etblARStatementRun__etblARStatementRun_Checksum` (`_etblARStatementRun_Checksum`),
  ADD KEY `idx__etblARStatementRun__etblARStatementRun_dModifiedDate` (`_etblARStatementRun_dModifiedDate`),
  ADD KEY `idx__etblARStatementRun__etblARStatementRun_iBranchID` (`_etblARStatementRun_iBranchID`),
  ADD KEY `idx__etblARStatementRun__etblARStatementRun_iChangeSetID` (`_etblARStatementRun_iChangeSetID`);

--
-- Indexes for table `_etblarstatements`
--
ALTER TABLE `_etblarstatements`
  ADD PRIMARY KEY (`idStatements`),
  ADD KEY `idx__etblARStatements__etblARStatements_Checksum` (`_etblARStatements_Checksum`),
  ADD KEY `idx__etblARStatements__etblARStatements_dModifiedDate` (`_etblARStatements_dModifiedDate`),
  ADD KEY `idx__etblARStatements__etblARStatements_iBranchID` (`_etblARStatements_iBranchID`),
  ADD KEY `idx__etblARStatements__etblARStatements_iChangeSetID` (`_etblARStatements_iChangeSetID`);

--
-- Indexes for table `_etblauditinglog`
--
ALTER TABLE `_etblauditinglog`
  ADD PRIMARY KEY (`idAuditingLog`);

--
-- Indexes for table `_etblautostrings`
--
ALTER TABLE `_etblautostrings`
  ADD PRIMARY KEY (`idAutoStrings`),
  ADD KEY `idx__etblAutoStrings__etblAutoStrings_Checksum` (`_etblAutoStrings_Checksum`),
  ADD KEY `idx__etblAutoStrings__etblAutoStrings_dModifiedDate` (`_etblAutoStrings_dModifiedDate`),
  ADD KEY `idx__etblAutoStrings__etblAutoStrings_iBranchID` (`_etblAutoStrings_iBranchID`),
  ADD KEY `idx__etblAutoStrings__etblAutoStrings_iChangeSetID` (`_etblAutoStrings_iChangeSetID`),
  ADD KEY `idx__etblAutoStrings_cAutoString` (`cAutoString`);

--
-- Indexes for table `_etblbankdetails`
--
ALTER TABLE `_etblbankdetails`
  ADD PRIMARY KEY (`idBankDetail`),
  ADD KEY `idx__etblBankDetails__etblBankDetails_Checksum` (`_etblBankDetails_Checksum`),
  ADD KEY `idx__etblBankDetails__etblBankDetails_dModifiedDate` (`_etblBankDetails_dModifiedDate`),
  ADD KEY `idx__etblBankDetails__etblBankDetails_iBranchID` (`_etblBankDetails_iBranchID`),
  ADD KEY `idx__etblBankDetails__etblBankDetails_iChangeSetID` (`_etblBankDetails_iChangeSetID`);

--
-- Indexes for table `_etblbatchpermissions`
--
ALTER TABLE `_etblbatchpermissions`
  ADD PRIMARY KEY (`idBatchPermissions`),
  ADD KEY `idx__etblBatchPermissions__etblBatchPermissions_Checksum` (`_etblBatchPermissions_Checksum`),
  ADD KEY `idx__etblBatchPermissions__etblBatchPermissions_dModifiedDate` (`_etblBatchPermissions_dModifiedDate`),
  ADD KEY `idx__etblBatchPermissions__etblBatchPermissions_iBranchID` (`_etblBatchPermissions_iBranchID`),
  ADD KEY `idx__etblBatchPermissions__etblBatchPermissions_iChangeSetID` (`_etblBatchPermissions_iChangeSetID`);

--
-- Indexes for table `_etblbranch`
--
ALTER TABLE `_etblbranch`
  ADD PRIMARY KEY (`idBranch`),
  ADD KEY `idx__etblBranch__etblBranch_Checksum` (`_etblBranch_Checksum`),
  ADD KEY `idx__etblBranch__etblBranch_dModifiedDate` (`_etblBranch_dModifiedDate`),
  ADD KEY `idx__etblBranch__etblBranch_iBranchID` (`_etblBranch_iBranchID`),
  ADD KEY `idx__etblBranch__etblBranch_iChangeSetID` (`_etblBranch_iChangeSetID`);

--
-- Indexes for table `_etblbudgetsprev`
--
ALTER TABLE `_etblbudgetsprev`
  ADD PRIMARY KEY (`idBudgetsPrev`),
  ADD KEY `idx__etblBudgetsPrev__etblBudgetsPrev_Checksum` (`_etblBudgetsPrev_Checksum`),
  ADD KEY `idx__etblBudgetsPrev__etblBudgetsPrev_dModifiedDate` (`_etblBudgetsPrev_dModifiedDate`),
  ADD KEY `idx__etblBudgetsPrev__etblBudgetsPrev_iBranchID` (`_etblBudgetsPrev_iBranchID`),
  ADD KEY `idx__etblBudgetsPrev__etblBudgetsPrev_iChangeSetID` (`_etblBudgetsPrev_iChangeSetID`),
  ADD KEY `idx__etblBudgetsPrev_iBudgetPrevAccountIDiBudgetPrevPeriodIDiB1` (`iBudgetPrevAccountID`,`iBudgetPrevPeriodID`,`iBudgetPrevProjectID`,`iBudgetPrevTxBranchID`);

--
-- Indexes for table `_etblcmagentcontact`
--
ALTER TABLE `_etblcmagentcontact`
  ADD PRIMARY KEY (`idContact`),
  ADD KEY `idx__etblCMAgentContact__etblCMAgentContact_Checksum` (`_etblCMAgentContact_Checksum`),
  ADD KEY `idx__etblCMAgentContact__etblCMAgentContact_dModifiedDate` (`_etblCMAgentContact_dModifiedDate`),
  ADD KEY `idx__etblCMAgentContact__etblCMAgentContact_iBranchID` (`_etblCMAgentContact_iBranchID`),
  ADD KEY `idx__etblCMAgentContact__etblCMAgentContact_iChangeSetID` (`_etblCMAgentContact_iChangeSetID`),
  ADD KEY `IDX__etblCMAgentContact_iAgentID` (`iAgentID`);

--
-- Indexes for table `_etblcmrejectreason`
--
ALTER TABLE `_etblcmrejectreason`
  ADD PRIMARY KEY (`idRejectReason`),
  ADD KEY `idx__etblCMRejectReason__etblCMRejectReason_Checksum` (`_etblCMRejectReason_Checksum`),
  ADD KEY `idx__etblCMRejectReason__etblCMRejectReason_dModifiedDate` (`_etblCMRejectReason_dModifiedDate`),
  ADD KEY `idx__etblCMRejectReason__etblCMRejectReason_iBranchID` (`_etblCMRejectReason_iBranchID`),
  ADD KEY `idx__etblCMRejectReason__etblCMRejectReason_iChangeSetID` (`_etblCMRejectReason_iChangeSetID`);

--
-- Indexes for table `_etbldashboardlayouts`
--
ALTER TABLE `_etbldashboardlayouts`
  ADD PRIMARY KEY (`idDashboardLayouts`),
  ADD KEY `idx__etblDashboardLayouts__etblDashboardLayouts_Checksum` (`_etblDashboardLayouts_Checksum`),
  ADD KEY `idx__etblDashboardLayouts__etblDashboardLayouts_dModifiedDate` (`_etblDashboardLayouts_dModifiedDate`),
  ADD KEY `idx__etblDashboardLayouts__etblDashboardLayouts_iBranchID` (`_etblDashboardLayouts_iBranchID`),
  ADD KEY `idx__etblDashboardLayouts__etblDashboardLayouts_iChangeSetID` (`_etblDashboardLayouts_iChangeSetID`);

--
-- Indexes for table `_etbldeladdress`
--
ALTER TABLE `_etbldeladdress`
  ADD PRIMARY KEY (`idDelAddress`),
  ADD KEY `idx__etblDelAddress__etblDelAddress_Checksum` (`_etblDelAddress_Checksum`),
  ADD KEY `idx__etblDelAddress__etblDelAddress_dModifiedDate` (`_etblDelAddress_dModifiedDate`),
  ADD KEY `idx__etblDelAddress__etblDelAddress_iBranchID` (`_etblDelAddress_iBranchID`),
  ADD KEY `idx__etblDelAddress__etblDelAddress_iChangeSetID` (`_etblDelAddress_iChangeSetID`),
  ADD KEY `idx_etblDelAddress_AccountID` (`iAccountID`,`iDCModule`),
  ADD KEY `idx_etblDelAddress_iDelAddressCodeID` (`iDelAddressCodeID`,`iDCModule`);

--
-- Indexes for table `_etbldeladdresscode`
--
ALTER TABLE `_etbldeladdresscode`
  ADD PRIMARY KEY (`IDDelAddressCode`),
  ADD KEY `idx__etblDelAddressCode__etblDelAddressCode_Checksum` (`_etblDelAddressCode_Checksum`),
  ADD KEY `idx__etblDelAddressCode__etblDelAddressCode_dModifiedDate` (`_etblDelAddressCode_dModifiedDate`),
  ADD KEY `idx__etblDelAddressCode__etblDelAddressCode_iBranchID` (`_etblDelAddressCode_iBranchID`),
  ADD KEY `idx__etblDelAddressCode__etblDelAddressCode_iChangeSetID` (`_etblDelAddressCode_iChangeSetID`);

--
-- Indexes for table `_etbldeleted`
--
ALTER TABLE `_etbldeleted`
  ADD PRIMARY KEY (`idDeleted`),
  ADD KEY `idx_etblDeleted_auditDate` (`_auditDate`),
  ADD KEY `idx_etblDeleted_cTableName` (`cTableName`);

--
-- Indexes for table `_etbldoccat`
--
ALTER TABLE `_etbldoccat`
  ADD PRIMARY KEY (`idDocCat`),
  ADD KEY `idx__etblDocCat__etblDocCat_Checksum` (`_etblDocCat_Checksum`),
  ADD KEY `idx__etblDocCat__etblDocCat_dModifiedDate` (`_etblDocCat_dModifiedDate`),
  ADD KEY `idx__etblDocCat__etblDocCat_iBranchID` (`_etblDocCat_iBranchID`),
  ADD KEY `idx__etblDocCat__etblDocCat_iChangeSetID` (`_etblDocCat_iChangeSetID`);

--
-- Indexes for table `_etbldoccatgroup`
--
ALTER TABLE `_etbldoccatgroup`
  ADD PRIMARY KEY (`idDocCatGroup`),
  ADD KEY `idx__etblDocCatGroup__etblDocCatGroup_Checksum` (`_etblDocCatGroup_Checksum`),
  ADD KEY `idx__etblDocCatGroup__etblDocCatGroup_dModifiedDate` (`_etblDocCatGroup_dModifiedDate`),
  ADD KEY `idx__etblDocCatGroup__etblDocCatGroup_iBranchID` (`_etblDocCatGroup_iBranchID`),
  ADD KEY `idx__etblDocCatGroup__etblDocCatGroup_iChangeSetID` (`_etblDocCatGroup_iChangeSetID`);

--
-- Indexes for table `_etbldocprofiles`
--
ALTER TABLE `_etbldocprofiles`
  ADD PRIMARY KEY (`idDocProfiles`),
  ADD KEY `idx__etblDocProfiles__etblDocProfiles_Checksum` (`_etblDocProfiles_Checksum`),
  ADD KEY `idx__etblDocProfiles__etblDocProfiles_dModifiedDate` (`_etblDocProfiles_dModifiedDate`),
  ADD KEY `idx__etblDocProfiles__etblDocProfiles_iBranchID` (`_etblDocProfiles_iBranchID`),
  ADD KEY `idx__etblDocProfiles__etblDocProfiles_iChangeSetID` (`_etblDocProfiles_iChangeSetID`),
  ADD KEY `idx__etblDocProfiles_iDocTypeiDocSubType` (`iDocType`,`iDocSubType`);

--
-- Indexes for table `_etbleftgatewaytype`
--
ALTER TABLE `_etbleftgatewaytype`
  ADD PRIMARY KEY (`GatewayID`),
  ADD KEY `idx__etblEFTGatewayType__etblEFTGatewayType_Checksum` (`_etblEFTGatewayType_Checksum`),
  ADD KEY `idx__etblEFTGatewayType__etblEFTGatewayType_dModifiedDate` (`_etblEFTGatewayType_dModifiedDate`),
  ADD KEY `idx__etblEFTGatewayType__etblEFTGatewayType_iBranchID` (`_etblEFTGatewayType_iBranchID`),
  ADD KEY `idx__etblEFTGatewayType__etblEFTGatewayType_iChangeSetID` (`_etblEFTGatewayType_iChangeSetID`);

--
-- Indexes for table `_etbleftreference`
--
ALTER TABLE `_etbleftreference`
  ADD PRIMARY KEY (`idNumber`),
  ADD KEY `idx__etblEFTReference__etblEFTReference_Checksum` (`_etblEFTReference_Checksum`),
  ADD KEY `idx__etblEFTReference__etblEFTReference_dModifiedDate` (`_etblEFTReference_dModifiedDate`),
  ADD KEY `idx__etblEFTReference__etblEFTReference_iBranchID` (`_etblEFTReference_iBranchID`),
  ADD KEY `idx__etblEFTReference__etblEFTReference_iChangeSetID` (`_etblEFTReference_iChangeSetID`);

--
-- Indexes for table `_etbleftsfilelayout`
--
ALTER TABLE `_etbleftsfilelayout`
  ADD PRIMARY KEY (`idEFTSLayout`),
  ADD KEY `idx__etblEFTSFileLayout__etblEFTSFileLayout_Checksum` (`_etblEFTSFileLayout_Checksum`),
  ADD KEY `idx__etblEFTSFileLayout__etblEFTSFileLayout_dModifiedDate` (`_etblEFTSFileLayout_dModifiedDate`),
  ADD KEY `idx__etblEFTSFileLayout__etblEFTSFileLayout_iBranchID` (`_etblEFTSFileLayout_iBranchID`),
  ADD KEY `idx__etblEFTSFileLayout__etblEFTSFileLayout_iChangeSetID` (`_etblEFTSFileLayout_iChangeSetID`);

--
-- Indexes for table `_etbleftsfilelayoutdetails`
--
ALTER TABLE `_etbleftsfilelayoutdetails`
  ADD PRIMARY KEY (`idEFTSLayoutDetails`),
  ADD KEY `idx__etblEFTSFileLayoutDetails__etblEFTSFileLayoutDetails_Chec63` (`_etblEFTSFileLayoutDetails_Checksum`),
  ADD KEY `idx__etblEFTSFileLayoutDetails__etblEFTSFileLayoutDetails_dMod64` (`_etblEFTSFileLayoutDetails_dModifiedDate`),
  ADD KEY `idx__etblEFTSFileLayoutDetails__etblEFTSFileLayoutDetails_iBra65` (`_etblEFTSFileLayoutDetails_iBranchID`),
  ADD KEY `idx__etblEFTSFileLayoutDetails__etblEFTSFileLayoutDetails_iCha66` (`_etblEFTSFileLayoutDetails_iChangeSetID`);

--
-- Indexes for table `_etbleucommodity`
--
ALTER TABLE `_etbleucommodity`
  ADD PRIMARY KEY (`IDEUCommodity`),
  ADD KEY `idx__etblEUCommodity__etblEUCommodity_Checksum` (`_etblEUCommodity_Checksum`),
  ADD KEY `idx__etblEUCommodity__etblEUCommodity_dModifiedDate` (`_etblEUCommodity_dModifiedDate`),
  ADD KEY `idx__etblEUCommodity__etblEUCommodity_iBranchID` (`_etblEUCommodity_iBranchID`),
  ADD KEY `idx__etblEUCommodity__etblEUCommodity_iChangeSetID` (`_etblEUCommodity_iChangeSetID`);

--
-- Indexes for table `_etbleucountry`
--
ALTER TABLE `_etbleucountry`
  ADD PRIMARY KEY (`IDEUCountry`),
  ADD KEY `idx__etblEUCountry__etblEUCountry_Checksum` (`_etblEUCountry_Checksum`),
  ADD KEY `idx__etblEUCountry__etblEUCountry_dModifiedDate` (`_etblEUCountry_dModifiedDate`),
  ADD KEY `idx__etblEUCountry__etblEUCountry_iBranchID` (`_etblEUCountry_iBranchID`),
  ADD KEY `idx__etblEUCountry__etblEUCountry_iChangeSetID` (`_etblEUCountry_iChangeSetID`);

--
-- Indexes for table `_etbleunotc`
--
ALTER TABLE `_etbleunotc`
  ADD PRIMARY KEY (`IDEUNoTC`),
  ADD KEY `idx__etblEUNoTC__etblEUNoTC_Checksum` (`_etblEUNoTC_Checksum`),
  ADD KEY `idx__etblEUNoTC__etblEUNoTC_dModifiedDate` (`_etblEUNoTC_dModifiedDate`),
  ADD KEY `idx__etblEUNoTC__etblEUNoTC_iBranchID` (`_etblEUNoTC_iBranchID`),
  ADD KEY `idx__etblEUNoTC__etblEUNoTC_iChangeSetID` (`_etblEUNoTC_iChangeSetID`);

--
-- Indexes for table `_etbleusupplementaryunit`
--
ALTER TABLE `_etbleusupplementaryunit`
  ADD PRIMARY KEY (`IDEUSupplementaryUnit`),
  ADD KEY `idx__etblEUSupplementaryUnit__etblEUSupplementaryUnit_Checksum` (`_etblEUSupplementaryUnit_Checksum`),
  ADD KEY `idx__etblEUSupplementaryUnit__etblEUSupplementaryUnit_dModifie88` (`_etblEUSupplementaryUnit_dModifiedDate`),
  ADD KEY `idx__etblEUSupplementaryUnit__etblEUSupplementaryUnit_iBranchID` (`_etblEUSupplementaryUnit_iBranchID`),
  ADD KEY `idx__etblEUSupplementaryUnit__etblEUSupplementaryUnit_iChangeS89` (`_etblEUSupplementaryUnit_iChangeSetID`);

--
-- Indexes for table `_etblfiscalprintermodels`
--
ALTER TABLE `_etblfiscalprintermodels`
  ADD PRIMARY KEY (`iFiscalPrinterModelsId`),
  ADD KEY `idx__etblFiscalPrinterModels__etblFiscalPrinterModels_Checksum` (`_etblFiscalPrinterModels_Checksum`),
  ADD KEY `idx__etblFiscalPrinterModels__etblFiscalPrinterModels_dModifie90` (`_etblFiscalPrinterModels_dModifiedDate`),
  ADD KEY `idx__etblFiscalPrinterModels__etblFiscalPrinterModels_iBranchID` (`_etblFiscalPrinterModels_iBranchID`),
  ADD KEY `idx__etblFiscalPrinterModels__etblFiscalPrinterModels_iChangeS91` (`_etblFiscalPrinterModels_iChangeSetID`);

--
-- Indexes for table `_etblfiscalprinters`
--
ALTER TABLE `_etblfiscalprinters`
  ADD PRIMARY KEY (`iFiscalPrinterId`),
  ADD KEY `idx__etblFiscalPrinters__etblFiscalPrinters_Checksum` (`_etblFiscalPrinters_Checksum`),
  ADD KEY `idx__etblFiscalPrinters__etblFiscalPrinters_dModifiedDate` (`_etblFiscalPrinters_dModifiedDate`),
  ADD KEY `idx__etblFiscalPrinters__etblFiscalPrinters_iBranchID` (`_etblFiscalPrinters_iBranchID`),
  ADD KEY `idx__etblFiscalPrinters__etblFiscalPrinters_iChangeSetID` (`_etblFiscalPrinters_iChangeSetID`);

--
-- Indexes for table `_etblglaccounttypes`
--
ALTER TABLE `_etblglaccounttypes`
  ADD PRIMARY KEY (`idGLAccountType`),
  ADD KEY `idx__etblGLAccountTypes__etblGLAccountTypes_Checksum` (`_etblGLAccountTypes_Checksum`),
  ADD KEY `idx__etblGLAccountTypes__etblGLAccountTypes_dModifiedDate` (`_etblGLAccountTypes_dModifiedDate`),
  ADD KEY `idx__etblGLAccountTypes__etblGLAccountTypes_iBranchID` (`_etblGLAccountTypes_iBranchID`),
  ADD KEY `idx__etblGLAccountTypes__etblGLAccountTypes_iChangeSetID` (`_etblGLAccountTypes_iChangeSetID`);

--
-- Indexes for table `_etblglmscoaaccounts`
--
ALTER TABLE `_etblglmscoaaccounts`
  ADD PRIMARY KEY (`idGLmSCOAAccount`),
  ADD KEY `idx__etblGLmSCOAAccounts__etblGLmSCOAAccounts_Checksum` (`_etblGLmSCOAAccounts_Checksum`),
  ADD KEY `idx__etblGLmSCOAAccounts__etblGLmSCOAAccounts_dModifiedDate` (`_etblGLmSCOAAccounts_dModifiedDate`),
  ADD KEY `idx__etblGLmSCOAAccounts__etblGLmSCOAAccounts_iBranchID` (`_etblGLmSCOAAccounts_iBranchID`),
  ADD KEY `idx__etblGLmSCOAAccounts__etblGLmSCOAAccounts_iChangeSetID` (`_etblGLmSCOAAccounts_iChangeSetID`),
  ADD KEY `idx__etblGLmSCOAAccounts_SCOAId` (`SCOAId`);

--
-- Indexes for table `_etblglreportcategory`
--
ALTER TABLE `_etblglreportcategory`
  ADD PRIMARY KEY (`idReportCategory`),
  ADD KEY `idx__etblGLReportCategory__etblGLReportCategory_Checksum` (`_etblGLReportCategory_Checksum`),
  ADD KEY `idx__etblGLReportCategory__etblGLReportCategory_dModifiedDate` (`_etblGLReportCategory_dModifiedDate`),
  ADD KEY `idx__etblGLReportCategory__etblGLReportCategory_iBranchID` (`_etblGLReportCategory_iBranchID`),
  ADD KEY `idx__etblGLReportCategory__etblGLReportCategory_iChangeSetID` (`_etblGLReportCategory_iChangeSetID`);

--
-- Indexes for table `_etblglrevisebudget`
--
ALTER TABLE `_etblglrevisebudget`
  ADD PRIMARY KEY (`idGLReviseBudget`),
  ADD KEY `idx__etblGLReviseBudget__etblGLReviseBudget_Checksum` (`_etblGLReviseBudget_Checksum`),
  ADD KEY `idx__etblGLReviseBudget__etblGLReviseBudget_dModifiedDate` (`_etblGLReviseBudget_dModifiedDate`),
  ADD KEY `idx__etblGLReviseBudget__etblGLReviseBudget_iBranchID` (`_etblGLReviseBudget_iBranchID`),
  ADD KEY `idx__etblGLReviseBudget__etblGLReviseBudget_iChangeSetID` (`_etblGLReviseBudget_iChangeSetID`),
  ADD KEY `idx_etblGLReviseBudget_iGLAccountID_iProjectID_iPeriod` (`iGLAccountID`,`iProjectID`,`iPeriod`);

--
-- Indexes for table `_etblglsegment`
--
ALTER TABLE `_etblglsegment`
  ADD PRIMARY KEY (`idSegment`),
  ADD KEY `idx__etblGLSegment__etblGLSegment_Checksum` (`_etblGLSegment_Checksum`),
  ADD KEY `idx__etblGLSegment__etblGLSegment_dModifiedDate` (`_etblGLSegment_dModifiedDate`),
  ADD KEY `idx__etblGLSegment__etblGLSegment_iBranchID` (`_etblGLSegment_iBranchID`),
  ADD KEY `idx__etblGLSegment__etblGLSegment_iChangeSetID` (`_etblGLSegment_iChangeSetID`),
  ADD KEY `idx__etblGLSegment_iSegmentNo` (`iSegmentNo`);

--
-- Indexes for table `_etblglsegmentsetup`
--
ALTER TABLE `_etblglsegmentsetup`
  ADD PRIMARY KEY (`idSegmentNo`),
  ADD KEY `idx__etblGLSegmentSetup__etblGLSegmentSetup_Checksum` (`_etblGLSegmentSetup_Checksum`),
  ADD KEY `idx__etblGLSegmentSetup__etblGLSegmentSetup_dModifiedDate` (`_etblGLSegmentSetup_dModifiedDate`),
  ADD KEY `idx__etblGLSegmentSetup__etblGLSegmentSetup_iBranchID` (`_etblGLSegmentSetup_iBranchID`),
  ADD KEY `idx__etblGLSegmentSetup__etblGLSegmentSetup_iChangeSetID` (`_etblGLSegmentSetup_iChangeSetID`);

--
-- Indexes for table `_etblinvcosttracking`
--
ALTER TABLE `_etblinvcosttracking`
  ADD PRIMARY KEY (`idCostTracking`),
  ADD KEY `idx__etblInvCostTracking_iStockIDdDateStamp` (`iStockID`,`dDateStamp`);

--
-- Indexes for table `_etblinvimages`
--
ALTER TABLE `_etblinvimages`
  ADD PRIMARY KEY (`idInvImage`),
  ADD KEY `idx__etblInvImages__etblInvImages_Checksum` (`_etblInvImages_Checksum`),
  ADD KEY `idx__etblInvImages__etblInvImages_dModifiedDate` (`_etblInvImages_dModifiedDate`),
  ADD KEY `idx__etblInvImages__etblInvImages_iBranchID` (`_etblInvImages_iBranchID`),
  ADD KEY `idx__etblInvImages__etblInvImages_iChangeSetID` (`_etblInvImages_iChangeSetID`),
  ADD KEY `idx_etblInvImages_StockLink` (`iStockLink`);

--
-- Indexes for table `_etblinvjrbatches`
--
ALTER TABLE `_etblinvjrbatches`
  ADD PRIMARY KEY (`IDInvJrBatches`),
  ADD KEY `idx__etblInvJrBatches__etblInvJrBatches_Checksum` (`_etblInvJrBatches_Checksum`),
  ADD KEY `idx__etblInvJrBatches__etblInvJrBatches_dModifiedDate` (`_etblInvJrBatches_dModifiedDate`),
  ADD KEY `idx__etblInvJrBatches__etblInvJrBatches_iBranchID` (`_etblInvJrBatches_iBranchID`),
  ADD KEY `idx__etblInvJrBatches__etblInvJrBatches_iChangeSetID` (`_etblInvJrBatches_iChangeSetID`);

--
-- Indexes for table `_etblinvjrbatchlines`
--
ALTER TABLE `_etblinvjrbatchlines`
  ADD PRIMARY KEY (`idInvJrBatchLines`,`iInvJrBatchID`),
  ADD KEY `idx__etblInvJrBatchLines__etblInvJrBatchLines_Checksum` (`_etblInvJrBatchLines_Checksum`),
  ADD KEY `idx__etblInvJrBatchLines__etblInvJrBatchLines_dModifiedDate` (`_etblInvJrBatchLines_dModifiedDate`),
  ADD KEY `idx__etblInvJrBatchLines__etblInvJrBatchLines_iBranchID` (`_etblInvJrBatchLines_iBranchID`),
  ADD KEY `idx__etblInvJrBatchLines__etblInvJrBatchLines_iChangeSetID` (`_etblInvJrBatchLines_iChangeSetID`);

--
-- Indexes for table `_etblinvjrbatchlinesn`
--
ALTER TABLE `_etblinvjrbatchlinesn`
  ADD KEY `idx__etblInvJrBatchLineSN__etblInvJrBatchLineSN_Checksum` (`_etblInvJrBatchLineSN_Checksum`),
  ADD KEY `idx__etblInvJrBatchLineSN__etblInvJrBatchLineSN_dModifiedDate` (`_etblInvJrBatchLineSN_dModifiedDate`),
  ADD KEY `idx__etblInvJrBatchLineSN__etblInvJrBatchLineSN_iBranchID` (`_etblInvJrBatchLineSN_iBranchID`),
  ADD KEY `idx__etblInvJrBatchLineSN__etblInvJrBatchLineSN_iChangeSetID` (`_etblInvJrBatchLineSN_iChangeSetID`),
  ADD KEY `idx__etblInvJrBatchLineSN_iSNGroupID` (`iInvJrBatchID`,`iSNGroupID`),
  ADD KEY `idx_etblInvJrBatchLineSN_iSNGroupID` (`iInvJrBatchID`,`iSNGroupID`);

--
-- Indexes for table `_etblinvoicedeposits`
--
ALTER TABLE `_etblinvoicedeposits`
  ADD PRIMARY KEY (`idInvoiceDeposits`),
  ADD KEY `idx__etblInvoiceDeposits__etblInvoiceDeposits_Checksum` (`_etblInvoiceDeposits_Checksum`),
  ADD KEY `idx__etblInvoiceDeposits__etblInvoiceDeposits_dModifiedDate` (`_etblInvoiceDeposits_dModifiedDate`),
  ADD KEY `idx__etblInvoiceDeposits__etblInvoiceDeposits_iBranchID` (`_etblInvoiceDeposits_iBranchID`),
  ADD KEY `idx__etblInvoiceDeposits__etblInvoiceDeposits_iChangeSetID` (`_etblInvoiceDeposits_iChangeSetID`),
  ADD KEY `idx__etblInvoiceDeposits_iInvoiceID` (`iInvoiceID`);

--
-- Indexes for table `_etblinvpriceupdatebatches`
--
ALTER TABLE `_etblinvpriceupdatebatches`
  ADD PRIMARY KEY (`idInvPriceUpdateBatches`),
  ADD KEY `idx__etblInvPriceUpdateBatches__etblInvPriceUpdateBatches_Chec2` (`_etblInvPriceUpdateBatches_Checksum`),
  ADD KEY `idx__etblInvPriceUpdateBatches__etblInvPriceUpdateBatches_dMod3` (`_etblInvPriceUpdateBatches_dModifiedDate`),
  ADD KEY `idx__etblInvPriceUpdateBatches__etblInvPriceUpdateBatches_iBra4` (`_etblInvPriceUpdateBatches_iBranchID`),
  ADD KEY `idx__etblInvPriceUpdateBatches__etblInvPriceUpdateBatches_iCha5` (`_etblInvPriceUpdateBatches_iChangeSetID`);

--
-- Indexes for table `_etblinvpriceupdatebatchlines`
--
ALTER TABLE `_etblinvpriceupdatebatchlines`
  ADD PRIMARY KEY (`idInvPriceUpdateBatchLines`),
  ADD KEY `idx__etblInvPriceUpdateBatchLines__etblInvPriceUpdateBatchLine8` (`_etblInvPriceUpdateBatchLines_Checksum`),
  ADD KEY `idx__etblInvPriceUpdateBatchLines__etblInvPriceUpdateBatchLine9` (`_etblInvPriceUpdateBatchLines_dModifiedDate`),
  ADD KEY `idx__etblInvPriceUpdateBatchLines__etblInvPriceUpdateBatchLine10` (`_etblInvPriceUpdateBatchLines_iBranchID`),
  ADD KEY `idx__etblInvPriceUpdateBatchLines__etblInvPriceUpdateBatchLine11` (`_etblInvPriceUpdateBatchLines_iChangeSetID`);

--
-- Indexes for table `_etblinvseggroup`
--
ALTER TABLE `_etblinvseggroup`
  ADD PRIMARY KEY (`idInvSegGroup`),
  ADD KEY `idx__etblInvSegGroup__etblInvSegGroup_Checksum` (`_etblInvSegGroup_Checksum`),
  ADD KEY `idx__etblInvSegGroup__etblInvSegGroup_dModifiedDate` (`_etblInvSegGroup_dModifiedDate`),
  ADD KEY `idx__etblInvSegGroup__etblInvSegGroup_iBranchID` (`_etblInvSegGroup_iBranchID`),
  ADD KEY `idx__etblInvSegGroup__etblInvSegGroup_iChangeSetID` (`_etblInvSegGroup_iChangeSetID`);

--
-- Indexes for table `_etblinvsegtype`
--
ALTER TABLE `_etblinvsegtype`
  ADD PRIMARY KEY (`idInvSegType`),
  ADD KEY `idx__etblInvSegType__etblInvSegType_Checksum` (`_etblInvSegType_Checksum`),
  ADD KEY `idx__etblInvSegType__etblInvSegType_dModifiedDate` (`_etblInvSegType_dModifiedDate`),
  ADD KEY `idx__etblInvSegType__etblInvSegType_iBranchID` (`_etblInvSegType_iBranchID`),
  ADD KEY `idx__etblInvSegType__etblInvSegType_iChangeSetID` (`_etblInvSegType_iChangeSetID`);

--
-- Indexes for table `_etblinvsegvalue`
--
ALTER TABLE `_etblinvsegvalue`
  ADD PRIMARY KEY (`idInvSegValue`),
  ADD KEY `idx__etblInvSegValue__etblInvSegValue_Checksum` (`_etblInvSegValue_Checksum`),
  ADD KEY `idx__etblInvSegValue__etblInvSegValue_dModifiedDate` (`_etblInvSegValue_dModifiedDate`),
  ADD KEY `idx__etblInvSegValue__etblInvSegValue_iBranchID` (`_etblInvSegValue_iBranchID`),
  ADD KEY `idx__etblInvSegValue__etblInvSegValue_iChangeSetID` (`_etblInvSegValue_iChangeSetID`);

--
-- Indexes for table `_etbllotstatus`
--
ALTER TABLE `_etbllotstatus`
  ADD PRIMARY KEY (`idLotStatus`),
  ADD KEY `idx__etblLotStatus__etblLotStatus_Checksum` (`_etblLotStatus_Checksum`),
  ADD KEY `idx__etblLotStatus__etblLotStatus_dModifiedDate` (`_etblLotStatus_dModifiedDate`),
  ADD KEY `idx__etblLotStatus__etblLotStatus_iBranchID` (`_etblLotStatus_iBranchID`),
  ADD KEY `idx__etblLotStatus__etblLotStatus_iChangeSetID` (`_etblLotStatus_iChangeSetID`);

--
-- Indexes for table `_etbllottracking`
--
ALTER TABLE `_etbllottracking`
  ADD PRIMARY KEY (`idLotTracking`),
  ADD KEY `idx__etblLotTracking__etblLotTracking_Checksum` (`_etblLotTracking_Checksum`),
  ADD KEY `idx__etblLotTracking__etblLotTracking_dModifiedDate` (`_etblLotTracking_dModifiedDate`),
  ADD KEY `idx__etblLotTracking__etblLotTracking_iBranchID` (`_etblLotTracking_iBranchID`),
  ADD KEY `idx__etblLotTracking__etblLotTracking_iChangeSetID` (`_etblLotTracking_iChangeSetID`);

--
-- Indexes for table `_etbllottrackingqty`
--
ALTER TABLE `_etbllottrackingqty`
  ADD PRIMARY KEY (`idLotTrackingQty`),
  ADD KEY `idx__etblLotTrackingQty__etblLotTrackingQty_Checksum` (`_etblLotTrackingQty_Checksum`),
  ADD KEY `idx__etblLotTrackingQty__etblLotTrackingQty_dModifiedDate` (`_etblLotTrackingQty_dModifiedDate`),
  ADD KEY `idx__etblLotTrackingQty__etblLotTrackingQty_iBranchID` (`_etblLotTrackingQty_iBranchID`),
  ADD KEY `idx__etblLotTrackingQty__etblLotTrackingQty_iChangeSetID` (`_etblLotTrackingQty_iChangeSetID`);

--
-- Indexes for table `_etbllottrackingtx`
--
ALTER TABLE `_etbllottrackingtx`
  ADD PRIMARY KEY (`idLotTrackingTx`),
  ADD KEY `idx__etblLotTrackingTx__etblLotTrackingTx_Checksum` (`_etblLotTrackingTx_Checksum`),
  ADD KEY `idx__etblLotTrackingTx__etblLotTrackingTx_dModifiedDate` (`_etblLotTrackingTx_dModifiedDate`),
  ADD KEY `idx__etblLotTrackingTx__etblLotTrackingTx_iBranchID` (`_etblLotTrackingTx_iBranchID`),
  ADD KEY `idx__etblLotTrackingTx__etblLotTrackingTx_iChangeSetID` (`_etblLotTrackingTx_iChangeSetID`);

--
-- Indexes for table `_etblmanufprocess`
--
ALTER TABLE `_etblmanufprocess`
  ADD PRIMARY KEY (`idManufProcess`),
  ADD KEY `idx__etblManufProcess__etblManufProcess_Checksum` (`_etblManufProcess_Checksum`),
  ADD KEY `idx__etblManufProcess__etblManufProcess_dModifiedDate` (`_etblManufProcess_dModifiedDate`),
  ADD KEY `idx__etblManufProcess__etblManufProcess_iBranchID` (`_etblManufProcess_iBranchID`),
  ADD KEY `idx__etblManufProcess__etblManufProcess_iChangeSetID` (`_etblManufProcess_iChangeSetID`),
  ADD KEY `idx_etblManufProcess_iInvNumID` (`iInvNumID`);

--
-- Indexes for table `_etblmanufprocessitem`
--
ALTER TABLE `_etblmanufprocessitem`
  ADD PRIMARY KEY (`idManufProcessItem`),
  ADD KEY `idx__etblManufProcessItem__etblManufProcessItem_Checksum` (`_etblManufProcessItem_Checksum`),
  ADD KEY `idx__etblManufProcessItem__etblManufProcessItem_dModifiedDate` (`_etblManufProcessItem_dModifiedDate`),
  ADD KEY `idx__etblManufProcessItem__etblManufProcessItem_iBranchID` (`_etblManufProcessItem_iBranchID`),
  ADD KEY `idx__etblManufProcessItem__etblManufProcessItem_iChangeSetID` (`_etblManufProcessItem_iChangeSetID`),
  ADD KEY `idx_etblManufProcessItem_iInvItemID` (`iInvItemID`),
  ADD KEY `idx_etblManufProcessItem_iManufProcessID` (`iManufProcessID`),
  ADD KEY `idx_etblManufProcessItem_iMFPItemID` (`iMFPItemID`);

--
-- Indexes for table `_etblmanufprocessline`
--
ALTER TABLE `_etblmanufprocessline`
  ADD PRIMARY KEY (`idManufProcessLine`),
  ADD KEY `idx__etblManufProcessLine__etblManufProcessLine_Checksum` (`_etblManufProcessLine_Checksum`),
  ADD KEY `idx__etblManufProcessLine__etblManufProcessLine_dModifiedDate` (`_etblManufProcessLine_dModifiedDate`),
  ADD KEY `idx__etblManufProcessLine__etblManufProcessLine_iBranchID` (`_etblManufProcessLine_iBranchID`),
  ADD KEY `idx__etblManufProcessLine__etblManufProcessLine_iChangeSetID` (`_etblManufProcessLine_iChangeSetID`),
  ADD KEY `idx_etblManufProcessLine_iManufProcessID` (`iManufProcessID`);

--
-- Indexes for table `_etblmcagentcriteria`
--
ALTER TABLE `_etblmcagentcriteria`
  ADD PRIMARY KEY (`idAgentCriteria`),
  ADD KEY `idx__etblMCAgentCriteria__etblMCAgentCriteria_Checksum` (`_etblMCAgentCriteria_Checksum`),
  ADD KEY `idx__etblMCAgentCriteria__etblMCAgentCriteria_dModifiedDate` (`_etblMCAgentCriteria_dModifiedDate`),
  ADD KEY `idx__etblMCAgentCriteria__etblMCAgentCriteria_iBranchID` (`_etblMCAgentCriteria_iBranchID`),
  ADD KEY `idx__etblMCAgentCriteria__etblMCAgentCriteria_iChangeSetID` (`_etblMCAgentCriteria_iChangeSetID`);

--
-- Indexes for table `_etblmcdefaultcriteria`
--
ALTER TABLE `_etblmcdefaultcriteria`
  ADD PRIMARY KEY (`idDefaultCriteria`),
  ADD KEY `idx__etblMCDefaultCriteria__etblMCDefaultCriteria_Checksum` (`_etblMCDefaultCriteria_Checksum`),
  ADD KEY `idx__etblMCDefaultCriteria__etblMCDefaultCriteria_dModifiedDate` (`_etblMCDefaultCriteria_dModifiedDate`),
  ADD KEY `idx__etblMCDefaultCriteria__etblMCDefaultCriteria_iBranchID` (`_etblMCDefaultCriteria_iBranchID`),
  ADD KEY `idx__etblMCDefaultCriteria__etblMCDefaultCriteria_iChangeSetID` (`_etblMCDefaultCriteria_iChangeSetID`);

--
-- Indexes for table `_etblordercancelreason`
--
ALTER TABLE `_etblordercancelreason`
  ADD PRIMARY KEY (`idOrderCancelReason`),
  ADD KEY `idx__etblOrderCancelReason__etblOrderCancelReason_Checksum` (`_etblOrderCancelReason_Checksum`),
  ADD KEY `idx__etblOrderCancelReason__etblOrderCancelReason_dModifiedDate` (`_etblOrderCancelReason_dModifiedDate`),
  ADD KEY `idx__etblOrderCancelReason__etblOrderCancelReason_iBranchID` (`_etblOrderCancelReason_iBranchID`),
  ADD KEY `idx__etblOrderCancelReason__etblOrderCancelReason_iChangeSetID` (`_etblOrderCancelReason_iChangeSetID`);

--
-- Indexes for table `_etblpaymentsbasedtax`
--
ALTER TABLE `_etblpaymentsbasedtax`
  ADD PRIMARY KEY (`idPaymentsBasedTax`),
  ADD KEY `idx__etblPaymentsBasedTax__etblPaymentsBasedTax_Checksum` (`_etblPaymentsBasedTax_Checksum`),
  ADD KEY `idx__etblPaymentsBasedTax__etblPaymentsBasedTax_dModifiedDate` (`_etblPaymentsBasedTax_dModifiedDate`),
  ADD KEY `idx__etblPaymentsBasedTax__etblPaymentsBasedTax_iBranchID` (`_etblPaymentsBasedTax_iBranchID`),
  ADD KEY `idx__etblPaymentsBasedTax__etblPaymentsBasedTax_iChangeSetID` (`_etblPaymentsBasedTax_iChangeSetID`);

--
-- Indexes for table `_etblperiod`
--
ALTER TABLE `_etblperiod`
  ADD PRIMARY KEY (`idPeriod`),
  ADD KEY `idx__etblPeriod__etblPeriod_Checksum` (`_etblPeriod_Checksum`),
  ADD KEY `idx__etblPeriod__etblPeriod_dModifiedDate` (`_etblPeriod_dModifiedDate`),
  ADD KEY `idx__etblPeriod__etblPeriod_iBranchID` (`_etblPeriod_iBranchID`),
  ADD KEY `idx__etblPeriod__etblPeriod_iChangeSetID` (`_etblPeriod_iChangeSetID`);

--
-- Indexes for table `_etblperiodyear`
--
ALTER TABLE `_etblperiodyear`
  ADD PRIMARY KEY (`idYear`),
  ADD KEY `idx__etblPeriodYear__etblPeriodYear_Checksum` (`_etblPeriodYear_Checksum`),
  ADD KEY `idx__etblPeriodYear__etblPeriodYear_dModifiedDate` (`_etblPeriodYear_dModifiedDate`),
  ADD KEY `idx__etblPeriodYear__etblPeriodYear_iBranchID` (`_etblPeriodYear_iBranchID`),
  ADD KEY `idx__etblPeriodYear__etblPeriodYear_iChangeSetID` (`_etblPeriodYear_iChangeSetID`);

--
-- Indexes for table `_etblpopdefaults`
--
ALTER TABLE `_etblpopdefaults`
  ADD PRIMARY KEY (`idPOPDefaults`),
  ADD KEY `idx__etblPOPDefaults__etblPOPDefaults_Checksum` (`_etblPOPDefaults_Checksum`),
  ADD KEY `idx__etblPOPDefaults__etblPOPDefaults_dModifiedDate` (`_etblPOPDefaults_dModifiedDate`),
  ADD KEY `idx__etblPOPDefaults__etblPOPDefaults_iBranchID` (`_etblPOPDefaults_iBranchID`),
  ADD KEY `idx__etblPOPDefaults__etblPOPDefaults_iChangeSetID` (`_etblPOPDefaults_iChangeSetID`);

--
-- Indexes for table `_etblpoprequisitionlines`
--
ALTER TABLE `_etblpoprequisitionlines`
  ADD PRIMARY KEY (`idPOPRequisitionLines`),
  ADD KEY `idx__etblPOPRequisitionLines__etblPOPRequisitionLines_Checksum` (`_etblPOPRequisitionLines_Checksum`),
  ADD KEY `idx__etblPOPRequisitionLines__etblPOPRequisitionLines_dModifie6` (`_etblPOPRequisitionLines_dModifiedDate`),
  ADD KEY `idx__etblPOPRequisitionLines__etblPOPRequisitionLines_iBranchID` (`_etblPOPRequisitionLines_iBranchID`),
  ADD KEY `idx__etblPOPRequisitionLines__etblPOPRequisitionLines_iChangeS7` (`_etblPOPRequisitionLines_iChangeSetID`),
  ADD KEY `idx__etblPOPRequisitionLines_iPOInvoiceID` (`iPOInvoiceID`),
  ADD KEY `idxRequisitionsID` (`iRequisitionID`,`iAgentID`);

--
-- Indexes for table `_etblpoprequisitionlineshist`
--
ALTER TABLE `_etblpoprequisitionlineshist`
  ADD PRIMARY KEY (`idPOPRequisitionLinesHist`),
  ADD KEY `idx__etblPOPRequisitionLinesHist__etblPOPRequisitionLinesHist_12` (`_etblPOPRequisitionLinesHist_Checksum`),
  ADD KEY `idx__etblPOPRequisitionLinesHist__etblPOPRequisitionLinesHist_13` (`_etblPOPRequisitionLinesHist_dModifiedDate`),
  ADD KEY `idx__etblPOPRequisitionLinesHist__etblPOPRequisitionLinesHist_14` (`_etblPOPRequisitionLinesHist_iBranchID`),
  ADD KEY `idx__etblPOPRequisitionLinesHist__etblPOPRequisitionLinesHist_15` (`_etblPOPRequisitionLinesHist_iChangeSetID`),
  ADD KEY `idxRequisitionHistID` (`iRequisitionHistID`,`iAgentID`),
  ADD KEY `idxRequisitionID` (`iRequisitionID`,`iAgentID`);

--
-- Indexes for table `_etblpoprequisitions`
--
ALTER TABLE `_etblpoprequisitions`
  ADD PRIMARY KEY (`idPOPRequisitions`),
  ADD KEY `idx__etblPOPRequisitions__etblPOPRequisitions_Checksum` (`_etblPOPRequisitions_Checksum`),
  ADD KEY `idx__etblPOPRequisitions__etblPOPRequisitions_dModifiedDate` (`_etblPOPRequisitions_dModifiedDate`),
  ADD KEY `idx__etblPOPRequisitions__etblPOPRequisitions_iBranchID` (`_etblPOPRequisitions_iBranchID`),
  ADD KEY `idx__etblPOPRequisitions__etblPOPRequisitions_iChangeSetID` (`_etblPOPRequisitions_iChangeSetID`);

--
-- Indexes for table `_etblpoprequisitionshist`
--
ALTER TABLE `_etblpoprequisitionshist`
  ADD PRIMARY KEY (`idPOPRequisitionsHist`),
  ADD KEY `idx__etblPOPRequisitionsHist__etblPOPRequisitionsHist_Checksum` (`_etblPOPRequisitionsHist_Checksum`),
  ADD KEY `idx__etblPOPRequisitionsHist__etblPOPRequisitionsHist_dModifie21` (`_etblPOPRequisitionsHist_dModifiedDate`),
  ADD KEY `idx__etblPOPRequisitionsHist__etblPOPRequisitionsHist_iBranchID` (`_etblPOPRequisitionsHist_iBranchID`),
  ADD KEY `idx__etblPOPRequisitionsHist__etblPOPRequisitionsHist_iChangeS22` (`_etblPOPRequisitionsHist_iChangeSetID`),
  ADD KEY `idxRequisitionID` (`iRequisitionID`,`iVersion`);

--
-- Indexes for table `_etblposdevices`
--
ALTER TABLE `_etblposdevices`
  ADD PRIMARY KEY (`idPOSDevices`),
  ADD KEY `idx__etblPOSDevices__etblPOSDevices_Checksum` (`_etblPOSDevices_Checksum`),
  ADD KEY `idx__etblPOSDevices__etblPOSDevices_dModifiedDate` (`_etblPOSDevices_dModifiedDate`),
  ADD KEY `idx__etblPOSDevices__etblPOSDevices_iBranchID` (`_etblPOSDevices_iBranchID`),
  ADD KEY `idx__etblPOSDevices__etblPOSDevices_iChangeSetID` (`_etblPOSDevices_iChangeSetID`);

--
-- Indexes for table `_etblpostdatedcheques`
--
ALTER TABLE `_etblpostdatedcheques`
  ADD PRIMARY KEY (`idPostDatedCheques`),
  ADD KEY `idx__etblPostDatedCheques__etblPostDatedCheques_Checksum` (`_etblPostDatedCheques_Checksum`),
  ADD KEY `idx__etblPostDatedCheques__etblPostDatedCheques_dModifiedDate` (`_etblPostDatedCheques_dModifiedDate`),
  ADD KEY `idx__etblPostDatedCheques__etblPostDatedCheques_iBranchID` (`_etblPostDatedCheques_iBranchID`),
  ADD KEY `idx__etblPostDatedCheques__etblPostDatedCheques_iChangeSetID` (`_etblPostDatedCheques_iChangeSetID`);

--
-- Indexes for table `_etblpostglhist`
--
ALTER TABLE `_etblpostglhist`
  ADD PRIMARY KEY (`AutoIdx`),
  ADD KEY `idx__etblPostGLHist__etblPostGLHist_Checksum` (`_etblPostGLHist_Checksum`),
  ADD KEY `idx__etblPostGLHist__etblPostGLHist_dModifiedDate` (`_etblPostGLHist_dModifiedDate`),
  ADD KEY `idx__etblPostGLHist__etblPostGLHist_iBranchID` (`_etblPostGLHist_iBranchID`),
  ADD KEY `idx__etblPostGLHist__etblPostGLHist_iChangeSetID` (`_etblPostGLHist_iChangeSetID`);

--
-- Indexes for table `_etblpostoutstandingexclap`
--
ALTER TABLE `_etblpostoutstandingexclap`
  ADD KEY `idx__etblPostOutstandingExclAP__etblPostOutstandingExclAP_Chec67` (`_etblPostOutstandingExclAP_Checksum`),
  ADD KEY `idx__etblPostOutstandingExclAP__etblPostOutstandingExclAP_dMod68` (`_etblPostOutstandingExclAP_dModifiedDate`),
  ADD KEY `idx__etblPostOutstandingExclAP__etblPostOutstandingExclAP_iBra69` (`_etblPostOutstandingExclAP_iBranchID`),
  ADD KEY `idx__etblPostOutstandingExclAP__etblPostOutstandingExclAP_iCha70` (`_etblPostOutstandingExclAP_iChangeSetID`);

--
-- Indexes for table `_etblpostoutstandingexclar`
--
ALTER TABLE `_etblpostoutstandingexclar`
  ADD KEY `idx__etblPostOutstandingExclAR__etblPostOutstandingExclAR_Chec71` (`_etblPostOutstandingExclAR_Checksum`),
  ADD KEY `idx__etblPostOutstandingExclAR__etblPostOutstandingExclAR_dMod72` (`_etblPostOutstandingExclAR_dModifiedDate`),
  ADD KEY `idx__etblPostOutstandingExclAR__etblPostOutstandingExclAR_iBra73` (`_etblPostOutstandingExclAR_iBranchID`),
  ADD KEY `idx__etblPostOutstandingExclAR__etblPostOutstandingExclAR_iCha74` (`_etblPostOutstandingExclAR_iChangeSetID`);

--
-- Indexes for table `_etblpricelistname`
--
ALTER TABLE `_etblpricelistname`
  ADD PRIMARY KEY (`IDPriceListName`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_Checksum` (`_etblPriceListName_Checksum`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_dModifiedDate` (`_etblPriceListName_dModifiedDate`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_iBranchID` (`_etblPriceListName_iBranchID`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_iChangeSetID` (`_etblPriceListName_iChangeSetID`);

--
-- Indexes for table `_etblpricelistprices`
--
ALTER TABLE `_etblpricelistprices`
  ADD PRIMARY KEY (`IDPriceListPrices`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_Checksum` (`_etblPriceListPrices_Checksum`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_dModifiedDate` (`_etblPriceListPrices_dModifiedDate`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_iBranchID` (`_etblPriceListPrices_iBranchID`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_iChangeSetID` (`_etblPriceListPrices_iChangeSetID`),
  ADD KEY `idx__etblPriceListPrices_iStockIDbUseMarkup` (`iStockID`,`bUseMarkup`),
  ADD KEY `idx_etblPriceListPrices_PriceListStockID` (`iPriceListNameID`,`iStockID`,`iWarehouseID`);

--
-- Indexes for table `_etblpromotion`
--
ALTER TABLE `_etblpromotion`
  ADD PRIMARY KEY (`iPromotionID`),
  ADD KEY `idx__etblPromotion__etblPromotion_Checksum` (`_etblPromotion_Checksum`),
  ADD KEY `idx__etblPromotion__etblPromotion_dModifiedDate` (`_etblPromotion_dModifiedDate`),
  ADD KEY `idx__etblPromotion__etblPromotion_iBranchID` (`_etblPromotion_iBranchID`),
  ADD KEY `idx__etblPromotion__etblPromotion_iChangeSetID` (`_etblPromotion_iChangeSetID`);

--
-- Indexes for table `_etblpromotionitem`
--
ALTER TABLE `_etblpromotionitem`
  ADD PRIMARY KEY (`iPromotionItemID`),
  ADD KEY `idx__etblPromotionItem__etblPromotionItem_Checksum` (`_etblPromotionItem_Checksum`),
  ADD KEY `idx__etblPromotionItem__etblPromotionItem_dModifiedDate` (`_etblPromotionItem_dModifiedDate`),
  ADD KEY `idx__etblPromotionItem__etblPromotionItem_iBranchID` (`_etblPromotionItem_iBranchID`),
  ADD KEY `idx__etblPromotionItem__etblPromotionItem_iChangeSetID` (`_etblPromotionItem_iChangeSetID`);

--
-- Indexes for table `_etblpromotionitemlist`
--
ALTER TABLE `_etblpromotionitemlist`
  ADD PRIMARY KEY (`iPromotionItemListID`),
  ADD KEY `idx__etblPromotionItemList__etblPromotionItemList_Checksum` (`_etblPromotionItemList_Checksum`),
  ADD KEY `idx__etblPromotionItemList__etblPromotionItemList_dModifiedDate` (`_etblPromotionItemList_dModifiedDate`),
  ADD KEY `idx__etblPromotionItemList__etblPromotionItemList_iBranchID` (`_etblPromotionItemList_iBranchID`),
  ADD KEY `idx__etblPromotionItemList__etblPromotionItemList_iChangeSetID` (`_etblPromotionItemList_iChangeSetID`);

--
-- Indexes for table `_etblremittancebatches`
--
ALTER TABLE `_etblremittancebatches`
  ADD PRIMARY KEY (`idRemittanceBatches`),
  ADD KEY `idx__etblRemittanceBatches__etblRemittanceBatches_Checksum` (`_etblRemittanceBatches_Checksum`),
  ADD KEY `idx__etblRemittanceBatches__etblRemittanceBatches_dModifiedDate` (`_etblRemittanceBatches_dModifiedDate`),
  ADD KEY `idx__etblRemittanceBatches__etblRemittanceBatches_iBranchID` (`_etblRemittanceBatches_iBranchID`),
  ADD KEY `idx__etblRemittanceBatches__etblRemittanceBatches_iChangeSetID` (`_etblRemittanceBatches_iChangeSetID`);

--
-- Indexes for table `_etblremittancelines`
--
ALTER TABLE `_etblremittancelines`
  ADD PRIMARY KEY (`IDRemittanceLines`),
  ADD KEY `idx__etblRemittanceLines__etblRemittanceLines_Checksum` (`_etblRemittanceLines_Checksum`),
  ADD KEY `idx__etblRemittanceLines__etblRemittanceLines_dModifiedDate` (`_etblRemittanceLines_dModifiedDate`),
  ADD KEY `idx__etblRemittanceLines__etblRemittanceLines_iBranchID` (`_etblRemittanceLines_iBranchID`),
  ADD KEY `idx__etblRemittanceLines__etblRemittanceLines_iChangeSetID` (`_etblRemittanceLines_iChangeSetID`);

--
-- Indexes for table `_etblremittancesuppliers`
--
ALTER TABLE `_etblremittancesuppliers`
  ADD PRIMARY KEY (`IDRemittanceSuppliers`),
  ADD KEY `idx__etblRemittanceSuppliers__etblRemittanceSuppliers_Checksum` (`_etblRemittanceSuppliers_Checksum`),
  ADD KEY `idx__etblRemittanceSuppliers__etblRemittanceSuppliers_dModifie31` (`_etblRemittanceSuppliers_dModifiedDate`),
  ADD KEY `idx__etblRemittanceSuppliers__etblRemittanceSuppliers_iBranchID` (`_etblRemittanceSuppliers_iBranchID`),
  ADD KEY `idx__etblRemittanceSuppliers__etblRemittanceSuppliers_iChangeS32` (`_etblRemittanceSuppliers_iChangeSetID`);

--
-- Indexes for table `_etblrepllog`
--
ALTER TABLE `_etblrepllog`
  ADD PRIMARY KEY (`idReplLog`),
  ADD KEY `idx_etblReplLog_iChangeSetID` (`iChangeSetID`);

--
-- Indexes for table `_etblreportjoblog`
--
ALTER TABLE `_etblreportjoblog`
  ADD PRIMARY KEY (`idReportJobLog`),
  ADD KEY `idx__etblReportJobLog__etblReportJobLog_Checksum` (`_etblReportJobLog_Checksum`),
  ADD KEY `idx__etblReportJobLog__etblReportJobLog_dModifiedDate` (`_etblReportJobLog_dModifiedDate`),
  ADD KEY `idx__etblReportJobLog__etblReportJobLog_iBranchID` (`_etblReportJobLog_iBranchID`),
  ADD KEY `idx__etblReportJobLog__etblReportJobLog_iChangeSetID` (`_etblReportJobLog_iChangeSetID`),
  ADD KEY `idxReportJobID` (`iReportJobID`);

--
-- Indexes for table `_etblreportjobs`
--
ALTER TABLE `_etblreportjobs`
  ADD PRIMARY KEY (`idReportJobs`),
  ADD KEY `idx__etblReportJobs__etblReportJobs_Checksum` (`_etblReportJobs_Checksum`),
  ADD KEY `idx__etblReportJobs__etblReportJobs_dModifiedDate` (`_etblReportJobs_dModifiedDate`),
  ADD KEY `idx__etblReportJobs__etblReportJobs_iBranchID` (`_etblReportJobs_iBranchID`),
  ADD KEY `idx__etblReportJobs__etblReportJobs_iChangeSetID` (`_etblReportJobs_iChangeSetID`),
  ADD KEY `idxReportJobAgentID` (`iReportJobAgentID`);

--
-- Indexes for table `_etblrevaluationhistory`
--
ALTER TABLE `_etblrevaluationhistory`
  ADD PRIMARY KEY (`idRevaluationHistory`),
  ADD KEY `idx__etblRevaluationHistory__etblRevaluationHistory_Checksum` (`_etblRevaluationHistory_Checksum`),
  ADD KEY `idx__etblRevaluationHistory__etblRevaluationHistory_dModifiedD51` (`_etblRevaluationHistory_dModifiedDate`),
  ADD KEY `idx__etblRevaluationHistory__etblRevaluationHistory_iBranchID` (`_etblRevaluationHistory_iBranchID`),
  ADD KEY `idx__etblRevaluationHistory__etblRevaluationHistory_iChangeSetID` (`_etblRevaluationHistory_iChangeSetID`);

--
-- Indexes for table `_etblsagepaybanks`
--
ALTER TABLE `_etblsagepaybanks`
  ADD PRIMARY KEY (`SagePayBankID`),
  ADD KEY `idx__etblSagePayBanks__etblSagePayBanks_Checksum` (`_etblSagePayBanks_Checksum`),
  ADD KEY `idx__etblSagePayBanks__etblSagePayBanks_dModifiedDate` (`_etblSagePayBanks_dModifiedDate`),
  ADD KEY `idx__etblSagePayBanks__etblSagePayBanks_iBranchID` (`_etblSagePayBanks_iBranchID`),
  ADD KEY `idx__etblSagePayBanks__etblSagePayBanks_iChangeSetID` (`_etblSagePayBanks_iChangeSetID`);

--
-- Indexes for table `_etblsagepayerrorcodes`
--
ALTER TABLE `_etblsagepayerrorcodes`
  ADD PRIMARY KEY (`idSagePayErrorCode`),
  ADD KEY `idx__etblSagePayErrorCodes__etblSagePayErrorCodes_Checksum` (`_etblSagePayErrorCodes_Checksum`),
  ADD KEY `idx__etblSagePayErrorCodes__etblSagePayErrorCodes_dModifiedDate` (`_etblSagePayErrorCodes_dModifiedDate`),
  ADD KEY `idx__etblSagePayErrorCodes__etblSagePayErrorCodes_iBranchID` (`_etblSagePayErrorCodes_iBranchID`),
  ADD KEY `idx__etblSagePayErrorCodes__etblSagePayErrorCodes_iChangeSetID` (`_etblSagePayErrorCodes_iChangeSetID`);

--
-- Indexes for table `_etblsagepaynow`
--
ALTER TABLE `_etblsagepaynow`
  ADD PRIMARY KEY (`idSagePayNow`),
  ADD KEY `idx__etblSagePayNow__etblSagePayNow_Checksum` (`_etblSagePayNow_Checksum`),
  ADD KEY `idx__etblSagePayNow__etblSagePayNow_dModifiedDate` (`_etblSagePayNow_dModifiedDate`),
  ADD KEY `idx__etblSagePayNow__etblSagePayNow_iBranchID` (`_etblSagePayNow_iBranchID`),
  ADD KEY `idx__etblSagePayNow__etblSagePayNow_iChangeSetID` (`_etblSagePayNow_iChangeSetID`);

--
-- Indexes for table `_etblsagepayqueue`
--
ALTER TABLE `_etblsagepayqueue`
  ADD PRIMARY KEY (`idSPQueue`),
  ADD KEY `idx__etblSagePayQueue__etblSagePayQueue_Checksum` (`_etblSagePayQueue_Checksum`),
  ADD KEY `idx__etblSagePayQueue__etblSagePayQueue_dModifiedDate` (`_etblSagePayQueue_dModifiedDate`),
  ADD KEY `idx__etblSagePayQueue__etblSagePayQueue_iBranchID` (`_etblSagePayQueue_iBranchID`),
  ADD KEY `idx__etblSagePayQueue__etblSagePayQueue_iChangeSetID` (`_etblSagePayQueue_iChangeSetID`);

--
-- Indexes for table `_etblsagepayservicekeys`
--
ALTER TABLE `_etblsagepayservicekeys`
  ADD PRIMARY KEY (`idSagePayServiceKey`),
  ADD KEY `idx__etblSagePayServiceKeys__etblSagePayServiceKeys_Checksum` (`_etblSagePayServiceKeys_Checksum`),
  ADD KEY `idx__etblSagePayServiceKeys__etblSagePayServiceKeys_dModifiedD83` (`_etblSagePayServiceKeys_dModifiedDate`),
  ADD KEY `idx__etblSagePayServiceKeys__etblSagePayServiceKeys_iBranchID` (`_etblSagePayServiceKeys_iBranchID`),
  ADD KEY `idx__etblSagePayServiceKeys__etblSagePayServiceKeys_iChangeSetID` (`_etblSagePayServiceKeys_iChangeSetID`);

--
-- Indexes for table `_etblsettlementterms`
--
ALTER TABLE `_etblsettlementterms`
  ADD PRIMARY KEY (`idSettlementTerms`),
  ADD KEY `idx__etblSettlementTerms__etblSettlementTerms_Checksum` (`_etblSettlementTerms_Checksum`),
  ADD KEY `idx__etblSettlementTerms__etblSettlementTerms_dModifiedDate` (`_etblSettlementTerms_dModifiedDate`),
  ADD KEY `idx__etblSettlementTerms__etblSettlementTerms_iBranchID` (`_etblSettlementTerms_iBranchID`),
  ADD KEY `idx__etblSettlementTerms__etblSettlementTerms_iChangeSetID` (`_etblSettlementTerms_iChangeSetID`);

--
-- Indexes for table `_etblsystem`
--
ALTER TABLE `_etblsystem`
  ADD PRIMARY KEY (`idSystem`),
  ADD KEY `idxIdentity` (`cIdentity`);

--
-- Indexes for table `_etblsystemdefaults`
--
ALTER TABLE `_etblsystemdefaults`
  ADD PRIMARY KEY (`idSystemDefaults`),
  ADD KEY `idx__etblSystemDefaults__etblSystemDefaults_Checksum` (`_etblSystemDefaults_Checksum`),
  ADD KEY `idx__etblSystemDefaults__etblSystemDefaults_dModifiedDate` (`_etblSystemDefaults_dModifiedDate`),
  ADD KEY `idx__etblSystemDefaults__etblSystemDefaults_iBranchID` (`_etblSystemDefaults_iBranchID`),
  ADD KEY `idx__etblSystemDefaults__etblSystemDefaults_iChangeSetID` (`_etblSystemDefaults_iChangeSetID`);

--
-- Indexes for table `_etblsystemupdate`
--
ALTER TABLE `_etblsystemupdate`
  ADD PRIMARY KEY (`idSystemUpdate`),
  ADD KEY `idx__etblSystemUpdate__etblSystemUpdate_Checksum` (`_etblSystemUpdate_Checksum`),
  ADD KEY `idx__etblSystemUpdate__etblSystemUpdate_dModifiedDate` (`_etblSystemUpdate_dModifiedDate`),
  ADD KEY `idx__etblSystemUpdate__etblSystemUpdate_iBranchID` (`_etblSystemUpdate_iBranchID`),
  ADD KEY `idx__etblSystemUpdate__etblSystemUpdate_iChangeSetID` (`_etblSystemUpdate_iChangeSetID`);

--
-- Indexes for table `_etbltaxboxlayout`
--
ALTER TABLE `_etbltaxboxlayout`
  ADD PRIMARY KEY (`idTaxBoxLayout`),
  ADD KEY `idx__etblTaxBoxLayout__etblTaxBoxLayout_Checksum` (`_etblTaxBoxLayout_Checksum`),
  ADD KEY `idx__etblTaxBoxLayout__etblTaxBoxLayout_dModifiedDate` (`_etblTaxBoxLayout_dModifiedDate`),
  ADD KEY `idx__etblTaxBoxLayout__etblTaxBoxLayout_iBranchID` (`_etblTaxBoxLayout_iBranchID`),
  ADD KEY `idx__etblTaxBoxLayout__etblTaxBoxLayout_iChangeSetID` (`_etblTaxBoxLayout_iChangeSetID`);

--
-- Indexes for table `_etbltaxboxsetup`
--
ALTER TABLE `_etbltaxboxsetup`
  ADD PRIMARY KEY (`idTaxBoxSetup`),
  ADD KEY `idx__etblTaxBoxSetup__etblTaxBoxSetup_Checksum` (`_etblTaxBoxSetup_Checksum`),
  ADD KEY `idx__etblTaxBoxSetup__etblTaxBoxSetup_dModifiedDate` (`_etblTaxBoxSetup_dModifiedDate`),
  ADD KEY `idx__etblTaxBoxSetup__etblTaxBoxSetup_iBranchID` (`_etblTaxBoxSetup_iBranchID`),
  ADD KEY `idx__etblTaxBoxSetup__etblTaxBoxSetup_iChangeSetID` (`_etblTaxBoxSetup_iChangeSetID`);

--
-- Indexes for table `_etbltaxdefaults`
--
ALTER TABLE `_etbltaxdefaults`
  ADD PRIMARY KEY (`idTaxDefaults`),
  ADD KEY `idx__etblTaxDefaults__etblTaxDefaults_Checksum` (`_etblTaxDefaults_Checksum`),
  ADD KEY `idx__etblTaxDefaults__etblTaxDefaults_dModifiedDate` (`_etblTaxDefaults_dModifiedDate`),
  ADD KEY `idx__etblTaxDefaults__etblTaxDefaults_iBranchID` (`_etblTaxDefaults_iBranchID`),
  ADD KEY `idx__etblTaxDefaults__etblTaxDefaults_iChangeSetID` (`_etblTaxDefaults_iChangeSetID`);

--
-- Indexes for table `_etbltaxgroup`
--
ALTER TABLE `_etbltaxgroup`
  ADD PRIMARY KEY (`idTaxGroup`),
  ADD KEY `idx__etblTaxGroup__etblTaxGroup_Checksum` (`_etblTaxGroup_Checksum`),
  ADD KEY `idx__etblTaxGroup__etblTaxGroup_dModifiedDate` (`_etblTaxGroup_dModifiedDate`),
  ADD KEY `idx__etblTaxGroup__etblTaxGroup_iBranchID` (`_etblTaxGroup_iBranchID`),
  ADD KEY `idx__etblTaxGroup__etblTaxGroup_iChangeSetID` (`_etblTaxGroup_iChangeSetID`);

--
-- Indexes for table `_etbltaxgrouptranstype`
--
ALTER TABLE `_etbltaxgrouptranstype`
  ADD PRIMARY KEY (`idTaxGroupTransType`),
  ADD KEY `idx__etblTaxGroupTransType__etblTaxGroupTransType_Checksum` (`_etblTaxGroupTransType_Checksum`),
  ADD KEY `idx__etblTaxGroupTransType__etblTaxGroupTransType_dModifiedDate` (`_etblTaxGroupTransType_dModifiedDate`),
  ADD KEY `idx__etblTaxGroupTransType__etblTaxGroupTransType_iBranchID` (`_etblTaxGroupTransType_iBranchID`),
  ADD KEY `idx__etblTaxGroupTransType__etblTaxGroupTransType_iChangeSetID` (`_etblTaxGroupTransType_iChangeSetID`);

--
-- Indexes for table `_etblterms`
--
ALTER TABLE `_etblterms`
  ADD PRIMARY KEY (`iTermID`),
  ADD KEY `idx__etblTerms__etblTerms_Checksum` (`_etblTerms_Checksum`),
  ADD KEY `idx__etblTerms__etblTerms_dModifiedDate` (`_etblTerms_dModifiedDate`),
  ADD KEY `idx__etblTerms__etblTerms_iBranchID` (`_etblTerms_iBranchID`),
  ADD KEY `idx__etblTerms__etblTerms_iChangeSetID` (`_etblTerms_iChangeSetID`);

--
-- Indexes for table `_etblunitcategory`
--
ALTER TABLE `_etblunitcategory`
  ADD PRIMARY KEY (`idUnitCategory`),
  ADD KEY `idx__etblUnitCategory__etblUnitCategory_Checksum` (`_etblUnitCategory_Checksum`),
  ADD KEY `idx__etblUnitCategory__etblUnitCategory_dModifiedDate` (`_etblUnitCategory_dModifiedDate`),
  ADD KEY `idx__etblUnitCategory__etblUnitCategory_iBranchID` (`_etblUnitCategory_iBranchID`),
  ADD KEY `idx__etblUnitCategory__etblUnitCategory_iChangeSetID` (`_etblUnitCategory_iChangeSetID`);

--
-- Indexes for table `_etblunitconversion`
--
ALTER TABLE `_etblunitconversion`
  ADD PRIMARY KEY (`idUnitConversion`),
  ADD KEY `_etblUnitConversion_idxAB` (`iUnitAID`,`iUnitBID`),
  ADD KEY `_etblUnitConversion_idxBA` (`iUnitBID`,`iUnitAID`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_Checksum` (`_etblUnitConversion_Checksum`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_dModifiedDate` (`_etblUnitConversion_dModifiedDate`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_iBranchID` (`_etblUnitConversion_iBranchID`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_iChangeSetID` (`_etblUnitConversion_iChangeSetID`);

--
-- Indexes for table `_etblunits`
--
ALTER TABLE `_etblunits`
  ADD PRIMARY KEY (`idUnits`),
  ADD KEY `idx__etblUnits__etblUnits_Checksum` (`_etblUnits_Checksum`),
  ADD KEY `idx__etblUnits__etblUnits_dModifiedDate` (`_etblUnits_dModifiedDate`),
  ADD KEY `idx__etblUnits__etblUnits_iBranchID` (`_etblUnits_iBranchID`),
  ADD KEY `idx__etblUnits__etblUnits_iChangeSetID` (`_etblUnits_iChangeSetID`);

--
-- Indexes for table `_etblvasairtimeitem`
--
ALTER TABLE `_etblvasairtimeitem`
  ADD PRIMARY KEY (`idVASAirtimeItem`),
  ADD KEY `idx__etblVASAirtimeItem__etblVASAirtimeItem_Checksum` (`_etblVASAirtimeItem_Checksum`),
  ADD KEY `idx__etblVASAirtimeItem__etblVASAirtimeItem_dModifiedDate` (`_etblVASAirtimeItem_dModifiedDate`),
  ADD KEY `idx__etblVASAirtimeItem__etblVASAirtimeItem_iBranchID` (`_etblVASAirtimeItem_iBranchID`),
  ADD KEY `idx__etblVASAirtimeItem__etblVASAirtimeItem_iChangeSetID` (`_etblVASAirtimeItem_iChangeSetID`);

--
-- Indexes for table `_etblvasairtimemaster`
--
ALTER TABLE `_etblvasairtimemaster`
  ADD PRIMARY KEY (`idVASAirtimeMaster`),
  ADD KEY `idx__etblVASAirtimeMaster__etblVASAirtimeMaster_Checksum` (`_etblVASAirtimeMaster_Checksum`),
  ADD KEY `idx__etblVASAirtimeMaster__etblVASAirtimeMaster_dModifiedDate` (`_etblVASAirtimeMaster_dModifiedDate`),
  ADD KEY `idx__etblVASAirtimeMaster__etblVASAirtimeMaster_iBranchID` (`_etblVASAirtimeMaster_iBranchID`),
  ADD KEY `idx__etblVASAirtimeMaster__etblVASAirtimeMaster_iChangeSetID` (`_etblVASAirtimeMaster_iChangeSetID`);

--
-- Indexes for table `_etblvasairtimenetwork`
--
ALTER TABLE `_etblvasairtimenetwork`
  ADD PRIMARY KEY (`idVASAirtimeNetwork`),
  ADD KEY `idx__etblVASAirtimeNetwork__etblVASAirtimeNetwork_Checksum` (`_etblVASAirtimeNetwork_Checksum`),
  ADD KEY `idx__etblVASAirtimeNetwork__etblVASAirtimeNetwork_dModifiedDate` (`_etblVASAirtimeNetwork_dModifiedDate`),
  ADD KEY `idx__etblVASAirtimeNetwork__etblVASAirtimeNetwork_iBranchID` (`_etblVASAirtimeNetwork_iBranchID`),
  ADD KEY `idx__etblVASAirtimeNetwork__etblVASAirtimeNetwork_iChangeSetID` (`_etblVASAirtimeNetwork_iChangeSetID`);

--
-- Indexes for table `_etblvasairtimeproduct`
--
ALTER TABLE `_etblvasairtimeproduct`
  ADD PRIMARY KEY (`idVASAirtimeProduct`),
  ADD KEY `idx__etblVASAirtimeProduct__etblVASAirtimeProduct_Checksum` (`_etblVASAirtimeProduct_Checksum`),
  ADD KEY `idx__etblVASAirtimeProduct__etblVASAirtimeProduct_dModifiedDate` (`_etblVASAirtimeProduct_dModifiedDate`),
  ADD KEY `idx__etblVASAirtimeProduct__etblVASAirtimeProduct_iBranchID` (`_etblVASAirtimeProduct_iBranchID`),
  ADD KEY `idx__etblVASAirtimeProduct__etblVASAirtimeProduct_iChangeSetID` (`_etblVASAirtimeProduct_iChangeSetID`);

--
-- Indexes for table `_etblvdap`
--
ALTER TABLE `_etblvdap`
  ADD PRIMARY KEY (`IDVD`),
  ADD KEY `idx__etblVDAP__etblVDAP_Checksum` (`_etblVDAP_Checksum`),
  ADD KEY `idx__etblVDAP__etblVDAP_dModifiedDate` (`_etblVDAP_dModifiedDate`),
  ADD KEY `idx__etblVDAP__etblVDAP_iBranchID` (`_etblVDAP_iBranchID`),
  ADD KEY `idx__etblVDAP__etblVDAP_iChangeSetID` (`_etblVDAP_iChangeSetID`),
  ADD KEY `idx_etblVDAP_iARAPID` (`iARAPID`),
  ADD KEY `idx_etblVDAP_iGroupID_iCurrencyID` (`iGroupID`,`iCurrencyID`);

--
-- Indexes for table `_etblvdar`
--
ALTER TABLE `_etblvdar`
  ADD PRIMARY KEY (`IDVD`),
  ADD KEY `idx__etblVDAR__etblVDAR_Checksum` (`_etblVDAR_Checksum`),
  ADD KEY `idx__etblVDAR__etblVDAR_dModifiedDate` (`_etblVDAR_dModifiedDate`),
  ADD KEY `idx__etblVDAR__etblVDAR_iBranchID` (`_etblVDAR_iBranchID`),
  ADD KEY `idx__etblVDAR__etblVDAR_iChangeSetID` (`_etblVDAR_iChangeSetID`),
  ADD KEY `idx_etblVDAR_iARAPID` (`iARAPID`),
  ADD KEY `idx_etblVDAR_iGroupID_iCurrencyID` (`iGroupID`,`iCurrencyID`);

--
-- Indexes for table `_etblvdlnap`
--
ALTER TABLE `_etblvdlnap`
  ADD PRIMARY KEY (`IDVDLn`),
  ADD KEY `idx__etblVDLnAP__etblVDLnAP_Checksum` (`_etblVDLnAP_Checksum`),
  ADD KEY `idx__etblVDLnAP__etblVDLnAP_dModifiedDate` (`_etblVDLnAP_dModifiedDate`),
  ADD KEY `idx__etblVDLnAP__etblVDLnAP_iBranchID` (`_etblVDLnAP_iBranchID`),
  ADD KEY `idx__etblVDLnAP__etblVDLnAP_iChangeSetID` (`_etblVDLnAP_iChangeSetID`),
  ADD KEY `idx_etblVDLnAP_iStGroupID_iCurrencyID` (`iStGroupID`,`iCurrencyID`),
  ADD KEY `idx_etblVDLnAP_iVDID_iStockID` (`iVDID`,`iStockID`);

--
-- Indexes for table `_etblvdlnar`
--
ALTER TABLE `_etblvdlnar`
  ADD PRIMARY KEY (`IDVDLn`),
  ADD KEY `idx__etblVDLnAR__etblVDLnAR_Checksum` (`_etblVDLnAR_Checksum`),
  ADD KEY `idx__etblVDLnAR__etblVDLnAR_dModifiedDate` (`_etblVDLnAR_dModifiedDate`),
  ADD KEY `idx__etblVDLnAR__etblVDLnAR_iBranchID` (`_etblVDLnAR_iBranchID`),
  ADD KEY `idx__etblVDLnAR__etblVDLnAR_iChangeSetID` (`_etblVDLnAR_iChangeSetID`),
  ADD KEY `idx_etblVDLnAR_iStGroupID_iCurrencyID` (`iStGroupID`,`iCurrencyID`),
  ADD KEY `idx_etblVDLnAR_iVDID_iStockID` (`iVDID`,`iStockID`);

--
-- Indexes for table `_etblvdlnlvlap`
--
ALTER TABLE `_etblvdlnlvlap`
  ADD PRIMARY KEY (`IDVDLnLvl`),
  ADD KEY `idx__etblVDLnLvlAP__etblVDLnLvlAP_Checksum` (`_etblVDLnLvlAP_Checksum`),
  ADD KEY `idx__etblVDLnLvlAP__etblVDLnLvlAP_dModifiedDate` (`_etblVDLnLvlAP_dModifiedDate`),
  ADD KEY `idx__etblVDLnLvlAP__etblVDLnLvlAP_iBranchID` (`_etblVDLnLvlAP_iBranchID`),
  ADD KEY `idx__etblVDLnLvlAP__etblVDLnLvlAP_iChangeSetID` (`_etblVDLnLvlAP_iChangeSetID`),
  ADD KEY `idx_etblVDLnLvlAP_iVDLnID_iLevel` (`iVDLnID`,`iLevel`);

--
-- Indexes for table `_etblvdlnlvlar`
--
ALTER TABLE `_etblvdlnlvlar`
  ADD PRIMARY KEY (`IDVDLnLvl`),
  ADD KEY `idx__etblVDLnLvlAR__etblVDLnLvlAR_Checksum` (`_etblVDLnLvlAR_Checksum`),
  ADD KEY `idx__etblVDLnLvlAR__etblVDLnLvlAR_dModifiedDate` (`_etblVDLnLvlAR_dModifiedDate`),
  ADD KEY `idx__etblVDLnLvlAR__etblVDLnLvlAR_iBranchID` (`_etblVDLnLvlAR_iBranchID`),
  ADD KEY `idx__etblVDLnLvlAR__etblVDLnLvlAR_iChangeSetID` (`_etblVDLnLvlAR_iChangeSetID`),
  ADD KEY `idx_etblVDLnLvlAR_iVDLnID_iLevel` (`iVDLnID`,`iLevel`);

--
-- Indexes for table `_etblwhdefaults`
--
ALTER TABLE `_etblwhdefaults`
  ADD PRIMARY KEY (`IDWhDefaults`),
  ADD KEY `idx__etblWhDefaults__etblWhDefaults_Checksum` (`_etblWhDefaults_Checksum`),
  ADD KEY `idx__etblWhDefaults__etblWhDefaults_dModifiedDate` (`_etblWhDefaults_dModifiedDate`),
  ADD KEY `idx__etblWhDefaults__etblWhDefaults_iBranchID` (`_etblWhDefaults_iBranchID`),
  ADD KEY `idx__etblWhDefaults__etblWhDefaults_iChangeSetID` (`_etblWhDefaults_iChangeSetID`);

--
-- Indexes for table `_etblwhseibt`
--
ALTER TABLE `_etblwhseibt`
  ADD PRIMARY KEY (`IDWhseIBT`),
  ADD KEY `idx__etblWhseIBT__etblWhseIBT_Checksum` (`_etblWhseIBT_Checksum`),
  ADD KEY `idx__etblWhseIBT__etblWhseIBT_dModifiedDate` (`_etblWhseIBT_dModifiedDate`),
  ADD KEY `idx__etblWhseIBT__etblWhseIBT_iBranchID` (`_etblWhseIBT_iBranchID`),
  ADD KEY `idx__etblWhseIBT__etblWhseIBT_iChangeSetID` (`_etblWhseIBT_iChangeSetID`);

--
-- Indexes for table `_etblwhseibtaddcosts`
--
ALTER TABLE `_etblwhseibtaddcosts`
  ADD PRIMARY KEY (`IDWhseIBTAddCosts`),
  ADD KEY `idx__etblWhseIBTAddCosts__etblWhseIBTAddCosts_Checksum` (`_etblWhseIBTAddCosts_Checksum`),
  ADD KEY `idx__etblWhseIBTAddCosts__etblWhseIBTAddCosts_dModifiedDate` (`_etblWhseIBTAddCosts_dModifiedDate`),
  ADD KEY `idx__etblWhseIBTAddCosts__etblWhseIBTAddCosts_iBranchID` (`_etblWhseIBTAddCosts_iBranchID`),
  ADD KEY `idx__etblWhseIBTAddCosts__etblWhseIBTAddCosts_iChangeSetID` (`_etblWhseIBTAddCosts_iChangeSetID`),
  ADD KEY `idx_etblWhseIBTAddCosts_iWhseIBTID` (`iWhseIBTID`);

--
-- Indexes for table `_etblwhseibtlines`
--
ALTER TABLE `_etblwhseibtlines`
  ADD PRIMARY KEY (`IDWhseIBTLines`),
  ADD KEY `idx__etblWhseIBTLines__etblWhseIBTLines_Checksum` (`_etblWhseIBTLines_Checksum`),
  ADD KEY `idx__etblWhseIBTLines__etblWhseIBTLines_dModifiedDate` (`_etblWhseIBTLines_dModifiedDate`),
  ADD KEY `idx__etblWhseIBTLines__etblWhseIBTLines_iBranchID` (`_etblWhseIBTLines_iBranchID`),
  ADD KEY `idx__etblWhseIBTLines__etblWhseIBTLines_iChangeSetID` (`_etblWhseIBTLines_iChangeSetID`);

--
-- Indexes for table `_etblwhseibtlinesn`
--
ALTER TABLE `_etblwhseibtlinesn`
  ADD PRIMARY KEY (`IDWhseIBTLineSN`),
  ADD KEY `idx__etblWhseIBTLineSN__etblWhseIBTLineSN_Checksum` (`_etblWhseIBTLineSN_Checksum`),
  ADD KEY `idx__etblWhseIBTLineSN__etblWhseIBTLineSN_dModifiedDate` (`_etblWhseIBTLineSN_dModifiedDate`),
  ADD KEY `idx__etblWhseIBTLineSN__etblWhseIBTLineSN_iBranchID` (`_etblWhseIBTLineSN_iBranchID`),
  ADD KEY `idx__etblWhseIBTLineSN__etblWhseIBTLineSN_iChangeSetID` (`_etblWhseIBTLineSN_iChangeSetID`),
  ADD KEY `idx_etblWhseIBTLineSN_iSNGroupID` (`iWhseIBTID`,`iSNGroupID`);

--
-- Indexes for table `_etblwhsetransferbatches`
--
ALTER TABLE `_etblwhsetransferbatches`
  ADD PRIMARY KEY (`idWhseTransferBatch`),
  ADD KEY `idx__etblWhseTransferBatches__etblWhseTransferBatches_Checksum` (`_etblWhseTransferBatches_Checksum`),
  ADD KEY `idx__etblWhseTransferBatches__etblWhseTransferBatches_dModifie96` (`_etblWhseTransferBatches_dModifiedDate`),
  ADD KEY `idx__etblWhseTransferBatches__etblWhseTransferBatches_iBranchID` (`_etblWhseTransferBatches_iBranchID`),
  ADD KEY `idx__etblWhseTransferBatches__etblWhseTransferBatches_iChangeS97` (`_etblWhseTransferBatches_iChangeSetID`);

--
-- Indexes for table `_etblworkers`
--
ALTER TABLE `_etblworkers`
  ADD PRIMARY KEY (`idWorkers`),
  ADD KEY `idx__etblWorkers__etblWorkers_Checksum` (`_etblWorkers_Checksum`),
  ADD KEY `idx__etblWorkers__etblWorkers_dModifiedDate` (`_etblWorkers_dModifiedDate`),
  ADD KEY `idx__etblWorkers__etblWorkers_iBranchID` (`_etblWorkers_iBranchID`),
  ADD KEY `idx__etblWorkers__etblWorkers_iChangeSetID` (`_etblWorkers_iChangeSetID`);

--
-- Indexes for table `_mtblmbrcategories`
--
ALTER TABLE `_mtblmbrcategories`
  ADD PRIMARY KEY (`idMBRCategories`),
  ADD KEY `idx__mtblMBRCategories__mtblMBRCategories_Checksum` (`_mtblMBRCategories_Checksum`),
  ADD KEY `idx__mtblMBRCategories__mtblMBRCategories_dModifiedDate` (`_mtblMBRCategories_dModifiedDate`),
  ADD KEY `idx__mtblMBRCategories__mtblMBRCategories_iBranchID` (`_mtblMBRCategories_iBranchID`),
  ADD KEY `idx__mtblMBRCategories__mtblMBRCategories_iChangeSetID` (`_mtblMBRCategories_iChangeSetID`);

--
-- Indexes for table `_retagentsession`
--
ALTER TABLE `_retagentsession`
  ADD PRIMARY KEY (`idAgentSession`),
  ADD KEY `idx__retAgentSession__retAgentSession_Checksum` (`_retAgentSession_Checksum`),
  ADD KEY `idx__retAgentSession__retAgentSession_dModifiedDate` (`_retAgentSession_dModifiedDate`),
  ADD KEY `idx__retAgentSession__retAgentSession_iBranchID` (`_retAgentSession_iBranchID`),
  ADD KEY `idx__retAgentSession__retAgentSession_iChangeSetID` (`_retAgentSession_iChangeSetID`);

--
-- Indexes for table `_retcashpickup`
--
ALTER TABLE `_retcashpickup`
  ADD PRIMARY KEY (`idCashPickup`),
  ADD KEY `idx__retCashPickup__retCashPickup_Checksum` (`_retCashPickup_Checksum`),
  ADD KEY `idx__retCashPickup__retCashPickup_dModifiedDate` (`_retCashPickup_dModifiedDate`),
  ADD KEY `idx__retCashPickup__retCashPickup_iBranchID` (`_retCashPickup_iBranchID`),
  ADD KEY `idx__retCashPickup__retCashPickup_iChangeSetID` (`_retCashPickup_iChangeSetID`),
  ADD KEY `idx__retCashPickup_iAgentSessionID` (`iAgentSessionID`);

--
-- Indexes for table `_retdefaults`
--
ALTER TABLE `_retdefaults`
  ADD PRIMARY KEY (`idRetailDefaults`),
  ADD KEY `idx__retDefaults__retDefaults_Checksum` (`_retDefaults_Checksum`),
  ADD KEY `idx__retDefaults__retDefaults_dModifiedDate` (`_retDefaults_dModifiedDate`),
  ADD KEY `idx__retDefaults__retDefaults_iBranchID` (`_retDefaults_iBranchID`),
  ADD KEY `idx__retDefaults__retDefaults_iChangeSetID` (`_retDefaults_iChangeSetID`);

--
-- Indexes for table `_retdenomination`
--
ALTER TABLE `_retdenomination`
  ADD PRIMARY KEY (`idDenomination`),
  ADD KEY `idx__retDenomination__retDenomination_Checksum` (`_retDenomination_Checksum`),
  ADD KEY `idx__retDenomination__retDenomination_dModifiedDate` (`_retDenomination_dModifiedDate`),
  ADD KEY `idx__retDenomination__retDenomination_iBranchID` (`_retDenomination_iBranchID`),
  ADD KEY `idx__retDenomination__retDenomination_iChangeSetID` (`_retDenomination_iChangeSetID`);

--
-- Indexes for table `_retdiscountreason`
--
ALTER TABLE `_retdiscountreason`
  ADD PRIMARY KEY (`idDiscountReason`),
  ADD KEY `idx__retDiscountReason__retDiscountReason_Checksum` (`_retDiscountReason_Checksum`),
  ADD KEY `idx__retDiscountReason__retDiscountReason_dModifiedDate` (`_retDiscountReason_dModifiedDate`),
  ADD KEY `idx__retDiscountReason__retDiscountReason_iBranchID` (`_retDiscountReason_iBranchID`),
  ADD KEY `idx__retDiscountReason__retDiscountReason_iChangeSetID` (`_retDiscountReason_iChangeSetID`);

--
-- Indexes for table `_retdocketlock`
--
ALTER TABLE `_retdocketlock`
  ADD PRIMARY KEY (`idDocketLock`);

--
-- Indexes for table `_retlaybys`
--
ALTER TABLE `_retlaybys`
  ADD PRIMARY KEY (`idLayBys`),
  ADD KEY `idx__retLayBys__retLayBys_Checksum` (`_retLayBys_Checksum`),
  ADD KEY `idx__retLayBys__retLayBys_dModifiedDate` (`_retLayBys_dModifiedDate`),
  ADD KEY `idx__retLayBys__retLayBys_iBranchID` (`_retLayBys_iBranchID`),
  ADD KEY `idx__retLayBys__retLayBys_iChangeSetID` (`_retLayBys_iChangeSetID`),
  ADD KEY `idx__retLayBys_iInvoiceID` (`iInvoiceID`);

--
-- Indexes for table `_retpettycash`
--
ALTER TABLE `_retpettycash`
  ADD PRIMARY KEY (`idPettyCash`),
  ADD KEY `idx__retPettyCash__retPettyCash_Checksum` (`_retPettyCash_Checksum`),
  ADD KEY `idx__retPettyCash__retPettyCash_dModifiedDate` (`_retPettyCash_dModifiedDate`),
  ADD KEY `idx__retPettyCash__retPettyCash_iBranchID` (`_retPettyCash_iBranchID`),
  ADD KEY `idx__retPettyCash__retPettyCash_iChangeSetID` (`_retPettyCash_iChangeSetID`);

--
-- Indexes for table `_retpettycashline`
--
ALTER TABLE `_retpettycashline`
  ADD PRIMARY KEY (`idPettyCashLine`),
  ADD KEY `idx__retPettyCashLine__retPettyCashLine_Checksum` (`_retPettyCashLine_Checksum`),
  ADD KEY `idx__retPettyCashLine__retPettyCashLine_dModifiedDate` (`_retPettyCashLine_dModifiedDate`),
  ADD KEY `idx__retPettyCashLine__retPettyCashLine_iBranchID` (`_retPettyCashLine_iBranchID`),
  ADD KEY `idx__retPettyCashLine__retPettyCashLine_iChangeSetID` (`_retPettyCashLine_iChangeSetID`);

--
-- Indexes for table `_retpettycashtype`
--
ALTER TABLE `_retpettycashtype`
  ADD PRIMARY KEY (`idPettyCashType`),
  ADD KEY `idx__retPettyCashType__retPettyCashType_Checksum` (`_retPettyCashType_Checksum`),
  ADD KEY `idx__retPettyCashType__retPettyCashType_dModifiedDate` (`_retPettyCashType_dModifiedDate`),
  ADD KEY `idx__retPettyCashType__retPettyCashType_iBranchID` (`_retPettyCashType_iBranchID`),
  ADD KEY `idx__retPettyCashType__retPettyCashType_iChangeSetID` (`_retPettyCashType_iChangeSetID`);

--
-- Indexes for table `_retposlogfile`
--
ALTER TABLE `_retposlogfile`
  ADD PRIMARY KEY (`idPOSLogFile`),
  ADD KEY `idx__retPOSLogFile__retPOSLogFile_Checksum` (`_retPOSLogFile_Checksum`),
  ADD KEY `idx__retPOSLogFile__retPOSLogFile_dModifiedDate` (`_retPOSLogFile_dModifiedDate`),
  ADD KEY `idx__retPOSLogFile__retPOSLogFile_iBranchID` (`_retPOSLogFile_iBranchID`),
  ADD KEY `idx__retPOSLogFile__retPOSLogFile_iChangeSetID` (`_retPOSLogFile_iChangeSetID`);

--
-- Indexes for table `_retposloglinks`
--
ALTER TABLE `_retposloglinks`
  ADD PRIMARY KEY (`idPOSLogLinks`),
  ADD KEY `idx__retPOSLogLinks__retPOSLogLinks_Checksum` (`_retPOSLogLinks_Checksum`),
  ADD KEY `idx__retPOSLogLinks__retPOSLogLinks_dModifiedDate` (`_retPOSLogLinks_dModifiedDate`),
  ADD KEY `idx__retPOSLogLinks__retPOSLogLinks_iBranchID` (`_retPOSLogLinks_iBranchID`),
  ADD KEY `idx__retPOSLogLinks__retPOSLogLinks_iChangeSetID` (`_retPOSLogLinks_iChangeSetID`),
  ADD KEY `idx__retPOSLogLinks_iInvNumID` (`iInvNumID`),
  ADD KEY `idx__retPOSLogLinks_iLogID` (`iLogID`);

--
-- Indexes for table `_retposmenu`
--
ALTER TABLE `_retposmenu`
  ADD PRIMARY KEY (`idPOSMenu`),
  ADD KEY `idx__retPOSMenu__retPOSMenu_Checksum` (`_retPOSMenu_Checksum`),
  ADD KEY `idx__retPOSMenu__retPOSMenu_dModifiedDate` (`_retPOSMenu_dModifiedDate`),
  ADD KEY `idx__retPOSMenu__retPOSMenu_iBranchID` (`_retPOSMenu_iBranchID`),
  ADD KEY `idx__retPOSMenu__retPOSMenu_iChangeSetID` (`_retPOSMenu_iChangeSetID`);

--
-- Indexes for table `_retposmenusetup`
--
ALTER TABLE `_retposmenusetup`
  ADD PRIMARY KEY (`idPOSMenuSetup`),
  ADD KEY `idx__retPOSMenuSetup__retPOSMenuSetup_Checksum` (`_retPOSMenuSetup_Checksum`),
  ADD KEY `idx__retPOSMenuSetup__retPOSMenuSetup_dModifiedDate` (`_retPOSMenuSetup_dModifiedDate`),
  ADD KEY `idx__retPOSMenuSetup__retPOSMenuSetup_iBranchID` (`_retPOSMenuSetup_iBranchID`),
  ADD KEY `idx__retPOSMenuSetup__retPOSMenuSetup_iChangeSetID` (`_retPOSMenuSetup_iChangeSetID`);

--
-- Indexes for table `_retpostender`
--
ALTER TABLE `_retpostender`
  ADD PRIMARY KEY (`idPOSTender`),
  ADD KEY `idx__retPOSTender__retPOSTender_Checksum` (`_retPOSTender_Checksum`),
  ADD KEY `idx__retPOSTender__retPOSTender_dModifiedDate` (`_retPOSTender_dModifiedDate`),
  ADD KEY `idx__retPOSTender__retPOSTender_iBranchID` (`_retPOSTender_iBranchID`),
  ADD KEY `idx__retPOSTender__retPOSTender_iChangeSetID` (`_retPOSTender_iChangeSetID`);

--
-- Indexes for table `_retpostransaction`
--
ALTER TABLE `_retpostransaction`
  ADD PRIMARY KEY (`idPOSTransaction`),
  ADD KEY `idx__rebtPOSTransaction_iAgentSessionID` (`iAgentSessionID`),
  ADD KEY `idx__retPOSTransaction__retPOSTransaction_Checksum` (`_retPOSTransaction_Checksum`),
  ADD KEY `idx__retPOSTransaction__retPOSTransaction_dModifiedDate` (`_retPOSTransaction_dModifiedDate`),
  ADD KEY `idx__retPOSTransaction__retPOSTransaction_iBranchID` (`_retPOSTransaction_iBranchID`),
  ADD KEY `idx__retPOSTransaction__retPOSTransaction_iChangeSetID` (`_retPOSTransaction_iChangeSetID`),
  ADD KEY `idx__retPOSTransaction_iInvNumID` (`iInvNumID`);

--
-- Indexes for table `_retpriceoverridereason`
--
ALTER TABLE `_retpriceoverridereason`
  ADD PRIMARY KEY (`idPriceOverrideReason`),
  ADD KEY `idx__retPriceOverrideReason__retPriceOverrideReason_Checksum` (`_retPriceOverrideReason_Checksum`),
  ADD KEY `idx__retPriceOverrideReason__retPriceOverrideReason_dModifiedD50` (`_retPriceOverrideReason_dModifiedDate`),
  ADD KEY `idx__retPriceOverrideReason__retPriceOverrideReason_iBranchID` (`_retPriceOverrideReason_iBranchID`),
  ADD KEY `idx__retPriceOverrideReason__retPriceOverrideReason_iChangeSetID` (`_retPriceOverrideReason_iChangeSetID`);

--
-- Indexes for table `_retreturnreason`
--
ALTER TABLE `_retreturnreason`
  ADD PRIMARY KEY (`idReturnReason`),
  ADD KEY `idx__retReturnReason__retReturnReason_Checksum` (`_retReturnReason_Checksum`),
  ADD KEY `idx__retReturnReason__retReturnReason_dModifiedDate` (`_retReturnReason_dModifiedDate`),
  ADD KEY `idx__retReturnReason__retReturnReason_iBranchID` (`_retReturnReason_iBranchID`),
  ADD KEY `idx__retReturnReason__retReturnReason_iChangeSetID` (`_retReturnReason_iChangeSetID`);

--
-- Indexes for table `_rettendertype`
--
ALTER TABLE `_rettendertype`
  ADD PRIMARY KEY (`idTenderType`),
  ADD KEY `idx__retTenderType__retTenderType_Checksum` (`_retTenderType_Checksum`),
  ADD KEY `idx__retTenderType__retTenderType_dModifiedDate` (`_retTenderType_dModifiedDate`),
  ADD KEY `idx__retTenderType__retTenderType_iBranchID` (`_retTenderType_iBranchID`),
  ADD KEY `idx__retTenderType__retTenderType_iChangeSetID` (`_retTenderType_iChangeSetID`);

--
-- Indexes for table `_rettill`
--
ALTER TABLE `_rettill`
  ADD PRIMARY KEY (`idTill`),
  ADD KEY `idx__retTill__retTill_Checksum` (`_retTill_Checksum`),
  ADD KEY `idx__retTill__retTill_dModifiedDate` (`_retTill_dModifiedDate`),
  ADD KEY `idx__retTill__retTill_iBranchID` (`_retTill_iBranchID`),
  ADD KEY `idx__retTill__retTill_iChangeSetID` (`_retTill_iChangeSetID`);

--
-- Indexes for table `_rettillsecurity`
--
ALTER TABLE `_rettillsecurity`
  ADD PRIMARY KEY (`idTillSecurity`),
  ADD KEY `idx__retTillSecurity__retTillSecurity_Checksum` (`_retTillSecurity_Checksum`),
  ADD KEY `idx__retTillSecurity__retTillSecurity_dModifiedDate` (`_retTillSecurity_dModifiedDate`),
  ADD KEY `idx__retTillSecurity__retTillSecurity_iBranchID` (`_retTillSecurity_iBranchID`),
  ADD KEY `idx__retTillSecurity__retTillSecurity_iChangeSetID` (`_retTillSecurity_iChangeSetID`);

--
-- Indexes for table `_rettillstationery`
--
ALTER TABLE `_rettillstationery`
  ADD PRIMARY KEY (`idTillStationery`),
  ADD KEY `idx__retTillStationery__retTillStationery_Checksum` (`_retTillStationery_Checksum`),
  ADD KEY `idx__retTillStationery__retTillStationery_dModifiedDate` (`_retTillStationery_dModifiedDate`),
  ADD KEY `idx__retTillStationery__retTillStationery_iBranchID` (`_retTillStationery_iBranchID`),
  ADD KEY `idx__retTillStationery__retTillStationery_iChangeSetID` (`_retTillStationery_iChangeSetID`);

--
-- Indexes for table `_rettradingsession`
--
ALTER TABLE `_rettradingsession`
  ADD PRIMARY KEY (`idTradingSession`),
  ADD KEY `idx__retTradingSession__retTradingSession_Checksum` (`_retTradingSession_Checksum`),
  ADD KEY `idx__retTradingSession__retTradingSession_dModifiedDate` (`_retTradingSession_dModifiedDate`),
  ADD KEY `idx__retTradingSession__retTradingSession_iBranchID` (`_retTradingSession_iBranchID`),
  ADD KEY `idx__retTradingSession__retTradingSession_iChangeSetID` (`_retTradingSession_iChangeSetID`);

--
-- Indexes for table `_retvariablebarcode`
--
ALTER TABLE `_retvariablebarcode`
  ADD PRIMARY KEY (`idVariableBarcode`),
  ADD KEY `idx__retVariableBarcode__retVariableBarcode_Checksum` (`_retVariableBarcode_Checksum`),
  ADD KEY `idx__retVariableBarcode__retVariableBarcode_dModifiedDate` (`_retVariableBarcode_dModifiedDate`),
  ADD KEY `idx__retVariableBarcode__retVariableBarcode_iBranchID` (`_retVariableBarcode_iBranchID`),
  ADD KEY `idx__retVariableBarcode__retVariableBarcode_iChangeSetID` (`_retVariableBarcode_iChangeSetID`);

--
-- Indexes for table `_rtblagentgroupmembers`
--
ALTER TABLE `_rtblagentgroupmembers`
  ADD PRIMARY KEY (`iGroupID`,`iAgentID`),
  ADD KEY `idx__rtblAgentGroupMembers__rtblAgentGroupMembers_Checksum` (`_rtblAgentGroupMembers_Checksum`),
  ADD KEY `idx__rtblAgentGroupMembers__rtblAgentGroupMembers_dModifiedDate` (`_rtblAgentGroupMembers_dModifiedDate`),
  ADD KEY `idx__rtblAgentGroupMembers__rtblAgentGroupMembers_iBranchID` (`_rtblAgentGroupMembers_iBranchID`),
  ADD KEY `idx__rtblAgentGroupMembers__rtblAgentGroupMembers_iChangeSetID` (`_rtblAgentGroupMembers_iChangeSetID`);

--
-- Indexes for table `_rtblagentgroups`
--
ALTER TABLE `_rtblagentgroups`
  ADD PRIMARY KEY (`idAgentGroups`),
  ADD KEY `idxGroupName` (`cGroupName`),
  ADD KEY `idx__rtblAgentGroups__rtblAgentGroups_Checksum` (`_rtblAgentGroups_Checksum`),
  ADD KEY `idx__rtblAgentGroups__rtblAgentGroups_dModifiedDate` (`_rtblAgentGroups_dModifiedDate`),
  ADD KEY `idx__rtblAgentGroups__rtblAgentGroups_iBranchID` (`_rtblAgentGroups_iBranchID`),
  ADD KEY `idx__rtblAgentGroups__rtblAgentGroups_iChangeSetID` (`_rtblAgentGroups_iChangeSetID`);

--
-- Indexes for table `_rtblagentlockedout`
--
ALTER TABLE `_rtblagentlockedout`
  ADD PRIMARY KEY (`IDAgentLockedOut`),
  ADD KEY `idx__rtblAgentLockedOut__rtblAgentLockedOut_Checksum` (`_rtblAgentLockedOut_Checksum`),
  ADD KEY `idx__rtblAgentLockedOut__rtblAgentLockedOut_dModifiedDate` (`_rtblAgentLockedOut_dModifiedDate`),
  ADD KEY `idx__rtblAgentLockedOut__rtblAgentLockedOut_iBranchID` (`_rtblAgentLockedOut_iBranchID`),
  ADD KEY `idx__rtblAgentLockedOut__rtblAgentLockedOut_iChangeSetID` (`_rtblAgentLockedOut_iChangeSetID`);

--
-- Indexes for table `_rtblagents`
--
ALTER TABLE `_rtblagents`
  ADD PRIMARY KEY (`idAgents`),
  ADD KEY `idx__rtblAgents__rtblAgents_Checksum` (`_rtblAgents_Checksum`),
  ADD KEY `idx__rtblAgents__rtblAgents_dModifiedDate` (`_rtblAgents_dModifiedDate`),
  ADD KEY `idx__rtblAgents__rtblAgents_iBranchID` (`_rtblAgents_iBranchID`),
  ADD KEY `idx__rtblAgents__rtblAgents_iChangeSetID` (`_rtblAgents_iChangeSetID`),
  ADD KEY `idxUserName` (`cAgentName`);

--
-- Indexes for table `_rtblbusclass`
--
ALTER TABLE `_rtblbusclass`
  ADD PRIMARY KEY (`idBusClass`),
  ADD KEY `idx__rtblBusClass__rtblBusClass_Checksum` (`_rtblBusClass_Checksum`),
  ADD KEY `idx__rtblBusClass__rtblBusClass_dModifiedDate` (`_rtblBusClass_dModifiedDate`),
  ADD KEY `idx__rtblBusClass__rtblBusClass_iBranchID` (`_rtblBusClass_iBranchID`),
  ADD KEY `idx__rtblBusClass__rtblBusClass_iChangeSetID` (`_rtblBusClass_iChangeSetID`);

--
-- Indexes for table `_rtblbusdept`
--
ALTER TABLE `_rtblbusdept`
  ADD PRIMARY KEY (`idBusDept`),
  ADD KEY `idx__rtblBusDept__rtblBusDept_Checksum` (`_rtblBusDept_Checksum`),
  ADD KEY `idx__rtblBusDept__rtblBusDept_dModifiedDate` (`_rtblBusDept_dModifiedDate`),
  ADD KEY `idx__rtblBusDept__rtblBusDept_iBranchID` (`_rtblBusDept_iBranchID`),
  ADD KEY `idx__rtblBusDept__rtblBusDept_iChangeSetID` (`_rtblBusDept_iChangeSetID`);

--
-- Indexes for table `_rtblbusdesig`
--
ALTER TABLE `_rtblbusdesig`
  ADD PRIMARY KEY (`idBusDesig`),
  ADD KEY `idx__rtblBusDesig__rtblBusDesig_Checksum` (`_rtblBusDesig_Checksum`),
  ADD KEY `idx__rtblBusDesig__rtblBusDesig_dModifiedDate` (`_rtblBusDesig_dModifiedDate`),
  ADD KEY `idx__rtblBusDesig__rtblBusDesig_iBranchID` (`_rtblBusDesig_iBranchID`),
  ADD KEY `idx__rtblBusDesig__rtblBusDesig_iChangeSetID` (`_rtblBusDesig_iChangeSetID`);

--
-- Indexes for table `_rtblbustype`
--
ALTER TABLE `_rtblbustype`
  ADD PRIMARY KEY (`idBusType`),
  ADD KEY `idx__rtblBusType__rtblBusType_Checksum` (`_rtblBusType_Checksum`),
  ADD KEY `idx__rtblBusType__rtblBusType_dModifiedDate` (`_rtblBusType_dModifiedDate`),
  ADD KEY `idx__rtblBusType__rtblBusType_iBranchID` (`_rtblBusType_iBranchID`),
  ADD KEY `idx__rtblBusType__rtblBusType_iChangeSetID` (`_rtblBusType_iChangeSetID`);

--
-- Indexes for table `_rtblclass`
--
ALTER TABLE `_rtblclass`
  ADD PRIMARY KEY (`idClass`),
  ADD KEY `idx__rtblClass__rtblClass_Checksum` (`_rtblClass_Checksum`),
  ADD KEY `idx__rtblClass__rtblClass_dModifiedDate` (`_rtblClass_dModifiedDate`),
  ADD KEY `idx__rtblClass__rtblClass_iBranchID` (`_rtblClass_iBranchID`),
  ADD KEY `idx__rtblClass__rtblClass_iChangeSetID` (`_rtblClass_iChangeSetID`);

--
-- Indexes for table `_rtblcmdefaults`
--
ALTER TABLE `_rtblcmdefaults`
  ADD PRIMARY KEY (`idCMDefaults`),
  ADD KEY `idx__rtblCMDefaults__rtblCMDefaults_Checksum` (`_rtblCMDefaults_Checksum`),
  ADD KEY `idx__rtblCMDefaults__rtblCMDefaults_dModifiedDate` (`_rtblCMDefaults_dModifiedDate`),
  ADD KEY `idx__rtblCMDefaults__rtblCMDefaults_iBranchID` (`_rtblCMDefaults_iBranchID`),
  ADD KEY `idx__rtblCMDefaults__rtblCMDefaults_iChangeSetID` (`_rtblCMDefaults_iChangeSetID`);

--
-- Indexes for table `_rtblcompetitor`
--
ALTER TABLE `_rtblcompetitor`
  ADD PRIMARY KEY (`idCompetitor`),
  ADD KEY `idx__rtblCompetitor__rtblCompetitor_Checksum` (`_rtblCompetitor_Checksum`),
  ADD KEY `idx__rtblCompetitor__rtblCompetitor_dModifiedDate` (`_rtblCompetitor_dModifiedDate`),
  ADD KEY `idx__rtblCompetitor__rtblCompetitor_iBranchID` (`_rtblCompetitor_iBranchID`),
  ADD KEY `idx__rtblCompetitor__rtblCompetitor_iChangeSetID` (`_rtblCompetitor_iChangeSetID`);

--
-- Indexes for table `_rtblcompetitorproduct`
--
ALTER TABLE `_rtblcompetitorproduct`
  ADD PRIMARY KEY (`IDCompetitorProduct`),
  ADD KEY `idx__rtblCompetitorProduct__rtblCompetitorProduct_Checksum` (`_rtblCompetitorProduct_Checksum`),
  ADD KEY `idx__rtblCompetitorProduct__rtblCompetitorProduct_dModifiedDate` (`_rtblCompetitorProduct_dModifiedDate`),
  ADD KEY `idx__rtblCompetitorProduct__rtblCompetitorProduct_iBranchID` (`_rtblCompetitorProduct_iBranchID`),
  ADD KEY `idx__rtblCompetitorProduct__rtblCompetitorProduct_iChangeSetID` (`_rtblCompetitorProduct_iChangeSetID`);

--
-- Indexes for table `_rtblcompetitorproductlink`
--
ALTER TABLE `_rtblcompetitorproductlink`
  ADD PRIMARY KEY (`idCompetitorProductLink`),
  ADD KEY `idx__rtblCompetitorProductLink__rtblCompetitorProductLink_Chec59` (`_rtblCompetitorProductLink_Checksum`),
  ADD KEY `idx__rtblCompetitorProductLink__rtblCompetitorProductLink_dMod60` (`_rtblCompetitorProductLink_dModifiedDate`),
  ADD KEY `idx__rtblCompetitorProductLink__rtblCompetitorProductLink_iBra61` (`_rtblCompetitorProductLink_iBranchID`),
  ADD KEY `idx__rtblCompetitorProductLink__rtblCompetitorProductLink_iCha62` (`_rtblCompetitorProductLink_iChangeSetID`);

--
-- Indexes for table `_rtblcontractdoclinks`
--
ALTER TABLE `_rtblcontractdoclinks`
  ADD PRIMARY KEY (`idDocLinks`),
  ADD KEY `idx__rtblContractDocLinks__rtblContractDocLinks_Checksum` (`_rtblContractDocLinks_Checksum`),
  ADD KEY `idx__rtblContractDocLinks__rtblContractDocLinks_dModifiedDate` (`_rtblContractDocLinks_dModifiedDate`),
  ADD KEY `idx__rtblContractDocLinks__rtblContractDocLinks_iBranchID` (`_rtblContractDocLinks_iBranchID`),
  ADD KEY `idx__rtblContractDocLinks__rtblContractDocLinks_iChangeSetID` (`_rtblContractDocLinks_iChangeSetID`);

--
-- Indexes for table `_rtblcontracts`
--
ALTER TABLE `_rtblcontracts`
  ADD PRIMARY KEY (`idContracts`),
  ADD KEY `idx__rtblContracts__rtblContracts_Checksum` (`_rtblContracts_Checksum`),
  ADD KEY `idx__rtblContracts__rtblContracts_dModifiedDate` (`_rtblContracts_dModifiedDate`),
  ADD KEY `idx__rtblContracts__rtblContracts_iBranchID` (`_rtblContracts_iBranchID`),
  ADD KEY `idx__rtblContracts__rtblContracts_iChangeSetID` (`_rtblContracts_iChangeSetID`);

--
-- Indexes for table `_rtblcontracttemplates`
--
ALTER TABLE `_rtblcontracttemplates`
  ADD PRIMARY KEY (`idContractTemplates`),
  ADD UNIQUE KEY `IX__rtblContractTemplates` (`idContractTemplates`),
  ADD KEY `idx__rtblContractTemplates__rtblContractTemplates_Checksum` (`_rtblContractTemplates_Checksum`),
  ADD KEY `idx__rtblContractTemplates__rtblContractTemplates_dModifiedDate` (`_rtblContractTemplates_dModifiedDate`),
  ADD KEY `idx__rtblContractTemplates__rtblContractTemplates_iBranchID` (`_rtblContractTemplates_iBranchID`),
  ADD KEY `idx__rtblContractTemplates__rtblContractTemplates_iChangeSetID` (`_rtblContractTemplates_iChangeSetID`);

--
-- Indexes for table `_rtblcontracttx`
--
ALTER TABLE `_rtblcontracttx`
  ADD PRIMARY KEY (`idContractTx`),
  ADD KEY `idx__rtblContractTx__rtblContractTx_Checksum` (`_rtblContractTx_Checksum`),
  ADD KEY `idx__rtblContractTx__rtblContractTx_dModifiedDate` (`_rtblContractTx_dModifiedDate`),
  ADD KEY `idx__rtblContractTx__rtblContractTx_iBranchID` (`_rtblContractTx_iBranchID`),
  ADD KEY `idx__rtblContractTx__rtblContractTx_iChangeSetID` (`_rtblContractTx_iChangeSetID`),
  ADD KEY `idxContractID` (`iContractID`),
  ADD KEY `idxTaskID` (`iIncidentID`);

--
-- Indexes for table `_rtblcountry`
--
ALTER TABLE `_rtblcountry`
  ADD PRIMARY KEY (`idCountry`),
  ADD KEY `idx__rtblCountry__rtblCountry_Checksum` (`_rtblCountry_Checksum`),
  ADD KEY `idx__rtblCountry__rtblCountry_dModifiedDate` (`_rtblCountry_dModifiedDate`),
  ADD KEY `idx__rtblCountry__rtblCountry_iBranchID` (`_rtblCountry_iBranchID`),
  ADD KEY `idx__rtblCountry__rtblCountry_iChangeSetID` (`_rtblCountry_iChangeSetID`);

--
-- Indexes for table `_rtbldoccat`
--
ALTER TABLE `_rtbldoccat`
  ADD PRIMARY KEY (`idDocCat`),
  ADD KEY `idx__rtblDocCat__rtblDocCat_Checksum` (`_rtblDocCat_Checksum`),
  ADD KEY `idx__rtblDocCat__rtblDocCat_dModifiedDate` (`_rtblDocCat_dModifiedDate`),
  ADD KEY `idx__rtblDocCat__rtblDocCat_iBranchID` (`_rtblDocCat_iBranchID`),
  ADD KEY `idx__rtblDocCat__rtblDocCat_iChangeSetID` (`_rtblDocCat_iChangeSetID`);

--
-- Indexes for table `_rtbldoclinks`
--
ALTER TABLE `_rtbldoclinks`
  ADD PRIMARY KEY (`idDocLinks`),
  ADD KEY `idx__rtblDocLinks__rtblDocLinks_Checksum` (`_rtblDocLinks_Checksum`),
  ADD KEY `idx__rtblDocLinks__rtblDocLinks_dModifiedDate` (`_rtblDocLinks_dModifiedDate`),
  ADD KEY `idx__rtblDocLinks__rtblDocLinks_iBranchID` (`_rtblDocLinks_iBranchID`),
  ADD KEY `idx__rtblDocLinks__rtblDocLinks_iChangeSetID` (`_rtblDocLinks_iChangeSetID`),
  ADD KEY `idx_LinkSrc_LinkID` (`iLinkID`,`idDocLinks`,`iDocStoreID`,`iLinkSource`);

--
-- Indexes for table `_rtbldocstore`
--
ALTER TABLE `_rtbldocstore`
  ADD PRIMARY KEY (`idDocStore`),
  ADD KEY `idx__rtblDocStore__rtblDocStore_Checksum` (`_rtblDocStore_Checksum`),
  ADD KEY `idx__rtblDocStore__rtblDocStore_dModifiedDate` (`_rtblDocStore_dModifiedDate`),
  ADD KEY `idx__rtblDocStore__rtblDocStore_iBranchID` (`_rtblDocStore_iBranchID`),
  ADD KEY `idx__rtblDocStore__rtblDocStore_iChangeSetID` (`_rtblDocStore_iChangeSetID`);

--
-- Indexes for table `_rtblescalategrp`
--
ALTER TABLE `_rtblescalategrp`
  ADD PRIMARY KEY (`idEscalateGrp`),
  ADD KEY `idx__rtblEscalateGrp__rtblEscalateGrp_Checksum` (`_rtblEscalateGrp_Checksum`),
  ADD KEY `idx__rtblEscalateGrp__rtblEscalateGrp_dModifiedDate` (`_rtblEscalateGrp_dModifiedDate`),
  ADD KEY `idx__rtblEscalateGrp__rtblEscalateGrp_iBranchID` (`_rtblEscalateGrp_iBranchID`),
  ADD KEY `idx__rtblEscalateGrp__rtblEscalateGrp_iChangeSetID` (`_rtblEscalateGrp_iChangeSetID`);

--
-- Indexes for table `_rtblincidentaction`
--
ALTER TABLE `_rtblincidentaction`
  ADD PRIMARY KEY (`idIncidentAction`),
  ADD KEY `idx__rtblIncidentAction__rtblIncidentAction_Checksum` (`_rtblIncidentAction_Checksum`),
  ADD KEY `idx__rtblIncidentAction__rtblIncidentAction_dModifiedDate` (`_rtblIncidentAction_dModifiedDate`),
  ADD KEY `idx__rtblIncidentAction__rtblIncidentAction_iBranchID` (`_rtblIncidentAction_iBranchID`),
  ADD KEY `idx__rtblIncidentAction__rtblIncidentAction_iChangeSetID` (`_rtblIncidentAction_iChangeSetID`);

--
-- Indexes for table `_rtblincidentcat`
--
ALTER TABLE `_rtblincidentcat`
  ADD PRIMARY KEY (`idIncidentCat`),
  ADD KEY `idx__rtblIncidentCat__rtblIncidentCat_Checksum` (`_rtblIncidentCat_Checksum`),
  ADD KEY `idx__rtblIncidentCat__rtblIncidentCat_dModifiedDate` (`_rtblIncidentCat_dModifiedDate`),
  ADD KEY `idx__rtblIncidentCat__rtblIncidentCat_iBranchID` (`_rtblIncidentCat_iBranchID`),
  ADD KEY `idx__rtblIncidentCat__rtblIncidentCat_iChangeSetID` (`_rtblIncidentCat_iChangeSetID`);

--
-- Indexes for table `_rtblincidentlog`
--
ALTER TABLE `_rtblincidentlog`
  ADD PRIMARY KEY (`idIncidentLog`),
  ADD KEY `idx__rtblIncidentLog__rtblIncidentLog_Checksum` (`_rtblIncidentLog_Checksum`),
  ADD KEY `idx__rtblIncidentLog__rtblIncidentLog_dModifiedDate` (`_rtblIncidentLog_dModifiedDate`),
  ADD KEY `idx__rtblIncidentLog__rtblIncidentLog_iBranchID` (`_rtblIncidentLog_iBranchID`),
  ADD KEY `idx__rtblIncidentLog__rtblIncidentLog_iChangeSetID` (`_rtblIncidentLog_iChangeSetID`),
  ADD KEY `idx_rtblIncidentLog_iAgentID` (`iAgentID`),
  ADD KEY `idxActionDate` (`dActionDate`,`iIncidentActionID`,`iNewAgentID`),
  ADD KEY `idxTaskID` (`iIncidentID`);

--
-- Indexes for table `_rtblincidentpriority`
--
ALTER TABLE `_rtblincidentpriority`
  ADD PRIMARY KEY (`idIncidentPriority`),
  ADD KEY `idx__rtblIncidentPriority__rtblIncidentPriority_Checksum` (`_rtblIncidentPriority_Checksum`),
  ADD KEY `idx__rtblIncidentPriority__rtblIncidentPriority_dModifiedDate` (`_rtblIncidentPriority_dModifiedDate`),
  ADD KEY `idx__rtblIncidentPriority__rtblIncidentPriority_iBranchID` (`_rtblIncidentPriority_iBranchID`),
  ADD KEY `idx__rtblIncidentPriority__rtblIncidentPriority_iChangeSetID` (`_rtblIncidentPriority_iChangeSetID`);

--
-- Indexes for table `_rtblincidents`
--
ALTER TABLE `_rtblincidents`
  ADD PRIMARY KEY (`idIncidents`),
  ADD KEY `idx__rtblIncidents__rtblIncidents_Checksum` (`_rtblIncidents_Checksum`),
  ADD KEY `idx__rtblIncidents__rtblIncidents_dModifiedDate` (`_rtblIncidents_dModifiedDate`),
  ADD KEY `idx__rtblIncidents__rtblIncidents_iBranchID` (`_rtblIncidents_iBranchID`),
  ADD KEY `idx__rtblIncidents__rtblIncidents_iChangeSetID` (`_rtblIncidents_iChangeSetID`),
  ADD KEY `idxCompany` (`iDebtorID`),
  ADD KEY `idxCreated` (`dCreated`),
  ADD KEY `idxCurrentAgent` (`iCurrentAgentID`),
  ADD KEY `idxLastModified` (`dLastModified`),
  ADD KEY `idxPerson` (`iPersonID`);

--
-- Indexes for table `_rtblincidentstatus`
--
ALTER TABLE `_rtblincidentstatus`
  ADD PRIMARY KEY (`idIncidentStatus`),
  ADD KEY `idx__rtblIncidentStatus__rtblIncidentStatus_Checksum` (`_rtblIncidentStatus_Checksum`),
  ADD KEY `idx__rtblIncidentStatus__rtblIncidentStatus_dModifiedDate` (`_rtblIncidentStatus_dModifiedDate`),
  ADD KEY `idx__rtblIncidentStatus__rtblIncidentStatus_iBranchID` (`_rtblIncidentStatus_iBranchID`),
  ADD KEY `idx__rtblIncidentStatus__rtblIncidentStatus_iChangeSetID` (`_rtblIncidentStatus_iChangeSetID`);

--
-- Indexes for table `_rtblincidents_archive`
--
ALTER TABLE `_rtblincidents_archive`
  ADD PRIMARY KEY (`idIncidents`),
  ADD KEY `idx__rtblIncidents_Archive__rtblIncidents_Archive_Checksum` (`_rtblIncidents_Archive_Checksum`),
  ADD KEY `idx__rtblIncidents_Archive__rtblIncidents_Archive_dModifiedDate` (`_rtblIncidents_Archive_dModifiedDate`),
  ADD KEY `idx__rtblIncidents_Archive__rtblIncidents_Archive_iBranchID` (`_rtblIncidents_Archive_iBranchID`),
  ADD KEY `idx__rtblIncidents_Archive__rtblIncidents_Archive_iChangeSetID` (`_rtblIncidents_Archive_iChangeSetID`);

--
-- Indexes for table `_rtblincidenttemplates`
--
ALTER TABLE `_rtblincidenttemplates`
  ADD PRIMARY KEY (`idIncidentTemplates`),
  ADD KEY `idx__rtblIncidentTemplates__rtblIncidentTemplates_Checksum` (`_rtblIncidentTemplates_Checksum`),
  ADD KEY `idx__rtblIncidentTemplates__rtblIncidentTemplates_dModifiedDate` (`_rtblIncidentTemplates_dModifiedDate`),
  ADD KEY `idx__rtblIncidentTemplates__rtblIncidentTemplates_iBranchID` (`_rtblIncidentTemplates_iBranchID`),
  ADD KEY `idx__rtblIncidentTemplates__rtblIncidentTemplates_iChangeSetID` (`_rtblIncidentTemplates_iChangeSetID`);

--
-- Indexes for table `_rtblincidenttype`
--
ALTER TABLE `_rtblincidenttype`
  ADD PRIMARY KEY (`idIncidentType`),
  ADD KEY `idx__rtblIncidentType__rtblIncidentType_Checksum` (`_rtblIncidentType_Checksum`),
  ADD KEY `idx__rtblIncidentType__rtblIncidentType_dModifiedDate` (`_rtblIncidentType_dModifiedDate`),
  ADD KEY `idx__rtblIncidentType__rtblIncidentType_iBranchID` (`_rtblIncidentType_iBranchID`),
  ADD KEY `idx__rtblIncidentType__rtblIncidentType_iChangeSetID` (`_rtblIncidentType_iChangeSetID`);

--
-- Indexes for table `_rtblkbadoclinks`
--
ALTER TABLE `_rtblkbadoclinks`
  ADD PRIMARY KEY (`idDocLinks`),
  ADD KEY `idx__rtblKBADocLinks__rtblKBADocLinks_Checksum` (`_rtblKBADocLinks_Checksum`),
  ADD KEY `idx__rtblKBADocLinks__rtblKBADocLinks_dModifiedDate` (`_rtblKBADocLinks_dModifiedDate`),
  ADD KEY `idx__rtblKBADocLinks__rtblKBADocLinks_iBranchID` (`_rtblKBADocLinks_iBranchID`),
  ADD KEY `idx__rtblKBADocLinks__rtblKBADocLinks_iChangeSetID` (`_rtblKBADocLinks_iChangeSetID`);

--
-- Indexes for table `_rtblkbcategorylinks`
--
ALTER TABLE `_rtblkbcategorylinks`
  ADD PRIMARY KEY (`idCategoryLinks`),
  ADD KEY `idx__rtblKBCategoryLinks__rtblKBCategoryLinks_Checksum` (`_rtblKBCategoryLinks_Checksum`),
  ADD KEY `idx__rtblKBCategoryLinks__rtblKBCategoryLinks_dModifiedDate` (`_rtblKBCategoryLinks_dModifiedDate`),
  ADD KEY `idx__rtblKBCategoryLinks__rtblKBCategoryLinks_iBranchID` (`_rtblKBCategoryLinks_iBranchID`),
  ADD KEY `idx__rtblKBCategoryLinks__rtblKBCategoryLinks_iChangeSetID` (`_rtblKBCategoryLinks_iChangeSetID`);

--
-- Indexes for table `_rtblknowledgebase`
--
ALTER TABLE `_rtblknowledgebase`
  ADD PRIMARY KEY (`idKnowledgeBase`),
  ADD KEY `idx__rtblKnowledgeBase__rtblKnowledgeBase_Checksum` (`_rtblKnowledgeBase_Checksum`),
  ADD KEY `idx__rtblKnowledgeBase__rtblKnowledgeBase_dModifiedDate` (`_rtblKnowledgeBase_dModifiedDate`),
  ADD KEY `idx__rtblKnowledgeBase__rtblKnowledgeBase_iBranchID` (`_rtblKnowledgeBase_iBranchID`),
  ADD KEY `idx__rtblKnowledgeBase__rtblKnowledgeBase_iChangeSetID` (`_rtblKnowledgeBase_iChangeSetID`);

--
-- Indexes for table `_rtblknowledgebasecat`
--
ALTER TABLE `_rtblknowledgebasecat`
  ADD PRIMARY KEY (`idKnowledgeBaseCat`),
  ADD KEY `idx__rtblKnowledgeBaseCat__rtblKnowledgeBaseCat_Checksum` (`_rtblKnowledgeBaseCat_Checksum`),
  ADD KEY `idx__rtblKnowledgeBaseCat__rtblKnowledgeBaseCat_dModifiedDate` (`_rtblKnowledgeBaseCat_dModifiedDate`),
  ADD KEY `idx__rtblKnowledgeBaseCat__rtblKnowledgeBaseCat_iBranchID` (`_rtblKnowledgeBaseCat_iBranchID`),
  ADD KEY `idx__rtblKnowledgeBaseCat__rtblKnowledgeBaseCat_iChangeSetID` (`_rtblKnowledgeBaseCat_iChangeSetID`);

--
-- Indexes for table `_rtblnotify`
--
ALTER TABLE `_rtblnotify`
  ADD PRIMARY KEY (`idNotify`),
  ADD KEY `idx__rtblNotify__rtblNotify_Checksum` (`_rtblNotify_Checksum`),
  ADD KEY `idx__rtblNotify__rtblNotify_dModifiedDate` (`_rtblNotify_dModifiedDate`),
  ADD KEY `idx__rtblNotify__rtblNotify_iBranchID` (`_rtblNotify_iBranchID`),
  ADD KEY `idx__rtblNotify__rtblNotify_iChangeSetID` (`_rtblNotify_iChangeSetID`),
  ADD KEY `idx_ForAgent` (`iForAgentID`);

--
-- Indexes for table `_rtblopportunity`
--
ALTER TABLE `_rtblopportunity`
  ADD PRIMARY KEY (`IDOpportunity`),
  ADD KEY `idx__rtblOpportunity__rtblOpportunity_Checksum` (`_rtblOpportunity_Checksum`),
  ADD KEY `idx__rtblOpportunity__rtblOpportunity_dModifiedDate` (`_rtblOpportunity_dModifiedDate`),
  ADD KEY `idx__rtblOpportunity__rtblOpportunity_iBranchID` (`_rtblOpportunity_iBranchID`),
  ADD KEY `idx__rtblOpportunity__rtblOpportunity_iChangeSetID` (`_rtblOpportunity_iChangeSetID`);

--
-- Indexes for table `_rtblopportunitycompetitor`
--
ALTER TABLE `_rtblopportunitycompetitor`
  ADD PRIMARY KEY (`idOpportunityCompetitor`),
  ADD KEY `idx__rtblOpportunityCompetitor__rtblOpportunityCompetitor_Chec17` (`_rtblOpportunityCompetitor_Checksum`),
  ADD KEY `idx__rtblOpportunityCompetitor__rtblOpportunityCompetitor_dMod18` (`_rtblOpportunityCompetitor_dModifiedDate`),
  ADD KEY `idx__rtblOpportunityCompetitor__rtblOpportunityCompetitor_iBra19` (`_rtblOpportunityCompetitor_iBranchID`),
  ADD KEY `idx__rtblOpportunityCompetitor__rtblOpportunityCompetitor_iCha20` (`_rtblOpportunityCompetitor_iChangeSetID`);

--
-- Indexes for table `_rtblopportunitydoclinks`
--
ALTER TABLE `_rtblopportunitydoclinks`
  ADD PRIMARY KEY (`IDDocLinks`),
  ADD KEY `idx__rtblOpportunityDocLinks__rtblOpportunityDocLinks_Checksum` (`_rtblOpportunityDocLinks_Checksum`),
  ADD KEY `idx__rtblOpportunityDocLinks__rtblOpportunityDocLinks_dModifie26` (`_rtblOpportunityDocLinks_dModifiedDate`),
  ADD KEY `idx__rtblOpportunityDocLinks__rtblOpportunityDocLinks_iBranchID` (`_rtblOpportunityDocLinks_iBranchID`),
  ADD KEY `idx__rtblOpportunityDocLinks__rtblOpportunityDocLinks_iChangeS27` (`_rtblOpportunityDocLinks_iChangeSetID`);

--
-- Indexes for table `_rtblopportunitysource`
--
ALTER TABLE `_rtblopportunitysource`
  ADD PRIMARY KEY (`IDOpportunitySource`),
  ADD KEY `idx__rtblOpportunitySource__rtblOpportunitySource_Checksum` (`_rtblOpportunitySource_Checksum`),
  ADD KEY `idx__rtblOpportunitySource__rtblOpportunitySource_dModifiedDate` (`_rtblOpportunitySource_dModifiedDate`),
  ADD KEY `idx__rtblOpportunitySource__rtblOpportunitySource_iBranchID` (`_rtblOpportunitySource_iBranchID`),
  ADD KEY `idx__rtblOpportunitySource__rtblOpportunitySource_iChangeSetID` (`_rtblOpportunitySource_iChangeSetID`);

--
-- Indexes for table `_rtblopportunitystage`
--
ALTER TABLE `_rtblopportunitystage`
  ADD PRIMARY KEY (`IDOpportunityStage`),
  ADD KEY `idx__rtblOpportunityStage__rtblOpportunityStage_Checksum` (`_rtblOpportunityStage_Checksum`),
  ADD KEY `idx__rtblOpportunityStage__rtblOpportunityStage_dModifiedDate` (`_rtblOpportunityStage_dModifiedDate`),
  ADD KEY `idx__rtblOpportunityStage__rtblOpportunityStage_iBranchID` (`_rtblOpportunityStage_iBranchID`),
  ADD KEY `idx__rtblOpportunityStage__rtblOpportunityStage_iChangeSetID` (`_rtblOpportunityStage_iChangeSetID`);

--
-- Indexes for table `_rtblopportunitystatus`
--
ALTER TABLE `_rtblopportunitystatus`
  ADD PRIMARY KEY (`IDOpportunityStatus`),
  ADD KEY `idx__rtblOpportunityStatus__rtblOpportunityStatus_Checksum` (`_rtblOpportunityStatus_Checksum`),
  ADD KEY `idx__rtblOpportunityStatus__rtblOpportunityStatus_dModifiedDate` (`_rtblOpportunityStatus_dModifiedDate`),
  ADD KEY `idx__rtblOpportunityStatus__rtblOpportunityStatus_iBranchID` (`_rtblOpportunityStatus_iBranchID`),
  ADD KEY `idx__rtblOpportunityStatus__rtblOpportunityStatus_iChangeSetID` (`_rtblOpportunityStatus_iChangeSetID`);

--
-- Indexes for table `_rtblpeople`
--
ALTER TABLE `_rtblpeople`
  ADD PRIMARY KEY (`idPeople`),
  ADD KEY `idx__rtblPeople__rtblPeople_Checksum` (`_rtblPeople_Checksum`),
  ADD KEY `idx__rtblPeople__rtblPeople_dModifiedDate` (`_rtblPeople_dModifiedDate`),
  ADD KEY `idx__rtblPeople__rtblPeople_iBranchID` (`_rtblPeople_iBranchID`),
  ADD KEY `idx__rtblPeople__rtblPeople_iChangeSetID` (`_rtblPeople_iChangeSetID`);

--
-- Indexes for table `_rtblpeoplelinks`
--
ALTER TABLE `_rtblpeoplelinks`
  ADD PRIMARY KEY (`idPeopleLinks`),
  ADD KEY `idxPeopleID` (`iPeopleID`),
  ADD KEY `idx__rtblPeopleLinks__rtblPeopleLinks_Checksum` (`_rtblPeopleLinks_Checksum`),
  ADD KEY `idx__rtblPeopleLinks__rtblPeopleLinks_dModifiedDate` (`_rtblPeopleLinks_dModifiedDate`),
  ADD KEY `idx__rtblPeopleLinks__rtblPeopleLinks_iBranchID` (`_rtblPeopleLinks_iBranchID`),
  ADD KEY `idx__rtblPeopleLinks__rtblPeopleLinks_iChangeSetID` (`_rtblPeopleLinks_iChangeSetID`),
  ADD KEY `idxCompanyID` (`iDebtorID`);

--
-- Indexes for table `_rtblprospect`
--
ALTER TABLE `_rtblprospect`
  ADD PRIMARY KEY (`IDProspect`),
  ADD KEY `idx__rtblProspect__rtblProspect_Checksum` (`_rtblProspect_Checksum`),
  ADD KEY `idx__rtblProspect__rtblProspect_dModifiedDate` (`_rtblProspect_dModifiedDate`),
  ADD KEY `idx__rtblProspect__rtblProspect_iBranchID` (`_rtblProspect_iBranchID`),
  ADD KEY `idx__rtblProspect__rtblProspect_iChangeSetID` (`_rtblProspect_iChangeSetID`);

--
-- Indexes for table `_rtblrefbase`
--
ALTER TABLE `_rtblrefbase`
  ADD PRIMARY KEY (`idRefBase`),
  ADD KEY `idx__rtblRefBase__rtblRefBase_Checksum` (`_rtblRefBase_Checksum`),
  ADD KEY `idx__rtblRefBase__rtblRefBase_dModifiedDate` (`_rtblRefBase_dModifiedDate`),
  ADD KEY `idx__rtblRefBase__rtblRefBase_iBranchID` (`_rtblRefBase_iBranchID`),
  ADD KEY `idx__rtblRefBase__rtblRefBase_iChangeSetID` (`_rtblRefBase_iChangeSetID`),
  ADD KEY `IX__rtblRefBase_cRefType` (`cRefType`);

--
-- Indexes for table `_rtblrefbook`
--
ALTER TABLE `_rtblrefbook`
  ADD PRIMARY KEY (`idRefBook`),
  ADD KEY `idx__rtblRefBook__rtblRefBook_Checksum` (`_rtblRefBook_Checksum`),
  ADD KEY `idx__rtblRefBook__rtblRefBook_dModifiedDate` (`_rtblRefBook_dModifiedDate`),
  ADD KEY `idx__rtblRefBook__rtblRefBook_iBranchID` (`_rtblRefBook_iBranchID`),
  ADD KEY `idx__rtblRefBook__rtblRefBook_iChangeSetID` (`_rtblRefBook_iChangeSetID`),
  ADD KEY `IX__rtblRefBook_iRefBaseID_iBookedNo` (`iRefBaseID`,`iBookedNo`);

--
-- Indexes for table `_rtblstocklinks`
--
ALTER TABLE `_rtblstocklinks`
  ADD PRIMARY KEY (`idStockLinks`),
  ADD KEY `idx__rtblStockLinks__rtblStockLinks_Checksum` (`_rtblStockLinks_Checksum`),
  ADD KEY `idx__rtblStockLinks__rtblStockLinks_dModifiedDate` (`_rtblStockLinks_dModifiedDate`),
  ADD KEY `idx__rtblStockLinks__rtblStockLinks_iBranchID` (`_rtblStockLinks_iBranchID`),
  ADD KEY `idx__rtblStockLinks__rtblStockLinks_iChangeSetID` (`_rtblStockLinks_iChangeSetID`),
  ADD KEY `idxStockLinks_DCLink` (`iDCLink`),
  ADD KEY `idxStockLinks_StockID` (`iStockID`),
  ADD KEY `idxStockLinks_WhseID` (`iWhseID`);

--
-- Indexes for table `_rtbluserdict`
--
ALTER TABLE `_rtbluserdict`
  ADD PRIMARY KEY (`idUserDict`),
  ADD KEY `idx__rtblUserDict__rtblUserDict_Checksum` (`_rtblUserDict_Checksum`),
  ADD KEY `idx__rtblUserDict__rtblUserDict_dModifiedDate` (`_rtblUserDict_dModifiedDate`),
  ADD KEY `idx__rtblUserDict__rtblUserDict_iBranchID` (`_rtblUserDict_iBranchID`),
  ADD KEY `idx__rtblUserDict__rtblUserDict_iChangeSetID` (`_rtblUserDict_iChangeSetID`),
  ADD KEY `idxcTableName` (`cTableName`);

--
-- Indexes for table `_rtblworkcal`
--
ALTER TABLE `_rtblworkcal`
  ADD PRIMARY KEY (`idWorkCal`),
  ADD KEY `idx__rtblWorkCal__rtblWorkCal_Checksum` (`_rtblWorkCal_Checksum`),
  ADD KEY `idx__rtblWorkCal__rtblWorkCal_dModifiedDate` (`_rtblWorkCal_dModifiedDate`),
  ADD KEY `idx__rtblWorkCal__rtblWorkCal_iBranchID` (`_rtblWorkCal_iBranchID`),
  ADD KEY `idx__rtblWorkCal__rtblWorkCal_iChangeSetID` (`_rtblWorkCal_iChangeSetID`);

--
-- Indexes for table `_rtblworkcalexdates`
--
ALTER TABLE `_rtblworkcalexdates`
  ADD PRIMARY KEY (`idWorkCalExDates`),
  ADD KEY `idx__rtblWorkCalExDates__rtblWorkCalExDates_Checksum` (`_rtblWorkCalExDates_Checksum`),
  ADD KEY `idx__rtblWorkCalExDates__rtblWorkCalExDates_dModifiedDate` (`_rtblWorkCalExDates_dModifiedDate`),
  ADD KEY `idx__rtblWorkCalExDates__rtblWorkCalExDates_iBranchID` (`_rtblWorkCalExDates_iBranchID`),
  ADD KEY `idx__rtblWorkCalExDates__rtblWorkCalExDates_iChangeSetID` (`_rtblWorkCalExDates_iChangeSetID`);

--
-- Indexes for table `_simtblreqheader`
--
ALTER TABLE `_simtblreqheader`
  ADD PRIMARY KEY (`idReqHeader`);

--
-- Indexes for table `_simtblreqlines`
--
ALTER TABLE `_simtblreqlines`
  ADD PRIMARY KEY (`idReqLines`);

--
-- Indexes for table `_simtblstkissuelines`
--
ALTER TABLE `_simtblstkissuelines`
  ADD PRIMARY KEY (`iAutoIdx`);

--
-- Indexes for table `_simtblstockissuelinesn`
--
ALTER TABLE `_simtblstockissuelinesn`
  ADD PRIMARY KEY (`idStockIssueLineSN`);

--
-- Indexes for table `_simtblstockissuemaster`
--
ALTER TABLE `_simtblstockissuemaster`
  ADD PRIMARY KEY (`iStkIssueId`);

--
-- Indexes for table `_wtblipaddetails`
--
ALTER TABLE `_wtblipaddetails`
  ADD PRIMARY KEY (`idIPadNumber`);

--
-- Indexes for table `_wtblipaduser`
--
ALTER TABLE `_wtblipaduser`
  ADD PRIMARY KEY (`idIPadUser`);

--
-- Indexes for table `_wtblpemmobilitymodules`
--
ALTER TABLE `_wtblpemmobilitymodules`
  ADD PRIMARY KEY (`idMobilityModule`),
  ADD UNIQUE KEY `gIdentifier` (`gIdentifier`);

--
-- Indexes for table `_wtblsystem`
--
ALTER TABLE `_wtblsystem`
  ADD PRIMARY KEY (`idSystem`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `AccountLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `idAreas` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bankmain`
--
ALTER TABLE `bankmain`
  MODIFY `Counter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bomcomp`
--
ALTER TABLE `bomcomp`
  MODIFY `BomComponentKey` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bomdef`
--
ALTER TABLE `bomdef`
  MODIFY `idBomDef` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bommast`
--
ALTER TABLE `bommast`
  MODIFY `BomID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ccdefs`
--
ALTER TABLE `ccdefs`
  MODIFY `idCCDefs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cliclass`
--
ALTER TABLE `cliclass`
  MODIFY `IdCliClass` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clidef`
--
ALTER TABLE `clidef`
  MODIFY `idCliDef` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `DCLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `Autoidx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `costcntr`
--
ALTER TABLE `costcntr`
  MODIFY `Counter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crdischd`
--
ALTER TABLE `crdischd`
  MODIFY `idCrDiscHd` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crdiscmx`
--
ALTER TABLE `crdiscmx`
  MODIFY `idCrDiscMx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `credmnt`
--
ALTER TABLE `credmnt`
  MODIFY `Autoidx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `CurrencyLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currencyhist`
--
ALTER TABLE `currencyhist`
  MODIFY `idCurrencyHist` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cwratio`
--
ALTER TABLE `cwratio`
  MODIFY `idCwRatio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deltbl`
--
ALTER TABLE `deltbl`
  MODIFY `Counter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dept`
--
ALTER TABLE `dept`
  MODIFY `idDept` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drdischd`
--
ALTER TABLE `drdischd`
  MODIFY `idDrDiscHd` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drdiscmx`
--
ALTER TABLE `drdiscmx`
  MODIFY `idDrDiscMx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entities`
--
ALTER TABLE `entities`
  MODIFY `idEntities` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `glbranch`
--
ALTER TABLE `glbranch`
  MODIFY `idGLBranch` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grptbl`
--
ALTER TABLE `grptbl`
  MODIFY `idGrpTbl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invnum`
--
ALTER TABLE `invnum`
  MODIFY `AutoIndex` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobdef`
--
ALTER TABLE `jobdef`
  MODIFY `idJobDef` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobnum`
--
ALTER TABLE `jobnum`
  MODIFY `AutoIndex` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobtxtp`
--
ALTER TABLE `jobtxtp`
  MODIFY `idJobTxTp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mastoffs`
--
ALTER TABLE `mastoffs`
  MODIFY `idMastOffs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nt_suppliers`
--
ALTER TABLE `nt_suppliers`
  MODIFY `NTSupID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nt_suppliers_audit`
--
ALTER TABLE `nt_suppliers_audit`
  MODIFY `iNTSupAuditID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `open_sessions`
--
ALTER TABLE `open_sessions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ordersdf`
--
ALTER TABLE `ordersdf`
  MODIFY `DefaultCounter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ordersst`
--
ALTER TABLE `ordersst`
  MODIFY `StatusCounter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pcktbl`
--
ALTER TABLE `pcktbl`
  MODIFY `idPckTbl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `periodpermissions`
--
ALTER TABLE `periodpermissions`
  MODIFY `idPeriodPermissions` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `petty_cashes`
--
ALTER TABLE `petty_cashes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `petty_cash_types`
--
ALTER TABLE `petty_cash_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pinneditems`
--
ALTER TABLE `pinneditems`
  MODIFY `PinnedItemID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posdefs`
--
ALTER TABLE `posdefs`
  MODIFY `IDPOSDefs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `postap`
--
ALTER TABLE `postap`
  MODIFY `AutoIdx` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `postar`
--
ALTER TABLE `postar`
  MODIFY `AutoIdx` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `postgl`
--
ALTER TABLE `postgl`
  MODIFY `AutoIdx` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `postst`
--
ALTER TABLE `postst`
  MODIFY `AutoIdx` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `printgrp`
--
ALTER TABLE `printgrp`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `ProjectLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recentitems`
--
ALTER TABLE `recentitems`
  MODIFY `RecentItemID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recurrc`
--
ALTER TABLE `recurrc`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recurrf`
--
ALTER TABLE `recurrf`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recurrl`
--
ALTER TABLE `recurrl`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recurrtx`
--
ALTER TABLE `recurrtx`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `refer`
--
ALTER TABLE `refer`
  MODIFY `AutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reminder`
--
ALTER TABLE `reminder`
  MODIFY `Autoidx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq`
--
ALTER TABLE `rfq`
  MODIFY `iRFQID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfqdf`
--
ALTER TABLE `rfqdf`
  MODIFY `DefaultCounter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_agentcostcentremap`
--
ALTER TABLE `rfq_agentcostcentremap`
  MODIFY `idAgentCostCentreMap` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_agentsectormapping`
--
ALTER TABLE `rfq_agentsectormapping`
  MODIFY `idAgentSectorMap` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_apshareholderlinks`
--
ALTER TABLE `rfq_apshareholderlinks`
  MODIFY `idAPShareholderLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_audittables`
--
ALTER TABLE `rfq_audittables`
  MODIFY `iAuditTableID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_costcentre`
--
ALTER TABLE `rfq_costcentre`
  MODIFY `idCostCentre` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_deviationreason`
--
ALTER TABLE `rfq_deviationreason`
  MODIFY `idDeviationReason` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_deviations`
--
ALTER TABLE `rfq_deviations`
  MODIFY `idDeviation` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_event`
--
ALTER TABLE `rfq_event`
  MODIFY `iEventID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_fileattachment`
--
ALTER TABLE `rfq_fileattachment`
  MODIFY `iFileID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_newquotationparams`
--
ALTER TABLE `rfq_newquotationparams`
  MODIFY `PK_NewQuotationParamID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_newtender`
--
ALTER TABLE `rfq_newtender`
  MODIFY `PK_NewTenderID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_newtenderdetails`
--
ALTER TABLE `rfq_newtenderdetails`
  MODIFY `PK_NewTenderDetailID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_newtenderparams`
--
ALTER TABLE `rfq_newtenderparams`
  MODIFY `PK_NewTenderParamID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_notes`
--
ALTER TABLE `rfq_notes`
  MODIFY `iNoteID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_parametercriteria`
--
ALTER TABLE `rfq_parametercriteria`
  MODIFY `idRfqParam` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_parameterudf`
--
ALTER TABLE `rfq_parameterudf`
  MODIFY `idRfqParamUdf` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_peoplelinks`
--
ALTER TABLE `rfq_peoplelinks`
  MODIFY `iPeopleLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_recordquotationparams`
--
ALTER TABLE `rfq_recordquotationparams`
  MODIFY `PK_RecordQuotationParamID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_recordtender`
--
ALTER TABLE `rfq_recordtender`
  MODIFY `PK_RecordTender` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_recordtenderdetails`
--
ALTER TABLE `rfq_recordtenderdetails`
  MODIFY `PK_RecordTenderDetailID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_recordtenderparams`
--
ALTER TABLE `rfq_recordtenderparams`
  MODIFY `PK_RecordTenderParamID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_sector`
--
ALTER TABLE `rfq_sector`
  MODIFY `idSector` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_stocklinks`
--
ALTER TABLE `rfq_stocklinks`
  MODIFY `idStockLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_supplierfiltering`
--
ALTER TABLE `rfq_supplierfiltering`
  MODIFY `idSupplierUDF` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_supplierpreference`
--
ALTER TABLE `rfq_supplierpreference`
  MODIFY `idSupplierUDF` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_tenderdf`
--
ALTER TABLE `rfq_tenderdf`
  MODIFY `PK_Default` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_tenderparameters`
--
ALTER TABLE `rfq_tenderparameters`
  MODIFY `PK_TenderParamID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_udf`
--
ALTER TABLE `rfq_udf`
  MODIFY `idUDF` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_vendor`
--
ALTER TABLE `rfq_vendor`
  MODIFY `idVendor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_vendorparameter`
--
ALTER TABLE `rfq_vendorparameter`
  MODIFY `VendorParameterID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_vendorscore`
--
ALTER TABLE `rfq_vendorscore`
  MODIFY `idVendorScore` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfq_workflowlink`
--
ALTER TABLE `rfq_workflowlink`
  MODIFY `iWorkflowLinkID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salesrep`
--
ALTER TABLE `salesrep`
  MODIFY `idSalesRep` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `serialmf`
--
ALTER TABLE `serialmf`
  MODIFY `SerialCounter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `serialtx`
--
ALTER TABLE `serialtx`
  MODIFY `SNTxCounter` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `simplesettings`
--
ALTER TABLE `simplesettings`
  MODIFY `SimpleSettingID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sliplay`
--
ALTER TABLE `sliplay`
  MODIFY `IDSlipLay` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stdftbl`
--
ALTER TABLE `stdftbl`
  MODIFY `idStDfTbl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stkitem`
--
ALTER TABLE `stkitem`
  MODIFY `StockLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxrate`
--
ALTER TABLE `taxrate`
  MODIFY `idTaxRate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tender`
--
ALTER TABLE `tender`
  MODIFY `IdTender` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tills`
--
ALTER TABLE `tills`
  MODIFY `IdTills` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trcodes`
--
ALTER TABLE `trcodes`
  MODIFY `idTrCodes` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `venclass`
--
ALTER TABLE `venclass`
  MODIFY `idVenClass` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendef`
--
ALTER TABLE `vendef`
  MODIFY `idVenDef` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `DCLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `whsemst`
--
ALTER TABLE `whsemst`
  MODIFY `WhseLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `whsestk`
--
ALTER TABLE `whsestk`
  MODIFY `IdWhseStk` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wht_batch`
--
ALTER TABLE `wht_batch`
  MODIFY `idBatch` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wht_batchstatus`
--
ALTER TABLE `wht_batchstatus`
  MODIFY `idBatchNumber` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wht_taxmaster`
--
ALTER TABLE `wht_taxmaster`
  MODIFY `idTaxMaster` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wht_userdetails`
--
ALTER TABLE `wht_userdetails`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailaccounts`
--
ALTER TABLE `_atblbulkemailaccounts`
  MODIFY `idBulkEmailAccount` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailfilters`
--
ALTER TABLE `_atblbulkemailfilters`
  MODIFY `idBulkEmailFilter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailhistory`
--
ALTER TABLE `_atblbulkemailhistory`
  MODIFY `idBulkEmailHistory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailhistorydocuments`
--
ALTER TABLE `_atblbulkemailhistorydocuments`
  MODIFY `idBulkEmailHistoryDocument` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailtemplatedata`
--
ALTER TABLE `_atblbulkemailtemplatedata`
  MODIFY `idBulkEmailTemplateData` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailtemplates`
--
ALTER TABLE `_atblbulkemailtemplates`
  MODIFY `idBulkEmailTemplate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblbulkemailudffilters`
--
ALTER TABLE `_atblbulkemailudffilters`
  MODIFY `idBulkEmailUDFFilter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblcolumnlookups`
--
ALTER TABLE `_atblcolumnlookups`
  MODIFY `idColumnLookup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblcolumnlookupvalues`
--
ALTER TABLE `_atblcolumnlookupvalues`
  MODIFY `idColumnLookupValue` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblcolumns`
--
ALTER TABLE `_atblcolumns`
  MODIFY `idColumn` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbldocdefaults`
--
ALTER TABLE `_atbldocdefaults`
  MODIFY `idDocDefault` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbldocimportdocumenttemplates`
--
ALTER TABLE `_atbldocimportdocumenttemplates`
  MODIFY `idDocImportDocumentTemplate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbldocimportdocumenttemplatetables`
--
ALTER TABLE `_atbldocimportdocumenttemplatetables`
  MODIFY `idDocumentTemplateTable` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbldocimportdocumenttypes`
--
ALTER TABLE `_atbldocimportdocumenttypes`
  MODIFY `idDocImportDocumentTypes` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbldocimportfieldmappings`
--
ALTER TABLE `_atbldocimportfieldmappings`
  MODIFY `idDocImportFieldMapping` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblemailaccounts`
--
ALTER TABLE `_atblemailaccounts`
  MODIFY `idEmailAccount` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblexportdefaults`
--
ALTER TABLE `_atblexportdefaults`
  MODIFY `idExportDefault` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblexportfieldmappings`
--
ALTER TABLE `_atblexportfieldmappings`
  MODIFY `idExportFieldMapping` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atblexporttemplates`
--
ALTER TABLE `_atblexporttemplates`
  MODIFY `idExportTemplate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbltablerelationships`
--
ALTER TABLE `_atbltablerelationships`
  MODIFY `idTableRelationship` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_atbltables`
--
ALTER TABLE `_atbltables`
  MODIFY `idTable` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblagentoptions`
--
ALTER TABLE `_btblagentoptions`
  MODIFY `IDAgentOptions` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblagentoutoffice`
--
ALTER TABLE `_btblagentoutoffice`
  MODIFY `IDAgentOutOffice` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblagentoutofficereasons`
--
ALTER TABLE `_btblagentoutofficereasons`
  MODIFY `iAgentOutOfficeID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblagentsystemtree`
--
ALTER TABLE `_btblagentsystemtree`
  MODIFY `idAgentSystemTree` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblages`
--
ALTER TABLE `_btblages`
  MODIFY `idAges` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblbatchcheckout`
--
ALTER TABLE `_btblbatchcheckout`
  MODIFY `idBatchCheckout` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblbinlocation`
--
ALTER TABLE `_btblbinlocation`
  MODIFY `idBinLocation` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbbankimportdefaults`
--
ALTER TABLE `_btblcbbankimportdefaults`
  MODIFY `idCBBankImportDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbbatchdefs`
--
ALTER TABLE `_btblcbbatchdefs`
  MODIFY `idBatchDefs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbbatches`
--
ALTER TABLE `_btblcbbatches`
  MODIFY `idBatches` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbbatchlines`
--
ALTER TABLE `_btblcbbatchlines`
  MODIFY `idBatchLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbimporthistory`
--
ALTER TABLE `_btblcbimporthistory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbmatchrules`
--
ALTER TABLE `_btblcbmatchrules`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcbstatement`
--
ALTER TABLE `_btblcbstatement`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcmevent`
--
ALTER TABLE `_btblcmevent`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcmeventattendees`
--
ALTER TABLE `_btblcmeventattendees`
  MODIFY `idCMEventAttendees` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcmworkflow`
--
ALTER TABLE `_btblcmworkflow`
  MODIFY `idWorkflow` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcmworkflowmembers`
--
ALTER TABLE `_btblcmworkflowmembers`
  MODIFY `idWorkflowMembers` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblcmworkflowstatus`
--
ALTER TABLE `_btblcmworkflowstatus`
  MODIFY `idWorkflowStatus` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaasset`
--
ALTER TABLE `_btblfaasset`
  MODIFY `idAssetNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassetblock`
--
ALTER TABLE `_btblfaassetblock`
  MODIFY `idAssetBlockNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassetimages`
--
ALTER TABLE `_btblfaassetimages`
  MODIFY `idImageNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassetserialno`
--
ALTER TABLE `_btblfaassetserialno`
  MODIFY `idSerialNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassettracking`
--
ALTER TABLE `_btblfaassettracking`
  MODIFY `idAssetTracking` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassettrackinglines`
--
ALTER TABLE `_btblfaassettrackinglines`
  MODIFY `idAssetTrackingLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassettype`
--
ALTER TABLE `_btblfaassettype`
  MODIFY `idAssetTypeNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaassetunitsofusage`
--
ALTER TABLE `_btblfaassetunitsofusage`
  MODIFY `idAssetUnitNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfacapexbudget`
--
ALTER TABLE `_btblfacapexbudget`
  MODIFY `idCapexBudgetNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfacapexorder`
--
ALTER TABLE `_btblfacapexorder`
  MODIFY `idCapexOrderNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfacapexphasing`
--
ALTER TABLE `_btblfacapexphasing`
  MODIFY `idCapexPhasingNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfacompanysetup`
--
ALTER TABLE `_btblfacompanysetup`
  MODIFY `iFACompanyNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfadepreciationmethod`
--
ALTER TABLE `_btblfadepreciationmethod`
  MODIFY `idDepreciationNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfadepreciationyear`
--
ALTER TABLE `_btblfadepreciationyear`
  MODIFY `idDepreciationYearNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfafinancemethod`
--
ALTER TABLE `_btblfafinancemethod`
  MODIFY `idFinMethod` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaglbatch`
--
ALTER TABLE `_btblfaglbatch`
  MODIFY `idBatch` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaglbatchassetvalues`
--
ALTER TABLE `_btblfaglbatchassetvalues`
  MODIFY `idBatchAssetValues` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaglbatchglentries`
--
ALTER TABLE `_btblfaglbatchglentries`
  MODIFY `idBatchGLEntries` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfaglperiod`
--
ALTER TABLE `_btblfaglperiod`
  MODIFY `idGLPeriodNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfagltotalassetvalues`
--
ALTER TABLE `_btblfagltotalassetvalues`
  MODIFY `idTotalAssetValues` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfagltotalglentries`
--
ALTER TABLE `_btblfagltotalglentries`
  MODIFY `idTotalGLEntries` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfalocation`
--
ALTER TABLE `_btblfalocation`
  MODIFY `idLocationNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfamovementtransaction`
--
ALTER TABLE `_btblfamovementtransaction`
  MODIFY `idMovementTransactionNo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblfatxdefaultglaccounts`
--
ALTER TABLE `_btblfatxdefaultglaccounts`
  MODIFY `idTXDefaultGLAccount` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvcount`
--
ALTER TABLE `_btblinvcount`
  MODIFY `idInvCount` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvcountlines`
--
ALTER TABLE `_btblinvcountlines`
  MODIFY `idInvCountLines` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvcountlinesuom`
--
ALTER TABLE `_btblinvcountlinesuom`
  MODIFY `IDInvCountLinesUOM` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvoicegrvsplit`
--
ALTER TABLE `_btblinvoicegrvsplit`
  MODIFY `idInvoiceGrvSplit` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvoicelines`
--
ALTER TABLE `_btblinvoicelines`
  MODIFY `idInvoiceLines` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvoicelinesn`
--
ALTER TABLE `_btblinvoicelinesn`
  MODIFY `idInvoiceLineSN` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblinvoicemessages`
--
ALTER TABLE `_btblinvoicemessages`
  MODIFY `idInvoiceMessages` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljcinvoicelines`
--
ALTER TABLE `_btbljcinvoicelines`
  MODIFY `idJCInvoiceLines` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljcmaster`
--
ALTER TABLE `_btbljcmaster`
  MODIFY `IdJCMaster` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljctxlines`
--
ALTER TABLE `_btbljctxlines`
  MODIFY `idJCTxLines` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljobfiscaltaxes`
--
ALTER TABLE `_btbljobfiscaltaxes`
  MODIFY `idInvoiceTaxes` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljrbatchdefs`
--
ALTER TABLE `_btbljrbatchdefs`
  MODIFY `idBatchDefs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljrbatches`
--
ALTER TABLE `_btbljrbatches`
  MODIFY `idBatches` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbljrbatchlines`
--
ALTER TABLE `_btbljrbatchlines`
  MODIFY `idBatchLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbllogdetails`
--
ALTER TABLE `_btbllogdetails`
  MODIFY `idLogDetails` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblnotes`
--
ALTER TABLE `_btblnotes`
  MODIFY `idNotes` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblpostendertx`
--
ALTER TABLE `_btblpostendertx`
  MODIFY `IDPOSTenderTx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblposxztable`
--
ALTER TABLE `_btblposxztable`
  MODIFY `IDPOSXZTable` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblserialnumberlink`
--
ALTER TABLE `_btblserialnumberlink`
  MODIFY `IDSerialNumberLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btblstate`
--
ALTER TABLE `_btblstate`
  MODIFY `idState` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbltmcalcsheet`
--
ALTER TABLE `_btbltmcalcsheet`
  MODIFY `idCalcSheet` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbltmtaxbox`
--
ALTER TABLE `_btbltmtaxbox`
  MODIFY `idTaxBox` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbltmtaxperiod`
--
ALTER TABLE `_btbltmtaxperiod`
  MODIFY `idTaxPeriod` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_btbltmtaxtotals`
--
ALTER TABLE `_btbltmtaxtotals`
  MODIFY `idTaxTotals` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbladdinregister`
--
ALTER TABLE `_etbladdinregister`
  MODIFY `idAddIn` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbladditionalcharges`
--
ALTER TABLE `_etbladditionalcharges`
  MODIFY `idAdditionalCharge` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblagentdocprofiles`
--
ALTER TABLE `_etblagentdocprofiles`
  MODIFY `idAgentDocProfiles` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblallocstemp`
--
ALTER TABLE `_etblallocstemp`
  MODIFY `idAllocsTemp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblapshareholderlinks`
--
ALTER TABLE `_etblapshareholderlinks`
  MODIFY `idAPShareholderLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblapshareholders`
--
ALTER TABLE `_etblapshareholders`
  MODIFY `idAPShareholders` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatchcontrasplit`
--
ALTER TABLE `_etblarapbatchcontrasplit`
  MODIFY `idARAPBatchContraSplit` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatchcontrasplithistory`
--
ALTER TABLE `_etblarapbatchcontrasplithistory`
  MODIFY `idARAPBatchContraSplitHistory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatchdefaults`
--
ALTER TABLE `_etblarapbatchdefaults`
  MODIFY `idARAPBatchDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatches`
--
ALTER TABLE `_etblarapbatches`
  MODIFY `idARAPBatches` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatchhistory`
--
ALTER TABLE `_etblarapbatchhistory`
  MODIFY `idARAPBatchHistory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatchhistorylines`
--
ALTER TABLE `_etblarapbatchhistorylines`
  MODIFY `idARAPBatchHistoryLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarapbatchlines`
--
ALTER TABLE `_etblarapbatchlines`
  MODIFY `idARAPBatchLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarstatementrun`
--
ALTER TABLE `_etblarstatementrun`
  MODIFY `idStatementRun` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblarstatements`
--
ALTER TABLE `_etblarstatements`
  MODIFY `idStatements` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblauditinglog`
--
ALTER TABLE `_etblauditinglog`
  MODIFY `idAuditingLog` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblautostrings`
--
ALTER TABLE `_etblautostrings`
  MODIFY `idAutoStrings` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblbankdetails`
--
ALTER TABLE `_etblbankdetails`
  MODIFY `idBankDetail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblbatchpermissions`
--
ALTER TABLE `_etblbatchpermissions`
  MODIFY `idBatchPermissions` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblbranch`
--
ALTER TABLE `_etblbranch`
  MODIFY `idBranch` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblbudgetsprev`
--
ALTER TABLE `_etblbudgetsprev`
  MODIFY `idBudgetsPrev` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblcmagentcontact`
--
ALTER TABLE `_etblcmagentcontact`
  MODIFY `idContact` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblcmrejectreason`
--
ALTER TABLE `_etblcmrejectreason`
  MODIFY `idRejectReason` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldashboardlayouts`
--
ALTER TABLE `_etbldashboardlayouts`
  MODIFY `idDashboardLayouts` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldeladdress`
--
ALTER TABLE `_etbldeladdress`
  MODIFY `idDelAddress` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldeladdresscode`
--
ALTER TABLE `_etbldeladdresscode`
  MODIFY `IDDelAddressCode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldeleted`
--
ALTER TABLE `_etbldeleted`
  MODIFY `idDeleted` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldoccat`
--
ALTER TABLE `_etbldoccat`
  MODIFY `idDocCat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldoccatgroup`
--
ALTER TABLE `_etbldoccatgroup`
  MODIFY `idDocCatGroup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbldocprofiles`
--
ALTER TABLE `_etbldocprofiles`
  MODIFY `idDocProfiles` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleftgatewaytype`
--
ALTER TABLE `_etbleftgatewaytype`
  MODIFY `GatewayID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleftreference`
--
ALTER TABLE `_etbleftreference`
  MODIFY `idNumber` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleftsfilelayout`
--
ALTER TABLE `_etbleftsfilelayout`
  MODIFY `idEFTSLayout` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleftsfilelayoutdetails`
--
ALTER TABLE `_etbleftsfilelayoutdetails`
  MODIFY `idEFTSLayoutDetails` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleucommodity`
--
ALTER TABLE `_etbleucommodity`
  MODIFY `IDEUCommodity` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleucountry`
--
ALTER TABLE `_etbleucountry`
  MODIFY `IDEUCountry` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleunotc`
--
ALTER TABLE `_etbleunotc`
  MODIFY `IDEUNoTC` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbleusupplementaryunit`
--
ALTER TABLE `_etbleusupplementaryunit`
  MODIFY `IDEUSupplementaryUnit` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblfiscalprintermodels`
--
ALTER TABLE `_etblfiscalprintermodels`
  MODIFY `iFiscalPrinterModelsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblfiscalprinters`
--
ALTER TABLE `_etblfiscalprinters`
  MODIFY `iFiscalPrinterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblglmscoaaccounts`
--
ALTER TABLE `_etblglmscoaaccounts`
  MODIFY `idGLmSCOAAccount` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblglreportcategory`
--
ALTER TABLE `_etblglreportcategory`
  MODIFY `idReportCategory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblglrevisebudget`
--
ALTER TABLE `_etblglrevisebudget`
  MODIFY `idGLReviseBudget` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblglsegment`
--
ALTER TABLE `_etblglsegment`
  MODIFY `idSegment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvcosttracking`
--
ALTER TABLE `_etblinvcosttracking`
  MODIFY `idCostTracking` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvimages`
--
ALTER TABLE `_etblinvimages`
  MODIFY `idInvImage` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvjrbatches`
--
ALTER TABLE `_etblinvjrbatches`
  MODIFY `IDInvJrBatches` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvjrbatchlines`
--
ALTER TABLE `_etblinvjrbatchlines`
  MODIFY `idInvJrBatchLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvoicedeposits`
--
ALTER TABLE `_etblinvoicedeposits`
  MODIFY `idInvoiceDeposits` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvpriceupdatebatches`
--
ALTER TABLE `_etblinvpriceupdatebatches`
  MODIFY `idInvPriceUpdateBatches` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvpriceupdatebatchlines`
--
ALTER TABLE `_etblinvpriceupdatebatchlines`
  MODIFY `idInvPriceUpdateBatchLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvseggroup`
--
ALTER TABLE `_etblinvseggroup`
  MODIFY `idInvSegGroup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvsegtype`
--
ALTER TABLE `_etblinvsegtype`
  MODIFY `idInvSegType` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblinvsegvalue`
--
ALTER TABLE `_etblinvsegvalue`
  MODIFY `idInvSegValue` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbllotstatus`
--
ALTER TABLE `_etbllotstatus`
  MODIFY `idLotStatus` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbllottracking`
--
ALTER TABLE `_etbllottracking`
  MODIFY `idLotTracking` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbllottrackingqty`
--
ALTER TABLE `_etbllottrackingqty`
  MODIFY `idLotTrackingQty` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbllottrackingtx`
--
ALTER TABLE `_etbllottrackingtx`
  MODIFY `idLotTrackingTx` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblmanufprocess`
--
ALTER TABLE `_etblmanufprocess`
  MODIFY `idManufProcess` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblmanufprocessitem`
--
ALTER TABLE `_etblmanufprocessitem`
  MODIFY `idManufProcessItem` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblmanufprocessline`
--
ALTER TABLE `_etblmanufprocessline`
  MODIFY `idManufProcessLine` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblmcagentcriteria`
--
ALTER TABLE `_etblmcagentcriteria`
  MODIFY `idAgentCriteria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblmcdefaultcriteria`
--
ALTER TABLE `_etblmcdefaultcriteria`
  MODIFY `idDefaultCriteria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblordercancelreason`
--
ALTER TABLE `_etblordercancelreason`
  MODIFY `idOrderCancelReason` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpaymentsbasedtax`
--
ALTER TABLE `_etblpaymentsbasedtax`
  MODIFY `idPaymentsBasedTax` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpopdefaults`
--
ALTER TABLE `_etblpopdefaults`
  MODIFY `idPOPDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpoprequisitionlines`
--
ALTER TABLE `_etblpoprequisitionlines`
  MODIFY `idPOPRequisitionLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpoprequisitionlineshist`
--
ALTER TABLE `_etblpoprequisitionlineshist`
  MODIFY `idPOPRequisitionLinesHist` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpoprequisitions`
--
ALTER TABLE `_etblpoprequisitions`
  MODIFY `idPOPRequisitions` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpoprequisitionshist`
--
ALTER TABLE `_etblpoprequisitionshist`
  MODIFY `idPOPRequisitionsHist` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblposdevices`
--
ALTER TABLE `_etblposdevices`
  MODIFY `idPOSDevices` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpostdatedcheques`
--
ALTER TABLE `_etblpostdatedcheques`
  MODIFY `idPostDatedCheques` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpostglhist`
--
ALTER TABLE `_etblpostglhist`
  MODIFY `AutoIdx` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpricelistname`
--
ALTER TABLE `_etblpricelistname`
  MODIFY `IDPriceListName` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpricelistprices`
--
ALTER TABLE `_etblpricelistprices`
  MODIFY `IDPriceListPrices` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpromotion`
--
ALTER TABLE `_etblpromotion`
  MODIFY `iPromotionID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpromotionitem`
--
ALTER TABLE `_etblpromotionitem`
  MODIFY `iPromotionItemID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblpromotionitemlist`
--
ALTER TABLE `_etblpromotionitemlist`
  MODIFY `iPromotionItemListID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblremittancebatches`
--
ALTER TABLE `_etblremittancebatches`
  MODIFY `idRemittanceBatches` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblremittancelines`
--
ALTER TABLE `_etblremittancelines`
  MODIFY `IDRemittanceLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblremittancesuppliers`
--
ALTER TABLE `_etblremittancesuppliers`
  MODIFY `IDRemittanceSuppliers` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblrepllog`
--
ALTER TABLE `_etblrepllog`
  MODIFY `idReplLog` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblreportjoblog`
--
ALTER TABLE `_etblreportjoblog`
  MODIFY `idReportJobLog` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblreportjobs`
--
ALTER TABLE `_etblreportjobs`
  MODIFY `idReportJobs` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblrevaluationhistory`
--
ALTER TABLE `_etblrevaluationhistory`
  MODIFY `idRevaluationHistory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsagepaybanks`
--
ALTER TABLE `_etblsagepaybanks`
  MODIFY `SagePayBankID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsagepayerrorcodes`
--
ALTER TABLE `_etblsagepayerrorcodes`
  MODIFY `idSagePayErrorCode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsagepaynow`
--
ALTER TABLE `_etblsagepaynow`
  MODIFY `idSagePayNow` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsagepayqueue`
--
ALTER TABLE `_etblsagepayqueue`
  MODIFY `idSPQueue` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsagepayservicekeys`
--
ALTER TABLE `_etblsagepayservicekeys`
  MODIFY `idSagePayServiceKey` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsettlementterms`
--
ALTER TABLE `_etblsettlementterms`
  MODIFY `idSettlementTerms` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsystem`
--
ALTER TABLE `_etblsystem`
  MODIFY `idSystem` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsystemdefaults`
--
ALTER TABLE `_etblsystemdefaults`
  MODIFY `idSystemDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblsystemupdate`
--
ALTER TABLE `_etblsystemupdate`
  MODIFY `idSystemUpdate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbltaxboxlayout`
--
ALTER TABLE `_etbltaxboxlayout`
  MODIFY `idTaxBoxLayout` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbltaxboxsetup`
--
ALTER TABLE `_etbltaxboxsetup`
  MODIFY `idTaxBoxSetup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbltaxdefaults`
--
ALTER TABLE `_etbltaxdefaults`
  MODIFY `idTaxDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbltaxgroup`
--
ALTER TABLE `_etbltaxgroup`
  MODIFY `idTaxGroup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etbltaxgrouptranstype`
--
ALTER TABLE `_etbltaxgrouptranstype`
  MODIFY `idTaxGroupTransType` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblterms`
--
ALTER TABLE `_etblterms`
  MODIFY `iTermID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblunitcategory`
--
ALTER TABLE `_etblunitcategory`
  MODIFY `idUnitCategory` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblunitconversion`
--
ALTER TABLE `_etblunitconversion`
  MODIFY `idUnitConversion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblunits`
--
ALTER TABLE `_etblunits`
  MODIFY `idUnits` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvasairtimeitem`
--
ALTER TABLE `_etblvasairtimeitem`
  MODIFY `idVASAirtimeItem` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvasairtimemaster`
--
ALTER TABLE `_etblvasairtimemaster`
  MODIFY `idVASAirtimeMaster` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvasairtimenetwork`
--
ALTER TABLE `_etblvasairtimenetwork`
  MODIFY `idVASAirtimeNetwork` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvasairtimeproduct`
--
ALTER TABLE `_etblvasairtimeproduct`
  MODIFY `idVASAirtimeProduct` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvdap`
--
ALTER TABLE `_etblvdap`
  MODIFY `IDVD` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvdar`
--
ALTER TABLE `_etblvdar`
  MODIFY `IDVD` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvdlnap`
--
ALTER TABLE `_etblvdlnap`
  MODIFY `IDVDLn` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvdlnar`
--
ALTER TABLE `_etblvdlnar`
  MODIFY `IDVDLn` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvdlnlvlap`
--
ALTER TABLE `_etblvdlnlvlap`
  MODIFY `IDVDLnLvl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblvdlnlvlar`
--
ALTER TABLE `_etblvdlnlvlar`
  MODIFY `IDVDLnLvl` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblwhdefaults`
--
ALTER TABLE `_etblwhdefaults`
  MODIFY `IDWhDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblwhseibt`
--
ALTER TABLE `_etblwhseibt`
  MODIFY `IDWhseIBT` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblwhseibtaddcosts`
--
ALTER TABLE `_etblwhseibtaddcosts`
  MODIFY `IDWhseIBTAddCosts` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblwhseibtlines`
--
ALTER TABLE `_etblwhseibtlines`
  MODIFY `IDWhseIBTLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblwhseibtlinesn`
--
ALTER TABLE `_etblwhseibtlinesn`
  MODIFY `IDWhseIBTLineSN` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblwhsetransferbatches`
--
ALTER TABLE `_etblwhsetransferbatches`
  MODIFY `idWhseTransferBatch` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_etblworkers`
--
ALTER TABLE `_etblworkers`
  MODIFY `idWorkers` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_mtblmbrcategories`
--
ALTER TABLE `_mtblmbrcategories`
  MODIFY `idMBRCategories` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retagentsession`
--
ALTER TABLE `_retagentsession`
  MODIFY `idAgentSession` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retcashpickup`
--
ALTER TABLE `_retcashpickup`
  MODIFY `idCashPickup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retdefaults`
--
ALTER TABLE `_retdefaults`
  MODIFY `idRetailDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retdenomination`
--
ALTER TABLE `_retdenomination`
  MODIFY `idDenomination` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retdiscountreason`
--
ALTER TABLE `_retdiscountreason`
  MODIFY `idDiscountReason` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retdocketlock`
--
ALTER TABLE `_retdocketlock`
  MODIFY `idDocketLock` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retlaybys`
--
ALTER TABLE `_retlaybys`
  MODIFY `idLayBys` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retpettycash`
--
ALTER TABLE `_retpettycash`
  MODIFY `idPettyCash` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retpettycashline`
--
ALTER TABLE `_retpettycashline`
  MODIFY `idPettyCashLine` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retpettycashtype`
--
ALTER TABLE `_retpettycashtype`
  MODIFY `idPettyCashType` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retposlogfile`
--
ALTER TABLE `_retposlogfile`
  MODIFY `idPOSLogFile` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retposloglinks`
--
ALTER TABLE `_retposloglinks`
  MODIFY `idPOSLogLinks` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retposmenu`
--
ALTER TABLE `_retposmenu`
  MODIFY `idPOSMenu` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retposmenusetup`
--
ALTER TABLE `_retposmenusetup`
  MODIFY `idPOSMenuSetup` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retpostender`
--
ALTER TABLE `_retpostender`
  MODIFY `idPOSTender` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retpostransaction`
--
ALTER TABLE `_retpostransaction`
  MODIFY `idPOSTransaction` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retpriceoverridereason`
--
ALTER TABLE `_retpriceoverridereason`
  MODIFY `idPriceOverrideReason` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retreturnreason`
--
ALTER TABLE `_retreturnreason`
  MODIFY `idReturnReason` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rettendertype`
--
ALTER TABLE `_rettendertype`
  MODIFY `idTenderType` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rettill`
--
ALTER TABLE `_rettill`
  MODIFY `idTill` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rettillsecurity`
--
ALTER TABLE `_rettillsecurity`
  MODIFY `idTillSecurity` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rettillstationery`
--
ALTER TABLE `_rettillstationery`
  MODIFY `idTillStationery` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rettradingsession`
--
ALTER TABLE `_rettradingsession`
  MODIFY `idTradingSession` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_retvariablebarcode`
--
ALTER TABLE `_retvariablebarcode`
  MODIFY `idVariableBarcode` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblagentgroups`
--
ALTER TABLE `_rtblagentgroups`
  MODIFY `idAgentGroups` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblagentlockedout`
--
ALTER TABLE `_rtblagentlockedout`
  MODIFY `IDAgentLockedOut` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblagents`
--
ALTER TABLE `_rtblagents`
  MODIFY `idAgents` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblbusclass`
--
ALTER TABLE `_rtblbusclass`
  MODIFY `idBusClass` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblbusdept`
--
ALTER TABLE `_rtblbusdept`
  MODIFY `idBusDept` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblbusdesig`
--
ALTER TABLE `_rtblbusdesig`
  MODIFY `idBusDesig` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblbustype`
--
ALTER TABLE `_rtblbustype`
  MODIFY `idBusType` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcmdefaults`
--
ALTER TABLE `_rtblcmdefaults`
  MODIFY `idCMDefaults` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcompetitor`
--
ALTER TABLE `_rtblcompetitor`
  MODIFY `idCompetitor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcompetitorproduct`
--
ALTER TABLE `_rtblcompetitorproduct`
  MODIFY `IDCompetitorProduct` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcompetitorproductlink`
--
ALTER TABLE `_rtblcompetitorproductlink`
  MODIFY `idCompetitorProductLink` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcontractdoclinks`
--
ALTER TABLE `_rtblcontractdoclinks`
  MODIFY `idDocLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcontracts`
--
ALTER TABLE `_rtblcontracts`
  MODIFY `idContracts` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcontracttemplates`
--
ALTER TABLE `_rtblcontracttemplates`
  MODIFY `idContractTemplates` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcontracttx`
--
ALTER TABLE `_rtblcontracttx`
  MODIFY `idContractTx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblcountry`
--
ALTER TABLE `_rtblcountry`
  MODIFY `idCountry` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtbldoccat`
--
ALTER TABLE `_rtbldoccat`
  MODIFY `idDocCat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtbldoclinks`
--
ALTER TABLE `_rtbldoclinks`
  MODIFY `idDocLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtbldocstore`
--
ALTER TABLE `_rtbldocstore`
  MODIFY `idDocStore` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblescalategrp`
--
ALTER TABLE `_rtblescalategrp`
  MODIFY `idEscalateGrp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblincidentcat`
--
ALTER TABLE `_rtblincidentcat`
  MODIFY `idIncidentCat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblincidentlog`
--
ALTER TABLE `_rtblincidentlog`
  MODIFY `idIncidentLog` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblincidents`
--
ALTER TABLE `_rtblincidents`
  MODIFY `idIncidents` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblincidents_archive`
--
ALTER TABLE `_rtblincidents_archive`
  MODIFY `idIncidents` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblincidenttemplates`
--
ALTER TABLE `_rtblincidenttemplates`
  MODIFY `idIncidentTemplates` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblincidenttype`
--
ALTER TABLE `_rtblincidenttype`
  MODIFY `idIncidentType` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblkbadoclinks`
--
ALTER TABLE `_rtblkbadoclinks`
  MODIFY `idDocLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblkbcategorylinks`
--
ALTER TABLE `_rtblkbcategorylinks`
  MODIFY `idCategoryLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblknowledgebase`
--
ALTER TABLE `_rtblknowledgebase`
  MODIFY `idKnowledgeBase` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblnotify`
--
ALTER TABLE `_rtblnotify`
  MODIFY `idNotify` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblopportunity`
--
ALTER TABLE `_rtblopportunity`
  MODIFY `IDOpportunity` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblopportunitycompetitor`
--
ALTER TABLE `_rtblopportunitycompetitor`
  MODIFY `idOpportunityCompetitor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblopportunitydoclinks`
--
ALTER TABLE `_rtblopportunitydoclinks`
  MODIFY `IDDocLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblopportunitysource`
--
ALTER TABLE `_rtblopportunitysource`
  MODIFY `IDOpportunitySource` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblopportunitystage`
--
ALTER TABLE `_rtblopportunitystage`
  MODIFY `IDOpportunityStage` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblopportunitystatus`
--
ALTER TABLE `_rtblopportunitystatus`
  MODIFY `IDOpportunityStatus` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblpeople`
--
ALTER TABLE `_rtblpeople`
  MODIFY `idPeople` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblpeoplelinks`
--
ALTER TABLE `_rtblpeoplelinks`
  MODIFY `idPeopleLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblprospect`
--
ALTER TABLE `_rtblprospect`
  MODIFY `IDProspect` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblrefbase`
--
ALTER TABLE `_rtblrefbase`
  MODIFY `idRefBase` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblrefbook`
--
ALTER TABLE `_rtblrefbook`
  MODIFY `idRefBook` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblstocklinks`
--
ALTER TABLE `_rtblstocklinks`
  MODIFY `idStockLinks` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtbluserdict`
--
ALTER TABLE `_rtbluserdict`
  MODIFY `idUserDict` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblworkcal`
--
ALTER TABLE `_rtblworkcal`
  MODIFY `idWorkCal` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_rtblworkcalexdates`
--
ALTER TABLE `_rtblworkcalexdates`
  MODIFY `idWorkCalExDates` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_simtblreqheader`
--
ALTER TABLE `_simtblreqheader`
  MODIFY `idReqHeader` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_simtblreqlines`
--
ALTER TABLE `_simtblreqlines`
  MODIFY `idReqLines` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_simtblstkissuelines`
--
ALTER TABLE `_simtblstkissuelines`
  MODIFY `iAutoIdx` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_simtblstockissuelinesn`
--
ALTER TABLE `_simtblstockissuelinesn`
  MODIFY `idStockIssueLineSN` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_simtblstockissuemaster`
--
ALTER TABLE `_simtblstockissuemaster`
  MODIFY `iStkIssueId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_wtblipaddetails`
--
ALTER TABLE `_wtblipaddetails`
  MODIFY `idIPadNumber` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_wtblipaduser`
--
ALTER TABLE `_wtblipaduser`
  MODIFY `idIPadUser` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_wtblpemmobilitymodules`
--
ALTER TABLE `_wtblpemmobilitymodules`
  MODIFY `idMobilityModule` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_wtblsystem`
--
ALTER TABLE `_wtblsystem`
  MODIFY `idSystem` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
