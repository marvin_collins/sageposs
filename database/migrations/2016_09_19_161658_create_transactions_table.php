<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_number')->default('');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('warehouse_id')->unsigned()->index();
            $table->integer('original_transaction_id')->nullable();
            $table->integer('invoice_id')->nullable();
            $table->integer('cashbook_id')->nullable();
            $table->integer('petty_cash_id')->nullable();
            $table->string('transaction_type');
            $table->string('payment_type')->nullable();
            $table->float('amount');
            $table->text('transaction_data')->nullable();
            $table->boolean('reversed')->default(false);
            $table->tinyInteger('status')->default(0);
            $table->boolean('printed')->default(false);
            $table->boolean('locked')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
