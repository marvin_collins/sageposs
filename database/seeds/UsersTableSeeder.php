<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'superuser',
            'agent_id' => 0,
            'email' => 'smodavprivate@gmail.com',
            'password' => bcrypt(config('pos.super_password')),
            'role_id' => 0,
            'permissions' => json_encode(['superuser'])
        ]);
//        DB::statement('SET IDENTITY_INSERT users ON;insert into [users] ([id], [name], [email], [password], [role_id], [permissions], [created_at], [updated_at]) values (0, \'Superuser\', \'smodavprivate@gmail.com\', \'' . bcrypt(env('SUPER_PASSWORD')) .'\', 0, \'["superuser"]\', \'2016-10-17 12:10:16\', \'2016-10-17 12:10:16\')');
//        DB::statement('SET IDENTITY_INSERT users OFF;');
    }
}
