<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use POS\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET IDENTITY_INSERT roles ON; insert into [roles] ([id], [name], [permissions], [created_at], [updated_at]) values (0, \'Superuser\', \'["superuser"]\', \'2016-10-17 12:00:59.000\', \'2016-10-17 12:00:59.000\')');
        DB::statement('SET IDENTITY_INSERT roles ON; insert into [roles] ([id], [name], [permissions], [created_at], [updated_at]) values (1, \'Default User\', \'[]\', \'2016-10-17 12:00:59.000\', \'2016-10-17 12:00:59.000\')');
        DB::statement('SET IDENTITY_INSERT users OFF;');
    }
}
