<?php

use Illuminate\Database\Seeder;
use POS\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'Full Control',
                'group' => 'Users'
            ],
            [
                'name' => 'Full Control',
                'group' => 'Roles'
            ],
            [
                'name' => 'Approve',
                'group' => 'Transactions'
            ],
            [
                'name' => 'View',
                'group' => 'Transactions'
            ],
            [
                'name' => 'Reprint',
                'group' => 'Transactions'
            ],
            [
                'name' => 'Full Control',
                'group' => 'Warehouse Assignment'
            ],
            [
                'name' => 'Full Control',
                'group' => 'Petty Cash Types'
            ],
            [
                'name' => 'Modify',
                'group' => 'System Settings'
            ],

        ];
        Permission::insert($permissions);
    }
}
