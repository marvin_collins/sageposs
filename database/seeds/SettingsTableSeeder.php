<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();


        DB::table('settings')->insert([
            [
                'name' => 'discount_type',
                'value' => 'amount',
                'default' => 'amount',
                'title' => 'Discount Type',
                'description' => 'This is the type of discount (either amount or percentage) to be used when making a sale',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'discount_amount',
                'value' => '1000',
                'default' => '1000',
                'title' => 'Discount Amount',
                'description' => 'This is maximum amount allowable as discount without asking for approval. 
                If the discount type is amount, then the discount amount is the maximum amount of cash allowable
                 and if percentage, its the maximum percentage allowable.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'insert_on_search',
                'value' => 'No',
                'default' => 'No',
                'title' => 'Add to cart on search',
                'description' => 'This determines whether the system should automatically add the searched item 
                 to the cart but only when the result yields a single item.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'code_only_search',
                'value' => 'No',
                'default' => 'No',
                'title' => 'Add a Code-Only search bar',
                'description' => 'This determines whether the system should add a search bar on the sale screen
                 that will only be used to search for items using their code.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'auto_focus',
                'value' => 'Yes',
                'default' => 'Yes',
                'title' => 'Auto-focus fields',
                'description' => 'This determines whether the system should automatically send the cursor focus to
                 the quantity then the discount on pressing ENTER key.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'auto_merge',
                'value' => 'Yes',
                'default' => 'Yes',
                'title' => 'Auto merge duplicate items on cart',
                'description' => 'This determines whether the system should automatically increment the quantity 
                 of a product instead of adding it as a separate product in the cart. When set to NO, the cart will
                  have duplicate items with quantity 1 each',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'auto_merge_receipt',
                'value' => 'Yes',
                'default' => 'Yes',
                'title' => 'Auto merge duplicate items on receipt',
                'description' => 'This determines whether the system should automatically increment the quantity 
                 of a product instead of adding it as a separate product in the receipt and printouts. 
                  When set to NO, the cart will have duplicate items with quantity 1 each',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
