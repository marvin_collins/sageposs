CREATE DATABASE IF NOT EXISTS `POS_BACKUP` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `POS_BACKUP`;

DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Client`;
CREATE TABLE `Client` (
  `DCLink` int(11) NOT NULL,
  `Account` varchar(20) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Title` varchar(5) DEFAULT NULL,
  `Init` varchar(6) DEFAULT NULL,
  `Contact_Person` varchar(30) DEFAULT NULL,
  `Physical1` varchar(40) DEFAULT NULL,
  `Physical2` varchar(40) DEFAULT NULL,
  `Physical3` varchar(40) DEFAULT NULL,
  `Physical4` varchar(40) DEFAULT NULL,
  `Physical5` varchar(40) DEFAULT NULL,
  `PhysicalPC` varchar(15) DEFAULT NULL,
  `Addressee` varchar(30) DEFAULT NULL,
  `Post1` varchar(40) DEFAULT NULL,
  `Post2` varchar(40) DEFAULT NULL,
  `Post3` varchar(40) DEFAULT NULL,
  `Post4` varchar(40) DEFAULT NULL,
  `Post5` varchar(40) DEFAULT NULL,
  `PostPC` varchar(15) DEFAULT NULL,
  `Delivered_To` varchar(30) DEFAULT NULL,
  `Telephone` varchar(25) DEFAULT NULL,
  `Telephone2` varchar(25) DEFAULT NULL,
  `Fax1` varchar(25) DEFAULT NULL,
  `Fax2` varchar(25) DEFAULT NULL,
  `AccountTerms` int(11) DEFAULT NULL,
  `CT` tinyint(1) NOT NULL DEFAULT '1',
  `Tax_Number` varchar(50) DEFAULT NULL,
  `Registration` varchar(20) DEFAULT NULL,
  `Credit_Limit` double DEFAULT NULL,
  `RepID` int(11) DEFAULT NULL,
  `Interest_Rate` double DEFAULT NULL,
  `Discount` double DEFAULT NULL,
  `On_Hold` tinyint(1) NOT NULL DEFAULT '0',
  `BFOpenType` int(11) DEFAULT NULL,
  `EMail` varchar(200) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `AutoDisc` double DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `MainAccLink` int(11) DEFAULT NULL,
  `CashDebtor` tinyint(1) NOT NULL DEFAULT '0',
  `DCBalance` double DEFAULT NULL,
  `CheckTerms` tinyint(1) NOT NULL DEFAULT '0',
  `UseEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iIncidentTypeID` int(11) DEFAULT NULL,
  `iBusTypeID` int(11) DEFAULT NULL,
  `iBusClassID` int(11) DEFAULT NULL,
  `iCountryID` int(11) DEFAULT NULL,
  `iAgentID` int(11) DEFAULT NULL,
  `dTimeStamp` datetime DEFAULT NULL,
  `cAccDescription` varchar(80) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `iClassID` int(11) DEFAULT NULL,
  `iAreasID` int(11) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bStatPrint` tinyint(1) NOT NULL DEFAULT '0',
  `bStatEmail` tinyint(1) NOT NULL DEFAULT '0',
  `cStatEmailPass` varchar(160) DEFAULT NULL,
  `bForCurAcc` tinyint(1) NOT NULL DEFAULT '0',
  `fForeignBalance` double DEFAULT NULL,
  `bTaxPrompt` tinyint(1) NOT NULL DEFAULT '1',
  `iARPriceListNameID` int(11) DEFAULT NULL,
  `iSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `bSourceDocPrint` tinyint(1) NOT NULL DEFAULT '1',
  `bSourceDocEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iEUCountryID` int(11) NOT NULL DEFAULT '0',
  `iDefTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `bCODAccount` tinyint(1) NOT NULL DEFAULT '0',
  `iAgeingTermID` int(11) DEFAULT NULL,
  `bElecDocAcceptance` tinyint(1) NOT NULL DEFAULT '0',
  `iBankDetailType` tinyint(3) UNSIGNED DEFAULT NULL,
  `cBankAccHolder` varchar(30) DEFAULT NULL,
  `cIDNumber` varchar(20) DEFAULT NULL,
  `cPassportNumber` varchar(20) DEFAULT NULL,
  `bInsuranceCustomer` tinyint(1) DEFAULT '0',
  `cBankCode` varchar(15) DEFAULT NULL,
  `cSwiftCode` varchar(11) DEFAULT NULL,
  `Client_iBranchID` int(11) DEFAULT NULL,
  `Client_dCreatedDate` datetime DEFAULT NULL,
  `Client_dModifiedDate` datetime DEFAULT NULL,
  `Client_iCreatedBranchID` int(11) DEFAULT NULL,
  `Client_iModifiedBranchID` int(11) DEFAULT NULL,
  `Client_iCreatedAgentID` int(11) DEFAULT NULL,
  `Client_iModifiedAgentID` int(11) DEFAULT NULL,
  `Client_iChangeSetID` int(11) DEFAULT NULL,
  `Client_Checksum` binary(20) DEFAULT NULL,
  `iSPQueueID` int(11) DEFAULT NULL,
  `bCustomerZoneEnabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `InvNum`;
CREATE TABLE `InvNum` (
  `AutoIndex` int(11) NOT NULL,
  `DocType` int(11) DEFAULT NULL,
  `DocVersion` int(11) DEFAULT NULL,
  `DocState` int(11) DEFAULT NULL,
  `DocFlag` int(11) DEFAULT NULL,
  `OrigDocID` int(11) DEFAULT NULL,
  `InvNumber` varchar(50) DEFAULT NULL,
  `GrvNumber` varchar(50) DEFAULT NULL,
  `GrvID` int(11) DEFAULT NULL,
  `AccountID` int(11) DEFAULT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `InvDate` datetime DEFAULT NULL,
  `OrderDate` datetime DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `DeliveryDate` datetime DEFAULT NULL,
  `TaxInclusive` tinyint(1) DEFAULT NULL,
  `Email_Sent` int(11) DEFAULT NULL,
  `Address1` varchar(40) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `Address4` varchar(40) DEFAULT NULL,
  `Address5` varchar(40) DEFAULT NULL,
  `Address6` varchar(40) DEFAULT NULL,
  `PAddress1` varchar(40) DEFAULT NULL,
  `PAddress2` varchar(40) DEFAULT NULL,
  `PAddress3` varchar(40) DEFAULT NULL,
  `PAddress4` varchar(40) DEFAULT NULL,
  `PAddress5` varchar(40) DEFAULT NULL,
  `PAddress6` varchar(40) DEFAULT NULL,
  `DelMethodID` int(11) DEFAULT NULL,
  `DocRepID` int(11) DEFAULT NULL,
  `OrderNum` varchar(50) DEFAULT NULL,
  `DeliveryNote` varchar(50) DEFAULT NULL,
  `InvDisc` double DEFAULT NULL,
  `InvDiscReasonID` int(11) DEFAULT NULL,
  `Message1` varchar(255) DEFAULT NULL,
  `Message2` varchar(255) DEFAULT NULL,
  `Message3` varchar(255) DEFAULT NULL,
  `ProjectID` int(11) DEFAULT NULL,
  `TillID` int(11) DEFAULT NULL,
  `POSAmntTendered` double DEFAULT NULL,
  `POSChange` double DEFAULT NULL,
  `GrvSplitFixedCost` tinyint(1) NOT NULL DEFAULT '0',
  `GrvSplitFixedAmnt` double DEFAULT NULL,
  `OrderStatusID` int(11) DEFAULT NULL,
  `OrderPriorityID` int(11) DEFAULT NULL,
  `ExtOrderNum` varchar(50) DEFAULT NULL,
  `ForeignCurrencyID` int(11) DEFAULT NULL,
  `InvDiscAmnt` double DEFAULT NULL,
  `InvDiscAmntEx` double DEFAULT NULL,
  `InvTotExclDEx` double DEFAULT NULL,
  `InvTotTaxDEx` double DEFAULT NULL,
  `InvTotInclDEx` double DEFAULT NULL,
  `InvTotExcl` double DEFAULT NULL,
  `InvTotTax` double DEFAULT NULL,
  `InvTotIncl` double DEFAULT NULL,
  `OrdDiscAmnt` double DEFAULT NULL,
  `OrdDiscAmntEx` double DEFAULT NULL,
  `OrdTotExclDEx` double DEFAULT NULL,
  `OrdTotTaxDEx` double DEFAULT NULL,
  `OrdTotInclDEx` double DEFAULT NULL,
  `OrdTotExcl` double DEFAULT NULL,
  `OrdTotTax` double DEFAULT NULL,
  `OrdTotIncl` double DEFAULT NULL,
  `bUseFixedPrices` tinyint(1) NOT NULL DEFAULT '0',
  `iDocPrinted` int(11) DEFAULT NULL,
  `iINVNUMAgentID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fGrvSplitFixedAmntForeign` double DEFAULT NULL,
  `fInvDiscAmntForeign` double DEFAULT NULL,
  `fInvDiscAmntExForeign` double DEFAULT NULL,
  `fInvTotExclDExForeign` double DEFAULT NULL,
  `fInvTotTaxDExForeign` double DEFAULT NULL,
  `fInvTotInclDExForeign` double DEFAULT NULL,
  `fInvTotExclForeign` double DEFAULT NULL,
  `fInvTotTaxForeign` double DEFAULT NULL,
  `fInvTotInclForeign` double DEFAULT NULL,
  `fOrdDiscAmntForeign` double DEFAULT NULL,
  `fOrdDiscAmntExForeign` double DEFAULT NULL,
  `fOrdTotExclDExForeign` double DEFAULT NULL,
  `fOrdTotTaxDExForeign` double DEFAULT NULL,
  `fOrdTotInclDExForeign` double DEFAULT NULL,
  `fOrdTotExclForeign` double DEFAULT NULL,
  `fOrdTotTaxForeign` double DEFAULT NULL,
  `fOrdTotInclForeign` double DEFAULT NULL,
  `cTaxNumber` varchar(50) DEFAULT NULL,
  `cAccountName` varchar(150) DEFAULT NULL,
  `iProspectID` int(11) NOT NULL DEFAULT '0',
  `iOpportunityID` int(11) NOT NULL DEFAULT '0',
  `InvTotRounding` double NOT NULL DEFAULT '0',
  `OrdTotRounding` double NOT NULL DEFAULT '0',
  `fInvTotForeignRounding` double NOT NULL DEFAULT '0',
  `fOrdTotForeignRounding` double NOT NULL DEFAULT '0',
  `bInvRounding` tinyint(1) NOT NULL DEFAULT '0',
  `iInvSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `cSettlementTermInvMsg` varchar(255) DEFAULT NULL,
  `iOrderCancelReasonID` int(11) NOT NULL DEFAULT '0',
  `iLinkedDocID` int(11) NOT NULL DEFAULT '0',
  `bLinkedTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `InvTotInclExRounding` double NOT NULL DEFAULT '0',
  `OrdTotInclExRounding` double NOT NULL DEFAULT '0',
  `fInvTotInclForeignExRounding` double NOT NULL DEFAULT '0',
  `fOrdTotInclForeignExRounding` double NOT NULL DEFAULT '0',
  `iEUNoTCID` int(11) NOT NULL DEFAULT '0',
  `iPOAuthStatus` int(11) NOT NULL DEFAULT '0',
  `iPOIncidentID` int(11) NOT NULL DEFAULT '0',
  `iSupervisorID` int(11) DEFAULT NULL,
  `iMergedDocID` int(11) DEFAULT '0',
  `iDocEmailed` int(11) NOT NULL DEFAULT '0',
  `fDepositAmountForeign` double DEFAULT NULL,
  `fRefundAmount` double DEFAULT NULL,
  `bTaxPerLine` tinyint(1) NOT NULL DEFAULT '1',
  `fDepositAmountTotal` double DEFAULT NULL,
  `fDepositAmountUnallocated` double DEFAULT NULL,
  `fDepositAmountNew` double DEFAULT NULL,
  `fDepositAmountTotalForeign` double DEFAULT NULL,
  `fDepositAmountUnallocatedForeign` double DEFAULT NULL,
  `fRefundAmountForeign` double DEFAULT NULL,
  `KeepAsideCollectionDate` datetime DEFAULT NULL,
  `KeepAsideExpiryDate` datetime DEFAULT NULL,
  `cContact` varchar(50) DEFAULT NULL,
  `cTelephone` varchar(25) DEFAULT NULL,
  `cFax` varchar(25) DEFAULT NULL,
  `cEmail` varchar(60) DEFAULT NULL,
  `cCellular` varchar(25) DEFAULT NULL,
  `imgOrderSignature` longblob,
  `iInsuranceState` int(11) NOT NULL DEFAULT '0',
  `cAuthorisedBy` varchar(50) DEFAULT NULL,
  `cClaimNumber` varchar(40) DEFAULT NULL,
  `cPolicyNumber` varchar(40) DEFAULT NULL,
  `dIncidentDate` datetime DEFAULT NULL,
  `cExcessAccName` varchar(150) DEFAULT NULL,
  `cExcessAccCont1` varchar(50) DEFAULT NULL,
  `cExcessAccCont2` varchar(50) DEFAULT NULL,
  `fExcessAmt` double DEFAULT NULL,
  `fExcessPct` double DEFAULT NULL,
  `fExcessExclusive` double NOT NULL DEFAULT '0',
  `fExcessInclusive` double NOT NULL DEFAULT '0',
  `fExcessTax` double NOT NULL DEFAULT '0',
  `fAddChargeExclusive` double NOT NULL DEFAULT '0',
  `fAddChargeTax` double NOT NULL DEFAULT '0',
  `fAddChargeInclusive` double NOT NULL DEFAULT '0',
  `fAddChargeExclusiveForeign` double NOT NULL DEFAULT '0',
  `fAddChargeTaxForeign` double NOT NULL DEFAULT '0',
  `fAddChargeInclusiveForeign` double NOT NULL DEFAULT '0',
  `fOrdAddChargeExclusive` double NOT NULL DEFAULT '0',
  `fOrdAddChargeTax` double NOT NULL DEFAULT '0',
  `fOrdAddChargeInclusive` double NOT NULL DEFAULT '0',
  `fOrdAddChargeExclusiveForeign` double NOT NULL DEFAULT '0',
  `fOrdAddChargeTaxForeign` double NOT NULL DEFAULT '0',
  `fOrdAddChargeInclusiveForeign` double NOT NULL DEFAULT '0',
  `iInvoiceSplitDocID` bigint(20) NOT NULL DEFAULT '0',
  `cGIVNumber` varchar(50) DEFAULT NULL,
  `bIsDCOrder` tinyint(1) NOT NULL DEFAULT '0',
  `iDCBranchID` int(11) DEFAULT NULL,
  `iSalesBranchID` int(11) DEFAULT NULL,
  `InvNum_iBranchID` int(11) DEFAULT NULL,
  `InvNum_dCreatedDate` datetime DEFAULT NULL,
  `InvNum_dModifiedDate` datetime DEFAULT NULL,
  `InvNum_iCreatedBranchID` int(11) DEFAULT NULL,
  `InvNum_iModifiedBranchID` int(11) DEFAULT NULL,
  `InvNum_iCreatedAgentID` int(11) DEFAULT NULL,
  `InvNum_iModifiedAgentID` int(11) DEFAULT NULL,
  `InvNum_iChangeSetID` int(11) DEFAULT NULL,
  `InvNum_Checksum` binary(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(41, '2014_09_15_103136_create_roles_table', 1),
(42, '2014_10_12_000000_create_users_table', 1),
(43, '2014_10_12_100000_create_password_resets_table', 1),
(44, '2016_09_16_045439_create_permissions_table', 1),
(45, '2016_09_19_161658_create_transactions_table', 1),
(46, '2016_09_20_103345_create_assignments_table', 1),
(47, '2016_09_20_124910_create_open_sessions_table', 1),
(48, '2016_09_21_173535_create_petty_cash_types_table', 1),
(49, '2016_09_21_173546_create_petty_cashes_table', 1),
(50, '2016_10_14_173520_create_jobs_table', 1);

DROP TABLE IF EXISTS `open_sessions`;
CREATE TABLE `open_sessions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `opened_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `open` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `permissions` (`id`, `name`, `group`, `created_at`, `updated_at`) VALUES
(1, 'Full Control', 'Users', NULL, NULL),
(2, 'Full Control', 'Roles', NULL, NULL),
(3, 'Approve', 'Transactions', NULL, NULL),
(4, 'View', 'Transactions', NULL, NULL),
(5, 'Full Control', 'Warehouse Assignment', NULL, NULL),
(6, 'Full Control', 'Petty Cash Types', NULL, NULL);

DROP TABLE IF EXISTS `petty_cashes`;
CREATE TABLE `petty_cashes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `petty_cash_type_id` int(10) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `reference` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `petty_cash_types`;
CREATE TABLE `petty_cash_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `cashbook_batch_id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED NOT NULL,
  `transaction_type` enum('Debit','Credit') COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(0, 'Superuser', '"superuser"', '2016-10-17 09:00:59', '2016-10-17 09:00:59'),
(1, 'Default User', '', '2016-10-17 09:00:59', '2016-10-17 09:00:59');

DROP TABLE IF EXISTS `StkItem`;
CREATE TABLE `StkItem` (
  `StockLink` int(11) NOT NULL,
  `Code` varchar(400) DEFAULT NULL,
  `Description_1` varchar(50) DEFAULT NULL,
  `Description_2` varchar(50) DEFAULT NULL,
  `Description_3` varchar(50) DEFAULT NULL,
  `ItemGroup` varchar(20) DEFAULT NULL,
  `Pack` varchar(5) DEFAULT NULL,
  `TTI` varchar(10) DEFAULT NULL,
  `TTC` varchar(10) DEFAULT NULL,
  `TTG` varchar(10) DEFAULT NULL,
  `TTR` varchar(10) DEFAULT NULL,
  `Bar_Code` varchar(400) DEFAULT NULL,
  `Re_Ord_Lvl` double DEFAULT NULL,
  `Re_Ord_Qty` double DEFAULT NULL,
  `Min_Lvl` double DEFAULT NULL,
  `Max_Lvl` double DEFAULT NULL,
  `AveUCst` double NOT NULL DEFAULT '0',
  `LatUCst` double NOT NULL DEFAULT '0',
  `LowUCst` double NOT NULL DEFAULT '0',
  `HigUCst` double NOT NULL DEFAULT '0',
  `StdUCst` double NOT NULL DEFAULT '0',
  `Qty_On_Hand` double NOT NULL DEFAULT '0',
  `LGrvCount` double DEFAULT NULL,
  `ServiceItem` tinyint(1) NOT NULL DEFAULT '0',
  `ItemActive` tinyint(1) NOT NULL DEFAULT '1',
  `ReservedQty` double NOT NULL DEFAULT '0',
  `QtyOnPO` double NOT NULL DEFAULT '0',
  `QtyOnSO` double NOT NULL DEFAULT '0',
  `WhseItem` tinyint(1) NOT NULL DEFAULT '0',
  `SerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `DuplicateSN` tinyint(1) NOT NULL DEFAULT '0',
  `StrictSN` tinyint(1) NOT NULL DEFAULT '0',
  `BomCode` varchar(1) DEFAULT NULL,
  `SMtrxCol` int(11) DEFAULT NULL,
  `PMtrxCol` int(11) DEFAULT NULL,
  `JobQty` double NOT NULL DEFAULT '0',
  `cModel` varchar(50) DEFAULT NULL,
  `cRevision` varchar(50) DEFAULT NULL,
  `cComponent` varchar(50) DEFAULT NULL,
  `dDateReleased` datetime DEFAULT NULL,
  `iBinLocationID` int(11) DEFAULT NULL,
  `dStkitemTimeStamp` datetime DEFAULT NULL,
  `iInvSegValue1ID` int(11) DEFAULT NULL,
  `iInvSegValue2ID` int(11) DEFAULT NULL,
  `iInvSegValue3ID` int(11) DEFAULT NULL,
  `iInvSegValue4ID` int(11) DEFAULT NULL,
  `iInvSegValue5ID` int(11) DEFAULT NULL,
  `iInvSegValue6ID` int(11) DEFAULT NULL,
  `iInvSegValue7ID` int(11) DEFAULT NULL,
  `cExtDescription` varchar(255) DEFAULT NULL,
  `cSimpleCode` varchar(20) DEFAULT NULL,
  `bCommissionItem` tinyint(1) NOT NULL DEFAULT '1',
  `MFPQty` double NOT NULL DEFAULT '0',
  `bLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iLotStatus` int(11) DEFAULT NULL,
  `bLotMustExpire` tinyint(1) NOT NULL DEFAULT '1',
  `iItemCostingMethod` int(11) NOT NULL DEFAULT '0',
  `fItemLastGRVCost` double NOT NULL DEFAULT '0',
  `iEUCommodityID` int(11) NOT NULL DEFAULT '0',
  `iEUSupplementaryUnitID` int(11) NOT NULL DEFAULT '0',
  `fNetMass` double NOT NULL DEFAULT '0',
  `iUOMStockingUnitID` int(11) DEFAULT NULL,
  `iUOMDefPurchaseUnitID` int(11) DEFAULT NULL,
  `iUOMDefSellUnitID` int(11) DEFAULT NULL,
  `fStockGPPercent` float(24,0) DEFAULT NULL,
  `bAllowNegStock` tinyint(1) NOT NULL DEFAULT '0',
  `fQtyToDeliver` double DEFAULT NULL,
  `StkItem_fLeadDays` double DEFAULT NULL,
  `cEachDescription` varchar(30) DEFAULT '0',
  `cMeasurement` varchar(5) DEFAULT '0',
  `fBuyLength` double DEFAULT '0',
  `fBuyWidth` double DEFAULT '0',
  `fBuyHeight` double DEFAULT '0',
  `fBuyArea` double DEFAULT '0',
  `fBuyVolume` double DEFAULT '0',
  `cBuyWeight` double DEFAULT '0',
  `cBuyUnit` varchar(5) DEFAULT '0',
  `fSellLength` double DEFAULT '0',
  `fSellWidth` double DEFAULT '0',
  `fSellHeight` double DEFAULT '0',
  `fSellArea` double DEFAULT '0',
  `fSellVolume` double DEFAULT '0',
  `cSellWeight` double DEFAULT '0',
  `cSellUnit` varchar(5) DEFAULT '0',
  `bOverrideSell` tinyint(1) DEFAULT '0',
  `bUOMItem` tinyint(1) NOT NULL DEFAULT '0',
  `bDimensionItem` tinyint(1) NOT NULL DEFAULT '0',
  `iBuyingAgentID` int(11) DEFAULT NULL,
  `bVASItem` tinyint(1) NOT NULL DEFAULT '0',
  `bAirtimeItem` tinyint(1) NOT NULL DEFAULT '0',
  `fIBTQtyToIssue` double NOT NULL DEFAULT '0',
  `fIBTQtyToReceive` double NOT NULL DEFAULT '0',
  `StkItem_iBranchID` int(11) DEFAULT NULL,
  `StkItem_dCreatedDate` datetime DEFAULT NULL,
  `StkItem_dModifiedDate` datetime DEFAULT NULL,
  `StkItem_iCreatedBranchID` int(11) DEFAULT NULL,
  `StkItem_iModifiedBranchID` int(11) DEFAULT NULL,
  `StkItem_iCreatedAgentID` int(11) DEFAULT NULL,
  `StkItem_iModifiedAgentID` int(11) DEFAULT NULL,
  `StkItem_iChangeSetID` int(11) DEFAULT NULL,
  `StkItem_Checksum` binary(20) DEFAULT NULL,
  `bSyncToSOT` tinyint(1) NOT NULL DEFAULT '0',
  `ucIIUnitWt` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `original_transaction_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `cashbook_id` int(11) DEFAULT NULL,
  `petty_cash_id` int(11) DEFAULT NULL,
  `transaction_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `transaction_data` text COLLATE utf8_unicode_ci,
  `reversed` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `printed` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `agent_id`, `role_id`, `name`, `email`, `password`, `permissions`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 'superuser', 'smodavprivate@gmail.com', '$2y$10$Sm9E0C1TlrTucoUC5w//LuwiBYEVW5PUjaJa6nRXyALt7EaCpNrXy', '["superuser"]', NULL, '2016-10-28 09:46:07', '2016-10-28 09:46:07');

DROP TABLE IF EXISTS `Vendor`;
CREATE TABLE `Vendor` (
  `DCLink` int(11) NOT NULL,
  `Account` varchar(20) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Title` varchar(5) DEFAULT NULL,
  `Init` varchar(6) DEFAULT NULL,
  `Contact_Person` varchar(30) DEFAULT NULL,
  `Physical1` varchar(40) DEFAULT NULL,
  `Physical2` varchar(40) DEFAULT NULL,
  `Physical3` varchar(40) DEFAULT NULL,
  `Physical4` varchar(40) DEFAULT NULL,
  `Physical5` varchar(40) DEFAULT NULL,
  `PhysicalPC` varchar(15) DEFAULT NULL,
  `Addressee` varchar(30) DEFAULT NULL,
  `Post1` varchar(40) DEFAULT NULL,
  `Post2` varchar(40) DEFAULT NULL,
  `Post3` varchar(40) DEFAULT NULL,
  `Post4` varchar(40) DEFAULT NULL,
  `Post5` varchar(40) DEFAULT NULL,
  `PostPC` varchar(15) DEFAULT NULL,
  `Delivered_To` varchar(30) DEFAULT NULL,
  `Telephone` varchar(25) DEFAULT NULL,
  `Telephone2` varchar(25) DEFAULT NULL,
  `Fax1` varchar(25) DEFAULT NULL,
  `Fax2` varchar(25) DEFAULT NULL,
  `AccountTerms` int(11) DEFAULT NULL,
  `CT` tinyint(1) NOT NULL DEFAULT '0',
  `Tax_Number` varchar(50) DEFAULT NULL,
  `Registration` varchar(20) DEFAULT NULL,
  `Credit_Limit` double DEFAULT NULL,
  `Interest_Rate` double DEFAULT NULL,
  `Discount` double DEFAULT NULL,
  `On_Hold` tinyint(1) NOT NULL DEFAULT '0',
  `BFOpenType` int(11) DEFAULT NULL,
  `EMail` varchar(200) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `AutoDisc` double DEFAULT NULL,
  `DiscMtrxRow` int(11) DEFAULT NULL,
  `DCBalance` double DEFAULT NULL,
  `CheckTerms` tinyint(1) NOT NULL DEFAULT '0',
  `UseEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iBusTypeID` int(11) DEFAULT NULL,
  `iBusClassID` int(11) DEFAULT NULL,
  `iCountryID` int(11) DEFAULT NULL,
  `cAccDescription` varchar(80) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `dTimeStamp` datetime DEFAULT NULL,
  `iClassID` int(11) DEFAULT NULL,
  `iAreasID` int(11) DEFAULT NULL,
  `cBankRefNr` varchar(30) DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bStatPrint` tinyint(1) NOT NULL DEFAULT '0',
  `bStatEmail` tinyint(1) NOT NULL DEFAULT '0',
  `cStatEmailPass` varchar(160) DEFAULT NULL,
  `bForCurAcc` tinyint(1) NOT NULL DEFAULT '0',
  `bRemittanceChequeEFTS` tinyint(1) NOT NULL DEFAULT '1',
  `fForeignBalance` double DEFAULT NULL,
  `iSettlementTermsID` int(11) NOT NULL DEFAULT '0',
  `bSourceDocPrint` tinyint(1) NOT NULL DEFAULT '1',
  `bSourceDocEmail` tinyint(1) NOT NULL DEFAULT '0',
  `iEUCountryID` int(11) NOT NULL DEFAULT '0',
  `iDefTaxTypeID` int(11) NOT NULL DEFAULT '0',
  `iAgeingTermID` int(11) DEFAULT NULL,
  `iBankDetailType` tinyint(3) UNSIGNED DEFAULT NULL,
  `cBankAccHolder` varchar(30) DEFAULT NULL,
  `cIDNumber` varchar(20) DEFAULT NULL,
  `cPassportNumber` varchar(20) DEFAULT NULL,
  `cBankCode` varchar(15) DEFAULT NULL,
  `cSwiftCode` varchar(11) DEFAULT NULL,
  `iBankDetailID` int(11) DEFAULT NULL,
  `Vendor_iBranchID` int(11) DEFAULT NULL,
  `Vendor_dCreatedDate` datetime DEFAULT NULL,
  `Vendor_dModifiedDate` datetime DEFAULT NULL,
  `Vendor_iCreatedBranchID` int(11) DEFAULT NULL,
  `Vendor_iModifiedBranchID` int(11) DEFAULT NULL,
  `Vendor_iCreatedAgentID` int(11) DEFAULT NULL,
  `Vendor_iModifiedAgentID` int(11) DEFAULT NULL,
  `Vendor_iChangeSetID` int(11) DEFAULT NULL,
  `Vendor_Checksum` binary(20) DEFAULT NULL,
  `iSPQueueID` int(11) DEFAULT NULL,
  `ulAPSector1` varchar(100) DEFAULT NULL,
  `ulAPSector2` varchar(100) DEFAULT NULL,
  `ulAPSector3` varchar(100) DEFAULT NULL,
  `ulAPSector4` varchar(100) DEFAULT NULL,
  `ulAPSector5` varchar(100) DEFAULT NULL,
  `ulAPSector6` varchar(100) DEFAULT NULL,
  `ulAPSector7` varchar(100) DEFAULT NULL,
  `ulAPSector8` varchar(100) DEFAULT NULL,
  `ulAPSector9` varchar(100) DEFAULT NULL,
  `ulAPSector10` varchar(100) DEFAULT NULL,
  `ulAPSupplierLocation` varchar(100) DEFAULT NULL,
  `udAPRegistrationDate` datetime DEFAULT NULL,
  `ubAPBEECompliant` tinyint(1) DEFAULT '1',
  `ulAPStatus` varchar(100) DEFAULT NULL,
  `ulAPDeliveryGoods` varchar(100) DEFAULT NULL,
  `ubAPFailToDeliver` tinyint(1) DEFAULT '1',
  `ubAPWorkforGovt` tinyint(1) DEFAULT '1',
  `ubAPBlacklistedbyGovt` tinyint(1) DEFAULT '1',
  `ubAPConfirmCompany` tinyint(1) DEFAULT '1',
  `ufAPHDI` double DEFAULT NULL,
  `ufAPFunctionality` double DEFAULT NULL,
  `ufAPExperience` double DEFAULT NULL,
  `ufAPWomen` double DEFAULT NULL,
  `ufAPWhite` double DEFAULT NULL,
  `ufAPMiscellaneous` double DEFAULT NULL,
  `ufAPPrice` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `WhseMst`;
CREATE TABLE `WhseMst` (
  `WhseLink` int(11) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `KnownAs` varchar(50) DEFAULT NULL,
  `Address1` varchar(40) DEFAULT NULL,
  `Address2` varchar(40) DEFAULT NULL,
  `Address3` varchar(40) DEFAULT NULL,
  `PostCode` varchar(15) DEFAULT NULL,
  `Tel` varchar(15) DEFAULT NULL,
  `Manager` varchar(30) DEFAULT NULL,
  `BankLink` int(11) DEFAULT NULL,
  `BranchCode` varchar(30) DEFAULT NULL,
  `BankAccNum` varchar(30) DEFAULT NULL,
  `BankAccType` varchar(30) DEFAULT NULL,
  `EMail` varchar(60) DEFAULT NULL,
  `ModemTel` varchar(15) DEFAULT NULL,
  `DefaultWhse` tinyint(1) NOT NULL DEFAULT '0',
  `AddNewStock` tinyint(1) NOT NULL DEFAULT '0',
  `dWarehouseTimeStamp` datetime DEFAULT NULL,
  `iWhseTypeID` int(11) NOT NULL DEFAULT '0',
  `bAllowToBuyInto` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowToSellFrom` tinyint(1) NOT NULL DEFAULT '1',
  `bAllowNegStock` tinyint(1) NOT NULL DEFAULT '0',
  `cDefaultItemGroup` varchar(20) DEFAULT NULL,
  `WhseMst_iBranchID` int(11) DEFAULT NULL,
  `WhseMst_dCreatedDate` datetime DEFAULT NULL,
  `WhseMst_dModifiedDate` datetime DEFAULT NULL,
  `WhseMst_iCreatedBranchID` int(11) DEFAULT NULL,
  `WhseMst_iModifiedBranchID` int(11) DEFAULT NULL,
  `WhseMst_iCreatedAgentID` int(11) DEFAULT NULL,
  `WhseMst_iModifiedAgentID` int(11) DEFAULT NULL,
  `WhseMst_iChangeSetID` int(11) DEFAULT NULL,
  `WhseMst_Checksum` binary(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `WhseStk`;
CREATE TABLE `WhseStk` (
  `IdWhseStk` bigint(20) NOT NULL,
  `WHWhseID` int(11) DEFAULT NULL,
  `WHStockLink` int(11) NOT NULL,
  `WHStockGroup` varchar(20) DEFAULT NULL,
  `WHQtyOnHand` double NOT NULL DEFAULT '0',
  `WHQtyOnSO` double NOT NULL DEFAULT '0',
  `WHQtyOnPO` double NOT NULL DEFAULT '0',
  `WHQtyReserved` double NOT NULL DEFAULT '0',
  `WHTTInv` varchar(10) DEFAULT NULL,
  `WHTTCrn` varchar(10) DEFAULT NULL,
  `WHTTGrv` varchar(10) DEFAULT NULL,
  `WHTTRts` varchar(10) DEFAULT NULL,
  `WHBarCode` varchar(400) DEFAULT NULL,
  `WHRe_Ord_Lvl` double DEFAULT NULL,
  `WHRe_Ord_Qty` double DEFAULT NULL,
  `WHMin_Lvl` double DEFAULT NULL,
  `WHMax_Lvl` double DEFAULT NULL,
  `WHUsePriceDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHUseInfoDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHUseOrderDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHUseDefaultDefs` tinyint(1) NOT NULL DEFAULT '1',
  `WHPackCode` varchar(5) DEFAULT NULL,
  `WHJobQty` double NOT NULL DEFAULT '0',
  `iBinLocationID` int(11) DEFAULT NULL,
  `fLGRVCount` double DEFAULT NULL,
  `WHMFPQty` double NOT NULL DEFAULT '0',
  `WHUseSupplierDefs` tinyint(1) NOT NULL DEFAULT '1',
  `fAverageCost` double NOT NULL DEFAULT '0',
  `fLatestCost` double NOT NULL DEFAULT '0',
  `fLowestCost` double NOT NULL DEFAULT '0',
  `fHighestCost` double NOT NULL DEFAULT '0',
  `fManualCost` double NOT NULL DEFAULT '0',
  `fWhseLastGRVCost` double NOT NULL DEFAULT '0',
  `bWHAllowNegStock` tinyint(1) NOT NULL DEFAULT '0',
  `fWHQtyToDeliver` double DEFAULT NULL,
  `WhseStk_fLeadDays` double DEFAULT NULL,
  `WHBuyingAgentID` int(11) DEFAULT NULL,
  `fIBTQtyToIssue` double NOT NULL DEFAULT '0',
  `fIBTQtyToReceive` double NOT NULL DEFAULT '0',
  `WhseStk_iBranchID` int(11) DEFAULT NULL,
  `WhseStk_dCreatedDate` datetime DEFAULT NULL,
  `WhseStk_dModifiedDate` datetime DEFAULT NULL,
  `WhseStk_iCreatedBranchID` int(11) DEFAULT NULL,
  `WhseStk_iModifiedBranchID` int(11) DEFAULT NULL,
  `WhseStk_iCreatedAgentID` int(11) DEFAULT NULL,
  `WhseStk_iModifiedAgentID` int(11) DEFAULT NULL,
  `WhseStk_iChangeSetID` int(11) DEFAULT NULL,
  `WhseStk_Checksum` binary(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_btblCbBatches`;
CREATE TABLE `_btblCbBatches` (
  `idBatches` int(11) NOT NULL,
  `cBatchNo` varchar(50) DEFAULT NULL,
  `cBatchDesc` varchar(40) DEFAULT NULL,
  `bModuleGL` tinyint(1) NOT NULL,
  `bModuleAR` tinyint(1) NOT NULL,
  `bModuleAP` tinyint(1) NOT NULL,
  `iInputTaxID` int(11) DEFAULT NULL,
  `iInputTaxAccID` int(11) DEFAULT NULL,
  `iOutputTaxID` int(11) DEFAULT NULL,
  `iOutputTaxAccID` int(11) DEFAULT NULL,
  `bCalcTax` tinyint(1) NOT NULL,
  `iTrCodeID` int(11) DEFAULT NULL,
  `bClearBatch` tinyint(1) NOT NULL,
  `iDateLineOpt` int(11) DEFAULT NULL,
  `dDefDate` datetime DEFAULT NULL,
  `iRefLineOpt` int(11) DEFAULT NULL,
  `cDefRef` varchar(20) DEFAULT NULL,
  `iDescLineOpt` int(11) DEFAULT NULL,
  `cDefDesc` varchar(40) DEFAULT NULL,
  `iGLBankAccID` int(11) DEFAULT NULL,
  `iGLARAccID` int(11) DEFAULT NULL,
  `iGLAPAccID` int(11) DEFAULT NULL,
  `iDefModule` int(11) DEFAULT NULL,
  `bAllowDisc` tinyint(1) NOT NULL DEFAULT '1',
  `iARDiscTrCodeID` int(11) DEFAULT NULL,
  `iAPDiscTrCodeID` int(11) DEFAULT NULL,
  `cDiscDesc` varchar(40) DEFAULT NULL,
  `bCheckedOut` tinyint(1) DEFAULT '0',
  `bDupRefs` tinyint(1) DEFAULT '1',
  `bPrintCheque` tinyint(1) NOT NULL DEFAULT '0',
  `bIncludeBankStatement` tinyint(1) NOT NULL DEFAULT '0',
  `iMaxRecur` int(11) DEFAULT NULL,
  `iBatchPosted` int(11) DEFAULT NULL,
  `cBatchRef` varchar(50) DEFAULT NULL,
  `fValidationTotDeposits` double DEFAULT NULL,
  `fValidationTotPayments` double DEFAULT NULL,
  `iCurrencyID` int(11) DEFAULT NULL,
  `bPromptGlobalChanges` tinyint(1) DEFAULT '0',
  `bTransDateOnCheque` tinyint(1) NOT NULL DEFAULT '0',
  `dDateBatchCreated` datetime DEFAULT NULL,
  `iAgentBatchCreated` int(11) NOT NULL DEFAULT '1',
  `iAgentCheckedOut` int(11) NOT NULL DEFAULT '0',
  `bApplySettDisc` tinyint(1) NOT NULL DEFAULT '0',
  `bAllowZeroValues` tinyint(1) NOT NULL DEFAULT '0',
  `bARAllowLinkedAccounts` tinyint(1) NOT NULL DEFAULT '0',
  `bInterBranchBatch` tinyint(1) NOT NULL DEFAULT '0',
  `iBranchLoanAccountID` int(11) DEFAULT NULL,
  `bOnlyAllowForCurrency` tinyint(1) NOT NULL DEFAULT '0',
  `dProcessedDate` datetime DEFAULT NULL,
  `bAutoAllocBBForward` tinyint(1) DEFAULT NULL,
  `bAutoAllocOpenItem` tinyint(1) NOT NULL DEFAULT '0',
  `bEFTSExport` tinyint(1) NOT NULL DEFAULT '0',
  `_btblCbBatches_iBranchID` int(11) DEFAULT NULL,
  `_btblCbBatches_dCreatedDate` datetime DEFAULT NULL,
  `_btblCbBatches_dModifiedDate` datetime DEFAULT NULL,
  `_btblCbBatches_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatches_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatches_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatches_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatches_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCbBatches_Checksum` binary(20) DEFAULT NULL,
  `bAutoAllocByReference` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_btblCbBatchLines`;
CREATE TABLE `_btblCbBatchLines` (
  `idBatchLines` int(11) NOT NULL,
  `iBatchesID` int(11) NOT NULL,
  `iSplitType` int(11) DEFAULT '0',
  `iSplitGroup` int(11) DEFAULT NULL,
  `dTxDate` datetime NOT NULL,
  `iModule` int(11) NOT NULL,
  `iAccountID` int(11) NOT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `cReference` varchar(50) DEFAULT NULL,
  `fDebit` double DEFAULT NULL,
  `fCredit` double DEFAULT NULL,
  `bReconcile` tinyint(1) NOT NULL,
  `fTaxAmount` double DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iTaxAccountID` int(11) DEFAULT NULL,
  `iProjectID` int(11) DEFAULT NULL,
  `bPostDated` tinyint(1) NOT NULL,
  `fDiscPerc` double DEFAULT NULL,
  `iDiscTrCodeID` int(11) DEFAULT NULL,
  `cDiscDesc` varchar(35) DEFAULT NULL,
  `iDiscTaxTypeID` int(11) DEFAULT NULL,
  `iDiscTaxAccID` int(11) DEFAULT NULL,
  `fDiscTaxAmount` double DEFAULT NULL,
  `cPayeeName` varchar(100) DEFAULT NULL,
  `bPrintCheque` tinyint(1) NOT NULL DEFAULT '0',
  `bChequePrinted` tinyint(1) NOT NULL DEFAULT '0',
  `iRepID` int(11) DEFAULT NULL,
  `fExchangeRate` double DEFAULT NULL,
  `fDebitForeign` double DEFAULT NULL,
  `fCreditForeign` double DEFAULT NULL,
  `fTaxAmountForeign` double DEFAULT NULL,
  `fDiscTaxAmountForeign` double DEFAULT NULL,
  `iCBBatchLinesReconID` int(11) DEFAULT NULL,
  `fFCAccountAmount` double NOT NULL DEFAULT '0',
  `fFCAccountExchange` double NOT NULL DEFAULT '1',
  `fFCAccountDiscAmount` double NOT NULL DEFAULT '0',
  `fFCAccountDiscTax` double NOT NULL DEFAULT '0',
  `iSettDiscGroupID` int(11) NOT NULL DEFAULT '0',
  `iSettDiscPostARAPID` bigint(20) NOT NULL DEFAULT '0',
  `cBankRef` varchar(20) DEFAULT NULL,
  `bIsPosted` tinyint(1) NOT NULL DEFAULT '0',
  `iMBPropertyID` int(11) DEFAULT '0',
  `iMBPortionID` int(11) DEFAULT '0',
  `iMBServiceID` int(11) DEFAULT '0',
  `iMBPropertyPortionServiceID` int(11) DEFAULT '0',
  `bMBOverride` tinyint(1) NOT NULL DEFAULT '0',
  `iMBMeterID` int(11) DEFAULT '0',
  `_btblCbBatchLines_iBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_dCreatedDate` datetime DEFAULT NULL,
  `_btblCbBatchLines_dModifiedDate` datetime DEFAULT NULL,
  `_btblCbBatchLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblCbBatchLines_Checksum` binary(20) DEFAULT NULL,
  `fFCAccountTaxAmount` double NOT NULL DEFAULT '0',
  `SagePayExtra1` longtext,
  `SagePayExtra2` longtext,
  `SagePayExtra3` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_btblInvoiceLines`;
CREATE TABLE `_btblInvoiceLines` (
  `idInvoiceLines` bigint(20) NOT NULL,
  `iInvoiceID` int(11) NOT NULL,
  `iOrigLineID` bigint(20) DEFAULT NULL,
  `iGrvLineID` bigint(20) DEFAULT NULL,
  `iLineDocketMode` int(11) DEFAULT NULL,
  `cDescription` varchar(100) DEFAULT NULL,
  `iUnitsOfMeasureStockingID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureCategoryID` int(11) DEFAULT NULL,
  `iUnitsOfMeasureID` int(11) DEFAULT NULL,
  `fQuantity` double DEFAULT NULL,
  `fQtyChange` double DEFAULT NULL,
  `fQtyToProcess` double DEFAULT NULL,
  `fQtyLastProcess` double DEFAULT NULL,
  `fQtyProcessed` double DEFAULT NULL,
  `fQtyReserved` double DEFAULT NULL,
  `fQtyReservedChange` double DEFAULT NULL,
  `cLineNotes` longtext,
  `fUnitPriceExcl` double DEFAULT NULL,
  `fUnitPriceIncl` double DEFAULT NULL,
  `iUnitPriceOverrideReasonID` int(11) DEFAULT NULL,
  `fUnitCost` double DEFAULT NULL,
  `fLineDiscount` double DEFAULT NULL,
  `iLineDiscountReasonID` int(11) DEFAULT NULL,
  `iReturnReasonID` int(11) DEFAULT NULL,
  `fTaxRate` double DEFAULT NULL,
  `bIsSerialItem` tinyint(1) NOT NULL DEFAULT '0',
  `bIsWhseItem` tinyint(1) NOT NULL DEFAULT '0',
  `fAddCost` double DEFAULT NULL,
  `cTradeinItem` varchar(20) DEFAULT NULL,
  `iStockCodeID` int(11) DEFAULT NULL,
  `iJobID` int(11) DEFAULT NULL,
  `iWarehouseID` int(11) DEFAULT NULL,
  `iTaxTypeID` int(11) DEFAULT NULL,
  `iPriceListNameID` int(11) DEFAULT NULL,
  `fQuantityLineTotIncl` double DEFAULT NULL,
  `fQuantityLineTotExcl` double DEFAULT NULL,
  `fQuantityLineTotInclNoDisc` double DEFAULT NULL,
  `fQuantityLineTotExclNoDisc` double DEFAULT NULL,
  `fQuantityLineTaxAmount` double DEFAULT NULL,
  `fQuantityLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyChangeLineTotIncl` double DEFAULT NULL,
  `fQtyChangeLineTotExcl` double DEFAULT NULL,
  `fQtyChangeLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyChangeLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyChangeLineTaxAmount` double DEFAULT NULL,
  `fQtyChangeLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyToProcessLineTotIncl` double DEFAULT NULL,
  `fQtyToProcessLineTotExcl` double DEFAULT NULL,
  `fQtyToProcessLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyToProcessLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmount` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyLastProcessLineTotIncl` double DEFAULT NULL,
  `fQtyLastProcessLineTotExcl` double DEFAULT NULL,
  `fQtyLastProcessLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyLastProcessLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmount` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmountNoDisc` double DEFAULT NULL,
  `fQtyProcessedLineTotIncl` double DEFAULT NULL,
  `fQtyProcessedLineTotExcl` double DEFAULT NULL,
  `fQtyProcessedLineTotInclNoDisc` double DEFAULT NULL,
  `fQtyProcessedLineTotExclNoDisc` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmount` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmountNoDisc` double DEFAULT NULL,
  `fUnitPriceExclForeign` double DEFAULT NULL,
  `fUnitPriceInclForeign` double DEFAULT NULL,
  `fUnitCostForeign` double DEFAULT NULL,
  `fAddCostForeign` double DEFAULT NULL,
  `fQuantityLineTotInclForeign` double DEFAULT NULL,
  `fQuantityLineTotExclForeign` double DEFAULT NULL,
  `fQuantityLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQuantityLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQuantityLineTaxAmountForeign` double DEFAULT NULL,
  `fQuantityLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyChangeLineTotInclForeign` double DEFAULT NULL,
  `fQtyChangeLineTotExclForeign` double DEFAULT NULL,
  `fQtyChangeLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyChangeLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyChangeLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyChangeLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotInclForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotExclForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyToProcessLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyToProcessLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotInclForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotExclForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyLastProcessLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotInclForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotExclForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotInclNoDiscForeign` double DEFAULT NULL,
  `fQtyProcessedLineTotExclNoDiscForeign` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmountForeign` double DEFAULT NULL,
  `fQtyProcessedLineTaxAmountNoDiscForeign` double DEFAULT NULL,
  `iLineRepID` int(11) DEFAULT NULL,
  `iLineProjectID` int(11) DEFAULT NULL,
  `iLedgerAccountID` int(11) DEFAULT NULL,
  `iModule` int(11) NOT NULL DEFAULT '0',
  `bChargeCom` tinyint(1) NOT NULL DEFAULT '1',
  `bIsLotItem` tinyint(1) NOT NULL DEFAULT '0',
  `iLotID` int(11) DEFAULT NULL,
  `cLotNumber` varchar(50) DEFAULT NULL,
  `dLotExpiryDate` datetime DEFAULT NULL,
  `iMFPID` int(11) DEFAULT NULL,
  `iLineID` int(11) NOT NULL DEFAULT '0',
  `iLinkedLineID` bigint(20) NOT NULL DEFAULT '0',
  `fQtyLinkedUsed` double DEFAULT NULL,
  `fUnitPriceInclOrig` double DEFAULT NULL,
  `fUnitPriceExclOrig` double DEFAULT NULL,
  `fUnitPriceInclForeignOrig` double DEFAULT NULL,
  `fUnitPriceExclForeignOrig` double DEFAULT NULL,
  `iDeliveryMethodID` int(11) DEFAULT NULL,
  `fQtyDeliver` double DEFAULT NULL,
  `dDeliveryDate` datetime DEFAULT NULL,
  `iDeliveryStatus` int(11) DEFAULT NULL,
  `fQtyForDelivery` double DEFAULT NULL,
  `bPromotionApplied` tinyint(1) NOT NULL DEFAULT '0',
  `fPromotionPriceExcl` double DEFAULT NULL,
  `fPromotionPriceIncl` double DEFAULT NULL,
  `cPromotionCode` varchar(20) DEFAULT NULL,
  `iSOLinkedPOLineID` bigint(20) NOT NULL DEFAULT '0',
  `fLength` double DEFAULT '0',
  `fWidth` double DEFAULT '0',
  `fHeight` double DEFAULT '0',
  `iPieces` int(11) DEFAULT '0',
  `iPiecesToProcess` int(11) DEFAULT '0',
  `iPiecesLastProcess` int(11) DEFAULT '0',
  `iPiecesProcessed` int(11) DEFAULT '0',
  `iPiecesReserved` int(11) DEFAULT '0',
  `iPiecesDeliver` int(11) DEFAULT '0',
  `iPiecesForDelivery` int(11) DEFAULT '0',
  `fQuantityUR` double DEFAULT NULL,
  `fQtyChangeUR` double DEFAULT NULL,
  `fQtyToProcessUR` double DEFAULT NULL,
  `fQtyLastProcessUR` double DEFAULT NULL,
  `fQtyProcessedUR` double DEFAULT NULL,
  `fQtyReservedUR` double DEFAULT NULL,
  `fQtyReservedChangeUR` double DEFAULT NULL,
  `fQtyDeliverUR` double DEFAULT NULL,
  `fQtyForDeliveryUR` double DEFAULT NULL,
  `fQtyLinkedUsedUR` double DEFAULT NULL,
  `iPiecesLinkedUsed` int(11) DEFAULT NULL,
  `iSalesWhseID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_dCreatedDate` datetime DEFAULT NULL,
  `_btblInvoiceLines_dModifiedDate` datetime DEFAULT NULL,
  `_btblInvoiceLines_iCreatedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iModifiedBranchID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iCreatedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iModifiedAgentID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_iChangeSetID` int(11) DEFAULT NULL,
  `_btblInvoiceLines_Checksum` binary(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_etblPriceListName`;
CREATE TABLE `_etblPriceListName` (
  `IDPriceListName` int(11) NOT NULL,
  `cName` varchar(30) NOT NULL,
  `cDescription` varchar(60) DEFAULT NULL,
  `bDefault` tinyint(1) NOT NULL DEFAULT '0',
  `iCurrencyID` int(11) DEFAULT NULL,
  `dPLNameTimeStamp` datetime DEFAULT NULL,
  `_etblPriceListName_iBranchID` int(11) DEFAULT NULL,
  `_etblPriceListName_dCreatedDate` datetime DEFAULT NULL,
  `_etblPriceListName_dModifiedDate` datetime DEFAULT NULL,
  `_etblPriceListName_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListName_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListName_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListName_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListName_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPriceListName_Checksum` binary(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_etblPriceListPrices`;
CREATE TABLE `_etblPriceListPrices` (
  `IDPriceListPrices` bigint(20) NOT NULL,
  `iPriceListNameID` int(11) NOT NULL,
  `iStockID` int(11) NOT NULL,
  `iWarehouseID` int(11) DEFAULT '0',
  `bUseMarkup` tinyint(1) NOT NULL DEFAULT '0',
  `iMarkupOnCost` int(11) NOT NULL DEFAULT '0',
  `fMarkupRate` double DEFAULT NULL,
  `fExclPrice` double DEFAULT NULL,
  `fInclPrice` double DEFAULT NULL,
  `dPLPricesTimeStamp` datetime DEFAULT NULL,
  `_etblPriceListPrices_iBranchID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_dCreatedDate` datetime DEFAULT NULL,
  `_etblPriceListPrices_dModifiedDate` datetime DEFAULT NULL,
  `_etblPriceListPrices_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_iChangeSetID` int(11) DEFAULT NULL,
  `_etblPriceListPrices_Checksum` binary(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_rtblAgents`;
CREATE TABLE `_rtblAgents` (
  `idAgents` int(11) NOT NULL,
  `bSysAccount` tinyint(1) NOT NULL DEFAULT '0',
  `cAgentName` varchar(50) DEFAULT NULL,
  `cPassword` varchar(160) DEFAULT NULL,
  `cFirstName` varchar(20) DEFAULT NULL,
  `cInitials` varchar(5) DEFAULT NULL,
  `cLastName` varchar(30) DEFAULT NULL,
  `cTitle` varchar(6) DEFAULT NULL,
  `cDisplayName` varchar(60) DEFAULT NULL,
  `cDescription` varchar(50) DEFAULT NULL,
  `cTelWork` varchar(20) DEFAULT NULL,
  `cTelFax` varchar(20) DEFAULT NULL,
  `cTelMobile` varchar(20) DEFAULT NULL,
  `cTelHome` varchar(20) DEFAULT NULL,
  `cEmail` varchar(60) DEFAULT NULL,
  `cWebPage` varchar(50) DEFAULT NULL,
  `cComments` varchar(1024) DEFAULT NULL,
  `cAddressStreet` varchar(512) DEFAULT NULL,
  `cAddressPOBox` varchar(30) DEFAULT NULL,
  `cAddressCity` varchar(30) DEFAULT NULL,
  `cAddressState` varchar(30) DEFAULT NULL,
  `cAddressZip` varchar(15) DEFAULT NULL,
  `cAddressCountry` varchar(30) DEFAULT NULL,
  `bCanAssign` tinyint(1) NOT NULL DEFAULT '1',
  `bPwdCanChange` tinyint(1) NOT NULL DEFAULT '1',
  `bPwdMustChange` tinyint(1) NOT NULL DEFAULT '1',
  `bPwdChangeEvery` tinyint(1) NOT NULL DEFAULT '0',
  `iPwdChangeDays` int(11) DEFAULT NULL,
  `dPwdLastChange` datetime DEFAULT NULL,
  `cPwdRemind` varchar(160) DEFAULT NULL,
  `bAgentOutOffice` tinyint(1) NOT NULL DEFAULT '0',
  `bExitWarning` tinyint(1) NOT NULL DEFAULT '1',
  `bCanSetOutOfOffice` tinyint(1) NOT NULL DEFAULT '1',
  `bKnowledgeBaseWarning` tinyint(1) NOT NULL DEFAULT '1',
  `bNewIncidentNotification` tinyint(1) NOT NULL DEFAULT '0',
  `bUseDefaultTree` tinyint(1) NOT NULL DEFAULT '1',
  `bAutoSpellCheck` tinyint(1) NOT NULL DEFAULT '0',
  `iDefIncidentTypeGroupID` int(11) DEFAULT NULL,
  `iDefTillId` int(11) DEFAULT NULL,
  `iDefCashAccount` int(11) DEFAULT NULL,
  `iDefWhseId` int(11) DEFAULT NULL,
  `iNotifyEscalateMinutes` int(11) DEFAULT NULL,
  `iNotifyDueMinutes` int(11) DEFAULT NULL,
  `bForceThisWarehouse` tinyint(1) NOT NULL DEFAULT '0',
  `bAgentActive` tinyint(1) NOT NULL DEFAULT '1',
  `bCBAgNoneVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bCBAgAllVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bJRAgNoneVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bJRAgAllVisible` tinyint(1) NOT NULL DEFAULT '0',
  `bCBUseGrpDefaults` tinyint(1) NOT NULL DEFAULT '0',
  `bJRUseGrpDefaults` tinyint(1) NOT NULL DEFAULT '0',
  `bCBAgOwnVisible` tinyint(1) NOT NULL DEFAULT '1',
  `bJRAgOwnVisible` tinyint(1) NOT NULL DEFAULT '1',
  `iPOAuthType` int(11) NOT NULL DEFAULT '0',
  `iPOIncidentTypeID` int(11) DEFAULT NULL,
  `bPOExclusive` tinyint(1) NOT NULL DEFAULT '1',
  `fPOLimit` double DEFAULT NULL,
  `bPOUseGrpDefaults` tinyint(1) NOT NULL DEFAULT '1',
  `cAccessPurchaseWhIDLst` varchar(1024) DEFAULT NULL,
  `cAccessSalesWhIDLst` varchar(1024) DEFAULT NULL,
  `cAccessOtherTxWhIDList` varchar(1024) DEFAULT NULL,
  `cAccessPurchaseWhChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessSalesWhChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessOtherTxWhChkLstInd` char(1) NOT NULL DEFAULT '2',
  `iDefProjectID` int(11) DEFAULT NULL,
  `cAccessProjectIDLst` varchar(1024) DEFAULT NULL,
  `cAccessProjectChkLstInd` char(1) DEFAULT NULL,
  `iDefRepID` int(11) DEFAULT NULL,
  `cAccessRepIDLst` varchar(1024) DEFAULT NULL,
  `cAccessRepChkLstInd` char(1) DEFAULT NULL,
  `Max_LDisc` double NOT NULL DEFAULT '100',
  `Max_Disc` double NOT NULL DEFAULT '100',
  `cOperatorCode` varchar(50) DEFAULT NULL,
  `cOperatorPassword` varchar(50) DEFAULT NULL,
  `cOperatorNewPassword` varchar(50) DEFAULT NULL,
  `cAccessBranchIDLst` varchar(1024) DEFAULT NULL,
  `cAccessBranchChkLstInd` char(1) DEFAULT NULL,
  `iDocketInputMode` int(11) NOT NULL DEFAULT '0',
  `cOperatorCodePOS` varchar(50) DEFAULT NULL,
  `cOperatorPasswordPOS` varchar(50) DEFAULT NULL,
  `cOperatorNewPasswordPOS` varchar(50) DEFAULT NULL,
  `bCanChangeSessionDate` tinyint(1) NOT NULL DEFAULT '0',
  `cEFTOperatorCode` varchar(6) DEFAULT NULL,
  `bSupervisorAgent` tinyint(1) DEFAULT '0',
  `bQuoteAir` tinyint(1) NOT NULL DEFAULT '0',
  `bLockedOut` tinyint(1) NOT NULL DEFAULT '0',
  `cAccessARGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessARGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `bIncludeARNoGroups` tinyint(1) NOT NULL DEFAULT '1',
  `bApplyARGroupsToEnqRep` tinyint(1) DEFAULT NULL,
  `cAccessAPGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessAPGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `bIncludeAPNoGroups` tinyint(1) NOT NULL DEFAULT '1',
  `bApplyAPGroupsToEnqRep` tinyint(1) DEFAULT NULL,
  `vbBiometric` longtext,
  `fDefMax_LDisc` double DEFAULT NULL,
  `fDefMax_Disc` double DEFAULT NULL,
  `bApplyAccessRepsToReports` tinyint(1) DEFAULT NULL,
  `bApplyAccessProjectsToReports` tinyint(1) DEFAULT NULL,
  `idPOSMenuSetup` int(11) DEFAULT NULL,
  `bAgentIsBuyer` tinyint(1) NOT NULL DEFAULT '0',
  `bUseBiometric` tinyint(1) NOT NULL DEFAULT '0',
  `FiscalPrinterId` int(11) NOT NULL DEFAULT '0',
  `FiscalDeviceId` int(11) NOT NULL DEFAULT '0',
  `_rtblAgents_iBranchID` int(11) DEFAULT NULL,
  `_rtblAgents_dCreatedDate` datetime DEFAULT NULL,
  `_rtblAgents_dModifiedDate` datetime DEFAULT NULL,
  `_rtblAgents_iCreatedBranchID` int(11) DEFAULT NULL,
  `_rtblAgents_iModifiedBranchID` int(11) DEFAULT NULL,
  `_rtblAgents_iCreatedAgentID` int(11) DEFAULT NULL,
  `_rtblAgents_iModifiedAgentID` int(11) DEFAULT NULL,
  `_rtblAgents_iChangeSetID` int(11) DEFAULT NULL,
  `_rtblAgents_Checksum` binary(20) DEFAULT NULL,
  `cAccessDocCatGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessDocCatGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `iDefDocCatID` int(11) DEFAULT NULL,
  `cAccessDocCatIDLst` varchar(1024) DEFAULT NULL,
  `cAccessDocCatChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessIncidentTypeGroupIDLst` varchar(1024) DEFAULT NULL,
  `cAccessIncidentTypeGroupChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cAccessIncidentTypeIDLst` varchar(1024) DEFAULT NULL,
  `cAccessIncidentTypeChkLstInd` char(1) NOT NULL DEFAULT '2',
  `cSagePayUserName` varchar(50) DEFAULT NULL,
  `cSagePayPassword` varchar(160) DEFAULT NULL,
  `cSagePayPIN` varchar(30) DEFAULT NULL,
  `iAgentLoginScreen` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `_etblUnits`;
CREATE TABLE `_etblUnits` (
  `idUnits` int(11) NOT NULL,
  `cUnitCode` varchar(50) DEFAULT NULL,
  `cUnitDescription` varchar(50) DEFAULT NULL,
  `iUnitCategoryID` int(11) DEFAULT NULL,
  `bUnitRoundUp` tinyint(1) NOT NULL DEFAULT '0',
  `_etblUnits_iBranchID` int(11) DEFAULT NULL,
  `_etblUnits_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblUnits_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblUnits_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblUnits_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblUnits_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblUnits_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblUnits_iChangeSetID` int(11) DEFAULT NULL,
  `_etblUnits_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `_etblUnitConversion`;
CREATE TABLE `_etblUnitConversion` (
  `idUnitConversion` int(11) NOT NULL,
  `iUnitAID` int(11) NOT NULL,
  `fUnitAQty` double NOT NULL,
  `iUnitBID` int(11) NOT NULL,
  `fUnitBQty` double NOT NULL,
  `fMarkup` double DEFAULT NULL,
  `_etblUnitConversion_iBranchID` int(11) DEFAULT NULL,
  `_etblUnitConversion_dCreatedDate` datetime(6) DEFAULT NULL,
  `_etblUnitConversion_dModifiedDate` datetime(6) DEFAULT NULL,
  `_etblUnitConversion_iCreatedBranchID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iModifiedBranchID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iCreatedAgentID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iModifiedAgentID` int(11) DEFAULT NULL,
  `_etblUnitConversion_iChangeSetID` int(11) DEFAULT NULL,
  `_etblUnitConversion_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `TaxRate`;
CREATE TABLE `TaxRate` (
  `idTaxRate` int(11) NOT NULL,
  `Code` varchar(10) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `TaxRate` double DEFAULT NULL,
  `TaxRate_iBranchID` int(11) DEFAULT NULL,
  `TaxRate_dCreatedDate` datetime(6) DEFAULT NULL,
  `TaxRate_dModifiedDate` datetime(6) DEFAULT NULL,
  `TaxRate_iCreatedBranchID` int(11) DEFAULT NULL,
  `TaxRate_iModifiedBranchID` int(11) DEFAULT NULL,
  `TaxRate_iCreatedAgentID` int(11) DEFAULT NULL,
  `TaxRate_iModifiedAgentID` int(11) DEFAULT NULL,
  `TaxRate_iChangeSetID` int(11) DEFAULT NULL,
  `TaxRate_Checksum` binary(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `syncs` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sql` text COLLATE utf8_unicode_ci NOT NULL,
  `bindings` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignments_user_id_index` (`user_id`),
  ADD KEY `assignments_warehouse_id_index` (`warehouse_id`);

ALTER TABLE `Client`
  ADD PRIMARY KEY (`DCLink`),
  ADD KEY `idx_Client_cIDNumber` (`cIDNumber`),
  ADD KEY `idx_Client_Client_Checksum` (`Client_Checksum`),
  ADD KEY `idx_Client_Client_dModifiedDate` (`Client_dModifiedDate`),
  ADD KEY `idx_Client_Client_iBranchID` (`Client_iBranchID`),
  ADD KEY `idx_Client_Client_iChangeSetID` (`Client_iChangeSetID`),
  ADD KEY `idx_Client_cPassportNumber` (`cPassportNumber`),
  ADD KEY `idx_CLIENT_iAreasID` (`iAreasID`),
  ADD KEY `idx_CLIENT_iClassID` (`iClassID`),
  ADD KEY `idxAccount` (`Account`),
  ADD KEY `idxName` (`Name`);

ALTER TABLE `InvNum`
  ADD PRIMARY KEY (`AutoIndex`),
  ADD KEY `idx_InvNum_InvNum_Checksum` (`InvNum_Checksum`),
  ADD KEY `idx_InvNum_InvNum_dModifiedDate` (`InvNum_dModifiedDate`),
  ADD KEY `idx_InvNum_InvNum_iBranchID` (`InvNum_iBranchID`),
  ADD KEY `idx_InvNum_InvNum_iChangeSetID` (`InvNum_iChangeSetID`),
  ADD KEY `idx_INVNUM_iOpportunityID` (`iOpportunityID`),
  ADD KEY `idxAccountID_DocType` (`AccountID`,`DocType`),
  ADD KEY `idxDocType` (`DocType`,`DocState`,`InvNumber`);

ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `open_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `open_sessions_user_id_index` (`user_id`),
  ADD KEY `open_sessions_warehouse_id_index` (`warehouse_id`);

ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `petty_cashes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `petty_cashes_user_id_index` (`user_id`),
  ADD KEY `petty_cashes_petty_cash_type_id_index` (`petty_cash_type_id`);

ALTER TABLE `petty_cash_types`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

ALTER TABLE `StkItem`
  ADD PRIMARY KEY (`StockLink`),
  ADD KEY `idx_StkItem_Code` (`Code`(255)),
  ADD KEY `idx_StkItem_StkItem_Checksum` (`StkItem_Checksum`),
  ADD KEY `idx_StkItem_StkItem_dModifiedDate` (`StkItem_dModifiedDate`),
  ADD KEY `idx_StkItem_StkItem_iBranchID` (`StkItem_iBranchID`),
  ADD KEY `idx_StkItem_StkItem_iChangeSetID` (`StkItem_iChangeSetID`),
  ADD KEY `idxBarCode` (`Bar_Code`(255)),
  ADD KEY `idxDescription1` (`Description_1`),
  ADD KEY `idxGroup` (`ItemGroup`);

ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_index` (`user_id`),
  ADD KEY `transactions_warehouse_id_index` (`warehouse_id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

ALTER TABLE `Vendor`
  ADD PRIMARY KEY (`DCLink`),
  ADD KEY `idx_Vendor_cIDNumber` (`cIDNumber`),
  ADD KEY `idx_Vendor_cPassportNumber` (`cPassportNumber`),
  ADD KEY `idx_VENDOR_iAreasID` (`iAreasID`),
  ADD KEY `idx_VENDOR_iClassID` (`iClassID`),
  ADD KEY `idx_Vendor_Vendor_Checksum` (`Vendor_Checksum`),
  ADD KEY `idx_Vendor_Vendor_dModifiedDate` (`Vendor_dModifiedDate`),
  ADD KEY `idx_Vendor_Vendor_iBranchID` (`Vendor_iBranchID`),
  ADD KEY `idx_Vendor_Vendor_iChangeSetID` (`Vendor_iChangeSetID`),
  ADD KEY `idxAccount` (`Account`),
  ADD KEY `idxName` (`Name`);

ALTER TABLE `WhseMst`
  ADD PRIMARY KEY (`WhseLink`),
  ADD KEY `idx_WhseMst_WhseMst_Checksum` (`WhseMst_Checksum`),
  ADD KEY `idx_WhseMst_WhseMst_dModifiedDate` (`WhseMst_dModifiedDate`),
  ADD KEY `idx_WhseMst_WhseMst_iBranchID` (`WhseMst_iBranchID`),
  ADD KEY `idx_WhseMst_WhseMst_iChangeSetID` (`WhseMst_iChangeSetID`);

ALTER TABLE `WhseStk`
  ADD PRIMARY KEY (`IdWhseStk`),
  ADD KEY `idx_WhseStk_WhseStk_Checksum` (`WhseStk_Checksum`),
  ADD KEY `idx_WhseStk_WhseStk_dModifiedDate` (`WhseStk_dModifiedDate`),
  ADD KEY `idx_WhseStk_WhseStk_iBranchID` (`WhseStk_iBranchID`),
  ADD KEY `idx_WhseStk_WhseStk_iChangeSetID` (`WhseStk_iChangeSetID`),
  ADD KEY `idxWhseStk_StockLink` (`WHStockLink`),
  ADD KEY `idxWhseStk_WhseID` (`WHWhseID`),
  ADD KEY `idxWhseStock` (`WHWhseID`,`WHStockLink`);

ALTER TABLE `_btblCbBatches`
  ADD PRIMARY KEY (`idBatches`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_Checksum` (`_btblCbBatches_Checksum`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_dModifiedDate` (`_btblCbBatches_dModifiedDate`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_iBranchID` (`_btblCbBatches_iBranchID`),
  ADD KEY `idx__btblCbBatches__btblCbBatches_iChangeSetID` (`_btblCbBatches_iChangeSetID`);

ALTER TABLE `_btblCbBatchLines`
  ADD PRIMARY KEY (`idBatchLines`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_Checksum` (`_btblCbBatchLines_Checksum`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_dModifiedDate` (`_btblCbBatchLines_dModifiedDate`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_iBranchID` (`_btblCbBatchLines_iBranchID`),
  ADD KEY `idx__btblCbBatchLines__btblCbBatchLines_iChangeSetID` (`_btblCbBatchLines_iChangeSetID`);

ALTER TABLE `_btblInvoiceLines`
  ADD PRIMARY KEY (`idInvoiceLines`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_Checksum` (`_btblInvoiceLines_Checksum`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_dModifiedDate` (`_btblInvoiceLines_dModifiedDate`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_iBranchID` (`_btblInvoiceLines_iBranchID`),
  ADD KEY `idx__btblInvoiceLines__btblInvoiceLines_iChangeSetID` (`_btblInvoiceLines_iChangeSetID`),
  ADD KEY `idx__btblInvoiceLines_iSOLinkedPOLineID` (`iSOLinkedPOLineID`),
  ADD KEY `idxInvoiceID` (`iInvoiceID`);

ALTER TABLE `_etblPriceListName`
  ADD PRIMARY KEY (`IDPriceListName`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_Checksum` (`_etblPriceListName_Checksum`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_dModifiedDate` (`_etblPriceListName_dModifiedDate`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_iBranchID` (`_etblPriceListName_iBranchID`),
  ADD KEY `idx__etblPriceListName__etblPriceListName_iChangeSetID` (`_etblPriceListName_iChangeSetID`);

ALTER TABLE `_etblPriceListPrices`
  ADD PRIMARY KEY (`IDPriceListPrices`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_Checksum` (`_etblPriceListPrices_Checksum`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_dModifiedDate` (`_etblPriceListPrices_dModifiedDate`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_iBranchID` (`_etblPriceListPrices_iBranchID`),
  ADD KEY `idx__etblPriceListPrices__etblPriceListPrices_iChangeSetID` (`_etblPriceListPrices_iChangeSetID`),
  ADD KEY `idx__etblPriceListPrices_iStockIDbUseMarkup` (`iStockID`,`bUseMarkup`),
  ADD KEY `idx_etblPriceListPrices_PriceListStockID` (`iPriceListNameID`,`iStockID`,`iWarehouseID`);

ALTER TABLE `_rtblAgents`
  ADD PRIMARY KEY (`idAgents`),
  ADD KEY `idx__rtblAgents__rtblAgents_Checksum` (`_rtblAgents_Checksum`),
  ADD KEY `idx__rtblAgents__rtblAgents_dModifiedDate` (`_rtblAgents_dModifiedDate`),
  ADD KEY `idx__rtblAgents__rtblAgents_iBranchID` (`_rtblAgents_iBranchID`),
  ADD KEY `idx__rtblAgents__rtblAgents_iChangeSetID` (`_rtblAgents_iChangeSetID`),
  ADD KEY `idxUserName` (`cAgentName`);


ALTER TABLE `assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Client`
  MODIFY `DCLink` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `InvNum`
  MODIFY `AutoIndex` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
ALTER TABLE `open_sessions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
ALTER TABLE `petty_cashes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `petty_cash_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `StkItem`
  MODIFY `StockLink` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `Vendor`
  MODIFY `DCLink` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `WhseMst`
  MODIFY `WhseLink` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `WhseStk`
  MODIFY `IdWhseStk` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `_btblCbBatches`
  MODIFY `idBatches` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `_btblCbBatchLines`
  MODIFY `idBatchLines` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `_btblInvoiceLines`
  MODIFY `idInvoiceLines` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `_etblPriceListName`
  MODIFY `IDPriceListName` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `_etblPriceListPrices`
  MODIFY `IDPriceListPrices` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `_rtblAgents`
  MODIFY `idAgents` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

ALTER TABLE `_etblUnits`
  ADD PRIMARY KEY (`idUnits`),
  ADD KEY `idx__etblUnits__etblUnits_Checksum` (`_etblUnits_Checksum`),
  ADD KEY `idx__etblUnits__etblUnits_dModifiedDate` (`_etblUnits_dModifiedDate`),
  ADD KEY `idx__etblUnits__etblUnits_iBranchID` (`_etblUnits_iBranchID`),
  ADD KEY `idx__etblUnits__etblUnits_iChangeSetID` (`_etblUnits_iChangeSetID`);

ALTER TABLE `_etblUnits`
  MODIFY `idUnits` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `_etblUnitConversion`
  ADD PRIMARY KEY (`idUnitConversion`),
  ADD KEY `_etblUnitConversion_idxAB` (`iUnitAID`,`iUnitBID`),
  ADD KEY `_etblUnitConversion_idxBA` (`iUnitBID`,`iUnitAID`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_Checksum` (`_etblUnitConversion_Checksum`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_dModifiedDate` (`_etblUnitConversion_dModifiedDate`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_iBranchID` (`_etblUnitConversion_iBranchID`),
  ADD KEY `idx__etblUnitConversion__etblUnitConversion_iChangeSetID` (`_etblUnitConversion_iChangeSetID`);

ALTER TABLE `_etblUnitConversion`
  MODIFY `idUnitConversion` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `TaxRate`
  ADD PRIMARY KEY (`idTaxRate`),
  ADD KEY `idx_TaxRate_TaxRate_Checksum` (`TaxRate_Checksum`),
  ADD KEY `idx_TaxRate_TaxRate_dModifiedDate` (`TaxRate_dModifiedDate`),
  ADD KEY `idx_TaxRate_TaxRate_iBranchID` (`TaxRate_iBranchID`),
  ADD KEY `idx_TaxRate_TaxRate_iChangeSetID` (`TaxRate_iChangeSetID`);

ALTER TABLE `TaxRate`
  MODIFY `idTaxRate` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `syncs`
  ADD PRIMARY KEY(`id`);