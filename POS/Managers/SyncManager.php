<?php

namespace POS\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use GuzzleHttp\Client;
use PDO;
use POS\Models\Sync;

class SyncManager
{
    public static function getCurrentFilename()
    {
        return self::getFilenameFromDate(Carbon::now());
    }

    private static function getFilenameFromDate(Carbon $date)
    {
        return 'statements/unprocessed-' . $date->format('Ymd') . '.log';
    }

    private static function getSiteFile($siteId)
    {
        return 'statements/sync/sync-' . $siteId . '.log';
    }

    private static function getLoggerNameFromDate(Carbon $date)
    {
        return 'statements/index-' . $date->format('Ymd') . '.log';
    }

    public static function getCurrentLoggerName()
    {
        return self::getLoggerNameFromDate(Carbon::now());
    }

    public static function getCurrentProcessedName()
    {
        return self::getProcessedNameFromDate(Carbon::now());
    }

    private static function getProcessedNameFromDate(Carbon $date)
    {
        return 'statements/processed-index-' . $date->format('Ymd') . '.log';
    }

    public static function getLogPointFromDate(Carbon $date)
    {
        if (! is_file(storage_path(self::getLoggerNameFromDate($date)))) {
            return 0;
        }

        return file_get_contents(storage_path(self::getLoggerNameFromDate($date)));
    }

    public static function getLogPoint()
    {
        return self::getLogPointFromDate(Carbon::now());
    }

    public static function getProcessedLogPoint()
    {
        return self::getProcessedLogPointFromDate(Carbon::now());
    }

    public static function getProcessedLogPointFromDate(Carbon $date)
    {
        if (! is_file(storage_path(self::getProcessedNameFromDate($date)))) {
            return 0;
        }

        return file_get_contents(storage_path(self::getProcessedNameFromDate($date)));
    }


    public static function getUnprocessed($date = null)
    {
        if (! $date) {
            return self::getUnprocessedByDate(Carbon::now());
        }

        return self::getUnprocessedByDate($date);
    }

    private static function getUnprocessedByDate(Carbon $date)
    {
        $filename = self::getFilenameFromDate($date);

        if (! is_file(storage_path($filename))) {
            return '';
        }

        return file_get_contents(storage_path($filename));
    }

    public static function getUnprocessedFromIndex($date = null, $index = 0)
    {
        $queries = explode(';', self::getUnprocessed($date));

        if (count($queries) < 1) {
            return '';
        }

        return implode(';', array_slice($queries, $index));
    }



    public static function pushCurrent()
    {
        self::pushForDate(Carbon::now());
    }

    private static function pushForDate(Carbon $date)
    {
        $unprocessedPoint = self::getProcessedLogPointFromDate($date);
        $currentPoint = self::getLogPointFromDate($date);

        if ($currentPoint == $unprocessedPoint || $currentPoint == 0) {
            echo 'Nothing to process.';

            self::synchronizeBackup();
            echo 'Synchronized.';
            return true;
        }

        $queries = self::getUnprocessedFromIndex($date, $unprocessedPoint);
        $guzzle = new Client();
        try {
            $request = $guzzle->request('POST', config('pos.main_server_ip') . '/api/v1/receive', [
                'form_params' => [
                    'last_date' => $date->toDateString(),
                    'queries' => $queries,
                    'site_id' => config('pos.site_id', 'TR')
                ]
            ]);
            $response = json_decode($request->getBody()->getContents());

            if ($response->status) {
                self::updateProcessed($date, $currentPoint);

                echo 'Successfully pushed changes';
            }

            self::synchronizeBackup();
            echo 'Synchronized.';
        } catch (Exception $ex) {
            echo 'Fatal Error: ' . $ex->getMessage();
        }
    }

    private static function updateProcessed(Carbon $date, $index)
    {
        file_put_contents(storage_path(self::getProcessedNameFromDate($date)), $index);
    }




    public static function getTables()
    {
        return [
            'Client', 'InvNum', 'open_sessions', 'permissions', 'petty_cashes', 'petty_cash_types', 'roles',
            'StkItem', 'transactions', 'users', 'Vendor', 'WhseMst', 'WhseStk', '_btblCbBatches', '_btblCbBatchLines',
            '_btblInvoiceLines', '_etblPriceListName', '_etblPriceListPrices', '_rtblAgents', '_etblUnits',
            '_etblUnitConversion', 'TaxRate', 'settings'
        ];
    }

    public static function updateClients()
    {
        $clients = DB::connection(env('DB_CONNECTION'))->table('Client')->get()->map(function ($client) {
            return (array) $client;
        })->chunk(20);

        foreach ($clients as $client) {
            DB::connection('backup')->table('Client')->insert($client->toArray());
        }
    }


    private static function updateTable($tableName)
    {
        DB::connection(env('DB_CONNECTION'))
            ->table($tableName)
            ->chunk(20, function ($rows) use ($tableName) {
                if (! count($rows)) {
                    return;
                }

                $newRecords = self::prepareFields($rows);
                $query = self::generateQuery($tableName, $newRecords);

                DB::connection('backup')->statement($query);
            });
    }

    /**
     * @param $rows
     *
     * @return mixed
     */
    private static function prepareFields($rows)
    {
        $newRecords = $rows->map(function ($record) {
            return collect((array) $record)->reject(function ($value, $key) {
                    return $key == 'row_num';
            })
                ->map(function ($value) {
                    if (is_numeric($value)) {
                        return $value;
                    }

                    if (is_null($value)) {
                        return 'NULL';
                    }

                    return strpos($value, '"') ? "'$value'" : '"' . $value .'"';
                });
        })->toArray();

        return $newRecords;
    }

    /**
     * @param $tableName
     * @param $newRecords
     *
     * @return string
     */
    private static function generateQuery($tableName, $newRecords)
    {
        $query = 'INSERT INTO ' . $tableName .
            ' (`' . implode('`, `', array_keys($newRecords[0])) . '`) VALUES ';
        $values = [];
        $duplicate = 'ON DUPLICATE KEY UPDATE ' . implode(', ', array_map(
            function ($value, $key) {
                return sprintf("`%s`=values(`%s`)", $key, $key);
            },
            $newRecords[0],
            array_keys($newRecords[0])
        ));

        foreach ($newRecords as $row) {
            $values[] = "(" . implode(", ", $row) . ")";
        }

        $query .= implode(', ', $values) . ' ' . $duplicate;

        return $query;
    }









    public static function saveQuery($query, $bindings)
    {
        $now = Carbon::now();
        DB::connection('backup')->table('syncs')->insert([
            'id' => microtime(true),
            'sql' => $query,
            'bindings' => serialize($bindings),
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }

    public static function pushUnprocessed()
    {
        $current = DB::connection('backup')->table('syncs')->get();
        if (self::pushRows($current)) {
            self::synchronizeBackup();
        }
        echo 'Completed.';

        return true;
    }

    private static function pushRows($rows)
    {
        $isSuccess = true;

        foreach ($rows as $row) {
            if (! self::sendRequest($row)) {
                $isSuccess = false;
                break;
            }
        }

        return $isSuccess;
    }

    public static function sendRequest($row)
    {
        $guzzle = new Client();

        try {
            $request = $guzzle->request('POST', config('pos.main_server_ip') . '/api/v1/receive', [
                'form_params' => [
                    'queries' => json_encode($row)
                ]
            ]);
            $response = json_decode($request->getBody()->getContents());

            if ($response->status == 1) {
                DB::connection('backup')->table('syncs')->where('id', $row->id)->delete();
                return true;
            }

            return false;
        } catch (Exception $ex) {
            echo 'Fatal Error: ' . $ex->getMessage();

            return false;
        }
    }

    public static function synchronizeBackup()
    {
        self::cleanupBackup();
        self::updateBackup();
    }

    public static function cleanupBackup()
    {
        try {
            fsockopen(env('DB_HOST'), env('DB_PORT'));
            $canConnect = true;
        } catch (\Exception $exception) {
            $canConnect = false;
        }

        if (! $canConnect) {
            return false;
        }

        $connection = DB::connection('backup')->getPdo();
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
        $connection->exec(preg_replace('/\n/', ' ', file_get_contents(database_path('backup.sql'))));

        DB::connection('backup')->statement("UPDATE `roles` SET `id` = ? WHERE `name` = ?;", [0, 'Superuser']);

        return true;
    }

    public static function updateBackup()
    {
        foreach (self::getTables() as $table) {
            self::updateTable($table);
            gc_collect_cycles();
        }

        return 'Done';
    }

    public static function processNew($imports)
    {
        try {
            $query = $imports->sql;
            $pattern = '/`([A-Za-z0-9_]+)`/i';
            $replacement = '[$1]';
            $query = preg_replace($pattern, $replacement, $query);

            DB::connection(env('DB_CONNECTION'))->statement($query, unserialize($imports->bindings));

            return true;
        } catch (Exception $ex) {
            print_r($ex->getMessage());
            return false;
        }
    }
}
