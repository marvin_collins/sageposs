<?php

use POS\Models\Permission;
use POS\Repositories\CacheRepository;
use POS\Repositories\SessionRepository;

function getCheckedIn()
{
    if (! $warehouse = SessionRepository::getWarehouseID()) {
        return 'Check In.';
    }

    if (! $assignments = getAssignments()) {
        return 'None';
    }

    return $assignments->where('warehouse_id', $warehouse)->first();
}

function getAssignments()
{
    return CacheRepository::getMyAssignments();
}

function getApproverPermissionId()
{
    return CacheRepository::getPermissions()
        ->where('name', Permission::APPROVE_TRANSACTION['name'])
        ->where('group', Permission::APPROVE_TRANSACTION['group'])
        ->first()
        ->id;
}

function getReprintPermissionId()
{
    return CacheRepository::getPermissions()
        ->where('name', Permission::REPRINT_TRANSACTION['name'])
        ->where('group', Permission::REPRINT_TRANSACTION['group'])
        ->first()
        ->id;
}

function formatQuery($query, $bindings)
{
    preg_match_all('/\?/', $query, $matches, PREG_OFFSET_CAPTURE);
    $formatted = $query;

    for($i = 0; $i < count($matches[0]); $i++) {
        $formatted = preg_replace('/\?/', "'" . $bindings[$i] . "'", $formatted, 1);
    }

    return $formatted;
}