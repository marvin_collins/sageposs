<?php

namespace POS\Repositories;

use Cache;
use DB;

class ClientRepository
{
    public static function all($pagination = 10)
    {
        return DB::table('Client')
            ->select(self::getColumns())
            ->paginate($pagination);
    }

    private static function getColumns()
    {
        return [
            'DCLink', 'Account', 'Name', 'Telephone', 'EMail', 'Physical1',
            'DCBalance', 'Credit_Limit', 'CashDebtor'
        ];
    }

    public static function find($searchText)
    {
        return DB::table('Client')
            ->where('Account', 'LIKE', '%' . $searchText . '%')
            ->orWhere('Name', 'LIKE', '%' . $searchText . '%')
            ->get(self::getColumns());
    }

    public static function getAll()
    {
        return DB::table('Client')
            ->where('On_Hold', 0)
            ->select(self::getColumns())
            ->get();
    }

    public static function findOne($id)
    {
        return DB::table('Client')
            ->where('DCLink', $id)
            ->first(self::getColumns());
    }

}
