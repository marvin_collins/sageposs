<?php

namespace POS\Repositories;

use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Collection;

class UserRepository
{
    public static function create($attributes = [])
    {
        $attributes['password'] = bcrypt($attributes['password']);
        $attributes['change_password'] = true;

        return User::create($attributes);
    }

    public static function update(User $user, array $attributes = [], array $options = [])
    {
        $attributes['permissions'] = json_encode(
            collect($attributes['permissions'])
                ->reject(function ($value) {
                    return $value == false;
                })
                ->keys()
        );

        if ($attributes['password'] != "") {
            $attributes['password'] = bcrypt($attributes['password']);

            return $user->update($attributes, $options);
        }
        unset($attributes['password']);

        return $user->update($attributes, $options);
    }

    public static function import()
    {
        $users = static::getNewUsers(static::getSageUsers());
        $users = static::mapNewUsers($users);

        User::insert($users->toArray());

        return $users->count();
    }

    private static function getSageUsers()
    {
        $users = collect(DB::table('_rtblAgents')
            ->where('bSysAccount', '=', 0)
            ->where('cAgentName', '<>', 'Guest')
            ->get(['idAgents', 'cAgentName', 'cEmail']))->unique('cEmail');

        return $users->map(function ($value) {
            return [
                'id' => $value->idAgents,
                'name' => $value->cAgentName,
                'email' =>$value->cEmail
            ];
        });
    }

    private static function getNewUsers(Collection $sageUsers)
    {
        $users = User::all(['name'])->map(function ($value) {
            return strtolower($value->name);
        })->toArray();

        return $sageUsers->reject(function ($value) use ($users) {
            return in_array(strtolower($value['name']), $users);
        });
    }

    private static function mapNewUsers(Collection $users)
    {
        $now = Carbon::now();

        return $users->map(function ($value) use ($now) {
            return [
                'agent_id' => $value['id'],
                'name' => $value['name'],
                'email' => $value['email'],
                'password' => bcrypt('Qwerty123!'),
                'role_id' => 1,
                'permissions' => json_encode([]),
                'created_at' => $now,
                'updated_at' => $now
            ];
        });
    }
}
