<?php

namespace POS\Repositories;

use DB;
use Illuminate\Support\Collection;

class StockRepository
{

    private static function getPriceLists()
    {
        return \Cache::remember('PRICES', 3, function () {
            return DB::table('_etblPriceListPrices')
                ->get([
                    'iPriceListNameID', 'iStockID', 'iWarehouseID', 'fExclPrice', 'fInclPrice'
                ])
                ->where('iWarehouseID', '>', 0)
                ->groupBy('iStockID');
        });
    }

    public static function mapPriceLists($items)
    {
        $priceLists = self::getPriceLists();
        $items = $items->map(function ($item) use ($priceLists) {
            $prices = $priceLists->get($item->StockLink);
            if (! $prices) {
                return $item;
            }

            $prices = $prices->where('iWarehouseID', $item->WHWhseID);

            if (! count($prices)) {
                return $item;
            }

            foreach ($prices as $price) {
                if ($price->iPriceListNameID == config('pos.rr_stock_price_list', 1)) {
                    $item->RRPStockingInclPrice = $price->fInclPrice;
                    continue;
                }
                if ($price->iPriceListNameID == config('pos.rr_out_price_list', 1)) {
                    $item->RRPOutInclPrice = $price->fInclPrice;
                    continue;
                }
                if ($price->iPriceListNameID == config('pos.alt_stock_price_list', 1)) {
                    $item->AltStockingInclPrice = $price->fInclPrice;
                    continue;
                }
                if ($price->iPriceListNameID == config('pos.alt_out_price_list', 1)) {
                    $item->AltOutInclPrice = $price->fInclPrice;
                    continue;
                }
            }

            return $item;
        });

        return $items;
    }

//    TODO: Find the weight of items.
    private static function buildCommonQuery()
    {
        return DB::table('WhseStk')
            ->select(DB::raw(implode(', ', self::getColumns())))
            ->join('WhseMst', 'WhseMst.WhseLink', '=', 'WhseStk.WHWhseID')
            ->join('StkItem', 'WhseStk.WHStockLink', '=', 'StkItem.StockLink')
            ->join('_etblUnits', 'StkItem.iUOMStockingUnitID', '=', '_etblUnits.idUnits', 'left')
            ->join('TaxRate', 'StkItem.TTI', '=', 'TaxRate.Code')
            ->join('_etblPriceListPrices as RecStockingPrice', 'RecStockingPrice.iStockID', '=', 'StkItem.StockLink', 'left')
            ->join('_etblPriceListPrices as RecOutPrice', 'RecOutPrice.iStockID', '=', 'StkItem.StockLink', 'left')
            ->join('_etblPriceListPrices as AltStockingPrice', 'AltStockingPrice.iStockID', '=', 'StkItem.StockLink', 'left')
            ->join('_etblPriceListPrices as AltOutPrice', 'AltOutPrice.iStockID', '=', 'StkItem.StockLink', 'left')
            ->where('RecStockingPrice.iPriceListNameID', '=', config('pos.rr_stock_price_list', 1))
            ->where('RecOutPrice.iPriceListNameID', '=', config('pos.rr_out_price_list', 1))
            ->when(config('pos.has_alternate_price', false), function ($query) {
                return $query->where('AltStockingPrice.iPriceListNameID', '=', config('pos.alt_stock_price_list', 1))
                    ->where('AltOutPrice.iPriceListNameID', '=', config('pos.alt_out_price_list', 1));
            })
            ->where('RecStockingPrice.iWarehouseID', 0)
            ->where('RecOutPrice.iWarehouseID', 0)
            ->where('AltStockingPrice.iWarehouseID', 0)
            ->where('AltOutPrice.iWarehouseID', 0)
            ->where('RecStockingPrice.fInclPrice', '>=', 0);
    }

    private static function getColumns()
    {
        return [
            'WhseStk.idWhseStk', 'WhseStk.WHWhseID', 'StkItem.Code', 'cUnitDescription', 'StkItem.iUOMStockingUnitID',
            '(WhseStk.WHQtyOnHand - WhseStk.WHQtyReserved) as WHQtyOnHand',
            'StkItem.StockLink', 'StkItem.Description_1', 'StkItem.AveUCst', 'StkItem.ItemGroup',
            'WhseMst.Name', 'TaxRate.idTaxRate as TaxCode', 'TaxRate.Description', 'TaxRate.TaxRate',
            'ROUND(RecStockingPrice.fInclPrice, 0) as RRPStockingInclPrice',
            'ROUND(RecOutPrice.fInclPrice, 0) as RRPOutInclPrice',
            'ROUND(AltStockingPrice.fInclPrice, 0) as AltStockingInclPrice',
            'ROUND(AltOutPrice.fInclPrice, 0) as AltOutInclPrice',
        ];
    }

    public static function all()
    {
        $items = self::buildCommonQuery()
            ->whereIn('WhseStk.WHWhseID', CacheRepository::getMyAssignedIds())
            ->whereRaw('(WhseStk.WHQtyOnHand - WhseStk.WHQtyReserved) > 0')
            ->orderBy('StkItem.Code')
            ->get();

        return self::mapPriceLists($items);
    }

    public static function getAll()
    {
        dd(self::getItemsWithRates());
        return self::buildCommonQuery()
            ->where('WhseStk.WHWhseID', SessionRepository::getWarehouseID())
            ->whereRaw('(WhseStk.WHQtyOnHand - WhseStk.WHQtyReserved) > 0')
            ->orderBy('StkItem.Code')
            ->get();
    }

    private static function groupItems(Collection $collection)
    {
        return $collection->map(function ($row) {
            return (array) $row;
        })
            ->groupBy('idWhseStk')
            ->map(function ($group) {
                $item = $group[0];
                unset($item['fUnitBQty']);
                unset($item['fUnitAQty']);
                unset($item['ConvertedUnit']);
                $item['conversions'] = [];

                foreach ($group as $conversionRate) {
                    $item['conversions'] [] = [
                        "fUnitAQty" => $conversionRate['fUnitAQty'],
                        "ConvertedUnit" => $conversionRate['ConvertedUnit'],
                        "fUnitBQty" => $conversionRate['fUnitBQty'],
                    ];
                }

                return $item;
            });
    }

    public static function forDataTable()
    {
        return [
            'data' => self::all()->map(function ($item) {
                return [
                    $item->Code,
                    $item->Description_1,
                    $item->Name,
                    number_format($item->WHQtyOnHand, 2) . ' ' . $item->cUnitDescription,
//                    number_format(doubleval($item->ucIIUnitWt), 2) . 'KGs',
                    number_format($item->RRPStockingInclPrice, 2),
                    number_format($item->RRPOutInclPrice, 2),
                ];
            })
        ];
    }

    public static function find($searchKey)
    {
        return self::buildCommonQuery()
            ->where('WhseStk.WHWhseID', '=', SessionRepository::getWarehouseID())
            ->whereRaw('WhseStk.WHQtyOnHand - WhseStk.WHQtyReserved > 0')
            ->where(function ($query) use ($searchKey) {
                $query->where('StkItem.Code', 'LIKE', '%' . $searchKey . '%')
                    ->orWhere('StkItem.Description_1', 'LIKE', '%' . $searchKey . '%')
                    ->orWhere('StkItem.Bar_Code', 'LIKE', '%' . $searchKey . '%');
            })
            ->paginate(50);
    }

    public static function findAll($searchKeys)
    {
        return self::buildCommonQuery()
            ->where('WhseStk.WHWhseID', '=', SessionRepository::getWarehouseID())
            ->where(function ($query) use ($searchKeys) {
                $query->whereIn('StkItem.StockLink', $searchKeys);
            })
            ->get();
    }

    public static function search($searchKey)
    {
        return self::buildCommonQuery()
            ->whereIn('WhseStk.WHWhseID', CacheRepository::getMyAssignedIds())
            ->where(function ($query) use ($searchKey) {
                $query->where('StkItem.Code', 'LIKE', '%' . $searchKey . '%')
                    ->orWhere('StkItem.Description_1', 'LIKE', '%' . $searchKey . '%');
            })
            ->paginate(10);
    }

    public static function getConversions()
    {
        return DB::table('_etblUnits')->select([
            '_etblUnits.cUnitDescription as FirstUnit', 'Converted.cUnitDescription as SecondUnit',
            '_etblUnits.idUnits as FirstUnitId', 'Converted.idUnits as SecondUnitId',
            '_etblUnitConversion.fUnitAQty as FirstQuantity', '_etblUnitConversion.fUnitBQty as SecondQuantity',
            '_etblUnitConversion.fMarkup as UnitPrice',
        ])
            ->join('_etblUnitConversion', '_etblUnits.idUnits', '=', '_etblUnitConversion.iUnitAID', 'left')
            ->join('_etblUnits as Converted', 'Converted.idUnits', '=', '_etblUnitConversion.iUnitBID')
            ->get();
    }

    public static function mapConversions()
    {
        return self::getConversions()->map(function ($value) {
            return (array) $value;
        });
    }

    public static function getItemsWithRates()
    {
        $rates = self::mapConversions();

        return self::getWarehouseStock()->map(function ($item) use ($rates) {
            $item->conversions = [];
            $item->conversions[] = [
                'id' => $item->iUOMStockingUnitID,
                'name' => $item->cUnitDescription,
                'ratio' => 1,
                'price' => $item->RRPStockingInclPrice
            ];
            $item->measure = [
                'id' => $item->iUOMStockingUnitID,
                'name' => $item->cUnitDescription,
                'ratio' => 1,
                'price' => $item->RRPStockingInclPrice
            ];

            $item->conversions = array_merge(
                $item->conversions,
                $rates->where('FirstUnitId', $item->iUOMStockingUnitID)
                ->map(function ($value) use ($item) {
                    return [
                        'id' => $value['SecondUnitId'],
                        'name' => $value['SecondUnit'],
                        'ratio' => $value['FirstQuantity'] == 1 ? -$value['SecondQuantity'] : $value['FirstQuantity'],
                        'price' => $item->RRPStockingInclPrice < 0 ? -$item->RRPStockingInclPrice : $item->RRPStockingInclPrice
                    ];
                })->toArray()
            );

            $item->conversions = array_merge(
                $item->conversions,
                $rates->where('SecondUnitId', $item->iUOMStockingUnitID)
                    ->map(function ($value) {
                        return [
                            'id' => $value['FirstUnitId'],
                            'name' => $value['FirstUnit'],
                            'ratio' => $value['SecondQuantity'] == 1 ? -$value['FirstQuantity'] : $value['SecondQuantity'],
                            'price' => $value['UnitPrice'] < 0 ? -$value['UnitPrice'] : $value['UnitPrice']
                        ];
                    })->toArray()
            );

            $item->multiplier = 1;
            $item->StockQuantity = $item->WHQtyOnHand;

            return $item;
        });
    }

    public static function getWarehouseStock()
    {
        $items = self::buildCommonQuery()
            ->where('WhseStk.WHWhseID', SessionRepository::getWarehouseID())
            ->whereRaw('(WhseStk.WHQtyOnHand - WhseStk.WHQtyReserved) > 0')
            ->orderBy('StkItem.Code')
            ->get();

        return self::mapPriceLists($items);
    }
}
