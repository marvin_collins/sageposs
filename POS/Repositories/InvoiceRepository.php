<?php

namespace POS\Repositories;

use Auth;
use Carbon\Carbon;
use DB;
use POS\Models\Transactions;
use function str_replace;

class InvoiceRepository
{
    private static function makeTransaction()
    {
        $transaction = Transactions::create([
            'user_id' => Auth::id(),
            'warehouse_id' => SessionRepository::getWarehouseID(),
            'invoice_id' => 0,
            'cashbook_id' => 0,
            'transaction_type' => '',
            'amount' => 0,
            'payment_type' => '',
            'transaction_data' => '',
            'status' => ''
        ]);

        $transaction->transaction_number = '#' . config('pos.site_id', 'TR') . '-' . $transaction->id;

        return $transaction;
    }

    public static function processPayment($paymentData)
    {
        return DB::transaction(function () use ($paymentData) {
            $transaction = self::makeTransaction();
            $reference = $transaction->transaction_number;

            $paymentData['client'] = ClientRepository::findOne($paymentData['client_id']);

            $cashBook = config('pos.cash_receipt_cashbook_batch', 1);

            if (strtoupper($paymentData['type']) != 'CASH') {
                $cashBook = config('pos.mpesa_cashbook_batch', 1);
            }

//            $cashBook = $paymentData['type'] == 'Cash' ? config('pos.cash_cashbook_batch', 1) : config('pos.mpesa_cashbook_batch', 1);
            $cashbookId = DB::table('_btblCbBatchLines')
                ->insertGetId(
                    self::mapCashBook(
                        $cashBook,
                        [
                            'customer' => [
                                'DCLink' => $paymentData['client_id']
                            ]
                        ],
                        $paymentData['amount'],
                        $paymentData['type'] . ' Receipt: ' . $paymentData['reference'],
                        $reference
                    )
                );

            $transaction->fill([
                'cashbook_id' => $cashbookId,
                'transaction_type' => Transactions::CASH_RECEIPT,
                'amount' => $paymentData['amount'],
                'payment_type' => $paymentData['type'] . ': ' . $paymentData['reference'],
                'transaction_data' => json_encode($paymentData),
                'status' => Transactions::STATUS_COMPLETED
            ]);

            $transaction->save();

            return $transaction;
        });
    }

    public static function makeInvoice($saleData, $saved = null)
    {
        if ($saved) {
            $transaction = Transactions::findOrFail($saved);
            $transaction->transaction_number = '#' . config('pos.site_id', 'TR') . '-' . $transaction->id;
        } else {
            $transaction = self::makeTransaction();
        }

        return DB::transaction(function () use ($saleData, $transaction) {
            $reference = $transaction->transaction_number;

            $invoiceId = DB::table('InvNum')->insertGetId(self::mapInvoiceFields($saleData, $reference));

            foreach (self::syncInvoiceLines($saleData['items'], $invoiceId) as $line) {
                DB::table('_btblInvoiceLines')->insert($line);
            }
            $transaction = self::makeCashbook($saleData, $invoiceId, $reference, $transaction, false);
            foreach (self::getUpdateQueries($saleData) as $query) {
                DB::statement($query);
            }

            return $transaction;
        }, 2);
    }

    private static function getUpdateQueries($saleData)
    {
        $queries = [];
        $queries[] = self::getUpdateStockQuery($saleData);
        $queries[] = self::getUpdateStockItemQuery($saleData);

        return $queries;
    }

    public static function makeCreditNote($saleData, $transactionId)
    {
        $transaction = Transactions::findOrFail($transactionId);
        $transaction->transaction_number = '#' . config('pos.site_id', 'TR') . '-' . $transaction->id;
//        $transaction->reversed = true;
        $invoiceId = Transactions::findOrFail($transaction->original_transaction_id)->invoice_id;
        $reference = DB::table('InvNum')->where('AutoIndex', $invoiceId)
            ->first();

        $reference = $reference ? $reference->OrderNum : 'CRN';

        DB::transaction(function () use ($saleData, $reference, $transaction) {
            $invoiceId = DB::table('InvNum')->insertGetId(self::mapInvoiceFields($saleData, $reference, 1));
            foreach (self::syncInvoiceLines($saleData['items'], $invoiceId) as $line) {
                DB::table('_btblInvoiceLines')->insert($line);
            }

            $transaction = self::makeCashbook($saleData, $invoiceId, $reference, $transaction, true);

//            $transaction->fill([
//                'user_id' => Auth::id(),
//                'invoice_id' => $invoiceId,
//                'cashbook_id' => null,
//                'transaction_type' => Transactions::REVERSAL,
//                'amount' => $saleData['totalPrice'],
//                'payment_type' => 'Credit Note',
//                'transaction_data' => json_encode($saleData),
//                'status' => Transactions::STATUS_COMPLETED
//            ]);
//
//            $transaction->save();
        }, 2);
    }

    public static function makeCashbook($saleData, $invoiceId, $reference, $transaction, $reversal = false)
    {
        $cashbookAmount = 0;
        $cashbookRef = '';
        $transactionRef = [];
        $mpesaAmount = 0;
        $mpesaRef = '';
        $creditAmount = 0;
        $creditRef = '';
        $creditTrRef = '';
        $mpesaDetails = [];

        foreach ($saleData['payments'] as $payment) {
            if ($payment['name'] == 'Credit' && $payment['amount'] > 0) {
                $transactionRef[] = $payment['name'] . ': ' . $payment['reference'];
                $creditTrRef .= $payment['name'] . ': ' . $payment['reference'];
                $creditAmount += $payment['amount'];
                $creditRef .= $payment['reference'];
                continue;
            }

            if ($payment['name'] == 'MPesa' && $payment['amount'] > 0) {
                $mpesaAmount += $payment['amount'];
                $mpesaDetails = array_merge($mpesaDetails, json_decode($payment['reference']));
                foreach (json_decode($payment['reference']) as $mpesa) {
                    if ($mpesa->amount < 1) {
                        continue;
                    }
                    $transactionRef[] = $payment['name'] . ': ' . $mpesa->reference;
                    $mpesaRef .= 'M-Pesa: ' .$mpesa->reference . ' ';
                }

                continue;
            }

            $cashbookRef = $payment['name'] . ' ' . $payment['reference'];
            $cashbookAmount += $payment['amount'];
        }

        $expectedCashAmount = $saleData['netPrice'] - ($creditAmount + $mpesaAmount);

        if ($cashbookAmount > $expectedCashAmount) {
            $cashbookAmount = $expectedCashAmount;
        }

        $cashbookId = 0;

        if ($cashbookAmount > 0) {
            $cashbookId = DB::table('_btblCbBatchLines')
                ->insertGetId(
                    self::mapCashBook(config('pos.cash_cashbook_batch', 1), $saleData, $cashbookAmount, $cashbookRef, $reference, $reversal)
                );
        }

        if ($mpesaAmount > 0) {
            foreach ($mpesaDetails as $details) {
                if ($details->amount < 1) {
                    continue;
                }

                $cashbookId = DB::table('_btblCbBatchLines')
                    ->insertGetId(
                        self::mapCashBook(config('pos.mpesa_cashbook_batch', 1), $saleData, $details->amount, 'M-Pesa: ' . $details->reference, $reference, $reversal)
                    );
            }
        }


        $transaction->fill([
            'user_id' => Auth::id(),
            'warehouse_id' => SessionRepository::getWarehouseID(),
            'invoice_id' => $invoiceId,
            'cashbook_id' => $cashbookId,
            'transaction_type' => $reversal ? Transactions::REVERSAL : Transactions::SALE,
            'amount' => $saleData['netPrice'],
            'payment_type' => implode(', ', $transactionRef),
            'transaction_data' => json_encode($saleData),
            'status' => Transactions::STATUS_COMPLETED,
            'locked' => false
        ]);

        $transaction->save();

        return $transaction;
    }

    public static function makeCRNCashbook($saleData, $invoiceId, $reference, $transaction, $reversal = false)
    {
        $cashbookAmount = 0;
        $cashbookRef = '';
        $transactionRef = [];
        $mpesaAmount = 0;
        $mpesaRef = '';
        $creditAmount = 0;
        $creditRef = '';
        $creditTrRef = '';
        $mpesaDetails = [];

        foreach ($saleData['payments'] as $payment) {
            if ($payment['name'] == 'Credit' && $payment['amount'] > 0) {
                $transactionRef[] = $payment['name'] . ': ' . $payment['reference'];
                $creditTrRef .= $payment['name'] . ': ' . $payment['reference'];
                $creditAmount += $payment['amount'];
                $creditRef .= $payment['reference'];
                continue;
            }

            if ($payment['name'] == 'MPesa' && $payment['amount'] > 0) {
                $mpesaAmount += $payment['amount'];
                $mpesaDetails = array_merge($mpesaDetails, json_decode($payment['reference']));
                foreach (json_decode($payment['reference']) as $mpesa) {
                    if ($mpesa->amount < 1) {
                        continue;
                    }
                    $transactionRef[] = $payment['name'] . ': ' . $mpesa->reference;
                    $mpesaRef .= 'M-Pesa: ' .$mpesa->reference . ' ';
                }

                continue;
            }

            $cashbookRef = $payment['name'] . ' ' . $payment['reference'];
            $cashbookAmount += $payment['amount'];
        }

        $expectedCashAmount = $saleData['netPrice'] - ($creditAmount + $mpesaAmount);

        if ($cashbookAmount > $expectedCashAmount) {
            $cashbookAmount = $expectedCashAmount;
        }

        $cashbookId = 0;

        if ($cashbookAmount > 0) {
            $cashbookId = DB::table('_btblCbBatchLines')
                ->insertGetId(self::mapCRNCashBook(config('pos.cash_cashbook_batch', 1), $saleData, $cashbookAmount, $cashbookRef, $reference));
        }

        if ($mpesaAmount > 0) {
            foreach ($mpesaDetails as $details) {
                if ($details->amount < 1) {
                    continue;
                }

                $cashbookId = DB::table('_btblCbBatchLines')
                    ->insertGetId(self::mapCRNCashBook(config('pos.mpesa_cashbook_batch', 1), $saleData, $details->amount, 'M-Pesa: ' . $details->reference, $reference));
            }
        }


        $transaction->fill([
            'user_id' => Auth::id(),
            'warehouse_id' => SessionRepository::getWarehouseID(),
            'invoice_id' => $invoiceId,
            'cashbook_id' => $cashbookId,
            'transaction_type' => $reversal ? Transactions::REVERSAL : Transactions::SALE,
            'amount' => $saleData['netPrice'],
            'payment_type' => implode(', ', $transactionRef),
            'transaction_data' => json_encode($saleData),
            'status' => Transactions::STATUS_COMPLETED,
            'locked' => false
        ]);

        $transaction->save();

        return $transaction;
    }

    public static function getUpdateStockQuery($saleData)
    {
        list($onHand, $onSO, $keys) = self::getWarehouseUpdateLink($saleData);

        return 'update WhseStk set WHQtyReserved = case idWhseStk '.
            $onHand .
            ' else WHQtyReserved'.
            ' end, WHQtyOnSO = case idWhseStk '.
            $onSO .
            ' else WHQtyReserved'.
            ' end' .
            ' where WHWhseID = ' . SessionRepository::getWarehouseID() .
            ' and idWhseStk in (' . implode(',', $keys).')';
    }

    public static function getStkItemUpdateLink($saleData)
    {
        $reserved = [];
        $onSO = [];
        $ids = [];
        foreach ($saleData['items'] as $line) {
            $amount = $line['quantity'] * $line['measure']['ratio'];
            if ($line['measure']['ratio'] > 1) {
                $amount = $line['quantity'] * $line['measure']['ratio'];
            }
            $ids[] = $line['StockLink'];
            $reserved [] = 'when '. $line['StockLink'] . ' then (ReservedQty + ' . $amount . ')';
            $onSO [] = 'when '. $line['StockLink'] . ' then (QtyOnSO + ' . $amount . ')';
        }

        return array(implode(' ', $reserved), implode(' ', $onSO), $ids);
    }

    public static function getUpdateStockItemQuery($saleData)
    {
        list($reserved, $onSO, $keys) = self::getStkItemUpdateLink($saleData);

        return 'update StkItem set ReservedQty = case StockLink '.
            $reserved .
            ' else ReservedQty'.
            ' end, QtyOnSO = case StockLink '.
            $onSO .
            ' else QtyOnSO'.
            ' end' .
            ' where StockLink in (' . implode(',', $keys).')';
    }

    public static function getWarehouseUpdateLink($saleData)
    {
        $reserved = [];
        $onSO = [];
        $ids = [];
        foreach ($saleData['items'] as $line) {
            $amount = $line['quantity'] * $line['measure']['ratio'];
            if ($line['measure']['ratio'] > 1) {
                $amount = $line['quantity'] * $line['measure']['ratio'];
            }

            $ids[] = $line['idWhseStk'];
            $reserved [] = 'when '. $line['idWhseStk'] . ' then (WHQtyReserved + ' . $amount . ')';
            $onSO [] = 'when '. $line['idWhseStk'] . ' then (WHQtyOnSO + ' . $amount . ')';
        }

        return array(implode(' ', $reserved), implode(' ', $onSO), $ids);
    }

    private static function mapInvoiceFields($invoice, $reference, $type = 4)
    {
        $totalTax = [];
        $totalTax['inclusive'] = 0;
        $totalTax['total'] = 0;
        $totalTax['amount'] = 0;

        foreach ($invoice['tax'] as $entry) {
            $totalTax['inclusive'] += str_replace(',', '', $entry['inclusive']);
            $totalTax['total'] += str_replace(',', '', $entry['total']);
            $totalTax['amount'] += str_replace(',', '', $entry['amount']);
        }

        $transactionDate = Carbon::now()->format('Y-m-d');
        $discount = ($invoice['saleDiscount'] / $invoice['totalPrice']) * 100;

        if (strpos($invoice['saleDiscount'], '%') === true) {
            $discount = substr($invoice['saleDiscount'], 0, -1);
        }

        $customerAccount = is_array($invoice['customer']) ? $invoice['customer']['DCLink'] : config('pos.cash_customer_id', 1);
        $isCashCustomer = $customerAccount == config('pos.cash_customer_id', 1);

        return [
            'iINVNUMAgentID' => Auth::user()->agent_id,
            'DocType' => $type,
            'DocVersion' => 1,
            'DocState' => 1,
            'DocFlag' => 0,
            'OrigDocID' => 0,
            'GrvID' => 0,
            'GrvNumber' => '',
            'InvNumber' => '',
            'OrderNum' => $reference,
            'ExtOrderNum' => $reference,
            'AccountID' => $customerAccount,
            'Description' => 'POS Sale',
            'InvDate' => $transactionDate,
            'OrderDate' => $transactionDate,
            'DueDate' => $transactionDate,
            'DeliveryDate' => $transactionDate,
            'TaxInclusive' => 1,
            'Email_Sent' => 1,
            'InvTotExclDEx' => 0,
            'InvTotTaxDEx' => 0,
            'InvTotInclDEx' => $invoice['totalPrice'],
            'InvTotExcl' => $invoice['totalPrice'] - $totalTax['amount'],
            'InvTotTax' => $totalTax['amount'],
            'InvTotIncl' => $invoice['totalPrice'],
//            'OrdDiscAmnt' => $invoice['totalDiscount'],
            'InvDisc' => $discount,
            'InvDiscAmnt' => 0,
            'OrdDiscAmntEx' => 0,
            'OrdTotExclDEx' => 0,
            'OrdTotTaxDEx' => 0,
            'OrdTotInclDEx' => $invoice['totalPrice'],
            'OrdTotExcl' => 0,
            'OrdTotTax' => 0,
            'OrdTotIncl' => $invoice['totalPrice'],
            'cTaxNumber' => $isCashCustomer ? 0 : null,
            'cAccountName' => $isCashCustomer ? 'Cash Customer' : '',
            'InvTotInclExRounding' => $invoice['totalPrice'],
            'OrdTotInclExRounding' => $invoice['totalPrice']
        ];
    }

    private static function syncInvoiceLines($lines, $invoiceId)
    {
        $invoiceLines = [];
        $priceList = config('pos.price_list');
        $lineNumber = 1;

        $taxRates = DB::table('TaxRate')->get()->keyBy('idTaxRate');

        foreach ($lines as $line) {
            $invoiceLines [] = self::mapInvoiceLineFields($lineNumber, $line, $invoiceId, $priceList, $taxRates);
            $lineNumber++;
        }

        return $invoiceLines;
    }

    private static function mapInvoiceLineFields($lineNumber, $line, $invoiceId, $priceList, $taxRates)
    {
        $discount = 0;
        if ($line['discount'] != 0) {
            $discount = ($line['discount'] / ($line['quantity'] * $line['sellingPrice'])) * 100;

            if (strpos($line['discount'], '%') === true) {
                $discount = substr($line['discount'], 0, -1);
            }
        }

        $taxRate = $taxRates->get($line['TaxCode'])->TaxRate;
        $calculatedRate = ((100 + $taxRate) / 100);
        $lineTotalNoDisc = $line['quantity'] * $line['sellingPrice'];
        $lineTotal = $lineTotalNoDisc - $line['discount'];
        $lineTotalExclNoDisc = $lineTotalNoDisc / $calculatedRate;
        $lineTotalExcl = $lineTotal / $calculatedRate;

        return [
            'iWarehouseID' => (int) $line['WHWhseID'],
            'iInvoiceID' => $invoiceId,
            'cDescription' => 'POS Sale',
            'iUnitsOfMeasureStockingID' => $line['iUOMStockingUnitID'],
            'iUnitsOfMeasureCategoryID' => 1,
            'iUnitsOfMeasureID' => $line['measure']['id'],
            'iOrigLineID' => 0,
            'iGrvLineID' => 0,
            'fQtyLastProcess' => 0,
            'fQtyProcessed' => 0,
            'fQtyReserved' => $line['quantity'],
            'fQtyReservedChange' => $line['quantity'],
            'cLineNotes' => '',
            'fUnitCost' => 0,
            'fLineDiscount' => $discount,
            'fTaxRate' => (float) $taxRate,
            'fAddCost' => 0,
            'iJobID' => 0,
            'iPriceListNameID' => (int) $priceList,
            'fQtyLastProcessLineTotIncl' => 0,
            'fQtyLastProcessLineTotExcl' => 0,
            'fQtyLastProcessLineTotInclNoDisc' => 0,
            'fQtyLastProcessLineTotExclNoDisc' => 0,
            'fQtyLastProcessLineTaxAmount' => 0,
            'fQtyLastProcessLineTaxAmountNoDisc' => 0,
            'fQtyProcessedLineTotIncl' => 0,
            'fQtyProcessedLineTotExcl' => 0,
            'fQtyProcessedLineTotInclNoDisc' => 0,
            'fQtyProcessedLineTotExclNoDisc' => 0,
            'fQtyProcessedLineTaxAmount' => 0,
            'fQtyProcessedLineTaxAmountNoDisc' => 0,
            'fUnitPriceExclForeign' => 0,
            'fUnitPriceInclForeign' => 0,
            'fAddCostForeign' => 0,
            'fQuantityLineTotInclForeign' => 0,
            'fQuantityLineTotExclForeign' => 0,
            'fQuantityLineTotInclNoDiscForeign' => 0,
            'fQuantityLineTotExclNoDiscForeign' => 0,
            'fQuantityLineTaxAmountForeign' => 0,
            'fQuantityLineTaxAmountNoDiscForeign' => 0,
            'fQtyChangeLineTotInclForeign' => 0,
            'fQtyChangeLineTotExclForeign' => 0,
            'fQtyChangeLineTotInclNoDiscForeign' => 0,
            'fQtyChangeLineTotExclNoDiscForeign' => 0,
            'fQtyChangeLineTaxAmountForeign' => 0,
            'fQtyChangeLineTaxAmountNoDiscForeign' => 0,
            'fQtyToProcessLineTotInclForeign' => 0,
            'fQtyToProcessLineTotExclForeign' => 0,
            'fQtyToProcessLineTotInclNoDiscForeign' => 0,
            'fQtyToProcessLineTotExclNoDiscForeign' => 0,
            'fQtyToProcessLineTaxAmountForeign' => 0,
            'fQtyToProcessLineTaxAmountNoDiscForeign' => 0,
            'fQtyLastProcessLineTotInclForeign' => 0,
            'fQtyLastProcessLineTotExclForeign' => 0,
            'fQtyLastProcessLineTotInclNoDiscForeign' => 0,
            'fQtyLastProcessLineTotExclNoDiscForeign' => 0,
            'fQtyLastProcessLineTaxAmountForeign' => 0,
            'fQtyLastProcessLineTaxAmountNoDiscForeign' => 0,
            'fQtyProcessedLineTotInclForeign' => 0,
            'fQtyProcessedLineTotExclForeign' => 0,
            'fQtyProcessedLineTotInclNoDiscForeign' => 0,
            'fQtyProcessedLineTotExclNoDiscForeign' => 0,
            'fQtyProcessedLineTaxAmountForeign' => 0,
            'fQtyProcessedLineTaxAmountNoDiscForeign' => 0,
            'iLineRepID' => 0,
            'iLineProjectID' => 0,
            'iLedgerAccountID' => 0,
            'iLotID' => 0,
            'cLotNumber' => '',
            'iMFPID' => 0,
            'iLineID' => $lineNumber,
            'iDeliveryMethodID' => 0,
            'iDeliveryStatus' => 0,
            'fPromotionPriceExcl' => 0,
            'fPromotionPriceIncl' => 0,
            'cPromotionCode' => '',
            'fQuantityUR' => $line['quantity'],
            'fQtyChangeUR' => $line['quantity'],
            'fQtyToProcessUR' => $line['quantity'],
            'fQtyLastProcessUR' => 0,
            'fQtyProcessedUR' => 0,
            'fQtyReservedUR' => $line['quantity'],
            'fQtyReservedChangeUR' => $line['quantity'],
            'iSalesWhseID' => 0,
            '_btblInvoiceLines_iBranchID' => 0,
            'fQuantity' => $line['quantity'],
            'fQtyChange' => $line['quantity'],
            'fQtyToProcess' => $line['quantity'],
            'fUnitPriceExcl' => $line['fExclPrice'],
            'fUnitPriceIncl' => $line['sellingPrice'],
            'iModule' => 0,
            'iStockCodeID' => $line['StockLink'],
            'iTaxTypeID' => $line['TaxCode'],
            'fQuantityLineTotIncl' => $lineTotal,
            'fQuantityLineTotExcl' => $lineTotalExcl,
            'fQuantityLineTotInclNoDisc' => $lineTotalNoDisc,
            'fQuantityLineTotExclNoDisc' => $lineTotalExclNoDisc,
            'fQuantityLineTaxAmount' => $lineTotal - $lineTotalExcl,
            'fQuantityLineTaxAmountNoDisc' => $lineTotalNoDisc - $lineTotalExclNoDisc,
            'fQtyChangeLineTotIncl' => $lineTotal,
            'fQtyChangeLineTotExcl' => $lineTotalExcl,
            'fQtyChangeLineTotInclNoDisc' => $lineTotalNoDisc,
            'fQtyChangeLineTotExclNoDisc' => $lineTotalExclNoDisc,
            'fQtyChangeLineTaxAmount' =>  0,
            'fQtyChangeLineTaxAmountNoDisc' => 0,
            'fQtyToProcessLineTotIncl' =>  $lineTotal,
            'fQtyToProcessLineTotExcl' => $lineTotalExcl,
            'fQtyToProcessLineTotInclNoDisc' => $lineTotalNoDisc,
            'fQtyToProcessLineTotExclNoDisc' => $lineTotalExclNoDisc,
            'fQtyToProcessLineTaxAmount' => 0,
            'fQtyToProcessLineTaxAmountNoDisc' => 0
        ];
    }

    private static function mapCashBook($cashBookBatch, $invoice, $paymentAmount, $paymentReference, $reference, $return = false)
    {
        $customerAccount = is_array($invoice['customer']) ? $invoice['customer']['DCLink'] : config('pos.cash_customer_id', 1);
        $payment = $paymentAmount;
        $cashBack = 0;

        if ($return) {
            $cashBack = $paymentAmount;
            $payment = 0;
        }

        return [
            'iBatchesID' => $cashBookBatch,
            'dTxDate' => Carbon::now()->format('Y-m-d'),
            'iModule' => 1,
            'iAccountID' => $customerAccount,
            'cDescription' => $paymentReference,
            'cReference' => $reference . ': ' . $paymentReference,
            'fDebit' => $payment, // TODO: separate the different payment options
            'fCredit' => $cashBack,
            'bReconcile' => 0,
            'bPostDated' => 0,
            'bPrintCheque' => 0,
            'bChequePrinted' => 0,
            'fTaxAmount' => 0,
            'iTaxTypeID' => 0, // TODO: implement the separate payment tax types
            'iTaxAccountID' => 0
        ];
    }

    private static function mapCRNCashBook($cashBookBatch, $invoice, $paymentAmount, $paymentReference, $reference)
    {
        $customerAccount = is_array($invoice['customer']) ? $invoice['customer']['DCLink'] : config('pos.cash_customer_id', 1);
        return [
            'iBatchesID' => $cashBookBatch,
            'dTxDate' => Carbon::now()->format('Y-m-d'),
            'iModule' => 1,
            'iAccountID' => $customerAccount,
            'cDescription' => $paymentReference,
            'cReference' => $reference . ': ' . $paymentReference,
            'fDebit' => 0, // TODO: separate the different payment options
            'fCredit' => $paymentAmount,
            'bReconcile' => 0,
            'bPostDated' => 0,
            'bPrintCheque' => 0,
            'bChequePrinted' => 0,
            'fTaxAmount' => 0,
            'iTaxTypeID' => 0, // TODO: implement the separate payment tax types
            'iTaxAccountID' => 0
        ];
    }


    public static function find($invoiceNumber, $cashbookIds)
    {
        $accountId = DB::table('InvNum')
            ->where('AutoIndex', '=', $invoiceNumber)
            ->first(['AccountID'])->AccountID;

        $customer = DB::table('Client')->where('DCLink', '=', $accountId)->first([
            'DCLink', 'Account', 'Name', 'Telephone', 'EMail', 'Physical1',
            'DCBalance', 'Credit_Limit'
        ]);

        $lines = DB::table('_btblInvoiceLines')->where('iInvoiceID', '=', $invoiceNumber)->get([
            'fLineDiscount', 'fQuantity', 'iStockCodeID', 'fUnitPriceIncl'
        ])->keyBy('iStockCodeID');

        $items = StockRepository::findAll($lines->keys())->map(function ($item) use ($lines) {
            $line = $lines->get($item->StockLink);
            $item->discount = $line->fLineDiscount > 0 ?
                ($line->fLineDiscount * $line->fUnitPriceIncl * $line->fQuantity) / 100 : 0;
            $item->quantity = $line->fQuantity;

            return $item;
        });

        $cashbooks = DB::table('_btblCbBatchLines')
            ->whereIn('idBatchLines', $cashbookIds)
            ->get(['fDebit', 'cDescription', 'cReference'])
            ->map(function ($value) {
                $type = explode(' ', $value->cDescription);
                $reference = explode(': ', $value->cReference);

                return [
                    'amount' => $value->fDebit,
                    'name' => $type[0],
                    'reference' => $reference[1],
                ];
            });

        $complete = [
            'customer' => $customer,
            'items' => $items->toArray(),
            'payments' => $cashbooks->toArray()
        ];

        return $complete;
    }
}
