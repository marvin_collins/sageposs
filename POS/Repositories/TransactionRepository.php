<?php

namespace POS\Repositories;

class TransactionRepository
{
    const FILENAME = 'trans.log';

    private static function getFilename()
    {
        return storage_path('statements'. DIRECTORY_SEPARATOR . self::FILENAME);
    }

    public static function getCurrentNumber()
    {
        if (! is_file(self::getFilename())) {
            if (! is_dir(storage_path('statements'))) {
                mkdir(storage_path('statements'), 0755, true);
            }
            file_put_contents(self::getFilename(), 0);

            return 0;
        }

        return intval(file_get_contents(self::getFilename()));
    }

    public static function increment()
    {
        $new = self::getCurrentNumber() + 1;
        file_put_contents(self::getFilename(), $new);

        return $new;
    }

    public static function getRawIncrement()
    {
        return self::getCurrentNumber() + 1;
    }
}
