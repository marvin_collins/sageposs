<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 11/7/17
 * Time: 8:08 AM
 */

namespace POS\Repositories;


use Illuminate\Support\Facades\DB;

class ProjectRepository
{
    public static function getAllProjects()
    {
        $projects = DB::table('Project')->get(['ProjectLink','ProjectCode','ProjectName']);

        return $projects;
    }
}