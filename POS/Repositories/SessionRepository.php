<?php

namespace POS\Repositories;

use Session;

class SessionRepository
{
//    TODO: If user is allowed to sell from any store, select the store after login
    const WAREHOUSE_KEY = 'POS_WAREHOUSE_SESSION';

    public static function getWarehouseID()
    {
        return Session::get(self::WAREHOUSE_KEY);
    }

    public static function setWarehouseID($warehouseId)
    {
        self::removeWarehouseID();
        Session::put(self::WAREHOUSE_KEY, $warehouseId);
    }

    public static function removeWarehouseID()
    {
        Session::remove(self::WAREHOUSE_KEY);
    }
}
