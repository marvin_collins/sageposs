<?php

namespace POS\Repositories;

use Auth;
use Cache;
use POS\Models\Assignment;
use POS\Models\Permission;
use POS\Models\Role;
use POS\Models\Setting;
use POS\Models\Warehouse;

class CacheRepository
{
    const WAREHOUSES_KEY = 'POS_WAREHOUSE';
    const ASSIGNMENTS_KEY = 'POS_WAREHOUSE';
    const ROLES_KEY = 'POS_ROLES';
    const SETTINGS_KEY = 'POS_SETTINGS';

    public static function cacheWarehouses()
    {
        Cache::forget(self::WAREHOUSES_KEY);
        Cache::remember(self::WAREHOUSES_KEY, 20, function () {
            return Warehouse::all(['WhseLink', 'Name']);
        });
    }

    public static function cacheAssignments()
    {
        Cache::forget(self::ASSIGNMENTS_KEY);

        Cache::remember(self::ASSIGNMENTS_KEY, 10, function () {
            return Assignment::with(['user' => function ($query) {
                $query->select(['id', 'name']);
            }, 'warehouse' => function ($query) {
                $query->select(['WhseLink', 'Name']);
            }])->get();
        });
    }

    public static function getAssignments()
    {
        if (! Cache::has(self::ASSIGNMENTS_KEY)) {
            self::cacheAssignments();
        }

        return Cache::get(self::ASSIGNMENTS_KEY);
    }

    public static function getMyAssignments()
    {
        return self::getAssignments()->where('user_id', Auth::id());
    }

    public static function getMyAssignedIds()
    {
        return self::getAssignments()->where('user_id', Auth::id())->map(function ($value) {
            return $value->warehouse_id;
        })->toArray();
    }

    public static function getPermissions()
    {
        if (! Cache::has('POS_PERMISSIONS')) {
            Cache::rememberForever('POS_PERMISSIONS', function () {
                return Permission::all();
            });
        }

        return Cache::get('POS_PERMISSIONS');
    }

    public static function cacheRoles()
    {
        Cache::forget(self::ROLES_KEY);
        Cache::remember(self::ROLES_KEY, 30, function () {
            return Role::all();
        });
    }

    public static function getRoles()
    {
        if (! Cache::has(self::ROLES_KEY)) {
            self::cacheRoles();
        }

        return Cache::get(self::ROLES_KEY);
    }

    public static function cacheSettings()
    {
        Cache::forget(self::SETTINGS_KEY);
        Cache::rememberForever(self::SETTINGS_KEY, function () {
            return Setting::all();
        });
    }

    public static function getSettings()
    {
        if (! Cache::has(self::SETTINGS_KEY)) {
            self::cacheSettings();
        }

        return Cache::get(self::SETTINGS_KEY);
    }
}
