<?php

namespace POS\Models;

class Permission extends POSModel
{
    protected $fillable = ['name', 'group'];

    const APPROVE_TRANSACTION = ['name' => 'Approve', 'group' => 'Transactions'];
    const REPRINT_TRANSACTION = ['name' => 'Reprint', 'group' => 'Transactions'];
}
