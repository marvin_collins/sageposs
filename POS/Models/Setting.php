<?php

namespace POS\Models;

class Setting extends POSModel
{
    const AUTO_MERGE = 'auto_merge';

    const AUTO_FOCUS = 'auto_focus';

    const DISCOUNT_TYPE = 'discount_type';

    const DISCOUNT_AMOUNT = 'discount_amount';

    const AUTO_INSERT = 'insert_on_search';

    const GP_PERCENT = 'gross_profit_percentage';

    const AUTO_MERGE_RECEIPT = 'auto_merge_receipt';

    const CODE_ONLY_SEARCH = 'code_only_search';

    protected $fillable = [
        'value'
    ];
}
