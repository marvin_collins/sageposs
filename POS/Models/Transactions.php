<?php

namespace POS\Models;

use App\User;
use POS\Repositories\TransactionRepository;

class Transactions extends POSModel
{
    const TILL_OPEN = 'Opening Till';
    const TILL_CLOSE = 'Closing Till';
    const SALE = 'Sale Made';
    const SAVED_SALE = 'Saved Sale';
    const PETTY_CASH = 'Petty Cash';
    const REVERSAL = 'Return of Goods';
    const APPROVAL = 'Pending Approval';
    const APPROVED = 'Approved';
    const DISAPPROVED = 'Disapproved';
    const SALES_ORDER = 'Sales Order';
    const CASH_RECEIPT = 'Cash Receipt';

    const STATUS_COMPLETED = 0;
    const STATUS_ON_HOLD = 1;
    const STATUS_PENDING = 2;
    const STATUS_SAVED = 3;
    const STATUS_DECLINED = 4;
    const STATUS_APPROVED = 5;

    protected $fillable = [
        'transaction_number', 'user_id', 'warehouse_id', 'transaction_type', 'amount',
        'invoice_id', 'cashbook_id', 'locked', 'petty_cash_id', 'payment_type',
        'transaction_data', 'reversed', 'status', 'original_transaction_id',
        'created_at', 'updated_at', 'remarks'
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($transaction) {
            $transaction->transaction_number = '#' . config('pos.site_id', 'TR') .
                '-' . TransactionRepository::increment();
        });
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'WhseLink');
    }

    public static function updateIfOld($data, $id = null)
    {
        if ($id) {
            $transaction = self::findOrFail($id);
            $transaction->fill($data)->save();

            return $transaction;
        }

        return self::create($data);
    }

}
