<?php

namespace POS\Models;

class Account extends POSModel
{
    protected $table = 'Accounts';

    protected $primaryKey = 'AccountLink';
}
