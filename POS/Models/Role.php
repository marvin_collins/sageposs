<?php

namespace POS\Models;

use App\User;
use Illuminate\Database\Eloquent\Builder;

class Role extends POSModel
{
    protected $fillable = ['name', 'permissions'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('hidden', function (Builder $builder) {
            $builder->where('id', '>', 0);
        });
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getPermissionsAttribute($value)
    {
        return json_decode($value);
    }
}
