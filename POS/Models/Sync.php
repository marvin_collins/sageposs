<?php

namespace POS\Models;

use Illuminate\Database\Eloquent\Model;

class Sync extends Model
{
    protected $connection = 'backup';

    protected $fillable = [
        'id', 'sql', 'bindings'
    ];

//    protected function getDateFormat()
//    {
//        return 'Y-m-d H:i:s';
//    }

}
