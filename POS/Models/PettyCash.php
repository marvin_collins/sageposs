<?php

namespace POS\Models;

class PettyCash extends POSModel
{
    protected $fillable = ['user_id', 'petty_cash_type_id', 'amount', 'reference'];

    public function type()
    {
        return $this->belongsTo(PettyCashType::class, 'petty_cash_type_id');
    }

}
