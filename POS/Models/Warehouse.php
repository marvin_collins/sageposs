<?php

namespace POS\Models;

class Warehouse extends POSModel
{
    protected $table = 'WhseMst';

    protected $primaryKey = 'WhseLink';
}
