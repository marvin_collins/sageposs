<?php

namespace POS\Models;

class PettyCashType extends POSModel
{
    protected $fillable = ['module_id', 'cashbook_batch_id', 'account_id', 'name', 'transaction_type'];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

}
