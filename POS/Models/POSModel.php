<?php

namespace POS\Models;

use App;
use App\Events\ExistingModelDeleted;
use App\Events\ExistingModelUpdated;
use App\Events\NewModelCreated;
use App\Jobs\ModelChanged;
use App\Jobs\ModelDeleted;
use DB;
use Event;
use Exception;
use Illuminate\Database\Eloquent\Model;
use POS\Traits\ModelSettings;

abstract class POSModel extends Model
{
    use ModelSettings;
}
