<?php

namespace POS\Models;

class OpenSession extends POSModel
{
    protected $fillable = ['user_id', 'warehouse_id', 'opened_at', 'open'];
}
