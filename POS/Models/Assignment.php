<?php

namespace POS\Models;

use App\User;

class Assignment extends POSModel
{
    protected $fillable = ['user_id', 'warehouse_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
}
