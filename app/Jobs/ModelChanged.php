<?php

namespace App\Jobs;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use POS\Models\POSModel;

class ModelChanged implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var POSModel
     */
    private $model;

    /**
     * Create a new job instance.
     *
     * @param POSModel $model
     */
    public function __construct(POSModel $model)
    {
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::setDefaultConnection(env('DB_CONNECTION'));
        $this->model->save();
    }
}
