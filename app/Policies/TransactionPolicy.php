<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use POS\Models\Permission;
use POS\Models\Transactions;
use POS\Repositories\CacheRepository;

class TransactionPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the transactions.
     *
     * @param  App\User  $user
     * @param  App\Transactions  $transactions
     * @return mixed
     */
    public function view(User $user, Transactions $transactions)
    {
        return $this->getAuthorization($user, 'View');
    }

    /**
     * Determine whether the user can create transactions.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the transactions.
     *
     * @param  App\User  $user
     * @param  App\Transactions  $transactions
     * @return mixed
     */
    public function update(User $user, Transactions $transactions)
    {
        //
    }

    /**
     * Determine whether the user can delete the transactions.
     *
     * @param  App\User  $user
     * @param  App\Transactions  $transactions
     * @return mixed
     */
    public function delete(User $user, Transactions $transactions)
    {
        //
    }

    public function approve(User $user, Transactions $transactions)
    {
        $permissionId = getApproverPermissionId();
        $rolePermissions = CacheRepository::getRoles()
            ->where('id', $user->role_id)->first()->permissions;

        $rolePermissions = is_null($rolePermissions) ? [] : $rolePermissions;
        $userPermissions = is_null($user->permissions) ? [] : $user->permissions;


        return in_array($permissionId, $rolePermissions) ||
            in_array($permissionId, json_decode($userPermissions));
    }

    public function reprint(User $user, Transactions $transactions)
    {
        $permissionId = getReprintPermissionId();
        $rolePermissions = CacheRepository::getRoles()
            ->where('id', $user->role_id)->first()->permissions;

        $rolePermissions = is_null($rolePermissions) ? [] : $rolePermissions;
        $userPermissions = is_null($user->permissions) ? [] : $user->permissions;


        return in_array($permissionId, $rolePermissions) ||
            in_array($permissionId, json_decode($userPermissions));
    }

    private function getAuthorization(User $authUser, $permission)
    {
        $permission = $this->getPermission($permission, 'Transactions');

        return $this->userHasPermission($authUser, $permission) ||
        $this->roleHasPermission($authUser, $permission);
    }
}
