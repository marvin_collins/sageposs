<?php

namespace App\Policies;

use App\User;
use POS\Models\PettyCashType;
use Illuminate\Auth\Access\HandlesAuthorization;

class PettyCashTypePolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the pettyCashTypes.
     *
     * @param  App\User  $user
     * @param  App\PettyCashTypes  $pettyCashTypes
     * @return mixed
     */
    public function view(User $user, PettyCashType $pettyCashTypes)
    {
        return $this->getAuthorization($user, 'Full Control');
    }

    /**
     * Determine whether the user can create pettyCashTypes.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the pettyCashTypes.
     *
     * @param  App\User  $user
     * @param  App\PettyCashTypes  $pettyCashTypes
     * @return mixed
     */
    public function update(User $user, PettyCashType $pettyCashTypes)
    {
        //
    }

    /**
     * Determine whether the user can delete the pettyCashTypes.
     *
     * @param  App\User  $user
     * @param  App\PettyCashTypes  $pettyCashTypes
     * @return mixed
     */
    public function delete(User $user, PettyCashType $pettyCashTypes)
    {
        //
    }

    private function getAuthorization(User $authUser, $permission)
    {
        $permission = $this->getPermission($permission, 'Petty Cash Types');

        return $this->userHasPermission($authUser, $permission) ||
        $this->roleHasPermission($authUser, $permission);
    }
}
