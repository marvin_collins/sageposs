<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use POS\Models\Setting;
use POS\Repositories\CacheRepository;

class SettingPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param User          $authUser
     * @param Setting $setting
     *
     * @return mixed
     */
    public function view(User $authUser, Setting $setting)
    {
        return $this->getAuthorization($authUser, 'Modify');
    }

    /**
     * Determine whether the user can create users.
     *
     * @param User $authUser
     *
     * @return mixed
     * @internal param App\User $user
     */
    public function create(User $authUser)
    {
        //
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param User          $authUser
     * @param App\User|User $user
     *
     * @return mixed
     */
    public function update(User $authUser, User $user)
    {
        //
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param User          $authUser
     * @param App\User|User $user
     *
     * @return mixed
     */
    public function delete(User $authUser, User $user)
    {
        //
    }

    private function getAuthorization(User $authUser, $permission)
    {
        $permission = $this->getPermission($permission, 'System Settings');

        return $this->userHasPermission($authUser, $permission) ||
        $this->roleHasPermission($authUser, $permission);
    }
}
