<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use POS\Models\Assignment;

class AssignmentPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the Assignment.
     *
     * @param App\User|User $user
     * @param Assignment    $assignment
     *
     * @return mixed
     */
    public function view(User $user, Assignment $assignment)
    {
        return $this->getAuthorization($user, 'Full Control');
    }

    /**
     * Determine whether the user can create Assignments.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the Assignment.
     *
     * @param App\User|User $user
     * @param Assignment    $assignment
     *
     * @return mixed
     */
    public function update(User $user, Assignment $assignment)
    {
        //
    }

    /**
     * Determine whether the user can delete the Assignment.
     *
     * @param App\User|User $user
     * @param Assignment    $assignment
     *
     * @return mixed
     */
    public function delete(User $user, Assignment $assignment)
    {
        //
    }

    private function getAuthorization(User $authUser, $permission)
    {
        $permission = $this->getPermission($permission, 'Warehouse Assignment');

        return $this->userHasPermission($authUser, $permission) ||
        $this->roleHasPermission($authUser, $permission);
    }
}
