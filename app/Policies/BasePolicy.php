<?php

namespace app\Policies;

use App\User;
use POS\Repositories\CacheRepository;

abstract class BasePolicy
{
    public function before($user, $ability)
    {
        if ($user->role_id == 0) {
            return true;
        }
    }

    protected function userHasPermission(User $user, $permissionId)
    {
        $permission = is_null($user->permissions) ? [] : json_decode($user->permissions);

        return in_array($permissionId, $permission);
    }

    protected function roleHasPermission(User $user, $permissionId)
    {
        $rolePermissions = CacheRepository::getRoles()
            ->where('id', $user->role_id)->first()->permissions;

        $permission = is_null($rolePermissions) ? [] : $rolePermissions;

        return in_array($permissionId, $permission);
    }

    protected function getPermission($permission, $group)
    {
        return CacheRepository::getPermissions()
            ->where('name', $permission)
            ->where('group', $group)
            ->first()
            ->id;
    }
}
