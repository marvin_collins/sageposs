<?php

namespace App\Providers;

use Carbon\Carbon;
use DB;
use Illuminate\Support\ServiceProvider;
use PDO;
use POS\Managers\SyncManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('pos.is_branch', 0)) {
            try {
                fsockopen(env('DB_HOST'), env('DB_PORT'));
            } catch (\Exception $exception) {
                DB::setDefaultConnection('backup');
            }
        }

        DB::listen(function ($query) {
            if (DB::getDefaultConnection() == 'backup' && ! starts_with($query->sql, 'select') && ! strpos($query->sql, 'syncs')) {
                SyncManager::saveQuery($query->sql, $query->bindings);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
