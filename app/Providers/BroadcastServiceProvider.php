<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;
use POS\Models\Transactions;
use POS\Repositories\CacheRepository;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes();

        // POS Online channel
        Broadcast::channel('POS.Online.*', function ($user, $userId) {
            if ((int) $user->id === (int) $userId) {
                return ['id' => $user->id, 'name' => $user->name];
            }
        });

        Broadcast::channel('POS.User.*', function ($user, $userId) {
            return (int) $user->id === (int) $userId;
        });

        Broadcast::channel('POS.Approver.*', function ($user, $warehouseId) {
            return $user->can('approve', new Transactions) &&
            in_array($warehouseId, CacheRepository::getMyAssignedIds());
        });

        Broadcast::channel('App.User.*', function ($user, $userId) {
            if ((int) $user->id === (int) $userId) {
                return ['id' => $user->id, 'name' => $user->name];
            }

            return false;
        });
    }
}
