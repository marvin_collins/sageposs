<?php

namespace App\Providers;

use App\Policies\AssignmentPolicy;
use App\Policies\PettyCashTypePolicy;
use App\Policies\RolePolicy;
use App\Policies\SettingPolicy;
use App\Policies\TransactionPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use POS\Models\Assignment;
use POS\Models\PettyCashType;
use POS\Models\Role;
use POS\Models\Setting;
use POS\Models\Transactions;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Transactions::class => TransactionPolicy::class,
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
        Assignment::class => AssignmentPolicy::class,
        PettyCashType::class => PettyCashTypePolicy::class,
        Setting::class => SettingPolicy::class,
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
