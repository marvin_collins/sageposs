<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewModelCreated' => [
            'App\Listeners\PrepareCreateSync',
        ],
        'App\Events\ExistingModelUpdated' => [
            'App\Listeners\PrepareUpdateSync',
        ],
        'App\Events\ExistingModelDeleted' => [
            'App\Listeners\PrepareDeleteSync',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
