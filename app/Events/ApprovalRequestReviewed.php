<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use POS\Models\Transactions;

class ApprovalRequestReviewed implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
    /**
     * @var Transactions
     */
    public $transaction;

    /**
     * Create a new event instance.
     *
     * @param Transactions $transaction
     */
    public function __construct(Transactions $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('POS.User.' . $this->transaction->user_id);
    }
}
