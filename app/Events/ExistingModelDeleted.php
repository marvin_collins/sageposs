<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use POS\Models\POSModel;

class ExistingModelDeleted implements ShouldQueue
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var POSModel
     */
    public $model;

    /**
     * Create a new event instance.
     *
     * @param POSModel $model
     */
    public function __construct(POSModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
