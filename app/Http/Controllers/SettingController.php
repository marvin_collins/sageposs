<?php

namespace App\Http\Controllers;

use Flash;
use Illuminate\Http\Request;
use POS\Models\Setting;
use POS\Repositories\CacheRepository;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.index')->withSettings(Setting::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = Setting::all();
        $data = $request->all();
        foreach ($settings as $setting) {
            if (! in_array($setting->name, array_keys($data))) {
                continue;
            }

            $setting->value = $data[$setting->name];
            $setting->save();
        }

        CacheRepository::cacheSettings();

        Flash::success('Settings', 'Successfully updated system settings.');

        return redirect()->route('settings.index');
    }
}
