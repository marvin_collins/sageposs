<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use PDO;
use POS\Models\Role;
use POS\Repositories\UserRepository;
use Response;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', new User);
        if (request()->ajax()) {
            if (request()->has('roles')) {
                return Response::json(Role::all(['id', 'name']));
            }
            return Response::json(User::with(['role'])->where('id', '>', 1)->paginate(10));
        }

        return view('users.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->only(['name', 'email', 'role_id']);
        $data['password'] = bcrypt($request->get('password'));
        $data['permissions'] = json_encode(collect($request->get('permissions'))->reject(function ($value) {
            return $value == false;
        })->keys());

        User::create($data);

        return Response::json(User::with(['role'])->where('id', '>', 1)->paginate(10));
    }

    /**
     * Import users from SAGE.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function import()
    {
        UserRepository::import();

        return Response::json(User::with(['role'])->where('id', '>', 1)->paginate(10));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User                      $user
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        UserRepository::update($user, $request->all());

        return Response::json(User::with(['role'])->where('id', '>', 1)->paginate(10));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     *
     */
    public function destroy(User $user)
    {
        $user->delete();

        return Response::json(User::with(['role'])->where('id', '>', 1)->paginate(10));
    }
}
