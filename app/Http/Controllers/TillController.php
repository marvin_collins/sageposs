<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\OpenSession;
use POS\Models\Transactions;
use POS\Repositories\SessionRepository;

class TillController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! request()->has('sel')) {
            return redirect()->route('check-in.create');
        }

        return view('session.till')->withSelected(request()->get('sel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existing = OpenSession::whereOpen(true)
            ->whereUserId(Auth::id())
            ->whereWarehouseId($request->get('selected'))
            ->firstOrNew([]);

        $existing->fill([
            'user_id' => Auth::id(),
            'warehouse_id' => $request->get('selected'),
            'open' => true,
            'opened_at' => Carbon::now()
        ]);

        $data = [
            'user_id' => Auth::id(),
            'warehouse_id' => $request->get('selected'),
            'transaction_type' => Transactions::TILL_OPEN,
            'amount' => $request->get('opening_amount'),
            'payment_type' => 'Cash'
        ];

        DB::transaction(function () use ($data, $existing) {
            $existing->save();
            $transaction = Transactions::create($data);
            $transaction->transaction_number = '#' . config('pos.site_id', 'TR') . '-' . $transaction->id;
            $transaction->save();
        });

        SessionRepository::setWarehouseID($request->get('selected'));

        return redirect()->route('pos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
