<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use POS\Models\Transactions;
use Response;

class AdminTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            if (request()->has('agents')) {
                return Response::json(['agents' => User::where('id', '>', 1)->get(['name', 'id'])]);
            }

            if (request()->has('start_date')) {
                return $this->getTransactions(request('start_date'), request('end_date'));
            }

            if (request()->has('number')) {
                return $this->getTransactions(null, null, request('number'));
            }

            return $this->getTransactions(Carbon::now(), Carbon::now());
        }

        return view('admin-transactions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transactions::findOrFail($id)->delete();

        return $this->getTransactions(Carbon::now());
    }

    private function getTransactions($startDate = null, $endDate = null, $number = null)
    {
        $transactions = Transactions::with(['creator' => function ($query) {
            return $query->select('id', 'name');
        }])
            ->when($startDate, function ($query) use ($startDate) {
                return $query->where('created_at', '>=', Carbon::parse($startDate)->startOfDay());
            })
            ->when($endDate, function ($query) use ($endDate) {
                return $query->where('created_at', '<=', Carbon::parse($endDate)->endOfDay());
            })
            ->when($number, function ($query) use ($number) {
                return $query->where('transaction_number', 'LIKE', '%' . $number . '%');
            })
            ->get()
            ->map(function ($transaction) {
                $transaction->client = '';
                $saleData = json_decode($transaction->transaction_data);
                if (! $saleData) {
                    return $transaction;
                }
                if (property_exists($saleData, 'customer')) {
                    if (isset($saleData->customer->Name)) {
                        $transaction->client = $saleData->customer->Name;
                    }
                }
                if (property_exists($saleData, 'client')) {
                    $transaction->client = $saleData->client->Name;
                }
                return $transaction;
            });

        $running = 0;

        foreach ($transactions as $transaction) {
            if ($transaction->transaction_type == Transactions::TILL_OPEN ||
                $transaction->transaction_type == Transactions::SALE ||
                $transaction->transaction_type == Transactions::CASH_RECEIPT
            ) {
                $running += $transaction->amount;
                $transaction->running = $running;
                continue;
            }

            if (starts_with($transaction->transaction_type, Transactions::PETTY_CASH)) {
                $running += $transaction->amount;
                $transaction->running = $running;
                continue;
            }

            if ($transaction->transaction_type == Transactions::REVERSAL) {
                $transactionData = json_decode($transaction->transaction_data);
                $transaction->amount = $transaction->amount * -1;
                foreach ($transactionData->payments as $payment) {
                    if ($payment->name == 'Cash') {
                        $running += $transaction->amount;
                        $transaction->running = $running;
                        break;
                    }
                }
                continue;
            }

            $transaction->running = $running;
        }

        return Response::json($transactions);
    }
}
