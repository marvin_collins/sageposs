<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\Permission;
use POS\Models\Role;
use POS\Repositories\CacheRepository;
use Response;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', new Role);
        if (request()->ajax()) {
            if (request()->has('permissions')) {
                return Response::json(Permission::all(['id', 'name', 'group'])->groupBy('group'));
            }

            if (request()->has('plain')) {
                return Response::json(Role::all(['id', 'name']));
            }
            return Response::json(Role::paginate(10));
        }

        return view('roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoleRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $data = $request->only(['name']);

        $data['permissions'] = json_encode(collect($request->get('permissions'))->reject(function ($value) {
            return $value == false;
        })->keys()->toArray());

        Role::create($data);

        CacheRepository::cacheRoles();

        return Response::json(Role::paginate(10));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoleRequest $request
     * @param              $roleId
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     *
     */
    public function update(RoleRequest $request, $roleId)
    {
        $data = $request->only(['name']);

        $data['permissions'] = json_encode(collect($request->get('permissions'))->reject(function ($value) {
            return $value == false;
        })->keys()->toArray());

        Role::findOrFail($roleId)->fill($data)->save();

        CacheRepository::cacheRoles();

        return Response::json(Role::paginate(10));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        CacheRepository::cacheRoles();

        return Response::json(Role::paginate(10));
    }
}
