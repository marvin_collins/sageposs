<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\PettyCash;
use POS\Models\PettyCashType;
use POS\Models\Transactions;
use POS\Repositories\SessionRepository;
use Response;

class PettyCashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->getPettyCash();
        }
        return view('petty-cash.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $type = PettyCashType::findOrFail($data['petty_cash_type_id']);

        DB::transaction(function () use ($type, $data) {
            $petty = PettyCash::create($data);
            $transaction = Transactions::create([
                'user_id' => Auth::id(),
                'warehouse_id' => SessionRepository::getWarehouseID(),
                'transaction_type' => Transactions::PETTY_CASH . ': ' . $type->name,
                'transaction_data' => json_encode($data),
                'amount' => -($data['amount']),
                'invoice_id' => null,
                'cashbook_id' => 0,
                'petty_cash_id' => $petty->id,
                'payment_type' => 'Cash REF: ' . $data['reference']
            ]);
            $transaction->transaction_number = '#' . config('pos.site_id') . '-' . $transaction->id;
            $cashID = $this->createCashBook($type, $data, $transaction->transaction_number);
            $transaction->cashbook_id = $cashID;
            $transaction->save();
        }, 2);

        return $this->getPettyCash();
    }

    private function createCashBook($details, $data, $transNumber)
    {
        return DB::table('_btblCbBatchLines')->insertGetId([
            'iBatchesID' => $details->cashbook_batch_id,
            'dTxDate' => Carbon::now()->format('Y-m-d'),
            'iModule' => $details->module_id,
            'iAccountID' => $details->account_id,
            'cDescription' => $data['reference'],
            'cReference' => $transNumber . ': ' .$data['reference'],
            'fDebit' => $details->transaction_type == 'Debit' ? $data['amount'] : 0,
            'fCredit' => $details->transaction_type == 'Credit' ? $data['amount'] : 0,
            'bReconcile' => 0,
            'bPostDated' => 0,
            'bPrintCheque' => 0,
            'bChequePrinted' => 0,
            'fTaxAmount' => 0,
            'iTaxTypeID' => 0, // TODO: implement the separate payment tax types
            'iTaxAccountID' => 0
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getPettyCash()
    {
        return Response::json(PettyCash::with(['type' => function ($query) {
            $query->select(['id', 'name']);
        }])->where('user_id', '=', Auth::id())
            ->where('created_at', '>=', Carbon::now()->startOfDay())
            ->where('created_at', '<=', Carbon::now()->endOfDay())
            ->select([
                'petty_cash_type_id', 'amount', 'reference'
            ])
            ->paginate(10));
    }
}
