<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Repositories\StockRepository;
use Response;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            if (request()->has('q')) {
                return Response::json(StockRepository::search(request('q')));
            }
            return Response::json(StockRepository::forDataTable());
        }

        return view('inventory.index');
    }
}
