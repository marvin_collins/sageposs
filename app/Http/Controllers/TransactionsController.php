<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use Exception;
use POS\Models\Assignment;
use POS\Models\PettyCashType;
use POS\Models\Transactions;
use POS\Models\Warehouse;
use POS\Repositories\CacheRepository;
use POS\Repositories\SessionRepository;
use Response;
use stdClass;

class TransactionsController extends Controller
{
    public function getLog()
    {
        if (request()->ajax()) {
            if (request()->has('start_date')) {
                return $this->getLogsFor(request('start_date'), request('end_date'));
            }

            return Response::json([]);
        }

        return view('logs.index');
    }

    public function getLogsFor($startDate, $endDate)
    {
        $references = DB::table('postgl')
            ->distinct()
            ->select(['reference'])
            ->where('reference', 'like', '%TPL%')
            ->where('TXDate', '>=', Carbon::parse($startDate)->startOfDay())
            ->where('TXDate', '<=', Carbon::parse($endDate)->endOfDay())
            ->get()
            ->map(function ($item) {
                preg_match('/TPL-([0-9]+)/', $item->reference, $matches);
                if (count($matches) < 2) {
                    return false;
                }

                return $matches[1];
            })
            ->reject(function ($item) {
                return ! $item;
            });

        $references = $references->merge(DB::table('invnum')
            ->distinct()
            ->select(['OrderNum'])
            ->where('OrderNum', 'like', '%TPL%')
            ->where('InvDate', '>=', Carbon::parse($startDate)->startOfDay())
            ->where('InvDate', '<=', Carbon::parse($endDate)->endOfDay())
            ->get()
            ->map(function ($item) {
                preg_match('/TPL-([0-9]+)/', $item->OrderNum, $matches);
                if (count($matches) < 2) {
                    return false;
                }

                return $matches[1];
            })
            ->reject(function ($item) {
                return ! $item;
            })
            ->toArray());

        $references = $references->unique()->toArray();

        $transactions = Transactions::where('created_at', '>=', Carbon::parse($startDate)->startOfDay())
            ->where('created_at', '<=', Carbon::parse($endDate)->endOfDay())
            ->whereNotIn('id', $references)
            ->whereNotIn('transaction_type', [
                Transactions::TILL_OPEN,
                Transactions::TILL_CLOSE,
                Transactions::DISAPPROVED,
                Transactions::SAVED_SALE
            ])
            ->get()
            ->reject(function ($transaction) use ($references) {
                return in_array($transaction->original_transaction_id, $references);
            })
            ->map(function ($transaction) {
                $transaction->client = '';
                $saleData = json_decode($transaction->transaction_data);
                if (! $saleData) {
                    return $transaction;
                }
                if (property_exists($saleData, 'customer')) {
                    if (isset($saleData->customer->Name)) {
                        $transaction->client = $saleData->customer->Name;
                    }
                }
                if (property_exists($saleData, 'client')) {
                    $transaction->client = $saleData->client->Name;
                }
                return $transaction;
            });
        $running = 0;

        foreach ($transactions as $transaction) {
            if ($transaction->transaction_type == Transactions::TILL_OPEN ||
                $transaction->transaction_type == Transactions::SALE ||
                $transaction->transaction_type == Transactions::CASH_RECEIPT
            ) {
                $running += $transaction->amount;
                $transaction->running = $running;
                continue;
            }

            if (starts_with($transaction->transaction_type, Transactions::PETTY_CASH)) {
                $running += $transaction->amount;
                $transaction->running = $running;
                continue;
            }

            if ($transaction->transaction_type == Transactions::REVERSAL) {
                $transactionData = json_decode($transaction->transaction_data);
                $transaction->amount = $transaction->amount * -1;
                foreach ($transactionData->payments as $payment) {
                    if ($payment->name == 'Cash') {
                        $running += $transaction->amount;
                        $transaction->running = $running;
                        break;
                    }
                }
                continue;
            }

            $transaction->running = $running;
        }

        return Response::json($transactions);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            if (request()->has('start_date')) {
                return $this->getTransactions(request('start_date'), request('end_date'));
            }

            if (request()->has('number')) {
                return $this->getTransactions(null, null, request('number'));
            }

            return $this->getTransactions(Carbon::now(), Carbon::now());
        }

        return view('transactions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transactions::findOrFail($id)->delete();

        return $this->getTransactions(Carbon::now(), Carbon::now());
    }

    /**
     * @param null $startDate
     * @param null $endDate
     * @param null $number
     *
     * @return \Illuminate\Http\JsonResponse
     * @internal param null $day
     */
    private function getTransactions($startDate = null, $endDate = null, $number = null)
    {
        $transactions = Transactions::where('user_id', Auth::id())
            ->where('warehouse_id', SessionRepository::getWarehouseID())
            ->when($startDate, function ($query) use ($startDate) {
                return $query->where('created_at', '>=', Carbon::parse($startDate)->startOfDay());
            })
            ->when($endDate, function ($query) use ($endDate) {
                return $query->where('created_at', '<=', Carbon::parse($endDate)->endOfDay());
            })
            ->when($number, function ($query) use ($number) {
                return $query->where('transaction_number', 'LIKE', '%' . $number . '%');
            })
            ->get()
            ->map(function ($transaction) {
                $transaction->client = '';
                $saleData = json_decode($transaction->transaction_data);
                if (! $saleData) {
                    return $transaction;
                }
                if (property_exists($saleData, 'customer')) {
                    if (isset($saleData->customer->Name)) {
                        $transaction->client = $saleData->customer->Name;
                    }
                }
                if (property_exists($saleData, 'client')) {
                    $transaction->client = $saleData->client->Name;
                }
                return $transaction;
            });
        $running = 0;

        foreach ($transactions as $transaction) {
            if ($transaction->transaction_type == Transactions::TILL_OPEN ||
                $transaction->transaction_type == Transactions::SALE ||
                $transaction->transaction_type == Transactions::CASH_RECEIPT
            ) {
                $running += $transaction->amount;
                $transaction->running = $running;
                continue;
            }

            if (starts_with($transaction->transaction_type, Transactions::PETTY_CASH)) {
                $running += $transaction->amount;
                $transaction->running = $running;
                continue;
            }

            if ($transaction->transaction_type == Transactions::REVERSAL) {
                $transactionData = json_decode($transaction->transaction_data);
                $transaction->amount = $transaction->amount * -1;
                foreach ($transactionData->payments as $payment) {
                    if ($payment->name == 'Cash') {
                        $running += $transaction->amount;
                        $transaction->running = $running;
                        break;
                    }
                }
                continue;
            }

            $transaction->running = $running;
        }

        return Response::json($transactions);
    }

    public function getEndDay()
    {
        $warehouses = DB::table('transactions')->select(DB::raw('distinct warehouse_id'))
            ->where('created_at', '>=', Carbon::now()->startOfDay())
            ->where('user_id', Auth::id())
            ->get()
            ->map(function ($value) {
                return $value->warehouse_id;
            })->toArray();

        $assignments = CacheRepository::getAssignments()
            ->where('user_id', Auth::id())
            ->whereIn('warehouse_id', $warehouses);

        return view('session.endday')->withAssignments($assignments);
    }

    public function endDay(Request $request)
    {
        return $this->processEndDay(Carbon::now(), Auth::id(), $request->get('warehouse_id'));
    }

    public function getAdminEndDay()
    {
        return view('admin-endDay.endday')
            ->with('users', User::where('id', '>', 1)->get(['id', 'name']))
            ->with('warehouses', Warehouse::all(['WhseLink', 'Name']))
            ->with('assignment', Assignment::all());
    }

    public function getAdminCompleteEndDay()
    {
        return view('admin-complete-endDay.endday');
    }

    public function adminEndDay(Request $request)
    {
        return $this->processEndDay(
            Carbon::parse($request->get('date')),
            $request->get('user_id'),
            $request->get('warehouse_id'),
            '/admin-end-of-day'
        );
    }

    public function adminCompleteEndDay(Request $request)
    {
        $nonMonetary = [
            Transactions::TILL_OPEN, Transactions::TILL_CLOSE, Transactions::SAVED_SALE, Transactions::APPROVAL,
            Transactions::APPROVED, Transactions::DISAPPROVED, Transactions::TILL_CLOSE, Transactions::TILL_CLOSE,
            Transactions::SALES_ORDER
        ];

        $transactions = Transactions::whereNotIn('transaction_type', $nonMonetary)
            ->where('created_at', '>=', Carbon::parse($request->get('date'))->startOfDay())
            ->where('created_at', '<=', Carbon::parse($request->get('date'))->endOfDay())
            ->orderBy('id')
            ->get();

        $totalTransactions = count($transactions);

        if (! $totalTransactions) {
            Flash::warning('End of Day', 'Sorry, there are no transactions for the selected date.');

            return redirect()->back();
        }

        $pettyCashTypes = PettyCashType::all(['name'])->map(function ($type) {
            return $type->name;
        })->toArray();

        $endOfDayDetails = [];
        $endOfDayDetails['openingAmount'] = null;
        $endOfDayDetails['closingAmount'] = 0;
        $endOfDayDetails['salesMade'] = 0;
        $endOfDayDetails['totalSalesAmount'] = 0;
        $endOfDayDetails['totalDiscount'] = 0;
        $endOfDayDetails['totalCash'] = 0;
        $endOfDayDetails['totalCredit'] = 0;
        $endOfDayDetails['totalMpesa'] = 0;
        $endOfDayDetails['totalReturns'] = 0;
        $endOfDayDetails['totalReturnsAmount'] = 0;
        $endOfDayDetails['totalCashReturnsAmount'] = 0;
        $endOfDayDetails['totalCreditReturnsAmount'] = 0;
        $endOfDayDetails['totalCashReceipts'] = 0;
        $endOfDayDetails['totalMpesaReceipts'] = 0;
        $endOfDayDetails['pettyCashAmount'] = 0;
        $endOfDayDetails['mpesaTransactions'] = [];
        $endOfDayDetails['cashSaleReturns'] = [];
        $endOfDayDetails['pettyCashTransactions'] = [];
        $endOfDayDetails['cashReceipts'] = [];
        $endOfDayDetails['mpesaReceipts'] = [];
        $endOfDayDetails['pettyCash'] = [];

        foreach ($transactions as $transaction) {
            switch ($transaction->transaction_type) {
                case Transactions::SALE:
                    $endOfDayDetails = $this->getSaleDetails($transaction, $endOfDayDetails);
                    continue;
                    break;
                case Transactions::REVERSAL:
                    $endOfDayDetails = $this->getReversalDetails($transaction, $endOfDayDetails);
                    continue;
                    break;
                case Transactions::CASH_RECEIPT:
                    $endOfDayDetails = $this->getReceiptDetails($transaction, $endOfDayDetails);
                    continue;
                    break;
                default:
                    break;
            }

            if (starts_with($transaction->transaction_type, Transactions::PETTY_CASH)) {
                $endOfDayDetails = $this->getPettyCashDetails($transaction, $endOfDayDetails, $pettyCashTypes);
                continue;
            }
        }

        $companyDetails = new stdClass();
        $companyDetails->name = 'Top Line Limited';
        $companyDetails->postal_address = 'P.O. Box 112 - 00217, Limuru';
        $companyDetails->telephone = '+254 703 799 568';
        $companyDetails->pin = 'P051139461F';
        $companyDetails->vat = '0120331R';

        return view('reports.complete-endday')
            ->with('day', Carbon::parse($request->get('date')))
            ->with('company', $companyDetails)
            ->with('endOfDayDetails', $endOfDayDetails);
    }

    private function processEndDay(Carbon $date, $userId, $warehouseId, $redirection = '/endday')
    {
        $transactions = Transactions::where('created_at', '>=', Carbon::parse($date)->startOfDay())
            ->where('created_at', '<=', Carbon::parse($date)->endOfDay())
            ->where('user_id', $userId)
            ->where('warehouse_id', $warehouseId)
            ->orderBy('id')
            ->get();

        $totalTransactions = count($transactions);

        if (! $totalTransactions) {
            Flash::warning('End of Day', 'Sorry, The selected store has no transactions for the agent.');

            return redirect($redirection);
        }


        if ($transactions->last()->transaction_type != Transactions::TILL_CLOSE) {
            Flash::warning('End of Day', 'Sorry, Please close the till first to get the end of day report.');

            return redirect($redirection);
        }

        $pettyCashTypes = PettyCashType::all(['name'])->map(function ($type) {
            return $type->name;
        })->toArray();

        $endOfDayDetails = [];
        $endOfDayDetails['openingAmount'] = null;
        $endOfDayDetails['closingAmount'] = 0;
        $endOfDayDetails['salesMade'] = 0;
        $endOfDayDetails['totalSalesAmount'] = 0;
        $endOfDayDetails['totalDiscount'] = 0;
        $endOfDayDetails['totalCash'] = 0;
        $endOfDayDetails['totalCredit'] = 0;
        $endOfDayDetails['totalMpesa'] = 0;
        $endOfDayDetails['totalReturns'] = 0;
        $endOfDayDetails['totalReturnsAmount'] = 0;
        $endOfDayDetails['totalCashReturnsAmount'] = 0;
        $endOfDayDetails['totalCreditReturnsAmount'] = 0;
        $endOfDayDetails['totalCashReceipts'] = 0;
        $endOfDayDetails['totalMpesaReceipts'] = 0;
        $endOfDayDetails['pettyCashAmount'] = 0;
        $endOfDayDetails['mpesaTransactions'] = [];
        $endOfDayDetails['cashSaleReturns'] = [];
        $endOfDayDetails['pettyCashTransactions'] = [];
        $endOfDayDetails['cashReceipts'] = [];
        $endOfDayDetails['mpesaReceipts'] = [];
        $endOfDayDetails['pettyCash'] = [];

        foreach ($transactions as $transaction) {
            switch ($transaction->transaction_type) {
                case Transactions::TILL_OPEN:
                    $endOfDayDetails['openingAmount'] = $this->getOpeningAmount($transaction, $endOfDayDetails);
                    continue;
                    break;
                case Transactions::TILL_CLOSE:
                    $endOfDayDetails['closingAmount'] = $transaction->amount;
                    continue;
                    break;
                case Transactions::SALE:
                    $endOfDayDetails = $this->getSaleDetails($transaction, $endOfDayDetails);
                    continue;
                    break;
                case Transactions::REVERSAL:
                    $endOfDayDetails = $this->getReversalDetails($transaction, $endOfDayDetails);
                    continue;
                    break;
                case Transactions::CASH_RECEIPT:
                    $endOfDayDetails = $this->getReceiptDetails($transaction, $endOfDayDetails);
                    continue;
                    break;
                default:
                    break;
            }

            if (starts_with($transaction->transaction_type, Transactions::PETTY_CASH)) {
                $endOfDayDetails = $this->getPettyCashDetails($transaction, $endOfDayDetails, $pettyCashTypes);
                continue;
            }
        }

        $endOfDayDetails['expectedClosing'] =
            (
                $endOfDayDetails['totalSalesAmount'] + $endOfDayDetails['totalCashReceipts'] +
                $endOfDayDetails['totalMpesaReceipts'] + $endOfDayDetails['openingAmount']
            ) -
            (
                $endOfDayDetails['pettyCashAmount'] + $endOfDayDetails['totalCashReturnsAmount'] +
                $endOfDayDetails['totalMpesa'] + $endOfDayDetails['totalCredit']
            );

        $companyDetails = new stdClass();
        $companyDetails->name = 'Top Line Limited';
        $companyDetails->postal_address = 'P.O. Box 112 - 00217, Limuru';
        $companyDetails->telephone = '+254 703 799 568';
        $companyDetails->pin = 'P051139461F';
        $companyDetails->vat = '0120331R';

        return view('reports.endday')
            ->with('day', Carbon::parse($date))
            ->with('company', $companyDetails)
            ->with('agent', User::find($userId))
            ->with('endOfDayDetails', $endOfDayDetails);
    }

    private function getOpeningAmount($transaction, $endOfDayDetails)
    {
        if ($endOfDayDetails['openingAmount']) {
            return $endOfDayDetails['openingAmount'];
        }

        return $transaction->amount;
    }

    private function getSaleDetails($transaction, $endOfDayDetails)
    {
        $saleDetails = json_decode($transaction->transaction_data);
        $endOfDayDetails['salesMade']++;
        $endOfDayDetails['totalSalesAmount'] += $saleDetails->netPrice;
        $endOfDayDetails['totalDiscount'] += $saleDetails->totalDiscount;

        $paymentAmount = 0;
        $cashAmount = 0;

        foreach ($saleDetails->payments as $payment) {
            $paymentAmount += $payment->amount;

            if ($payment->name == 'Credit') {
                $endOfDayDetails['totalCredit'] += $payment->amount;
                continue;
            }

            if ($payment->name == 'MPesa') {
                $endOfDayDetails['totalMpesa'] += $payment->amount;
                try {
                    $mpesaTrx = json_decode($payment->reference);
                    if (! $mpesaTrx) {
                        $mpesaTrx = [$payment];
                    }
                } catch (Exception $ex) {
                    $mpesaTrx = [$payment];
                }

                $endOfDayDetails['mpesaTransactions'] = array_merge($mpesaTrx, $endOfDayDetails['mpesaTransactions'] );
                continue;
            }

            $cashAmount = $payment->amount;
        }

        if ($paymentAmount > $saleDetails->netPrice) {
            $cashAmount = $cashAmount - ($paymentAmount - $saleDetails->netPrice);
        }

        $endOfDayDetails['totalCash'] += $cashAmount;

        return $endOfDayDetails;
    }

    private function getReversalDetails($transaction, $endOfDayDetails)
    {
        $saleDetails = json_decode($transaction->transaction_data);
        $endOfDayDetails['totalReturns']++;
        $endOfDayDetails['totalReturnsAmount'] += $saleDetails->netPrice;
        $endOfDayDetails['cashSaleReturns'] [] = [
            'transaction_number' => $transaction->transaction_number,
            'amount' => $transaction->amount
        ];

        foreach ($saleDetails->payments as $payment) {
            if ($payment->name == 'Credit') {
                $endOfDayDetails['totalCreditReturnsAmount'] += $payment->amount;
                continue;
            }

            if ($payment->name == 'MPesa') {
                continue;
            }

            $endOfDayDetails['totalCashReturnsAmount'] += $payment->amount;
        }

        return $endOfDayDetails;
    }

    private function getPettyCashDetails($transaction, $endOfDayDetails, $pettyCashTypes)
    {
        foreach ($pettyCashTypes as $type) {
            if (strpos(strtolower($transaction->transaction_type), strtolower($type))) {
                if (! isset($endOfDayDetails['pettyCash'][$type])) {
                    $endOfDayDetails['pettyCash'][$type] = 0;
                }
                $endOfDayDetails['pettyCash'][$type] += $transaction->amount * -1;
                break;
            }
        }

        $endOfDayDetails['pettyCashAmount'] += $transaction->amount * -1;
        $transactionDetails = json_decode($transaction->transaction_data);



        $endOfDayDetails['pettyCashTransactions'] [] = [
            'type' => substr($transaction->transaction_type, 12),
            'amount' => $transaction->amount * -1,
            'reference' => $transactionDetails->reference
        ];

        return $endOfDayDetails;
    }

    private function getReceiptDetails($transaction, $endOfDayDetails)
    {
        $transactionDetails = json_decode($transaction->transaction_data);
        if (strtoupper($transactionDetails->type) == 'CASH') {
            $endOfDayDetails['cashReceipts'][] = [
                'client' => $transactionDetails->client->Name,
                'amount' => $transaction->amount
            ];
            $endOfDayDetails['totalCashReceipts'] += $transaction->amount;

            return $endOfDayDetails;
        }

        $endOfDayDetails['mpesaReceipts'][] = [
            'client' => $transactionDetails->client->Name,
            'amount' => $transaction->amount
        ];
        $endOfDayDetails['totalMpesaReceipts'] += $transaction->amount;

        return $endOfDayDetails;
    }
}


/******** Version 1 *******************
 *
 *
private function processEndDay(Carbon $date, $userId, $warehouseId, $redirection = '/endday')
{
$transactions = Transactions::where('created_at', '>=', Carbon::parse($date)->startOfDay())
->where('created_at', '<=', Carbon::parse($date)->endOfDay())
->where('user_id', $userId)
->where('warehouse_id', $warehouseId)
->orderBy('id')
->get();

$totalTransactions = count($transactions);

if (! $totalTransactions) {
Flash::warning('End of Day', 'Sorry, The selected store has no transactions for the agent.');

return redirect($redirection);
}


if ($transactions->last()->transaction_type != Transactions::TILL_CLOSE) {
Flash::warning('End of Day', 'Sorry, Please close the till first to get the end of day report.');

return redirect($redirection);
}

$endOfDayDetails = [];
$endOfDayDetails['openingAmount'] = null;
$endOfDayDetails['closingAmount'] = 0;
$endOfDayDetails['salesMade'] = 0;
$endOfDayDetails['totalSalesAmount'] = 0;
$endOfDayDetails['totalDiscount'] = 0;
$endOfDayDetails['totalCash'] = 0;
$endOfDayDetails['totalCredit'] = 0;
$endOfDayDetails['totalMpesa'] = 0;
$endOfDayDetails['totalReturns'] = 0;
$endOfDayDetails['totalReturnsAmount'] = 0;
$endOfDayDetails['totalCashReturnsAmount'] = 0;
$endOfDayDetails['totalCreditReturnsAmount'] = 0;
$endOfDayDetails['totalCashReceipts'] = 0;
$endOfDayDetails['pettyCashAmount'] = 0;
$endOfDayDetails['mpesaTransactions'] = [];
$endOfDayDetails['cashSaleReturns'] = [];
$endOfDayDetails['pettyCashTransactions'] = [];
$endOfDayDetails['cashReceipts'] = [];

foreach ($transactions as $transaction) {
switch ($transaction->transaction_type) {
case Transactions::TILL_OPEN:
$endOfDayDetails['openingAmount'] = $this->getOpeningAmount($transaction, $endOfDayDetails);
continue;
break;
case Transactions::TILL_CLOSE:
$endOfDayDetails['closingAmount'] = $transaction->amount;
continue;
break;
case Transactions::SALE:
$endOfDayDetails = $this->getSaleDetails($transaction, $endOfDayDetails);
continue;
break;
case Transactions::REVERSAL:
$endOfDayDetails = $this->getReversalDetails($transaction, $endOfDayDetails);
continue;
break;
case Transactions::CASH_RECEIPT:
$endOfDayDetails = $this->getReceiptDetails($transaction, $endOfDayDetails);
continue;
break;
default:
break;
}

if (starts_with($transaction->transaction_type, Transactions::PETTY_CASH)) {
$endOfDayDetails = $this->getPettyCashDetails($transaction, $endOfDayDetails);
continue;
}
}

$endOfDayDetails['expectedClosing'] = ($endOfDayDetails['totalCash'] + $endOfDayDetails['totalCashReceipts'] + $endOfDayDetails['openingAmount'])
- ($endOfDayDetails['pettyCashAmount'] + $endOfDayDetails['totalCashReturnsAmount']);

$companyDetails = new stdClass();
$companyDetails->name = 'Top Line Limited';
$companyDetails->postal_address = 'P.O. Box 112 - 00217, Limuru';
$companyDetails->telephone = '+254 703 799 568';
$companyDetails->pin = 'P051139461F';
$companyDetails->vat = '0120331R';

return view('reports.endday')
->with('day', Carbon::parse($date))
->with('company', $companyDetails)
->with('agent', User::find($userId))
->with('endOfDayDetails', $endOfDayDetails);
}

private function getOpeningAmount($transaction, $endOfDayDetails)
{
if ($endOfDayDetails['openingAmount']) {
return $endOfDayDetails['openingAmount'];
}

return $transaction->amount;
}

private function getSaleDetails($transaction, $endOfDayDetails)
{
$saleDetails = json_decode($transaction->transaction_data);
$endOfDayDetails['salesMade']++;
$endOfDayDetails['totalSalesAmount'] += $saleDetails->netPrice;
$endOfDayDetails['totalDiscount'] += $saleDetails->totalDiscount;

$paymentAmount = 0;
$cashAmount = 0;

foreach ($saleDetails->payments as $payment) {
$paymentAmount += $payment->amount;

if ($payment->name == 'Credit') {
$endOfDayDetails['totalCredit'] += $payment->amount;
continue;
}

if ($payment->name == 'MPesa') {
$endOfDayDetails['totalMpesa'] += $payment->amount;
$endOfDayDetails['mpesaTransactions'] [] = $payment;
continue;
}

$cashAmount = $payment->amount;
}

if ($paymentAmount > $saleDetails->netPrice) {
$cashAmount = $cashAmount - ($paymentAmount - $saleDetails->netPrice);
}

$endOfDayDetails['totalCash'] += $cashAmount;

return $endOfDayDetails;
}

private function getReversalDetails($transaction, $endOfDayDetails)
{
$saleDetails = json_decode($transaction->transaction_data);
$endOfDayDetails['totalReturns']++;
$endOfDayDetails['totalReturnsAmount'] += $saleDetails->netPrice;
$endOfDayDetails['cashSaleReturns'] [] = [
'transaction_number' => $transaction->transaction_number,
'amount' => $transaction->amount
];

foreach ($saleDetails->payments as $payment) {
if ($payment->name == 'Credit') {
$endOfDayDetails['totalCreditReturnsAmount'] += $payment->amount;
continue;
}

if ($payment->name == 'MPesa') {
continue;
}

$endOfDayDetails['totalCashReturnsAmount'] += $payment->amount;
}

return $endOfDayDetails;
}

private function getPettyCashDetails($transaction, $endOfDayDetails)
{
$endOfDayDetails['pettyCashAmount'] += $transaction->amount * -1;
$transactionDetails = json_decode($transaction->transaction_data);

$endOfDayDetails['pettyCashTransactions'] [] = [
'type' => substr($transaction->transaction_type, 12),
'amount' => $transaction->amount * -1,
'reference' => $transactionDetails->reference
];

return $endOfDayDetails;
}

private function getReceiptDetails($transaction, $endOfDayDetails)
{
$transactionDetails = json_decode($transaction->transaction_data);
$endOfDayDetails['cashReceipts'][] = [
'client' => $transactionDetails->client->Name,
'amount' => $transaction->amount
];
$endOfDayDetails['totalCashReceipts'] += $transaction->amount;

return $endOfDayDetails;
}
 *
 * **************/
