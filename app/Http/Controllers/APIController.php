<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Managers\SyncManager;
use Response;

class APIController extends Controller
{
    public function getUser(Request $request)
    {
        return $request->user();
    }

    public function getLogPoint()
    {
        return Response::json([
            'currentDate' => Carbon::now()->toDateString(),
            'logIndex' => SyncManager::getLogPoint()
        ]);
    }

    public function fetchCurrentQueries($index)
    {
        return Response::json([
            'currentDate' => Carbon::now()->toDateString(),
            'queries' => SyncManager::getUnprocessedFromIndex(Carbon::now(), $index)
        ]);
    }

    public function pushCurrent()
    {
        SyncManager::pushUnprocessed();
    }

    public function receive(Request $request)
    {
        if (SyncManager::processNew(json_decode($request->get('queries')))) {
            return Response::json([
                'status' => 1
            ]);
        }

        return Response::json([
            'status' => 0
        ]);
    }

    public function lastSync()
    {

    }

    public static function fetchQueriesForDate(Request $request)
    {

    }

}
