<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\Assignment;
use POS\Models\Warehouse;
use POS\Repositories\CacheRepository;
use Response;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', new Assignment);
        if (request()->ajax()) {
            if (request()->has('users')) {
                return Response::json(User::where('id', '>', 1)->get(['id', 'name']));
            }
            if (request()->has('warehouses')) {
                return Response::json(Warehouse::all(['WhseLink', 'Name']));
            }

            return Response::json($this->getSelected(), 200, [], JSON_FORCE_OBJECT);
        }

        return view('assignment.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rows = [];

        foreach ($request->get('user_id') as $user) {
            foreach ($request->get('warehouse_id') as $warehouse) {
                $rows [] = [
                    'user_id'      => $user,
                    'warehouse_id' => $warehouse
                ];
            }
        }

        Assignment::insert($rows);

        CacheRepository::cacheAssignments();

        return Response::json($this->getSelected(), 200, [], JSON_FORCE_OBJECT);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Assignment $assignment
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        $assignment->delete();

        CacheRepository::cacheAssignments();

        return Response::json($this->getSelected(), 200, [], JSON_FORCE_OBJECT);
    }

    private function getSelected()
    {
        return Assignment::with(['user' => function ($query) {
                $query->select(['id', 'name']);
        }, 'warehouse' => function ($query) {
            $query->select(['WhseLink', 'Name']);
        }])->paginate(10);
    }
}
