<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\OpenSession;
use POS\Models\Transactions;
use POS\Models\Warehouse;
use POS\Repositories\CacheRepository;
use POS\Repositories\SessionRepository;

class CheckinController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $notClosed = OpenSession::whereOpen(true)
            ->whereUserId(Auth::id())
            ->where('created_at', '<', Carbon::now()->startOfDay())
            ->get();

        if (count($notClosed) > 0) {
            return redirect()->route('check-in.edit', $notClosed->first()->warehouse_id);
        }

        $assignments = CacheRepository::getAssignments()
            ->where('user_id', Auth::id());

//        if (count($assignments) == 1) {
//            return $this->checkIn($assignments->first()->warehouse_id);
//        }

        if (count($assignments) < 1) {
            Flash::error('Assignment', 'Sorry, you have not been assigned any till.')->persist();
        }

        return view('session.checkin', compact('assignments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->checkIn($request->get('warehouse_id'));
    }

    private function checkIn($warehouse)
    {
        $existing = OpenSession::whereOpen(true)
            ->whereUserId(Auth::id())
            ->whereWarehouseId($warehouse)
            ->firstOrNew([]);

        $existing->fill([
            'user_id' => Auth::id(),
            'warehouse_id' => $warehouse,
            'open' => true,
            'opened_at' => Carbon::now()
        ]);

        if (! $existing->id) {
            return redirect()->route('till.create', ['sel' => $warehouse]);
        }

        SessionRepository::setWarehouseID($warehouse);

        $existing->save();

        return redirect()->route('pos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warehouse = Warehouse::select(['WhseLink', 'Name'])->findOrFail($id);

        return view('session.checkout')->withSelected($warehouse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session = OpenSession::whereOpen(true)
            ->whereUserId(Auth::id())
            ->whereWarehouseId($id)
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('created_at', '>=', Carbon::now()->startOfDay())
                        ->where('created_at', '<=', Carbon::now()->endOfDay());
                })->orWhere(function ($query) {
                    $query->where('created_at', '>=', Carbon::now()->subDay()->startOfDay())
                        ->where('created_at', '<=', Carbon::now()->subDay()->endOfDay());
                })->orWhere('created_at', '<', Carbon::now());
            })
            ->orderBy('created_at', 'desc')
            ->firstOrFail();

        $session->open = false;
        $session->save();

        $closingTime = Carbon::parse($session->created_at)->endOfDay();

        $data = [
            'user_id' => Auth::id(),
            'warehouse_id' => $id,
            'transaction_type' => Transactions::TILL_CLOSE,
            'amount' => request()->get('opening_amount'),
            'payment_type' => 'Cash',
            'status' => Transactions::STATUS_COMPLETED,
            'created_at' => $closingTime,
            'updated_at' => $closingTime
        ];

        $transaction = Transactions::create($data);
        $transaction->transaction_number = '#' . config('pos.site_id', 'TR') . '-' . $transaction->id;
        $transaction->save();

        SessionRepository::removeWarehouseID();

        return redirect()->route('pos.index');
    }
}
