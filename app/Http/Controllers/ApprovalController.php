<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\Transactions;
use POS\Repositories\CacheRepository;
use Response;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('approve', new Transactions);
        if (request()->ajax()) {
            return $this->getTransactions();
        }

        return view('approvals.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getTransactions()
    {
        return Response::json(Transactions::with(['warehouse' => function ($query) {
            $query->select('WhseLink','Name');
        },'creator' => function ($query) {
            $query->select('name', 'id');
        }])
            ->whereIn('warehouse_id', CacheRepository::getMyAssignedIds())
            ->where('status', 'LIKE', Transactions::STATUS_PENDING)
            ->where('transaction_type', 'LIKE', Transactions::APPROVAL . '%')
            ->get()
            ->map(function ($transaction) {
                $transaction->client = '';
                $transaction->warehouseName = $transaction->warehouse->Name;
                unset($transaction->warehouse);
                $saleData = json_decode($transaction->transaction_data);
                if (! $saleData) {
                    return $transaction;
                }
                if (property_exists($saleData, 'customer')) {
                    if (isset($saleData->customer->Name)) {
                        $transaction->client = $saleData->customer->Name;
                    }
                }
                if (property_exists($saleData, 'client')) {
                    $transaction->client = $saleData->client->Name;
                }

                return $transaction;
            })
        );
    }
}
