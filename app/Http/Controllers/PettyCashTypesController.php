<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use POS\Models\Account;
use POS\Models\PettyCashType;
use Response;

class PettyCashTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {

        if (request()->ajax()) {
            if (request()->has('accounts')) {
                return Response::json(Account::orderBy('Description')->get([
                    'AccountLink', 'Description'
                ]));
            }
            if (request()->has('batches')) {
                return Response::json(DB::table('_btblCbBatches')->get(['idBatches', 'cBatchNo', 'cBatchDesc']));
            }

            return $this->getPettyCash();
        }

        $this->authorize('view', new PettyCashType);

        return view('petty-cash.types.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['module_id'] = 0;
        PettyCashType::create($data);

        return $this->getPettyCash();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PettyCashType::findOrFail($id)->fill($request->all())->save();

        return $this->getPettyCash();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PettyCashType::findOrFail($id)->delete();

        return $this->getPettyCash();
    }

    private function getPettyCash()
    {
        $petty = PettyCashType::with(['account' => function ($query) {
            $query->select(['AccountLink', 'Description']);
        }])->get([
            'id', 'module_id', 'name', 'transaction_type', 'account_id', 'cashbook_batch_id',
            'transaction_type'
        ]);

        return Response::json($petty->toArray(), 200, [], JSON_FORCE_OBJECT);
    }
}
