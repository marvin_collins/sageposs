<?php

namespace App\Http\Controllers;

use App\Events\ApprovalCompleted;
use App\Events\ApprovalRequestReviewed;
use App\Events\ApprovalRequestSent;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Picqer\Barcode\BarcodeGeneratorPNG;
use POS\Models\Transactions;
use POS\Repositories\CacheRepository;
use POS\Repositories\ClientRepository;
use POS\Repositories\InvoiceRepository;
use POS\Repositories\ProjectRepository;
use POS\Repositories\SessionRepository;
use POS\Repositories\StockRepository;
use Response;
use stdClass;

class POSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return Response::json([
                'products' => StockRepository::getItemsWithRates(),
                'clients' => ClientRepository::getAll(),
                'projects' => ProjectRepository::getAllProjects()
            ]);
        }

        $settings = CacheRepository::getSettings();
        $parsedSettings = [];
        foreach ($settings as $setting) {
            $parsedSettings[$setting->name] = $setting->value;
        }

        return view('pos.index')->with('settings', $parsedSettings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = InvoiceRepository::makeInvoice($request->all(), $request->get('id'));

//        $transaction = Transactions::whereUserId(Auth::id())->orderBy('id', 'desc')->first();

        $content = $this->printReceipt($transaction->id, 'reports.receipt', $transaction)->render();

        return json_encode(['status' => 'success', 'id' => $transaction->id, 'content' => $content]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transactions::findOrFail($id);

        return Response::json(json_decode($transaction->transaction_data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        InvoiceRepository::makeCreditNote($request->all(), $id);

        $content = $this->printReceipt($id, 'reports.receipt')->render();

        return Response::json(['status' => 'success', 'content' => $content]);
    }

    public function save(Request $request)
    {
        $data = [
            'user_id' => Auth::id(),
            'warehouse_id' => SessionRepository::getWarehouseID(),
            'invoice_id' => null,
            'cashbook_id' => null,
            'transaction_type' => Transactions::SAVED_SALE,
            'amount' => 0,
            'payment_type' => '',
            'transaction_data' => json_encode($request->all()),
            'status' => Transactions::STATUS_SAVED
        ];

        Transactions::updateIfOld($data, $request->get('id'));

        return Response::json(['success']);
    }

    public function approval(Request $request)
    {
        $data = [
            'user_id' => Auth::id(),
            'warehouse_id' => SessionRepository::getWarehouseID(),
            'invoice_id' => null,
            'cashbook_id' => null,
            'transaction_type' => Transactions::APPROVAL . ': ' .$request->get('reason'),
            'original_transaction_id' => $request->get('original_id'),
            'amount' => $request->get('netPrice'),
            'payment_type' => '',
            'transaction_data' => json_encode($request->all()),
            'status' => Transactions::STATUS_PENDING,
            'locked' => false
        ];

        $transaction = Transactions::updateIfOld($data, $request->get('id'));
        $transaction->update([
            'transaction_number' => '#' . config('pos.site_id', 'TR') . '-' . $transaction->id
        ]);

        event(new ApprovalRequestSent($transaction));

        return Response::json($transaction->toArray());
    }

    public function process(Request $request)
    {
        $transaction = Transactions::findOrFail($request->get('id'));
        $reason = explode(': ', $transaction->transaction_type)[1];
        $type = $request->get('approval') == true ? Transactions::APPROVED . ': ' . $reason : Transactions::DISAPPROVED . ': ' . $reason;
        $status = $request->get('approval') == true ? Transactions::STATUS_APPROVED : Transactions::STATUS_PENDING;

        $transaction->fill([
            'amount' => $request->get('netPrice'),
            'transaction_type' => $type,
            'transaction_data' => json_encode($request->all()),
            'status' => $status,
            'locked' => true
        ])->save();

        event(new ApprovalRequestReviewed($transaction));
        event(new ApprovalCompleted($transaction));

        return Response::json(['success']);
    }

    public function printReceipt($transactionId, $view = 'reports.fullreceipt', $transaction = null)
    {
        if (! $transaction) {
            $transaction = Transactions::with(['creator', 'warehouse'])->find($transactionId);
        }
        $transactionDetails = json_decode($transaction->transaction_data);
        $companyDetails = new stdClass();
        $companyDetails->name = 'Top Line Limited';
        $companyDetails->postal_address = 'P.O. Box 112 - 00217, Limuru';
        $companyDetails->telephone = '+254 703 799 568';
        $companyDetails->pin = 'P051139461F';
        $companyDetails->vat = '0120331R';
        $reversal = $transaction->transaction_type == Transactions::REVERSAL;
        $generator = new BarcodeGeneratorPNG();

        $newTransaction = $transaction->replicate();
        $newTransaction->id = $transaction->id;
        $newTransaction->created_at = $transaction->created_at;
        $newTransaction->updated_at = $transaction->updated_at;
        $newTransaction->id = $transaction->id;
        $transaction->printed = true;
        $transaction->save();

        $mappedPayments = [];

        if (isset($transactionDetails->payments)) {
            foreach ($transactionDetails->payments as $payment) {
                if ($payment->name != 'MPesa') {
                    $mappedPayments[] = $payment;
                    continue;
                }

                $decoded = json_decode($payment->reference);
                if (! $decoded) {
                    $mappedPayments[] = $payment;
                    continue;
                }

                $mappedPayments = array_merge($mappedPayments, array_map(function ($payment) {
                    $payment->name = 'MPesa';

                    return $payment;
                }, $decoded));
            }

            $transactionDetails->payments = $mappedPayments;
        }

        return view($view)
            ->withDetails($transactionDetails)
            ->withCompany($companyDetails)
            ->withReversal($reversal)
            ->withGenerator($generator)
            ->withTransaction($newTransaction);
    }

    public function addCustomer(Request $request)
    {
        $index = DB::table('Client')->get()->last()->DCLink;

        $data['Account'] = strtoupper(substr($request->customerName,0,3).'001' .$index);
        $data['Name'] = $request->customerName;
        $data['Title'] = '';
        $data['Init']  = '';
        $data['Contact_Person'] = '';
        $data['Physical1'] = '';
        $data['Physical2'] = '';
        $data['Physical3'] = '';
        $data['Physical4'] = '';
        $data['Physical5'] = '';
        $data['PhysicalPC'] = '';
        $data['Addressee'] = '';
        $data['Post1'] = '';
        $data['Post2'] = '';
        $data['Post3'] = '';
        $data['Post4'] = '';
        $data['Post5'] = '';
        $data['PostPC'] = '';
        $data['Delivered_To'] = '';
        $data['Telephone'] = $request->customerTelephone;
        $data['Telephone2'] = '';
        $data['Fax1'] = '';
        $data['Fax2'] = '';
        $data['AccountTerms'] = 0;
        $data['CT'] = 1;
        $data['Tax_Number'] = '';
        $data['Registration'] = '';
        $data['Credit_Limit'] = 0;
        $data['RepID'] = 0;
        $data['Interest_Rate'] = 0;
        $data['Discount'] = 0;
        $data['On_Hold'] = 0;
        $data['BFOpenType'] = 1;
        $data['EMail'] = '';
        $data['AutoDisc'] = 0;
        $data['DiscMtrxRow'] = 0;
        $data['CashDebtor'] = 1;
        $data['CheckTerms'] = 1;
        $data['UseEmail'] = 0;
        $data['dTimeStamp'] = Carbon::now()->format('M d Y h:i:s A');
        $data['cAccDescription'] = $request->customerName;
        $data['cWebPage'] = '';
        $data['iClassID'] = 0;
        $data['iAreasID'] = 0;
        $data['iCurrencyID'] = 0;
        $data['bStatPrint'] = 1;
        $data['bStatEmail'] = 0;
        $data['cStatEmailPass'] = '';
        $data['bTaxPrompt'] = 1;
        $data['iARPriceListNameID'] = 1;
        $data['iSettlementTermsID'] = 0;
        $data['bSourceDocPrint'] = 1;
        $data['bSourceDocEmail'] = 0;
        $data['iEUCountryID'] = 0;
        $data['iAgeingTermID'] = 1;
        $data['Client_iBranchID'] = 0;

        $client_id = DB::table('Client')->insertGetId($data);

        return ['customer' => DB::table('Client')->select('DCLink', 'Account', 'Name', 'Telephone', 'EMail', 'Physical1',
            'DCBalance', 'Credit_Limit', 'CashDebtor')->where('DCLink',$client_id)->first()];
    }
}
