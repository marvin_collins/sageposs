<?php

namespace App\Http\Middleware;

use Closure;
use POS\Repositories\SessionRepository;

class SelectedStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! SessionRepository::getWarehouseID()) {
            return redirect()->route('check-in.create');
        }
        return $next($request);
    }
}
