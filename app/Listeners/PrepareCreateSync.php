<?php

namespace App\Listeners;

use App\Events\NewModelCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrepareCreateSync
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewModelCreated  $event
     * @return void
     */
    public function handle(NewModelCreated $event)
    {
        $event->model->save();
    }
}
