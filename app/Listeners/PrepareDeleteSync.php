<?php

namespace App\Listeners;

use App\Events\ExistingModelDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrepareDeleteSync
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExistingModelDeleted  $event
     * @return void
     */
    public function handle(ExistingModelDeleted $event)
    {
        $event->model->save();
    }
}
