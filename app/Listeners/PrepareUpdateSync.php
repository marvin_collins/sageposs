<?php

namespace App\Listeners;

use App\Events\ExistingModelUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrepareUpdateSync
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExistingModelUpdated  $event
     * @return void
     */
    public function handle(ExistingModelUpdated $event)
    {
        $event->model->save();
    }
}
