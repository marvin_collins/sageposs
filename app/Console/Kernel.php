<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use POS\Managers\SyncManager;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->exec('laravel-echo-server start')
//            ->name('echo-server')
//            ->withoutOverlapping()
//            ->everyFiveMinutes();
//
//        $schedule->command('queue:listen --sleep=1')
//            ->name('queue')
//            ->withoutOverlapping()
//            ->everyFiveMinutes();

        $schedule->call(function () {
            SyncManager::pushUnprocessed();
        })
            ->name('sync')
            ->withoutOverlapping()
            ->everyMinute();

        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
