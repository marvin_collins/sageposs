@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Check In</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('till.store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="selected" value="{{ $selected }}">
                        <div class="form-group{{ $errors->has('opening_amount') ? ' has-error' : '' }}">
                            <label for="opening_amount" class="col-md-4 control-label">Opening Amount</label>

                            <div class="col-md-6">
                                <input id="opening_amount" type="number" min="0" class="form-control" name="opening_amount" required>

                                @if ($errors->has('opening_amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('opening_amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Open Till
                                </button>
                                <a href="{{ route('check-in.create') }}"
                                   class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
