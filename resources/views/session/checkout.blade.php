@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Check In</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('check-in.destroy', $selected->WhseLink) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div>
                            <h3>Manage Till: {{ ucwords(strtolower($selected->Name)) }}</h3>
                        </div>
                        <div class="form-group{{ $errors->has('opening_amount') ? ' has-error' : '' }}">
                            <label for="opening_amount" class="col-md-4 control-label">Closing Amount</label>

                            <div class="col-md-6">
                                <input id="opening_amount" type="number" min="0" class="form-control" name="opening_amount" required>

                                @if ($errors->has('opening_amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('opening_amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Close Till
                                </button>
                                <a class="btn btn-warning" href="{{ route('check-in.create') }}">Switch Store</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
