@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Check In</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('check-in.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('warehouse_id') ? ' has-error' : '' }}">
                            <label for="warehouse_id" class="col-md-4 control-label">Select Store</label>

                            <div class="col-md-6">
                                <select name="warehouse_id" id="warehouse_id" class="form-control" required>
                                @foreach($assignments as $assignment)
                                    <option value="{{ $assignment->warehouse_id }}">{{ $assignment->warehouse->Name }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('warehouse_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('warehouse_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="localStorage.removeItem('sale');">
                                    Check In
                                </button>
                                <a href="{{ url('endday') }}"
                                   class="btn btn-success">End Of Day</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
