@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">End Of Day Report</div>
                <div class="panel-body">
                    <form class="form-horizontal" target="_blank" role="form" method="POST" action="{{ url('admin-complete-end-of-day') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-4 control-label">Select Date</label>

                            <div class="col-md-6">
                                <input type="text" id="date" class="form-control" name="date">

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Get Report
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $('#date').datepicker({
                autoclose: true,
                endDate: '0d',
                format: 'dd-mm-yyyy'
            });
            $('#date').datepicker('setDate', new Date());
        });
    </script>
@endsection
