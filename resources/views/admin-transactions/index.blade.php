@extends('layouts.app')

@section('content')
    <transactions-index root="/admin/transactions" :rep="{{ Auth::user()->can('reprint', new POS\Models\Transactions) ? 'true' : 'false' }}" :admin="true"></transactions-index>
@endsection

@section('footer')
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
@endsection