<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset(elixir('css/app.css')) }}" rel="stylesheet">
    <script src="{{ asset('js/socket.js') }}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'canonical' => url('/'),
            'drimbler' => Auth::id(),
            'eEnabled' => config('pos.echo_enabled', false),
            'server' => config('pos.echo_server', '127.0.0.1:6001')
        ]); ?>
    </script>
</head>
<body>
<div class="hidden-print">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if (! Auth::guest())
                        <li><a href="{{ route('pos.index') }}">Sale</a></li>
                        <li><a href="{{ route('receipts.index') }}">Receive Cash</a></li>
                        <li><a href="{{ route('petty-cash.index') }}">Petty Cash</a></li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                More <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu notification-menu" role="menu">
                                <li><a href="{{ route('clients.index') }}">Clients</a></li>
                                <li><a href="{{ route('inventory.index') }}">Inventory</a></li>
                                <li><a href="{{ route('transactions.index') }}">Transactions</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-bell-o"></i><span class="badge" v-show="notifications.length" v-html="notifications.length"></span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu notification-menu" role="menu">
                            <li v-if="notifications.length"><a href="#" @@click="clearNotifications" class="text-right"><span class="btn btn-xs btn-primary">Clear All</span></a></li>
                            <li v-for="notification in notifications"><a href="#" @@click="removeNotification(notification)">@{{ notification.message }}</a></li>
                            <li v-if="! notifications.length"><a href="#">No Notifications</a></li>
                        </ul>
                    </li>
                    @if (! Auth::guest())
                        @if(getCheckedIn() == 'Check In.')
                            <li><a href="{{ route('check-in.create') }}">Check In</a></li>
                        @elseif(! is_null(getCheckedIn()))
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ getCheckedIn()->warehouse->Name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('check-in.edit', \POS\Repositories\SessionRepository::getWarehouseID()) }}">Check Out</a></li>
                                </ul>
                            </li>
                        @endif
                        @if(Auth::user()->can('view', new POS\Models\Role) || Auth::user()->can('view', new App\User) ||
                         Auth::user()->can('view', new POS\Models\Assignment) || Auth::user()->can('approve', new POS\Models\Transactions) ||
                         Auth::user()->can('view', new POS\Models\PettyCashType) || Auth::user()->can('view', new POS\Models\Transactions))
                            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Administration <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::user()->can('view', new POS\Models\Transactions) || Auth::user()->can('approve', new POS\Models\Transactions))
                                <li class="dropdown-header">Transactions</li>
                            @endif
                            @can('view', new POS\Models\Transactions)
                                <li><a href="{{ route('admin.transactions.index') }}">View</a></li>
                            @endcan
                            @can('approve', new POS\Models\Transactions)
                                <li><a href="{{ route('approvals.index') }}">Approval</a></li>
                            @endcan
                            @can('view', new POS\Models\Transactions)
                                <li><a href="{{ route('admin.endofday') }}">End of Day</a></li>
                                <li><a href="{{ url('admin-complete-end-of-day') }}">Full End of Day</a></li>
                                <li><a href="{{ route('admin.log') }}">Log</a></li>
                            @endcan
                            @if(Auth::user()->can('view', new POS\Models\Role) || Auth::user()->can('view', new App\User) || Auth::user()->can('view', new POS\Models\Assignment))
                                <li class="dropdown-header">User Management</li>
                            @endif
                            @can('view', new POS\Models\Role)
                                <li><a href="{{ route('roles.index') }}">User Groups</a></li>
                            @endcan
                            @can('view', new App\User)
                                <li><a href="{{ route('users.index') }}">User Accounts</a></li>
                            @endcan
                            @can('view', new POS\Models\Assignment)
                                <li><a href="{{ route('assignment.index') }}">Warehouse Assignment</a></li>
                            @endcan
                            @if(Auth::user()->can('view', new POS\Models\PettyCashType) || Auth::user()->can('view', new POS\Models\Setting))
                                <li class="dropdown-header">Settings</li>
                            @endif
                            @can('view', new POS\Models\PettyCashType)
                                <li><a href="{{ route('petty-types.index') }}">Petty Cash Types</a></li>
                            @endcan
                            @can('view', new POS\Models\Setting)
                                <li><a href="{{ route('settings.index') }}">System Settings</a></li>
                            @endcan
                        </ul>
                    </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')
</div>
<div id="printout" class="visible-print-block"></div>
<script src="{{ asset(elixir('js/app.js')) }}"></script>
<script src="{{ asset('js/pnotify.custom.min.js') }}"></script>
@include('vendor.smodav.flash.flash')
<script>PNotify.prototype.options.styling = "bootstrap3"; PNotify.desktop.permission();</script>
@if(Auth::check() && config('pos.echo_enabled', false))
    <script>
    @can('approve', new POS\Models\Transactions)
        function notifyRequest(e) {
            window._Notification.play();
            app.notifyUser(e.transaction.id, 'There is a transaction awaiting your approval.', '/approvals');
            (new PNotify({
                title: 'Transaction',
                text: 'There is a transaction awaiting your approval.',
                type: 'info',
                desktop: {
                    desktop: true
                }
            })).get().click(function (e) {
                if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
                window.focus();
                window.location = '/approvals';
            });
        }
        @foreach(POS\Repositories\CacheRepository::getMyAssignedIds() as $assigned)
            var appr = Echo.private('POS.Approver.{{ $assigned }}');
            appr.listen('ApprovalRequestSent', notifyRequest);
            appr.listen('ApprovalCompleted', (e) => {
                app.clearNotification(e.transaction.id);
            });
        @endforeach
    @endcan

        var channel = Echo.private('POS.User.' + Laravel.drimbler);

        channel.listen('ApprovalRequestReviewed', function (e) {
            window._Notification.play();
            app.notifyUser(e.transaction.id, 'Your pending transaction for has been reviewed.', '/transactions');
            PNotify.desktop.permission();
            (new PNotify({
                title: 'Transaction',
                text: 'Your pending transaction for has been reviewed',
                type: 'info',
                desktop: {
                    desktop: true
                }
            })).get().click(function(e) {
                if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
                window.focus();
                window.location = '/transactions';
            });
        });
    </script>
@endif
@yield('footer')
</body>
</html>
