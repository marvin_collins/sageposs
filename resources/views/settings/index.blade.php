@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        System Settings
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('settings.store') }}" method="post">
                            {{ csrf_field() }}
                            @foreach($settings as $setting)
                                <div class="row" style="border-bottom: 1px solid #e5e5e5; margin-top: 5px;">
                                    <div class="col-sm-10">
                                        <strong>{{ $setting->title }}</strong>
                                        <p style="text-align: justify;">{{ $setting->description }}</p>
                                    </div>
                                    <div class="col-sm-2" style="padding-top: 5px;">
                                        @if($setting->value == 'amount' || $setting->value == 'percentage')
                                            <select class="form-control" name="{{ $setting->name }}" id="{{ $setting->name }}">
                                                <option value="amount"{{ $setting->value == 'amount' ? ' selected' : '' }}>Amount</option>
                                                <option value="percentage"{{ $setting->value == 'percentage' ? ' selected' : '' }}>Percentage</option>
                                            </select>
                                        @elseif(is_numeric($setting->value))
                                            <input type="number" min="0" class="form-control" name="{{ $setting->name }}" id="{{ $setting->name }}" value="{{ $setting->value }}">
                                        @elseif($setting->value == 'Yes' || $setting->value == 'No')
                                            <select class="form-control" name="{{ $setting->name }}" id="{{ $setting->name }}">
                                                <option value="Yes"{{ $setting->value == 'Yes' ? ' selected' : '' }}>Yes</option>
                                                <option value="No"{{ $setting->value == 'No' ? ' selected' : '' }}>No</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            @endforeach

                            <div class="row">
                                <br>
                                <br>
                                <div class="col-sm-12">
                                    <input type="submit" class="btn btn-success pull-right" value="Save Settings">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
