<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Receipt</title>
    <link rel="stylesheet" href="{{ asset(elixir('css/app.css')) }}">
    <script>
        window.onload = function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 10);
        }
    </script>
</head>
<body style='background: #fff; font-size: 13px; font-family: monospace;'>

<div style="background: #fff; font-size: 13px; font-family: monospace; width: 100%; padding: 2px;">
    <div class="text-center">{{ strtoupper($company->name) }}</div>
    <div class="text-center">{{ strtoupper($company->postal_address) }}</div>
    <div class="text-center">{{ strtoupper($company->telephone) }}</div>
    <div class="text-center">VAT #:{{ strtoupper($company->vat) }}</div>
    <div class="text-center">PIN: {{ strtoupper($company->pin) }}</div>
    <h4 class="text-center"><strong> DAY SUMMARY </strong></h4>
    <div>{{ $agent->name }}</div>
    <div>DATE: {{ $day->format('d F Y') }}</div>

    <br>
    <div><strong>TRANSACTIONS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>

        <div class="col-xs-8"><strong>OPENING AMOUNT</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['openingAmount'], 2) }}</strong></div>

        <div class="col-xs-8"><strong>CASH SALES</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalCash'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>M-PESA TRANSACTIONS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['mpesaTransactions'] as $payment)
            <div class="col-xs-8">REF: {{ $payment->reference }}</div>
            <div class="col-xs-4 text-right">{{ number_format($payment->amount, 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>MPESA TOTAL</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalMpesa'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>CREDIT CASH RECEIVED</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['cashReceipts'] as $payment)
            <div class="col-xs-8">REF: {{ $payment['client'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($payment['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL RECEIVED</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalCashReceipts'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>CASH SALE RETURNS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['cashSaleReturns'] as $returns)
            <div class="col-xs-8">{{ $returns['transaction_number'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($returns['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL RETURNS</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalReturnsAmount'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>TILL DRAWINGS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['pettyCashTransactions'] as $pettyCash)
            <div class="col-xs-8">{{ $pettyCash['type'] }} - {{ $pettyCash['reference'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($pettyCash['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL DRAWINGS</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['pettyCashAmount'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <br>
    <div><strong>SUMMARY</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>

        <div class="col-xs-8">OPENING AMOUNT</div>
        <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['openingAmount'], 2) }}</div>

        <div class="col-xs-8">CASH SALES</div>
        <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalCash'], 2) }}</div>

        <div class="col-xs-8">CREDIT CASH RECEIVED</div>
        <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalCashReceipts'], 2) }}</div>

        <div class="col-xs-8">LESS RETURNS</div>
        <div class="col-xs-4 text-right">({{ number_format($endOfDayDetails['totalCashReturnsAmount'], 2) }})</div>

        <div class="col-xs-8">LESS TILL DRAWINGS</div>
        <div class="col-xs-4 text-right">({{ number_format($endOfDayDetails['pettyCashAmount'], 2) }})</div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>

        <br>

        <div class="col-xs-8"><strong>EXPECTED CLOSING AMOUNT</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['expectedClosing'], 2) }}</strong></div>

        <div class="col-xs-8"><strong>ACTUAL CLOSING AMOUNT</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['closingAmount'], 2) }}</strong></div>

        <div class="col-xs-8"><strong>DIFFERENCE</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['closingAmount'] - $endOfDayDetails['expectedClosing'], 2) }}</strong></div>
    </div>

</div>
</body>
</html>
