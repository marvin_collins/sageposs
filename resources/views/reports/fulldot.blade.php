<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Receipt</title>
    <link rel="stylesheet" href="{{ asset(elixir('css/app.css')) }}">
    {{--<script>--}}
        {{--window.onload = function () {--}}
            {{--window.print();--}}
            {{--setTimeout(function () {--}}
                {{--window.close();--}}
            {{--}, 10);--}}
        {{--}--}}
    {{--</script>--}}
</head>
<body style='background: #fff; font-family: monospace;'>
    @include('reports.dotprint')
</body>
</html>
