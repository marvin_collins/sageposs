<?php $qty = 0; ?>
@foreach(collect($details->items)->chunk(3) as $group)
    <div style="height: 110mm; margin:30px 5px; width: 100%; background: #fff; font-size: 14px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif, monospace;">
        <?php $outloop = $loop->last; ?>
        <div>
        <div style="width: 7%; display: inline-block;">{{ is_object($details->customer) ? $details->customer->Account : 'C001' }}</div>
        <div style="width: 28%; display: inline-block;">{{ is_object($details->customer) ? $details->customer->Name : 'Cash Customer' }}</div>
        <div style="width: 11%; display: inline-block;">{{ $transaction->created_at->format('d/m/Y') }}</div>
        <div style="width: 13%; display: inline-block;">TR{{ $transaction->id }}</div>
        <div style="width: 13%; display: inline-block;"></div>
        <div style="width: 16%; display: inline-block;"></div>
        <div style="width: 10%; display: inline-block;"></div>
    </div>
        <div style="height: 25mm">
            @foreach($group as $item)
                <?php $qty += $item->quantity; ?>
                <div>
                    <div style="width: 11%; display: inline-block;">{{ $item->Code }}</div>
                    <div style="width: 33%; display: inline-block;">{{ $item->Description_1 }}</div>
                    <div style="width: 5%; display: inline-block;"></div>
                    <div style="width: 5%; display: inline-block; text-align: center">{{ $reversal ? '-' : '' }}{{ $item->quantity }}</div>
                    <div style="width: 7%; display: inline-block;"></div>
                    <div style="width: 10%; display: inline-block; text-align: right">{{ number_format($item->fInclPrice, 2) }}</div>
                    <div style="width: 3%; display: inline-block;"></div>
                    <div style="width: 17%; display: inline-block; text-align: right">{{ number_format(($item->quantity * $item->fInclPrice) - $item->discount, 2) }}</div>
                </div>
            @endforeach
        </div>
        @if($outloop)
            <div>
                <div style="width: 70%; display: inline-block;">
                    <div style="width: 70%;">
                        <div class="col-xs-2"><strong>CODE</strong></div>
                        <div class="col-xs-2 text-right"><strong>RATE</strong></div>
                        <div class="col-xs-4 text-right"><strong>VATABLE</strong></div>
                        <div class="col-xs-4 text-right"><strong>VAT AMT</strong></div>
                        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
                        <?php $taxAmount = 0; ?>
                        @foreach($details->tax as $key => $tax)
                            <?php $taxAmount += $tax->amount; ?>
                            <div class="row">
                                <div class="col-xs-2">{{ $key }}</div>
                                <div class="col-xs-2 text-right">{{ $tax->rate }}</div>
                                <div class="col-xs-4 text-right">{{ $reversal && $tax->amount > 0 ? '-' : '' }}{{ $tax->total }}</div>
                                <div class="col-xs-4 text-right">{{ $reversal && $tax->amount > 0 ? '-' : '' }}{{ $tax->amount }}</div>
                            </div>
                        @endforeach
                    </div>
                    <div style="width: 30%"></div>
                </div>
                <div style="width: 8%; display: inline-block;"></div>
                <div style="width: 18%; display: inline-block; text-align: right">
                    <br>
                    <div>{{ number_format(($details->totalPrice - $details->totalDiscount) - $taxAmount, 2) }}</div>
                    <br>
                    <div>{{ number_format($taxAmount, 2) }}</div>
                    <br>
                    <div>{{ number_format(($details->totalPrice - $details->totalDiscount), 2) }}</div>
                    <br>
                </div>
            </div>
        @else
            <div class="page-break"></div>
        @endif
    </div>
@endforeach


