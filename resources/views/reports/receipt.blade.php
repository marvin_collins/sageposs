<div style="font-weight: 600 !important; font-family: monospace !important;">
@if($transaction->printed)
    <div class="reprint">
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
        REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT REPRINT
    </div>
@endif


@if ($transaction->transaction_type == 'Cash Receipt')
    <div style="background: #fff; font-size: 13px; font-family: monospace; width: 100%; padding: 2px;">
        @if($transaction->printed)
        <h2 class="text-center">REPRINT</h2>
        @endif
        <div class="text-center">{{ strtoupper($company->name) }}</div>
        <div class="text-center">{{ strtoupper($company->postal_address) }}</div>
        <div class="text-center">{{ strtoupper($company->telephone) }}</div>
        <div class="text-center">VAT #:{{ strtoupper($company->vat) }}</div>
        <div class="text-center">PIN: {{ strtoupper($company->pin) }}</div>
        <div class="text-center">
            <div><img src="data:image/png;base64, {{ base64_encode($generator->getBarcode($transaction->id, $generator::TYPE_CODE_128)) }}"></div>
            <div>RECEIPT {{ $transaction->transaction_number }}</div>
        </div>
        <h4 class="text-center"><strong> CASH RECEIPT </strong></h4>
        <h5 class="text-center"><strong> {{ strtoupper($transaction->warehouse->Name) }} </strong></h5>
        <div>{{ json_decode($transaction->transaction_data)->client->Name }}</div>
        <div>DATE: {{ $transaction->created_at->format('d/m/Y H:m:s') }}</div>

        <div class="row">
            <div class="col-xs-12" style="border-top: 1px dashed #000;"></div>
            <div class="col-xs-8">ITEM</div>
            <div class="col-xs-4 text-right">Amount</div>
        </div>

        <div class="row">
            <div class="col-xs-12" style="border-top: 1px dashed #000;"></div>
            <div class="col-xs-8">Receipt For Payment</div>
            <div class="col-xs-4 text-right">{{ number_format($transaction->amount, 2) }}</div>
            <div class="col-xs-8">{{ $transaction->payment_type }}</div>
        </div>
        <br>
        <br>

        <div class="row text-center">
            <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
            <div class="col-xs-12">PRICES INCLUSIVE OF VAT WHERE APPLICABLE</div>
            <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
            <div class="col-xs-12">YOU WERE SERVED BY {{ $transaction->creator->name }}</div>
            <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        </div>
    </div>
@else

    <?php
    $isCredit = false;
    foreach($details->payments as $payment) {
        if (strtolower($payment->name) == 'credit') {
            $isCredit = true;
            break;
        }
    }
    ?>

    <div style="background: #fff; font-size: 13px; font-family: monospace; width: 100%; padding: 2px;">
        @if($transaction->printed)
            <h2 class="text-center">REPRINT</h2>
        @endif
        <div class="text-center">{{ strtoupper($company->name) }}</div>
        <div class="text-center">{{ strtoupper($company->postal_address) }}</div>
        <div class="text-center">{{ strtoupper($company->telephone) }}</div>
        <div class="text-center">VAT #:{{ strtoupper($company->vat) }}</div>
        <div class="text-center">PIN: {{ strtoupper($company->pin) }}</div>
        <div class="text-center">
            <div><img src="data:image/png;base64, {{ base64_encode($generator->getBarcode($transaction->id, $generator::TYPE_CODE_128)) }}"></div>
            <div>RECEIPT {{ $transaction->transaction_number }}</div>
        </div>
        <h4 class="text-center">
            <strong>
                @if (strtoupper($transaction->transaction_type) == 'SALE MADE')
                    @if ($isCredit)
                        INVOICE
                    @else
                        CASH SALE
                    @endif
                @else
                    {{ strtoupper($transaction->transaction_type) }}{{ is_null($transaction->original_transaction_id) ? '' : ' REF# ' . $transaction->original_transaction_id }}
                @endif
            </strong>
        </h4>
        <h5 class="text-center"><strong> {{ strtoupper($transaction->warehouse->Name) }} </strong></h5>
        <div>{{ is_object($details->customer) ? $details->customer->Name : 'Cash Customer' }}</div>
        <div>DATE: {{ $transaction->created_at->format('d/m/Y H:m:s') }}</div>

        <?php $qty = 0; $totalWeight = 0; ?>
        @if($transaction->transaction_type == 'Approved: Sales Order')
            <div class="row">
                <div class="col-xs-12" style="border-top: 1px dashed #000;"></div>
                <div class="col-xs-8">ITEM</div>
                <div class="col-xs-2 text-right">QTY</div>
                <div class="col-xs-2">UOM</div>
            </div>
            <hr style="margin-top:0 !important; margin-bottom: 0 !important; border-top: 1px solid #999 !important;">

            @foreach($details->items as $item)
<!--                --><?php //$totalWeight += intval($item->ucIIUnitWt); ?>
                <div class="row">
                    <?php $qty += $item->quantity; ?>
                    <div class="col-xs-8">{{ $item->Code }}</div>
                    <div class="col-xs-2 text-right">{{ $reversal ? '-' : '' }}{{ $item->quantity }}</div>
                    <div class="col-xs-2">{{ $item->measure->name }}</div>
                    <div class="col-xs-12">{{ $item->Description_1 }}</div>
                </div>
                <hr style="margin-top:0 !important; margin-bottom: 0 !important; border-top: 1px solid #999 !important;">
            @endforeach
        @else
            <div class="row">
                <div class="col-xs-12" style="border-top: 1px dashed #000;"></div>
                <div class="col-xs-2">ITEM</div>
                <div class="col-xs-1 text-right">QTY</div>
                <div class="col-xs-2">UOM</div>
                <div class="col-xs-2 text-right">PRICE</div>
                <div class="col-xs-1 text-center">VAT</div>
                <div class="col-xs-3 text-right">AMOUNT INC</div>
            </div>
            <hr style="margin-top:0 !important; margin-bottom: 0 !important; border-top: 1px solid #999 !important;">
            @foreach($details->items as $item)
                <?php
                    if (! isset($item->sellingPrice)) {
                        $item->sellingPrice = $item->fInclPrice;
                    }
                    if ($item->quantity == 0) {
                        continue;
                    }
                ?>
<!--                --><?php //$totalWeight += intval($item->ucIIUnitWt); ?>
                <div class="row">
                    <?php $qty += $item->quantity; ?>
                    <div class="col-xs-2">{{ $item->Code }}</div>
                    <div class="col-xs-1 text-right">{{ $reversal ? '-' : '' }}{{ $item->quantity }}</div>
                    <div class="col-xs-2">{{ $item->measure->name }}</div>
                    <div class="col-xs-2 text-right">{{ number_format($item->sellingPrice) }}</div>
                    <div class="col-xs-1 text-center">{{ $item->TaxCode == '14' ? 'A' : ($item->TaxCode == '16' ? 'Z' : 'E') }}</div>
                    <div class="col-xs-3 text-right">{{ number_format(($item->quantity * $item->sellingPrice) - $item->discount, 2) }}</div>
                    <div class="col-xs-12">{{ $item->Description_1 }}</div>
                </div>
                <hr style="margin-top:0 !important; margin-bottom: 0 !important; border-top: 1px solid #999 !important;">
            @endforeach
            <br>
            <div class="row">
                <div class="col-xs-6">SUB TOTAL</div>
                <div class="col-xs-6 text-right">{{ number_format($details->totalPrice, 2) }}</div>
                <div class="col-xs-6">TOTAL DISCOUNT</div>
                <div class="col-xs-6 text-right">{{ number_format($details->totalDiscount, 2) }}</div>
                <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
                <div class="col-xs-6"><h4>TOTAL</h4></div>
                <div class="col-xs-6 text-right"><h4>{{ number_format($details->totalPrice - $details->totalDiscount, 2) }}</h4></div>
                <?php $pays = 0; ?>
                @foreach($details->payments as $payment)
                <?php $pays += $payment->amount; ?>
                    <div class="col-xs-8">{{ strtoupper($payment->name) }}{{ $reversal ? ' GIVEN' : '' }}{{ $payment->reference == '' ? '' : ': ' . strtoupper($payment->reference) }}</div>
                    <div class="col-xs-4 text-right">{{ number_format($payment->amount, 2) }}</div>
                @endforeach
                @if(! $reversal)
                    <div class="col-xs-6">CHANGE</div>
                    <div class="col-xs-6 text-right">{{ number_format($pays - ($details->totalPrice - $details->totalDiscount), 2) }}</div>
                @endif
                <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
                <div class="col-xs-12">TOTAL ITEMS: {{ $qty }}</div>
            </div>
            <div class="row">
                <div class="col-xs-1"><strong>CODE</strong></div>
                <div class="col-xs-1 text-right"><strong>RATE</strong></div>
                <div class="col-xs-3 text-right"><strong>NET</strong></div>
                <div class="col-xs-3 text-right"><strong>VAT</strong></div>
                <div class="col-xs-3 text-right"><strong>TOTAL</strong></div>
            </div>
            <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>

            @foreach($details->tax as $key => $tax)
                <div class="row">
                    <div class="col-xs-1">{{ $key }}</div>
                    <div class="col-xs-1 text-right">{{ $tax->rate }}</div>
                    <div class="col-xs-3 text-right">{{ $reversal && $tax->total > 0 ? '-' : '' }}{{ $tax->total }}</div>
                    <div class="col-xs-3 text-right">{{ $reversal && $tax->amount > 0 ? '-' : '' }}{{ $tax->amount }}</div>
                    <div class="col-xs-3 text-right">{{ $reversal && $tax->inclusive > 0 ? '-' : '' }}{{ $tax->inclusive }}</div>
                </div>
            @endforeach
            <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @endif

        <br>

        <div class="row">
            <div class="col-xs-3">
                WEIGHT: {{ number_format(intval($details->totalWeight) > 0 ? $details->totalWeight : $totalWeight, 2) }} KGs
            </div>
            <div class="col-xs-3">
                VEHICLE NUMBER: ................
            </div>
            <div class="col-xs-3">
                DRIVER: .............
            </div>



        @if($isCredit)
            <div class="col-xs-3">
                RECEIVER: ..................
            </div>
            <div class="col-xs-3">
                RECEIVER SIGNATURE: ............
            </div>
        @endif
        </div>
        <br>

        @if($transaction->transaction_type != 'Approved: Sales Order')
            <div class="row text-center">
                <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
                <div class="col-xs-12">PRICES INCLUSIVE OF VAT WHERE APPLICABLE</div>
                <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
                <div class="col-xs-12">YOU WERE SERVED BY {{ $transaction->creator->name }}</div>
                <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
            </div>
            <br>
        @endif
    </div>
@endif
</div>