<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Receipt</title>
    <link rel="stylesheet" href="{{ asset(elixir('css/app.css')) }}">
    <script>
        window.onload = function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 10);
        }
    </script>
</head>
<body style='background: #fff; font-size: 13px; font-family: monospace;'>

<div style="background: #fff; font-size: 13px; font-family: monospace; width: 100%; padding: 2px;">
    <div class="text-center">{{ strtoupper($company->name) }}</div>
    <div class="text-center">{{ strtoupper($company->postal_address) }}</div>
    <div class="text-center">{{ strtoupper($company->telephone) }}</div>
    <div class="text-center">VAT #:{{ strtoupper($company->vat) }}</div>
    <div class="text-center">PIN: {{ strtoupper($company->pin) }}</div>
    <h4 class="text-center"><strong> DAY SUMMARY </strong></h4>
    <div>{{ strtoupper($company->name) }} END OF DAY TRANSACTIONS</div>
    <div>DATE: {{ $day->format('d F Y') }}</div>

    <br>
    <div><strong>TRANSACTION SUMMARY</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>

        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-8"><strong>TOTAL SALES</strong></div>
                <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalSalesAmount'], 2) }}</strong></div>
            </div>
            <div class="row">
                <div class="col-xs-8" style="padding-left: 40px">CASH SALES</div>
                <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalSalesAmount'] - $endOfDayDetails['totalCredit'], 2) }}</div>
            </div>
            <div class="row">
                <div class="col-xs-8" style="padding-left: 40px">CREDIT SALES</div>
                <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalCredit'], 2) }}</div>
            </div>
        </div>

        <div class="col-xs-8">CREDIT CASH RECEIVED</div>
        <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalCashReceipts'], 2) }}</div>

        <div class="col-xs-8">CREDIT MPESA RECEIVED</div>
        <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalMpesaReceipts'], 2) }}</div>

        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-8"><strong>TOTAL RETURNS</strong></div>
                <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalReturnsAmount'], 2) }}</strong></div>
            </div>
            <div class="row">
                <div class="col-xs-8" style="padding-left: 40px">CASH RETURNS</div>
                <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalCashReturnsAmount'], 2) }}</div>
            </div>
            <div class="row">
                <div class="col-xs-8" style="padding-left: 40px">CREDIT RETURNS</div>
                <div class="col-xs-4 text-right">{{ number_format($endOfDayDetails['totalReturnsAmount'] - $endOfDayDetails['totalCashReturnsAmount'], 2) }}</div>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-8"><strong>TILL DRAWINGS</strong></div>
                <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['pettyCashAmount'], 2) }}</strong></div>
            </div>
            @foreach($endOfDayDetails['pettyCash'] as $key => $amount)
                <div class="row">
                    <div class="col-xs-8" style="padding-left: 40px">{{ strtoupper($key) }}</div>
                    <div class="col-xs-4 text-right">{{ number_format($amount, 2) }}</div>
                </div>
            @endforeach
        </div>

        <div class="col-xs-8">M-PESA</div>
        <div class="col-xs-4 text-right">({{ number_format($endOfDayDetails['totalMpesa'], 2) }})</div>
    </div>

    <br>
    <div><strong>M-PESA TRANSACTIONS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['mpesaTransactions'] as $payment)
            <div class="col-xs-8">REF: {{ $payment->reference }}</div>
            <div class="col-xs-4 text-right">{{ number_format($payment->amount, 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>MPESA TOTAL</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalMpesa'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>CREDIT CASH RECEIVED</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['cashReceipts'] as $payment)
            <div class="col-xs-8">REF: {{ $payment['client'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($payment['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL RECEIVED</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalCashReceipts'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>CREDIT MPESA RECEIVED</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['mpesaReceipts'] as $payment)
            <div class="col-xs-8">REF: {{ $payment['client'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($payment['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL RECEIVED</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalMpesaReceipts'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>CASH SALE RETURNS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['cashSaleReturns'] as $returns)
            <div class="col-xs-8">{{ $returns['transaction_number'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($returns['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL RETURNS</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['totalReturnsAmount'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>

    <br>
    <div><strong>TILL DRAWINGS</strong></div>
    <div class="row">
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        @foreach($endOfDayDetails['pettyCashTransactions'] as $pettyCash)
            <div class="col-xs-8">{{ $pettyCash['type'] }} - {{ $pettyCash['reference'] }}</div>
            <div class="col-xs-4 text-right">{{ number_format($pettyCash['amount'], 2) }}</div>
        @endforeach
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
        <div class="col-xs-8"><strong>TOTAL DRAWINGS</strong></div>
        <div class="col-xs-4 text-right"><strong>{{ number_format($endOfDayDetails['pettyCashAmount'], 2) }}</strong></div>
        <div class="col-xs-12" style="border-bottom: 1px dashed #000;"></div>
    </div>
</div>
</body>
</html>
