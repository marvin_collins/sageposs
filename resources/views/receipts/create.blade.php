@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Receive Cash</div>

                    <div class="panel-body">
                        <form action="{{ route('receipts.store') }}" method="post" role="form" target="_blank" id="receipt-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="type">Type*</label>
                                <select name="type" id="type" class="form-control" required>
                                    <option value="Cash">Cash</option>
                                    <option value="M-Pesa">M-Pesa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="client_id">Client*</label>
                                <select name="client_id" id="client_id" class="form-control" required>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->DCLink }}">{{ $client->Name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="amount">Amount Received*</label>
                                <input name="amount" id="amount" class="form-control" type="number" min="0" required>
                            </div>
                            <div class="form-group">
                                <label for="reference">Reference</label>
                                <textarea name="reference" id="reference" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input class="btn btn-success" type="submit" value="Process Payment">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $('#client_id').select2();
        $('#receipt-form').on('submit', function () {
           setTimeout(function () {
               $('#amount').val('');
               $('#reference').val('');
           }, 1000);
        });
    </script>
@endsection