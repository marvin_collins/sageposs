@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">End Of Day Report</div>
                <div class="panel-body">
                    <form class="form-horizontal" target="_blank" role="form" method="POST" action="{{ url('admin-end-of-day') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('warehouse_id') ? ' has-error' : '' }}">
                            <label for="user_id" class="col-md-4 control-label">Select Agent</label>

                            <div class="col-md-6">
                                <select name="user_id" id="user_id" class="form-control" required>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('user_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('warehouse_id') ? ' has-error' : '' }}">
                            <label for="warehouse_id" class="col-md-4 control-label">Select Store</label>

                            <div class="col-md-6">
                                <select name="warehouse_id" id="warehouse_id" class="form-control" required>
                                @foreach($warehouses as $warehouse)
                                    <option value="{{ $warehouse->WhseLink }}">{{ $warehouse->Name }}</option>
                                @endforeach
                                </select>

                                @if ($errors->has('warehouse_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('warehouse_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-4 control-label">Select Date</label>

                            <div class="col-md-6">
                                <input type="text" id="date" class="form-control" name="date">

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Get Report
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $('#date').datepicker({
                autoclose: true,
                endDate: '0d',
                format: 'dd-mm-yyyy'
            });
            $('#date').datepicker('setDate', new Date());
        });
    </script>
@endsection
