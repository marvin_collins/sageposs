@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Your Inventory</div>

                    <div class="panel-body">
                        <table class="table table-responsive table-striped" id="inventoryTable">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Available Warehouse</th>
                                <th class="text-right">Qty On Hand</th>
                                <th class="text-right">Weight</th>
                                <th class="text-right">Stocking Unit Price</th>
                                <th class="text-right">Outer Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function() {
            $('#inventoryTable').DataTable( {
                "columnDefs": [
                    { className: "text-right", targets: [3, 4, 5, 6] }
                ],
                "ajax": '/inventory'
            } );
        } );
    </script>
@endsection
