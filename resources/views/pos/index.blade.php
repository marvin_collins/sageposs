@extends('layouts.app')

@section('content')
    <pos-index :settings='{!! json_encode($settings) !!}'></pos-index>
@endsection
