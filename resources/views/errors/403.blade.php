@extends('layouts.app')

@section('content')
<div class="container-fluid text-center">
    <h2>Whoops!</h2>
    <h4>Sorry, you are not authorized to perform that action.</h4>
</div>
@endsection
