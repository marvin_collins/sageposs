@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Your Clients</div>

                    <div class="panel-body">
                        <client-index></client-index>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
