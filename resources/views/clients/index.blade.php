@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Your Clients</div>

                    <div class="panel-body">
                        <table class="table table-responsive table-striped" id="clientTable">
                            <thead>
                            <tr>
                                <th>Account #</th>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>EMail</th>
                                <th class="text-right">Credit Limit</th>
                                <th class="text-right">DCBalance</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total = 0; ?>
                            @foreach($clients as $client)
                                <?php $total += $client->DCBalance == null ? 0 : $client->DCBalance; ?>
                            <tr>
                                <td>{{ $client->Account }}</td>
                                <td>{{ $client->Name }}</td>
                                <td>{{ $client->Telephone }}</td>
                                <td>{{ $client->EMail }}</td>
                                <td class="text-right">{{ $client->Credit_Limit == null ? 0 : number_format($client->Credit_Limit, 2) }}</td>
                                <td class="text-right">{{ $client->DCBalance == null ? 0 : number_format($client->DCBalance, 2) }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-right">TOTAL</th>
                                <th class="text-right"></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function() {
            $('#clientTable').DataTable({
                "lengthMenu": [ [10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"] ],
                "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        if (typeof i === 'string') {
                            let value = i.replace(/[,]/g, '');
                            value = value.replace(/[(]/g, '-');
                            value = value.replace(/[)]/g, '') * 1;

                            return value;
                        }

                        return typeof i === 'number' ? i : 0;
                    };

                    let total = api
                        .column(5, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(5).footer()).html(window.numeral(total).format('0,0.00'));
                }
            });
        } );
    </script>
@endsection
