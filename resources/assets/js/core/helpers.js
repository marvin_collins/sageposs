window.notify = (status = 'info', title, message, persist = false, cancel = false, confirmCallback = null) => {
    sflash({
        title: title,
        text: message,
        type: status,
        allowOutsideClick: true,
        showCancelButton: cancel,
        showConfirmButton: persist,
        confirmButtonText: "Okay",
        timer: persist ? null : 2500
    }, confirmCallback);
};