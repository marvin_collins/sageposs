
export function httpRequest(vm, method, uri, form, successCallback, errorCallback = (response) => {}) {
    vm.$dispatch('loading', true);
    vm.$http[method](uri, form)
        .then(response => {
            vm.$dispatch('loading', false);
            return response.json();
        })
        .then(response => {
            successCallback(response);
        })
        .catch(response => {
            if (typeof response.data === 'object') {
                form.errors = _.flatten(_.toArray(response.data));
            } else {
                form.errors = ['Something went wrong. Please try again.'];
            }
            errorCallback(response);
            vm.$dispatch('loading', false);
        });
}
