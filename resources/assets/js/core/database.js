window.fluentDb = require('fluent-indexed-db');

const VERSION = 3;
fluentDb.open('POS', VERSION, function (upgradeDB) {
    switch(upgradeDB.oldVersion) {
        case 0:
            fluentDb.createObject(upgradeDB, 'clients', { keyPath: 'DCLink' });
            fluentDb.createObject(upgradeDB, 'products', { keyPath: 'idWhseStk' });
            fluentDb.createObject(upgradeDB, 'projects', { keyPath: 'ProjectLink' });
            console.log('created version 1');
        case 1:
            fluentDb.createIndex(upgradeDB, 'products', 'code', 'Code');
            fluentDb.createIndex(upgradeDB, 'projects', 'projectcode', 'ProjectCode');
            fluentDb.createIndex(upgradeDB, 'projects', 'projectname', 'ProjectName');
            fluentDb.createIndex(upgradeDB, 'products', 'description', 'Description_1');
            fluentDb.createIndex(upgradeDB, 'clients', 'account', 'Account');
            fluentDb.createIndex(upgradeDB, 'clients', 'telephone', 'Telephone');
            fluentDb.createIndex(upgradeDB, 'clients', 'name', 'Name');
            console.log('created version 2');
        case 2:
            fluentDb.createObject(upgradeDB, 'settings');
            console.log('created version 3');
    }
});