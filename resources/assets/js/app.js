/** require bootstrap items **/

require('./bootstrap');
require('./components');
window._Notification = new Audio('/sounds/notifications.mp3');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

const app = new Vue({
    el: 'body',
    ready() {
        let notifications = localStorage.getItem('notifications');
        if (notifications) {
            this.notifications = JSON.parse(localStorage.getItem('notifications'));
        }
    },
    data: {
        notifications: []
    },
    methods: {
        clearNotifications() {
            this.notifications = [];
            window.localStorage.removeItem('notifications');
        },
        notifyUser(transactionId, text, uri) {
            this.notifications.push({ id:transactionId, message: text, location: uri });
            window.localStorage.setItem('notifications', JSON.stringify(this.notifications));
        },
        clearNotification(id) {
            "use strict";
            this.notifications = this.notifications.filter((notification) => {
                return notification.id != id;
            });
            window.localStorage.setItem('notifications', JSON.stringify(this.notifications));
        },
        removeNotification(notification) {
            this.notifications.$remove(notification);
            window.localStorage.setItem('notifications', JSON.stringify(this.notifications));
            window.location = notification.location;
        }
    }
});

$('#app-navbar-collapse').find('a[href="' + window.location + '"]')
    .parents('li')
    .addClass('active');

if (Laravel.eEnabled) {
    Echo.join('POS.Online.' + Laravel.drimbler)
        .here((users) => {
            console.log(users);
        })
        .joining((user) => {
            console.log('joining');
            console.log(user);
        })
        .leaving((user) => {
            console.log('leaving');
            console.log(user);
        });
}
window.app = app;

// $('.dataTable').DataTable();