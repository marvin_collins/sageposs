<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', 'APIController@getUser')->middleware('auth:api');

Route::get('log-point', 'APIController@getLogPoint');
Route::get('current/queries/{index}', 'APIController@fetchCurrentQueries');
Route::get('current/push', 'APIController@pushCurrent');
Route::post('receive', 'APIController@receive');
Route::get('last-sync', 'APIController@lastSync');
