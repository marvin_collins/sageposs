<?php

//use POS\Models\Transactions;
//
//Route::get('test', function () {
//
////    $items = unserialize(file_get_contents('results.txt'));
////
////    return view('welcome')->with('lines', $items);
//
//    $headers = [
//        'invoice_id',
//        'invoice_line',
//        'sage_amount',
//        'pos_excl',
//        'pos_discount',
//        'pos_amount',
//        'difference',
//    ];
//
//    file_put_contents('results.csv', implode(',', $headers) . "\n");
//
//    $transactions = Transactions::join('invnum', 'transactions.invoice_id', '=', 'invnum.autoindex')
//        ->whereRaw('invnum.invtotincl <> transactions.amount and id > 54000')
//        ->select(['invoice_id', 'transaction_data'])
//        ->chunk(100, function ($result) {
//            $items = array_flatten($result->map(function ($item) { return $item->invoice_id;})->toArray());
//            $lines = DB::table('_btblInvoiceLines')
//                ->whereIn('iInvoiceId', $items)
//                ->get(['idInvoiceLines', 'iInvoiceId', 'iStockCodeID', 'fQuantityLineTotIncl'])
//                ->groupBy('iInvoiceId');
//
//            $result = $result->map(function ($item) {
//                $decoded = json_decode($item->transaction_data)->items;
//                $item->transaction_data = $decoded;
//
//                return $item;
//            })->toArray();
//
//            $unmatched = [];
//
//            foreach ($result as $res) {
//                $item = $lines->get($res['invoice_id'])->keyBy('iStockCodeID');
//
//                foreach ($res['transaction_data'] as $data) {
//                    $total = $data->quantity * $data->sellingPrice;
//                    $totalExl = $data->quantity * $data->fExclPrice;
//                    $sage = $item->get($data->StockLink);
//                    if (! $sage) {
//                        continue;
//                    }
//
//                    $diff = $sage->fQuantityLineTotIncl - $total;
//
//                    if ($diff) {
////                        $unmatched [] = [
////                            'invoice_id' => $res['invoice_id'],
////                            'invoice_line' => $sage->idInvoiceLines,
////                            'sage_amount' => $sage->fQuantityLineTotIncl,
////                            'pos_excl' => $totalExl,
////                            'pos_discount' => $data->discount,
////                            'pos_amount' => $total,
////                            'difference' => $diff
////                        ];
//                        $body = [
//                            $res['invoice_id'],
//                            $sage->idInvoiceLines,
//                            $sage->fQuantityLineTotIncl,
//                            $totalExl,
//                            $data->discount,
//                            $total,
//                            $diff
//                        ];
//                        file_put_contents('results.csv', implode(',', $body) . "\n", FILE_APPEND);
//                    }
//                }
//            }
//
////            $original = [];
////            if (is_file('results.txt')) {
////                $original = unserialize(file_get_contents('results.txt'));
////            }
////
////            file_put_contents('results.txt', serialize(array_merge($unmatched, $original)));
//        });
//
//
//    dd($transactions);
//});

Route::get('home', 'HomeController@home');

Route::group(['middleware' => ['auth', 'checkedIn']], function () {
    Route::get('/', function () {
        return redirect()->route('pos.index');
    });

    Route::post('pos/save', 'POSController@save');
    Route::post('pos/approval', 'POSController@approval');
    Route::resource('pos', 'POSController');
    Route::post('add-customer', 'POSController@addCustomer');
    Route::resource('petty-cash', 'PettyCashController');
    Route::resource('transactions', 'TransactionsController');
    Route::resource('receipts', 'ReceiptsController');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('pos/print/{id}', 'POSController@printReceipt');
    Route::get('admin-end-of-day', 'TransactionsController@getAdminEndDay')->name('admin.endofday');
    Route::get('admin-complete-end-of-day', 'TransactionsController@getAdminCompleteEndDay');
    Route::get('admin-log', 'TransactionsController@getLog')->name('admin.log');
    Route::post('admin-end-of-day', 'TransactionsController@adminEndDay');
    Route::post('admin-complete-end-of-day', 'TransactionsController@adminCompleteEndDay');
    Route::resource('check-in', 'CheckinController', ['except' => ['index', 'show']]);
    Route::resource('till', 'TillController', ['except' => ['index', 'show']]);
    Route::resource('roles', 'RolesController');
    Route::get('users/import', 'UsersController@import');
    Route::resource('users', 'UsersController');
    Route::resource('assignment', 'AssignmentController');
    Route::resource('clients', 'ClientsController');
    Route::resource('petty-types', 'PettyCashTypesController');
    Route::resource('inventory', 'InventoryController');
    Route::get('endday', 'TransactionsController@getEndDay');
    Route::post('endday/', 'TransactionsController@endDay');
    Route::resource('admin/transactions', 'AdminTransactionsController', ['as' => 'admin']);
    Route::resource('approvals', 'ApprovalController');
    Route::post('pos/process', 'POSController@process');
    Route::resource('settings', 'SettingController');
});

Auth::routes();

